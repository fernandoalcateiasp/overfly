/********************************************************************
modalcaracteristicas.js

Library javascript para o modalcaracteristicas.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_aEmpresaData = null;
var glb_nUserID = 0;
// Controla se algum dado do grid foi alterado
var glb_dataGridWasChanged = false;
var glb_CaracteristicasTimerInt = null;
var glb_bReplica = false;
var glb_nProdutoID;
var glb_bGravaAuto = false;
var glb_chkDescricaoAutomaticaAntes;
var glb_sMensagem = "";
var glb_nB4I;
var glb_verificaDescricao = null;
var glb_nRowSelected;

// Dados do grid .RDS
var dsoGrid = new CDatatransport("dsoGrid");
var dsoExecSincroniza = new CDatatransport("dsoExecSincroniza");
var dsoReplicaCaracteristicas = new CDatatransport("dsoReplicaCaracteristicas");
var dsoSalvaGrid = new CDatatransport("dsoSalvaGrid");
var dsoDescricaoAutomatica = new CDatatransport("dsoDescricaoAutomatica");
var dsoValor = new CDatatransport("dsoValor");

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
cmb_onchange()
btn_onclick(ctl)
refreshParamsAndDataAndShowModalWin(fromServer)
getCurrEmpresaData()
setupBtnsFromGridState()
fillGridData()
fillGridData_DSC()
saveDataInGrid()
saveDataInGrid_DSC()

EVENTOS DE GRID

DEFINIDAS NO ARQUIVO modalcaracteristicas.ASP
Sub fg_BeforeRowColChange(OldRow, OldCol, NewRow, NewCol, Cancel)
    Cancel = js_fg_BeforeRowColChange (fg, OldRow, OldCol, NewRow, NewCol, Cancel)
End Sub

Sub fg_AfterRowColChange(OldRow, OldCol, NewRow, NewCol)
    fg_AfterRowColChange()
End Sub

Sub fg_EnterCell()
    js_fg_EnterCell fg
End Sub
FINAL DE DEFINIDAS NO ARQUIVO modalcaracteristicas.ASP

js_fg_modalcaracteristicasBeforeMouseDown(fg, Button, Shift, X, Y, Cancel)
js_fg_modalcaracteristicasDblClick( grid, Row, Col)
js_modalcaracteristicasKeyPress(KeyAscii)
js_modalcaracteristicas_AfterRowColChange
js_modalcaracteristicas_ValidateEdit()
js_modalcaracteristicas_AfterEdit(Row, Col)
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    modalFrame = getFrameInHtmlTop('frameModal');
    modalFrame.style.top = 27;
    
    window_onload_1stPart();
    
    // ajusta o body do html
    with (modalcaracteristicasBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // configuracao inicial do html
    setupPage();   

    // mostra a modal
    refreshParamsAndDataAndShowModalWin(true);
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() 
{
    // Costura o titulo da modal
    glb_nProdutoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'txtRegistroID.value');
    
    var sProdutoDescricao = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['ProdutoDescricao'].value");

    secText(('Alterar Caracter�sticas - ' + sProdutoDescricao + ' #' + glb_nProdutoID), 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var eLeft = 2;
    var eTop = 1;
    var rQuote = 0;
    var btnWidth = 78;
    
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    eLeft = 2;
    eTop = 2;
    rQuote = 0;
    
    // desabilita o botao OK
    //btnOK.disabled = true;
    DisableBtnOk();

    // troca label botao Cancelar   
    btnCanc.value = 'Cancelar';
    
    // ajusta o divFG
    with (divFG.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;    
        height = 280;
    }
    
    with (fg.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10) - ELEM_GAP;
    }

    // ajusta o btnIncluir
    with (btnIncluir.style) 
    {
        top = 526;
        left = ELEM_GAP;
    }

    // ajusta o btnExcluir
    with (btnExcluir.style) {
        top = btnIncluir.style.top;
        left = parseInt(btnIncluir.currentStyle.left, 10) + parseInt(btnIncluir.currentStyle.width, 10) + ELEM_GAP;
    }

    // ajusta o chkDescricaoAutomatica
    with (chkDescricaoAutomatica.style) {
        top = parseInt(btnExcluir.currentStyle.top, 10) + 2;
        left = 355;
        width = FONT_WIDTH * 3;
    }

    // ajusta o lblDescricaoAutomatica
    with (lblDescricaoAutomatica.style) {
        top = parseInt(btnExcluir.currentStyle.top, 10) - ELEM_GAP - 1;
        left = 358;
    }

    // ajusta o selValores
    with (selValor.style) {
        top = 292;
        left = 0;
        height = 195;
        width = 620; //317;
    }

    // ajusta o selValores
    with (lblValor.style) {
        fontSize = '10pt';
        width = 400;
        top = parseInt(selValor.currentStyle.top, 10) - ELEM_GAP - 5;
        left = selValor.style.left;
    }

    selValor.ondblclick = selValor_ondlbclick;

    glb_chkDescricaoAutomaticaAntes = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'chkAuto.checked');
    
    glb_verificaDescricao = window.setInterval("verificaDescricaoAutomatica()", 500, 'JavaScript');
    
    /*if (chkDescricaoAutomatica.checked) {
        glb_bGravaAuto = true;
    }*/

    btnOK.disabled = false;

    startGridInterface(fg);
    fg.Cols = 0;
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;    
    fg.Redraw = 2;
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;
    
    if (glb_chkDescricaoAutomaticaAntes != chkDescricaoAutomatica.checked)
        glb_bGravaAuto = true;

    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnOK')
    {
            if (glb_sMensagem.length > 0 && chkDescricaoAutomatica.checked) 
            {
                if (window.top.overflyGen.Alert(glb_sMensagem) == 0)
                    return null;

                return null;
            }
            else 
            {
                if (glb_bGravaAuto)
                    gravaDescricaoAutomatica();
                
                saveDataInGrid();

                
                
                /*if (glb_bReplica) {
                //replicaCaracteristicas(glb_nProdutoID);
                sincronizaCaracteristicas('D', glb_nProdutoID);
                }*/

                sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, (glb_bGravaAuto ? 'SupRefr' : null));
        }
    }
    // inclui linhas de caracteristicas
    else if (controlID == 'btnIncluir') {
        sincronizaCaracteristicas('I', glb_nProdutoID);
    }
    // exclui linhas de caracteristicas sem OK
    else if (controlID == 'btnExcluir') {
        sincronizaCaracteristicas('D', glb_nProdutoID);
    }
    // fecha modal
    else if (controlID == 'btnCanc') {
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, (glb_bGravaAuto ? 'SupRefr' : null));
    }

    /*
    // bot�o proximo
    
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null);
        sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, '__btn_SEG(' + '\'' + 'sup' + '\'' + ')');
        sendJSMessage('INF_HTML', JS_DATAINFORM, EXECEVAL, 'openModalCaracteristica()');
        //openModalCaracteristica();
    
    
    */

    // codigo privado desta janela
    else {
        var _retMsg;
        var sMsg = '';

        if (sMsg != '') {
            _retMsg = window.top.overflyGen.Confirm(sMsg);

            if ((_retMsg == 0) || (_retMsg == 2))
                return null;
        }

        //restoreInterfaceFromModal();
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null);
    }    
}

/********************************************************************
Evento do checkbox Auto
********************************************************************/
function chkDescricaoAutomatica_onclick() {
    if (chkDescricaoAutomatica.checked)
        glb_bReplica = true;
}

/********************************************************************
Funcao chamada pela automacao
Esta funcao so executa se a modal havia sido previamente executada,
ou seja, apos a primeira carga da modal.
Atualizar parametros e dados aqui e apos, mostrar a modal.

Parametros:
fromServer  - true, a modal esta vindo do servidor
            - null, a modal ja esta carregada

********************************************************************/
function refreshParamsAndDataAndShowModalWin(fromServer)
{
    // @@ atualizar parametros e dados aqui
    glb_aEmpresaData = getCurrEmpresaData();
    glb_nUserID = glb_USERID;
        
    // @@ atualizar interface aqui
    //setupBtnsFromGridState();
    
    // mostra a janela modal
    fillGridData();
}

/********************************************************************
Trava/destrava todos os botoes
********************************************************************/
function setupBtnsFromGridState()
{
    var bHasRowsInGrid = fg.Rows > 1;
    
    btnOK.disabled = !(bHasRowsInGrid && glb_dataGridWasChanged);
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridData()
{
    if (glb_CaracteristicasTimerInt != null)
    {
        window.clearInterval(glb_CaracteristicasTimerInt);
        glb_CaracteristicasTimerInt = null;
    }

	var aEmpresa = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,'getCurrEmpresaData()');

    lockControlsInModalWin(true);
    
    // zera o grid
    fg.Rows = 1;
    
    glb_dataGridWasChanged = false;
    
    var nProdutoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['ProdutoID'].value");

    setConnection(dsoValor);
/*
    dsoValor.SQL = 'SELECT DISTINCT  CaracteristicaID, Valor ' +
                        'FROM Conceitos a WITH(NOLOCK) ' +
                            'INNER JOIN Conceitos_Caracteristicas b WITH(NOLOCK) ON (b.ProdutoID = a.ConceitoID) ' +
                            'WHERE a.ProdutoID = ' + nProdutoID ;

    dsoValor.ondatasetcomplete = fillGridData_DSC;
    dsoValor.Refresh();
*/

    // parametrizacao do dso dsoGrid
    setConnection(dsoGrid);

    dsoGrid.SQL =
        'SELECT a.ConCaracteristicaID, a.Checado, a.CaracteristicaID, a.Valor, dbo.fn_Produto_CaracteristicaOrdem(a.ProdutoID, a.CaracteristicaID) AS Ordem, ' +
        '(SELECT dbo.fn_Tradutor(Conceito, ' + aEmpresa[7] + ', ' + aEmpresa[8] + ', NULL) FROM Conceitos WITH(NOLOCK) WHERE ConceitoID = a.CaracteristicaID) AS Caracteristica, ' +
        'b.DescricaoAutomatica, ' +
        'dbo.fn_Produto_CaracteristicaBit(a.ProdutoID, a.CaracteristicaID, 1) AS FichaTecnica, ' +
        'dbo.fn_Produto_CaracteristicaBit(a.ProdutoID, a.CaracteristicaID, 2) AS Recebimento, ' +
        'dbo.fn_Produto_CaracteristicaBit(a.ProdutoID, a.CaracteristicaID, 3) AS Descricao, ' +
        'dbo.fn_Produto_CaracteristicaBit(a.ProdutoID, a.CaracteristicaID, 4) AS DescricaoComplementar, ' +
        'dbo.fn_Produto_CaracteristicaBit(a.ProdutoID, a.CaracteristicaID, 5) AS Obrigatorio, ' +
        'dbo.fn_Produto_CaracteristicaBit(a.ProdutoID, a.CaracteristicaID, 6) AS Imagens, ' +
        'dbo.fn_Produto_CaracteristicaBit(a.ProdutoID, a.CaracteristicaID, 7) AS PesosMedidas, ' +
        'dbo.fn_Produto_CaracteristicaBit(a.ProdutoID, a.CaracteristicaID, 8) AS Web ' +
        'FROM Conceitos_Caracteristicas a WITH(NOLOCK) ' +
        'INNER JOIN Conceitos b WITH(NOLOCK) ON (b.ConceitoID = a.ProdutoID) ' +
        'WHERE a.ProdutoID = ' + glb_nProdutoID + ' ' +
        'ORDER BY Ordem';

    dsoGrid.ondatasetcomplete = fillGridData_DSC;
    dsoGrid.Refresh();
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData_DSC() 
{
    showExtFrame(window, true);
    // restoreInterfaceFromModal(true);

    var dTFormat = '';
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';
        
    fg.Redraw = 0;
    fg.Editable = false;
    startGridInterface(fg);
    fg.FrozenCols = 0;
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;    
    
    headerGrid(fg,['Ordem',
                   'OK',
                   'Caracter�stica',
                   'Valor',
                   'FT',
                   'Rec',
                   'Desc',
                   'DC',
                   'Obrig',
                   'Img',
                   'PM',
                   'Web',
                   'CaracteristicaID',
                   'ConCaracteristicaID'],[12, 13]);
                       
    fillGridMask(fg,dsoGrid,['Ordem*',
                             'Checado', 
                             'Caracteristica*',
                             'Valor',
                             'FichaTecnica*',
                             'Recebimento*',
                             'Descricao*',
                             'DescricaoComplementar*',
                             'Obrigatorio*',
                             'Imagens*',
                             'PesosMedidas*',
                             'Web*',
                             'CaracteristicaID',
							 'ConCaracteristicaID'],
                             ['99', '', '', '', '', '', '', '', '', '', '', '', '', '']);

    glb_aCelHint = [[0, 4, 'Esta Caracter�stica ser� usada na Ficha T�cnica do produto?'],
                        [0, 5, 'Esta Caracter�stica ser� usada no Recebimento do produto?'],
                        [0, 6, 'Esta Caracter�stica ser� usada para compor a Descri��o do produto?'],
						[0, 7, 'Esta Caracter�stica ser� usada para compor a Descri��o Complementar do produto?'],
						[0, 8, 'Esta Caracter�stica � obrigatoria para essa fam�lia?'],
                        [0, 9, 'Esta Caracter�stica ser� usada para replicar imagens para os produtos da mesma s�rie desta fam�lia?'],
                        [0, 10, 'Esta Caracter�stica ser� usada para replicar pesos e medidas para os produtos da mesma s�rie desta fam�lia?'],
                        [0, 11, 'Esta Caracter�stica ser� usada na pesquisa da Web?']];

    pintaGrid(null);

    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);
    fg.FrozenCols = 3;
    fg.Redraw = 2;

/*
    var aLinesState = new Array();
    for (i = 1; i < fg.Rows; i++)
        aLinesState[aLinesState.length] = true;

    setLinesState(fg, aLinesState);    
*/

    // Nao retirar o if abaixo, ela garante que o grid
    // so deixa editar a coluna 4
    if ( fg.Rows > 1 )
        fg.Col = 3;

    if (fg.Rows > 1)
        fg.Editable = true;


    //insertcomboData(fg, getColIndexByColKey(fg, 'Valor'), dsoValor, 'Valor', 'caracter�sticaID');

    lockControlsInModalWin(false);

    // se nenhuma linha foi selecionada, seleciona a primeira
    // se a mesma existe
    if ( (fg.Row < 1) && (fg.Rows > 1) )
        fg.Row = 1;
        
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;    
    
    fg.Redraw = 2;

    // se tem linhas no grid, coloca foco no grid
    if (fg.Rows > 1)
    {
        window.focus();
        fg.focus();
    }                
    else
    {
        ;
    }

    // ajusta estado dos botoes
    //setupBtnsFromGridState();
}

function pintaGrid(nRow)
{
    var nBranco = 0XFFFFFF;
    var nVerde = 0X90EE90;
    var nAzul = 0XFFDBBF;
    var nAmarelo = 0X8CE6F0;
    var nLaranja = 0X60A4F4;
    var nVermelho = 0X7280FA;
    var nRosa = 0XFF99FF;

    var nContadorAmarelo = 0, nContadorLaranja = 0, nContadorVermelho = 0, nContadorRosa = 0;

    var sValor = '';
    
    for (i = (nRow == null ? 1 : nRow); i < fg.Rows; i++) 
    {
        sValor = fg.TextMatrix(i, getColIndexByColKey(fg, 'Valor'));
        
        sValor = (sValor == null ? '' : sValor);

        if (fg.TextMatrix(i, getColIndexByColKey(fg, 'Checado')) == 0) 
        {
            if ((fg.TextMatrix(i, getColIndexByColKey(fg, 'Descricao*')) != 0) || (fg.TextMatrix(i, getColIndexByColKey(fg, 'DescricaoComplementar*')) != 0))
            {
                fg.Cell(6, i, getColIndexByColKey(fg, 'Valor'), i, getColIndexByColKey(fg, 'Valor')) = nVermelho;
                nContadorVermelho += 1;
            }
            else if (fg.TextMatrix(i, getColIndexByColKey(fg, 'Obrigatorio*')) != 0)
            {
                fg.Cell(6, i, getColIndexByColKey(fg, 'Valor'), i, getColIndexByColKey(fg, 'Valor')) = nRosa;
                nContadorRosa += 1;
            }
        }
        else if (sValor.indexOf('?') != -1) 
        {
            fg.Cell(6, i, getColIndexByColKey(fg, 'Valor'), i, getColIndexByColKey(fg, 'Valor')) = nLaranja;
            nContadorLaranja += 1;
        }
        else if (((fg.TextMatrix(i, getColIndexByColKey(fg, 'Descricao*')) != 0) || (fg.TextMatrix(i, getColIndexByColKey(fg, 'DescricaoComplementar*')) != 0)) && 
                 (sValor.indexOf(';') == -1) || (sValor == ''))
        {
            fg.Cell(6, i, getColIndexByColKey(fg, 'Valor'), i, getColIndexByColKey(fg, 'Valor')) = nAmarelo;
            nContadorAmarelo += 1;
        }
        else if (fg.TextMatrix(i, getColIndexByColKey(fg, 'DescricaoComplementar*')) != 0) 
        {
            fg.Cell(6, i, getColIndexByColKey(fg, 'Valor'), i, getColIndexByColKey(fg, 'Valor')) = nAzul;
        }
        else if (fg.TextMatrix(i, getColIndexByColKey(fg, 'Descricao*')) != 0) {
            fg.Cell(6, i, getColIndexByColKey(fg, 'Valor'), i, getColIndexByColKey(fg, 'Valor')) = nVerde;
        }
        else 
        {
            fg.Cell(6, i, getColIndexByColKey(fg, 'Valor'), i, getColIndexByColKey(fg, 'Valor')) = nBranco;
        }

        if (i == nRow)
            break;
    }

    glb_sMensagem = "";
    
    if (nContadorAmarelo > 0)
        glb_sMensagem += "- itens em amarelo: inserir um ponto e v�rgula (;) ou preencher o campo \n";

    if (nContadorLaranja > 0)
        glb_sMensagem += "- itens em laranja: confirmar a caracter�stica \n";

    // if ((nContadorVermelho > 0) || (nContadorRosa > 0))
    //    glb_sMensagem += "- itens em vermelho: marcar OK \n";

    if (glb_sMensagem.length > 0)
        glb_sMensagem = "Este produto est� com as seguintes pend�ncias: \n" + glb_sMensagem + "\nCorrija as pend�ncias ou desligue o campo Auto.";
}

/********************************************************************
Salva as linhas alteradas no grid
********************************************************************/
function saveDataInGrid()
{
    glb_dataGridWasChanged = false;
    
    lockControlsInModalWin(true);

//    if (glb_chkDescricaoAutomaticaAntes != chkDescricaoAutomatica.checked)
//        glb_bGravaAuto = true;

    try 
    {
        /*
        var ConCaracteristicaID, Valor, Checado;
        var strPars = new String();

        for (i = 1; i < fg.Rows; i++) {
            ConCaracteristicaID = fg.TextMatrix(i, getColIndexByColKey(fg, 'ConCaracteristicaID'));
            Valor = fg.TextMatrix(i, getColIndexByColKey(fg, 'Valor'));
            Checado = fg.TextMatrix(i, getColIndexByColKey(fg, 'Checado'));
        
            strPars += (i == 1 ? '?' : '&') + 'nConCaracteristicaID=' + escape(ConCaracteristicaID);
            strPars += '&sValor=' + escape(Valor);
            strPars += '&sChecado=' + escape(Checado);
        }

        strPars += '&sAuto=' + escape( (glb_bGravaAuto ? (chkDescricaoAutomatica.checked ? '1' : '0') : '') );
        strPars += '&nProdutoID=' + escape(glb_nProdutoID); 
        strPars += '&nUsuarioID=' + escape(glb_nUserID);
        
        dsoSalvaGrid.URL = SYS_ASPURLROOT + '/pcm/conceitos/serverside/salvagrid.aspx' + strPars;
        dsoSalvaGrid.ondatasetcomplete = saveDataInGrid_DSC();
        dsoSalvaGrid.refresh();
        */
        dsoGrid.SubmitChanges();

    }
    catch(e)
    {
        // Numero de erro qdo o registro foi alterado ou removido por outro usuario
        if (e.number == -2147217887)
        {
            if ( window.top.overflyGen.Alert ('Este registro acaba de ser alterado ou removido.') == 0 )
                return null;
        }
        
        fg.Rows = 1;
        lockControlsInModalWin(false);
        
        setupBtnsFromGridState();
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null ); 
        return null;
    }    

    dsoGrid.ondatasetcomplete = saveDataInGrid_DSC;
    dsoGrid.Refresh();
}

/********************************************************************
Retorno do servidor, apos salvar as linhas alteradas no grid
********************************************************************/
function saveDataInGrid_DSC()
{
    lockControlsInModalWin(false);
}


/********************************************************************
Inclui as todas as Caracter�sticas do Produto. 
********************************************************************/
function sincronizaCaracteristicas(sDelIncl, nConceitoID) {
    lockInterface(true);
    
    var strPars = new String();

    //saveDataInGrid();

    strPars = '?nConceitoID=' + escape(nConceitoID);
    strPars += '&sDelIncl=' + escape(sDelIncl);
    dsoExecSincroniza.URL = SYS_ASPURLROOT + '/pcm/conceitos/serverside/sincroniza.aspx' + strPars;
    dsoExecSincroniza.ondatasetcomplete = sincronizaCaracteristicas_DSC(sDelIncl);
    dsoExecSincroniza.refresh();
}

/********************************************************************
Retorno da funcao sincronizaCaracter�sticas. 
********************************************************************/
function sincronizaCaracteristicas_DSC(sDelIncl) {
    //destrava a Interface
    lockInterface(false);

    //if (sDelIncl == 'I') {
        //espera para atualizar grid da modal
        glb_rewriteInterval = window.setInterval('rewriteInterval()', 500, 'JavaScript');
    //}
}

/********************************************************************
Depois do tempo de intervalo roda a fun��o desejada
********************************************************************/
function rewriteInterval() {
    if (glb_rewriteInterval != null) {
        window.clearInterval(glb_rewriteInterval);
        glb_rewriteInterval = null;
    }

    // atualiza grid da modal
    fillGridData();
    
}

/********************************************************************
Inclui as todas as Caracter�sticas do Produto. 
********************************************************************/
function replicaCaracteristicas(nProdutoID) {
    lockInterface(true);

    var strPars = new String();

    strPars = '?nProdutoID=' + escape(nProdutoID);
    //strPars += '&sDelIncl=' + escape(sDelIncl);
    dsoReplicaCaracteristicas.URL = SYS_ASPURLROOT + '/pcm/conceitos/serverside/replicacaracteristicas.aspx' + strPars;
    dsoReplicaCaracteristicas.ondatasetcomplete = replicaCaracteristicas_DSC();
    dsoReplicaCaracteristicas.refresh();
}

/********************************************************************
Retorno da funcao sincronizaCaracter�sticas. 
********************************************************************/
function replicaCaracteristicas_DSC() {
    //destrava a Interface
    lockInterface(false);
    
}

/********************************************************************
Grava DescricaoAutomatica do sup
********************************************************************/
function gravaDescricaoAutomatica() {
    lockInterface(true);

    var ConCaracteristicaID, Valor, Checado;
    var strPars = new String();

    strPars += '?sAuto=' + escape((chkDescricaoAutomatica.checked ? '1' : '0'));
    strPars += '&nProdutoID=' + escape(glb_nProdutoID);
    strPars += '&nUsuarioID=' + escape(glb_nUserID);

    dsoSalvaGrid.URL = SYS_ASPURLROOT + '/pcm/conceitos/serverside/salvagrid.aspx' + strPars;
    dsoSalvaGrid.ondatasetcomplete = gravaDescricaoAutomatica_DSC();
    dsoSalvaGrid.refresh();
}

/********************************************************************
Retorno da funcao gravaDescricaoAutomatica
********************************************************************/
function gravaDescricaoAutomatica_DSC() {
    //destrava a Interface
    lockInterface(false);
}


/********************************************************************
Busca valor do campo DescricaoAutomatica
********************************************************************/
function verificaDescricaoAutomatica() {

    setConnection(dsoDescricaoAutomatica);

    dsoDescricaoAutomatica.SQL = 'SELECT a.DescricaoAutomatica, b.PermiteAlterarDescricaoAutomatica, b.Replicar ' +
                                    'FROM Conceitos a WITH(NOLOCK) ' +
                                    'INNER JOIN Conceitos b WITH(NOLOCK) ON (b.ConceitoID = a.ProdutoID) ' +
                                    'WHERE a.ConceitoID = ' + glb_nProdutoID;

    dsoDescricaoAutomatica.ondatasetcomplete = verificaDescricaoAutomatica_DSC;
    dsoDescricaoAutomatica.Refresh();
}

/********************************************************************
Retorno da funcao verificaDescricaoAutomatica
********************************************************************/
function verificaDescricaoAutomatica_DSC() {
    var bDescricaoAutomatica, bPermiteAlterarDescricaoAutomatica;

    dsoDescricaoAutomatica.recordset.MoveFirst();
    bDescricaoAutomatica = dsoDescricaoAutomatica.recordset['DescricaoAutomatica'].value;
    bPermiteAlterarDescricaoAutomatica = dsoDescricaoAutomatica.recordset['PermiteAlterarDescricaoAutomatica'].value;
    bReplicar = dsoDescricaoAutomatica.recordset['Replicar'].value;

    chkDescricaoAutomatica.checked = (bDescricaoAutomatica == 1 ? true : false);

    if (bPermiteAlterarDescricaoAutomatica == 1)
    {
        chkDescricaoAutomatica.disabled = false;

        // Verifica se usuario logado tem direito de Inclus�o no bot�o clicado
        // Assistente de Produtos tem direitos, Assistente de Marketing n�o tem direitos
        glb_nB4I = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrRightValue(\'INF\',\'B4I\')');

        if (glb_nB4I == 0)
            chkDescricaoAutomatica.disabled = true;
    }
    else
    {
        chkDescricaoAutomatica.checked = bReplicar;
        chkDescricaoAutomatica.disabled = true;
    }

    if (glb_verificaDescricao != null) {
        window.clearInterval(glb_verificaDescricao);
        glb_verificaDescricao = null;
    }
}

/********************************************************************
Pinta o Grid dinamicamente
********************************************************************/
function js_fg_AfterRowColChange(fg, OldRow, OldCol, NewRow, NewCol) {
    pintaGrid(null);

    if ((OldRow != NewRow) && ((selValor.options.length) > 0))
        clearComboEx(['selValor']);
}

// EVENTOS DE GRID **************************************************

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalcaracteristicasBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
{
	;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalcaracteristicasDblClick(grid, Row, Col)
{
    var nCaracteristicaID, sCaracteristica, sTitle;

    if (getColIndexByColKey(fg, 'Valor') == Col)
    {
        nCaracteristicaID = fg.TextMatrix(Row, getColIndexByColKey(fg, 'CaracteristicaID'));
        sCaracteristica = fg.TextMatrix(Row, getColIndexByColKey(fg, 'Caracteristica'));

        //replaceStr(lblValor.innerHTML, sCaracteristica, '');

        lblValor.innerHTML = "Valores da caracter�stica " + sCaracteristica;

        glb_nRowSelected = Row;

        startDynamicCmbs(nCaracteristicaID);
    }
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalcaracteristicasKeyPress(KeyAscii)
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalcaracteristicas_ValidateEdit()
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalcaracteristicas_AfterEdit(Row, Col)
{
    var nType;
    var ColKey = '';
    
    if (fg.Editable)
    {
        // Altera o campo do dso com o novo valor
        dsoGrid.recordset.MoveFirst();
        dsoGrid.recordset.Find(fg.ColKey(getColIndexByColKey(fg, 'ConCaracteristicaID')), fg.TextMatrix(Row, getColIndexByColKey(fg, 'ConCaracteristicaID')));

        if ( !(dsoGrid.recordset.EOF) ) {
            
            ColKey = fg.ColKey(Col);
            
            if (ColKey.indexOf('*') >= 0)
                return;

            ColKey = ColKey.replace("*", "");

            nType = dsoGrid.recordset[ColKey].type;
            
			// Se decimal , numerico , int, bigint ou boolean
			if ( (nType == 131) || (nType == 14) || (nType == 11) || (nType == 3) || (nType == 20) )
			    dsoGrid.recordset[ColKey].value = fg.ValueMatrix(Row, Col);
            else
                dsoGrid.recordset[ColKey].value = trimStr(fg.TextMatrix(Row, Col));

            if (fg.TextMatrix(Row, getColIndexByColKey(fg, 'Descricao*')) != "1") {
                glb_bReplica = true;
                glb_dataGridWasChanged = true;
                //setupBtnsFromGridState();
            }

            // Se editou o grid mas perfil n�o tem direito de setar Auto, desmarca checkbox Auto
            //if ((glb_nB4I == 0) && (chkDescricaoAutomatica.checked))
            //    chkDescricaoAutomatica.checked = false;

            if (getColIndexByColKey(fg, 'Checado') == Col) 
            {
                pintaGrid(Row);
            }
        }    
    }    
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_BeforeEdit(grid, Row, Col) {

    // return null;
    var nomeColuna = grid.ColKey(Col);

    var lastPos = nomeColuna.length - 1;

    // retira o asterisco do nome do campo
    if (nomeColuna.substr(lastPos, 1) == '*') {
        nomeColuna = nomeColuna.substr(0, lastPos);
    }

    if ((dsoGrid.recordset[nomeColuna].type == 200) || (dsoGrid.recordset[nomeColuna].type == 129))
        fieldMaxLength(fg, Col, dsoGrid);
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
/*function fg_modalmodalcaracteristicasAfterRowColChange(oldRow, oldCol, newRow, newCol) {
    fg_AfterRowColChange();
}*/

// FINAL DE EVENTOS DE GRID *****************************************

function DisableBtnOk() 
{
    var nEstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['EstadoID'].value");
    var bTemKardex = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['TemKardex'].value");
    var nPermiteSuporte = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'glb_PermiteSuporte');
    var nPermiteHomologacao = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'glb_PermiteHomologacao');
    
    if ((nEstadoID == 2 || nEstadoID == 11) && /*(bTemKardex == 1) &&*/ (nPermiteSuporte == 0) && (nPermiteHomologacao == 0))
    {
        btnOK.disabled = false;
    }    

}

/********************************************************************
Ida ao servidor para o segundo combo
********************************************************************/
function startDynamicCmbs(nCaracteristicaID) {
    var sFiltro = '';
    glb_CounterCmbsDynamics2 = 1;

    var nProdutoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['ProdutoID'].value");

    setConnection(dsoValor);

    dsoValor.SQL = 'SELECT DISTINCT CaracteristicaID AS fldID, Valor AS fldName ' +
                            'FROM Conceitos a WITH(NOLOCK) ' +
                                'INNER JOIN Conceitos_Caracteristicas b WITH(NOLOCK) ON (b.ProdutoID = a.ConceitoID) ' +
                            'WHERE a.EstadoID IN (2, 11) AND a.ProdutoID = ' + nProdutoID + ' AND CaracteristicaID = '+ nCaracteristicaID + ' ' +
                            'ORDER BY fldID';

    dsoValor.ondatasetcomplete = dsoCmbDynamic_DSC;
    dsoValor.Refresh();
}

/********************************************************************
Retorno do servidor
********************************************************************/
function dsoCmbDynamic_DSC() {
    var optionStr, optionValue;
    var aCmbsDynamics = [selValor];
    var aDSOsDynamics = [dsoValor];
    var oldDataSrc;
    var oldDataFld;
    var oOption;
    var i;
    var lFirstRecord;
    var nQtdCmbs = 1;
    var nRows;

    // Inicia o carregamento de combos dinamicos (selPapelEmpresa,selValor)
    clearComboEx(['selValor']);

    var nRows = dsoValor.recordset.RecordCount();

    for (i = 0; i < nQtdCmbs; i++) {
        oldDataSrc = aCmbsDynamics[i].dataSrc;
        oldDataFld = aCmbsDynamics[i].dataFld;
        aCmbsDynamics[i].dataSrc = '';
        aCmbsDynamics[i].dataFld = '';
        lFirstRecord = true;

        while (!aDSOsDynamics[i].recordset.EOF) {
            optionStr = aDSOsDynamics[i].recordset['fldName'].value;
            optionValue = aDSOsDynamics[i].recordset['fldID'].value;
            var oOption = document.createElement("OPTION");
            oOption.text = optionStr;
            oOption.value = optionValue;
            aCmbsDynamics[i].add(oOption);
            aDSOsDynamics[i].recordset.MoveNext();
            lFirstRecord = false;
        }

        aCmbsDynamics[i].dataSrc = oldDataSrc;
        aCmbsDynamics[i].dataFld = oldDataFld;
    }

    return null;
}

function selValor_ondlbclick()
{
    var nIndex;

    nIndex = selValor.selectedIndex;

    fg.TextMatrix(glb_nRowSelected, getColIndexByColKey(fg, 'Valor')) = selValor[nIndex].text;

    js_modalcaracteristicas_AfterEdit(glb_nRowSelected, getColIndexByColKey(fg, 'Valor'));

    //fg.TextMatrix(Row, getColIndexByColKey(fg, 'Valor')) = ;
}