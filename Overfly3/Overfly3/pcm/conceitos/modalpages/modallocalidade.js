/********************************************************************
modallocalidade.js

Library javascript para o modallocalidade.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

var dsoPesq = new CDatatransport("dsoPesq");
    
// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();   
    
    // ajusta o body do html
    var elem = document.getElementById('modallocalidadeBody');

    with (elem)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
       
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
    // coloca foco no campo apropriado
    if ( document.getElementById('selUF').disabled == false )
        selUF.focus();
    else if ( document.getElementById('txtCidade').disabled == false )
        txtCidade.focus();
        
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // texto da secao01
    secText('Selecionar Localidade', 1);
    
    var stretchCtrl = 0;
    
    // Imposto Estadual --> Modifica a Interface da Janela
    if ( glb_sNivelGovID == 232)
        stretchCtrl = 20;
        
    // ajusta elementos da janela
    var elem;
    
    // ajusta o divUF
    elem = window.document.getElementById('divUF');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(document.getElementById('divMod01').style.height, 10) + ELEM_GAP;
        width = (2 + stretchCtrl + 4) * FONT_WIDTH;    
        height = 40;
        
    }
        
    // ajusta o selUF
    elem = window.document.getElementById('selUF');
    with (elem.style)
    {
        width = (2 + stretchCtrl + 4) * FONT_WIDTH;    
    }
    
    // ajusta o divCidade
    elem = window.document.getElementById('divCidade');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = parseInt(document.getElementById('divUF').style.left, 10) + parseInt(document.getElementById('divUF').style.width, 10) + ELEM_GAP;
        top = parseInt(document.getElementById('divUF').style.top, 10);
        width = (40 * FONT_WIDTH);
        height = 40;
        
    }
    
    // txtCidade
    elem = window.document.getElementById('txtCidade');
    elem.maxLength = 30;  // aceita vinte e hum caracteres de digitacao
    with (elem.style)
    {
        left = 0;
        top = 16;
        width = (20) * FONT_WIDTH;
        heigth = 24;
    }
    
    // btnFindPesquisa
    elem = window.document.getElementById('btnFindPesquisa');
    elem.disabled = true;

    with (elem.style)
    {
        top = parseInt(document.getElementById('txtCidade').style.top, 10);
        left = parseInt(document.getElementById('txtCidade').style.left, 10) + parseInt(document.getElementById('txtCidade').style.width, 10) + 2;
        width = 80;
        height = 24;
    }
    
    // ajusta o divFG
    elem = window.document.getElementById('divFG');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(document.getElementById('divUF').style.top, 10) + parseInt(document.getElementById('divUF').style.height, 10) + ELEM_GAP;
        width = 57 * FONT_WIDTH + 4;
        height = (MAX_FRAMEHEIGHT_OLD / 2) - (ELEM_GAP * 7) + 4;
    }
    
    elem = document.getElementById('fg');
    with (elem.style)
    {
        left = 0;
        top = 0;
        width = parseInt(document.getElementById('divFG').style.width, 10);
        height = parseInt(document.getElementById('divFG').style.height, 10);
    }
    
    fillCmbEstado();
    
    startGridInterface(fg);
    headerGrid(fg,['Localidade'+replicate(' ',35),
                   'LocalidadeID'], [1]);
    fg.Redraw = 2;
}

function selUF_onchange()
{
    btnOK.disabled = true;
    
    // Imposto Estadual
    if ( glb_sNivelGovID == 232)
    {
        if ( selUF.value != null )
            btnOK.disabled = false;
    }
    else if ( glb_sNivelGovID == 233 )  // Imposto municipal
    {
        startGridInterface(fg);
        headerGrid(fg,['Localidade'+replicate(' ',35),
                       'LocalidadeID'], [1]);
        fg.Redraw = 2;
    }
}

function fillCmbEstado()
{
    // 1. O Pais nao tem estados
    if ( glb_arrayEstado.length == 0 )
    {
        selUF.disabled = true;
        return;
    }

    // 2. O Pais tem estados
    var i;
    var optionStr, optionValue;

    for ( i=0; i<glb_arrayEstado.length;i++ )
    {
        // Imposto Estadual carrega nome completo do estado
        if ( glb_sNivelGovID == 232)
            optionStr = glb_arrayEstado[i][1];
        else    // Municipal carrega sigla do estado
            optionStr = glb_arrayEstado[i][2];
            
        optionValue = glb_arrayEstado[i][0];
        
        nCmb = selUF;
        addOptionsInCombos(optionStr, optionValue, nCmb);
    }
    
    // Imposto Estadual carrega nome completo do estado
    if ( glb_sNivelGovID == 232)
    {
        if ( selUF.value != null )
            btnOK.disabled = false;
    }
}

function addOptionsInCombos(optionStr, optionValue, nCmb)
{
    var oOption = document.createElement("OPTION");
    
    oOption.text = optionStr;
    oOption.value = optionValue;

    nCmb.add(oOption);
}

function txtCidade_ondigit(ctl)
{
    if ( glb_sNivelGovID == 233 )// Municipal
        changeBtnState(ctl.value);
}

function btnFindPesquisa_onclick(ctl)
{
    txtCidade.value = trimStr(txtCidade.value);
    
    changeBtnState(txtCidade.value);

    if (btnFindPesquisa.disabled)
        return;
    
    if ( (selUF.length != 0) && (selUF.selectedIndex == -1) )
    {
        if ( window.top.overflyGen.Alert ('Selecione UF.') == 0 )
            return null;
            
        selUF.focus();
        return;
    }
        
    startPesq(selUF.value, txtCidade.value);
}

function fg_DblClick()
{
    // Se tem cidade selecionada e o botao ok esta liberado,
    // chama o botao OK
    
    if ( (fg.Row >= 1 )&& (btnOK.disabled == false) )
        btn_onclick(btnOK);
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    // 1. Usuario escolheu uma cidade, deu um duplo clique na mesma
    // ou escolheu uma cidade e clicou o botao OK
    // 2. Usuario escolheu um estado e clicou o botao OK
    
    // Devolve sempre um array:
    // Elem 0: glb_sBtnClicked
    // Elem 1:
    // glb_sNivelGovID == 232 - Imposto estadual
    // ou
    // glb_sNivelGovID == 233 - Imposto municipal
    // Elem 2:
    // Nome do estado ou da cidade
    // Elem 3:
    // Sigla do estado, da cidade nao tem devolve ''
    // Elem 4:
    // ID do estado ou da cidade
    
    if (ctl.id == btnOK.id )
    {
        // loop no array de estados
        var i, UFID, UFName, UFSigla;
        
        UFID = 0;
        UFName = '';
        UFSigla = '';
        
        if ( selUF.length != 0 )
            UFID = selUF.value;
        
        for (i=0; i<glb_arrayEstado.length; i++)
        {
            if ( glb_arrayEstado[i][0] == selUF.value)
            {
                UFID = glb_arrayEstado[i][0];
                UFName = glb_arrayEstado[i][1];
                UFSigla = glb_arrayEstado[i][2];
                break;
            }
        }
        
        var aRet = new Array();

        aRet[0] = glb_sBtnClicked;        
        aRet[1] = glb_sNivelGovID;
        
        if ( glb_sNivelGovID == 232 )   // Imposto estadual
        {
            aRet[2] = UFName;
            aRet[3] = UFSigla;
            aRet[4] = UFID;
        }
        else if ( glb_sNivelGovID == 233 )   // Imposto municipal
        {
            aRet[2] = fg.TextMatrix(fg.Row, 0);
            aRet[3] = '';
            aRet[4] = fg.TextMatrix(fg.Row, 1);
        }
        else    // se falhou
        {
            sendJSMessage(getHtmlId(), JS_DATAINFORM,'CANCEL_CALLFORM_I', null );
            return true;    
        }
        
        // tudo OK
        sendJSMessage( getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_I' , aRet );
    }    
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_I', null );    
    
}

function startPesq(estadoID, strCidade)
{
    lockControlsInModalWin(true);
    
    writeInStatusBar('child', 'Listando', 'cellMode' , true);
        
    var strPas = '?';
    strPas += 'nEstado='+escape(estadoID);
    strPas += '&nPais='+escape(glb_sPaisID);
    strPas += '&strToFind='+escape(strCidade);
    
    dsoPesq.URL = SYS_ASPURLROOT + '/pcm/conceitos/serverside/pesqcidade.aspx'+strPas;
    dsoPesq.ondatasetcomplete = dsopesq_DSC;
    dsoPesq.Refresh();
}

function dsopesq_DSC()
{
    startGridInterface(fg);
    headerGrid(fg,['Localidade',
                   'LocalidadeID'], [1]);
    
    fillGridMask(fg,dsoPesq,['Localidade',
                             'LocalidadeID'],['','']);
                           
    lockControlsInModalWin(false);
    
    // destrava botao OK se tem linhas no grid
    if (fg.Rows > 1)
        btnOK.disabled = false;
    else
        btnOK.disabled = true;    
        
    writeInStatusBar('child', 'cellMode', 'Origem');    
    
}
