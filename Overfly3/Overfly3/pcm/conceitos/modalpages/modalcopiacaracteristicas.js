/********************************************************************
modalcopiacaracteristicas.js

Library javascript para o modalcopiacaracteristicas.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_aEmpresaData = null;
var glb_nUserID = 0;
var glb_CaracteristicasTimerInt = null;

// Dados do grid .RDS
var dsoGrid = new CDatatransport("dsoGrid");
// Gravacao no Servidor .URL
var dsoCopiaCaracteristica = new CDatatransport("dsoCopiaCaracteristica");
var dsoComboFiltro = new CDatatransport("dsoComboFiltro");
var glb_Carregamento = true;
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
cmb_onchange()
btn_onclick(ctl)
refreshParamsAndDataAndShowModalWin(fromServer)
getCurrEmpresaData()
setupBtnsFromGridState()
fillGridData()
fillGridData_DSC()
saveDataInGrid()
saveDataInGrid_DSC()

EVENTOS DE GRID

DEFINIDAS NO ARQUIVO modalcopiacaracteristicas.ASP
Sub fg_BeforeRowColChange(OldRow, OldCol, NewRow, NewCol, Cancel)
    Cancel = js_fg_BeforeRowColChange (fg, OldRow, OldCol, NewRow, NewCol, Cancel)
End Sub

Sub fg_AfterRowColChange(OldRow, OldCol, NewRow, NewCol)
    fg_AfterRowColChange()
End Sub

Sub fg_EnterCell()
    js_fg_EnterCell fg
End Sub
FINAL DE DEFINIDAS NO ARQUIVO modalcopiacaracteristicas.ASP

js_fg_modalcopiacaracteristicasBeforeMouseDown(fg, Button, Shift, X, Y, Cancel)
js_fg_modalcopiacaracteristicasDblClick( grid, Row, Col)
js_modalcopiacaracteristicasKeyPress(KeyAscii)
js_modalcopiacaracteristicas_AfterRowColChange
js_modalcopiacaracteristicas_ValidateEdit()
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload() 
{
    modalFrame = getFrameInHtmlTop('frameModal');
    modalFrame.style.top = 27;

    window_onload_1stPart();
    
    // ajusta o body do html
    with (modalcopiacaracteristicasBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // configuracao inicial do html
    setupPage();   

    // mostra a modal
    refreshParamsAndDataAndShowModalWin(true);
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // Costura o titulo da modal
    glb_nProdutoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'txtRegistroID.value');
    var sProdutoDescricao = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['ProdutoDescricao'].value");

    secText(('Clonar Caracter�sticas - ' + sProdutoDescricao + ' #' + glb_nProdutoID), 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var eLeft = 2;
    var eTop = 1;
    var rQuote = 0;
    var btnWidth = 78;
    
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }

    eLeft = 2;
    eTop = 2;
    rQuote = 0;

    // desabilita o botao OK
    btnOK.disabled = true;

    // troca label botao Cancelar   
    btnCanc.value = 'Cancelar';

    adjustElementsInForm([['lblBusca', 'txtBusca', 25, 1, 0, 20],
                          ['lblInternacional', 'chkInternacional', 3, 1],
                          ['lblProdutos', 'selProdutos', 70, 1, -3]]);
    
    selProdutos.onchange = selProdutos_onchange;
                          
    // ajusta o divFG
    with (divFG.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(selProdutos.currentStyle.top, 10) + 
            parseInt(selProdutos.currentStyle.height, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;    
        height = parseInt(btnOK.currentStyle.top, 10) - parseInt(top, 10) - ELEM_GAP;
    }

    with (fg.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }

    txtBusca.value = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'txtSerie.value');
    sendJSMessage('INF_HTML', JS_DATAINFORM, EXECEVAL, '__btn_REFR(' + '\'' + 'inf' + '\'' + ')');
    chkInternacional.checked = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'chkInternacional.checked');

    startGridInterface(fg);
    fg.Cols = 0;
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;    
    fg.Redraw = 2;
    
    filtraCombo();
}

function selProdutos_onchange()
{
    fillGridData();
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;
    
    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnOK')
    {
        var nProdutoToID = parseInt(selProdutos.value, 10);

        if (glb_nProdutoID == nProdutoToID) {
            if (window.top.overflyGen.Alert("Selecione um produto para clonar") == 0)
                return null;
            return null;
        }
        else if (isNaN(nProdutoToID))
            return null;
        else
            saveDataInGrid();

        //sendJSMessage('INF_HTML', JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null);
        //sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, 'SupRefr');
    }
    // codigo privado desta janela
    else
    {
        var _retMsg;
        var sMsg = '';
            
        if ( sMsg != '' )
        {
            _retMsg = window.top.overflyGen.Confirm(sMsg);
	        	    
	        if ( (_retMsg == 0) || (_retMsg == 2) )
	            return null;
        }
        
        //restoreInterfaceFromModal();
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null ); 
    }    
}

/********************************************************************
Funcao chamada pela automacao
Esta funcao so executa se a modal havia sido previamente executada,
ou seja, apos a primeira carga da modal.
Atualizar parametros e dados aqui e apos, mostrar a modal.

Parametros:
fromServer  - true, a modal esta vindo do servidor
            - null, a modal ja esta carregada

********************************************************************/
function refreshParamsAndDataAndShowModalWin(fromServer)
{
    // @@ atualizar parametros e dados aqui
    glb_aEmpresaData = getCurrEmpresaData();
    glb_nUserID = glb_USERID;
        
    // @@ atualizar interface aqui
    setupBtnsFromGridState();
    
    // mostra a janela modal
    fillGridData();
}

/********************************************************************
Trava/destrava todos os botoes
********************************************************************/
function setupBtnsFromGridState()
{
    var bHasRowsInGrid = fg.Rows > 1;
    
    btnOK.disabled = !(bHasRowsInGrid);

    DisableBtnOk();
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridData()
{
    var nProdutoID = parseInt(selProdutos.value, 10);
    var aEmpresa = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,'getCurrEmpresaData()');
    if (glb_CaracteristicasTimerInt != null)
    {
        window.clearInterval(glb_CaracteristicasTimerInt);
        glb_CaracteristicasTimerInt = null;
    }
    
    lockControlsInModalWin(true);
    
    // zera o grid
    fg.Rows = 1;
    
    // parametrizacao do dso dsoGrid
    setConnection(dsoGrid);

    nProdutoID = (isNaN(nProdutoID) ? 0 : nProdutoID);

    dsoGrid.SQL = 
        'SELECT dbo.fn_Produto_CaracteristicaOrdem(CaracteristicasProduto.ProdutoID, CaracteristicasProduto.CaracteristicaID) AS Ordem, CaracteristicasProduto.Checado, ' +
	        'dbo.fn_Tradutor(Caracteristicas.Conceito, ' + aEmpresa[7] + ', ' + aEmpresa[8] + ', NULL) AS Caracteristica, ' +
	        'ISNULL(REPLACE(REPLACE(REPLACE(CaracteristicasProduto.Valor, CHAR(10), SPACE(0)), CHAR(13), SPACE(0)), CHAR(39), SPACE(0)), SPACE(0)) AS Valor, ' +
            'dbo.fn_Produto_CaracteristicaBit(CaracteristicasProduto.ProdutoID, CaracteristicasProduto.CaracteristicaID, 1) AS FichaTecnica, ' +
            'dbo.fn_Produto_CaracteristicaBit(CaracteristicasProduto.ProdutoID, CaracteristicasProduto.CaracteristicaID, 2) AS Recebimento, ' +
            'dbo.fn_Produto_CaracteristicaBit(CaracteristicasProduto.ProdutoID, CaracteristicasProduto.CaracteristicaID, 3) AS Descricao, ' +
            'dbo.fn_Produto_CaracteristicaBit(CaracteristicasProduto.ProdutoID, CaracteristicasProduto.CaracteristicaID, 4) AS DescricaoComplementar, ' +
            'dbo.fn_Produto_CaracteristicaBit(CaracteristicasProduto.ProdutoID, CaracteristicasProduto.CaracteristicaID, 5) AS Obrigatorio, ' +
            'dbo.fn_Produto_CaracteristicaBit(CaracteristicasProduto.ProdutoID, CaracteristicasProduto.CaracteristicaID, 6) AS Imagens, ' +
            'dbo.fn_Produto_CaracteristicaBit(CaracteristicasProduto.ProdutoID, CaracteristicasProduto.CaracteristicaID, 7) AS PesosMedidas ' +
	    'FROM Conceitos_Caracteristicas CaracteristicasProduto  WITH(NOLOCK), Conceitos Caracteristicas  WITH(NOLOCK) ' +
	    'WHERE (CaracteristicasProduto.ProdutoID = ' + nProdutoID.toString() + ' AND ' +
	    	'CaracteristicasProduto.CaracteristicaID = Caracteristicas.ConceitoID) ' +
	    'ORDER BY Ordem';

    dsoGrid.ondatasetcomplete = fillGridData_DSC;
    dsoGrid.Refresh();
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData_DSC()
{
    showExtFrame(window, true);
    // restoreInterfaceFromModal(true);

    var dTFormat = '';
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';
        
    fg.Redraw = 0;
    fg.Editable = false;
    startGridInterface(fg);
    fg.FrozenCols = 0;
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;    
    
    headerGrid(fg,['Ordem',
                   'OK',
                   'Caracter�stica',
                   'Valor',
                   'FT',
                   'Rec',
                   'Desc',
                   'DC',
                   'Obrig',
                   'Img',
                   'PM'], []);
                       
    fillGridMask(fg,dsoGrid,['Ordem*',
                             'Checado*', 
                             'Caracteristica*',
                             'Valor*',
                             'FichaTecnica*',
                             'Recebimento*',
                             'Descricao*',
                             'DescricaoComplementar*',
                             'Obrigatorio*',
                             'Imagens*',
                             'PesosMedidas*'],
                             ['99', '', '', '', '', '', '', '', '', '', '']);

    glb_aCelHint = [[0, 4, 'Esta Caracter�stica ser� usada na Ficha T�cnica do produto?'],
                        [0, 5, 'Esta Caracter�stica ser� usada no Recebimento do produto?'],
                        [0, 6, 'Esta Caracter�stica ser� usada para compor a Descri��o do produto?'],
						[0, 7, 'Esta Caracter�stica ser� usada para compor a Descri��o Complementar do produto?'],
						[0, 8, 'Esta Caracter�stica � obrigatoria para essa fam�lia?'],
                        [0, 9, 'Esta Caracter�stica � relevante para replica��o de Imagens?'],
                        [0, 10, 'Esta Caracter�stica � relevante para replica��o de Pesos e Medidas?']];

    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);
    fg.FrozenCols = 3;
    fg.Redraw = 2;
    
    // Nao retirar o if abaixo, ela garante que o grid
    // so deixa editar a coluna 4
    if ( fg.Rows > 1 )
        fg.Col = 3;
    
    lockControlsInModalWin(false);

    // se nenhuma linha foi selecionada, seleciona a primeira
    // se a mesma existe
    if ( (fg.Row < 1) && (fg.Rows > 1) )
        fg.Row = 1;
        
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;    
    
    fg.Redraw = 2;

    // se tem linhas no grid, coloca foco no grid
    //if (fg.Rows > 1)
    //{
    //    window.focus();
    //    fg.focus();
    //}                
    //else
    //{
    //    ;
    //}            

    selProdutos.focus();

    // ajusta estado dos botoes
    setupBtnsFromGridState();
}

/********************************************************************
Salva as linhas alteradas no grid
********************************************************************/
function saveDataInGrid()
{
    lockControlsInModalWin(true);

    var strPars;
    var nProdutoToID = parseInt(selProdutos.value, 10);

    strPars = '?nProdutoFromID=' + escape(nProdutoToID);
    strPars += '&nProdutoToID=' + escape(glb_nProdutoID);
    
    dsoCopiaCaracteristica.URL = SYS_ASPURLROOT + '/pcm/conceitos/serverside/copiacaracteristicas.aspx' + strPars;
    dsoCopiaCaracteristica.ondatasetcomplete = saveDataInGrid_DSC;
    dsoCopiaCaracteristica.Refresh();
}

/********************************************************************
Retorno do servidor, apos salvar as linhas alteradas no grid
********************************************************************/
function saveDataInGrid_DSC()
{
    lockControlsInModalWin(false);
    
    var sMensagemErro = (dsoCopiaCaracteristica.recordset.Fields['fldresp'].value == null ? '' : dsoCopiaCaracteristica.recordset.Fields['fldresp'].value);

    if (sMensagemErro != '') {
        if (window.top.overflyGen.Alert(sMensagemErro) == 0)
            return null;
    }
    else {
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null);
        sendJSMessage('INF_HTML', JS_DATAINFORM, EXECEVAL, 'openModalCaracteristica();');
    }
}

// EVENTOS DE GRID **************************************************

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalcopiacaracteristicasBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
{
	;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalcopiacaracteristicasDblClick(grid, Row, Col)
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalcopiacaracteristicasKeyPress(KeyAscii)
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalcopiacaracteristicas_ValidateEdit()
{
    ;
}

// FINAL DE EVENTOS DE GRID *****************************************

function txtBusca_ondigit(ctl){
    if (event.keyCode == 13)
        filtraCombo();
    return null;    
}

function chkInternacional_onclick() {
    filtraCombo();
}

function filtraCombo() {
    //lockControlsInModalWin(true);

    var strPars;
    var sFiltro = txtBusca.value;
    var bInternacional = ((chkInternacional.checked) ? ("1") : ("0"));
    var nFamiliaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['ProdutoID'].value");
    var nProdutoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['ConceitoID'].value");

    setConnection(dsoComboFiltro);

    dsoComboFiltro.SQL =
        "SELECT 1 AS Ordem, a.ConceitoID, " +
            "dbo.fn_Produto_Descricao2(a.ConceitoID,NULL,12) + '  #' + CONVERT(VARCHAR(10), a.ConceitoID) + (CASE WHEN a.Internacional = 1 THEN ' (Int)' ELSE '' END) AS ProdutoDescricao " +
            "FROM Conceitos a WITH(NOLOCK) " +
            "WHERE a.ConceitoID=" + nProdutoID + " " +
        "UNION " +
        "SELECT 2 AS Ordem, a.ConceitoID, dbo.fn_Produto_Descricao2(a.ConceitoID,NULL,12) + '  #' + CONVERT(VARCHAR(10), a.ConceitoID) + (CASE WHEN a.Internacional = 1 THEN ' (Int)' ELSE '' END) AS ProdutoDescricao " +
            "FROM Conceitos a WITH(NOLOCK) " +
            //"INNER JOIN (SELECT TOP 1 Serie FROM Conceitos WITH(NOLOCK) WHERE ConceitoID = " + nProdutoID + ") ProdutoOrigem ON (ProdutoOrigem.Serie <> a.Serie) " +
            "WHERE (a.ProdutoID = " + nFamiliaID + " AND a.EstadoID IN (2,4) AND a.DescricaoAutomatica = 1 AND a.ConceitoID <> " + nProdutoID +
            "AND dbo.fn_Produto_Conformidade(a.ConceitoID, 1) = 100 " +
            "AND (SELECT COUNT(*) FROM Conceitos_Caracteristicas WITH(NOLOCK) WHERE (a.ConceitoID = ProdutoID)) > 0) " +
                    ((sFiltro == "") ? "" : "AND dbo.fn_Produto_Descricao2(a.ConceitoID,NULL,12) + '  #' + CONVERT(VARCHAR(10), a.ConceitoID) LIKE '%" + sFiltro + "%' ") +
            "AND a.Internacional = " + bInternacional +
        " ORDER BY Ordem, ProdutoDescricao, a.ConceitoID";

    dsoComboFiltro.ondatasetcomplete = filtraCombo_DSC;
    dsoComboFiltro.Refresh();
}

function filtraCombo_DSC() {
    lockControlsInModalWin(false);
    var sProdutoDescricao, conceitoID;

    dsoComboFiltro.recordset.MoveFirst();
    clearComboEx(['selProdutos']);

    while (!dsoComboFiltro.recordset.EOF) 
    {
        sProdutoDescricao = dsoComboFiltro.recordset['ProdutoDescricao'].value;
        conceitoID = dsoComboFiltro.recordset['ConceitoID'].value;

        var oOption = document.createElement("OPTION");
        oOption.text = sProdutoDescricao;
        oOption.value = conceitoID;
        selProdutos.add(oOption);
        
        dsoComboFiltro.recordset.MoveNext();
    }

    selProdutos.disabled = (selProdutos.options.length == 0);
    selProdutos.selectedIndex = (selProdutos.options.length > 0 ? 0 : -1);
    
    if (glb_Carregamento)
    {
        glb_Carregamento = false;
        fillGridData();
    }
}

function DisableBtnOk() {
    var nEstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['EstadoID'].value");
    var bTemKardex = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['TemKardex'].value");
    var nPermiteSuporte = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'glb_PermiteSuporte');
    var nPermiteHomologacao = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'glb_PermiteHomologacao');

    if ((nEstadoID == 2 || nEstadoID == 11) && /*(bTemKardex == 1) &&*/ (nPermiteSuporte == 0) && (nPermiteHomologacao == 0))
    {
        btnOK.disabled = false;
    }

}
