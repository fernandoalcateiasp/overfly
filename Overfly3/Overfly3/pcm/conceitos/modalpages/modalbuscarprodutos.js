/********************************************************************
modalbuscarprodutos.js

Library javascript para o modalbuscarprodutos.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var dsoPesq = new CDatatransport("dsoPesq");
var dsoGrava = new CDatatransport("dsoGrava");
var dsoperfilUsuario = new CDatatransport("dsoperfilUsuario");

var glb_aSendDataToServer = new Array();
var glb_nPointToaSendDataToServer = 0;
var glb_nTimerSendDataToServer = null;
var glb_nMaxStringSize = 1024 * 1;
var glb_DescricaoAutomatica;
var glb_sTxtFiltro;
var glb_nCurrUserID;
var glb_empresa;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload() {

    glb_nCurrUserID = getCurrUserID();

    glb_empresa = getCurrEmpresaData();

    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();

    // ajusta o body do html
    var elem = document.getElementById('modalbuscarprodutosHtmlBody');

    with (elem) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    modalFrame = getFrameInHtmlTop('frameModal');
    modalFrame.style.top = 27;
    //modalFrame.style.left = 0;

    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);

    // coloca foco no campo apropriado
    if (document.getElementById('txtProdutos').disabled == false)
        txtProdutos.focus();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() {
    // texto da secao01
    secText('Buscar Produtos', 1);

    // ajusta elementos da janela
    var elem;
    var temp;
    glb_sTxtFiltro = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'txtFiltro.value');

    elem = window.document.getElementById('divProdutos');
    with (elem.style) {
        border = 'none';
        backgroundColor = 'transparent';
        // left = parseInt(document.getElementById('divUF').style.left) + parseInt(document.getElementById('divUF').style.width) + ELEM_GAP;
        left = ELEM_GAP;
        // top = parseInt(document.getElementById('divUF').style.top);
        top = parseInt(document.getElementById('divMod01').style.height) + ELEM_GAP;
        width = (30 * FONT_WIDTH) + 24 + 20 + 42;
        temp = parseInt(width);
        height = 40;
    }

    // txtProdutos
    elem = window.document.getElementById('txtProdutos');
    //elem.maxLength = 30;
    with (elem.style) {
        left = 0;
        top = 16;
        width = 753;
        heigth = 24;

        txtProdutos.setAttribute('thePrecision', 10, 1);
        txtProdutos.setAttribute('theScale', 0, 1);
        txtProdutos.setAttribute('verifyNumPaste', 1);
        txtProdutos.setAttribute('minMax', new Array(1, 9999999999), 1);
    }

    // btnFindPesquisa
    elem = window.document.getElementById('btnFindPesquisa');

    if (txtProdutos.value != '')
        btnFindPesquisa.disabled = false;
    else
        btnFindPesquisa.disabled = true;
    
    with (elem.style) {
        top = parseInt(document.getElementById('txtProdutos').style.top);
        left = parseInt(document.getElementById('txtProdutos').style.left) + parseInt(document.getElementById('txtProdutos').style.width) + 30;
        width = 80;
        height = 24;
    }
    
    // btnSalvar
    elem = window.document.getElementById('btnSalvar');
    btnSalvar.disabled = true;
    with (elem.style) {
        top = 16;
        left = parseInt(document.getElementById('btnFindPesquisa').style.left) + parseInt(document.getElementById('btnFindPesquisa').style.width) + 8;
        width = 80;
        height = 24;
    }

    // chkFiltro
    elem = window.document.getElementById('lblFiltro');
    with (elem.style) {
        top = 0;
        left = 758;
    }
    elem = window.document.getElementById('chkFiltro');
    with (elem.style) {
        top = 15;
        left = 752;
        width = 30;
    }

    chkFiltro.checked = true;

    // ajusta o divFG
    elem = window.document.getElementById('divFG');
    with (elem.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(document.getElementById('divProdutos').style.top) + parseInt(document.getElementById('divProdutos').style.height) + ELEM_GAP;
        width = 955;
        height = 472;
    }

    elem = document.getElementById('fg');
    with (elem.style) {
        left = 0;
        top = 0;
        width = parseInt(document.getElementById('divFG').style.width);
        height = parseInt(document.getElementById('divFG').style.height);
    }

    btnOK.style.visibility = 'hidden';
    btnCanc.style.visibility = 'hidden';

    startGridInterface(fg);
    headerGrid(fg, ['ID',
                    'Est',
                    'Produto',
                    'Marca',
                    'Linha',
                    'Modelo',
                    'Part Number',
                    'S�rie',
                    'Auto',
                    'Int',
                    'Descri��o',
                    'Descri��o Complementar',
                    'Descri��o Abreviada',
                    'BNDES',
                    'GP',
                    'Especialista',
                    'OK',
                    'TemKardex'], [16]);

    //fg.ColWidth(getColIndexByColKey(fg, 'NotaFiscalID')) = 1430;
    fg.Redraw = 2;

    perfilUsuario();

    var sArgumento = '';
    sArgumento = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'txtArgumento.value');
    
    if (sArgumento != '')
        txtProdutos.value = sArgumento;
    else if(glb_sUltimaPesquisa != '')
        txtProdutos.value = glb_sUltimaPesquisa;

    configuraBtnFindPesquisa();

    if (!btnFindPesquisa.disabled)
        startPesq(txtProdutos.value);
}

function txtProdutos_ondigit(ctl) {
    configuraBtnFindPesquisa();
}

function chkFiltro_onclick() {
    configuraBtnFindPesquisa();
}

function configuraBtnFindPesquisa() {
    if ((trimStr(txtProdutos.value) != '') || ((chkFiltro.checked) && (glb_sTxtFiltro != '')))
    {
        btnFindPesquisa.disabled = false;

        if (event.keyCode == 13)
            btn_onclick(btnFindPesquisa);
    }
    else
        btnFindPesquisa.disabled = true;
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl) {
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)

    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id)
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null);
    else if (ctl.id == btnCanc.id)
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null);
    else if (ctl.id == 'btnFindPesquisa') {
        txtProdutos.value = trimStr(txtProdutos.value);
        startPesq(txtProdutos.value);
    }
    else if (ctl.id == 'btnSalvar')
        saveDataInGrid();
}

function startPesq(strPesquisa) {
    lockControlsInModalWin(true);
    btnFindPesquisa.disabled = true;
    setConnection(dsoPesq);

    // Salva ultima pesquisa para caso abra o form novamente
    sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, "glb_sUltimaPesquisa = '" + strPesquisa + "'");
    
    var sFiltro = '', nFiltroID = '', nProprietarioID;

    if (chkFiltro.checked)
    {
        sFiltro = glb_sTxtFiltro;

        nFiltroID = getCmbCurrDataInControlBar('sup', 2)[1];

        nProprietarioID = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'selProprietariosPL.value');

        if (nProprietarioID != 0)
            sFiltro += ((sFiltro != '') ? ' AND ' : '') + '(a.ProprietarioID = ' + nProprietarioID + ') ';
    }

    var strPars = new String();
    strPars = '?sBusca=' + escape(strPesquisa);
    strPars += '&sFiltro=' + escape(sFiltro);
    strPars += '&nFiltroID=' + escape(nFiltroID);

    dsoPesq.URL = SYS_ASPURLROOT + '/pcm/conceitos/serverside/buscarprodudos.aspx' + strPars;
    dsoPesq.ondatasetcomplete = dsopesq_DSC;
    dsoPesq.refresh();
}

function dsopesq_DSC() {
    startGridInterface(fg);
    fg.FrozenCols = 0;

    headerGrid(fg, ['ID',
                    'Est',
                    'Produto',
                    'Marca',
                    'Linha',
                    'Modelo',
                    'Part Number',
                    'S�rie',
                    'Auto',
                    'Int',
                    'Descri��o',
                    'Descri��o Complementar',
                    'Descri��o Abreviada',
                    'BNDES',
                    'GP',
                    'Especialista',
                    'OK',
                    'TemKardex'], [16]);

    fillGridMask(fg, dsoPesq, ['ProdutoID*',
                               'Estado*',
                               'Familia*',
                               'Marca*',
                               'Linha*',
                               'Modelo',
                               'PartNumber',
                               'Serie',
                               'DescricaoAutomatica*',
                               'Internacional',
                               'Descricao',
                               'DescricaoComplementar',
                               'DescricaoAbreviada',
                               'CodigoBNDES',
                               'GP*',
                               'Esp*',
                               'OK',
                               'TemKardex*'], ['', '', '', '', '', '', '', '', '', '', '', '', '','', '', '', '', '']);

    gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[0, '######', 'C']]);

    glb_aCelHint = [[0, 8, 'Compor Descri��o e Descri��o complementar automaticamente?'],
                    [0, 9, 'Produto Internacional?']];

    if (fg.Rows > 1) {
        var i, nProdutos = 0;

        var nColDescricaoAutomatica = getColIndexByColKey(fg, 'DescricaoAutomatica*');
        var nColDescricao = getColIndexByColKey(fg, 'Descricao');
        var nColDescricaoComplementar = getColIndexByColKey(fg, 'DescricaoComplementar');

        for (i = 2; i < fg.Rows; i++) {
            if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'ProdutoID*')) != 0) {
                nProdutos += 1;
            }

            // Pinta celula
            if ((fg.ValueMatrix(i, getColIndexByColKey(fg, 'TemKardex*')) != 0) && !(glb_PermiteSuporte || glb_PermiteHomologacao) && (glb_USERID != 1069))
            {
                fg.Cell(6, i, getColIndexByColKey(fg, 'Modelo'), i, getColIndexByColKey(fg, 'Modelo')) = 0XDCDCDC;
                fg.Cell(6, i, getColIndexByColKey(fg, 'PartNumber'), i, getColIndexByColKey(fg, 'PartNumber')) = 0XDCDCDC;
                fg.Cell(6, i, getColIndexByColKey(fg, 'Serie'), i, getColIndexByColKey(fg, 'Serie')) = 0XDCDCDC;
                fg.Cell(6, i, getColIndexByColKey(fg, 'Internacional'), i, getColIndexByColKey(fg, 'Internacional')) = 0XDCDCDC;
                fg.Cell(6, i, getColIndexByColKey(fg, 'Descricao'), i, getColIndexByColKey(fg, 'Descricao')) = 0XDCDCDC;
                fg.Cell(6, i, getColIndexByColKey(fg, 'DescricaoComplementar'), i, getColIndexByColKey(fg, 'DescricaoComplementar')) = 0XDCDCDC;
                fg.Cell(6, i, getColIndexByColKey(fg, 'DescricaoAbreviada'), i, getColIndexByColKey(fg, 'DescricaoComplementar')) = 0XDCDCDC;
                fg.Cell(6, i, getColIndexByColKey(fg, 'CodigoBNDES'), i, getColIndexByColKey(fg, 'CodigoBNDES')) = 0XDCDCDC;
            }
            else if (fg.ValueMatrix(i, nColDescricaoAutomatica) != 0)
            {
                fg.Cell(6, i, nColDescricao, i, nColDescricao) = 0XDCDCDC;
                fg.Cell(6, i, nColDescricaoComplementar, i, nColDescricaoComplementar) = 0XDCDCDC;
            }
        }

        if (nProdutos > 0)
            fg.TextMatrix(1, getColIndexByColKey(fg, 'ProdutoID*')) = nProdutos;
    }

    btnFindPesquisa.disabled = false;
    alignColsInGrid(fg, [0]);

    fg.FrozenCols = 1;
    fg.FontSize = '8';
    fg.ExplorerBar = 5;
    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1);
    fg.Redraw = 2;
    
    lockControlsInModalWin(false);

    window.focus();

    // destrava botao Salvar
    if (fg.Rows > 1) {
        btnSalvar.disabled = false;
        fg.Editable = true;
        fg.focus();
    }
    else {
        btnSalvar.disabled = true;
        txtProdutos.focus();
    }
    
    txtProdutos.focus();
}

function saveDataInGrid() {
    var strPars = new String();
    var i = 0;
    var nDataLen = 0;
    var nBytesAcum = 0;

    glb_aSendDataToServer = [];
    glb_nPointToaSendDataToServer = 0;
    glb_sResultado = '';

    lockControlsInModalWin(true);

    for (i = 2; i < fg.Rows; i++) {
        if (getCellValueByColKey(fg, 'OK', i) != 0) {
            nBytesAcum = strPars.length;

            if ((nBytesAcum >= glb_nMaxStringSize) || (strPars == '')) {
                nBytesAcum = 0;
                if (strPars != '') {
                    glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars + '&nDataLen=' + escape(nDataLen);
                    strPars = '';
                    nDataLen = 0;
                }

                strPars += '?nUsuarioID=' + escape(glb_nCurrUserID);
            }

            //var auto = getCellValueByColKey(fg, 'DescricaoAutomatica*', i);
            var internacional = getCellValueByColKey(fg, 'Internacional', i);

            nDataLen++;
            strPars += '&nID=' + escape(getCellValueByColKey(fg, 'ProdutoID*', i));
            strPars += '&nFormID=' + escape(2110); //Form Conceitos
            strPars += '&nSubFormID=' + escape(21000); //SFS Grupo-Conceito
            strPars += '&sModelo=' + getCellValueByColKey(fg, 'Modelo', i);
            strPars += '&sPartNumber=' + getCellValueByColKey(fg, 'PartNumber', i);
            strPars += '&sSerie=' + getCellValueByColKey(fg, 'Serie', i);
            //strPars += '&sDescricaoAutomatica=' + ((auto == "-1" || auto == "1") ? "1" : "0");
            strPars += '&sInternacional=' + ((internacional == "-1" || internacional == "1") ? "1" : "0");
            strPars += '&sDescricao=' + getCellValueByColKey(fg, 'Descricao', i);
            strPars += '&sDescricaoComplementar=' + getCellValueByColKey(fg, 'DescricaoComplementar', i);
            strPars += '&sDescricaoAbreviada=' + getCellValueByColKey(fg, 'DescricaoAbreviada', i);
            strPars += '&sBNDES=' + getCellValueByColKey(fg, 'CodigoBNDES', i);
        }
    }

    if (nDataLen > 0)
        glb_aSendDataToServer[glb_aSendDataToServer.length] = replaceStr(strPars, '+', '%2B') + '&nDataLen=' + escape(nDataLen);

    sendDataToServer();
}

function sendDataToServer() {
    if (glb_nTimerSendDataToServer != null) {
        window.clearInterval(glb_nTimerSendDataToServer);
        glb_nTimerSendDataToServer = null;
    }

    try {
        if (glb_nPointToaSendDataToServer < glb_aSendDataToServer.length) {
            dsoGrava.URL = SYS_ASPURLROOT + '/pcm/conceitos/serverside/salvarproduto.aspx' + glb_aSendDataToServer[glb_nPointToaSendDataToServer];
            dsoGrava.ondatasetcomplete = sendDataToServer_DSC;
            dsoGrava.refresh();
        }
        else {
            lockControlsInModalWin(false);
            startPesq(txtProdutos.value);
        }
    }
    catch (e) {
        if (window.top.overflyGen.Alert('Opera��o n�o autorizada') == 0)
            return null;

        lockControlsInModalWin(false);
        return null;
    }
}

function sendDataToServer_DSC() {
    if (!(dsoGrava.recordset.BOF && dsoGrava.recordset.EOF)) {
        if (dsoGrava.recordset['fldresp'].value != 1)
            glb_sResultado = dsoGrava.recordset['fldresp'].value;
    }

    glb_nPointToaSendDataToServer++;
    glb_nTimerSendDataToServer = window.setInterval('sendDataToServer()', INTERVAL_BATCH_SAVE, 'JavaScript');
}

function js_fg_modalbuscarprodutosDblClick(grid, Row, Col) {

    if (grid.Rows <= 1)
        return true;

    // trap so colunas editaveis
    if ((Col != getColIndexByColKey(grid, 'Ok')))
        return true;

    var i;
    var bFill = true;

    fg.Editable = false;
    fg.Redraw = 0;
    lockControlsInModalWin(true);


    // limpa coluna se tem um check box checado
    for (i = 1; i < grid.Rows; i++) {
        if (grid.ValueMatrix(i, Col) != 0) {
            bFill = false;
            break;
        }
    }

    for (i = 2; i < grid.Rows; i++) {
        if (bFill)
            grid.TextMatrix(i, Col) = 1;
        else
            grid.TextMatrix(i, Col) = 0;
    }

    lockControlsInModalWin(false);
    fg.Editable = true;
    fg.Redraw = 2;
    window.focus();
    fg.focus();
}

function js_fg_modalbuscarprodutos_BeforeEdit(grid, nRow, nCol) {

    // return null;
    var nomeColuna = grid.ColKey(nCol);

    var lastPos = nomeColuna.length - 1;

    // retira o asterisco do nome do campo
    if (nomeColuna.substr(lastPos, 1) == '*') {
        nomeColuna = nomeColuna.substr(0, lastPos);
    }

    if ((dsoPesq.recordset[nomeColuna].type == 200) || (dsoPesq.recordset[nomeColuna].type == 129))
        fieldMaxLength(fg, nCol, dsoPesq);
}
/*
function js_fg_modalbuscarprodutosAfterEdit(fg, nRow, nCol) {
    if (getColIndexByColKey(fg, 'Ok') != nCol)
        fg.TextMatrix(nRow, getColIndexByColKey(fg, 'Ok')) = 1;
}*/

function js_modalbuscarprodutos_BeforeRowColChange(grid, OldRow, OldCol, NewRow, NewCol, Cancel) {
    //Forca celula read-only
    if (!glb_GridIsBuilding)
    {
        if (glb_FreezeRolColChangeEvents)
            return true;

        var nColMarca = getColIndexByColKey(fg, 'Marca');
        var nColModelo = getColIndexByColKey(fg, 'Modelo');
        var nColPartNumber = getColIndexByColKey(fg, 'PartNumber');
        var nColSerie = getColIndexByColKey(fg, 'Serie');
        var nColInternacional = getColIndexByColKey(fg, 'Internacional');
        var nColDescricaoAutomatica = getColIndexByColKey(fg, 'DescricaoAutomatica*');
        var nColDescricao = getColIndexByColKey(fg, 'Descricao');
        var nColDescricaoAbreviada = getColIndexByColKey(fg, 'DescricaoAbreviada');
        var nColBNDES = getColIndexByColKey(fg, 'CodigoBNDES');
        var nColDescricaoComplementar = getColIndexByColKey(fg, 'DescricaoComplementar');

        if ((fg.ValueMatrix(NewRow, getColIndexByColKey(fg, 'TemKardex*')) != 0) && !(glb_PermiteSuporte || glb_PermiteHomologacao) && (glb_USERID != 1069) &&
                ((NewCol == nColModelo) || (NewCol == nColPartNumber) || (NewCol == nColSerie) || (NewCol == nColInternacional) || (NewCol == nColDescricao) ||
                 (NewCol == nColDescricaoAbreviada) || (NewCol == nColDescricaoComplementar) || (NewCol == nColBNDES)))
        {
            glb_validRow = OldRow;
            glb_validCol = OldCol;
        }
    
        else if ((fg.ValueMatrix(NewRow, nColDescricaoAutomatica) != 0) &&
             ((NewCol == nColDescricao) /*|| (NewCol == nColDescricaoAbreviada)*/ || (NewCol == nColDescricaoComplementar)))
        {
            glb_validRow = OldRow;
            glb_validCol = OldCol;
        }
        else
            js_fg_BeforeRowColChange(grid, OldRow, OldCol, NewRow, NewCol, Cancel);

    }
}
/*
function js_fg_modalbuscarprodutos_AfterRowColChange(fg, OldRow, OldCol, NewRow, NewCol)
{
    ;   
}
*/
function js_modalbuscarprodutos_ValidateEdit(grid, nRow, nCol) {
    if (getColIndexByColKey(fg, 'Ok') != nCol && getColIndexByColKey(fg, 'DescricaoAutomatica*') != nCol)
        fg.TextMatrix(nRow, getColIndexByColKey(fg, 'Ok')) = 1;
}

//Apenas perfil de desenvolvedor, administrador e homologa��o poder�o alterar alguns dados do coneito em A.
function perfilUsuario() {
    setConnection(dsoperfilUsuario);

    dsoperfilUsuario.SQL = 'SELECT COUNT(DISTINCT a.PerfilID) AS PermiteSuporte, ' +
                                    '(SELECT COUNT(DISTINCT aa.PerfilID) ' +
							            'FROM RelacoesPesRec_Perfis aa WITH(NOLOCK) ' +
									        'INNER JOIN RelacoesPesRec bb WITH(NOLOCK) ON (bb.RelacaoID = aa.RelacaoID) ' +
		                                'WHERE (bb.objetoID = 999 AND bb.EstadoID = 2 AND  bb.SujeitoID =  ' + glb_nCurrUserID + ' AND aa.PerfilID IN (525, 619)))  AS PermiteHomologacao ' +
	                            'FROM RelacoesPesRec_Perfis a WITH(NOLOCK) ' +
		                            'INNER JOIN RelacoesPesRec b ON (b.RelacaoID = a.RelacaoID) ' +
	                            'WHERE (b.objetoID = 999 AND b.EstadoID = 2 AND b.SujeitoID =  ' + glb_nCurrUserID + '  AND a.PerfilID IN (500, 501)) ';

    dsoperfilUsuario.ondatasetcomplete = perfilUsuario_DSC;
    dsoperfilUsuario.Refresh();
}

function perfilUsuario_DSC() {
    glb_PermiteSuporte = dsoperfilUsuario.recordset['PermiteSuporte'].value;
    glb_PermiteHomologacao = dsoperfilUsuario.recordset['PermiteHomologacao'].value;

    return null;
}