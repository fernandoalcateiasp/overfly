/********************************************************************
modallocalidadeimpostos.js

Library javascript para o modallocalidadeimpostos.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

var dsoPais = new CDatatransport("dsoPais");
var dsoUF = new CDatatransport("dsoUF");
var dsoMunicipio = new CDatatransport("dsoMunicipio");
    
// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();   
    
    // ajusta o body do html
    var elem = document.getElementById('modallocalidadeimpostosBody');

    with (elem)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
       
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // texto da secao01
    secText('Selecionar Localidade', 1);
    
    var stretchCtrl = 0;
    
    // ajusta elementos da janela
    var elem;
    
    // ajusta o divUF
    elem = window.document.getElementById('divGeneral');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(document.getElementById('divMod01').style.height, 10) + ELEM_GAP;
        width = (2 + stretchCtrl + 4) * FONT_WIDTH;    
        height = 40;
    }

    lockControlsInModalWin(true);
    
    adjustElementsInForm([
	            ['lblPais', 'selPais', 10, 1, -7],
	            ['lblUF', 'selUF', 30, 1, -2],
	            ['lblMunicipio', 'selMunicipio', 30, 1, -4]], null, null, true);


    var nNextLeft = (lblMunicipio.offsetLeft + lblMunicipio.offsetWidth + 184);
    
    btnOK.style.visibility = 'inherit';
    btnOK.style.top = selMunicipio.offsetTop + 33;
    btnOK.style.left = nNextLeft;
    nNextLeft = btnOK.offsetLeft + btnOK.offsetWidth + 5;

    btnCanc.style.visibility = 'hidden';
    btnCanc.style.top = selMunicipio.offsetTop;
    btnCanc.style.left = nNextLeft;

    selPais.onchange = selPais_onChange;
    selUF.onchange = selUF_onChange;

    preenchePais();    
}

function preenchePais()
{
    setConnection(dsoPais);

    dsoPais.SQL = 'SELECT 0 AS LocalidadeID, \'\' AS Localidade ' +
                  'UNION ALL ' +
                  'SELECT LocalidadeID, Localidade ' +
	                'FROM Localidades a WITH(NOLOCK) ' +
	                'WHERE ((TipoLocalidadeID = 203) AND (LocalidadeID IN (130,167)) AND (EstadoID = 2))';

    dsoPais.ondatasetcomplete = preenchePais_DSC;
    dsoPais.Refresh();
}

function preenchePais_DSC() 
{
    clearComboEx(['selPais']);

    if (!(dsoPais.recordset.BOF || dsoPais.recordset.EOF)) {
        dsoPais.recordset.moveFirst;

        while (!dsoPais.recordset.EOF) 
        {
            var oOption = document.createElement("OPTION");
            oOption.text = dsoPais.recordset['Localidade'].value; ;
            oOption.value = dsoPais.recordset['LocalidadeID'].value;
            selPais.add(oOption);
            dsoPais.recordset.MoveNext();
        }

        if (selPais.length > 1)
        {
            selPais.value = 130; //seta Brasil
            selPais.disabled = false;
        }
        else
            selPais.disabled = true;
    }

    preencheUF();    
}

function preencheUF() 
{
    var sFiltroPais = '';
    
    sFiltroPais = ' AND LocalizacaoID = ' + selPais.value.toString();

    setConnection(dsoUF);

    dsoUF.SQL = 'SELECT 0 AS LocalidadeID, \'\' AS Localidade ' +
                  'UNION ALL ' +
                  'SELECT LocalidadeID, Localidade ' +
	                'FROM Localidades a WITH(NOLOCK) ' +
	                'WHERE ((TipoLocalidadeID = 204) AND (EstadoID = 2))' + sFiltroPais;

    dsoUF.ondatasetcomplete = preencheUF_DSC;
    dsoUF.Refresh();
}

function preencheUF_DSC() 
{
    clearComboEx(['selUF']);

    if (!(dsoUF.recordset.BOF || dsoUF.recordset.EOF)) {
        dsoUF.recordset.moveFirst;

        while (!dsoUF.recordset.EOF) {
            var oOption = document.createElement("OPTION");
            oOption.text = dsoUF.recordset['Localidade'].value; ;
            oOption.value = dsoUF.recordset['LocalidadeID'].value;
            selUF.add(oOption);
            dsoUF.recordset.MoveNext();
        }        
    }

    preencheMunicipio();    
}

function preencheMunicipio() 
{
    var sFiltroUF = '';

    sFiltroUF = ' AND LocalizacaoID = ' + selUF.value.toString();

    setConnection(dsoMunicipio);

    dsoMunicipio.SQL = 'SELECT 0 AS LocalidadeID, \'\' AS Localidade ' +
                  'UNION ALL ' +
                  'SELECT LocalidadeID, Localidade ' +
	                'FROM Localidades a WITH(NOLOCK) ' +
	                'WHERE ((TipoLocalidadeID = 205) AND (EstadoID = 2))' + sFiltroUF + 
                    ' ORDER BY Localidade ASC';

    dsoMunicipio.ondatasetcomplete = preencheMunicipio_DSC;
    dsoMunicipio.Refresh();
}

function preencheMunicipio_DSC() 
{
    clearComboEx(['selMunicipio']);

    if (!(dsoMunicipio.recordset.BOF || dsoMunicipio.recordset.EOF)) 
    {
        dsoMunicipio.recordset.moveFirst;

        while (!dsoMunicipio.recordset.EOF) 
        {
            var oOption = document.createElement("OPTION");
            oOption.text = dsoMunicipio.recordset['Localidade'].value; ;
            oOption.value = dsoMunicipio.recordset['LocalidadeID'].value;
            selMunicipio.add(oOption);
            dsoMunicipio.recordset.MoveNext();
        }        
    }

    lockControlsInModalWin(false);
    
    selUF.disabled = (selUF.length < 2);
    selMunicipio.disabled = (selMunicipio.length < 2);
}

function selPais_onChange() 
{
    lockControlsInModalWin(true);
    preencheUF();
}

function selUF_onChange() 
{
    lockControlsInModalWin(true);
    preencheMunicipio();
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    // 1. Usuario escolheu uma cidade, deu um duplo clique na mesma
    // ou escolheu uma cidade e clicou o botao OK
    // 2. Usuario escolheu um estado e clicou o botao OK
    
    // Devolve sempre um array:
    // Elem 0: glb_sBtnClicked
    // Elem 1:
    // glb_sNivelGovID == 232 - Imposto estadual
    // ou
    // glb_sNivelGovID == 233 - Imposto municipal
    // Elem 2:
    // Nome do estado ou da cidade
    // Elem 3:
    // Sigla do estado, da cidade nao tem devolve ''
    // Elem 4:
    // ID do estado ou da cidade
    
    if (ctl.id == btnOK.id )
    {
        var aLocalidade = new Array();
        var _retMsg = 0;
        var sMensagem = "Deseja alterar a localidade para ";

        if (selMunicipio.value != 0) 
        {
            aLocalidade[0] = selMunicipio.value;
            aLocalidade[1] = selMunicipio.options.item(selMunicipio.selectedIndex).text;

            sMensagem += "o Municipio " + aLocalidade[1] + "?";
        }
        else if (selUF.value != 0) 
        {
            aLocalidade[0] = selUF.value;
            aLocalidade[1] = selUF.options.item(selUF.selectedIndex).text;

            sMensagem += "a UF " + aLocalidade[1] + "?";
        }
        else if (selPais.value != 0) 
        {
            aLocalidade[0] = selPais.value;
            aLocalidade[1] = selPais.options.item(selPais.selectedIndex).text;

            sMensagem += "o Pais " + aLocalidade[1] + "?";
        }

        if (glb_nLocalidadeID != '0') 
            _retMsg = window.top.overflyGen.Confirm(sMensagem.toString());
        else
            _retMsg = 1;

        if (_retMsg == 1) 
        {
            sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_I', aLocalidade);
        }
        else 
        {
            lockControlsInModalWin(false);
            return null;
        }
    }    
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_I', null );    
    
}