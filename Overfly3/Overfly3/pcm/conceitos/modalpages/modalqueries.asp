<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim strConn, pagesURLRoot
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))

    Set objSvrCfg = Nothing
%>

<html id="modalqueriesHtml" name="modalqueriesHtml">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/modcontabil/submodcontabilidade/historicospadrao/modalpages/modalqueries.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/pcm/conceitos/modalpages/modalqueries.js" & Chr(34) & "></script>" & vbCrLf    
%>

<%
'Script de variaveis globais

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

Dim i, btnOKState, dso, fieldName, gridID, colQuery, nConceitoID
Dim sConta

btnOKState = "D"  'desabilitado
dso = ""
fieldName = ""
gridID = ""
colQuery = 0
sConta = ""
nConceitoID = 0


For i = 1 To Request.QueryString("btnOKState").Count
    btnOKState = Request.QueryString("btnOKState")(i)
Next

For i = 1 To Request.QueryString("dso").Count
    dso = Request.QueryString("dso")(i)
Next

For i = 1 To Request.QueryString("fieldName").Count
    fieldName = Request.QueryString("fieldName")(i)
Next

For i = 1 To Request.QueryString("gridID").Count
    gridID = Request.QueryString("gridID")(i)
Next

For i = 1 To Request.QueryString("colQuery").Count
    colQuery = Request.QueryString("colQuery")(i)
Next

For i = 1 To Request.QueryString("sConta").Count
    sConta = Request.QueryString("sConta")(i)
Next

For i = 1 To Request.QueryString("nConceitoID").Count
    nConceitoID = Request.QueryString("nConceitoID")(i)
Next

If (btnOKState = "D") Then
    Response.Write "var glb_btnOKState = 'D';"
Else
    Response.Write "var glb_btnOKState = 'H';"
End If

Response.Write "var glb_dso = " & Chr(39) & dso & Chr(39) & ";"
Response.Write vbcrlf
Response.Write "var glb_fieldName = " & Chr(39) & fieldName & Chr(39) & ";"
Response.Write vbcrlf
Response.Write "var glb_gridID = " & Chr(39) & gridID & Chr(39) & ";"
Response.Write vbcrlf
Response.Write "var glb_colQuery = " & CStr(colQuery) & ";"
Response.Write vbcrlf
Response.Write "var glb_sConta = " & Chr(39) & sConta & Chr(39) & ";"
Response.Write vbcrlf
Response.Write "var glb_nConceitoID = " & Chr(39) & nConceitoID & Chr(39) & ";"
Response.Write vbcrlf

Response.Write vbcrlf

Response.Write "</script>"
Response.Write vbcrlf
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--
;
//-->
</script>

</head>

<body id="modalqueriesBody" name="modalqueriesBody" LANGUAGE="javascript" onload="return window_onload()">
    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>    

    <div id="divCampos" name="divCampos" class="divGeneral">
	    <p id="lblIdiomaID" name="lblIdiomaID" class="lblGeneral">Idioma</p>
	    <select id="selIdiomaID" name="selIdiomaID" class="fldGeneral">
<%
	Dim strSQL, rsData

	Set rsData = Server.CreateObject("ADODB.Recordset")

	strSQL = "SELECT a.ItemID AS IdiomaID, a.ItemMasculino AS Idioma, ISNULL(b.ConArgumentoID, 0) AS ConArgumentoID, " & _
	    "ISNULL(b.ArgumentosVenda, SPACE(0)) AS ArgumentosVenda, " & _
	    "ISNULL(b.OK, 0) AS OK " & _
        "FROM TiposAuxiliares_Itens a WITH(NOLOCK) LEFT OUTER JOIN " & _
        "Conceitos_ArgumentosVenda b WITH(NOLOCK) ON a.ItemID=b.IdiomaID AND b.ConceitoID=" & CStr(nConceitoID) & " " & _
        "WHERE (a.EstadoID=2 AND a.TipoID=48) " & _
        "ORDER BY a.Ordem"

	rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

	If (Not (rsData.BOF AND rsData.EOF) ) Then

		While Not (rsData.EOF)

			Response.Write("<option value='" & rsData.Fields("IdiomaID").Value & "' ")
			Response.Write("ConArgumentoID='" & rsData.Fields("ConArgumentoID").Value & "' ")
			Response.Write("argumentosVenda='" & rsData.Fields("ArgumentosVenda").Value & "' ")
			Response.Write("OK='" & rsData.Fields("OK").Value & "' >")
			Response.Write(rsData.Fields("Idioma").Value & "</option>")

			rsData.MoveNext()

		WEnd

	End If

	rsData.Close
	
	Set rsData = Nothing
%>
	    </select>
	    
	    <p id="lblOK" name="lblOK" class="lblGeneral" LANGUAGE=javascript>OK</p>
	    <input type="checkbox" id="chkOK" name="chkOK" class="fldGeneral" title="Esse Argumento de Venda est� OK?"></input>
    </div>    

     <div id="divObservacoes" name="divObservacoes" class="divGeneral">
        <textarea id="Textarea1" name="txtQuery" VIEWASTEXT LANGUAGE="javascript" onkeyup="return txtQuery_ondigit(this)" onfocus="return __selFieldContent(this)" class="fldGeneral"></textarea>
    </div>    

    <input type="button" id="btnOK" name="btnOK" value="OK" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnCanc" name="btnCanc" value="Cancelar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">        

</body>

</html>
