/********************************************************************
pesqlist.js

Library javascript de funcoes do pesqlist do form conceitos
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_sUltimaPesquisa = '';
var glb_first = true;
var glb_contexto = 0;
// Dados da listagem da pesquisa
var dsoListData01 = new CDatatransport("dsoListData01");
// Dados dos combos de contexto e filtros
var dsoCmbsContFilt = new CDatatransport("dsoCmbsContFilt");
// Dados dos proprietarios para o pesqlist .URL
var dsoPropsPL = new CDatatransport("dsoPropsPL");
// Replica��o dos produtos
var dsoReplicarInformacoes = new CDatatransport('dsoReplicarInformacoes');
// Replica��o dos produtos
var dsoReplicar = new CDatatransport('dsoReplicar');
var glb_TimerClick = null;
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

FUNCOES DA AUTOMACAO:
    pesqlistIsVisibleAndUnlocked()
    btnBarClicked(controlBar, btnClicked)
    modalInformForm(idElement, param1, param2)
    specialClauseOfResearch()

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

function window_onload() {
    windowOnLoad_1stPart();
    

    //@@ Ordem e titulos das colunas do grid de pesquisa
    glb_COLPESQORDER = new Array('ID', 'Est',
                                 'Conceito', 'Tipo', 'Observacoes');

    //var contexto = getCmbCurrDataInControlBar('sup', 1);

    glb_aCOLPESQFORMAT = new Array('', '', '', '', '', '', '', '###.00', '###.00', '###.00', '###.00', '###.00', '###.00', '###.00', '');

    adjustElementsInForm([['lblRefrInf', 'chkRefrInf', 3, 1],
                              ['lblOrdem', 'chkOrdem', 3, 1, -9],
                              ['lblRegistrosVencidos', 'chkRegistrosVencidos', 3, 1, -9],
                              ['lblProprietariosPL', 'selProprietariosPL', 18, 1, -5],                              
                              ['lblRegistros', 'selRegistros', 6, 1, -3],
                              ['lblMetodoPesq', 'selMetodoPesq', 9, 1, -9],
                              ['lblPesquisa', 'selPesquisa', 14, 1, -4],
                              ['lblArgumento', 'txtArgumento', 12, 1, -4],
                              ['lblFiltro', 'txtFiltro', 50, 1, -4]]);


    windowOnLoad_2ndPart();
}

/********************************************************************
Funcao disparada pelo frame work.
O pesqlist esta visivel e ativo
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function pesqlistIsVisibleAndUnlocked() {
    showBtnsEspecControlBar('sup', true, [1, 1, 1, 1, 1, 1, 1, 1, 1]);
    tipsBtnsEspecControlBar('sup', ['Documentos', 'Relat�rios', 'Procedimento', 'Imagem', 'N�meros de s�rie', 'NCM', 'Buscar Produtos', 'Replicar cadastro', 'Produtos ESD']);

    setupEspecBtnsControlBar('sup', 'HHHDHHHHH');

    var contexto = getCmbCurrDataInControlBar('sup', 1);
    var lines = fg.Rows;

    if ((contexto[1] == 2112) && (lines > 1)) {
        var confIndex;
        var total;

        confIndex = getColIndexByColKey(fg, 'Conf');

        // loop das colunas, as 7 colunas seguintes far�o a media
        for (j = confIndex; j < confIndex + 7; j++) {
            total = 0;

            // loop de linhas - faz media dos totais
            for (i = 2; i < lines; i++)
                total += fg.ValueMatrix(i, j);

            // atualiza linhas de totais
            fg.TextMatrix(1, parseInt(j)) = total / (lines - 2);
        }
    }
    else if (contexto[1] == 2115) {
        alignColsInGrid(fg, [12]);
    }
    btnGetProps.style.visibility = 'hidden';

    if (glb_first == true) {
        carregaProprietarios();
        glb_contexto = contexto[1];
    } else if (glb_contexto != contexto[1]) {
        carregaProprietarios();
        glb_first = true;
        glb_contexto = contexto[1];        
    }

    lblProprietariosPL.title= 'Clique simples: recarrega o combo \n'+
                              'Clique duplo: alterna o combo entre Propriet�rio e Alternativo e recarrega o combo';

    // Proprietario
    lblProprietariosPL.onmouseover = lblProprietariosPL_onmouseover;
    lblProprietariosPL.onmouseout = lblProprietariosPL_onmouseout;    
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, 4, 5

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked) {
    if (controlBar == 'SUP') {
        // Usuario clicou botao documentos
        if (btnClicked == 1) {
            if (fg.Rows > 1) {
                __openModalDocumentos(controlBar, fg.TextMatrix(fg.Row, 0));
            }
            else {
                window.top.overflyGen.Alert('Selecione um registro.');
            }
        }
            // usuario clicou botao imprimir
        else if (btnClicked == 2)
            openModalPrint();
        else if (btnClicked == 3)
            window.top.openModalControleDocumento('PL', '', 710, null, '', 'T');
        else if (btnClicked == 5)
            openModalPesqNumSerie();
        else if (btnClicked == 6)
            detalhaNCMPesqlist();
        else if (btnClicked == 7)
            openModalBuscarProdutos();
        else if (btnClicked == 8)
            replicarCadastroInformacoes();
        else if (btnClicked == 9)
            openModalProdutosESD();
    }
}

/********************************************************************
********************************************************************/
function detalhaNCMPesqlist() {
    var sTipo = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'TipoConceito'));
    var NCM;

    if (sTipo == "Classifica��o Fiscal") {
        NCM = parseInt(fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Conceito')), 10);
        NCM = parseInt(NCM / 10000);

        strPars = '?sNCM=' + NCM;
        htmlPath = SYS_ASPURLROOT + '/pcm/conceitos/serverside/imagemncm.aspx' + strPars;
        window.open(htmlPath);
    } else {
        alert("O imposto escolhido n�o � uma classifica��o fiscal.");
    }

}

/********************************************************************
Funcao disparada pelo frame work.
Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2) {
    // Modal de impressao
    if (idElement.toUpperCase() == 'MODALPRINTHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');

            // nao mexer
            return 0;
        }
    }
    if (idElement.toUpperCase() == 'MODALCONTROLEDOCUMENTOHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');
            return 0;
        }
    }

    // Modal Pesquisa de Num Serie
    if (idElement.toUpperCase() == 'MODALPESQNUMSERIEHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');

            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');

            return 0;
        }
    }

    // Modal de documentos
    if (idElement.toUpperCase() == 'MODALDOCUMENTOSHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }
    // Modal Busca de Produtos
    if (idElement.toUpperCase() == 'MODALBUSCARPRODUTOSHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');

            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');

            return 0;
        }
    }
    // Modal Produtos ESD
    if (idElement.toUpperCase() == 'MODALPRODUTOSESDHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');

            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');

            return 0;
        }
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Pede string complementar de pesquisa para forms que tenham este caso.

Parametro:
nenhum

Retorno:
a string ou null se nao tem
********************************************************************/
function specialClauseOfResearch() {
    var contexto = getCmbCurrDataInControlBar('sup', 1);

    if (contexto[1] != 2112) {
        glb_aCOLPESQFORMAT = null;
    }

    // Se for contexto de Imposto exibe coluna de imposto
    if (contexto[1] == 2115)
        glb_COLPESQORDER = new Array('ID', 'Est', 'Conceito', 'Tipo', 'Imposto', 'Observacao', 'Observacoes', 'Proprietario', 'Alternativo');

        // Se for contexto de Produtos exibe coluna de Descricao
    else if (contexto[1] == 2112) {
        glb_COLPESQORDER = new Array('ID',
                                     'Est',
                                     'Conceito',
                                     'Tipo',
                                     'Descricao',
                                     'Int',
                                     'Auto',
                                     'Conf',
                                     'Carac',
                                     'AV',
                                     'Imagens',
                                     'PM',
                                     'Ident',
                                     'Espec',
                                     'BF',
                                     'Observa��o',
                                     'Observa��es',
                                     'Propriet�rio',
                                     'Alternativo');
    }
    else{
        glb_COLPESQORDER = new Array('ID',
                                     'Est',
                                     'Conceito',
                                     'Tipo',                                     
                                     'Observa��o',
                                     'Observa��es',
                                     'Propriet�rio',
                                     'Alternativo');
    }

    //@@ da automacao -> retorno padrao
    return null;
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal de impressao

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalPrint() {
    // por questoes esteticas, coloca foco no grid de pesquisa
    if (fg.disabled == false)
        fg.focus();

    var htmlPath;
    var strPars = new String();
    var empresaData = getCurrEmpresaData();
    var empresaID = empresaData[0];
    var empresaFantasia = empresaData[3];
    var contexto = getCmbCurrDataInControlBar('sup', 1);
    var userID = getCurrUserID();

    // mandar os parametros para o servidor
    // o primeiro parametro e obrigatorio

    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + escape('PL');

    // parametros opcionais
    strPars += '&nEmpresaID=' + escape(empresaID);
    strPars += '&sEmpresaFantasia=' + escape(empresaFantasia);
    strPars += '&nContextoID=' + escape(contexto[1]);
    strPars += '&nUserID=' + escape(userID);

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_ASPURLROOT + '/pcm/conceitos/modalpages/modalprint.asp' + strPars;

    showModalWin(htmlPath, new Array(370, 200));
}

function openModalPesqNumSerie() {
    var htmlPath;
    var strPars = new String();
    var nWidth = 770;
    var nHeight = 470;

    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + escape('PL');
    strPars += '&nPedidoID=' + escape(0);

    // carregar a modal
    htmlPath = SYS_PAGESURLROOT + '/modcomercial/submodpedidos/frmpedidos/modalpages/modalpesqnumserie.asp' + strPars;

    showModalWin(htmlPath, new Array(nWidth, nHeight));
}

function openModalBuscarProdutos() {
    var htmlPath;
    var strPars = new String();
    var nWidth = 980;
    var nHeight = 570;

    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + escape('PL');
    strPars += '&glb_sUltimaPesquisa=' + escape(glb_sUltimaPesquisa);

    // carregar a modal
    htmlPath = SYS_PAGESURLROOT + '/pcm/conceitos/modalpages/modalbuscarprodutos.asp' + strPars;

    showModalWin(htmlPath, new Array(nWidth, nHeight));
}

function openModalProdutosESD() {
    var htmlPath;
    var strPars = new String();
    var nWidth = 880;
    var nHeight = 520;

    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + escape('PL');

    // carregar a modal
    htmlPath = SYS_PAGESURLROOT + '/pcm/conceitos/modalpages/modalprodutosesd.asp' + strPars;

    showModalWin(htmlPath, new Array(nWidth, nHeight));
}

function replicarCadastroInformacoes() {
    lockInterface(true);

    dsoReplicarInformacoes.SQL =
        "DECLARE @Informacoes TABLE (Informacao VARCHAR(50), Ordem INT) " +
        "INSERT INTO @Informacoes " +
	        "SELECT TOP 1 CONVERT(VARCHAR, a.dtData, 103) + ' ' + CONVERT(VARCHAR, a.dtData, 108) AS Informacao, " +
			        "1 AS Ordem " +
		        "FROM LogReplicacao a WITH(NOLOCK) " +
		        "WHERE a.dtReplicacao IS NOT NULL " +
		        "ORDER BY dtData DESC " +
        "INSERT INTO @Informacoes " +
	        "SELECT (CASE b.TipoReplicacao " +
                        "WHEN 1 THEN 'Caracteristicas: ' + CONVERT(VARCHAR,COUNT(1)) + ' produto' + (CASE WHEN COUNT(1) > 1 THEN 's' ELSE '' END) " +
                        "WHEN 2 THEN 'Argumentos de Venda: ' + CONVERT(VARCHAR,COUNT(1)) + ' produto' + (CASE WHEN COUNT(1) > 1 THEN 's' ELSE '' END) " +
                        "WHEN 3 THEN 'Imagens: ' + CONVERT(VARCHAR,COUNT(1)) + ' produto' + (CASE WHEN COUNT(1) > 1 THEN 's' ELSE '' END) " +
                        "WHEN 4 THEN 'Pesos e Medidas: ' + CONVERT(VARCHAR,COUNT(1)) + ' produto' + (CASE WHEN COUNT(1) > 1 THEN 's' ELSE '' END) " +
				        "ELSE '' END) AS Informacao, " +
			        "2 AS Ordem " +
                "FROM Conceitos a WITH(NOLOCK) " +
                    "INNER JOIN LogReplicacao b WITH(NOLOCK) ON (b.RegistroID = a.ConceitoID) " +
                "WHERE ((RTRIM(LTRIM(ISNULL(a.Serie, SPACE(0)))) <> SPACE(0)) AND (b.dtReplicacao IS NULL)) " +
		        "GROUP BY b.TipoReplicacao " +
		        "ORDER BY b.TipoReplicacao " +
        "SELECT * FROM @Informacoes ORDER BY Ordem ";

    dsoReplicarInformacoes.ondatasetcomplete = replicarCadastroInformacoes_DSC;
    dsoReplicarInformacoes.refresh();
}

function replicarCadastroInformacoes_DSC() {
    var ultimaReplicacao = '';
    var replicacaoPendente = '';
    var desejaReplicar = '';
    var sAlert, _retMsg;

    //dsoReplicarInformacoes.recordset.Filter = 'Ordem = 1'
    dsoReplicarInformacoes.recordset.moveFirst();

    while (!(dsoReplicarInformacoes.recordset.BOF || dsoReplicarInformacoes.recordset.EOF)) {
        // Pega data da ultima replica��o realizada
        if (dsoReplicarInformacoes.recordset['Ordem'].value == 1)
            ultimaReplicacao = '�ltima replica��o efetuada em ' + dsoReplicarInformacoes.recordset['Informacao'].value + '.\n\n';
            // Pega componentes que ser�o replicados
        else if (dsoReplicarInformacoes.recordset['Ordem'].value == 2)
            replicacaoPendente += ((replicacaoPendente == '') ? ('Replica��es pendentes: \n') : ('')) + '- ' + dsoReplicarInformacoes.recordset['Informacao'].value + '\n';

        dsoReplicarInformacoes.recordset.moveNext();
    }

    if (replicacaoPendente.length == 0) {
        replicacaoPendente = 'N�o tem replica��o pendente. \n';
        desejaReplicar = '';
    }
    else {
        desejaReplicar = '\nDeseja replicar?\n';
    }

    sAlert = ultimaReplicacao + replicacaoPendente + desejaReplicar;

    if (desejaReplicar != '')
        _retMsg = window.top.overflyGen.Confirm(sAlert);
    else
        window.top.overflyGen.Alert(replicacaoPendente);

    if (_retMsg == 1)
        replicarCadastro();
    else {
        lockInterface(false);
        return null;
    }
}

function replicarCadastro() {
    var strPars = new String();

    strPars = '?nProdutoID=0';
    strPars += '&nUsuarioID=' + glb_USERID;

    dsoReplicar.URL = SYS_ASPURLROOT + '/pcm/conceitos/serverside/replicarcadastro.aspx' + strPars;
    dsoReplicar.ondatasetcomplete = replicarCadastro_DSC;
    dsoReplicar.refresh();
}

function replicarCadastro_DSC() {
    lockInterface(false);
}
/********************************************************************
Retorno de usuario clicou o botao do combo de proprietarios

Parametros:
nenhum

Retorno:
nenhum
********************************************************************/
function btnGetProps_onClick_DSC()
{
    var optionStr, optionValue;

    clearComboEx([selProprietariosPL.name]);

    while (!dsoPropsPL.recordset.EOF) {
        var oOption = document.createElement("OPTION");
        oOption.text = dsoPropsPL.recordset['Proprietario'].value;
        oOption.value = dsoPropsPL.recordset['ProprietarioID'].value;
        selProprietariosPL.add(oOption);
        dsoPropsPL.recordset.MoveNext();
    }

    if (glb_first == true)
    {
        carregaProprietarios();
        glb_first = false;
    }
    else
    {
        lockInterface(false);
    }
}

function carregaProprietarios()
{
    lockInterface(true);
    var cmbContextData = getCmbCurrDataInControlBar('sup', 1);
    var cmbFiltroPadraoData = getCmbCurrDataInControlBar('sup', 2);
    var contextID = cmbContextData[1];
    var FiltroPadraoID = cmbFiltroPadraoData[1];
    var Proprietario = 0;
    var RegistrosVencidos = 0;

    // Registros Vencidos
    if (chkRegistrosVencidos.checked)
        RegistrosVencidos = 1;
    else
        RegistrosVencidos = 0;

    // Parametros para o servidor
    var strPars = new String();
    strPars = '?';

    var subFormID = window.top.subFormID;  // variavel global do browser filho    

    // formID
    strPars += 'nSubFormID=';
    strPars += encodeURIComponent(subFormID.toString());
    // contextoID
    strPars += '&nContextoID=';
    strPars += encodeURIComponent(contextID.toString());
    //FiltroPadraoID
    strPars += '&nFiltroPadraoID=';
    strPars += encodeURIComponent(FiltroPadraoID.toString());
    // RegistrosVencidos
    strPars += '&bRegistrosVencidos=' + encodeURIComponent(RegistrosVencidos);
    // empresaID
    strPars += '&nEmpresaID=' + encodeURIComponent((sendJSMessage('AnyHtml', JS_COMBODATA, 'VALUE', null)).toString());
    // userID
    strPars += '&nUsuarioID=' + encodeURIComponent((sendJSMessage('AnyHtml', JS_WIDEMSG, '__CURRUSERID', null)).toString());
    strPars += '&bAllList=' + encodeURIComponent(1);
    // Resultado
    strPars += '&bResultado=' + encodeURIComponent((glb_modoComboProprietario == true ? 1 : 0));

    dsoPropsPL.URL = SYS_ASPURLROOT + '/serversidegenEx/cmbspropspl2.aspx' + strPars;
    dsoPropsPL.ondatasetcomplete = btnGetProps_onClick_DSC;
    dsoPropsPL.refresh();
}


function lblProprietariosPL_onClick()
{
    glb_TimerClick = window.setInterval('lblProprietariosPL_onClick_Continue()', 10, 'JavaScript');;
}

function lblProprietariosPL_onClick_Continue()
{
    if (glb_TimerClick != null)
    {
        window.clearInterval(glb_TimerClick);
        glb_TimerClick = null;
    }

    carregaProprietarios();
}

function lblProprietariosPL_ondblClick()
{
    glb_modoComboProprietario = !glb_modoComboProprietario;

    lblProprietariosPL.innerText = (glb_modoComboProprietario == true ? 'Propriet�rio' : 'Alternativo');

    carregaProprietarios();
}


function lblProprietariosPL_onmouseover()
{
    lblProprietariosPL.style.color = 'blue';
    lblProprietariosPL.style.cursor = 'hand';
}

function lblProprietariosPL_onmouseout()
{
    lblProprietariosPL.style.color = 'black';
    lblProprietariosPL.style.cursor = 'point';
}


