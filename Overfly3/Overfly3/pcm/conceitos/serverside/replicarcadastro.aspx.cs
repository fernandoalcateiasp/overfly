using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.pcm.conceitos.serverside
{
    public partial class replicarcadastro : System.Web.UI.OverflyPage
	{
        private Integer ProdutoID;
        protected Integer nProdutoID
        {
            get { return ProdutoID; }
            set { ProdutoID = value; }
        }

        private Integer UsuarioID;
        protected Integer nUsuarioID
        {
            get { return UsuarioID; }
            set { UsuarioID = value; }
        }

        protected void replicacadastro()
        {
            ProcedureParameters[] parameters = new ProcedureParameters[2];
            parameters[0] = new ProcedureParameters(
                "@ProdutoOrigemID",
                System.Data.SqlDbType.Int,
                (((ProdutoID != null) && (ProdutoID.intValue() != 0)) ? (Object)ProdutoID.intValue() : System.DBNull.Value));

            parameters[1] = new ProcedureParameters(
                "@UsuarioID",
                System.Data.SqlDbType.Int,
                (UsuarioID != null ? (Object)UsuarioID.intValue() : System.DBNull.Value));
                
            DataInterfaceObj.execNonQueryProcedure(
                "sp_Produto_Replicar",
                parameters);
        }

		protected override void PageLoad(object sender, EventArgs e)
		{
            replicacadastro();

            WriteResultXML(DataInterfaceObj.getRemoteData("select 0 as fldresp"));
		}
	}
}
