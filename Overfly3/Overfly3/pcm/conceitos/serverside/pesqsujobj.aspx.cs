using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.pcm.conceitos.serverside
{
	public partial class pesqsujobj : System.Web.UI.OverflyPage
	{
		private Integer tipoRelID;
		protected Integer nTipoRelID
		{
			get { return tipoRelID; }
			set { tipoRelID = value; }
		}

		private string tipoSujObj;
		protected string sTipoSujObj
		{
			get { return tipoSujObj; }
			set { tipoSujObj = value; }
		}

		private string strOperator = " >= ";

		private string  toFind;
		protected string strToFind
		{
			get { return toFind; }
			set
			{
				toFind = value;

				if (toFind != null && (toFind.StartsWith("%") || toFind.EndsWith("%")))
				{
					strOperator = " LIKE ";
				}
				else
				{
					strOperator = " >= ";
				}
			}
		}

		private Integer sujObjID;
		protected Integer nSujObjID
		{
			get { return sujObjID; }
			set { sujObjID = value; }
		}

		private Integer relEntreID;
		protected Integer nRelEntreID
		{
			get { return relEntreID; }
			set { relEntreID = value; }
		}

		protected string SQL
		{
			get
			{
				string sql = "";

				// Rel entre Conceitos
				if (nRelEntreID.intValue() == 134)
				{
					if(sTipoSujObj == "SUJEITO")
					{
						sql = "SELECT TOP 100 c.Conceito AS fldName,c.ConceitoID AS fldID,c.Conceito AS Fantasia " +
                                 "FROM Conceitos a WITH(NOLOCK), TiposRelacoes_Itens b WITH(NOLOCK), Conceitos c WITH(NOLOCK) " + 
								 "WHERE a.ConceitoID = " + nSujObjID + " AND a.TipoConceitoID = b.TipoObjetoID " + 
								 "AND b.TipoRelacaoID = " + nTipoRelID + " AND b.TipoSujeitoID = c.TipoConceitoID " + 
								 "AND c.EstadoID = 2 " +
                                 "AND c.ConceitoID NOT IN (SELECT a.SujeitoID FROM RelacoesConceitos a WITH(NOLOCK) " + 
								 "WHERE a.ObjetoID=" + nSujObjID + " AND a.TipoRelacaoID=" + nTipoRelID + ") " + 
								 "AND c.Conceito " + strOperator + "'" + strToFind + "' " + 
								 "ORDER BY fldName ";
					}
					else if (sTipoSujObj == "OBJETO")
					{
						sql = "SELECT TOP 100 c.Conceito AS fldName,c.ConceitoID AS fldID,c.Conceito AS Fantasia " +
                                 "FROM Conceitos a WITH(NOLOCK), TiposRelacoes_Itens b WITH(NOLOCK), Conceitos c WITH(NOLOCK) " + 
								 "WHERE a.ConceitoID = " + nSujObjID + " AND a.TipoConceitoID = b.TipoSujeitoID " + 
								 "AND b.TipoRelacaoID = " + nTipoRelID + " AND b.TipoObjetoID = c.TipoConceitoID " + 
								 "AND c.EstadoID = 2 " +
                                 "AND c.ConceitoID NOT IN (SELECT a.ObjetoID FROM RelacoesConceitos a WITH(NOLOCK) " + 
								 "WHERE a.SujeitoID=" + nSujObjID + " AND a.TipoRelacaoID=" + nTipoRelID + ") " + 
								 "AND c.Conceito " + strOperator + "'" + strToFind + "' " + 
								 "ORDER BY fldName ";
					}
				}
				// Rel entre Pessoas e Conceitos
				else if (nRelEntreID.intValue() == 135)
				{
					if (nTipoRelID.intValue() == 61)
					{
						sql = "SELECT TOP 100 c.Nome AS fldName,c.PessoaID AS fldID,c.Fantasia AS Fantasia, " +
                                 "dbo.fn_Pessoa_Documento(c.PessoaID, NULL, NULL, 101, 111, 0) AS Documento, g.Localidade " +  
								 "AS Cidade, h.CodigoLocalidade2 AS UF, i.CodigoLocalidade2 AS Pais " +
                                 "FROM Conceitos a WITH(NOLOCK), TiposRelacoes_Itens b WITH(NOLOCK), Pessoas c WITH(NOLOCK) " +
                                 "LEFT OUTER JOIN Pessoas_Enderecos f WITH(NOLOCK) ON (c.PessoaID = f.PessoaID AND (f.Ordem=1)) " +
                                 "LEFT OUTER JOIN Localidades g WITH(NOLOCK) ON f.CidadeID = g.LocalidadeID " +
                                 "LEFT OUTER JOIN Localidades h WITH(NOLOCK) ON f.UFID = h.LocalidadeID " +
                                 "LEFT OUTER JOIN Localidades i WITH(NOLOCK) ON f.PaisID = i.LocalidadeID, RelacoesPesRec d " + 
								 "WHERE a.ConceitoID = " + nSujObjID + " AND a.TipoConceitoID = b.TipoObjetoID " + 
								 "AND b.TipoRelacaoID =  " + nTipoRelID + " AND b.TipoSujeitoID = c.TipoPessoaID " +  
								 "AND c.EstadoID = 2 AND c.PessoaID = d.SujeitoID AND d.TipoRelacaoID = 12 AND d.ObjetoID = 999 AND d.EstadoID = 2 " +
                                 "AND c.PessoaID NOT IN (SELECT a.SujeitoID FROM RelacoesPesCon a WITH(NOLOCK) " + 
								 "WHERE a.ObjetoID= " + nSujObjID + " AND a.TipoRelacaoID=" + nTipoRelID + " ) " + 
								 "AND c.Nome " + strOperator + "'" + strToFind + "' " + 
								 "ORDER BY fldName ";
					}
					else
					{
						sql = "SELECT TOP 100 c.Nome AS fldName,c.PessoaID AS fldID,c.Fantasia AS Fantasia, " +
                                 "dbo.fn_Pessoa_Documento(c.PessoaID, NULL, NULL, 101, 111, 0) AS Documento, g.Localidade " +  
								 "AS Cidade, h.CodigoLocalidade2 AS UF, i.CodigoLocalidade2 AS Pais " +
                                 "FROM Conceitos a WITH(NOLOCK), TiposRelacoes_Itens b WITH(NOLOCK), Pessoas c WITH(NOLOCK) " +
                                 "LEFT OUTER JOIN Pessoas_Enderecos f WITH(NOLOCK) ON (c.PessoaID = f.PessoaID AND (f.Ordem=1)) " +
                                 "LEFT OUTER JOIN Localidades g WITH(NOLOCK) ON f.CidadeID = g.LocalidadeID " +
                                 "LEFT OUTER JOIN Localidades h WITH(NOLOCK) ON f.UFID = h.LocalidadeID " +
                                 "LEFT OUTER JOIN Localidades i WITH(NOLOCK) ON f.PaisID = i.LocalidadeID " + 
								 "WHERE a.ConceitoID = " + nSujObjID + " AND a.TipoConceitoID = b.TipoObjetoID " + 
								 "AND b.TipoRelacaoID =  " + nTipoRelID + " AND b.TipoSujeitoID = c.TipoPessoaID " +  
								 "AND c.EstadoID = 2 " +
                                 "AND c.PessoaID NOT IN (SELECT a.SujeitoID FROM RelacoesPesCon a WITH(NOLOCK) " + 
								 "WHERE a.ObjetoID= " + nSujObjID + " AND a.TipoRelacaoID=" + nTipoRelID + " ) " + 
								 "AND c.Nome " + strOperator + "'" + strToFind + "' " + 
								 "ORDER BY fldName ";
					}
				}

				return sql;
			}
		}

		protected override void PageLoad(object sender, EventArgs e)
		{
			WriteResultXML(DataInterfaceObj.getRemoteData(SQL));
		}
	}
}
