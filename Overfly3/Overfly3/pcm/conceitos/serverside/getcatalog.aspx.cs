using System;
using System.Xml;
using System.IO;
using System.Net;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;
using System.Collections.Generic;

namespace Overfly3.pcm.conceitos.serverside
{
	public partial class getcatalog : System.Web.UI.OverflyPage
	{
        protected String getToken()
        {
            WebRequest req = WebRequest.Create("http://esd.alcateia.com.br/api/ESDStandardApi/GetAuthKey?username=admin&password=2cAtH5");
            
            String str = "";
            req.Method = "GET";

            HttpWebResponse resp = req.GetResponse() as HttpWebResponse;

            if (resp.StatusCode == HttpStatusCode.OK)
            {
                using (Stream respStream = resp.GetResponseStream())
                {
                    XmlTextReader reader = new XmlTextReader(respStream);


                    String element = "";

                    while (reader.Read())
                    {
                        switch (reader.NodeType)
                        {
                            // The node is an Element.
                            case XmlNodeType.Element: element = reader.Name; break;

                            //Display the text in each element.
                            case XmlNodeType.Text: if ("Token" == element) str += reader.Value; break;
                        }
                    }

                    reader.Close();
                    return str;
                }
            }
            else
            {
                return (string.Format("ERROR!!\nStatus Code: {0}, Status Description: {1}", resp.StatusCode, resp.StatusDescription));
            }
        }

        protected DataSet getCatalog()
        {
            String token = getToken();
            
            WebRequest request =
                WebRequest.Create("http://esd.alcateia.com.br/api/ESDStandardApi/GetCatalog?authKey=" + token);
            request.Method = "GET";
            HttpWebResponse response = request.GetResponse() as HttpWebResponse;
            
            DataSet dsAtma = new DataSet();
            
            if (response.StatusCode == HttpStatusCode.OK)
            {
                Stream responseStream = response.GetResponseStream();
                XmlTextReader reader = new XmlTextReader(responseStream);
                dsAtma.ReadXml(reader);
            }

            DataSet ds = DataInterfaceObj.getRemoteData("SELECT TOP 0 CONVERT(VARCHAR, SPACE(0)) AS teste, CONVERT(VARCHAR, SPACE(0)) AS PartNumber, " +
                                                        "CONVERT(VARCHAR, SPACE(0)) AS Description, " +
                                                        "CONVERT(VARCHAR, SPACE(0)) AS Manufacturer, " +
                                                        "CONVERT(VARCHAR, SPACE(25)) AS Conceito, " +
                                                        "CONVERT(VARCHAR, SPACE(40)) AS Serie, " +
                                                        "CONVERT(BIT, 0) AS OK");

            ds.Tables.Remove("TABLE_DATA");
            ds.Tables.Add(dsAtma.Tables["CatalogItemResult"].Copy());
            ds.Tables["CatalogItemResult"].TableName = "TABLE_DATA";

            ds.Tables[1].Columns.Remove("Items_Id");

            return ds;
        }

        protected DataSet filtraProdutosESD()
        {
            DataSet ds = getCatalog();
            DataSet ProdutosOverfly = DataInterfaceObj.getRemoteData("SELECT PartNumber FROM Conceitos WITH(NOLOCK) WHERE LinhaProdutoID = 4099");

            for (int i = 0; i < ProdutosOverfly.Tables[1].Rows.Count; i++)
            {
                for (int j = 0; j < ds.Tables[1].Rows.Count; j++)
                {
                    if (ds.Tables[1].Rows[j]["PartNumber"].ToString() == ProdutosOverfly.Tables[1].Rows[i]["PartNumber"].ToString())
                    {
                        ds.Tables[1].Rows.Remove(ds.Tables[1].Rows[j]);
                    }
                }
            }

            return ds;
        }

		protected override void PageLoad(object sender, EventArgs e)
		{
            WriteResultXML(filtraProdutosESD());
		}
	}
}
