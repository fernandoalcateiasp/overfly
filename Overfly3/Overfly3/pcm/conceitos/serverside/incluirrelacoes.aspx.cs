using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.pcm.conceitos.serverside
{
	public partial class incluirrelacoes : System.Web.UI.OverflyPage
	{
		private Integer tipoRelacaoID;
		protected Integer nTipoRelacaoID
		{
			get { return tipoRelacaoID; }
			set { tipoRelacaoID = value; }
		}

		private Integer produtoObjetoID;
		protected Integer nProdutoObjetoID
		{
			get { return produtoObjetoID; }
			set { produtoObjetoID = value; }
		}

		private Integer usuarioID;
		protected Integer nUsuarioID
		{
			get { return usuarioID; }
			set { usuarioID = value; }
		}

		private Integer[] produtoSujeitoID;
		protected Integer[] nProdutoSujeitoID
		{
			get { return produtoSujeitoID; }
			set { produtoSujeitoID = value; }
		}

		protected bool RelacoesConceitosGerador()
		{
			ProcedureParameters[] param = new ProcedureParameters[]
			{
				new ProcedureParameters("@TipoRelacaoID", SqlDbType.Int, nTipoRelacaoID.intValue(), ParameterDirection.Input),
				new ProcedureParameters("@SujeitoID", SqlDbType.Int, DBNull.Value, ParameterDirection.Input), // nProdutoSujeitoID[i]
				new ProcedureParameters("@ObjetoID", SqlDbType.Int, nProdutoObjetoID.intValue(), ParameterDirection.Input),
				new ProcedureParameters("@UsuarioID", SqlDbType.Int, nUsuarioID.intValue(), ParameterDirection.Input),
			};

			foreach (Integer value in nProdutoSujeitoID)
			{
				param[1].Data = value;

				try
				{
					DataInterfaceObj.execNonQueryProcedure("sp_RelacoesConceitos_Gerador", param);
				}
				catch (System.Exception)
				{
					return false;
				}
			}

			return true;
		}

		protected override void PageLoad(object sender, EventArgs e)
		{
			if (RelacoesConceitosGerador())
			{
				WriteResultXML(DataInterfaceObj.getRemoteData("select '' as Mensagem"));
			}
			else
			{
				WriteResultXML(DataInterfaceObj.getRemoteData("select 'Erro ao gravar. Tente novamente.' as Mensagem"));
			}
		}
	}
}
