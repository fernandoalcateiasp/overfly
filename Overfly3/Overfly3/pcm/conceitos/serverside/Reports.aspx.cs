﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using System.Data;
using System.Configuration;
using OVERFLYSVRCFGLib;
using System.Data.SqlClient;
using System.IO;
using System.Diagnostics;
using WSData;

namespace Overfly3.PrintJet
{
    public partial class ConceitosReports : System.Web.UI.Page
    {
        ClsReport Relatorio = new ClsReport();

        public string[,] arrSqlQuery;
        public string[,] arrIndexKey;
        private int RelatorioID;
        private int mEmpresaID = Convert.ToInt32(HttpContext.Current.Request.Params["glb_nEmpresaID"]);
        bool chkPedidosWeb = Convert.ToBoolean(HttpContext.Current.Request.Params["bchkPedidosWeb"]);
        bool chkPedidosPb = Convert.ToBoolean(HttpContext.Current.Request.Params["bchkPedidosPb"]);
        string sDataInicio = HttpContext.Current.Request.Params["sDataInicio"];
        string sDataFim = HttpContext.Current.Request.Params["sDataFim"];
        protected dataInterface DataInterfaceObj = new dataInterface(System.Configuration.ConfigurationManager.AppSettings["application"]);

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                string nomeRel = Request.Params["relatorioID"].ToString();
                RelatorioID = Convert.ToInt32(Request.Params["relatorioID"].ToString());

                switch (nomeRel)
                {
                    // Homologacao de produtos
                    case "40113":
                        homologacaoProdutos();
                        break;
                    // Homologacao resumo
                    case "40114":
                        homologacaoResumo();
                        break;
                }
            }
        }

        public void homologacaoProdutos()
        {
            // Excel
            int Formato = 2;
            string i;
            string Title = "Homologação Produtos";
            mEmpresaID = Convert.ToInt32(HttpContext.Current.Request.Params["EmpresaID"]);
            string strSQL = "EXEC sp_RelatorioHomologacaoProdutos";
            var Header_Body = (Formato == 1 ? "Header" : "Body");
            var posY = (Formato == 1 ? "0" : "0.004");

            arrSqlQuery = new string[,] { { "Query1", strSQL } };

            string[,] arrIndexKey = new string[,] { { "" } };

            if (Formato == 1)
            {
                Relatorio.CriarRelatório(Title.ToString(), arrSqlQuery, "BRA", "Landscape", "0.80");

                Relatorio.CriarObjLabel("Fixo", "", Title, "10.13834", "0.1", "11", "#0113a0", "B", Header_Body, "4.51332", "");
                //Relatorio.CriarObjLabel("Fixo", "", Title, "1.013834", "0.01", "11", "#0113a0", "B", Header_Body, "4.51332", "");
                Relatorio.CriarObjLabelData("Now", "", "", "19.795", "0.1", "11", "black", "B", Header_Body, "3.72187", "Horas");
                //Relatorio.CriarObjLabelData("Now", "", "", "1.9795", "0.01", "11", "black", "B", Header_Body, "3.72187", "Horas");

                //Cria Tabela      
                Relatorio.CriarObjTabela("0.2", posY, "Query1", true, true, true);
                //Relatorio.CriarObjTabela("0.02", posY, "Query1", true, true, true);
                Relatorio.TabelaEnd();
            }
            else if (Formato == 2) // Se Excel
            {
                Relatorio.CriarRelatório(Title.ToString(), arrSqlQuery, "BRA", "Excel");

                Relatorio.CriarObjLabel("Fixo", "", Title, "10.13834", "0.0", "11", "#0113a0", "B", Header_Body, "4.51332", "");
                //Relatorio.CriarObjLabel("Fixo", "", Title, "1.013834", "0.0", "11", "#0113a0", "B", Header_Body, "4.51332", "");
                Relatorio.CriarObjLabelData("Now", "", "", "16.795", "0.0", "11", "black", "B", Header_Body, "3.72187", "Horas");
                //Relatorio.CriarObjLabelData("Now", "", "", "1.6795", "0.0", "11", "black", "B", Header_Body, "3.72187", "Horas");


                //Relatorio.CriarObjLabel("Fixo", "", Title, "1", "0.0", "11", "#0113a0", "B", Header_Body, "4.51332", "");
                //Relatorio.CriarObjLabelData("Now", "", "", "80", "0.0", "11", "black", "B", Header_Body, "3.72187", "Horas");

                //Cria Tabela           
                Relatorio.CriarObjTabela("0.00", "0.0001", "Query1", true, true, true);
                //Relatorio.CriarObjTabela("0.00", "0.00001", "Query1", true, true, true);
                Relatorio.TabelaEnd();
            }

            Relatorio.CriarPDF_Excel(Title, Formato);
        }

        public void homologacaoResumo()
        {
            // Excel
            int Formato = 2;
            string i;
            string Title = "Homologação Resumo";
            mEmpresaID = Convert.ToInt32(HttpContext.Current.Request.Params["EmpresaID"]);
            string strSQL = "EXEC sp_RelatorioHomologacaoResumo";
            var Header_Body = (Formato == 1 ? "Header" : "Body");
            var posY = (Formato == 1 ? "0" : "0.004");

            arrSqlQuery = new string[,] { { "Query1", strSQL } };

            string[,] arrIndexKey = new string[,] { { "" } };

            if (Formato == 1)
            {
                Relatorio.CriarRelatório(Title.ToString(), arrSqlQuery, "BRA", "Landscape", "0.80");

                Relatorio.CriarObjLabel("Fixo", "", Title, "1.013834", "0.01", "11", "#0113a0", "B", Header_Body, "4.51332", "");
                Relatorio.CriarObjLabelData("Now", "", "", "1.9795", "0.01", "11", "black", "B", Header_Body, "3.72187", "Horas");

                //Cria Tabela           
                Relatorio.CriarObjTabela("0.02", posY, "Query1", true, true, true);
                Relatorio.TabelaEnd();
            }
            else if (Formato == 2) // Se Excel
            {
                Relatorio.CriarRelatório(Title.ToString(), arrSqlQuery, "BRA", "Excel");

                Relatorio.CriarObjLabel("Fixo", "", Title, "1", "0.0", "11", "#0113a0", "B", Header_Body, "4.51332", "");
                Relatorio.CriarObjLabelData("Now", "", "", "80", "0.0", "11", "black", "B", Header_Body, "3.72187", "Horas");

                //Cria Tabela           
                Relatorio.CriarObjTabela("0.00", "0.00001", "Query1", true, true, true);
                Relatorio.TabelaEnd();
            }

            Relatorio.CriarPDF_Excel(Title, Formato);
        }
    }
}
