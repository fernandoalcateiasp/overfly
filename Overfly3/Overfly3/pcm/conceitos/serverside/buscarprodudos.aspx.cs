using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.pcm.conceitos.serverside
{
	public partial class buscarprodudos : System.Web.UI.OverflyPage
	{
		private string busca;
        protected string sBusca
		{
            get { return busca; }
            set { busca = value; }
		}

        private string filtro;
        protected string sFiltro
		{
            get { return filtro; }
            set { filtro = value; }
		}

        private Integer filtroID;
        protected Integer nFiltroID
        {
            get { return filtroID; }
            set { filtroID = value; }
        }
        
        protected DataSet strPesquisa()
        {
            ProcedureParameters[] parameters = new ProcedureParameters[5];

            parameters[0] = new ProcedureParameters(
                "@Busca",
                SqlDbType.VarChar,
                busca, 300);

            parameters[1] = new ProcedureParameters(
                "@IdiomaID",
                System.Data.SqlDbType.Int, 246);

            parameters[2] = new ProcedureParameters(
                "@FiltroID",
                System.Data.SqlDbType.Int,
                (filtroID != null ? (Object)filtroID.intValue() : DBNull.Value));
            
            parameters[3] = new ProcedureParameters(
                "@Filtro",
                SqlDbType.VarChar,
                filtro, 8000);

            parameters[4] = new ProcedureParameters(
                "@TipoResultado",
                System.Data.SqlDbType.Int, 2);

            DataInterfaceObj.execNonQueryProcedure(
                "sp_Produto_Busca",
                parameters);

            return DataInterfaceObj.execQueryProcedure("sp_Produto_Busca", parameters);
        }

		protected override void PageLoad(object sender, EventArgs e)
		{
            WriteResultXML(strPesquisa());
		}
	}
}
