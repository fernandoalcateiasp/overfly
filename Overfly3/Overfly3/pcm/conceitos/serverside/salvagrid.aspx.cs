using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.pcm.conceitos.serverside
{
    public partial class salvagrid : System.Web.UI.OverflyPage
	{
        private static Integer ZERO = new Integer(0);
        private static Integer[] EMPTY = new Integer[0];

        /*protected Integer[] ConCaracteristicaID = EMPTY;
        protected Integer[] nConCaracteristicaID
        {
            get { return ConCaracteristicaID; }
            set { if (value != null) ConCaracteristicaID = value; }
        }

        protected string[] Valor;
        protected string[] sValor
        {
            get { return Valor; }
            set { if (value != null) Valor = value; }
        }

        protected string[] Checado;
        protected string[] sChecado
        {
            get { return Checado; }
            set { if (value != null) Checado = value; }
        }*/

        protected Integer ProdutoID = ZERO;
        protected Integer nProdutoID
        {
            get { return ProdutoID; }
            set { if (value != null) ProdutoID = value; }
        }

        protected string Auto;
        protected string sAuto
        {
            get { return Auto; }
            set { if (value != null) Auto = value; }
        }

        protected Integer UsuarioID = ZERO;
        protected Integer nUsuarioID
        {
            get { return UsuarioID; }
            set { if (value != null) UsuarioID = value; }
        }

        protected string SQL
		{
			get
			{
				string sql = "";
				/*int i;

                for (i = 0; i < ConCaracteristicaID.Length; i++)
                {
                    sql += " UPDATE Conceitos_Caracteristicas SET Checado = " + Checado[i] + ", Valor = '" + Valor[i] + "' " +
                            "WHERE ConCaracteristicaID = " + ConCaracteristicaID[i];
				}

                if (Auto != "")*/
                    sql += " UPDATE Conceitos SET DescricaoAutomatica =  " + Auto + ", UsuarioID = " + UsuarioID + " WHERE ConceitoID = " + ProdutoID;

				return sql;
			}
		}

        protected override void PageLoad(object sender, EventArgs e)
        {
            int fldresp = DataInterfaceObj.ExecuteSQLCommand(SQL);

            WriteResultXML(DataInterfaceObj.getRemoteData(
                "select " + fldresp + " as fldresp"
            ));
        }

	}
}