using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using OVERFLYSVRCFGLib;
using Scripting;

namespace Overfly3.pcm.conceitos.serverside
{
	public partial class imagemncm : System.Web.UI.OverflyPage
	{
		protected string findArquivoNCM(string NCM)
		{
			string result = "";

			OverflyMTSClass omts = new OverflyMTSClass();
			FileSystemObject fso = new FileSystemObject();

			string rootPath = omts.RootLogicAppPath(DataInterfaceObj.ApplicationName);
			string urlRoot = omts.RootLogicAppPath(DataInterfaceObj.ApplicationName);

			string folder = rootPath + "/NCM";
			string urlFile = urlRoot + "/NCM";

			// Sai da funcao se o diretorio de banners nao existir
			if(fso.FolderExists(folder))
			{
				if(fso.FileExists(folder + "/NCM " + NCM + ".pdf"))
				{
					result = urlFile + "/NCM " + NCM + ".pdf";
				}
			}

			return result;
		}

		private string ncm;
		protected string sNCM
		{
			get { return ncm; }
			set { ncm = value; }
		}

		protected override void PageLoad(object sender, EventArgs e)
		{
			string filePath = findArquivoNCM(sNCM);

			if(filePath != null && filePath != "")
			{
				Response.Redirect(filePath);
			}
		}
	}
}
