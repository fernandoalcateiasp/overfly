using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;
using Overfly3.systemEx.serverside;

namespace Overfly3.pcm.conceitos.serverside
{
	public partial class pesqcidade : System.Web.UI.OverflyPage
	{
		private Integer pais = null;
		protected Integer nPais
		{
			get { return pais; }
			set { pais = value; }
		}

		private Integer estado = Constants.INT_ZERO;
		protected Integer nEstado
		{
			get { return estado; }
			set
			{ 
				estado = value;

				if(estado == null)
				{
					estado = Constants.INT_ZERO;
				}
			}
		}

		string toFind = Constants.STR_EMPTY;
		protected string strToFind
		{
			get { return toFind; }
			set
			{
				toFind = value;

				if (toFind == null)
				{
					toFind = Constants.STR_EMPTY;
				}
			}
		}

		protected string SQL
		{
			get
			{
				string sql = "SELECT DISTINCT TOP 100 a.LocalidadeID AS LocalidadeID,a.Localidade AS Localidade " +
                         "FROM Localidades a WITH(NOLOCK) " +
						 "WHERE (a.TipoLocalidadeID = 205 AND a.LocalizacaoID = " + nEstado + ") AND " +
						 "(a.Localidade >= '" + strToFind + "') " +
						 "ORDER BY Localidade ";

				return sql;
			}
		}

		protected override void PageLoad(object sender, EventArgs e)
		{
			WriteResultXML(DataInterfaceObj.getRemoteData(SQL));
		}
	}
}
