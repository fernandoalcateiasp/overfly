using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.pcm.conceitos.serverside
{
    public partial class cadastroesd : System.Web.UI.OverflyPage
	{
        private static Integer ZERO = new Integer(0);

        protected Integer DataLen = ZERO;
        protected Integer nDataLen
        {
            get { return DataLen; }
            set { if (value != null) DataLen = value; }
        }

        protected string[] PartNumber;
        protected string[] sPartNumber
		{
			get { return PartNumber; }
			set { if(value != null) PartNumber = value; }
		}

        protected string[] Conceito;
        protected string[] sConceito
        {
            get { return Conceito; }
            set { if (value != null) Conceito = value; }
        }

        protected string[] Serie;
        protected string[] sSerie
        {
            get { return Serie; }
            set { if (value != null) Serie = value; }
        }
        
        protected Integer UsuarioID = ZERO;
        protected Integer nUsuarioID
        {
            get { return UsuarioID; }
            set { if (value != null) UsuarioID = value; }
        }

        protected bool geraCadastro(int i)
        {
            ProcedureParameters[] param = new ProcedureParameters[5];

            param[0] = new ProcedureParameters(
                "@PartNumber",
                SqlDbType.VarChar,
                PartNumber[i], 20);
            
            param[1] = new ProcedureParameters(
                "@Conceito",
                SqlDbType.VarChar,
                Conceito[i], 25);

            param[2] = new ProcedureParameters(
                "@Serie",
                SqlDbType.VarChar,
                Serie[i], 40);

            param[3] = new ProcedureParameters(
                "@UsuarioID",
                SqlDbType.Int,
                UsuarioID.intValue());

            param[4] = new ProcedureParameters(
                "@Resultado",
                SqlDbType.Bit,
                DBNull.Value,
                ParameterDirection.InputOutput);

            param[4].Length = 800;

            DataInterfaceObj.execNonQueryProcedure("sp_Conceito_CadastroESD", param);

            // Obt�m o resultado da execu��o.
            return (bool)param[4].Data;
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            string fldresp = "";

            for ( int i = 0; i < DataLen.intValue(); i++ )
            {
                bool response = geraCadastro(i);

                if (response && fldresp == "")
                    fldresp = "Um ou mais produtos n�o puderam ser cadastrados.";
            }

            WriteResultXML(DataInterfaceObj.getRemoteData(
                "select '" + fldresp + "' as fldresp"
            ));
        }
    }
}
