using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.pcm.conceitos.serverside
{
	public partial class sincroniza : System.Web.UI.OverflyPage
	{
		private Integer conceitoID;
		protected Integer nConceitoID
		{
			get { return conceitoID; }
			set { conceitoID = value; }
		}

		private string delIncl;
		protected string sDelIncl
		{
			get { return delIncl; }
			set { delIncl = value; }
		}

		private string sql = "";
		protected string SQL
		{
			get
			{
				if (sql != "")
				{
					return sql;
				}

				if(sDelIncl.ToUpper() == "I")
				{
					sql = "INSERT INTO Conceitos_Caracteristicas (ProdutoID,Checado,CaracteristicaID,Valor) " +
                        "SELECT " + nConceitoID + " AS FldID,0 AS Checado, a.SujeitoID AS CaracteristicaID,a.ValorDefault AS Valor " +
                        "FROM RelacoesConceitos a WITH(NOLOCK), Conceitos b WITH(NOLOCK) " + 
						"WHERE b.ConceitoID = " + nConceitoID + " AND b.ProdutoID = a.ObjetoID AND " + 
						"a.EstadoID = 2 AND a.TipoRelacaoID = 43 AND a.SujeitoID NOT IN " +
                        "(SELECT CaracteristicaID FROM Conceitos_Caracteristicas WITH(NOLOCK) WHERE ProdutoID=" + nConceitoID + ")";
				}
				else if (sDelIncl.ToUpper() == "D")
				{
					sql = "DELETE Conceitos_Caracteristicas " +
                        "WHERE ProdutoID = " + nConceitoID + " AND (Checado = 0 OR Checado IS NULL) AND (dbo.fn_Produto_CaracteristicaBit(ProdutoID, CaracteristicaID, 5) = 0) ";
				}

				return sql;
			}
		}

		protected override void PageLoad(object sender, EventArgs e)
		{
			int fldresp;

			if (SQL != "") fldresp = DataInterfaceObj.ExecuteSQLCommand(SQL);
			else fldresp = 0;

			WriteResultXML(DataInterfaceObj.getRemoteData("select " + fldresp + " as fldresp"));
		}
	}
}
