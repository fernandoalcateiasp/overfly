using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.pcm.conceitos.serverside
{
	public partial class copiacaracteristicas : System.Web.UI.OverflyPage
	{
        private string mensagem;
		
        private Integer produtoFromID;
		protected Integer nProdutoFromID
		{
			get { return produtoFromID; }
			set { produtoFromID = value; }
		}

		private Integer produtoToID;
		protected Integer nProdutoToID
		{
			get { return produtoToID; }
			set { produtoToID = value; }
		}

		protected void VerificaProdutoClonar()
        {
            ProcedureParameters[] procparam = new ProcedureParameters[3];

            procparam[0] = new ProcedureParameters(
                "@ProdutoOrigemID",
                SqlDbType.Int,
                nProdutoFromID.ToString());
            procparam[1] = new ProcedureParameters(
                "@ProdutoDestinoID",
                SqlDbType.Int,
                nProdutoToID.ToString());
            procparam[2] = new ProcedureParameters(
                "@MensagemErro",
                SqlDbType.VarChar,
                DBNull.Value,
                ParameterDirection.InputOutput);
            procparam[2].Length = 8000;

            // Executa a procedure.
            DataInterfaceObj.execNonQueryProcedure(
                "sp_Produto_ClonarCaracteristicas",
                procparam
            );

            // Obt�m o resultado da execu��o.
            mensagem = procparam[2].Data.ToString();
        }

		protected override void PageLoad(object sender, EventArgs e)
		{
            VerificaProdutoClonar();
            WriteResultXML(DataInterfaceObj.getRemoteData("select '" + mensagem + "' as fldresp"));
		}
	}
}
