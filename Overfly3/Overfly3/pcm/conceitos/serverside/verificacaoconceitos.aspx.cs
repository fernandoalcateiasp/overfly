using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.pcm.conceitos.serverside
{
	public partial class verificacaoconceitos : System.Web.UI.OverflyPage
	{
        private int response;
        private string mensagem;

		private Integer conceitoID;
		protected Integer nConceitoID
		{
			get { return conceitoID; }
			set { conceitoID = value; }
		}

		private Integer estadoDeID;
		protected Integer nEstadoDeID
		{
			get { return estadoDeID; }
			set { estadoDeID = value; }
		}

		private Integer estadoParaID;
		protected Integer nEstadoParaID
		{
			get { return estadoParaID; }
			set { estadoParaID = value; }
		}

		protected void ConceitoVerifica()
		{
			ProcedureParameters[] param = new ProcedureParameters[6];

            param[0] = new ProcedureParameters(
                "@ConceitoID", 
                SqlDbType.Int, 
                nConceitoID.intValue());
            param[1] = new ProcedureParameters(
                "@EstadoDeID", 
                SqlDbType.Int, 
                nEstadoDeID.intValue());
				
            param[2] = new ProcedureParameters(
                "@EstadoParaID", 
                SqlDbType.Int, 
                nEstadoParaID.intValue());

            param[3] = new ProcedureParameters(
                "@EmpresaID",
                SqlDbType.Int,
                DBNull.Value);

            param[4] = new ProcedureParameters(
                "@Resultado", 
                SqlDbType.Int, 
                DBNull.Value, 
                ParameterDirection.InputOutput);
			    
            param[5] = new ProcedureParameters(
                "@Mensagem", 
                SqlDbType.VarChar, 
                DBNull.Value, 
                ParameterDirection.InputOutput);

			param[5].Length = 8000;
			
            DataInterfaceObj.execNonQueryProcedure("sp_Conceito_Verifica", param);
            
            // Obt�m o resultado da execu��o.
            response = int.Parse(param[4].Data.ToString());
            mensagem = param[5].Data.ToString();
		}

		protected override void PageLoad(object sender, EventArgs e)
		{
			ConceitoVerifica();

            WriteResultXML(
                DataInterfaceObj.getRemoteData("select " + response + " as Resultado, '" + mensagem + "' as Mensagem")
            );

		}
	}
}
