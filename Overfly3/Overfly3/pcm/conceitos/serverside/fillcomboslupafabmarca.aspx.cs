using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.pcm.conceitos.serverside
{
	public partial class fillcomboslupafabmarca : System.Web.UI.OverflyPage
	{
		private string comboID;
		protected string sComboID
		{
			get { return comboID; }
			set { comboID = value; }
		}

		private string param1;
		protected string sParam1
		{
			get { return param1; }
			set { param1 = value; }
		}

		private string param2;
		protected string sParam2
		{
			get { return param2; }
			set { param2 = value; }
		}

		protected string SQL
		{
			get
			{
				string sql = "select -1 as fldID, '' AS fldName";

				if (sComboID.Equals("selFabricanteID"))
				{
					sql = "SELECT b.PessoaID AS fldID, b.Fantasia AS fldName " +
                             "FROM RelacoesPesCon a WITH(NOLOCK), Pessoas b WITH(NOLOCK) " +
							 "WHERE a.EstadoID=2 AND a.TipoRelacaoID = 62 " +
							 "AND a.ObjetoID=" + sParam1 + " " +
							 "AND a.SujeitoID = b.PessoaID " +
							 "ORDER BY fldName";
				}
				else if (sComboID.Equals("selMarcaID"))
				{
					sql = "SELECT b.ConceitoID AS fldID, b.Conceito AS fldName, a.Ordem " +
                             "FROM RelacoesPesCon a WITH(NOLOCK),Conceitos b WITH(NOLOCK) " +
							 "WHERE a.EstadoID = 2 AND a.TipoRelacaoID = 63 " +
							 "AND a.SujeitoID=" + sParam1 + " " +
							 "AND a.ObjetoID = b.ConceitoID " +
							 "ORDER BY a.Ordem";
				}
				else if (sComboID.Equals("selLinhaProdutoID"))
				{
					sql = "SELECT a.ConceitoID AS fldID, a.Conceito AS fldName, a.Conceito AS Ordem " +
                             "FROM Conceitos a WITH(NOLOCK) " +
							 "WHERE (a.EstadoID = 2 AND a.ConceitoID = 4001 AND a.TipoConceitoID = 310) " +
							 "UNION " +
							 "SELECT b.ConceitoID AS fldID, b.Conceito AS fldName, b.Conceito AS Ordem " +
                             "FROM RelacoesConceitos a WITH(NOLOCK),Conceitos b WITH(NOLOCK) " +
							 "WHERE a.EstadoID = 2 AND a.TipoRelacaoID = 50 " +
							 "AND a.ObjetoID=" + sParam1 + " " +
							 "AND a.SujeitoID = b.ConceitoID " +
							 "ORDER BY Ordem";
				}

				return sql;
			}
		}

		protected override void PageLoad(object sender, EventArgs e)
		{
			WriteResultXML(DataInterfaceObj.getRemoteData(SQL));
		}
	}
}
