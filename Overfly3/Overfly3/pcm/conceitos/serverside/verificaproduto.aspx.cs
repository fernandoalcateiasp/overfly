using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.pcm.conceitos.serverside
{
	public partial class verificaproduto : System.Web.UI.OverflyPage
	{
		private Integer conceitoID;
		protected Integer nConceitoID
		{
			get { return conceitoID; }
			set { conceitoID = value; }
		}

		private Integer produtoID;
		protected Integer nProdutoID
		{
			get { return produtoID; }
			set { produtoID = value; }
		}

		private Integer marcaID;
		protected Integer nMarcaID
		{
			get { return marcaID; }
			set { marcaID = value; }
		}

		private string modelo;
		protected string sModelo
		{
			get { return modelo; }
			set { modelo = value; }
		}

        private string partNumber;
        protected string sPartNumber
        {
            get { return partNumber; }
            set { partNumber = value; }
        }

		protected string SQL
		{
			get
			{
				string sql =
                "SELECT " +
                    "ISNULL((SELECT COUNT(1) " +
                        "FROM Conceitos a WITH(NOLOCK) " +
                        "WHERE ConceitoID <> " + nConceitoID + " " +
                            "AND ProdutoID = " + nProdutoID + " " +
                            "AND MarcaID = " + nMarcaID + " " +
                            "AND Modelo = '" + sModelo + "'), 0) AS QuantidadeModelo, " +
                    "ISNULL((SELECT COUNT(1) " +
                        "FROM Conceitos a WITH(NOLOCK) " +
                        "WHERE ConceitoID <> " + nConceitoID + " " +
                            "AND ProdutoID = " + nProdutoID + " " +
                            "AND MarcaID = " + nMarcaID + " " +
                            "AND PartNumber = '" + sPartNumber + "'), 0) AS QuantidadePartNumber ";

				return sql;
			}
		}

		protected override void PageLoad(object sender, EventArgs e)
		{
			WriteResultXML(DataInterfaceObj.getRemoteData(SQL));
		}
	}
}
