using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.pcm.conceitos.serverside
{
	public partial class sincronizaconfiguracao : System.Web.UI.OverflyPage
	{
		private Integer conceitoID;
		protected Integer nConceitoID
		{
			get { return conceitoID; }
			set { conceitoID = value; }
		}


		protected override void PageLoad(object sender, EventArgs e)
		{
			DataInterfaceObj.execNonQueryProcedure("sp_Produto_SincronizaConfiguracao",
				new ProcedureParameters[]
				{
					new ProcedureParameters("@ProdutoID", SqlDbType.Int, nConceitoID.intValue())
				}
			);

			WriteResultXML(DataInterfaceObj.getRemoteData("select '' as Resultado"));
		}
	}
}
