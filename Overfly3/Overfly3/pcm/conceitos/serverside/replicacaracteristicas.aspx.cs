using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.pcm.conceitos.serverside
{
    public partial class replicacaracteristicas : System.Web.UI.OverflyPage
	{
        private Integer ProdutoID;
        protected Integer nProdutoID
        {
            get { return ProdutoID; }
            set { ProdutoID = value; }
        }

        protected void replicacacteristicas()
        {
            ProcedureParameters[] parameters = new ProcedureParameters[2];
            parameters[0] = new ProcedureParameters(
                "@ProdutoID",
                System.Data.SqlDbType.Int,
                (ProdutoID != null ? (Object)ProdutoID.intValue() : System.DBNull.Value));

            parameters[1] = new ProcedureParameters(
                "@CaracteristicaID",
                System.Data.SqlDbType.Int,
                //(ProdutoID != null ? (Object)ProdutoID.intValue() : DBNull.Value)
                System.DBNull.Value);

            DataInterfaceObj.execNonQueryProcedure(
                "sp_Produto_ReplicarCaracteristicas",
                parameters);
        }

		protected override void PageLoad(object sender, EventArgs e)
		{
            replicacacteristicas();

            WriteResultXML(DataInterfaceObj.getRemoteData("select 0 as fldresp"));
		}
	}
}
