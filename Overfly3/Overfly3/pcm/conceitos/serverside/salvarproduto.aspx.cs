using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.pcm.conceitos.serverside
{
	public partial class salvarproduto : System.Web.UI.OverflyPage
	{
		private static Integer ZERO = new Integer(0);
		private static Integer[] EMPTY = new Integer[0];

        protected Integer DataLen = ZERO;
        protected Integer nDataLen
        {
            get { return DataLen; }
            set { if (value != null) DataLen = value; }
        }

		protected Integer[] ID = EMPTY;
		protected Integer[] nID
		{
			get { return ID; }
			set { if(value != null) ID = value; }
		}

		protected Integer UsuarioID = ZERO;
		protected Integer nUsuarioID
		{
			get { return UsuarioID; }
			set { if(value != null) UsuarioID = value; }
		}
                
        protected Integer[] FormID = EMPTY;
		protected Integer[] nFormID
		{
			get { return FormID; }
			set { if(value != null) FormID = value; }
		}

		protected Integer[] SubFormID = EMPTY;
		protected Integer[] nSubFormID
		{
			get { return SubFormID; }
			set { if(value != null) SubFormID = value; }
		}
        
		protected string[] Modelo;
		protected string[] sModelo
		{
			get { return Modelo; }
			set { if(value != null) Modelo = value; }
		}

		protected string[] PartNumber;
		protected string[] sPartNumber
		{
			get { return PartNumber; }
			set { if(value != null) PartNumber = value; }
		}

        protected string[] Serie;
        protected string[] sSerie
        {
            get { return Serie; }
            set { if (value != null) Serie = value; }
        }
        /*
        protected string[] DescricaoAutomatica;
        protected string[] sDescricaoAutomatica
        {
            get { return DescricaoAutomatica; }
            set { if (value != null) DescricaoAutomatica = value; }
        }*/

        protected string[] Internacional;
        protected string[] sInternacional
        {
            get { return Internacional; }
            set { if (value != null) Internacional = value; }
        }

        protected string[] Descricao;
		protected string[] sDescricao
		{
			get { return Descricao; }
			set { if(value != null) Descricao = value; }
		}

        protected string[] DescricaoComplementar;
        protected string[] sDescricaoComplementar
        {
            get { return DescricaoComplementar; }
            set { if (value != null) DescricaoComplementar = value; }
        }

        protected string[] DescricaoAbreviada;
        protected string[] sDescricaoAbreviada
        {
            get { return DescricaoAbreviada; }
            set { if (value != null) DescricaoAbreviada = value; }
        }

        protected string[] BNDES;
        protected string[] sBNDES
        {
            get { return BNDES; }
            set { if (value != null) BNDES = value; }
        }
        
        protected string SQL
		{
			get
			{
				string sql = "";
				int i;

                for (i = 0; i < DataLen.intValue(); i++)
                {
                    sql += " UPDATE Conceitos SET Modelo = '" + Modelo[i] + "', PartNumber = '" + PartNumber[i] + "', Serie = '" + Serie[i] + "', " +
                        /*"DescricaoAutomatica = " + DescricaoAutomatica[i] + ", " +*/ "Internacional = " + Internacional[i] + ", Descricao = '" + Descricao[i] + "', " + 
                        "DescricaoComplementar = " + ((DescricaoComplementar[i] == "null") ? "NULL" : "'" + DescricaoComplementar[i] + "'") +
                        ", CodigoBNDES = " + ((BNDES[i] == "null") ? "NULL" : "'" + BNDES[i] + "'") +  
                        ", Conceito = '" + DescricaoAbreviada[i] + "', UsuarioID = " + UsuarioID + " WHERE ConceitoID = " + ID[i];
				}
				
				return sql;
			}
		}

        protected override void PageLoad(object sender, EventArgs e)
        {
            int fldresp = DataInterfaceObj.ExecuteSQLCommand(SQL);

            WriteResultXML(DataInterfaceObj.getRemoteData(
                "select " + fldresp + " as fldresp"
            ));
        }
    }
}
