using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.pcm.conceitos.serverside
{
	public partial class valordefaultcaracteristica : System.Web.UI.OverflyPage
	{
		private Integer relacaoID;
		protected Integer nRelacaoID
		{
			get { return relacaoID; }
			set { relacaoID = value; }
		}

		private string valor;
		protected string sValor
		{
			get { return valor; }
			set { valor = value; }
		}

		protected int updateRelacoesConceitos()
		{
			string sql = "UPDATE RelacoesConceitos SET ValorDefault = '" + sValor + "' " +
				"WHERE (RelacaoID = " + nRelacaoID + ")";

			if (nRelacaoID != null && nRelacaoID.intValue() > 0)
			{
				return DataInterfaceObj.ExecuteSQLCommand(sql);
			}

			return 0;
		}

		protected override void PageLoad(object sender, EventArgs e)
		{
			WriteResultXML(DataInterfaceObj.getRemoteData(
				"select " + updateRelacoesConceitos() + " as fldresp"
			));
		}
	}
}
