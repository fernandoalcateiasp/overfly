using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.pcm.conceitos.serverside
{
	public partial class fillstrconceito : System.Web.UI.OverflyPage
	{
		private Integer produtoID;
		protected Integer Item0
		{
			get { return produtoID; }
			set { produtoID = value; }
		}

		private Integer marcaID;
		protected Integer Item1
		{
			get { return marcaID; }
			set { marcaID = value; }
		}

		protected string SQL
		{
			get
			{
				string sql = "SELECT TOP 1 a.ConceitoAbreviado AS Produto, b.ConceitoAbreviado AS Marca " +
                             "FROM Conceitos a WITH(NOLOCK), Conceitos b WITH(NOLOCK) " +
							 "WHERE a.ConceitoID =  " + produtoID +
							 " AND b.ConceitoID = " + marcaID; 

				return sql;
			}
		}

		protected override void PageLoad(object sender, EventArgs e)
		{
			WriteResultXML(DataInterfaceObj.getRemoteData(SQL));
		}
	}
}
