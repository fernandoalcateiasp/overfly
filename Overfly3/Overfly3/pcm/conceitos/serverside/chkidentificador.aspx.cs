using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.pcm.conceitos.serverside
{
	public partial class chkidentificador : System.Web.UI.OverflyPage
	{
		private string identificador;
		protected string sIdentificador
		{
			get { return identificador; }
			set { identificador = value; }
		}

		private Integer regID;
		protected Integer nRegID
		{
			get { return regID; }
			set { regID = value; }
		}

		protected string SQL
		{
			get
			{
				string sql =
					"SELECT DISTINCT ProdutoID as fldresp " +
                    "FROM Conceitos_Identificadores WITH(NOLOCK) " +
					"WHERE Identificador = '" + sIdentificador + "'";

				if(nRegID != null) sql += " AND ConIdentificadorID <> " + nRegID;

				return sql;
			}
		}

		protected override void PageLoad(object sender, EventArgs e)
		{
			WriteResultXML(DataInterfaceObj.getRemoteData(SQL));
		}
	}
}
