/********************************************************************
js_especificsup.js

Library javascript de funcoes para abertura de objetos rds dos sups.
Especifica para o form de conceitos
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:
    setSqlToDso(dso,nID,sCompleteFunction,lByProprietario)
    setup_dsoSup01FromPesq(dso)

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Esta funcao seta o sql do dso para buscar o registro a ser mostrado no detalhe,
vai ao banco de dados no servidor e chama a funcao de ondatasetcomplete de acordo 
com o parametro recebido.
           
Parametros: 
dso                        -> dso a ser aberto
nID                        -> id do usuario logado se registro novo
                           -> ou id do registro
sCompleteFunction          -> funcao de ondatasetcomplete a ser disparada na volta do servidor
lByProprietario(opcional)  -> se e um novo registro lByProprietario e true, pq qdo e um novo
                              registro o filtro deve ser por proprietarioID e nao pelo identity
                              do registro

Retorno:
nulo
********************************************************************/
function setSqlToDso(dso, nID, sCompleteFunction, newRegister)
{
    var sSQL;

    //@@
    var nUserID = getCurrUserID();

	var nFormID = window.top.formID;
	
	var nSubFormID = window.top.subFormID;

	var empresaData = getCurrEmpresaData();

    if (newRegister == true)
    {
        sSQL = 'SELECT TOP 1 *, ' +
			   'dbo.fn_Produto_TemImagem(ConceitoID) AS TemImagem, ' +
			   'dbo.fn_Imagem_Resumo(' + nFormID + ', ' + nSubFormID + ', ConceitoID) AS ImagemResumo, ' +
               'Prop1 = CASE ProprietarioID ' +
               'WHEN ' + nID + ' THEN 1 ' +
               'ELSE 0 ' +
               'END, ' +
               'Prop2 = (CASE WHEN ProprietarioID= ' + nID + ' THEN 1 ' +
               'WHEN AlternativoID= ' + nID + ' THEN 1 ' +
               'WHEN (SELECT COUNT (*) FROM RelacoesPessoas WITH(NOLOCK) ' + 
               'WHERE EstadoID=2 AND TipoRelacaoID=34 AND SujeitoID= ' + nID + ' AND ObjetoID=Conceitos.AlternativoID)>0 THEN 1 ' +
               'ELSE 0 ' +
               'END), ' +
               'NULL AS DescricaoFiscal, ' +
               '0 AS Conformidade_Conf, ' +
               '0 AS Conformidade_Carac, ' +
               '0 AS Conformidade_AV, ' +
               '0 AS Conformidade_Imagens, ' +
               '0 AS Conformidade_PM, ' +
               '0 AS Conformidade_Ident, ' +
               '0 AS Conformidade_Espec, ' +
               '0 AS TemKardex ' +
               'FROM Conceitos WITH(NOLOCK) ' +
               'WHERE UsuarioID = ' + nID + ' ';

        // ID Calculado pela Trigger
        if (window.top.registroID_Type == 16)
            sSQL += 'AND ' + glb_sFldTipoRegistroName + ' = ' + glb_ConceitosTipoRegID + ' ';
               
        sSQL += 'ORDER BY ConceitoID DESC';
    }            
    else
        sSQL = 'SELECT *, ' +
               'dbo.fn_Localidade_Beneficio(dbo.fn_Pessoa_Localidade(FabricanteID, 1, NULL,NULL), 2307, NULL) AS LocalidadeTemPPB, ' +
		       'dbo.fn_Localidade_Beneficio(dbo.fn_Pessoa_Localidade(FabricanteID, 3, NULL,NULL), 2308, NULL) AS LocalidadeTemSUFRAMA, ' +
			   'dbo.fn_Produto_TemImagem(ConceitoID) AS TemImagem, ' +
			   'dbo.fn_Imagem_Resumo(' + nFormID + ', ' + nSubFormID + ', ConceitoID) AS ImagemResumo, ' +
               'Prop1 = CASE ProprietarioID ' +
               'WHEN ' + nUserID + ' THEN 1 ' +
               'ELSE 0 ' +
               'END, ' +
               'Prop2 = (CASE WHEN ProprietarioID= ' + nUserID + ' THEN 1 ' +
               'WHEN AlternativoID= ' + nUserID + ' THEN 1 ' +
               'WHEN (SELECT COUNT (*) FROM RelacoesPessoas WITH(NOLOCK) ' + 
               'WHERE EstadoID=2 AND TipoRelacaoID=34 AND SujeitoID= ' + nUserID + ' AND ObjetoID=Conceitos.AlternativoID)>0 THEN 1 ' +
               'ELSE 0 ' +
               'END), ' +
               '(CASE TipoConceitoID WHEN 303 THEN dbo.fn_Produto_Descricao2(ConceitoID, NULL, 11) ELSE NULL END) AS ProdutoDescricao, ' +
               'dbo.fn_Produto_Descricao2(ConceitoID, ' + empresaData[8] + ', 14) AS DescricaoFiscal, ' +
               'dbo.fn_Produto_Conformidade(ConceitoID,0) AS Conformidade_Conf, ' +
               'dbo.fn_Produto_Conformidade(ConceitoID,1) AS Conformidade_Carac, ' +
               'dbo.fn_Produto_Conformidade(ConceitoID,2) AS Conformidade_AV, ' +
               'dbo.fn_Produto_Conformidade(ConceitoID,3) AS Conformidade_Imagens, ' +
               'dbo.fn_Produto_Conformidade(ConceitoID,4) AS Conformidade_PM, ' +
               'dbo.fn_Produto_Conformidade(ConceitoID,5) AS Conformidade_Ident, ' +
               'dbo.fn_Produto_Conformidade(ConceitoID,6) AS Conformidade_Espec, ' +
               'dbo.fn_TemKardex(ConceitoID) AS TemKardex, ' +
               'dbo.fn_familia_produto(ConceitoID) AS Familia ' +
               'FROM Conceitos WITH(NOLOCK) ' +
               'WHERE ConceitoID = ' + nID + ' ORDER BY ConceitoID DESC';

    setConnection(dso);
    
    dso.SQL = sSQL;
    dso.ondatasetcomplete = eval(sCompleteFunction);
    dso.refresh();
    
    return null;
}

/********************************************************************
Esta funcao configura o dsoSup01 para inclusao de novo registro
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function setup_dsoSup01FromPesq(dso)
{
    setConnection(dso);
        
    var sql;

	var nFormID = window.top.formID;

	var nSubFormID = window.top.subFormID;
    
    sql = 'SELECT *, ' +
	      'dbo.fn_Produto_TemImagem(ConceitoID) AS TemImagem, ' +
  	      'dbo.fn_Imagem_Resumo(' + nFormID + ', ' + nSubFormID + ', ConceitoID) AS ImagemResumo, ' +
          '0 AS Prop1, 0 AS Prop2, 0 AS TemImagem, ' +
          'NULL AS DescricaoFiscal, ' +
          '0 AS Conformidade_Conf ' +
          'FROM Conceitos WITH(NOLOCK) WHERE ConceitoID = 0';
    
    dso.SQL = sql;          
}              
