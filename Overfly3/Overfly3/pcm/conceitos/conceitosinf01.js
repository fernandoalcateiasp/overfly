/********************************************************************
conceitosinf01.js

Library javascript para o conceitosinf01.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

var __inFTimer_Conceitos = null;

// Dados do registro corrente .SQL (no minimo: regID, observacoes, propID e alternativoID) 
var dso01JoinSup = new CDatatransport("dso01JoinSup");
// Dados de combos estaticos do form .URL 
var dsoEstaticCmbs = new CDatatransport("dsoEstaticCmbs");
// Dados de combos dinamicos do form .URL 
var dsoDynamicCmbs = new CDatatransport("dsoDynamicCmbs");
// Checa se ja existe identificador semelhante .URL 
var dsoChkIdentificador = new CDatatransport("dsoChkIdentificador");
// Dados de um grid .SQL 
var dso01Grid = new CDatatransport("dso01Grid");
var dsoCmb01Grid_01 = new CDatatransport("dsoCmb01Grid_01");
var dsoCmb01Grid_02 = new CDatatransport("dsoCmb01Grid_02");
var dsoCmb01Grid_03 = new CDatatransport("dsoCmb01Grid_03");
var dsoCmb01Grid_04 = new CDatatransport("dsoCmb01Grid_04");
var dsoCmb01Grid_05 = new CDatatransport("dsoCmb01Grid_05");
var dso01GridLkp = new CDatatransport("dso01GridLkp");
var dso02GridLkp = new CDatatransport("dso02GridLkp");
var dso03GridLkp = new CDatatransport("dso03GridLkp");
var dso04GridLkp = new CDatatransport("dso04GridLkp");
var dsoParallelGrid = new CDatatransport("dsoParallelGrid");
var dsoperfilUsuario = new CDatatransport("dsoperfilUsuario");

// Dados de um grid .URL 
var dso01PgGrid = new CDatatransport("dso01PgGrid");

// Dados do DSO da fun��o sincronizaCaracteristicas 
var dsoExecSincroniza = new CDatatransport("dsoExecSincroniza");
var dsoValorDefault = new CDatatransport("dsoValorDefault");

// Dados do DSO da fun��o sincronizaConfiguracao 
var dsoSincronizaConfiguracao = new CDatatransport("dsoSincronizaConfiguracao");

// Descricao dos estados atuais dos registros do grid corrente .SQL         
var dsoStateMachineLkp = new CDatatransport("dsoStateMachineLkp");

// Dados do combo de filtros da barra inferior .SQL         
var dsoFiltroInf = new CDatatransport("dsoFiltroInf");

// Dados do combo de filtros usado nos grids de relacoes         
var dsoCmbRel = new CDatatransport("dsoCmbRel");

// Dados dos combos de proprietario e alternativo
var dsoCmbsPropAlt = new CDatatransport("dsoCmbsPropAlt");

// Dados da empresa Logada
var glb_aEmpresaData = getCurrEmpresaData();

var glb_PermiteSuporte;
var glb_PermiteHomologacao;
var glb_PermiteLogistica;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:
adjustElementsInDivsNonSTD()
putSpecialAttributesInControls()
optChangedInCmb(cmb)
prgServerInf(btnClicked)
prgInterfaceInf(btnClicked, pastaID, dso)
finalOfInfCascade(btnClicked)
treatsControlsBars(btnClicked, folderID, tipoCtlrsPasta, currDSO)
btnLupaClicked(btnClicked)
addedLineInGrid(folderID, grid, nLineInGrid, dso)
btnBarClicked(controlBar, btnClicked)
btnBarNotEspecClicked(controlBar, btnClicked)
modalInformForm(idElement, param1, param2)
btnAltPressedInGrid( folderID )
btnAltPressedInNotGrid( folderID )
fillCmbFiltsInfAutomatic(folderID)
selPastaChanged(optText, optValue, optIndex, nQtyGridsInFolder)
frameIsAboutToApplyRights(folderID)
    
FUNCOES DA MAQUINA DE ESTADO:
stateMachOpened( currEstadoID )
stateMachClosed( oldEstadoID, currEstadoID )

EVENTOS DO GRID FG
fg_AfterRowColChange_Prg()
fg_DblClick_Prg()
fg_ChangeEdit_Prg()
fg_ValidateEdit_Prg()
fg_BeforeRowColChange_Prg()
fg_EnterCell_Prg()
fg_MouseUp_Prg()
fg_MouseDown_Prg()
fg_BeforeEdit_Prg()

********************************************************************/

/********************************************************************

// IMPLEMENTACAO DAS FUNCOES

//@@ todas as funcoes abaixo sao particulares de cada form
// conforme o form algumas funcoes devem ser removidas
// e outras serao acrescentadas
********************************************************************/

function window_onload() {
    // Ze em 17/03/08
    dealWithGrid_Load();

    // Funcoes complementares da automacao
    windowOnLoad_1stPart();

    //@@ Combos estaticos. Array bidimensional de combos ids e campo indice
    // na tabela, identificador do combo.
    // O array deve ser null se nao tem combos estaticos
    glb_aStaticCombos = null;

    //@@ Combos estaticos em grids.
    // Array bidimensional da pasta id e array bidimensional
    // do indice das colunas
    // (a contar de zero), id do dso dos dados do combo,
    // nome do campo da tabela visivel no combo e
    // nome do campo do grid que linka o grid com o combo.
    // O array deve ser null se nenhum grid tem combos estaticos
    // Notacao para combo multicoluna:
    // [ [20012, [[1, 'dsoCmb01Grid_01', '*ItemAbreviado|ItemMasculino','ItemID']]] ] 

    glb_aGridStaticCombos = [[20010, [[0, 'dso01GridLkp', '', ''],
                                       [0, 'dso02GridLkp', '', ''],
                                       [0, 'dso03GridLkp', '', ''],
                                       [0, 'dso04GridLkp', '', '']]],
                              [21011, [[0, 'dso01GridLkp', '', ''],
                                       [0, 'dso02GridLkp', '', ''],
                                       [0, 'dso03GridLkp', '', '']]],
                              [21013, [[0, 'dso01GridLkp', '', ''],
                                       [0, 'dso02GridLkp', '', ''],
                                       [3, 'dsoCmb01Grid_05', 'fldName', 'fldID']]],
                              [21014, [[0, 'dsoStateMachineLkp', '', '']]],
                              [21015, [[0, 'dso01GridLkp', '', ''],
                                       [0, 'dsoStateMachineLkp', '', '']]],
                              [21016, [[0, 'dso01GridLkp', '', ''],
                                       [0, 'dsoStateMachineLkp', '', '']]],
                              [21017, [[0, 'dsoCmb01Grid_01', 'RecursoFantasia', 'RecursoID']]],
                              [21018, [[0, 'dso01GridLkp', '', ''],
									   [0, 'dso02GridLkp', '', '']]],
						      [21020, [[0, 'dsoCmb01Grid_01', 'ItemMasculino', 'ItemID']]],
						      [21079, [[0, 'dsoCmb01Grid_01', 'ItemMasculino', 'ItemID']]],
						      [21021, [[0, 'dsoCmb01Grid_01', 'fldName', 'fldID'],
						               [2, 'dsoCmb01Grid_02', 'fldName', 'fldID'],
						               [3, 'dsoCmb01Grid_03', 'fldName', 'fldID']]],
                              [21022, [[0, 'dsoCmb01Grid_01', 'fldName', 'fldID'],
                                       [1, 'dsoCmb01Grid_02', 'fldName', 'fldID']]],
                              [21023, [[0, 'dso01GridLkp', '', ''],
                                       [2, 'dsoCmb01Grid_01', 'fldName', 'fldID'],
                                       [3, 'dsoCmb01Grid_02', 'fldName', 'fldID'],
                                       [4, 'dsoCmb01Grid_03', 'fldName', 'fldID'],
                                       [6, 'dsoCmb01Grid_04', 'fldName', 'fldID']]],
                              [21024, [[0, 'dso01GridLkp', '', ''],
                                       [0, 'dsoStateMachineLkp', '', '']]],
                              [21025, [[0, 'dso01GridLkp', '', ''],
                                       [1, 'dsoCmb01Grid_01', 'Imposto', 'ImpostoID']]],
                              [21026, [[0, 'dso01GridLkp', '', ''],
                                       [1, 'dsoCmb01Grid_01', 'ItemMasculino', 'ItemID']]],
                              [21028, [[1, 'dsoCmb01Grid_01', '*Conta|ContaID', 'ContaID']]],
                              [21029, [[1, 'dsoCmb01Grid_01', 'ItemMasculino', 'TipoBeneficioID'],
                                       [0, 'dsoStateMachineLkp', '', ''],
                                       [2, 'dso01GridLkp', '', '']]]];

    //@@ Linka os divs com os correspondentes subforms
    // criando o atributo sfsGrupo nos divs.
    // Os infs nao tem div principal e todos os divs sao secundarios.
    // Parametros:
    // 1o par: id do div principal ou null se nao tem div principal
    // 2opar: array de divs ids dos divs secundarios
    // ou null se nao tem divs secundarios
    // 3o par: array de xxx_ID que identifica o grupo contido no div
    // ou null se o form nao tem div secundarios.
    // Esta funcao tageia o div principal e preenche array
    // dos divs secundarios do arquivo guardando na variavel
    // global do arquivo: glb_aDIVS
    linkDivsAndSubForms(null, ['divInf01_01',
                               'divInf01_02',
                               'divInf01_03',
                               'divInf01_04',
                               'divInf01_05'],
                               [20008, 20009,
                               [20010, 21009, 21011, 21012, 21013, 21014, 21015, 21016, 21017, 21018, 21020, 21021, 21022, 21023, 21024, 21025, 21026, 21028, 21029, 21079, 21081, 21050], //Adicionado as pastas 21023, 21024 e 21025. BJBN 25/03/2011 21026 RRF 20/07/2011. 21027 21/12/2011
                               21010, 21019]);

    // Funcoes complementares da automacao
    windowOnLoad_2ndPart();
}

function setupPage() {
    //@@ Ajustar os divs
    // Nada a codificar

    adjustDivs('inf', [[1, 'divInf01_01'],
                      [1, 'divInf01_02'],
                      [1, 'divInf01_03'],
                      [1, 'divInf01_04'],
                      [1, 'divinf01_05']]);

    // txtHomologacao
    elem = window.document.getElementById('txtHomologacao');
    //elem.disabled = false;
    with (elem.style) {
        left = 10;
        top = 10;
        width = MAX_FRAMEWIDTH - (ELEM_GAP * 2);
        height = (MAX_FRAMEHEIGHT / 2) - (ELEM_GAP * 7);
    }

    elem = window.document.getElementById('txtEspeficicaoesTecnicas');
    //elem.disabled = false;
    with (elem.style) {
        left = 10;
        top = 10;
        width = MAX_FRAMEWIDTH - (ELEM_GAP * 2);
        height = (MAX_FRAMEHEIGHT / 2) - (ELEM_GAP * 7);
    }

    perfilUsuario();
}

// EVENTOS DO GRID FG ***********************************************
// Nota - sao disparados pelo frame work apos suas operacoes

function fg_AfterRowColChange_Prg() {

}

function fg_DblClick_Prg() {
    // Pasta de Caracteristicas
    if ((keepCurrFolder() == 21011) && (!fg.Editable))
        sendJSCarrier(getHtmlId(), 'SHOWRELCONCEITOS', new Array(glb_aEmpresaData[0], fg.TextMatrix(fg.Row, getColIndexByColKey(fg, '^CaracteristicaID^dso02GridLkp^ConceitoID^RelacaoID*'))));
    // Se for pasta relacoes 
    else if ((keepCurrFolder() == 21015) && (!fg.Editable))
        sendJSCarrier(getHtmlId(), 'SHOWRELCONCEITOS', new Array(glb_aEmpresaData[0], fg.TextMatrix(fg.Row, 0)));
    else if ((keepCurrFolder() == 21016) && (!fg.Editable))
        sendJSCarrier(getHtmlId(), 'SHOWRELPESCON', new Array(glb_aEmpresaData[0], fg.TextMatrix(fg.Row, 0)));
}

function fg_ChangeEdit_Prg() {

}
function fg_ValidateEdit_Prg() {

}
function fg_BeforeRowColChange_Prg() {

}

function fg_EnterCell_Prg() {

}

function fg_MouseUp_Prg() {

}

function fg_MouseDown_Prg() {

}

function fg_BeforeEdit_Prg() {

}

// FINAL DE EVENTOS DO GRID FG **************************************

/********************************************************************
Funcao disparada pelo frame work.
Ajusta graficamente elementos de divs que nao estao no padrao do
framework.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function adjustElementsInDivsNonSTD() {
    //@@
    // Este form nao tem este caso    
}

/********************************************************************
Funcao disparada pelo frame work.
Coloca atributos em campos para serem manobrados graficamente
(esconde/mostra e habilita desabilita) pelo framework.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function putSpecialAttributesInControls() {
    //@@

    // Este form tem grids com paginacao
    glb_aFoldersPaging = [21015, 21016];
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao recebe todos os onchange de combos do inf e executa as
funcoes apropriadas definidas pelo programador e aqui invocadas. 
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function optChangedInCmb(cmb) {
    var cmbID = cmb.id;

    //@@ Os if e os else if abaixo sao particulares de cada form
    // as operacoes foram executadas manualmente pelo usuario

    // Troca a interface do sup em funcao de item selecionado
    // no combo de tipo.
    // Se for necessario ao programador, repetir o if abaixo
    // definido, antes deste comentario.
    // Nao mexer - Inicio de automacao ==============================
    if (cmbID == 'selProprietario' || cmbID == 'selAlternativo')
        setPropAlt();
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares de servidor
e preenchimentos de combos do inf. Esta funcao e chamada quando
o usuario clica um botao da barra do sup ou inf.
           
Parametros: 
btnClicked    - ultimo botao clicado na barra (superior ou inferior)

Retorno:
nenhum
********************************************************************/
function prgServerInf(btnClicked) {
    //@@
    // Chama funcoes particulares do form que forem necessarias
    // antes de trocar o div
    // Uso avancado, so roda uma vez
    if (dsoCmbRel.recordset.Fields.Count == 0) {
        glb_lastBtnClicked = btnClicked;
        var formID = window.top.formID;  // variavel global do browser filho
        // passa o id do form por parametro
        var strPars = new String();
        strPars = '?';
        strPars += 'formID=';
        strPars += escape(formID.toString());

        dsoCmbRel.URL = SYS_ASPURLROOT + '/serversidegenEx/tiposrelacoes.aspx' + strPars;
        dsoCmbRel.ondatasetcomplete = dsoCmbRelComplete_DSC;
        dsoCmbRel.refresh();
    }
    else
    // Mover esta funcao para os finais retornos de operacoes no
    // banco de dados
        finalOfInfCascade(btnClicked);
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares
do inf, necessarias apos um changeDiv.
           
Parametros: 
btnClicked  - ultimo botao clicado na barra de botoes inferior
pastaID     - id da pasta corrente
dso         - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function prgInterfaceInf(btnClicked, pastaID, dso) {
    //@@
    // Chama funcoes particulares do form que forem necessarias
    // depois de trocar o div
    var nTipoImp;

    showBtnsEspecControlBar('inf', false, [1, 1, 1, 1]);

    // Pasta de Identificadores
    if (pastaID == 21009) {
        showBtnsEspecControlBar('inf', true, [1, 0, 0, 0]);
        tipsBtnsEspecControlBar('inf', ['Procedimento', '', '', '']);
    }
    // Pasta de Homologa��o
    else if (pastaID == 21010) {
        showBtnsEspecControlBar('inf', true, [1, 0, 0, 0]);
        tipsBtnsEspecControlBar('inf', ['Procedimento', '', '', '']);
    }
    // Pasta de Caracteristicas
    else if (pastaID == 21011) {
        showBtnsEspecControlBar('inf', true, [1, 1, 1, 1]);
        tipsBtnsEspecControlBar('inf', ['Setar como default', 'Detalhar Rela��o', 'Clonar Caracter�sticas', 'Alterar Caracter�sticas']);
    }
    // Pasta de Aliquotas tem botao adicional
    else if (pastaID == 21013) {
        nTipoImp = getCurrDataInControl('sup', 'selRegraAliquotaID');
        // Imposto tem regra de Excess�o (Loc Origem,Loc Destino)
        if (nTipoImp == 333) {
            showBtnsEspecControlBar('inf', true, [1, 1, 0, 0]);
            tipsBtnsEspecControlBar('inf', ['Preenche o combo Localidade Origem', '', '', '']);
            tipsBtnsEspecControlBar('inf', ['Preenche o combo Localidade Destino', '', '', '']);
        }
        // Imposto n�o tem regra de Excess�o (Loc Origem)
        else {
            showBtnsEspecControlBar('inf', true, [1, 0, 0, 0]);
            showBtnsEspecControlBar('inf', false, [0, 1, 0, 0]);
            tipsBtnsEspecControlBar('inf', ['Preenche o combo Localidade Origem', '', '', '']);
        }
    }
    else if (pastaID == 21015) {
        showBtnsEspecControlBar('inf', true, [1, 1, 1, 1]);
        tipsBtnsEspecControlBar('inf', ['Pesquisar', 'Preenche combo', 'Detalhar Rela��o', 'Incluir Rela��o']);
    }
    else if (pastaID == 21016) {
        showBtnsEspecControlBar('inf', true, [1, 1, 1, 0]);
        tipsBtnsEspecControlBar('inf', ['Pesquisar', 'Preenche combo', 'Detalhar Rela��o', '']);
    }
    // Especific tecnicas
    else if (pastaID == 21019) {
        showBtnsEspecControlBar('inf', true, [0, 0, 0, 0]);
        tipsBtnsEspecControlBar('inf', ['', '', '', '']);
    }
    // Configura��o
    else if (pastaID == 21079) {
        showBtnsEspecControlBar('inf', true, [1, 0, 0, 0]);
        tipsBtnsEspecControlBar('inf', ['Sincronizar', '', '', '']);
    }
    // Argumentos de venda
    else if (pastaID == 21020) {
        showBtnsEspecControlBar('inf', true, [1, 0, 0, 0]);
        tipsBtnsEspecControlBar('inf', ['Editar argumento', '', '', '']);
    }
    else if (pastaID == 21021) {
        showBtnsEspecControlBar('inf', true, [1, 0, 0, 0]);
        tipsBtnsEspecControlBar('inf', ['Editar Observa��o', '', '', '']);
    }
    else if (pastaID == 21023) {
        showBtnsEspecControlBar('inf', true, [1, 0, 0, 0]);
        tipsBtnsEspecControlBar('inf', ['Editar Localidades', '', '', '']);
    }
    else if (pastaID == 21024) {
        showBtnsEspecControlBar('inf', true, [1, 1, 0, 0]);
        tipsBtnsEspecControlBar('inf', ['Editar Localidades', 'Descri��o', '', '']);
    }
    else if (pastaID == 21025) {
        showBtnsEspecControlBar('inf', true, [1, 0, 0, 0]);
        tipsBtnsEspecControlBar('inf', ['Editar Localidades', '', '', '']);
    }
    else if (pastaID == 21026) {
        showBtnsEspecControlBar('inf', true, [0, 0, 0, 0]);
        tipsBtnsEspecControlBar('inf', ['', '', '', '']);
    }
    else if (pastaID == 21028) {
        showBtnsEspecControlBar('inf', true, [0, 0, 0, 0]);
        tipsBtnsEspecControlBar('inf', ['', '', '', '']);
    }
    else if (pastaID == 21081) {
        showBtnsEspecControlBar('inf', true, [0, 0, 0, 0]);
        tipsBtnsEspecControlBar('inf', ['', '', '', '']);
    }
    // NVE
    else if (pastaID == 21050) {
        showBtnsEspecControlBar('inf', true, [0, 0, 0, 0]);
        tipsBtnsEspecControlBar('inf', ['', '', '', '']);
    }
    else if (pastaID != null) {
        showBtnsEspecControlBar('inf', false, [1, 1, 1, 1]);
    }

    // Ao final das operacoes particulares da interface do inf
    // manda mensagem para a biblioteca do inf tratar os controls
    // bars superior e inferior
    sendJSMessage(getHtmlId(), JS_INFSYS, 'FINISHCHANGEDIV', btnClicked);
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao tem que ser chamada por todas as funcoes de retorno de
dados do servidor.
Se o botao clicado e da barra superior volta mensagem para o superior
efetuar troca de interface.
Se o botao clicado e da barra inferior volta mensagem para o inferior
efetuar troca de interface.           
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function finalOfInfCascade(btnClicked) {
    //@@

    // Nao mexer - Inicio de automacao ==============================
    if ((btnClicked.toUpperCase()).indexOf('SUP') >= 0)
    // usario clicou botao no control bar sup
        sendJSMessage(getHtmlId(), JS_SUPSYS, 'CHANGEDIV', btnClicked);
    else
    // usario clicou botao no control bar inf
        sendJSMessage(getHtmlId(), JS_INFSYS, 'CHANGEDIV', btnClicked);
    // Final de Nao mexer - Inicio de automacao =====================

}

/********************************************************************
Funcao disparada pelo frame work.
Tratar os controls bar sup e inf aqui, apos o final de uma operacao
de interface e dados do form.
           
Parametros: 
btnClicked      - ultimo control bar (sup/inf) e botao clicado
folderID        - id da pasta corrente
tipoCtlrsPasta  - 'GRID' a pasta e um grid/ 'CTRLS' a pasta nao e
um grid
dsoID           - id do dso corrente

Retorno:
nenhum
********************************************************************/
function treatsControlsBars(btnClicked, folderID, tipoCtlrsPasta, dsoID) 
{
    var nContexto = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
		'getCmbCurrDataInControlBar(' + '\'' + 'sup' + '\'' + ', 1)[1]');

    var nTipoConceitoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['TipoConceitoID'].value");

    if (nContexto == 2115) 
    {
        setupEspecBtnsControlBar('sup', 'HHHHDH');
    }
    else if ((nContexto == 2112) || (nContexto == 2113) || (nContexto == 2111 && nTipoConceitoID == 302))
    {
        setupEspecBtnsControlBar('sup', 'HHHHDDDH');
    }
    else 
    {
        setupEspecBtnsControlBar('sup', 'HHHDDD');
    }

    if (btnClicked == 'SUPINCL') 
    {
        if (nContexto == 2112) {
            sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
				'window.focus()');

            sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
				'selClassificacaoID.focus()');
        }
    }

    setupEspecBtnsControlBar('inf', 'DDDD');

    // Identificadores
    if (folderID == 21009) 
    {
        setupEspecBtnsControlBar('inf', 'HDDD');
    }
    // Homologa��o
    else if (folderID == 21010) 
    {
        setupEspecBtnsControlBar('inf', 'HDDD');
    }
    // Caracteristica
    else if (folderID == 21011) 
    {
        var strBtns = currentBtnsCtrlBarString('inf');
        var aStrBtns = strBtns.split('');
        var nEstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['EstadoID'].value");
        var bTemKardex = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['TemKardex'].value");
        
        

        // Bot�o + da barra inferior
        aStrBtns[2] = 'D';
        // Bot�o - da barra inferior
        aStrBtns[5] = 'D';

        strBtns = aStrBtns.toString();
        re = /,/g;
        strBtns = strBtns.replace(re, '');
        adjustControlBarEx(window, 'inf', strBtns);

        if ((bTemKardex) && !(glb_PermiteSuporte || glb_PermiteHomologacao) && (glb_USERID != 1069))
            setupEspecBtnsControlBar('inf', 'HHDD');
        else if (!(bTemKardex) && (fg.Rows <= 1))
            setupEspecBtnsControlBar('inf', 'DDHH');
        else
            setupEspecBtnsControlBar('inf', 'HHHH');

        if (btnClicked == 'INFOK') {
            sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, '__btn_REFR(' + '\'' + 'sup' + '\'' + ')');
        }
    }
    // Rel entre Conceito, Rel com Pessoas
    else if ((folderID == 21015) || (folderID == 21016)) 
    {
        // relacao nao pode ser incluida se o registro nao estiver ativo
        nEstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['EstadoID'].value");
        if ((nEstadoID != 2) && (nEstadoID != 11)) 
        {
            var strBtns = currentBtnsCtrlBarString('inf');
            var aStrBtns = strBtns.split('');
            aStrBtns[2] = 'D';
            strBtns = aStrBtns.toString();
            re = /,/g;
            strBtns = strBtns.replace(re, '');
            adjustControlBarEx(window, 'inf', strBtns);
        }

        if (folderID == 21015) 
        {
            if (fg.Rows > 1)
                setupEspecBtnsControlBar('inf', 'HDHH');
            else
                setupEspecBtnsControlBar('inf', 'HDDH');
        }
        else if (folderID == 21016) 
        {
            if (fg.Rows > 1)
                setupEspecBtnsControlBar('inf', 'HDHD');
            else
                setupEspecBtnsControlBar('inf', 'HDDD');
        }
    }
    // Homologa��o
    else if (folderID == 21010) 
    {
        setupEspecBtnsControlBar('inf', 'DDDD');
    }
    // Argumento de venda
    else if (folderID == 21020) 
    {
        setupEspecBtnsControlBar('inf', 'HDDD');
    }
    // Protocolos
    else if (folderID == 21021) 
    {
        if (fg.Rows > 1)
            setupEspecBtnsControlBar('inf', 'HDDD');
    }
    // configura��o
    else if (folderID == 21079) 
    {
        setupEspecBtnsControlBar('inf', 'HDDD');
    }
    // Retencoes BJBN
    else if (folderID == 21023) 
    {
        setupEspecBtnsControlBar('inf', 'DDDD');
    }
    // Servicos BJBN
    else if (folderID == 21024) 
    {
        setupEspecBtnsControlBar('inf', 'DHDD');
    }
    // Serv Retencoes BJBN
    else if (folderID == 21025) 
    {
        setupEspecBtnsControlBar('inf', 'DDDD');
    }
    // Lojas e Canais RRF
    else if (folderID == 21026) 
    {
        setupEspecBtnsControlBar('inf', 'DDDD');
    }
    else if (folderID == 21028) 
    {
        setupEspecBtnsControlBar('inf', 'DDDD');
    }
    else if (folderID == 21081) {
        setupEspecBtnsControlBar('inf', 'DDDD');
    }
    // NVE
    else if (folderID == 21050) {
        setupEspecBtnsControlBar('inf', 'DDDD');
    }
}


/********************************************************************
Funcao disparada pelo frame work.
Propaga clique e botao de lupa que nao seja proprietario e
alternativo.
           
Parametros: 
btnClicked      - referencia ao botao de lupa clicado

Retorno:
nenhum
********************************************************************/
function btnLupaClicked(btnClicked) {

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario inseriu uma linha nova no grid.
           
Parametros: 
folderID        - id da pasta corrente
grid            - referencia ao grid
nLineInGrid     - numero da linha inserida no grid
dso             - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function addedLineInGrid(folderID, grid, nLineInGrid, dso) 
{
    // Aliquotas
    if (folderID == 21013)
        enableEspBtnAliquotas();
    // Rel entre Conceitos, Rel com Pessoas
    else if ((folderID == 21015) || (folderID == 21016))
        setupEspecBtnsControlBar('inf', 'DHDD');
    // Pesos e Medidas
    else if (folderID == 21018) 
    {
        // Grava o id do colaborador
        fg.TextMatrix(keepCurrLineInGrid(), getColIndexByColKey(fg, 'ColaboradorID')) = getCurrUserID();
        fg.LeftCol = 0;
    }
    else if (folderID == 21020) 
    {
        // Merge de Colunas
        fg.MergeCells = 0;

        setupEspecBtnsControlBar('inf', 'HDDD');
    }
    else if (folderID == 21021) 
    {
        // Merge de Colunas
        fg.MergeCells = 0;

        setupEspecBtnsControlBar('inf', 'HDDD');
    }
    else if (folderID == 21023) 
    {
        setupEspecBtnsControlBar('inf', 'HDDD');
    }
    else if (folderID == 21024) 
    {
        setupEspecBtnsControlBar('inf', 'HHDD');
    }
    else if (folderID == 21025) 
    {
        setupEspecBtnsControlBar('inf', 'HDDD');
    }
    else if (folderID == 21026) 
    {
        setupEspecBtnsControlBar('inf', 'DDDD');
    }
    else if (folderID == 21028) 
    {
        setupEspecBtnsControlBar('inf', 'DDDD');
    }
    else if (folderID == 21081) {
        setupEspecBtnsControlBar('inf', 'DDDD');
    }
    // NVE
    else if (folderID == 21050) {
        setupEspecBtnsControlBar('inf', 'DDDD');
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Dispara logo apos abrir a maquina de estado. 
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
nenhum
********************************************************************/
function stateMachOpened(currEstadoID) {

}

/********************************************************************
Funcao disparada pelo frame work.
Dispara se o usuario trocou o estado.
Dispara logo apos fechar a maquina de estado. 
           
Parametros: 
oldEstadoID     - previo EstadoID do registro
currEstadoID    - novo EstadoID do registro

Retorno:
nenhum
********************************************************************/
function stateMachClosed(oldEstadoID, currEstadoID) {
}
/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked) {

    // Pasta de Identificadores
    if (keepCurrFolder() == 21009) {
        // Usuario apertou o botao 1    
        if (btnClicked == 1)
            window.top.openModalControleDocumento('S', '', 7101, null, '213', 'T');
    }
    // Pasta de Homologa��o
    else if (keepCurrFolder() == 21010) {
        // Usuario apertou o botao 1    
        if (btnClicked == 1)
            window.top.openModalControleDocumento('S', '', 7101, null, '212', 'T');
    }
    // Pasta de Caracteristicas
    else if (keepCurrFolder() == 21011) {
        // Usuario apertou o botao 1    
        if (btnClicked == 1)
            setValorDefaultCaracteristica(fg.TextMatrix(fg.Row, getColIndexByColKey(fg, '^CaracteristicaID^dso02GridLkp^ConceitoID^RelacaoID*')), fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Valor')));
        // Usuario apertou o botao 2    
        else if (btnClicked == 2)
            sendJSCarrier(getHtmlId(), 'SHOWRELCONCEITOS', new Array(glb_aEmpresaData[0], fg.TextMatrix(fg.Row, getColIndexByColKey(fg, '^CaracteristicaID^dso02GridLkp^ConceitoID^RelacaoID*'))));
        else if (btnClicked == 3)
            openModalCopiaCaracteristica();
        else if (btnClicked == 4)
            openModalCaracteristica();
    }
    // pasta de Aliquotas - 21013
    else if (keepCurrFolder() == 21013) {
        // Usuario apertou o botao 1    
        if (btnClicked == 1)
            openModalAliquota('ORIG');
        // Usuario apertou o botao 2    
        else if (btnClicked == 2)
            openModalAliquota('DEST');
    }
    // modal de pesquisa na paginacao
    else if (((keepCurrFolder() == 21015) || (keepCurrFolder() == 21016)) && (btnClicked == 1))
        loadModalOfPagingPesq();
    // Usuario apertou o botao 2 - pasta de Rel Conceitos Rel com Pes - 21015/21016
    else if (((keepCurrFolder() == 21015) || (keepCurrFolder() == 21016))
        && (btnClicked == 2))
        openModalRelacoes();
    else if ((keepCurrFolder() == 21015) && (btnClicked == 3))
        sendJSCarrier(getHtmlId(), 'SHOWRELCONCEITOS', new Array(glb_aEmpresaData[0], fg.TextMatrix(fg.Row, 0)));
    else if ((keepCurrFolder() == 21015) && (btnClicked == 4))
        openModalInclueRelacoes();
    else if ((keepCurrFolder() == 21016) && (btnClicked == 3))
        sendJSCarrier(getHtmlId(), 'SHOWRELPESCON', new Array(glb_aEmpresaData[0], fg.TextMatrix(fg.Row, 0)));
    else if ((keepCurrFolder() == 21020) && (btnClicked == 1)) {
        // funcao do frame work
        // abre janela modal para campos Memo
        openModalQueries(fg, dso01Grid, 'ArgumentosVenda', getColIndexByColKey(fg, 'ArgumentosVenda'));
    }
    else if ((keepCurrFolder() == 21021) && (btnClicked == 1)) {
        // funcao do frame work
        // abre janela modal para campos Memo
        openModalObsProtocolos();
    }
    else if ((keepCurrFolder() == 21023) && (btnClicked == 1)) {
        var strPars = '?';
        var nLocalidadeID = '0';

        if (fg.Rows > 1) {
            nRegistroID = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'ConRetencaoID'));

            if (fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'LocalidadeID')).length > 0)
                nLocalidadeID = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'LocalidadeID'));
            else
                nLocalidadeID = 0;

            strPars += 'nLocalidadeID=' + escape(nLocalidadeID);

            htmlPath = SYS_PAGESURLROOT + '/pcm/conceitos/modalpages/modallocalidadeimpostos.asp' + strPars;
            showModalWin(htmlPath, new Array(690, 100));
        }
    }
    else if ((keepCurrFolder() == 21024) && (btnClicked == 1)) {
        var strPars = '?';
        var nLocalidadeID = '0';

        if (fg.Rows > 1) {
            if (fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'LocalidadeID')).length > 0)
                nLocalidadeID = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'LocalidadeID'));

            strPars += 'nLocalidadeID=' + escape(nLocalidadeID);

            htmlPath = SYS_PAGESURLROOT + '/pcm/conceitos/modalpages/modallocalidadeimpostos.asp' + strPars;
            showModalWin(htmlPath, new Array(690, 100));
        }
    }
    else if ((keepCurrFolder() == 21024) && (btnClicked == 2)) {
        openModalDescricaoServico();
    }
    else if ((keepCurrFolder() == 21025) && (btnClicked == 1)) {
        var strPars = '?';
        var nLocalidadeID = '0';

        if (fg.Rows > 1) {
            if (fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'LocalidadeID')).length > 0)
                nLocalidadeID = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'LocalidadeID'));
            else
                nLocalidadeID = 0;

            strPars += 'nLocalidadeID=' + escape(nLocalidadeID);

            htmlPath = SYS_PAGESURLROOT + '/pcm/conceitos/modalpages/modallocalidadeimpostos.asp' + strPars;
            showModalWin(htmlPath, new Array(690, 100));
        }
    }
    else if ((keepCurrFolder() == 21079) && (btnClicked == 1))
        sincronizaConfiguracao();

}

function openModalQueries(grid, dso, fldArgumentosVenda, colQuery) {
    var nConceitoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['ConceitoID'].value");
    var strPars = new String();
    //var nA2 = getCurrRightValue('INF', 'B1A2');
    
    //var nA2 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, ('getCurrRightValue(' + '\'' + 'INF' + '\'' + ',' + '\'' + 'A2' + '\'' + ')'));
    
    strPars = '?';

    // se esta em modo de edicao/inclusao
    if (grid.Editable)
        strPars += ('btnOKState=' + escape('H'));
    else
        strPars += ('btnOKState=' + escape('D'));

    // dso do grid
    strPars += ('&dso=' + escape(dso.id));

    // nome do campo de Obs no dso
    strPars += ('&fieldName=' + escape(fldArgumentosVenda));

    // id do grid
    strPars += ('&gridID=' + escape(grid.id));

    // coluna do grid referente ao campo de Observacoes
    strPars += ('&colQuery=' + escape(colQuery));

    // o titulo da conta
    strPars += ('&sConta=' + escape(''));
    
    // direitos de A2
    //strPars += ('&nA2=' + escape(nA2));

    strPars += ('&nConceitoID=' + escape(nConceitoID));

    htmlPath = SYS_PAGESURLROOT + '/pcm/conceitos/modalpages/modalqueries.asp' + strPars;
    showModalWin(htmlPath, new Array(740, 463));
}

function openModalObsProtocolos() {
    var nConProtocoloID = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'ConProtocoloID'));
    var sObservacao = ''; //fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Observacoes'));
    var strPars = new String();

    // ID do Protocolo
    strPars += ('?nConProtocoloID=' + escape(nConProtocoloID));

    // Conteudo Campo Obs
    strPars += ('&sObservacao=' + escape(sObservacao));

    // o titulo da conta
    strPars += ('&sConta=' + escape(''));

    htmlPath = SYS_PAGESURLROOT + '/pcm/conceitos/modalpages/modalobsprotocolos.asp' + strPars;
    showModalWin(htmlPath, new Array(740, 463));
}

function openModalDescricaoServico() {
    var nConServicoID = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'ConServicoID'));
    var sObservacao = ''; //fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Observacoes'));
    var strPars = new String();

    if (nConServicoID.length == 0)
        nConServicoID = '0';

    strPars += ('?nConServicoID=' + escape(nConServicoID));

    // se esta em modo de edicao/inclusao
    if (fg.Editable)
        strPars += ('&btnOKState=' + escape('H'));
    else
        strPars += ('&btnOKState=' + escape('D'));

    htmlPath = SYS_PAGESURLROOT + '/pcm/conceitos/modalpages/modaldescricaoservico.asp' + strPars;
    showModalWin(htmlPath, new Array(740, 463));
}

function openModalInclueRelacoes() {
    if (fg.disabled == false)
        fg.focus();

    var htmlPath;
    var strPars = new String();
    var nWidth = 771;
    var nHeight = 484;
    var aEmpresa = glb_aEmpresaData;
    var nProdutoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['ConceitoID'].value");
    var sProduto = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['Conceito'].value");
    var nFamiliaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['ProdutoID'].value");
    var sFamilia = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'selProdutoID.options(selProdutoID.selectedIndex).text');

    // mandar os parametros para o servidor
    // Esta modal fica sempre carregada e o que identifica
    // isto e o terceiro parametro da funcao showModalWin abaixo.
    // No strPars, o primeiro parametro e obrigatorio e e o UNICO!!!
    // que pode ser mandado

    strPars = '?sCaller=' + escape('I');
    strPars += '&nEmpresaID=' + escape(aEmpresa[0]);
    strPars += '&nProdutoID=' + escape(nProdutoID);
    strPars += '&sProduto=' + escape(sProduto);
    strPars += '&nFamiliaID=' + escape(nFamiliaID);
    strPars += '&sFamilia=' + escape(sFamilia);

    // carregar a modal
    htmlPath = SYS_ASPURLROOT + '/pcm/conceitos/modalpages/modalincluerelacoes.asp' + strPars;

    showModalWin(htmlPath, new Array(nWidth, nHeight));
}


/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou um botao nao especifico do control bar.

Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - Um dos seguintes botoes:
- Avancar, retroagir, incluir, alterar, estado, 
excluir, OK, cancelar refresh, anterior e proximo. 
(Testar a string que vem).

Retornos:
null            - a automacao prossegue. Qualquer outro retorno a
automacao nao prossegue.
********************************************************************/
function btnBarNotEspecClicked(controlBar, btnClicked) {
    // Caracter�sticas
    /*if ((keepCurrFolder() == 21011) &&
         (btnClicked == 'INFINCL' || btnClicked == 'INFEXCL')) {
        sincronizaCaracteristicas(btnClicked);
        return true;
    }*/

    // Identificadores
    if ((keepCurrFolder() == 21009) && (btnClicked == 'INFOK')) {

        // Vai ao servidor checar se ja existe algum identificador semelhante

        strPars = '?sIdentificador=' + escape(fg.TextMatrix(keepCurrLineInGrid(), getColIndexByColKey(fg, 'Identificador')));
        strPars += '&nRegID=' + escape(fg.TextMatrix(keepCurrLineInGrid(), fg.Cols - 1));

        dsoChkIdentificador.URL = SYS_ASPURLROOT + '/pcm/conceitos/serverside/chkidentificador.aspx' + strPars;
        dsoChkIdentificador.ondatasetcomplete = dsoChkIdentificador_DSC;
        dsoChkIdentificador.refresh();
        return true;
    }
    else if ((keepCurrFolder() == 21009) && (btnClicked == 'INFALT')) 
    {
        fg.MergeCol(-1) = false;
    }

    //Beneficios Fiscais
    if (keepCurrFolder() == 21029) 
    {
        var nEstadoBeneficioID;
        var nEstadoID;
        var strMensagem = "";

        if (btnClicked == 'INFOK') 
        {
            //Quando usuario clicar botao OK, verifica se ja existe aquele beneficio a ser inserido e verifica tamb�m se o produto pode possuir aquele beneficio
            var nTipobeneficioID, nPPB = 2307, nSUFRAMA = 2308;
            var nTemSUFRAMA = 0, nTemPPB = 0;
            var bPaisTemPPB, bCidadeTemSUFRAMA;            

            //Recupera beneficio escolhido pelo usu�rio
            nTipoBeneficioID = fg.TextMatrix(keepCurrLineInGrid(), getColIndexByColKey(fg, 'TipoBeneficioID'));

            bPaisTemPPB = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['LocalidadeTemPPB'].value");
            bCidadeTemSUFRAMA = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['LocalidadeTemSUFRAMA'].value");

            for (var i = 0; i < fg.Rows; i++) {
                //Verifica se Produto j� possui PPB cadastrado
                if (fg.TextMatrix(i, getColIndexByColKey(fg, 'TipoBeneficioID')) == nPPB)
                    nTemPPB++;
                //Verifica se Produto j� possui SUFRAMA cadastrado
                else if (fg.TextMatrix(i, getColIndexByColKey(fg, 'TipoBeneficioID')) == nSUFRAMA)
                    nTemSUFRAMA++;
            }

            nTemPPB = (nTipoBeneficioID == nPPB ? (nTemPPB - 1) : nTemPPB);
            nTemSUFRAMA = (nTipoBeneficioID == nSUFRAMA ? (nTemSUFRAMA - 1) : nTemSUFRAMA);

            //Beneficio escolhido pelo usuario no combo � PPB ou SUFRAMA
            if (nTipoBeneficioID == nPPB || nTipoBeneficioID == nSUFRAMA) {

                //Para este produto, n�o existe PPB ou SUFRAMA Cadastrado
                if (nTemPPB == 0 && nTemSUFRAMA == 0) {
                    //Usuario escolheu PPB e pais do Fabricante n�o possui beneficio PPB
                    if (nTipoBeneficioID == nPPB && bPaisTemPPB == 0)
                        strMensagem += "Pais do fabricante n�o permite que ele tenha PPB. ";
                    //Usuario escolheu SUFRAMA e cidade do Fabricante n�o possui beneficio SUFRAMA
                    else if (nTipoBeneficioID == nSUFRAMA && bCidadeTemSUFRAMA == 0)
                        strMensagem += "Cidade do fabricante n�o permite que ele tenha SUFRAMA. ";
                }
                else
                    strMensagem += "Produto j� possui benef�cio PPB ou SUFRAMA cadastrado. ";
            }

            if (strMensagem != "") {
                window.top.overflyGen.Alert(strMensagem);
                return true;
            }
        }
        else if (btnClicked == 'INFEXCL') 
        {
            nEstadoBeneficioID = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'EstadoID'));
            nEstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['EstadoID'].value");
            
            if ((nEstadoBeneficioID == 2) && (nEstadoID != 1))
                strMensagem = "Benef�cio n�o pode ser deletado, pois j� esta ativo. ";
                
            if (strMensagem != "") 
            {
                window.top.overflyGen.Alert(strMensagem);
                return true;
            }
        }
    }
    if (keepCurrFolder() == 21018)
    {
        var nEmpresaPaisID = glb_aEmpresaData[1];

        var strMensagem = "";

        if (btnClicked == 'INFOK') 
        {
            nTipoConceitoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['TipoConceitoID'].value");
            nClassificacaoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['ClassificacaoID'].value");
            sFamilia = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['Familia'].value");

            if ((nTipoConceitoID == 303 && nClassificacaoID == 311) && !(sFamilia.match(/Licen�a.*/)))
            {
                //Quando usu�rio clicar bot�o OK, verifica se todos os campos de medidas (peso, largura, altura, profundidade) est�o preenchidos.
                var nPeso = '', nLargura = '', nAltura = '', nProfundidade = '';

                nPeso = fg.TextMatrix(keepCurrLineInGrid(), getColIndexByColKey(fg, 'Peso'));
                nLargura = fg.TextMatrix(keepCurrLineInGrid(), getColIndexByColKey(fg, 'Largura'));
                nAltura = fg.TextMatrix(keepCurrLineInGrid(), getColIndexByColKey(fg, 'Altura'));
                nProfundidade = fg.TextMatrix(keepCurrLineInGrid(), getColIndexByColKey(fg, 'Profundidade'));

                if ((nPeso == '' || nLargura == '' || nAltura == '' || nProfundidade == '') && glb_USERID != 1069) {
                    strMensagem += "� necess�rio preencher todos os campos de medidas e peso do produto.";
                }

                if (strMensagem != "") {
                    window.top.overflyGen.Alert(strMensagem);
                    return true;
                }
            }
        }
    }

    // Para prosseguir a automacao retornar null
    return null;
}

function dsoChkIdentificador_DSC() 
{
    var sProducts = dsoChkIdentificador.recordset['fldresp'].value;
    if ((sProducts != '') && (sProducts != null)) {
        sProducts = 'Este Identificador ja existe no(s) seguinte(s) produto(s)' +
			String.fromCharCode(10, 13) + sProducts + String.fromCharCode(10, 13) +
			'Deseja realmente gravar?';

        var _retMsg = window.top.overflyGen.Confirm(sProducts);

        if (_retMsg == 0)
            return null;
        else if (_retMsg == 1) {
            glb_LastActionInForm__ = 0;
            glb_btnCtlSupInf = 'INFOK';
            saveRegister_1stPart();
        }
    }
    else {
        glb_LastActionInForm__ = 0;
        glb_btnCtlSupInf = 'INFOK';
        saveRegister_1stPart();
    }

}

/********************************************************************
Funcao disparada pelo frame work.
Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2) {
    // Janela de pesquisa para paging
    if (((keepCurrFolder() == 21015) || (keepCurrFolder() == 21016)) && (idElement.toUpperCase() == 'MODALOFPAGINGINFHTML'))
        execPesqInPaging(param1, param2);

    if (keepCurrFolder() == 21013 && (idElement.toUpperCase() == 'MODALLOCALIDADEHTML')) {
        if (param1 == 'OK') {
            // 1. Usuario escolheu uma cidade ou um estado    
            // Devolve sempre um array no param2:
            // Elem 0: glb_sBtnClicked
            // Elem 1:
            // glb_sNivelGovID = 232 - Imposto estadual
            // ou
            // glb_sNivelGovID = 233 - Imposto municipal
            // Elem 2:
            // Nome do estado ou da cidade
            // Elem 3:
            // Sigla do estado, da cidade nao tem devolve ''
            // Elem 4:
            // ID do estado ou da cidade

            // Escreve LocalidadeOrigem no grid de Aliquotas
            writeOriDestInGrid(param2);
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Altera��o');
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Altera��o');
            return 0;
        }
    }
    if (((keepCurrFolder() == 21015) || (keepCurrFolder() == 21016))
            && (idElement.toUpperCase() == 'MODALRELACOESHTML')) {
        if (param1 == 'OK') {
            // Escreve ID, Label e TipoRelacaoID no grid de relacoes
            writeSujObjInGrid(param2);
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Altera��o');
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Altera��o');
            return 0;
        }
    }
    if ((keepCurrFolder() == 21011) &&
         ((idElement.toUpperCase() == 'MODALCARACTERISTICASHTML') ||
          (idElement.toUpperCase() == 'MODALCOPIACARACTERISTICASHTML'))) {
        // Codigo botao OK apenas para compatibilidade
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();

            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();

            if (param2 == 'SupRefr') {
                sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "__btn_REFR('sup')");

                var chkRefrInf = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, "chkRefrInf.checked");
                
                if (!chkRefrInf)
                    __inFTimer_Conceitos = window.setInterval('forceInfRefr()', 1000, 'JavaScript');
            }
            else
                __inFTimer_Conceitos = window.setInterval('forceInfRefr()', 10, 'JavaScript');

            return 0;
        }
    }
    // Modal inclue relacoes
    if (idElement.toUpperCase() == 'MODALINCLUERELACOESHTML') {
        if (param1 == 'OK') {
            // esta aqui apenas para compatibilidade
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');

            // nao mexer
            return 0;
        }
    }
    // Argumentos de venda
    if ((keepCurrFolder() == 21020) && (idElement.toUpperCase() == 'MODALQUERIESHTML')) {
        if (param1 == 'OK') {
            // escreve observacoes no grid
            //fg.Redraw = 0;

            // Escreve no campo
            //fg.TextMatrix(keepCurrLineInGrid(),getColIndexByColKey(fg, 'ArgumentosVenda')) = param2;
            //fg.TextMatrix(keepCurrLineInGrid(),getColIndexByColKey(fg, '_calc_ArgumentoAbreviado*')) = param2;

            //fg.Redraw = 2;
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            __inFTimer_Conceitos = window.setInterval('forceInfRefr()', 10, 'JavaScript');

            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    //Observa��es Protocolos
    else if ((keepCurrFolder() == 21021) && (idElement.toUpperCase() == 'MODALOBSPROTOCOLOSHTML')) {
        if (param1 == 'OK') {
            // escreve observacoes no grid
            //fg.Redraw = 0;

            // Escreve no campo
            //fg.TextMatrix(keepCurrLineInGrid(),getColIndexByColKey(fg, 'ArgumentosVenda')) = param2;
            //fg.TextMatrix(keepCurrLineInGrid(),getColIndexByColKey(fg, '_calc_ArgumentoAbreviado*')) = param2;

            //fg.Redraw = 2;
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            __inFTimer_Conceitos = window.setInterval('forceInfRefr()', 10, 'JavaScript');

            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }

    else if (((keepCurrFolder() == 21023) || (keepCurrFolder() == 21024) || (keepCurrFolder() == 21025)) && (idElement.toUpperCase() == 'MODALLOCALIDADEIMPOSTOSHTML')) {
        if (param1 == 'OK') {
            // escreve observacoes no grid
            //fg.Redraw = 0;

            // Escreve no campo

            fg.TextMatrix(keepCurrLineInGrid(), getColIndexByColKey(fg, 'LocalidadeID')) = param2[0];

            if (keepCurrFolder() == 21023)
                fg.TextMatrix(keepCurrLineInGrid(), getColIndexByColKey(fg, '^ConSerRetencaoID^dso01GridLkp^ConSerRetencaoID^Localidade*')) = param2[1];
            else if (keepCurrFolder() == 21024)
                fg.TextMatrix(keepCurrLineInGrid(), getColIndexByColKey(fg, '^ConServicoID^dso01GridLkp^ConServicoID^Localidade*')) = param2[1];
            else if (keepCurrFolder() == 21025)
                fg.TextMatrix(keepCurrLineInGrid(), getColIndexByColKey(fg, '^ConSerRetencaoID^dso01GridLkp^ConSerRetencaoID^Localidade*')) = param2[1];

            //fg.Redraw = 2;
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            //__inFTimer_Conceitos = window.setInterval('forceInfRefr()', 10, 'JavaScript');

            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }

    //Observa��es Protocolos
    else if ((keepCurrFolder() == 21024) && (idElement.toUpperCase() == 'MODALDESCRICAOSERVICOHTML')) {
        if (param1 == 'OK') {
            // escreve observacoes no grid
            //fg.Redraw = 0;

            // Escreve no campo
            fg.TextMatrix(keepCurrLineInGrid(), getColIndexByColKey(fg, 'DescricaoLocal')) = param2;
            fg.TextMatrix(keepCurrLineInGrid(), getColIndexByColKey(fg, 'Descricao*')) = ((param2.length > 100) ? param2.substring(0, 100) + '...' : param2);

            //fg.Redraw = 2;
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            //__inFTimer_Conceitos = window.setInterval('forceInfRefr()', 10, 'JavaScript');

            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
}

function forceInfRefr() {
    if (__inFTimer_Conceitos != null) {
        window.clearInterval(__inFTimer_Conceitos);
        __inFTimer_Conceitos = null;
    }

    __btn_REFR('inf');
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou o botao de alteracao em uma pasta de grid. 
           
Parametros: 
folderID    - id da pasta corrente

Retorno:
nenhum
********************************************************************/
function btnAltPressedInGrid(folderID) 
{
    if (folderID == 21013)
        enableEspBtnAliquotas();
    // Pesos e Medidas
    else if ((folderID == 21018) && (fg.Rows > 1)) {
        fg.TextMatrix(keepCurrLineInGrid(), getColIndexByColKey(fg, 'ColaboradorID')) = getCurrUserID();
        fg.LeftCol = 0;
    }

    // Argumentos de Venda
    else if (folderID == 21020) {
        // Merge de Colunas
        fg.MergeCells = 0;

        setupEspecBtnsControlBar('inf', 'HDDD');
    }
    else if (folderID == 21023) {
        setupEspecBtnsControlBar('inf', 'HDDD');
    }
    else if (folderID == 21024) {
        setupEspecBtnsControlBar('inf', 'HHDD');
    }
    else if (folderID == 21025) {
        setupEspecBtnsControlBar('inf', 'HDDD');
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou o botao de alteracao em uma pasta que nao e grid. 
           
Parametros: 
folderID    - id da pasta corrente

Retorno:
nenhum
********************************************************************/
function btnAltPressedInNotGrid(folderID) {

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario selecionou uma pasta no control bar inferior.
Nesta funcao o programador preenche o combo de filtros inferiores com 
dados
particulares do form ou deixa o frame work preencher com o combo de
filtros inferiores com os dados dos filtros inferiores.
           
Parametros: 
folderID    - id da pasta corrente

Retorno:
true    - o frame work opera automaticamente
false   - o programador preenche o combo manualmente
********************************************************************/
function fillCmbFiltsInfAutomatic(folderID) {
    //@@ Retornar true se nao ha preenchimento manual.
    // Retornar false se ha.

    if ((folderID == 21015) || (folderID == 21016))
        glb_scmbKeyPesqSel = 'aRelacaoID';

    if ((folderID != 21015) && (folderID != 21016))
        return true;

    var aCmbTipoRelacaoText = new Array();
    var aCmbTipoRelacaoValue = new Array();
    var sDefaultRel = '';
    var nRelacaoEntreID;
    var i = 0;

    // Preenche o combo de filtros com as relacoes possiveis
    if ((folderID == 21015) || (folderID == 21016)) //Rel entre Conceitos/Rel com Pessoas
    {
        cleanupSelInControlBar('inf', 2);
        nCurrRegID = getCurrDataInControl('sup', 'txtRegistroID');
        nCurrTipoRegID = getCurrDataInControl('sup', 'selTipoRegistroID');

        if (folderID == 21015)
            nRelacaoEntreID = 134;
        else if (folderID == 21016)
            nRelacaoEntreID = 135;

        if (!((dsoCmbRel.recordset.BOF) && (dsoCmbRel.recordset.EOF))) {
            dsoCmbRel.recordset.MoveFirst();
            if (!dsoCmbRel.recordset.EOF) {
                while (!dsoCmbRel.recordset.EOF) {
                    if ((dsoCmbRel.recordset['TipoPossivel'].value == nCurrTipoRegID) &&
                         (dsoCmbRel.recordset['RelacaoEntreID'].value == nRelacaoEntreID)) {
                        if (ascan(aCmbTipoRelacaoText, dsoCmbRel.recordset['NomePasta'].value, false) < 0) {
                            aCmbTipoRelacaoText[i] = dsoCmbRel.recordset['NomePasta'].value;
                            aCmbTipoRelacaoValue[i] = [dsoCmbRel.recordset['TipoRelacaoID'].value,
                                                       dsoCmbRel.recordset['Tipo'].value,
                                                       dsoCmbRel.recordset['Header'].value,
                                                       dsoCmbRel.recordset['RelacaoEntreID'].value];

                            if ((dsoCmbRel.recordset['EhDefault'].value) && (dsoCmbRel.recordset['Tipo'].value == 'OBJETO'))
                                sDefaultRel = (aCmbTipoRelacaoValue[i]).toString();

                            i++;
                        }
                    }
                    dsoCmbRel.recordset.MoveNext();
                }
            }
        }

        if (aCmbTipoRelacaoText.length > 0) {
            for (i = 0; i < aCmbTipoRelacaoText.length; i++) {
                addOptionToSelInControlBar('inf', 2, aCmbTipoRelacaoText[i], (aCmbTipoRelacaoValue[i]).toString());
            }
        }

        if (!selOptByValueOfSelInControlBar('inf', 2, sDefaultRel))
            if (aCmbTipoRelacaoValue.length > 0)
            selOptByValueOfSelInControlBar('inf', 2, (aCmbTipoRelacaoValue[0]).toString());

        return false;
    }

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario selecionou uma pasta no control bar inferior.
Esta funcao executa antes de qualquer operacao do frame work           

Parametros: 
optText             - texto do option do combo
optValue            - value do option do combo
optIndex            - indice do option do combo
nQtyGridsInFolder   - 0 nao tem grid, 1 ou 2

Retorno:
qualquer coisa diferente de null para a automacao
********************************************************************/
function selPastaChanged(optText, optValue, optIndex, nQtyGridsInFolder) {
    // este form usa o combo de filtros para outras finalidades
    // desta forma o combo deve ser preenchido aqui e nao
    // na funcao prgInterfaceInf
    if (!((optValue == 21015) || (optValue == 21016)))
        refillCmb2Inf(optValue);

    // Uso avancado, so roda uma vez
    if ((optValue == 21015) || (optValue == 21016)) {
        if (dsoCmbRel.recordset.Fields.Count == 0) {
            var formID = window.top.formID;  // variavel global do browser filho
            // passa o id do form por parametro
            var strPars = new String();
            strPars = '?';
            strPars += 'formID=';
            strPars += escape(formID.toString());

            dsoCmbRel.URL = SYS_ASPURLROOT + '/serversidegenEx/tiposrelacoes.aspx' + strPars;
            dsoCmbRel.ondatasetcomplete = dsoCmbRelComplete_1_DSC;
            dsoCmbRel.refresh();
            // Para a automacao
            return true;
        }
    }

    //@@ retornar null para prosseguir a automacao
    return null;
}

function dsoCmbRelComplete_1_DSC() {
    setCurrDSOByCmb1(keepCurrFolder(), true);
}

/********************************************************************
Funcao disparada pelo frame work.
O frame work esta prestes a setar os botoes da barra infeior, em funcao
dos direitos do usuario logado

Parametros: 
folderID    - pasta corrente

Retorno:
indiferente para a automacao
********************************************************************/
function frameIsAboutToApplyRights(folderID) {

    var bTemKardex = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['TemKardex'].value");

    // Pasta de LOG Read Only
    if (folderID == 20010) {
        // trava as barras do inf para a pasta acima definida
        glb_BtnsIncAltEstExcl[0] = true;
        glb_BtnsIncAltEstExcl[1] = true;
        glb_BtnsIncAltEstExcl[2] = true;
        glb_BtnsIncAltEstExcl[3] = true;
    }
    // A pasta de Estoque � Read Only
    else if (folderID == 21012) {
        // trava as barras do inf para a pasta acima definida
        glb_BtnsIncAltEstExcl[0] = true;
        glb_BtnsIncAltEstExcl[1] = true;
        glb_BtnsIncAltEstExcl[2] = true;
        glb_BtnsIncAltEstExcl[3] = true;
    }
    // As pastas de Relacoes so habilta o botao de alterar
    else if ((folderID == 21015) || (folderID == 21016)) {
        // trava as barras do inf para a pasta acima definida
        glb_BtnsIncAltEstExcl[1] = true;
        glb_BtnsIncAltEstExcl[2] = true;
        glb_BtnsIncAltEstExcl[3] = true;
    }
    // Pesos e Medidas
    else if ((folderID == 21018) && (bTemKardex) && glb_USERID != 1069 && !(glb_PermiteSuporte > 0 || glb_PermiteHomologacao > 0 || glb_PermiteLogistica > 0))
    {
        glb_BtnsIncAltEstExcl[0] = true;
        glb_BtnsIncAltEstExcl[1] = true;
        glb_BtnsIncAltEstExcl[2] = true;
        glb_BtnsIncAltEstExcl[3] = true;
    }                                                                                                                            
    // Caracter�sticas                                                                                                           
    else if ((folderID == 21011) && (bTemKardex) && (glb_USERID != 1069) && !(glb_PermiteSuporte > 0 || glb_PermiteHomologacao > 0)) {
        glb_BtnsIncAltEstExcl[0] = true;
        glb_BtnsIncAltEstExcl[1] = true;
        glb_BtnsIncAltEstExcl[2] = true;
        glb_BtnsIncAltEstExcl[3] = true;
    }
    else if (folderID == 21020) {
        glb_BtnsIncAltEstExcl[0] = true;
        glb_BtnsIncAltEstExcl[1] = true;
        glb_BtnsIncAltEstExcl[2] = true;
    }
    else {
        // destrava as barras do inf para a pasta acima definida
        glb_BtnsIncAltEstExcl[0] = null;
        glb_BtnsIncAltEstExcl[1] = null;
        glb_BtnsIncAltEstExcl[2] = null;
        glb_BtnsIncAltEstExcl[3] = null;
    }
    return null;
}

/********************************************************************
Funcao criada pelo programador.
Habilita os bot�es espec�ficos do grid de Al�quotas
Pasta de Aliquotas - 21013

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function enableEspBtnAliquotas() {
    // 1. Se a pasta corrente e a pasta de Aliquotas - 21013
    // 1.1. Habilita/desabilita o botao de pesquisa de Localidade Origem/Destino 
    //no control bar inferior

    nTipoImp = getCurrDataInControl('sup', 'selRegraAliquotaID');
    var nTipoImp;

    // IMPORTANTE:
    // 1. Se a pasta corrente e a pasta de Aliquotas - 21013
    // 1.1. Habilita/desabilita o botao de pesquisa de Localidade Origem/Destino 
    //no control bar inferior

    nTipoImp = getCurrDataInControl('sup', 'selRegraAliquotaID');
    // Regra de Excess�o habilita Origem e Destino
    if (nTipoImp == 333)
        setupEspecBtnsControlBar('inf', 'HHDD');
    else
        setupEspecBtnsControlBar('inf', 'HDDD');

}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal de enderecos para a pasta de enderecos - 20111

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalAliquota(btnClicked) {
    var aTipoImp;
    // Array que cont�m a configuracao do Imposto   
    aTipoImp = [getCurrDataInControl('sup', 'selPaisID'),
                getCurrDataInControl('sup', 'selNivelGovernoID'),
                getCurrDataInControl('sup', 'selRegraAliquotaID'),
                getCurrDataInControl('sup', 'selPaisID', true)];

    if (btnClicked == 'ORIG') {
        // Para Imposto Federal, grava no grid o Pais do imposto
        if (aTipoImp[1] == 231) // Imposto Federal
        {
            fg.TextMatrix(keepCurrLineInGrid(), 0) = aTipoImp[3];
            fg.TextMatrix(keepCurrLineInGrid(), 3) = aTipoImp[0];
        }
    }
    else if (btnClicked == 'DEST') {
        // Para Imposto Federal, grava no grid o Pais do imposto
        if (aTipoImp[1] == 231) // Imposto Federal
        {
            fg.TextMatrix(keepCurrLineInGrid(), 1) = aTipoImp[3];
            fg.TextMatrix(keepCurrLineInGrid(), 4) = aTipoImp[0];
        }
    }

    // Para Imposto Municipal/Estadual, abre a Janela Modal  
    if ((aTipoImp[1] == 233) || (aTipoImp[1] == 232)) // Imposto Municipal/Estadual
    {
        var strPars = new String();
        strPars = '?';
        // Monta a string do array para a janela modal
        for (i = 0; i < aTipoImp.length; i++) {
            strPars += 'Item' + i + '=' + escape(aTipoImp[i]);
            if (i <= aTipoImp.length - 2)
                strPars += '&';
        }

        // chamada pelo botao ORIG ou pelo botao DEST
        strPars += '&';
        strPars += 'BtnClicked=' + escape(btnClicked);

        var htmlPath;

        // carregar modal - faz operacao de banco no carregamento
        htmlPath = SYS_ASPURLROOT + '/pcm/conceitos/modalpages/modallocalidade.asp' + strPars;

        showModalWin(htmlPath, new Array(485, 334));
        if (btnClicked == 'ORIG')
            writeInStatusBar('child', 'cellMode', 'Origem', true);
        else if (btnClicked == 'DEST')
            writeInStatusBar('child', 'cellMode', 'Destino', true);
    }

}

/********************************************************************
Funcao criada pelo programador.
Escreve a Localidade Origem/Destino vindo da janela modal de Localidades.
Pasta de Aliquotas - 21013

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function writeOriDestInGrid(Origem) {
    // Devolve sempre um array no parametro Origem:
    // Elem 0: glb_sBtnClicked
    // Elem 1:
    // glb_sNivelGovID == 232 - Imposto estadual
    // ou
    // glb_sNivelGovID == 233 - Imposto municipal
    // Elem 2:
    // Nome do estado ou da cidade
    // Elem 3:
    // Sigla do estado, da cidade nao tem devolve ''
    // Elem 4:
    // ID do estado ou da cidade

    // o parametro Origem contem: LocalidadeOrigemID
    if (Origem[0] == 'ORIG') // alterar marco
    {
        fg.TextMatrix(keepCurrLineInGrid(), 0) = Origem[2];
        fg.TextMatrix(keepCurrLineInGrid(), 3) = Origem[4];
    }
    else if (Origem[0] == 'DEST') {
        fg.TextMatrix(keepCurrLineInGrid(), 1) = Origem[2];
        fg.TextMatrix(keepCurrLineInGrid(), 5) = Origem[4];
    }
}

/********************************************************************
Funcao criada pelo programador.
Seta um valor de Caracteristica com default
Pasta de Caracter�sticas - 21011

Parametro:
nRelacaoID, sValor

Retorno:
nenhum
********************************************************************/
function setValorDefaultCaracteristica(nRelacaoID, sValor) {
    var _retMsg = window.top.overflyGen.Confirm('Setar: ' + '\"' + sValor + '\"' + ' como valor default desta caracter�stica?');

    if (_retMsg == 0)
        return null;
    else if (_retMsg == 1) {
        lockInterface(true);
        var strPars = new String();
        strPars = '?nRelacaoID=' + escape(nRelacaoID);
        strPars += '&sValor=' + escape(sValor);
        dsoValorDefault.URL = SYS_ASPURLROOT + '/pcm/conceitos/serverside/valordefaultcaracteristica.aspx' + strPars;
        dsoValorDefault.ondatasetcomplete = setValorDefaultCaracteristica_DSC;
        dsoValorDefault.refresh();
    }
    else
        return null;
}

/********************************************************************
Funcao criada pelo programador.
Retorno da funcao setValorDefaultCaracteristica. 
Pasta de Caracter�sticas - 21011

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function setValorDefaultCaracteristica_DSC() {
    //destrava a Interface
    lockInterface(false);
}

/********************************************************************
Funcao criada pelo programador.
Retorno do servidor dos dados do combo de filtros, usado para relacoes.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function dsoCmbRelComplete_DSC() {
    // ao final volta para automacao
    finalOfInfCascade(glb_lastBtnClicked);
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal de Relacoes

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalRelacoes() {
    var currCmb2Data = getCmbCurrDataInControlBar('inf', 2);
    var aValue = currCmb2Data[1].split(',');
    var htmlPath;
    var strPars = new String();
    var nTipoRelID = aValue[0];
    var sTipoSujObj = aValue[1];
    var sHeader = aValue[2];
    var nSujObjID = getCurrDataInControl('sup', 'txtRegistroID');
    var nRelEntreID = aValue[3];

    // mandar os parametros para o servidor
    strPars = '?';
    strPars += 'nUserID=' + escape(getCurrUserID());
    strPars += '&nTipoRelID=' + escape(nTipoRelID);
    strPars += '&sTipoSujObj=' + escape(sTipoSujObj);
    strPars += '&sHeader=' + escape(sHeader);
    strPars += '&nSujObjID=' + escape(nSujObjID);
    strPars += '&nRelEntreID=' + escape(nRelEntreID);

    // carregar modal - nao faz operacao de banco no carregamento
    htmlPath = SYS_PAGESURLROOT + '/pcm/conceitos/modalpages/modalrelacoes.asp' + strPars;
    showModalWin(htmlPath, new Array(571, 284));
    // writeInStatusBar('child', 'cellMode', 'XXXXX', true);    
}

/********************************************************************
Funcao criada pelo programador.
Escreve Sujeito/Objeto ID da Relacao vindo da janela modal de Relacoes.
Pasta de Relacoes - 21015/21016

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function writeSujObjInGrid(aSujObj) {
    // o array aSujObj contem: fldID, Fantasia, TipoRelacaoID

    fg.Redraw = 0;
    // Escreve nos campos

    fg.TextMatrix(keepCurrLineInGrid(), 2) = aSujObj[0];
    fg.TextMatrix(keepCurrLineInGrid(), 4) = aSujObj[2];
    fg.TextMatrix(keepCurrLineInGrid(), 5) = aSujObj[1];

    fg.Redraw = 2;
}


/********************************************************************
Funcao criada pelo programador.
Abre janela modal do carrinho de compras

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalCaracteristica() {
    var htmlPath;
    var strPars = new String();
    var nWidth = 996;
    var nHeight = 570;

    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + escape('I');

    // carregar a modal
    htmlPath = SYS_ASPURLROOT + '/pcm/conceitos/modalpages/modalcaracteristicas.asp' + strPars;

    showModalWin(htmlPath, new Array(nWidth, nHeight));
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal do carrinho de compras

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalCopiaCaracteristica() {
    var htmlPath;
    var strPars = new String();
    var nWidth = 996;
    var nHeight = 570;
    var nFamiliaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['ProdutoID'].value");
    var nProdutoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['ConceitoID'].value");

    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + escape('I');
    strPars += '&nFamiliaID=' + escape(nFamiliaID);
    strPars += '&nProdutoID=' + escape(nProdutoID);

    // carregar a modal
    htmlPath = SYS_ASPURLROOT + '/pcm/conceitos/modalpages/modalcopiacaracteristicas.asp' + strPars;

    showModalWin(htmlPath, new Array(nWidth, nHeight));
}

/********************************************************************
Funcao criada pelo programador.
Atualiza as familias qeu o produto tem compatibilidade como objeto

Parametro: 
nenhum

Retorno:
nenhum
********************************************************************/
function sincronizaConfiguracao() {
    var nConceitoID;

    nConceitoID = getCurrDataInControl('sup', 'txtRegistroID');

    lockInterface(true);

    var strPars = new String();

    strPars = '?nConceitoID=' + escape(nConceitoID);

    dsoSincronizaConfiguracao.URL = SYS_ASPURLROOT + '/pcm/conceitos/serverside/sincronizaconfiguracao.aspx' + strPars;
    dsoSincronizaConfiguracao.ondatasetcomplete = sincronizaConfiguracao_DSC;
    dsoSincronizaConfiguracao.refresh();
}

function sincronizaConfiguracao_DSC() {
    //destrava a Interface
    lockInterface(false);
    __btn_REFR('inf');
}

//Apenas perfil de desenvolvedor, administrador e homologa��o poder�o alterar alguns dados do coneito em A.
function perfilUsuario() {
    var userID = getCurrUserID();

    setConnection(dsoperfilUsuario);

    dsoperfilUsuario.SQL = 'SELECT COUNT(DISTINCT a.PerfilID) AS PermiteSuporte, ' +
                                    '(SELECT COUNT(DISTINCT aa.PerfilID) ' +
							            'FROM RelacoesPesRec_Perfis aa WITH(NOLOCK) ' +
									        'INNER JOIN RelacoesPesRec bb WITH(NOLOCK) ON (bb.RelacaoID = aa.RelacaoID) ' +
		                                'WHERE (bb.objetoID = 999 AND bb.EstadoID = 2 AND  bb.SujeitoID =  ' + userID + ' AND aa.PerfilID IN (525, 619)))  AS PermiteHomologacao, ' +
                                    '(SELECT COUNT(DISTINCT aa.PerfilID) ' +
			                            'FROM RelacoesPesRec_Perfis aa WITH(NOLOCK) ' +
				                            'INNER JOIN RelacoesPesRec bb WITH(NOLOCK) ON (bb.RelacaoID = aa.RelacaoID) ' +
			                            'WHERE (bb.objetoID = 999 AND bb.EstadoID = 2 AND  bb.SujeitoID =  ' + userID + '  AND aa.PerfilID IN (639, 676, 677)))  AS PermiteLogistica ' +
	                            'FROM RelacoesPesRec_Perfis a WITH(NOLOCK) ' +
		                            'INNER JOIN RelacoesPesRec b ON (b.RelacaoID = a.RelacaoID) ' +
	                            'WHERE (b.objetoID = 999 AND b.EstadoID = 2 AND b.SujeitoID =  ' + userID + '  AND a.PerfilID IN (500, 501)) ';

    dsoperfilUsuario.ondatasetcomplete = perfilUsuario_DSC;
    dsoperfilUsuario.Refresh();
}

function perfilUsuario_DSC() {
    glb_PermiteSuporte = dsoperfilUsuario.recordset['PermiteSuporte'].value;
    glb_PermiteHomologacao = dsoperfilUsuario.recordset['PermiteHomologacao'].value;
    glb_PermiteLogistica = dsoperfilUsuario.recordset['PermiteLogistica'].value;

    return null;
}