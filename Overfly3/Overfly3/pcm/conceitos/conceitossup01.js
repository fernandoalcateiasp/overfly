/********************************************************************
conceitossup01.js

Library javascript para o conceitossup01.asp
********************************************************************/
// VARIAVEIS GLOBAIS ************************************************

// Guarda o tipo do registro selecionado para ser usado no select
// que traz o registro de volta na gravacao, pois este form tem
// Identity calculado por trigger
//var glb_ConceitosTipoRegID = null;

// guarda o status corrente na barra de status
var glb_lstStatusInStatusBar = '';

// guarda id do combo de lupa a preencher
var glb_cmbLupaID;
var glb_localTimerInt = null;
var glb_TimerFabricante = null;
var glb_PermiteSuporte = 0;
var glb_PermiteHomologacao = 0;
var glb_Empresa;

// Dados do registro corrente .SQL 
var dsoSup01 = new CDatatransport("dsoSup01");
// Dados dos combos dinamicos (selFabricanteID ,selMarcaID .SQL 
var dsoCmbDynamic01 = new CDatatransport("dsoCmbDynamic01");
var dsoCmbDynamic02 = new CDatatransport("dsoCmbDynamic02");
var dsoCmbDynamic03 = new CDatatransport("dsoCmbDynamic03");
var dsoCmbDynamic04 = new CDatatransport("dsoCmbDynamic04");
var dsoCmbDynamic05 = new CDatatransport("dsoCmbDynamic05");
// Descricao do estado atual do registro .SQL 
var dsoStateMachine = new CDatatransport("dsoStateMachine");
// Dados dos combos estaticos .URL 
var dsoEstaticCmbs = new CDatatransport("dsoEstaticCmbs");
// Dados dos combos de Lupa .SQL 
var dsoCmbsLupa = new CDatatransport("dsoCmbsLupa");
// //@@ Os dsos sao definidos de acordo com o form 
// Dados do Conceito .URL 
var dsoStrConceito = new CDatatransport("dsoStrConceito");
// Dados do Conceito .URL 
var dsoVerificaProduto = new CDatatransport("dsoVerificaProduto");
// verifica��o estados 
var dsoVerificacao = new CDatatransport("dsoVerificacao");
// replica��o dos produtos
var dsoReplicarInformacoes = new CDatatransport('dsoReplicarInformacoes');
// replica��o dos produtos
var dsoReplicar = new CDatatransport('dsoReplicar');
// Permite perfil do �sario
var dsoperfilUsuario = new CDatatransport('dsoperfilUsuario');
// Preenche Auto = 1 de acordo com Replicar da Familia do produto
//var dsoDescricaoAutomatica = new CDatatransport('dsoDescricaoAutomatica');


// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

FUNCOES DA AUTOMACAO:
    putSpecialAttributesInControls()
    prgServerSup(btnClicked)
    prgInterfaceSup(btnClicked)
    optChangedInCmb(cmb)
    btnLupaClicked(btnClicked)
    finalOfSupCascade(btnClicked)
    btnBarClicked(controlBar, btnClicked)
    btnBarNotEspecClicked(controlBar, btnClicked)
    modalInformForm(idElement, param1, param2)
    supInitEditMode()
    formFinishLoad()

FUNCOES DA MAQUINA DE ESTADO:
    stateMachOpened( currEstadoID )
    stateMachBtnOK( currEstadoID, newEstadoID )
    stateMachClosed( oldEstadoID, currEstadoID, bSavedID )

FUNCOES ESPECIFICAS DO FORM:    

FUNCOES DO CARRIER:
    carrierArrived(idElement, idBrowser, param1, param2)
    carrierReturning(idElement, idBrowser, param1, param2)

********************************************************************/

/********************************************************************

// IMPLEMENTACAO DAS FUNCOES

//@@ todas as funcoes abaixo sao particulares de cada form
// conforme o form algumas funcoes devem ser removidas
// e outras serao acrescentadas
********************************************************************/

function window_onload()
{
    //@@ Iniciado carregamento de form, primeira funcao controladora
    initInterfaceLoad(window.top.formName, 0X7);
    
    //@@ array bidimensional de combos ids e campo indice na tabela
    // identificador do combo. O array deve ser null se nao tem combos estaticos
    glb_aStaticCombos = ([['selTipoRegistroID', '1'],
                          ['selClassificacaoID', '2'],
                          ['selProdutoID', '3'],
                          ['selUnidadeID', '4'],
                          ['selPaisID', '5'],
                          ['selImpostoPaisID','5'],
                          ['selPaisID2','5'],
                          ['selNivelGovernoID', '6'],
                          ['selRegraAliquotaID', '7'],
                          ['selMoedaPaisID', '8'],
                          ['selPaisID3', '5']]);

    
    glb_Empresa = getCurrEmpresaData();

    windowOnLoad_1stPart();
        
    //@@ Path dos arquivos inf e pesqlist
    glb_aFilePath = new Array(SYS_PAGESURLROOT + '/pcm/conceitos/conceitosinf01.asp',
                              SYS_PAGESURLROOT + '/pcm/conceitos/conceitospesqlist.asp');                          
        
    //@@ Linka os divs com os correspondentes subforms
    // criando o atributo sfsGrupo nos divs.
    // Os infs nao tem div principal e todos os divs sao secundarios.
    // Parametros:
    // 1o par: id do div principal ou null se nao tem div principal
    // 2opar: array de divs ids dos divs secundarios
    // ou null se nao tem divs secundarios
    // 3o par: array de xxx_ID que identifica o grupo contido no div
    // ou null se o form nao tem div secundarios.
    // Esta funcao tageia o div principal e preenche array
    // dos divs secundarios do arquivo guardando na variavel
    // global do arquivo: glb_aDIVS
    linkDivsAndSubForms('divSup01_01',  ['divSup02_01',
                                         'divSup02_02',
                                         'divSup02_03',
                                         'divSup02_04',
                                         'divSup02_05',
                                         'divSup02_06',
                                         'divSup02_07',
                                         'divSup02_08',
                                         'divSup02_09',
                                         'divSup02_10'],
                                        [301, 302, 303, 304, 306, 307, 308, 309, 319, 320] );

    windowOnLoad_2ndPart();
    
    //@@ Guarda o nome do campo na tabela que contem o id do registro corrente no sup
    glb_sFldIDName = 'ConceitoID';
    
    //@@ Guarda o id do controle (combo) que contem os tipos de registros
    // se nao e aplicavel no form, fazer glb_sCtlTipoRegistroID = '';
    glb_sCtlTipoRegistroID = 'selTipoRegistroID';

    //@@ Guarda o nome do campo na tabela que contem o tipo do registro corrente no sup
    // se nao e aplicavel no form, fazer glb_sFldTipoRegistroName = ''
    glb_sFldTipoRegistroName = 'TipoConceitoID';

    perfilUsuario();
}

function setupPage()
{
    adjustDivs('sup',[[1,'divSup01_01'],
                      [2,'divSup02_01'],
                      [2,'divSup02_02'],
                      [2,'divSup02_03'],
                      [2,'divSup02_04'],
                      [2,'divSup02_05'],
                      [2,'divSup02_06'],
                      [2,'divSup02_07'],
                      [2,'divSup02_08'],
                      [2,'divSup02_09'],
                      [2,'divSup02_10'],
                      [2,'divSup02_08']]);

/****************************** CONCEITO DIV 1 - divSup01_01 **********************************/
    adjustElementsInForm([['lblRegistroID','txtRegistroID',10,1],
                          ['lblEstadoID','txtEstadoID',2,1],
                          ['lblConceito','txtConceito',25,1],
                          ['lblTipoRegistroID', 'selTipoRegistroID', 20, 1]]);
    
/************************* CONCEITO ABSTRATO DIV 2 - divSup02_01 *****************************/
    adjustElementsInForm([['lblNivel','selNivel',5,2]],['lblHrConceitoAbstrato','hrConceitoAbstrato']);

/************************* CONCEITO CONCRETO DIV 2 - divSup02_02 *****************************/
    adjustElementsInForm([['lblConceitoAbreviado_02', 'txtConceitoAbreviado_02', 5, 2],
                          ['lblOrdemConcreto', 'txtOrdemConcreto', 5, 2],
                          ['lblEhConfiguravel', 'chkEhConfiguravel', 3, 2],
                          ['lblEhServico', 'chkEhServico', 3, 2],
                          ['lblEhMicro', 'chkEhMicro', 3, 2],
                          ['lblEhGabinete', 'chkEhGabinete', 3, 2],
                          ['lblFamiliaPrincipal', 'chkFamiliaPrincipal', 3, 2],
                          ['lblAceitaMaisProdutos', 'chkAceitaMaisProdutos', 3, 2],                          
                          ['lblBloqueiaMicroSemEste', 'chkBloqueiaMicroSemEste', 3, 2],
                          ['lblPeso', 'chkPeso', 3, 2],
                          ['lblHomologavel', 'chkHomologavel', 3, 2],
                          ['lblReplicar', 'chkReplicar', 3, 2],
                          ['lblDescricaoAuto', 'chkDescricaoAuto', 3, 2],
                          ['lblTamanho', 'selTamanho', 5, 2],
                          ['lblImagensRequeridas', 'selImagensRequeridas', 5, 2],
                          ['lblValorEstoqueSeguro', 'txtValorEstoqueSeguro', 11, 2],
                          ['lblSinonimos', 'txtSinonimos', 52, 3],
                          ['lblObservacao', 'txtObservacao', 41, 3]],
                          ['lblHrConceitoConcreto', 'hrConceitoConcreto']);

/***************************** PRODUTO DIV 2 - divSup02_03 **********************************/
    adjustElementsInForm([['lblClassificacaoID','selClassificacaoID',20,2],
                          ['lblProdutoID','selProdutoID',25,2] ,
                          ['lblFabricanteID','selFabricanteID',20,2],
                          ['btnFindFabricante','btn',24,2],
                          ['lblMarcaID','selMarcaID',20,2],
                          ['btnFindMarca','btn',24,2],
                          ['lblLinhaProdutoID','selLinhaProdutoID',20,2],
                          ['btnFindLinhaProduto','btn',24,2],
                          ['lblModelo','txtModelo',18,3],
                          ['lblSerie','txtSerie',25,3],
                          ['lblDescricao','txtDescricao',38,3],
                          ['lblDescricaoComplementar','txtDescricaoComplementar',38,3],
                          ['lblPartNumber', 'txtPartNumber', 20, 4],
                          ['lblUnidadeID','selUnidadeID',12,4],
                          ['lblTamanho2','selTamanho2',5,4],
                          ['lblInternacional','chkInternacional',3,4],
                          ['lblAuto','chkAuto',3,4],
                          ['lblControlaEstoque','chkControlaEstoque',3,4],
                          ['lblProdutoSeparavel','chkProdutoSeparavel',3,4],
                          ['lblNumerosSerieAlternativos','txtNumerosSerieAlternativos',3,4],
                          ['lblControle','chkControle',3,4],
                          ['lblNumeroSerieEspecifico','chkNumeroSerieEspecifico',3,4],
                          ['lblEtiquetaPropria','chkEtiquetaPropria',3,4],
                          ['lblImagem_Produto','chkImagem_Produto',3,4, FONT_WIDTH * 2.5],
                          ['lblIdentificadoresOK','chkIdentificadoresOK',3,4],
                          ['lblHomologacaoOK','chkHomologacaoOK',3,4,-3],
                          ['lblTemCodigoGTIN','chkTemCodigoGTIN',3,4,-3],
                          ['lblCad', 'txtCad', 7, 4],
                          ['lblCodigoBNDES', 'txtCodigoBNDES', 10, 4],
                          ['lblDescricaoFiscal','txtDescricaoFiscal',90,5,0,-5]]);

/***************************** MARCA DIV 2 - divSup02_04 ***********************************/
    adjustElementsInForm([['lblConceitoAbreviado_04', 'txtConceitoAbreviado_04', 5, 2],
                          ['lblDescricaoModelo', 'chkDescricaoModelo', 3, 2],
                          ['lblDescricaoPartnumber', 'chkDescricaoPartnumber', 3, 2],
                          ['lblDescricaoInicio', 'chkDescricaoInicio', 3, 2],
    	                  ['lblPublica', 'chkPublica', 3, 2],
    	                  ['lbldestacainternet', 'chkdestacainternet', 3, 2],
    	                  ['lblImagem_Marca', 'chkImagem_Marca', 3, 2],
    	                  ['lblSinonimos2','txtSinonimos2',52,2]],['lblHrMarca','hrMarca']);

/**************************** IMPOSTO DIV 2 - divSup02_05 **********************************/
	adjustElementsInForm([['lblOrdem','txtOrdem',3,2],
	                      ['lblImposto','txtImposto',7,2],
	                      ['lblPaisID','selPaisID',25,2],
	                      ['lblNivelGovernoID','selNivelGovernoID',10,2],
	                      ['lblIncidePreco','chkIncidePreco',3,2,-12],
	                      ['lblInclusoPreco','chkInclusoPreco',3,2],
	                      ['lblEsquemaCredito','chkEsquemaCredito',3,2,-7],
	                      ['lblAlteraAliquota','chkAlteraAliquota',3,2,-4],
	                      ['lblDestacaNota','chkDestacaNota',3,2,-4],
	                      ['lblAIC','chkAIC',3,2,-4],
	                      ['lblRegraAliquotaID','selRegraAliquotaID',9,2]],['lblHrImposto','hrImposto']);
	                            
/********************* GRUPO DE IMPOSTO  DIV 2 - divSup02_06 **********************************/
    adjustElementsInForm([['lblImpostoOrdem','txtImpostoOrdem',3,2],
                          ['lblImpostoPaisID','selImpostoPaisID',25,2],
                          ['lblGrupoAlternativoID','selGrupoAlternativoID',25,2]],['lblHrGrupodeImposto','hrGrupodeImposto']);

/********************* MOEDA  DIV 2 - divSup02_07 **********************************/
    adjustElementsInForm([['lblMoedaPaisID','selMoedaPaisID',25,2],
                          ['lblCodigo','txtCodigo',4,2],
                          ['lblSimbolo', 'txtSimbolo', 6, 2],
                          ['lblCodigoConceito', 'txtCodigoConceito', 5, 2]], ['lblHrMoeda', 'hrMoeda']);

/********************* CLASSIFICA��O FISCAL  DIV 2 - divSup02_08 **********************************/
    adjustElementsInForm([['lblPaisID2','selPaisID2',25,2],
                          ['lblClassificacaoFiscal','txtClassificacaoFiscal',12,2],
                          ['lblMPBem', 'chkMPBem', 3, 2],
                          ['lblObservacaoImposto', 'txtObservacaoImposto', 30, 2]],['lblHrClassificacaoFiscal','hrClassificacaoFiscal']);

/********************* NBS  DIV 2 - divSup02_09 **********************************/
    adjustElementsInForm([['lblPaisID3', 'selPaisID3', 25, 2],
                          ['lblNBS', 'txtNBS', 12, 2],
                          ['lblObservacaoNBS', 'txtObservacaoNBS', 30, 2]], ['lblHrNBS', 'hrNBS']);

/********************* SVF  DIV 2 - divSup02_10 **********************************/
    adjustElementsInForm([['lblObservacaoConceito', 'txtObservacaoConceito', 41, 2]], ['lblSvf', 'hrSvf']);

    // ajusta manualmente o combo selNivel
    var aOptions = new Array (['1','1'],
                              ['2','2']);

    var oOption;
    for (i=0; i<aOptions.length; i++)
    {
        oOption = document.createElement("OPTION");
        oOption.text = aOptions[i][1];
        oOption.value = aOptions[i][0];
        selNivel.add(oOption);
    }

    //txtCad.readonly = true;
    txtCad.disabled = true;
    chkAuto.disabled = true;
}

// CARRIER **********************************************************

/********************************************************************
DISPARAR UM CARRIER:
Usar a seguinte funcao:

sendJSCarrier(getHtmlId(),param1, param2);

Onde:
getHtmlId() - fixo, remete o id do html que disparou o carrier.
param1      - qualquer coisa. De uso do programador.
param2      - qualquer coisa. De uso do programador.

********************************************************************/

/********************************************************************
Funcao disparada pelo frame work.
CARRIER CHEGANDO NO ARQUIVO.
Executa quando um carrier e disparado por outro form.

Parametros: 
idElement   - id do html que disparou o carrier
idBrowser   - id do browser que disparou o carrier
param1      - parametro um trazido pelo carrier
param2      - parametro dois trazido pelo carrier

Retorno:
Se intercepta sempre retornar o seguinte array:
array[0] = getHtmlId() ou null
array[1] = qualquer coisa ou null
array[2] = qualquer coisa ou null
Se nao intercepta retornar null
********************************************************************/
function carrierArrived(idElement, idBrowser, param1, param2)
{
    if (param1 == 'SHOWCONCEITO')
    {
        // Da automacao, deve constar de todos os ifs do carrierArrived
        if ( __currFormState() != 0 )
            return null;
            
        // param2 - traz array:
            // o id da empresa do form que mandou
            // o id do registro a abrir
        // No sendJSMessage abaixo:
        // param2[1] = null significa que nao deve paginar
        // param2[2] = null significa que nao ha array de ids
       
        if (param2[0] != glb_Empresa[0])
            return null;

        window.top.focus();
        
        // Exclusivo para exibir detalhe em form
        showDetailByCarrier(param2[1]);
        
        return new Array(getHtmlId(), null, null);          
    }
    // Programador alterar conforme necessario
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
CARRIER RETORNANDO AO ARQUIVO QUE O DISPAROU.
Executa quando um carrier e disparado por este form e apos executar
nos demais forms abertos, retorna a este form.

Parametros: 
idElement   - string id do html que interceptou o carrier
idBrowser   - string id do browser que contem o form que interceptou
              o carrier
param1      - parametro um trazido pelo carrier
param2      - parametro dois trazido pelo carrier

Retorno:
Sempre retorna null
********************************************************************/
function carrierReturning(idElement, idBrowser, param1, param2)
{

    // Nao mexer - Inicio de automacao ==============================
    return null;
    // Final de Nao mexer - Inicio de automacao =====================
}

// FINAL DE CARRIER *************************************************

/********************************************************************
Funcao disparada pelo frame work.
Coloca atributos em campos para serem manobrados graficamente
(esconde/mostra e habilita desabilita) pelo framework.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function putSpecialAttributesInControls()
{
    //@@
    
    // Tabela de atributos especiais:
    // 1. fnShowHide - seta funcao do programador que o framework executa
    //                 ao mostrar/esconder o controle.
    // 2. prog_SHHD  - (true) retira do frame work a operacao de mostrar/esconder
    //                 habiitar/desabilitar o controle e transfere para
    //                 o programador. False ou null devolve para o framework.
    
    // os campos abaixo sao visiveis ou nao em funcao do value dos combos
    // selClassificacaoID e selTipoRecursoID
    // (nClass == 11) && (nTipoRec == 2)

    // evento em campo

    chkImagem_Produto.disabled = true;
    chkImagem_Marca.disabled = true;
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares de servidor
do e preenchimentos de combos do sup, quando o usuario clica
um botao da barra do sup.
           
Parametros: 
ultimo botao clicado na barra de botoes superior

Retorno:
nenhum
********************************************************************/
function prgServerSup(btnClicked)
{
    // chama funcao do programador que inicia o carregamento
    // de dados dos combos dinamicos (selSujeitoID e selObjetoID)
    glb_BtnFromFramWork = btnClicked;
    if ((btnClicked == 'SUPDET') ||
         (btnClicked == 'SUPREFR') ||
         (btnClicked == 'SUPANT') ||
         (btnClicked == 'SUPSEG')) 
    {
		chkImagem_Produto.checked = dsoSup01.recordset['TemImagem'].value;
		lblImagem_Produto.title = dsoSup01.recordset['ImagemResumo'].value;
		chkImagem_Produto.title = dsoSup01.recordset['ImagemResumo'].value;

		chkImagem_Marca.checked = dsoSup01.recordset['TemImagem'].value;
		lblImagem_Marca.title = dsoSup01.recordset['ImagemResumo'].value;
		chkImagem_Marca.title = dsoSup01.recordset['ImagemResumo'].value;

		txtCad.title = 'Conformidade do produto com os seguintes requisitos:\n' +
		                'Caracter�sticas: ' + dsoSup01.recordset['Conformidade_Carac'].value + '%\n' +
		                'Argumentos de Venda: ' + dsoSup01.recordset['Conformidade_AV'].value + '%\n' +
		                'Imagens: ' + dsoSup01.recordset['Conformidade_Imagens'].value + '%\n' +
		                'Pesos e Medidas: ' + dsoSup01.recordset['Conformidade_PM'].value + '%\n' +
		                'Identificadores: ' + dsoSup01.recordset['Conformidade_Ident'].value + '%\n' +
		                'Especifica��es T�cnicas: ' + dsoSup01.recordset['Conformidade_Espec'].value + '%';

		controleCampos(btnClicked);
		startDynamicCmbs();
    }
    else    
        // volta para a automacao    
	    finalOfSupCascade(glb_BtnFromFramWork);
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao tem que ser chamada na ultima (ou ultimas) funcoes de
retorno de dados do servidor, feitas pelo programador.
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function finalOfSupCascade(btnClicked)
{
    // Nao mexer - Inicio de automacao ==============================
    // Invoca a funcao lockAndOrSvrInf_SYS() do detailinf.js
    // para as operacoes de bancos de dados da automacao.
	sendJSMessage(getHtmlId(), JS_INFSYS, 'ENDPROGSERVERSUP', btnClicked);
	// Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares
do form, necessarias apos um changeDiv. NAO PODE CONTER NENHUMA
OPERACAO DE BANCO DE DADOS.
           
Parametros: 
ultimo botao clicado na barra de botoes superior

Retorno:
nenhum
********************************************************************/
function prgInterfaceSup(btnClicked)
{
    //@@
    // Chama funcoes particulares do form que forem necessarias
    // depois de trocar o div
    
    showBtnsEspecControlBar('sup', true, [1,1,1,1,1,1,0,1]);
    tipsBtnsEspecControlBar('sup', ['Documentos', 'Relat�rios', 'Procedimento', 'Imagem NCM', 'N�meros de s�rie', 'NCM', '', 'Replicar Cadastro']);

    clearAndLockCmbsDynamics(btnClicked);
    
    if (btnClicked == 'SUPINCL')
    {
        if (selTipoRegistroID.length == 1) {
            dsoSup01.recordset[glb_sFldTipoRegistroName].value = selTipoRegistroID.options[0].value;

            selTipoRegistroID.selectedIndex = 0;   

            if (selTipoRegistroID.value == 303) {
                //dsoSup01.recordset['DescricaoAutomatica'].value = true;
                dsoSup01.recordset['ControlaEstoque'].value = true;
                dsoSup01.recordset['ProdutoSeparavel'].value = true;

                putSpecialAttributesInControls();
            }
            adjustSupInterface();
        }

        direitoCampos();
        controleCampos(btnClicked);
    }
    else
    {
        setLabelProduto(dsoSup01.recordset['ProdutoID'].value);
        setLabelFabricante(dsoSup01.recordset['FabricanteID'].value);
        setLabelMarca(dsoSup01.recordset['MarcaID'].value);
        setLabelLinhaProduto(dsoSup01.recordset['LinhaProdutoID'].value);
        setLabelTipoRegistro(dsoSup01.recordset['TipoConceitoID'].value);
        setLabelClassificacao(dsoSup01.recordset['ClassificacaoID'].value);
    }

    // Nao mexer - Inicio de automacao ==============================
    sendJSMessage(getHtmlId(), JS_INFSYS, 'ENDCHANGEDIVSUP', btnClicked);
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao recebe todos os onchange de combos do form e executa as
funcoes apropriadas definidas pelo programador e aqui invocadas. 
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function optChangedInCmb(cmb)
{
    if ( glb_TimerFabricante != null )
    {
        window.clearInterval(glb_TimerFabricante);
        glb_TimerFabricante = null;
    }
    
    var cmbID = cmb.id;
    //@@ Os if e os else if abaixo sao particulares de cada form
    // as operacoes foram executadas manualmente pelo usuario
    if ( cmbID == 'selClassificacaoID' )
        setLabelClassificacao(cmb.value);   
    else if ( cmbID == 'selProdutoID' )
    {
        clearComboEx(['selFabricanteID','selMarcaID','selLinhaProdutoID']);
        selFabricanteID.disabled = true;
        selMarcaID.disabled = true;
        selLinhaProdutoID.disabled = true;
        lockBtnLupa(btnFindFabricante, true);
        lockBtnLupa(btnFindMarca, true);
        lockBtnLupa(btnFindLinhaProduto, true);
        dsoSup01.recordset['FabricanteID'].value = null;
        dsoSup01.recordset['MarcaID'].value = null;
        dsoSup01.recordset['LinhaProdutoID'].value = null;

        if ( cmbID.selectedIndex != -1 )
        {            
            lockBtnLupa(btnFindFabricante, false);
            getDataAndLoadCmbsLupaNoModal('selFabricanteID');            
        }
        
        setLabelProduto(cmb.value);   
        setLabelFabricante(); 
        setLabelMarca();
        
        //setConnection(dsoDescricaoAutomatica);
        //dsoDescricaoAutomatica.SQL = "SELECT Replicar, PermiteAlterarDescricaoAutomatica FROM Conceitos WITH(NOLOCK) WHERE ConceitoID = " + selProdutoID.value;
        //dsoDescricaoAutomatica.ondatasetcomplete = dsoDescricaoAutomatica_DSC;
        //dsoDescricaoAutomatica.Refresh();

        mountAndFillTxtConceito();
    }
    else if ( cmbID == 'selFabricanteID' )
    {
        clearComboEx(['selMarcaID','selLinhaProdutoID']);
        selMarcaID.disabled = true;
        selLinhaProdutoID.disabled = true;
        lockBtnLupa(btnFindMarca, true);
        lockBtnLupa(btnFindLinhaProduto, true);

        if ( cmbID.selectedIndex != -1 )
        {
            lockBtnLupa(btnFindMarca, false);        
            getDataAndLoadCmbsLupaNoModal('selMarcaID');            
        }
        
        setLabelFabricante(cmb.value);
        setLabelMarca();
        setLabelLinhaProduto();
    }

    if ( cmbID == 'selMarcaID' )
    {
        clearComboEx(['selLinhaProdutoID']);
        selLinhaProdutoID.disabled = true;
        lockBtnLupa(btnFindLinhaProduto, true);

        if ( cmbID.selectedIndex != -1 )
        {
            lockBtnLupa(btnFindLinhaProduto, false);        
            getDataAndLoadCmbsLupaNoModal('selLinhaProdutoID');            
        }

        setLabelMarca(cmb.value);
        mountAndFillTxtConceito();
    }

    if ( cmbID == 'selLinhaProdutoID' )
        setLabelLinhaProduto(cmb.value);

    // Troca a interface do sup em funcao de item selecionado
    // no combo de tipo.
    // Se for necessario ao programador, repetir o if abaixo
    // definido, antes deste comentario.
    // Nao mexer - Inicio de automacao ==============================
    if (cmbID == 'selTipoRegistroID') {

        if (glb_btnCtlSup == 'SUPINCL')
            selImagensRequeridas.selectedIndex = 3;


        adjustSupInterface();

    }
    // Final de Nao mexer - Inicio de automacao =====================
}

//function dsoDescricaoAutomatica_DSC() {
//    chkAuto.checked = dsoDescricaoAutomatica.recordset['Replicar'].value;

//    controleCamposDescricao();
//    controleCamposDescricao_EnableDisable();
//}

/********************************************************************
Funcao disparada pelo frame work.
Dispara logo apos abrir a maquina de estado. 
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
nenhum
********************************************************************/
function stateMachOpened( currEstadoID )
{

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou OK na maquina de estado
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
false - automacao prossegue
true  - automacao para        
       
Nota:
Se o programador retornar diferente de null nesta funcao,
o sistema permanecera travado, ao final do seu codigo o programador
dever� chamar a seguinte funcao da automacao:
stateMachSupExec(action)
onde action = 'OK' - o novo estado sera gravado
              'CANC' - a gravacao sera cancelada
para prosseguir a automacao
********************************************************************/
function stateMachBtnOK( currEstadoID, newEstadoID )
{
    verifyConceitosInServer(currEstadoID, newEstadoID);
    return true;
}

function verifyConceitosInServer(currEstadoID, newEstadoID)
{
    var nFinanceiroID = dsoSup01.recordset['ConceitoID'].value;
    var strPars = new String();

    strPars = '?nConceitoID='+escape(nFinanceiroID);
    strPars += '&nEstadoDeID='+escape(currEstadoID);
    strPars += '&nEstadoParaID='+escape(newEstadoID);    

    dsoVerificacao.URL = SYS_ASPURLROOT + '/pcm/conceitos/serverside/verificacaoconceitos.aspx' + strPars;
    dsoVerificacao.ondatasetcomplete = verifyConceitosInServer_DSC;
    dsoVerificacao.refresh();
}

/********************************************************************
Retorno do servidor - Verificacoes do Financeiro
********************************************************************/
function verifyConceitosInServer_DSC()
{
    /*var sResultado = dsoVerificacao.recordset['Resultado'].value;

    if (sResultado != null)
    {
        stateMachSupExec('CANC');
        window.top.overflyGen.Alert(sResultado);
    }
    else
        stateMachSupExec('OK');*/

    var sMensagem = (dsoVerificacao.recordset['Mensagem'].value == null ? '' : dsoVerificacao.recordset['Mensagem'].value);
    var nResultado = (dsoVerificacao.recordset['Resultado'].value == null ? 0 : dsoVerificacao.recordset['Resultado'].value);

    if (nResultado == 0)
    {
        if (sMensagem != '') 
        {
            if (window.top.overflyGen.Alert(sMensagem) == 0)
                return null;
        }

        stateMachSupExec('OK');
    }
    else if (nResultado == 1)
    {
        var _retConf = window.top.overflyGen.Confirm(sMensagem);

        if (_retConf == 0)
            return null;
        else if (_retConf == 1)
            stateMachSupExec('OK');
        else
            stateMachSupExec('CANC');
    }  
    else if (nResultado == 2)
    {
        if (sMensagem != '')
        {
            if (window.top.overflyGen.Alert(sMensagem) == 0)
                return null;
        }
        stateMachSupExec('CANC');
    }
}    

/********************************************************************
Funcao disparada pelo frame work.
Dispara se o usuario trocou o estado.
Dispara logo apos fechar a maquina de estado. 
           
Parametros: 
oldEstadoID     - previo EstadoID do registro
currEstadoID    - novo EstadoID do registro
bSavedID        - o novo estadoID foi gravado no banco (true/false)

Retorno:
nenhum
********************************************************************/
function stateMachClosed( oldEstadoID, currEstadoID, bSavedID )
{

}

/********************************************************************
Funcao disparada pelo frame work.
Propaga clique e botao de lupa que nao seja proprietario e
alternativo.
           
Parametros: 
btnClicked      - referencia ao botao de lupa clicado

Retorno:
nenhum
********************************************************************/
function btnLupaClicked(btnClicked)
{
    //@@
    var cmbID = '';
    
    if ( btnClicked.id == 'btnFindFabricante' )
    {   
        cmbID = 'selFabricanteID';
        getDataAndLoadCmbsLupaNoModal('selFabricanteID');
    }
    else if ( btnClicked.id == 'btnFindMarca' )
    {   
        cmbID = 'selMarcaID';
        getDataAndLoadCmbsLupaNoModal('selMarcaID');
    }
    else if ( btnClicked.id == 'btnFindLinhaProduto' )
    {   
        cmbID = 'selLinhaProdutoID';
        getDataAndLoadCmbsLupaNoModal('selLinhaProdutoID');
    }
    // No caso espec�fico do Form de Conceitos, a lupa n�o abre janela modal
    // para os combos de Fabricante e Marca
    else
    {
        // Nao mexer - Inicio de automacao ==============================
        // Invoca janela modal
        loadModalOfLupa(cmbID, null);
        // Final de Nao mexer - Inicio de automacao =====================
    }
}

/********************************************************************
Funcao do programador
Disparada pela funcao prgServerSup()
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function startDynamicCmbs()
{
    // controla o retorno do servidor dos dados de combos dinamicos
    // (selFabricanteID, selMarcaID, selGrupoAlternativoID, selLinhaProdutoID)
    glb_CounterCmbsDynamics = 5;
    
    // parametrizacao do dso dsoCmbDynamic01 (designado para selFabricanteID)
    setConnection(dsoCmbDynamic01);
                              
    dsoCmbDynamic01.SQL = 'SELECT PessoaID,Fantasia ' +
                          'FROM Pessoas WITH(NOLOCK) ' +
                          'WHERE PessoaID = ' +  dsoSup01.recordset['FabricanteID'].value;
    dsoCmbDynamic01.ondatasetcomplete = dsoCmbDynamic_DSC;
    dsoCmbDynamic01.Refresh();

    // parametrizacao do dso dsoCmbDynamic02 (designado para selMarcaID)
    setConnection(dsoCmbDynamic02);
    
    dsoCmbDynamic02.SQL = 'SELECT ConceitoID,Conceito ' +
                          'FROM Conceitos WITH(NOLOCK) ' +
                          'WHERE ConceitoID = ' +  dsoSup01.recordset['MarcaID'].value;
    dsoCmbDynamic02.ondatasetcomplete = dsoCmbDynamic_DSC;
    dsoCmbDynamic02.Refresh();

    // parametrizacao do dso dsoCmbDynamic03 (designado para selGrupoALternativoID)
    setConnection(dsoCmbDynamic03);
    
    dsoCmbDynamic03.SQL = 'SELECT 0 AS ConceitoID, ' + '\'' + ' ' + '\'' + ' ' + 'AS Conceito ' +
                          'UNION ALL SELECT ConceitoID,Conceito ' +
                          'FROM Conceitos WITH(NOLOCK) ' +
                          'WHERE EstadoID = 2 AND TipoConceitoID = 307 ' +
                          'AND PaisID = ' + glb_Empresa[1] + ' ' +
                          'ORDER BY Conceito';
    
    dsoCmbDynamic03.ondatasetcomplete = dsoCmbDynamic_DSC;
    dsoCmbDynamic03.Refresh();

    // parametrizacao do dso dsoCmbDynamic04 (designado para selLinhaProdutoID)
    setConnection(dsoCmbDynamic04);
    
    dsoCmbDynamic04.SQL = 'SELECT ConceitoID,Conceito ' +
                          'FROM Conceitos WITH(NOLOCK) ' +
                          'WHERE ConceitoID = ' + dsoSup01.recordset['LinhaProdutoID'].value;
    
    dsoCmbDynamic04.ondatasetcomplete = dsoCmbDynamic_DSC;
    dsoCmbDynamic04.Refresh();

    // parametrizacao do dso dsoCmbDynamic05 (designado para selGrupoALternativoID)
    setConnection(dsoCmbDynamic05);
    
    dsoCmbDynamic05.SQL = 'SELECT (ISNULL(MAX(TamanhoMaximo), 0)) AS TipoMaximo ' +
            'FROM LocalizacoesEstoque a WITH(NOLOCK) ' +
            'WHERE (a.EstadoID = 2 AND a.EmpresaID = ' + glb_Empresa[0] + ')';
    
    dsoCmbDynamic05.ondatasetcomplete = dsoCmbDynamic_DSC;
    dsoCmbDynamic05.Refresh();
}

/********************************************************************
Funcao disparada pelo programado.
Funcao do ondatasetcomplete disparada pela funcao prgServerSup()
           
Parametros:     
Nenhum  

Retorno:
nenhum
********************************************************************/
function dsoCmbDynamic_DSC()
{
    var optionStr, optionValue;
    var oldDataSrc;
    var oldDataFld;
    var aCmbsDynamics = [selFabricanteID,selMarcaID,selGrupoAlternativoID,selLinhaProdutoID,selTamanho,selTamanho2];
    var aDSOsDunamics = [dsoCmbDynamic01,dsoCmbDynamic02,dsoCmbDynamic03,dsoCmbDynamic04,dsoCmbDynamic05,dsoCmbDynamic05];
    var i = 0;
    var j = 0;
    var nTipoMaximo = 0;

    // Inicia o carregamento de combos dinamicos (selFabricanteID, selMarcaID)
    //
    clearComboEx(['selFabricanteID','selMarcaID','selGrupoAlternativoID','selLinhaProdutoID','selTamanho','selTamanho2']);
    
    glb_CounterCmbsDynamics --;
    
    if (glb_CounterCmbsDynamics == 0)
    {
        for (i=0; i<=4; i++)
        {
            if (i < 4)
            {
				while (! aDSOsDunamics[i].recordset.EOF )
				{
					if (i == 0)
					{
						optionStr = aDSOsDunamics[i].recordset['Fantasia'].value;
	        			optionValue = aDSOsDunamics[i].recordset['PessoaID'].value;
	        		}
	        		else if (i == 1)
	        		{
						optionStr = aDSOsDunamics[i].recordset['Conceito'].value;
	        			optionValue = aDSOsDunamics[i].recordset['ConceitoID'].value;
	        		}
	        		else if (i == 2)
	        		{
						optionStr = aDSOsDunamics[i].recordset['Conceito'].value;
	        			optionValue = aDSOsDunamics[i].recordset['ConceitoID'].value;
	        		}
	        		else if (i == 3)
	        		{
						optionStr = aDSOsDunamics[i].recordset['Conceito'].value;
	        			optionValue = aDSOsDunamics[i].recordset['ConceitoID'].value;
	        		}
					var oOption = document.createElement("OPTION");
					oOption.text = optionStr;
					oOption.value = optionValue;
					aCmbsDynamics[i].add(oOption);
					aDSOsDunamics[i].recordset.MoveNext();
				}
            }
            else
            {
				if ( !((aDSOsDunamics[i].recordset.BOF) && (aDSOsDunamics[i].recordset.EOF)) )
					nTipoMaximo = aDSOsDunamics[i].recordset['TipoMaximo'].value;

				for (j=1; j<=nTipoMaximo; j++)
				{
					optionStr = j;
	        		optionValue = j;
					var oOption = document.createElement("OPTION");
					oOption.text = optionStr;
					oOption.value = optionValue;
					aCmbsDynamics[i].add(oOption);
					
					var oOption2 = document.createElement("OPTION");
					oOption2.text = optionStr;
					oOption2.value = optionValue;
					aCmbsDynamics[i+1].add(oOption2);
				}
            }
        }

        // volta para a automacao
	    finalOfSupCascade(glb_BtnFromFramWork);
	}
	return null;
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked)
{
    if ( controlBar == 'SUP' )
    {
        // Documentos
        if ((controlBar == 'SUP') && (btnClicked == 1)) {
            __openModalDocumentos(controlBar, dsoSup01.recordset.Fields[0].value);
        }
        // usuario clicou botao imprimir
        if (btnClicked == 2)
            openModalPrint();
        else if (btnClicked == 3)
            window.top.openModalControleDocumento('S', '', 710, null, '', 'T');
        else if (btnClicked == 4)
            loadModalImagem();
        else if (btnClicked == 6)
            detalhaNCMSuperior();
        else if (btnClicked == 8)
            replicarCadastroInformacoes();

   }
}

/********************************************************************
********************************************************************/
function detalhaNCMSuperior()
{
	var Tipo = parseInt(selTipoRegistroID.value);
	var NCM;
	
	if(Tipo == 309) {
		NCM = parseInt(txtClassificacaoFiscal.value);
		NCM = parseInt(NCM/10000);
		
		strPars = '?sNCM=' + NCM;
		htmlPath = SYS_ASPURLROOT + '/pcm/conceitos/serverside/imagemncm.aspx' + strPars;
		window.open(htmlPath);
	} else {
		alert("O imposto escolhido n�o � uma classifica��o fiscal.");
	}
	
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou um botao nao especifico do control bar.

Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - Um dos seguintes botoes:
                - Avancar, retroagir, incluir, alterar, estado, 
                  excluir, OK, cancelar refresh, anterior e proximo. 
                  (Testar a string que vem).

Retornos:
null            - a automacao prossegue. Qualquer outro retorno a
                  automacao nao prossegue.
********************************************************************/
function btnBarNotEspecClicked(controlBar, btnClicked)
{
    if (btnClicked == 'SUPINCL')
    {
        // Atualiza nomes dos Labels
        setLabelProduto();
        setLabelFabricante();
        setLabelMarca();
        setLabelTipoRegistro();
        setLabelClassificacao();
    }

    var bReturn = controleCampos(btnClicked);

    if (bReturn != null)
        return bReturn;
        
    // Para prosseguir a automacao retornar null
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2)
{
    // Modal de impressao
    if ( idElement.toUpperCase() == 'MODALPRINTHTML' )    
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
           
            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }    
    }
    if (idElement.toUpperCase() == 'MODALCONTROLEDOCUMENTOHTML')
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    // Imagens
    if ( (idElement.toUpperCase() == 'MODALGERIMAGENSHTML')||
          (idElement.toUpperCase() == 'MODALSHOWIMAGENSHTML'))
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    // Modal de documentos
    else if (idElement.toUpperCase() == 'MODALDOCUMENTOSHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Informa que o sup esta em modo de edicao e a interface ja esta
destravada.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function supInitEditMode() {

     // chamada abaixo necessaria para prosseguir a automacao
    // mover para final de operacoes de servidor se for o caso

    // Habilita desabilita campo frete chkFrete
    direitoCampos();

    controleCampos('ModoEdicao');

    supInitEditMode_Continue();
}
/********************************************************************
Funcao disparada pelo frame work.
Form carregou e vai mostrar a interface.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function formFinishLoad()
{
    // seta botao de impressao
    // especBtnIsPrintBtn('sup', 1);
}

/********************************************************************
Funcao criada pelo programador.
Recolhe dados no servidor e preenche combos de lupa sem janela modal.

Parametro:
idCombo         - id do combo a preencher

Retorno:
nenhum
********************************************************************/
function getDataAndLoadCmbsLupaNoModal(idCombo)
{
    glb_cmbLupaID = idCombo;
        
    var sParam1 = '';
    var sParam2 = '';
    
    // Combo de Fabricante depende do combo do produto
    if ( idCombo == 'selFabricanteID' )
        sParam1 = selProdutoID.value;
         
    else if ( idCombo == 'selMarcaID' )
        sParam1 = selFabricanteID.value;    
            
    else if ( idCombo == 'selLinhaProdutoID' )
        sParam1 = selMarcaID.value;    

  	if ( sParam1 == null )
	{
	    clearComboEx([idCombo]);
	    document.getElementById(idCombo).disabled = true;
	    return null;
	}

    lockInterface(true);
    
    glb_lstStatusInStatusBar = getDataInStatusBar('child', 'cellMode');
    writeInStatusBar('child', 'cellMode', 'Processando', true);
    
    var strPars = new String();
    strPars = '?';
    strPars += 'sComboID=';
    strPars += escape(idCombo.toString());
    strPars += '&sParam1=';
    strPars += escape(sParam1.toString());
    strPars += '&sParam2=';
    strPars += escape(sParam2.toString());
    dsoCmbsLupa.URL = SYS_ASPURLROOT + '/pcm/conceitos/serverside/fillcomboslupafabmarca.aspx'+strPars;
    dsoCmbsLupa.ondatasetcomplete = getDataAndLoadCmbsLupaNoModal_DSC;
    dsoCmbsLupa.refresh();

}

/********************************************************************
Funcao criada pelo programador.
Recebe dados no servidor e preenche combos de lupa sem janela modal.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function getDataAndLoadCmbsLupaNoModal_DSC()
{
    var cmbRef;
    cmbRef = document.getElementById(glb_cmbLupaID);

    clearComboEx([glb_cmbLupaID]);
            	    
    var oldSrc = cmbRef.dataSrc;
    var oldDtf = cmbRef.dataFld;
    cmbRef.dataSrc = '';
    cmbRef.dataFld = '';

    var optionStr,optionValue;
    
    while (!dsoCmbsLupa.recordset.EOF)
    {
        var oOption = document.createElement("OPTION");
        oOption.text = dsoCmbsLupa.recordset['fldName'].value;
        oOption.value = dsoCmbsLupa.recordset['fldID'].value;
        cmbRef.add(oOption);
        dsoCmbsLupa.recordset.MoveNext();
    }
            	    
    cmbRef.selectedIndex = -1;

    cmbRef.dataSrc = oldSrc;
    cmbRef.dataFld = oldDtf;
                
    // destrava a interface
    lockInterface(false);
    
    cmbRef.disabled = true;
    
    if ( cmbRef.options.length != 0 )
    {
        //seleciona opcao no combo
        if ( cmbRef.id == 'selMarcaID' )
        {
			if (selMarcaID.length == 1)
				setValueInControlLinked(cmbRef, dsoSup01, null);
		}
		
        //seleciona opcao no combo
        if ( cmbRef.id == 'selLinhaProdutoID' )
        {
			if (selLinhaProdutoID.length == 1)
				setValueInControlLinked(cmbRef, dsoSup01, null);
		}

        // destrava e coloca foco no combo se tem options
        cmbRef.disabled = false;
        cmbRef.focus();
    }

    // escreve na barra de status
    if (glb_lstStatusInStatusBar != '')
        writeInStatusBar('child', 'cellMode', glb_lstStatusInStatusBar);
        
	if (glb_cmbLupaID.toUpperCase() == 'SELFABRICANTEID')
	{
		if (selFabricanteID.length == 1)
		{
			cmbRef.dataSrc = '';
			cmbRef.dataFld = '';
			
			dsoSup01.recordset['FabricanteID'].value = selFabricanteID.options(0).value;
			selFabricanteID.selectedIndex = 0;
			
			cmbRef.dataSrc = oldSrc;
			cmbRef.dataFld = oldDtf;

			glb_TimerFabricante = window.setInterval('optChangedInCmb(selFabricanteID)', 10, 'JavaScript');  			
		}
	}
	
	if (glb_cmbLupaID.toUpperCase() == 'SELMARCAID')
	{
		if (selMarcaID.length == 1)
		{
			cmbRef.dataSrc = '';
			cmbRef.dataFld = '';
			
			dsoSup01.recordset['MarcaID'].value = selMarcaID.options(0).value;
			selMarcaID.selectedIndex = 0;
			
			cmbRef.dataSrc = oldSrc;
			cmbRef.dataFld = oldDtf;

			glb_TimerFabricante = window.setInterval('optChangedInCmb(selMarcaID)', 10, 'JavaScript');  			
		}
	}

}

/********************************************************************
Funcao criada pelo programador.
Limpa e trava os combos dinamicos quando o usuario clicar em inclusao

Parametro:
btnClicked  -> Ultimo botao clicado da barra superior

Retorno:
nenhum
********************************************************************/
function clearAndLockCmbsDynamics(btnClicked)
{
    if (btnClicked == 'SUPINCL')
    {
        clearComboEx(['selFabricanteID','selMarcaID','selLinhaProdutoID']);
        selFabricanteID.disabled = true;
        selMarcaID.disabled = true;
        selLinhaProdutoID.disabled = true;
                
        // trava o botao de lupa de Fabricante e Marca
        lockBtnLupa(btnFindFabricante, true);
        lockBtnLupa(btnFindMarca, true);
        lockBtnLupa(btnFindLinhaProduto, true);
    }
}

/********************************************************************
Funcao criada pelo programador.
Evento on change do campo txtModelo.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function mountAndFillTxtConceito()
{
    // Preenche o campo Conceito se o selProdutoID,selMarcaID,
    // txtModelo estao preenchidos e o txtConceito est� em branco
    if ((selProdutoID.selectedIndex != -1) &&
        (selMarcaID.selectedIndex != -1))
    {
        loadStrConceito([selProdutoID.value, selMarcaID.value]);
    }
    else
        txtConceito.value = '';

}

/********************************************************************
Funcao criada pelo programador.
Evento on click do chkAuto.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function chkAuto_onclick() {
    controleCampos('chkAuto_onclick');
}


/********************************************************************
Funcao criada pelo programador.
Evento on click do chkAuto.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function chkReplicar_onclick(){
    if (chkReplicar.checked == false) {
        chkDescricaoAuto.checked = false;
        chkDescricaoAuto.disabled = true;
    }
    else {
        chkDescricaoAuto.disabled = false;
    }
}


/********************************************************************
Funcao criada pelo programador.
Carrega a string do campo Conceito.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function loadStrConceito(strFind)
{
    writeInStatusBar('child', 'cellMode', 'Processando', true);
	//trava a interface
	lockInterface(true);

	// trazer os itens do Conceito do servidor

    var strPars = new String();
    strPars = '?';
       // Monta a string do array para o arquivo ASP
    for (i = 0; i < strFind.length; i++)
    {
        strPars += 'Item' + i + '='+escape(strFind[i]);
        if (i<= strFind.length - 2)
            strPars +='&'; 
    }
        
	dsoStrConceito.URL = SYS_ASPURLROOT + '/pcm/conceitos/serverside/fillstrconceito.aspx'+strPars;
	dsoStrConceito.ondatasetcomplete = loadStrConceito_DSC;
	dsoStrConceito.refresh();
}

/********************************************************************
Funcao criada pelo programador.
Retorno da funcao loadStrConceito.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function loadStrConceito_DSC()
{
    var ProdutoAbr, MarcaAbr;
    
    while (!dsoStrConceito.recordset.EOF)
    {
        ProdutoAbr = dsoStrConceito.recordset['Produto'].value;
        MarcaAbr = dsoStrConceito.recordset['Marca'].value;
	    dsoStrConceito.recordset.MoveNext();
    }

    // destrava a interface
	lockInterface(false);
    writeInStatusBar('child', 'cellMode', 'Altera��o', false);
    ProdutoAbr = trimStr(ProdutoAbr);
    ProdutoAbr = ProdutoAbr+'   ';
    ProdutoAbr = ProdutoAbr.substr(0,3);
    MarcaAbr = trimStr(MarcaAbr);
    MarcaAbr = MarcaAbr+'   ';
    MarcaAbr = MarcaAbr.substr(0,3);

    txtConceito.value = ProdutoAbr + MarcaAbr + ' ';
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal de impressao

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalPrint()
{
    var htmlPath;
    var strPars = new String();
    var empresaID = glb_Empresa[0];
    var empresaFantasia = glb_Empresa[3];
    var contexto = getCmbCurrDataInControlBar('sup', 1);
    var userID = getCurrUserID();
    
    // mandar os parametros para o servidor
    // o primeiro parametro e obrigatorio
        
    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + escape('S');

    // parametros opcionais
    strPars += '&nEmpresaID=' + escape(empresaID);
    strPars += '&sEmpresaFantasia=' + escape(empresaFantasia);       
    strPars += '&nContextoID=' + escape(contexto[1]);       
    strPars += '&nUserID=' + escape(userID);       

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_ASPURLROOT + '/pcm/conceitos/modalpages/modalprint.asp'+strPars;
       
    showModalWin(htmlPath, new Array(346,200));
}

/********************************************************************
Funcao criada pelo programador.
Ajusta o label do controle lblProdutoID, de acordo com o ID do
Produto selecionado

Parametro:
nProdutoID   ID do Produto

Retorno:
nenhum
********************************************************************/
function setLabelProduto(nProdutoID)
{
    if (nProdutoID == null)
        nProdutoID = '';
        
    var sLabel = 'Produto ' + nProdutoID;
    lblProdutoID.style.width = sLabel.length * FONT_WIDTH;
    lblProdutoID.innerText = sLabel;
}

/********************************************************************
Funcao criada pelo programador.
Ajusta o label do controle lblFabricanteID, de acordo com o ID do
Fabricante selecionado

Parametro:
nFabricanteID   ID do Fabricante

Retorno:
nenhum
********************************************************************/
function setLabelFabricante(nFabricanteID)
{
    if (nFabricanteID == null)
        nFabricanteID = '';
        
    var sLabel = 'Fabricante ' + nFabricanteID;
    lblFabricanteID.style.width = sLabel.length * FONT_WIDTH;
    lblFabricanteID.innerText = sLabel;
}

/********************************************************************
Funcao criada pelo programador.
Ajusta o label do controle lblProdutoID, de acordo com o ID do
Produto selecionado

Parametro:
nProdutoID   ID do Produto

Retorno:
nenhum
********************************************************************/
function setLabelMarca(nMarcaID)
{
    if (nMarcaID == null)
        nMarcaID = '';
        
    var sLabel = 'Marca ' + nMarcaID;
    lblMarcaID.style.width = sLabel.length * FONT_WIDTH;
    lblMarcaID.innerText = sLabel;
}

function setLabelLinhaProduto(nLinhaProdutoID)
{
    if (nLinhaProdutoID == null)
        nLinhaProdutoID = '';
        
    var sLabel = 'Linha de Produto ' + nLinhaProdutoID;
    lblLinhaProdutoID.style.width = sLabel.length * FONT_WIDTH;
    lblLinhaProdutoID.innerText = sLabel;
}
/********************************************************************
Funcao criada pelo programador.
Ajusta o label do controle lblTipoRegistroID, de acordo com o ID do
Tipo de registro selecionado

Parametro:
nTipoRegistroID   ID do Tipo de registro

Retorno:
nenhum
********************************************************************/
function setLabelTipoRegistro(nTipoRegistroID)
{
    if (nTipoRegistroID == null)
        nTipoRegistroID = '';
        
    var sLabel = 'Tipo ' + nTipoRegistroID;
    lblTipoRegistroID.style.width = sLabel.length * FONT_WIDTH;
    lblTipoRegistroID.innerText = sLabel;
}

/********************************************************************
Funcao criada pelo programador.
Ajusta o label do controle lblClassificacaoID, de acordo com o ID da
Classificacao selecionado

Parametro:
nClassificacaoID   ID da classificacao

Retorno:
nenhum
********************************************************************/
function setLabelClassificacao(nClassificacaoID)
{
    if (nClassificacaoID == null)
        nClassificacaoID = '';
        
    var sLabel = 'Classificacao ' + nClassificacaoID;
    lblClassificacaoID.style.width = sLabel.length * FONT_WIDTH;
    lblClassificacaoID.innerText = sLabel;
}

/********************************************************************
Funcao criada pelo programador.
Verifica se h� no0 cadastro algum produto igual

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function verificaProduto()
{
    var nConceitoID, nProdutoID, nFabricanteID, nMarcaID, sModelo, sPartNumber;

	nConceitoID = 0;
	if (txtRegistroID.value != '')
		nConceitoID = parseInt(txtRegistroID.value, 10);

	nProdutoID = selProdutoID.value;
	nFabricanteID = selFabricanteID.value;
	nMarcaID = selMarcaID.value;
	sModelo = txtModelo.value;
	sPartNumber = txtPartNumber.value;

	//trava a interface
	lockInterface(true);

    var strPars = new String();
    strPars = '?';
    strPars += 'nConceitoID=';
    strPars += escape(nConceitoID);
    strPars += '&nProdutoID=';
    strPars += escape(nProdutoID);
    strPars += '&nMarcaID=';
    strPars += escape(nMarcaID);
    strPars += '&sModelo=';
    strPars += escape(sModelo);
    strPars += '&sPartNumber=';
    strPars += escape(sPartNumber);
    
    strPars = parseStrEscaped(strPars);
        
	dsoVerificaProduto.URL = SYS_ASPURLROOT + '/pcm/conceitos/serverside/verificaproduto.aspx'+strPars;
	dsoVerificaProduto.ondatasetcomplete = verificaProduto_DSC;
	dsoVerificaProduto.refresh();
}

/********************************************************************
Funcao criada pelo programador.
Retorno da funcao verificaProduto().

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function verificaProduto_DSC()
{
	var sProdutos = '';
    dsoVerificaProduto.recordset.MoveFirst();

    var nQuantidadeModelo = dsoVerificaProduto.recordset['QuantidadeModelo'].value;
    var nQuantidadePartNumber = dsoVerificaProduto.recordset['QuantidadePartNumber'].value;

    if ((nQuantidadeModelo > 0) || (nQuantidadePartNumber > 0))
    {
        var sMensagem = '';

        sMensagem = 'Produto duplicado.\n\n';
            
        if (nQuantidadeModelo > 0)
            sMensagem += 'Modelo: ' + nQuantidadeModelo + '\n';

        if (nQuantidadePartNumber > 0)
            sMensagem += 'PartNumber: ' + nQuantidadePartNumber + '\n';

        sMensagem += '\nProsseguir mesmo assim?';

        var _retConf = window.top.overflyGen.Confirm(sMensagem);

        if (_retConf == 1) {
            // destrava a interface
            lockInterface(false);
            glb_btnCtlSup = 'SUPOK';
            saveRegister_1stPart();
        }
        else {
            // destrava a interface
            lockInterface(false);
        }
    }
    else {
        // destrava a interface
        lockInterface(false);
        glb_btnCtlSup = 'SUPOK';
        saveRegister_1stPart();
    }

}
/********************************************************************
Criada pelo programador
abre o detalhe de um Produto ja cadastrado
           
Parametros: 
nenhum:

Retorno:
nenhum
********************************************************************/
function showConceitoByID(nConceitoID)
{
    if ( glb_localTimerInt != null )
    {
        window.clearInterval(glb_localTimerInt);
        glb_localTimerInt = null;
    }    
    
    sendJSMessage(window.top.pesqListID, JS_DATAINFORM, 'SHOWDETAIL', 
      [nConceitoID, null, null]);
}
/********************************************************************
Funcao criada pelo programador.
Controla a habilitacao dos campos com direitos

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function direitoCampos()
{
	var aItemSelected, nContextoID;

	aItemSelected = getCmbCurrDataInControlBar('sup', 1);
	nContextoID = aItemSelected[1];

	// Contexto de Produtos
	if ( nContextoID == 2112)
	{
		var nE1 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
		                            ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'E1' + '\'' + ')') );

		var nE2 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
		                            ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'E2' + '\'' + ')') );

		var bReadOnly;
    
        //DireitoEspecifico
        //Conceitos->Produtos->SUP
        //21000 SFS-Grupo Conceitos-> E1&&E2 -> Habilita check box ControlaEstoque.
		if ( (nE1 == 1) && (nE2 == 1) )
		    bReadOnly = false;
		else
		    bReadOnly = true;

		chkControlaEstoque.disabled = bReadOnly;
	}
}

function loadModalImagem()
{
    var contextoID = getCmbCurrDataInControlBar('sup', 1);
    var strPars = new String();
    var htmlPath = '';
    var nConceitoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,"dsoSup01.recordset['ConceitoID'].value");
    var nTipoConceitoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,"dsoSup01.recordset['TipoConceitoID'].value");
    var sCaller = 'S';
    var nA1 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                                ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'A1' + '\'' + ')') );
    var nA2 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                                ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'A2' + '\'' + ')') );
    /*
    // Produtos
    // RRF 27/07/2011 - Marco pediou para abrir tambem para marca
    //Incluso o contexto Conceito com tipo de Conceito Concreto( cadastrar imagem para Familia)
    if (contextoID[1] == 2112 || contextoID[1] == 2113 || (contextoID[1] == 2111 && nTipoConceitoID == 302))
    {
	    strPars = '?nRegistroID=' + escape(nConceitoID);
	    strPars += '&nTipoRegistroID=' + escape(nTipoConceitoID);
	    strPars += '&sCaller=' + escape(sCaller);

        //DireitoEspecifico
        //Conceitos->Produtos -> SUP ->Modal Imagem
        //21000 SFS-Grupo Conceitos-> A1&&A2 -> Abre modal de gerencia de imagem, caso contrario apenas mostra as imagens.
        if ( (nA1 == 1) && (nA2 == 1) )
        {
            htmlPath = SYS_ASPURLROOT + '/modalgen/modalgerimagens.asp' + strPars;
		    //showModalWin(htmlPath, new Array(524, 488));
		    showModalWin(htmlPath, new Array(724, 488));
        }
        else
        {
            htmlPath = SYS_ASPURLROOT + '/modalgen/modalshowimagens.asp' + strPars;
		    showModalWin(htmlPath, new Array(0,0));
        }
    }*/
    // Impostos
    if (contextoID[1] == 2115)
    {
        var sNCM = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,"dsoSup01.recordset['ClassificacaoFiscal'].value");
        sNCM = sNCM.substring(0,4);
        strPars = '?sNCM=' + escape(sNCM);
        htmlPath = SYS_ASPURLROOT + '/pcm/conceitos/serverside/imagemncm.aspx' + strPars;
        window.open(htmlPath);
    } else {
        if (window.top.overflyGen.Alert('Modal dispon�vel somente no contexto Impostos.') == 0)
            return null;
    }
}

function replicarCadastroInformacoes() 
{
    lockInterface(true);

    dsoReplicarInformacoes.SQL =
        "DECLARE @Informacoes TABLE (Informacao VARCHAR(50), TipoReplicacao INT, Ordem INT) " +
        "INSERT INTO @Informacoes " +
	        "SELECT TOP 1 CONVERT(VARCHAR, a.dtData, 103) + ' ' + CONVERT(VARCHAR, a.dtData, 108) AS Informacao, " +
			        "NULL, " +
			        "1 AS Ordem " +
		        "FROM LogReplicacao a WITH(NOLOCK) " +
		        "WHERE a.RegistroID = " + txtRegistroID.value + " AND a.dtReplicacao IS NOT NULL " +
		        "ORDER BY dtData DESC " +
        "INSERT INTO @Informacoes " +
	        "SELECT DISTINCT " +
			        "(CASE a.TipoReplicacao " +
				        "WHEN 1 THEN 'Caracteristicas' " +
				        "WHEN 2 THEN 'Argumentos de Venda' " +
				        "WHEN 3 THEN 'Imagens' " +
				        "WHEN 4 THEN 'Pesos e Medidas' ELSE '' END) AS Informacao, " +
			        "a.TipoReplicacao, " +
			        "2 AS Ordem " +
		        "FROM LogReplicacao a WITH(NOLOCK) " +
		        "WHERE a.RegistroID = " + txtRegistroID.value + " AND a.dtReplicacao IS NULL " +
		        "ORDER BY TipoReplicacao " +
        "SELECT * FROM @Informacoes ORDER BY Ordem ";

    dsoReplicarInformacoes.ondatasetcomplete = replicarCadastroInformacoes_DSC;
    dsoReplicarInformacoes.refresh();
}

function replicarCadastroInformacoes_DSC() 
{
    var ultimaReplicacao = '';
    var replicacaoPendente = '';
    var desejaReplicar = '';
    var sAlert, _retMsg;

    //dsoReplicarInformacoes.recordset.Filter = 'Ordem = 1'
    dsoReplicarInformacoes.recordset.moveFirst();

    while (!(dsoReplicarInformacoes.recordset.BOF || dsoReplicarInformacoes.recordset.EOF)) 
    {
        // Pega data da ultima replica��o realizada
        if (dsoReplicarInformacoes.recordset['Ordem'].value == 1)
            ultimaReplicacao = '�ltima replica��o efetuada em ' + dsoReplicarInformacoes.recordset['Informacao'].value + '.\n\n';
        // Pega componentes que ser�o replicados
        else if (dsoReplicarInformacoes.recordset['Ordem'].value == 2)
            replicacaoPendente += ((replicacaoPendente == '') ? ('Replica��es pendentes: \n') : ('')) + '- ' + dsoReplicarInformacoes.recordset['Informacao'].value + '\n';

        dsoReplicarInformacoes.recordset.moveNext();
    }

    if (replicacaoPendente.length == 0) 
    {
        replicacaoPendente = 'N�o tem replica��o pendente. \n';
        desejaReplicar = '\nDeseja replicar assim mesmo?\n';
    }
    else 
    {
        desejaReplicar = '\nDeseja replicar?\n';
    }

    sAlert = ultimaReplicacao + replicacaoPendente + desejaReplicar;

    _retMsg = window.top.overflyGen.Confirm(sAlert);

    if (_retMsg == 1)
        replicarCadastro();
    else 
    {
        lockInterface(false);
        return null;
    }
}

function replicarCadastro() 
{
    var strPars = new String();

    strPars = '?nProdutoID=' + txtRegistroID.value;
    strPars += '&nUsuarioID=' + glb_USERID;

    dsoReplicar.URL = SYS_ASPURLROOT + '/pcm/conceitos/serverside/replicarcadastro.aspx' + strPars;
    dsoReplicar.ondatasetcomplete = replicarCadastro_DSC;
    dsoReplicar.refresh();
}

function replicarCadastro_DSC() 
{
    lockInterface(false);
}

//Apenas perfil de desenvolvedor, administrador e homologa��o poder�o alterar alguns dados do coneito em A.
function perfilUsuario() {
    var userID = getCurrUserID();

    setConnection(dsoperfilUsuario);

    dsoperfilUsuario.SQL = 'SELECT COUNT(DISTINCT a.PerfilID) AS PermiteSuporte, ' +
                                    '(SELECT COUNT(DISTINCT aa.PerfilID) ' +
							            'FROM RelacoesPesRec_Perfis aa WITH(NOLOCK) ' +
									        'INNER JOIN RelacoesPesRec bb WITH(NOLOCK) ON (bb.RelacaoID = aa.RelacaoID) ' +
		                                'WHERE (bb.objetoID = 999 AND bb.EstadoID = 2 AND  bb.SujeitoID =  ' + userID + ' AND aa.PerfilID IN (525, 619)))  AS PermiteHomologacao ' +
	                            'FROM RelacoesPesRec_Perfis a WITH(NOLOCK) ' +
		                            'INNER JOIN RelacoesPesRec b ON (b.RelacaoID = a.RelacaoID) ' +
	                            'WHERE (b.objetoID = 999 AND b.EstadoID = 2 AND b.SujeitoID =  ' + userID + '  AND a.PerfilID IN (500, 501)) ';

    dsoperfilUsuario.ondatasetcomplete = perfilUsuario_DSC;
    dsoperfilUsuario.Refresh();
}

function perfilUsuario_DSC() {
    glb_PermiteSuporte = dsoperfilUsuario.recordset['PermiteSuporte'].value;
    glb_PermiteHomologacao = dsoperfilUsuario.recordset['PermiteHomologacao'].value;

    return null;
}

/********************************************************************
Funcao criada pelo programador.
Controla Campos editaveis, Cores das Labels e Valida��o dos campos
********************************************************************/
function controleCampos(sChamada)
{
    // Detalhe, Refresh, RegistroAnterior ou Registro Seguinte do Conceito
    if ((sChamada == 'SUPDET') || (sChamada == 'SUPREFR') || (sChamada == 'SUPANT') || (sChamada == 'SUPSEG'))
    {
        controleLabelsPadrao(false);
        controleCamposDescricao();

        // Label de Descri��o Fiscal
        if (txtDescricaoFiscal.value.length > 90) {
            lblDescricaoFiscal.style.color = 'red';
            lblDescricaoFiscal.innerText = 'Descri��o Fiscal (' + txtDescricaoFiscal.value.length + ')';
        }
        else {
            lblDescricaoFiscal.style.color = 'black';
            lblDescricaoFiscal.innerText = 'Descri��o Fiscal';
        }
    }
    
    // Bot�o Incluir do SUP
    else if (sChamada == 'SUPINCL')
    {
        controleLabelsPadrao(true);
        controleCamposCadastro();
    }

    // Bot�o Alterar do SUP
    else if (sChamada == 'SUPALT')
    {
        controleCamposDescricao();
        controleCamposDescricao_EnableDisable();
    }

    // Bot�o OK - Ap�s clicar em Alterar ou Incluir
    else if (sChamada == 'SUPOK')
    {
        // Faz valida��o dos Campos obrigatorios para produto
        if (selTipoRegistroID.value == 303)
        {
            txtModelo.value = trimStr(txtModelo.value);
            txtDescricao.value = trimStr(txtDescricao.value);
            txtDescricaoComplementar.value = trimStr(txtDescricaoComplementar.value);

            var bDadosIncompletos = false;

            if (txtConceito.value.length <= 7) {
                bDadosIncompletos = true;
                lblConceito.style.color = 'red';
            }
            else
                lblConceito.style.color = 'blue';

            if (selClassificacaoID.selectedIndex == -1) {
                bDadosIncompletos = true;
                lblClassificacaoID.style.color = 'red';
            }
            else
                lblClassificacaoID.style.color = 'blue';

            if (selProdutoID.selectedIndex == -1) {
                bDadosIncompletos = true;
                lblProdutoID.style.color = 'red';
            }
            else
                lblProdutoID.style.color = 'blue';

            if (selFabricanteID.selectedIndex == -1) {
                bDadosIncompletos = true;
                lblFabricanteID.style.color = 'red';
            }
            else
                lblFabricanteID.style.color = 'blue';

            if (selMarcaID.selectedIndex == -1) {
                bDadosIncompletos = true;
                lblMarcaID.style.color = 'red';
            }
            else
                lblMarcaID.style.color = 'blue';

            if (selLinhaProdutoID.selectedIndex == -1) {
                bDadosIncompletos = true;
                lblLinhaProdutoID.style.color = 'red';
            }
            else
                lblLinhaProdutoID.style.color = 'blue';

            if (txtModelo.value == '') {
                bDadosIncompletos = true;
                lblModelo.style.color = 'red';
            }
            else
                lblModelo.style.color = 'blue';


            if ((selClassificacaoID.value == 311) && (txtPartNumber.value == '')) {
                bDadosIncompletos = true;
                lblPartNumber.style.color = 'red';
            }
            else
                lblPartNumber.style.color = 'blue';

            if (!chkAuto.checked) {
                if (txtDescricao.value == '') {
                    bDadosIncompletos = true;
                    lblDescricao.style.color = 'red';
                }
                else {
                    lblDescricao.style.color = 'blue';
                }
            }

            if (selUnidadeID.selectedIndex == -1) {
                bDadosIncompletos = true;
                lblUnidadeID.style.color = 'red';
            }
            else
                lblUnidadeID.style.color = 'blue';


            if (bDadosIncompletos)
            {
                if (window.top.overflyGen.Alert('Produto com dados incompletos.') == 0)
                    return null;

                return false;
            }
            else
            {
                verificaProduto();
                return true;
            }
        }
    }

    // Chamada: chkAuto_onclick
    else if (sChamada == 'chkAuto_onclick')
    {
        controleCamposDescricao();
        controleCamposDescricao_EnableDisable();
    }

    // Chamada: supInitEditMode (sup em modo de edicao)
    else if (sChamada == 'ModoEdicao')
    {
        var nEstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['EstadoID'].value");
        var nTipoConceitoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['TipoConceitoID'].value");
        var bTemKardex = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['TemKardex'].value");
        var userID = getCurrUserID();

        // Estado Ativo e tipo conceito � Produto
        // Permitir Allplus alterar o conceito, independente do estado ou linha de kardex. Solicitado pelo Marco Fortunto.
        if ((nEstadoID == 2 || nEstadoID == 11) && nTipoConceitoID == 303 && bTemKardex == 1 && glb_USERID != 1069)
        {
            txtRegistroID.disabled = true;
            txtEstadoID.disabled = true;

            // Comentado este trecho, solicitado pelo Eduardo Rodrigues. GHSR
            //txtDescricao.disabled = ((glb_PermiteSuporte > 0 || glb_PermiteHomologacao > 0) ? false : true);

            txtDescricao.disabled = true;
            txtConceito.disabled = ((glb_PermiteSuporte > 0 || glb_PermiteHomologacao > 0) ? false : true);
            txtConceitoAbreviado_02.disabled = true;
            txtOrdemConcreto.disabled = true;
            chkEhConfiguravel.disabled = true;
            chkEhServico.disabled = true;
            chkEhMicro.disabled = true;
            chkEhGabinete.disabled = true;
            chkFamiliaPrincipal.disabled = true;
            chkAceitaMaisProdutos.disabled = true;
            chkBloqueiaMicroSemEste.disabled = true;
            chkHomologavel.disabled = false;
            chkReplicar.disabled = true;
            selTamanho.disabled = true;
            txtValorEstoqueSeguro.disabled = true;
            txtSinonimos.disabled = true;
            txtObservacao.disabled = true;
            hrConceitoConcreto.disabled = true;
            selClassificacaoID.disabled = true;
            selProdutoID.disabled = true;
            selFabricanteID.disabled = true;
            btnFindFabricante.disabled = true;
            selTipoRegistroID.disabled = true;
            selMarcaID.disabled = true;
            btnFindMarca.disabled = true;
            selLinhaProdutoID.disabled = false;
            btnFindLinhaProduto.disabled = false;
            txtModelo.disabled = ((glb_PermiteSuporte > 0) ? false : true);;
            txtSerie.disabled = ((glb_PermiteSuporte > 0 || glb_PermiteHomologacao > 0) ? false : true);
            chkAuto.disabled = true;

            // Comentado este trecho, solicitado pelo Eduardo Rodrigues. GHSR
            //txtDescricaoComplementar.disabled = ((glb_PermiteSuporte > 0 || glb_PermiteHomologacao > 0) ? false : true);

            txtDescricaoComplementar.disabled = true;
            txtPartNumber.disabled = ((glb_PermiteSuporte > 0) ? false : true);;
            selUnidadeID.disabled = true;
            selTamanho2.disabled = true;
            chkInternacional.disabled = false;
            chkControlaEstoque.disabled = ((glb_PermiteSuporte > 0 || glb_PermiteHomologacao > 0) ? false : true);
            chkProdutoSeparavel.disabled = false;
            txtNumerosSerieAlternativos.disabled = true;
            chkControle.disabled = ((glb_PermiteSuporte > 0 || glb_PermiteHomologacao > 0) > 0 ? false : true);
            chkNumeroSerieEspecifico.disabled = ((glb_PermiteSuporte > 0 || glb_PermiteHomologacao > 0) ? false : true);
            chkEtiquetaPropria.disabled = ((glb_PermiteSuporte > 0 || glb_PermiteHomologacao > 0) ? false : true);
            chkImagem_Produto.disabled = true;
            chkIdentificadoresOK.disabled = ((glb_PermiteSuporte > 0 || glb_PermiteHomologacao > 0) ? false : true);
            chkHomologacaoOK.disabled = ((glb_PermiteSuporte > 0 || glb_PermiteHomologacao > 0) ? false : true);
            chkTemCodigoGTIN.disabled = ((glb_PermiteSuporte > 0 || glb_PermiteHomologacao > 0) ? false : true);
            txtCad.disabled = true;
            txtConceitoAbreviado_04.disabled = true;
            chkPublica.disabled = true;
            chkdestacainternet.disabled = true;
            chkImagem_Marca.disabled = true;
            txtSinonimos2.disabled = true;
            txtOrdem.disabled = true;
            txtImposto.disabled = true;
            selPaisID.disabled = true;
            selNivelGovernoID.disabled = true;
            chkIncidePreco.disabled = true;
            chkInclusoPreco.disabled = true;
            chkEsquemaCredito.disabled = true;
            chkAlteraAliquota.disabled = true;
            chkDestacaNota.disabled = true;
            chkAIC.disabled = true;
            selRegraAliquotaID.disabled = true;
            txtImpostoOrdem.disabled = true;
            selImpostoPaisID.disabled = true;
            selGrupoAlternativoID.disabled = true;
            selMoedaPaisID.disabled = true;
            txtCodigo.disabled = true;
            txtSimbolo.disabled = true;
            txtCodigoConceito.disabled = true;
            selPaisID2.disabled = true;
            txtClassificacaoFiscal.disabled = true;
            chkMPBem.disabled = true;
            txtObservacaoImposto.disabled = true;
            selPaisID3.disabled = true;
            txtNBS.disabled = true;
            txtObservacaoNBS.disabled = true;
            txtObservacaoConceito.disabled = true;
        }

        // Estado Cadastrado e tipo conceito � Produto
        else if (nEstadoID == 1 && nTipoConceitoID == 303)
        {
            controleCamposCadastro();
        }

        // Estado Supenso, Desativo e Deletado
        else if (nEstadoID == 3 || nEstadoID == 4 || nEstadoID == 5)
        {
            txtRegistroID.disabled = true;
            txtEstadoID.disabled = true;
            txtDescricao.disabled = true;
            txtConceito.disabled = true;
            txtConceitoAbreviado_02.disabled = true;
            txtOrdemConcreto.disabled = true;
            chkEhConfiguravel.disabled = true;
            chkEhServico.disabled = true;
            chkEhMicro.disabled = true;
            chkEhGabinete.disabled = true;
            chkFamiliaPrincipal.disabled = true;
            chkAceitaMaisProdutos.disabled = true;
            chkBloqueiaMicroSemEste.disabled = true;
            chkHomologavel.disabled = false;
            chkReplicar.disabled = true;
            selTamanho.disabled = true;
            txtValorEstoqueSeguro.disabled = true;
            txtSinonimos.disabled = true;
            txtObservacao.disabled = true;
            hrConceitoConcreto.disabled = true;
            selClassificacaoID.disabled = true;
            selProdutoID.disabled = true;
            selFabricanteID.disabled = true;
            btnFindFabricante.disabled = true;
            selTipoRegistroID.disabled = true;
            selMarcaID.disabled = true;
            btnFindMarca.disabled = true;
            selLinhaProdutoID.disabled = true;
            btnFindLinhaProduto.disabled = true;
            txtModelo.disabled = true;
            txtSerie.disabled = true;
            chkAuto.disabled = true;
            txtDescricaoComplementar.disabled = true;
            txtPartNumber.disabled = true;
            txtCodigoBNDES.disabled = true;
            selUnidadeID.disabled = true;
            selTamanho2.disabled = true;
            chkInternacional.disabled = true;
            chkControlaEstoque.disabled = true;
            chkProdutoSeparavel.disabled = true;
            txtNumerosSerieAlternativos.disabled = true;
            chkControle.disabled = true;
            chkNumeroSerieEspecifico.disabled = true;
            chkEtiquetaPropria.disabled = true;
            chkImagem_Produto.disabled = true;
            chkIdentificadoresOK.disabled = true;
            chkHomologacaoOK.disabled = ((glb_PermiteSuporte > 0 || glb_PermiteHomologacao > 0) ? false : true);
            chkTemCodigoGTIN.disabled = true;
            txtCad.disabled = true;
            txtConceitoAbreviado_04.disabled = true;
            chkPublica.disabled = true;
            chkdestacainternet.disabled = true;
            chkImagem_Marca.disabled = true;
            txtSinonimos2.disabled = true;
            txtOrdem.disabled = true;
            txtImposto.disabled = true;
            selPaisID.disabled = true;
            selNivelGovernoID.disabled = true;
            chkIncidePreco.disabled = true;
            chkInclusoPreco.disabled = true;
            chkEsquemaCredito.disabled = true;
            chkAlteraAliquota.disabled = true;
            chkDestacaNota.disabled = true;
            chkAIC.disabled = true;
            selRegraAliquotaID.disabled = true;
            txtImpostoOrdem.disabled = true;
            selImpostoPaisID.disabled = true;
            selGrupoAlternativoID.disabled = true;
            selMoedaPaisID.disabled = true;
            txtCodigo.disabled = true;
            txtSimbolo.disabled = true;
            txtCodigoConceito.disabled = true;
            selPaisID2.disabled = true;
            txtClassificacaoFiscal.disabled = true;
            chkMPBem.disabled = true;
            txtObservacaoImposto.disabled = true;
            selPaisID3.disabled = true;
            txtNBS.disabled = true;
            txtObservacaoNBS.disabled = true;
            txtObservacaoConceito.disabled = true;
        }

        controleLabelsPadrao(true);
    }
}

function controleCamposCadastro()
{
    txtRegistroID.disabled = false;
    txtEstadoID.disabled = false;
    txtDescricao.disabled = false;
    txtConceito.disabled = false;
    txtConceitoAbreviado_02.disabled = false;
    txtOrdemConcreto.disabled = false;
    chkEhConfiguravel.disabled = false;
    chkEhServico.disabled = false;
    chkEhMicro.disabled = false;
    chkEhGabinete.disabled = false;
    chkFamiliaPrincipal.disabled = false;
    chkAceitaMaisProdutos.disabled = false;
    chkBloqueiaMicroSemEste.disabled = false;
    chkHomologavel.disabled = false;
    chkReplicar.disabled = false;
    selTamanho.disabled = false;
    txtValorEstoqueSeguro.disabled = false;
    txtSinonimos.disabled = false;
    txtObservacao.disabled = false;
    hrConceitoConcreto.disabled = false;
    selClassificacaoID.disabled = false;
    selProdutoID.disabled = false;
    selFabricanteID.disabled = false;
    btnFindFabricante.disabled = false;
    selTipoRegistroID.disabled = false;
    selMarcaID.disabled = false;
    btnFindMarca.disabled = false;
    selLinhaProdutoID.disabled = false;
    btnFindLinhaProduto.disabled = false;
    txtModelo.disabled = false;
    txtSerie.disabled = false;
    txtDescricaoComplementar.disabled = false;
    txtPartNumber.disabled = false;
    txtCodigoBNDES.disabled = false;
    selUnidadeID.disabled = false;
    selTamanho2.disabled = false;
    chkInternacional.disabled = false;
    chkControlaEstoque.disabled = false;
    chkProdutoSeparavel.disabled = false;
    txtNumerosSerieAlternativos.disabled = false;
    chkControle.disabled = false;
    chkNumeroSerieEspecifico.disabled = false;
    chkEtiquetaPropria.disabled = false;
    chkImagem_Produto.disabled = false;
    chkIdentificadoresOK.disabled = false;
    chkHomologacaoOK.disabled = ((glb_PermiteSuporte > 0 || glb_PermiteHomologacao > 0) ? false : true);
    chkTemCodigoGTIN.disabled = false;
    txtCad.disabled = false;
    txtConceitoAbreviado_04.disabled = false;
    chkPublica.disabled = false;
    chkdestacainternet.disabled = false;
    chkImagem_Marca.disabled = false;
    txtSinonimos2.disabled = false;
    txtOrdem.disabled = false;
    txtImposto.disabled = false;
    selPaisID.disabled = false;
    selNivelGovernoID.disabled = false;
    chkIncidePreco.disabled = false;
    chkInclusoPreco.disabled = false;
    chkEsquemaCredito.disabled = false;
    chkAlteraAliquota.disabled = false;
    chkDestacaNota.disabled = false;
    chkAIC.disabled = false;
    selRegraAliquotaID.disabled = false;
    txtImpostoOrdem.disabled = false;
    selImpostoPaisID.disabled = false;
    selGrupoAlternativoID.disabled = false;
    selMoedaPaisID.disabled = false;
    txtCodigo.disabled = false;
    txtSimbolo.disabled = false;
    txtCodigoConceito.disabled = false;
    selPaisID2.disabled = false;
    txtClassificacaoFiscal.disabled = false;
    chkMPBem.disabled = false;
    txtObservacaoImposto.disabled = false;
    selPaisID3.disabled = false;
    txtNBS.disabled = false;
    txtObservacaoNBS.disabled = false;
    txtObservacaoConceito.disabled = false;   
}

// Configura as cores padr�es dos Labels
function controleLabelsPadrao(bEditMode)
{
    lblConceito.style.color = 'blue';
    lblClassificacaoID.style.color = 'blue';
    lblProdutoID.style.color = 'blue';
    lblFabricanteID.style.color = 'blue';
    lblMarcaID.style.color = 'blue';
    lblLinhaProdutoID.style.color = 'blue';
    lblModelo.style.color = 'blue';
    lblSerie.style.color = 'blue';

    controleCamposDescricao();

    if (bEditMode)
        controleCamposDescricao_EnableDisable();

    lblPartNumber.style.color = 'blue';
    lblUnidadeID.style.color = 'blue';
    lblTamanho2.style.color = 'green';
    lblInternacional.style.color = 'blue';
    lblControlaEstoque.style.color = 'green';
    lblProdutoSeparavel.style.color = 'green';
    lblNumerosSerieAlternativos.style.color = 'green';
    lblControle.style.color = 'green';
    lblNumeroSerieEspecifico.style.color = 'green';
    lblEtiquetaPropria.style.color = 'green';
    lblCodigoBNDES.style.color = 'green';
}

function controleCamposDescricao() {
    if (chkAuto.checked) {
        lblDescricao.style.color = 'black';
        lblDescricaoComplementar.style.color = 'black';
    }
    else {
        lblDescricao.style.color = 'blue';
        lblDescricaoComplementar.style.color = 'blue';
    }
}

function controleCamposDescricao_EnableDisable() {

    var bTemKardex = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['TemKardex'].value");

    // Permitir Allplus alterar o conceito, independente do estado ou linha de kardex. Solicitado pelo Marco Fortunto.
    if (((chkAuto.checked) || (bTemKardex)) && !(glb_PermiteSuporte || glb_PermiteHomologacao) && glb_USERID != 1069)
    {
        txtDescricao.disabled = true;
        txtDescricaoComplementar.disabled = true;
        chkHomologacaoOK.disabled = true;
        txtDescricao.value = '';
        txtDescricaoComplementar.value = '';
    }
    else {
        txtDescricao.disabled = false;
        txtDescricaoComplementar.disabled = false;
    }
}
