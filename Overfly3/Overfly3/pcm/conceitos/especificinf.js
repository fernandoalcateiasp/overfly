
/********************************************************************
especificinf.js

Library javascript de funcoes para abertura de objetos rds dos infs e
preenchimento do combo1 de pastas do control bar inferior.
Especifica para o form de conceitos
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_nFormID = window.top.formID;
var glb_nSubFormID = window.top.subFormID;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
fillCmbPastasInCtlBarInf(nTipoRegistroID)    
criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)
constructGridBody(folderID, currDSO)    

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Esta funcao seta o sql do dso para buscar o registro a ser mostrado no detalhe,
vai ao banco de dados no servidor e chama a funcao de ondatasetcomplete de acordo 
com o parametro recebido.
           
Parametros: 
dso                        -> dso a ser aberto
folderID                   -> id da pasta corrente no control bar inferior
idToFind                   -> id do registro corrente no sup
sFiltro                    -> string do filtro corrente no control bar inferior
nTipoRegistroID            -> tipo do registro corrente
Retorno:
nulo
********************************************************************/
function setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID) {
    var nEmpresaData = getCurrEmpresaData();

    setConnection(dso);

    //@@
    // Prop/Altern , Observacoes ou Homologacao
    if ((folderID == 20008) || (folderID == 20009) || (folderID == 21010) || (folderID == 21019)) {
        dso.SQL = 'SELECT a.ConceitoID, a.ProprietarioID,a.AlternativoID,a.Observacoes,a.Homologacao, a.EspecificacoesTecnicas ' +
                  'FROM Conceitos a WITH(NOLOCK) WHERE ' +
                  sFiltro + 'a.ConceitoID = ' + idToFind;
        return 'dso01JoinSup_DSC';
    }
    else if (folderID == 20010) // LOG
    {
        
        var aContextoID = getCmbCurrDataInControlBar('sup', 1);
        var nContextoID = aContextoID[1];

        // Contexto "Impostos".
        if (nContextoID == 2115) {
            dso.SQL = 'SELECT a.LOGID, a.FormID, a.SubFormID, a.RegistroID, a.Data, a.UsuarioID, a.EventoID, a.EstadoID, ISNULL(a.Motivo, SPACE(1)) AS Motivo, ' +
                    'dbo.fn_Data_Fuso(a.Data, NULL, ' + nEmpresaData[2] + ') AS DataFuso ' +
	                'FROM LOGs a WITH(NOLOCK) ' +
		                'INNER JOIN Conceitos_Servicos b WITH(NOLOCK) ON b.ConServicoID = a.RegistroID ' +
		                'INNER JOIN Conceitos c WITH(NOLOCK) ON c.ConceitoID = b.ConceitoID ' +
	                'WHERE c.ConceitoID = ' + idToFind + ' ' +
	                'AND a.FormID = ' + glb_nFormID + ' ' +
                    'AND a.SubFormID = 27003 ' +
	                'ORDER BY a.SubFormID ASC, a.Data DESC';
        } else {
            dso.SQL = 'SELECT *, dbo.fn_Data_Fuso(a.Data, NULL, ' + nEmpresaData[2] + ') AS DataFuso ' +
                  'FROM LOGs a WITH(NOLOCK) ' +
                  'WHERE ' + sFiltro + ' a.RegistroID = ' + idToFind + ' ' +
                  'AND a.FormID = ' + glb_nFormID + ' ' +
                  'AND a.SubFormID =' + glb_nSubFormID + ' ' +
                  'ORDER BY a.SubFormID ASC, a.Data DESC';
        }

        return 'dso01Grid_DSC';
    }
    else if (folderID == 21009) // Identificadores
    {
        dso.SQL = 'SELECT a.*, LEN(Identificador) AS Tamanho ' +
                      'FROM Conceitos_Identificadores a WITH(NOLOCK) ' +
                      'WHERE ' + sFiltro + 'a.ProdutoID = ' + idToFind + ' ' +
                      'ORDER BY a.Variacao, a.Quantidade, a.GTIN DESC, a.Identificador';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 21011) // Caracteristicas
    {
        dso.SQL = 'SELECT a.*, dbo.fn_Produto_CaracteristicaOrdem(a.ProdutoID, a.CaracteristicaID) AS Ordem, ' +
                        'dbo.fn_Produto_CaracteristicaBit(a.ProdutoID, a.CaracteristicaID, 1) AS FichaTecnica, ' +
                        'dbo.fn_Produto_CaracteristicaBit(a.ProdutoID, a.CaracteristicaID, 2) AS Recebimento, ' +
                        'dbo.fn_Produto_CaracteristicaBit(a.ProdutoID, a.CaracteristicaID, 3) AS Descricao, ' +
                        'dbo.fn_Produto_CaracteristicaBit(a.ProdutoID, a.CaracteristicaID, 4) AS DescricaoComplementar, ' +
                        'dbo.fn_Produto_CaracteristicaBit(a.ProdutoID, a.CaracteristicaID, 5) AS Obrigatorio, ' +
                        'dbo.fn_Produto_CaracteristicaBit(a.ProdutoID, a.CaracteristicaID, 6) AS Imagens, ' +
                        'dbo.fn_Produto_CaracteristicaBit(a.ProdutoID, a.CaracteristicaID, 7) AS PesosMedidas, ' +
                        'dbo.fn_Produto_CaracteristicaBit(a.ProdutoID, a.CaracteristicaID, 8) AS Web, ' +
						'(SELECT cc.PesquisaWeb ' +
						'FROM Conceitos bb WITH(NOLOCK) ' +
							'INNER JOIN RelacoesConceitos cc WITH(NOLOCK) ON cc.SujeitoID= a.CaracteristicaID AND ' +
							'cc.ObjetoID=bb.ProdutoID AND a.ProdutoID= bb.ConceitoID WHERE (cc.EstadoID = 2)) AS PesquisaWeb ' +
                      'FROM Conceitos_Caracteristicas a WITH(NOLOCK) ' +
                      'WHERE ' + sFiltro + 'a.ProdutoID = ' + idToFind + ' AND a.checado = 1' +
                      'ORDER BY Ordem';

        return 'dso01Grid_DSC';
    }
    else if (folderID == 21079) // Configura��o
    {
        dso.SQL = 'SELECT a.* ' +
                      'FROM Conceitos_Configuracoes a WITH(NOLOCK) ' +
                      'WHERE ' + sFiltro + 'a.ProdutoID = ' + idToFind + ' ' +
                      'ORDER BY a.FamiliaID';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 21012) // Estoques
    {
        var nFiltroID = getCmbCurrDataInControlBar('inf', 2);
        if ((nFiltroID != null) && (nFiltroID[1] == 41042))
            sFiltro = sFiltro + ' a.SujeitoID=' + nEmpresaData[0] + ' AND ';

        dso.SQL = 'SELECT c.Fantasia, (STR(dbo.fn_Produto_Estoque(a.SujeitoID,a.ObjetoID,341,NULL,NULL,NULL,375, -1),6,0)) AS Fisico, ' +
                  '(STR(dbo.fn_Produto_Estoque(a.SujeitoID,a.ObjetoID,356,NULL,NULL,NULL,375, NULL),6,0)) AS Disponivel, ' +
                  '(STR(dbo.fn_Produto_Estoque(a.SujeitoID,a.ObjetoID,341,NULL,NULL,NULL,375, -1),6,0)) AS EstoquePO, (STR(dbo.fn_Produto_Estoque(a.SujeitoID,a.ObjetoID,342,NULL,NULL,NULL,375, -1),6,0)) AS EstoquePE, ' +
                  '(STR(dbo.fn_Produto_Estoque(a.SujeitoID,a.ObjetoID,343,NULL,NULL,NULL,375, -1),6,0)) AS EstoquePD, (STR(dbo.fn_Produto_Estoque(a.SujeitoID,a.ObjetoID,344,NULL,NULL,NULL,375, -1),6,0)) AS EstoquePC, ' +
                  '(STR(dbo.fn_Produto_Estoque(a.SujeitoID,a.ObjetoID,346,NULL,NULL,NULL,375, -1),6,0)) AS EstoqueTO, (STR(dbo.fn_Produto_Estoque(a.SujeitoID,a.ObjetoID,347,NULL,NULL,NULL,375, -1),6,0)) AS EstoqueTE,' +
                  '(STR(dbo.fn_Produto_Estoque(a.SujeitoID,a.ObjetoID,348,NULL,NULL,NULL,375, -1),6,0)) AS EstoqueTD, (STR(dbo.fn_Produto_Estoque(a.SujeitoID,a.ObjetoID,349,NULL,NULL,NULL,375, -1),6,0)) AS EstoqueTC, ' +
                  '(STR(dbo.fn_Produto_Estoque(a.SujeitoID,a.ObjetoID,351,NULL,NULL,NULL,375, NULL),6,0)) AS EstoqueRC, (STR(dbo.fn_Produto_Estoque(a.SujeitoID,a.ObjetoID,352,NULL,NULL,NULL,375, NULL),6,0)) AS EstoqueRCC, ' +
                  '(STR(dbo.fn_Produto_Estoque(a.SujeitoID,a.ObjetoID,353,NULL,NULL,NULL,375, NULL),6,0)) AS EstoqueRV, (STR(dbo.fn_Produto_Estoque(a.SujeitoID,a.ObjetoID,354,NULL,NULL,NULL,375, NULL),6,0)) AS EstoqueRVC ' +
                  'FROM RelacoesPesCon a WITH(NOLOCK) ' +
                  'LEFT OUTER JOIN RelacoesPesCon_Fornecedores d WITH(NOLOCK) ON (a.RelacaoID=d.RelacaoID AND (d.Ordem=1)), ' +
                  'RelacoesPesRec b WITH(NOLOCK), Pessoas c WITH(NOLOCK) ' +
                  'WHERE ' + sFiltro +
                  'a.ObjetoID= ' + idToFind + ' AND a.TipoRelacaoID=61 AND a.SujeitoID=b.SujeitoID ' +
                  'AND b.TipoRelacaoID=12 AND b.ObjetoID=999 AND a.SujeitoID=c.PessoaID ' +
                  'ORDER BY c.Fantasia';

        return 'dso01Grid_DSC';
    }
    else if (folderID == 21013) // Aliquotas
    {
        dso.SQL = 'SELECT a.* ' +
                      'FROM Conceitos_Aliquotas a WITH(NOLOCK) ' +
                      'WHERE ' + sFiltro + 'a.ImpostoID = ' + idToFind + ' ' +
                      'ORDER BY a.LocalidadeOrigemID,a.LocalidadeDestinoID';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 21014) // Mensagens
    {
        dso.SQL = 'SELECT a.* ' +
                  'FROM Conceitos_Mensagens a WITH(NOLOCK) ' +
                  'WHERE ' + sFiltro + 'a.ImpostoID = ' + idToFind + ' ' +
                  'ORDER BY a.MensagemID';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 21018) // Pesos e Medidas
    {
        dso.SQL = 'SELECT a.* ' +
                  'FROM Conceitos_PesosMedidas a WITH(NOLOCK) ' +
                  'WHERE ' + sFiltro + 'a.ProdutoID = ' + idToFind + ' ' +
                  'ORDER BY a.Quantidade';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 21015) // Rel entre Conceitos
    {
        currCmb2Data = getCmbCurrDataInControlBar('inf', 2);
        aValue = currCmb2Data[1].split(',');
        //@@ As variaveis a seguir sao da automacao 
        //e devem ser definidas pelo programador
        glb_currGridTable = 'RelacoesConceitos';
        glb_aCmbKeyPesq = [['ID', 'aRelacaoID'], [aValue[2], 'bConceito']];
        glb_vOptParam1 = aValue[1];
        glb_vOptParam2 = aValue[0];
        // Final da automacao =========================================================

        if (aValue[1] == 'SUJEITO')
            return execPaging(dso, 'ObjetoID', idToFind, folderID);
        else if (aValue[1] == 'OBJETO')
            return execPaging(dso, 'SujeitoID', idToFind, folderID);
    }
    else if (folderID == 21016) // Rel com Pessoas
    {
        currCmb2Data = getCmbCurrDataInControlBar('inf', 2);
        aValue = currCmb2Data[1].split(',');
        //@@ As variaveis a seguir sao da automacao 
        //e devem ser definidas pelo programador
        glb_currGridTable = 'RelacoesPesCon';
        glb_aCmbKeyPesq = [['ID', 'aRelacaoID'], [aValue[2], 'bFantasia']];
        glb_vOptParam1 = aValue[0];
        // Final da automacao =========================================================

        return execPaging(dso, 'ObjetoID', idToFind, folderID);
    }
    else if (folderID == 21017) // Tempo de Producao
    {
        dso.SQL = 'SELECT a.* ' +
                  'FROM Conceitos_TemposProducao a WITH(NOLOCK) ' +
                  'WHERE ' + sFiltro + 'a.ProdutoID = ' + idToFind + ' ' +
                  'ORDER BY (SELECT Ordem FROM RelacoesRecursos WITH(NOLOCK) ' +
					'WHERE (a.EstadoID = SujeitoID AND TipoRelacaoID = 3 AND ObjetoID = 371)), a.Tempo';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 21020) // Argumento de Vendas
    {
        dso.SQL = 'SELECT a.*, SUBSTRING(a.ArgumentosVenda, 0, 100) + ' +
                  '(CASE WHEN LEN(SUBSTRING(a.ArgumentosVenda, 0, 102)) <=100 THEN SPACE(0) ELSE \'...\' END) AS ArgumentoAbreviado ' +
                  'FROM Conceitos_ArgumentosVenda a WITH(NOLOCK) ' +
                  'WHERE ' + sFiltro + 'a.ConceitoID = ' + idToFind + ' ' +
                  'ORDER BY a.ConArgumentoID';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 21021) // Protocolo
    {
        dso.SQL = 'SELECT a.* ' +
                  'FROM Conceitos_Protocolos a WITH(NOLOCK) ' +
                  'WHERE ' + sFiltro + 'a.ConceitoID = ' + idToFind + ' ' +
                  'ORDER BY a.ImpostoID, a.UFOrigemID, a.UFDestinoID';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 21022) // Impostos
    {
        dso.SQL = 'SELECT a.* ' +
                  'FROM Conceitos_Impostos a WITH(NOLOCK) ' +
                  'WHERE ' + sFiltro + 'a.ConceitoID = ' + idToFind + ' ' +
                  'ORDER BY a.ImpostoID';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 21023) // Retencoes BJBN 25/03/2011
    {
        dso.SQL = 'SELECT a.* ' +
                  'FROM Conceitos_ImpostosRetencoes a WITH(NOLOCK) ' +
                  'WHERE ' + sFiltro + 'a.ConceitoID = ' + idToFind + ' ' +
                  'ORDER BY a.ConceitoID';
        return 'dso01Grid_DSC';
    }

    else if (folderID == 21024) // Servicos BJBN 25/03/2011
    {
        dso.SQL = 'SELECT *, ' +
                        '(CASE WHEN (LEN(DescricaoLocal) > 100) THEN (LEFT(DescricaoLocal, 100) + \'...\') ELSE DescricaoLocal END) AS Descricao ' +
                    'FROM Conceitos_Servicos WITH(NOLOCK) ' +
                    'WHERE ' + sFiltro + 'ConceitoID = ' + idToFind + 
                    'ORDER BY (SELECT Localidade FROM Localidades aa WHERE aa.LocalidadeID = Conceitos_Servicos.LocalidadeID), Codigo, CodigoTomador';
        return 'dso01Grid_DSC';
    }

    else if (folderID == 21025) // Servicos Retencoes BJBN 25/03/2011
    {
        dso.SQL = 'SELECT a.* ' +
                  'FROM Conceitos_ServicosRetencoes a WITH(NOLOCK) ' +
                  'WHERE ' + sFiltro + 'a.ConceitoID = ' + idToFind + ' ' +
                  'ORDER BY a.LocalidadeID, a.ImpostoID';
        return 'dso01Grid_DSC';
    }

    else if (folderID == 21026) // Loja e Canais RRF 19/07/2011
    {
        /*dso.SQL = 'SELECT a.ConConceitoID, a.ConceitoID, a.LojaCanalID ' +
        'FROM Conceitos_LojasCanais a ' +
        'WHERE ' + sFiltro + 'a.ConceitoID = ' + idToFind + ' ' +
        'ORDER BY a.ConceitoID';*/
        dso.SQL = 'SELECT a.ConConceitoID, a.ConceitoID, a.LojaCanalID ' +
                  'FROM Conceitos_LojasCanais a WITH(NOLOCK) ' +
                  'WHERE ' + sFiltro + 'a.ConceitoID = ' + idToFind + ' ' +
                  'ORDER BY dbo.fn_TipoAuxiliar_Item(a.LojaCanalID,4), a.ConceitoID ';
        return 'dso01Grid_DSC';

    }
    else if (folderID == 21028) // Contas BJBN 26/07/2011
    {
        dso.SQL = 'SELECT a.* ' +
                  'FROM Conceitos_Contas a WITH(NOLOCK) ' +
                  'WHERE ' + sFiltro + 'a.ConceitoID = ' + idToFind + ' ' +
                  'ORDER BY a.ConceitoID';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 21029) //Beneficios Fiscias JL21/12/2011
    {
        dso.SQL = 'SELECT a.* ' +
	                'FROM Conceitos_Beneficios a WITH (NOLOCK) ' +
	                'WHERE ' + sFiltro + 'a.ConceitoID = ' + idToFind + ' ' +
	                'ORDER BY a.TipoBeneficioID ';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 21081) // CEST TSA 30/03/2016
    {
        dso.SQL = 'SELECT a.* ' +
                  'FROM Conceitos_Cest a WITH(NOLOCK) ' +
                  'WHERE ' + sFiltro + 'a.ConceitoID = ' + idToFind + ' ' +
                  'ORDER BY a.ConceitoID';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 21050) // Projeto Importacao
    {
        dso.SQL = 'SELECT * ' +
                  'FROM Conceitos_NVE WITH(NOLOCK) ' +
                  'WHERE ' + sFiltro + 'ConceitoID = ' + idToFind + ' ' +
                  'ORDER BY ConceitoID';
        return 'dso01Grid_DSC';
    }

}

/********************************************************************
Monta string de select para combo dentro de grid

Parametros:
dso                 - referencia ao dso corrente
pastaID             - pasta corrente
nRegistroID         - id do registro corrente
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID) {
    setConnection(dso);

    var nEmpresaData = getCurrEmpresaData();
    var nEmpresaPaisID = nEmpresaData[1];
    var aContextoID = getCmbCurrDataInControlBar('sup', 1);
    var nContextoID = aContextoID[1];

    // LOG
    if (pastaID == 20010) {
        if (nContextoID == 2115) {
            if (dso.id == 'dso01GridLkp') {
                dso.SQL = 'SELECT d.RecursoID, d.RecursoFantasia ' +
	                      'FROM LOGs a WITH(NOLOCK) ' +
		                    'INNER JOIN Conceitos_Servicos b WITH(NOLOCK) ON b.ConServicoID = a.RegistroID ' +
		                    'INNER JOIN Conceitos c WITH(NOLOCK) ON c.ConceitoID = b.ConceitoID ' +
		                    'INNER JOIN Recursos d WITH(NOLOCK) ON d.RecursoID = a.SubFormID ' +
	                      'WHERE (a.FormID = ' + glb_nFormID + ' AND a.SubFormID = 27003  AND c.ConceitoID = ' + nRegistroID + ')';
            }
            else if (dso.id == 'dso02GridLkp') {
                dso.SQL = 'SELECT d.PessoaID, d.Fantasia ' +
                          'FROM LOGs a WITH(NOLOCK) ' +
	                         'INNER JOIN Conceitos_Servicos b WITH(NOLOCK) ON b.ConServicoID = a.RegistroID ' +
	                         'INNER JOIN Conceitos c WITH(NOLOCK) ON c.ConceitoID = b.ConceitoID ' +
	                         'INNER JOIN Pessoas d WITH(NOLOCK) ON d.PessoaID = a.UsuarioID ' +
                          'WHERE (a.FormID = ' + glb_nFormID + ' AND a.SubFormID = 27003  AND c.ConceitoID = ' + nRegistroID + ')';
            }
            else if (dso.id == 'dso03GridLkp') {
                dso.SQL = 'SELECT d.ItemID, d.ItemAbreviado ' +
                          'FROM LOGs a WITH(NOLOCK) ' +
		                     'INNER JOIN Conceitos_Servicos b WITH(NOLOCK) ON b.ConServicoID = a.RegistroID ' +
		                     'INNER JOIN Conceitos c WITH(NOLOCK) ON c.ConceitoID = b.ConceitoID ' +
		                     'INNER JOIN TiposAuxiliares_Itens d WITH(NOLOCK) ON d.ItemID = a.EventoID ' +
                          'WHERE (a.FormID = ' + glb_nFormID + ' AND a.SubFormID = 27003 AND c.ConceitoID = ' + nRegistroID + ')';
            }
            else if (dso.id == 'dso04GridLkp') {
                dso.SQL = 'SELECT d.RecursoID, d.RecursoAbreviado ' +
                          'FROM LOGs a WITH(NOLOCK) ' +
		                     'INNER JOIN Conceitos_Servicos b WITH(NOLOCK) ON b.ConServicoID = a.RegistroID ' +
		                     'INNER JOIN Conceitos c WITH(NOLOCK) ON c.ConceitoID = b.ConceitoID ' +
		                     'INNER JOIN Recursos d WITH(NOLOCK) ON d.RecursoID = a.EstadoID ' +
                          'WHERE (a.FormID = ' + glb_nFormID + ' AND a.SubFormID= 27003 AND c.ConceitoID = ' + nRegistroID + ')';
            }
        }
        else {
            if (dso.id == 'dso01GridLkp') {
                dso.SQL = 'SELECT b.RecursoID, b.RecursoFantasia ' +
                          'FROM LOGs a WITH(NOLOCK) ' +
	                         'INNER JOIN Recursos b WITH(NOLOCK) ON b.RecursoID = a.SubFormID ' +
                          'WHERE (a.FormID = ' + glb_nFormID + ' AND a.RegistroID = ' + nRegistroID + ')';
            }
            else if (dso.id == 'dso02GridLkp') {
                dso.SQL = 'SELECT b.PessoaID, b.Fantasia ' +
                          'FROM LOGs a WITH(NOLOCK) ' +
	                          'INNER JOIN Pessoas b WITH(NOLOCK) ON b.PessoaID = a.UsuarioID ' +
                          'WHERE (a.FormID = ' + glb_nFormID + ' AND a.RegistroID = ' + nRegistroID + ')';
            }
            else if (dso.id == 'dso03GridLkp') {
                dso.SQL = 'SELECT b.ItemID, b.ItemAbreviado ' +
                          'FROM LOGs a WITH(NOLOCK) ' +
                              'INNER JOIN TiposAuxiliares_Itens b WITH(NOLOCK) ON b.ItemID = a.EventoID ' +
                          'WHERE (a.FormID = ' + glb_nFormID + ' AND a.RegistroID = ' + nRegistroID + ')';
            }
            else if (dso.id == 'dso04GridLkp') {
                dso.SQL = 'SELECT b.RecursoID, b.RecursoAbreviado ' +
                          'FROM LOGs a WITH(NOLOCK) ' +
                              'INNER JOIN Recursos b WITH(NOLOCK) ON b.RecursoID = a.EstadoID ' +
                          'WHERE (a.FormID=' + glb_nFormID + ' AND a.RegistroID = ' + nRegistroID + ')';
            }
        } 
    }
    else if (pastaID == 21011) // Caracteristicas
    {
        if (dso.id == 'dso01GridLkp') {
            dso.SQL = 'SELECT a.ConceitoID, ' +
							'dbo.fn_Tradutor(a.Conceito, ' + nEmpresaData[7] + ', ' + nEmpresaData[8] + ', NULL) AS Conceito ' +
                      'FROM Conceitos a WITH(NOLOCK), Conceitos_Caracteristicas b WITH(NOLOCK) ' +
                      'WHERE b.CaracteristicaID=a.ConceitoID AND b.ProdutoID = ' + nRegistroID;
        }
        else if (dso.id == 'dso02GridLkp') {
            dso.SQL = 'SELECT a.ConceitoID, d.RelacaoID ' +
                      'FROM Conceitos a WITH(NOLOCK), Conceitos_Caracteristicas b WITH(NOLOCK), Conceitos c WITH(NOLOCK), RelacoesConceitos d WITH(NOLOCK) ' +
                      'WHERE b.CaracteristicaID=a.ConceitoID AND c.ConceitoID = ' + nRegistroID + ' AND ' +
                      'c.ConceitoID = b.ProdutoID AND ' +
                      'b.CaracteristicaID = d.SujeitoID AND c.ProdutoID = d.ObjetoID AND d.TipoRelacaoID = 43';
        }
        else if (dso.id == 'dso03GridLkp') {
            dso.SQL = 'SELECT a.CaracteristicaID, dbo.fn_Produto_CaracteristicaOrdem(a.ProdutoID, a.CaracteristicaID) AS Ordem ' +
			          'FROM Conceitos_Caracteristicas a WITH(NOLOCK) ' +
			          'WHERE a.ProdutoID = ' + nRegistroID + ' ' +
			          'ORDER BY Ordem';
        }

    }
    else if (pastaID == 21013) // Aliquotas
    {
        if (dso.id == 'dso01GridLkp') {
            dso.SQL = 'SELECT a.LocalidadeID,a.Localidade ' +
                      'FROM Localidades a WITH(NOLOCK), Conceitos_Aliquotas b WITH(NOLOCK) ' +
                      'WHERE b.LocalidadeOrigemID=a.LocalidadeID AND b.ImpostoID = ' + nRegistroID;
        }
        else if (dso.id == 'dso02GridLkp') {
            dso.SQL = 'SELECT a.LocalidadeID,a.Localidade ' +
                      'FROM Localidades a WITH(NOLOCK), Conceitos_Aliquotas b WITH(NOLOCK) ' +
                      'WHERE b.LocalidadeDestinoID=a.LocalidadeID AND b.ImpostoID = ' + nRegistroID;
        }
        else if (dso.id == 'dsoCmb01Grid_05') {
            dso.SQL = 'SELECT NULL AS fldID, SPACE(0) AS fldName ' +
                      'UNION ALL ' +
                      'SELECT ItemID AS fldID, ItemAbreviado AS fldName ' +
                      'FROM TiposAuxiliares_Itens WITH(NOLOCK) ' +
                      'WHERE TipoID = 114 ';
        }
    }
    else if (pastaID == 21014) // Mensagens
    {
        if (dso.id == 'dsoStateMachineLkp') {
            dso.SQL = 'SELECT DISTINCT b.RecursoAbreviado as Estado,b.RecursoID ' +
                      'FROM Conceitos_Mensagens a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE a.ImpostoID = ' + nRegistroID + ' ' +
                      'AND a.EstadoID = b.RecursoID';
        }
    }
    else if (pastaID == 21018) // Pesos e Medidas
    {
        if (dso.id == 'dso01GridLkp') {
            dso.SQL = 'SELECT a.ConPesoMedidaID, dbo.fn_Produto_PesosMedidas(a.ProdutoID, a.Quantidade, 5) AS Cubagem, ' +
            'CONVERT(NUMERIC(7,2),dbo.fn_Produto_PesosMedidas(a.ProdutoID, a.Quantidade, 6)) AS PesoCubadoRodoviario, ' +
            'CONVERT(NUMERIC(7,2),dbo.fn_Produto_PesosMedidas(a.ProdutoID, a.Quantidade, 7)) AS PesoCubadoAereo ' +
                      'FROM Conceitos_PesosMedidas a WITH(NOLOCK) ' +
                      'LEFT OUTER JOIN Pessoas b WITH(NOLOCK) ON (a.ColaboradorID = b.PessoaID) ' +
                      'WHERE (a.ProdutoID = ' + nRegistroID + ') ';
        }
        else if (dso.id == 'dso02GridLkp') {
            dso.SQL = 'SELECT b.PessoaID, b.Fantasia ' +
                      'FROM Conceitos_PesosMedidas a WITH(NOLOCK), Pessoas b WITH(NOLOCK) ' +
                      'WHERE (a.ProdutoID = ' + nRegistroID + ' AND a.ColaboradorID = b.PessoaID)';
        }
    }
        // Rel entre Conceitos
    else if (pastaID == 21015) {
        if (dso.id == 'dso01GridLkp') {
            currCmb2Data = getCmbCurrDataInControlBar('inf', 2);
            aValue = currCmb2Data[1].split(',');
            if (aValue[1] == 'SUJEITO') {
                dso.SQL = 'SELECT b.ConceitoID, b.Conceito ' +
                          'FROM RelacoesConceitos a WITH(NOLOCK), Conceitos b WITH(NOLOCK) ' +
                          'WHERE a.ObjetoID = ' + nRegistroID + ' AND a.TipoRelacaoID = ' + aValue[0] + ' ' +
                          'AND a.SujeitoID = b.ConceitoID ';
            }
            else if (aValue[1] == 'OBJETO') {
                dso.SQL = 'SELECT b.ConceitoID, b.Conceito ' +
                          'FROM RelacoesConceitos a WITH(NOLOCK), Conceitos b WITH(NOLOCK) ' +
                          'WHERE a.SujeitoID = ' + nRegistroID + ' AND a.TipoRelacaoID = ' + aValue[0] + ' ' +
                          'AND a.ObjetoID = b.ConceitoID ';
            }
        }
        else if (dso.id == 'dsoStateMachineLkp') {
            currCmb2Data = getCmbCurrDataInControlBar('inf', 2);
            aValue = currCmb2Data[1].split(',');
            if (aValue[1] == 'SUJEITO') {
                dso.SQL = 'SELECT DISTINCT b.RecursoAbreviado as Estado,b.RecursoID ' +
                          'FROM RelacoesConceitos a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                          'WHERE a.ObjetoID = ' + nRegistroID +
                          ' AND a.TipoRelacaoID = ' + aValue[0] + ' ' +
                          'AND a.EstadoID = b.RecursoID ';
            }
            else if (aValue[1] == 'OBJETO') {
                dso.SQL = 'SELECT DISTINCT b.RecursoAbreviado as Estado,b.RecursoID ' +
                          'FROM RelacoesConceitos a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                          'WHERE a.SujeitoID = ' + nRegistroID +
                          ' AND a.TipoRelacaoID = ' + aValue[0] + ' ' +
                          'AND a.EstadoID = b.RecursoID ';
            }
        }
    }
    else if (pastaID == 21016) // Rel com Pessoas
    {
        if (dso.id == 'dso01GridLkp') {
            currCmb2Data = getCmbCurrDataInControlBar('inf', 2);
            aValue = currCmb2Data[1].split(',');
            dso.SQL = 'SELECT b.PessoaID, b.Fantasia ' +
                      'FROM RelacoesPesCon a WITH(NOLOCK), Pessoas b WITH(NOLOCK) ' +
                      'WHERE a.ObjetoID = ' + nRegistroID + ' AND a.TipoRelacaoID = ' + aValue[0] + ' ' +
                      'AND a.SujeitoID = b.PessoaID ';
        }
        else if (dso.id == 'dsoStateMachineLkp') {
            currCmb2Data = getCmbCurrDataInControlBar('inf', 2);
            aValue = currCmb2Data[1].split(',');
            dso.SQL = 'SELECT DISTINCT b.RecursoAbreviado as Estado,b.RecursoID ' +
                      'FROM RelacoesPesCon a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE a.ObjetoID = ' + nRegistroID +
                      ' AND a.TipoRelacaoID = ' + aValue[0] + ' ' +
                      'AND a.EstadoID = b.RecursoID ';
        }

    }
    else if (pastaID == 21017) // Tempo de Producao
    {
        if (dso.id == 'dsoCmb01Grid_01') {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoFantasia ' +
					  'FROM RelacoesRecursos a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
					  'WHERE (a.TipoRelacaoID = 3 AND a.ObjetoID = 371 AND a.SujeitoID = b.RecursoID AND ' +
					  'b.RecursoID NOT IN(5, 124)) ' +
					  'ORDER BY a.Ordem';
        }
    }
    else if (pastaID == 21020) // Argumentos de Venda
    {
        if (dso.id == 'dsoCmb01Grid_01') {
            dso.SQL = 'SELECT  ItemID, ItemMasculino ' +
			            'FROM TiposAuxiliares_Itens WITH(NOLOCK) ' +
                        'WHERE (EstadoID=2 AND TipoID=48) ' +
                        'ORDER BY Ordem ';
        }
    }
    else if (pastaID == 21021) // Protocolos
    {
        if (dso.id == 'dsoCmb01Grid_01') {
            dso.SQL = 'SELECT ConceitoID AS fldID, Imposto AS fldName ' +
                      'FROM Conceitos WITH(NOLOCK) ' +
                      'WHERE EstadoID=2 AND TipoConceitoID=306 ' +
                      'AND PaisID= ' + nEmpresaPaisID + ' ' +
                      'AND IncidePreco=1 ' +
                      'ORDER BY Imposto';
        }
        else if (dso.id == 'dsoCmb01Grid_02') {
            dso.SQL = 'SELECT 0 AS Indice, LocalidadeID AS fldID, CodigoLocalidade2 AS fldName ' +
		                'FROM Localidades WITH(NOLOCK) ' +
                        'WHERE (EstadoID=2 AND TipoLocalidadeID=204 AND LocalizacaoID=' + nEmpresaPaisID + ') ' +
                        'ORDER BY Indice, fldName';
        }
        else if (dso.id == 'dsoCmb01Grid_03') {
            dso.SQL = 'SELECT LocalidadeID AS fldID, CodigoLocalidade2 AS fldName ' +
			                'FROM Localidades WITH(NOLOCK) ' +
                            'WHERE (EstadoID=2 AND TipoLocalidadeID=204 AND LocalizacaoID=' + nEmpresaPaisID + ') ' +
                            'ORDER BY fldName';
        }
    }
    else if (pastaID == 21022) // Impostos
    {
        if (dso.id == 'dsoCmb01Grid_01') {
            dso.SQL = 'SELECT a.ConceitoID AS fldID, a.Imposto AS fldName ' +
                        'FROM Conceitos a WITH(NOLOCK) ' +
                        'WHERE a.EstadoID = 2 ' +
                                'AND a.TipoConceitoID = 306 ' +
                                'AND a.PaisID = ' + nEmpresaPaisID + ' ' +
                                'AND IncidePreco=1 ' +
                        'ORDER BY fldName';
        }
        else if (dso.id == 'dsoCmb01Grid_02') {
            dso.SQL = 'SELECT 0 AS Indice, 0 AS fldID, SPACE(0) AS fldName ' +
                        'UNION ALL ' +
                        'SELECT 1 AS Indice, LocalidadeID AS fldID, CodigoLocalidade2 AS fldName ' +
                        'FROM Localidades WITH(NOLOCK) ' +
                        'WHERE (EstadoID=2 AND TipoLocalidadeID=204 AND LocalizacaoID=' + nEmpresaPaisID + ') ' +
                        'ORDER BY Indice, fldName';
        }
    }
    else if (pastaID == 21023) // Retencoes
    {
        if (dso.id == 'dso01GridLkp') {
            dso.SQL = 'SELECT Conceitos_ImpostosRetencoes.ConRetencaoID, Localidades.Localidade ' +
	                    'FROM Localidades WITH(NOLOCK) ' +
		                    'INNER JOIN Conceitos_ImpostosRetencoes WITH(NOLOCK) ON (Conceitos_ImpostosRetencoes.LocalidadeID = Localidades.LocalidadeID) ' +
                        'WHERE Localidades.EstadoID = 2 AND ' +
                        'Conceitos_ImpostosRetencoes.ConceitoID = ' + nRegistroID + ' ' +
                        'ORDER BY Localidades.Localidade';
        }
        else if (dso.id == 'dsoCmb01Grid_01') {
            dso.SQL = 'SELECT 1 AS fldID, \'So ele\' AS fldName ' +
                            'UNION ALL ' +
                          'SELECT 2 AS fldID, \'O dia\' AS fldName ' +
                            'UNION ALL ' +
                          'SELECT 3 AS fldID, \'O mes\' AS fldName';
        }
        else if (dso.id == 'dsoCmb01Grid_02') {
            dso.SQL = 'SELECT 1 AS fldID, \'Lancamento\' AS fldName ' +
                        'UNION ALL ' +
                      'SELECT 2 AS fldID, \'Vencimento\' AS fldName ' +
                        'UNION ALL ' +
                      'SELECT 3 AS fldID, \'Pagamento\' AS fldName ';
        }
        else if (dso.id == 'dsoCmb01Grid_03') {
            dso.SQL = 'SELECT 1 AS fldID, \'Base\' AS fldName ' +
                        'UNION ALL ' +
                      'SELECT 2 AS fldID, \'Retencao\' AS fldName ';
        }
        else if (dso.id == 'dsoCmb01Grid_04') {
            dso.SQL = 'SELECT 1 AS fldID, \'Dia\' AS fldName ' +
                        'UNION ALL ' +
                      'SELECT 2 AS fldID, \'Semana\' AS fldName ' +
                        'UNION ALL ' +
                      'SELECT -2 AS fldID, \'Quinzena\' AS fldName ' +
                        'UNION ALL ' +
                      'SELECT 3 AS fldID, \'Mes\' AS fldName ' +
                        'UNION ALL ' +
                      'SELECT 4 AS fldID, \'Quarter\' AS fldName ' +
                        'UNION ALL ' +
                      'SELECT 5 AS fldID, \'Ano\' AS fldName ';
        }
    }
    else if (pastaID == 21024) // Servicos
    {
        if (dso.id == 'dso01GridLkp') {
            dso.SQL = 'SELECT DISTINCT Conceitos_Servicos.ConServicoID, Localidades.Localidade ' +
	                    'FROM Localidades WITH(NOLOCK) ' +
		                    'INNER JOIN Conceitos_Servicos WITH(NOLOCK) ON (Conceitos_Servicos.LocalidadeID = Localidades.LocalidadeID) ' +
                        'WHERE Localidades.EstadoID = 2 AND ' +
                        'Conceitos_Servicos.ConceitoID = ' + nRegistroID + ' ' +
                        'ORDER BY Localidade';
        }

        if (dso.id == 'dsoStateMachineLkp') {
            dso.SQL = 'SELECT DISTINCT Recursos.RecursoAbreviado as Estado, Recursos.RecursoID ' +
                     'FROM Localidades WITH(NOLOCK) ' +
		                    'INNER JOIN Conceitos_Servicos WITH(NOLOCK) ON (Conceitos_Servicos.LocalidadeID = Localidades.LocalidadeID) ' +
                            'INNER JOIN Recursos WITH(NOLOCK) ON (Recursos.RecursoID = Conceitos_Servicos.EstadoID) ' +
                        'WHERE Localidades.EstadoID = 2 AND ' +
                        'Conceitos_Servicos.ConceitoID = ' + nRegistroID + ' ' +
                        'ORDER BY Estado';
        }
    }
    else if (pastaID == 21025) // Servicos Retencoes
    {
        if (dso.id == 'dso01GridLkp') {
            dso.SQL = 'SELECT DISTINCT Conceitos_ServicosRetencoes.ConSerRetencaoID, Localidades.Localidade ' +
	                    'FROM Localidades WITH(NOLOCK) ' +
		                    'INNER JOIN Conceitos_ServicosRetencoes WITH(NOLOCK) ON (Conceitos_ServicosRetencoes.LocalidadeID = Localidades.LocalidadeID) ' +
                        'WHERE Localidades.EstadoID = 2 AND ' +
                        'Conceitos_ServicosRetencoes.ConceitoID = ' + nRegistroID + ' ' +
                        'ORDER BY Localidade ';
        }
        else if (dso.id == 'dsoCmb01Grid_01') {
            dso.SQL = 'SELECT a.ConceitoID AS ImpostoID, a.Imposto AS Imposto ' +
	                    'FROM Conceitos a WITH(NOLOCK) ' +
	                    'WHERE (a.TipoConceitoID = 306) AND (a.EstadoID = 2) AND (a.PaisID = 130) ' +
	                    'ORDER BY a.ConceitoID';
        }
    }
    else if (pastaID == 21026) // Lojas e Canais
    {
        if (dso.id == 'dsoCmb01Grid_01') {
            dso.SQL = ' SELECT  ItemID, ItemMasculino ' +
                      ' FROM TiposAuxiliares_Itens WITH(NOLOCK) ' +
                      ' WHERE (EstadoID=2) AND ' +
                      ' ((TipoID=108) OR (TipoID=13 AND Filtro LIKE \'%<CLI>%\'))' +
                      ' ORDER BY TipoID, Ordem';
        }
        if (dso.id == 'dso01GridLkp') {
            dso.SQL = 'SELECT CASE b.TipoID WHEN 108 THEN \'Loja\' ELSE \'Canal\' END AS Tipo, a.ConConceitoID ' +
                      'FROM Conceitos_LojasCanais a WITH(NOLOCK) ' +
                      'INNER JOIN TiposAuxiliares_Itens b WITH(NOLOCK) ON b.itemid = a.LojaCanalID ' +
                      'WHERE a.ConceitoID = ' + nRegistroID + ' ' +
                      'ORDER BY b.TipoID, a.ConceitoID';
        }
    }

    else if (pastaID == 21079) // Configura��o
    {
        if (dso.id == 'dsoCmb01Grid_01') {
            dso.SQL = 'SELECT  COnceitoID AS ItemID, Conceito AS ItemMasculino ' +
			            'FROM Conceitos WITH(NOLOCK) ' +
                        'WHERE (EstadoID=2 AND TipoConceitoID=302) ' +
                        'ORDER BY Ordem ';
        }
    }

    else if (pastaID == 21028) // Contas
    {
        if (dso.id == 'dsoCmb01Grid_01') {
            if (nTipoRegistroID == 309) {
                dso.SQL = 'SELECT a.ContaID AS ContaID, a.Conta AS Conta ' +
                    'FROM PlanoContas a WITH(NOLOCK) ' +
                    'WHERE (a.EstadoID = 2) AND ((a.Observacoes LIKE \'%<AI>%\') OR (a.Observacoes LIKE \'%<MUC>%\')) ' +
                    'ORDER BY a.ContaID';
            }
            else {
                dso.SQL = 'SELECT a.ContaID AS ContaID, a.Conta AS Conta ' +
                    'FROM PlanoContas a WITH(NOLOCK) ' +
                    'WHERE (a.EstadoID = 2) ' +
                    'ORDER BY a.Conta';
            }
        }
    }
    else if (pastaID == 21029)  //Beneficios Fiscais
    {

        var aContextoID = getCmbCurrDataInControlBar('sup', 1);
        var nContextoID = aContextoID[1];

        if (dso.id == 'dsoCmb01Grid_01') {
            dso.SQL = 'SELECT  ItemID AS TipoBeneficioID, ItemMasculino ' +
                        'FROM TiposAuxiliares_Itens WITH(NOLOCK) ' +
                        'WHERE (EstadoID=2) AND (TipoID=113) AND Filtro like ' + '\'' + '%<' + nContextoID + '>%' + '\'' + ' ' +
                        'ORDER BY TipoID, Ordem ';
        }
        else if (dso.id == 'dsoStateMachineLkp') {
            dso.SQL = 'SELECT DISTINCT b.RecursoAbreviado as Estado,b.RecursoID ' +
                          'FROM Conceitos_Beneficios a WITH(NOLOCK) ' +
                                'INNER JOIN Recursos b WITH(NOLOCK) ON a.EstadoID = b.RecursoID ' +
                          'WHERE a.ConceitoID = ' + nRegistroID + ' ORDER BY b.RecursoID ';
        }
        else if (dso.id == 'dso01GridLkp') {
            //dso.SQL = declare @user int = 1000, @empresaid int = 2, @formid int = 2110, @pastaid int = 21029, @defaultcontextoID INT = 2112
            var userid = getCurrUserID();
            var empresaid = getCurrEmpresaData();

            dso.SQL = 'SELECT ' + nRegistroID + ' AS RegistroID, CONVERT(BIT, COUNT(*)) As TemDireitoAtivar ' +
                         'FROM RelacoesRecursos a WITH(NOLOCK) ' +
				                'INNER JOIN RelacoesRecursos b WITH(NOLOCK) on a.MaquinaEstadoID=b.ObjetoID  ' +
				                'INNER JOIN RelacoesRecursos_Estados c WITH(NOLOCK) on b.RelacaoID=c.RelacaoID  ' +
				                'INNER JOIN RelacoesRecursos d WITH(NOLOCK) on d.SujeitoID=c.RecursoID AND d.ObjetoID=a.MaquinaEstadoID  ' +
				                'INNER JOIN Recursos_Direitos g WITH(NOLOCK) on g.EstadoParaID=c.RecursoID  ' +
				                'INNER JOIN RelacoesPesRec_Perfis f WITH(NOLOCK) on g.PerfilID=f.PerfilID ' +
				                'INNER JOIN RelacoesPesRec e WITH(NOLOCK) on e.RelacaoID=f.RelacaoID ' +
				                'INNER JOIN Recursos h WITH(NOLOCK) on g.EstadoParaID=h.RecursoID  ' +
				                'INNER JOIN Recursos i WITH(NOLOCK) on f.PerfilID = i.RecursoID ' +
                         'WHERE a.EstadoID=2  ' +
			                'AND a.TipoRelacaoID=1  ' +
			                'AND a.ObjetoID= 2112 ' +
			                'AND a.SujeitoID= 21029 ' +
			                'AND b.EstadoID=2  ' +
			                'AND b.TipoRelacaoID=3 ' +
			                'AND b.SujeitoID= 1  ' +
			                'AND ISNULL(c.UsoSistema, 0) = 0  ' +
			                'AND d.TipoRelacaoID=3  ' +
			                'AND d.EstadoID=2 ' +
			                'AND e.SujeitoID IN ((SELECT  ' + userid + ' UNION ALL SELECT UsuarioDeID FROM DelegacaoDireitos WITH(NOLOCK) WHERE (UsuarioParaID = ' + userid + ' AND GETDATE() BETWEEN dtInicio AND dtFim))) ' +
			                'AND e.ObjetoID=999 AND e.TipoRelacaoID=11 ' +
			                'AND e.EstadoID=2  ' +
			                'AND f.EmpresaID= ' + empresaid[0] + ' ' +
			                'AND g.RecursoID= 1  ' +
			                'AND g.RecursoMaeID= 21029 ' +
			                'AND g.ContextoID= 2112 ' +
			                'AND i.EstadoID = 2 ' +
			                'AND g.Alterar1 = 1 ' +
			                'AND g.Alterar2 = 1 ' +
			                'AND g.RecursoID = 1 ' +
			                'AND g.EstadoParaID = 2 ';

        }
    }
}

/********************************************************************
Esta funcao preenche o combo de pastas do controlbar inferior

Parametros:
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function fillCmbPastasInCtlBarInf(nTipoRegistroID) {
    var i = 0;
    var currSelection = getCmbCurrDataInControlBar('inf', 1);
    var vPastaName;

    cleanupSelInControlBar('inf', 1);

    vPastaName = window.top.getPastaName(20008);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 20008); //Observa��es

    vPastaName = window.top.getPastaName(20009);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 20009); //Proprietario

    vPastaName = window.top.getPastaName(20010);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 20010); //LOG

    if (nTipoRegistroID == 302) {
        vPastaName = window.top.getPastaName(21010);

        if (vPastaName != '')
            addOptionToSelInControlBar('inf', 1, vPastaName, 21010); //Homologacao

        vPastaName = window.top.getPastaName(21026);

        if (vPastaName != '')
            addOptionToSelInControlBar('inf', 1, vPastaName, 21026); //Lojas e Canais
    }
    else if (nTipoRegistroID == 303) {
        vPastaName = window.top.getPastaName(21011);
        if (vPastaName != '')
            addOptionToSelInControlBar('inf', 1, vPastaName, 21011); //Caracteristicas

        vPastaName = window.top.getPastaName(21020);
        if (vPastaName != '')
            addOptionToSelInControlBar('inf', 1, vPastaName, 21020); //Argumento de Vendas

        vPastaName = window.top.getPastaName(21018);
        if (vPastaName != '')
            addOptionToSelInControlBar('inf', 1, vPastaName, 21018); //Pesos e Medidas

        vPastaName = window.top.getPastaName(21009);
        if (vPastaName != '')
            addOptionToSelInControlBar('inf', 1, vPastaName, 21009); //Identificadores

        vPastaName = window.top.getPastaName(21019);
        if (vPastaName != '')
            addOptionToSelInControlBar('inf', 1, vPastaName, 21019); //Espeficif tecnicas

        vPastaName = window.top.getPastaName(21010);
        if (vPastaName != '')
            addOptionToSelInControlBar('inf', 1, vPastaName, 21010); //Homologacao

        vPastaName = window.top.getPastaName(21079);
        if (vPastaName != '')
            addOptionToSelInControlBar('inf', 1, vPastaName, 21079); //Configura��o

        vPastaName = window.top.getPastaName(21029);
        if (vPastaName != '')
            addOptionToSelInControlBar('inf', 1, vPastaName, 21029); // Beneficios Fiscais

        vPastaName = window.top.getPastaName(21017);
        if (vPastaName != '')
            addOptionToSelInControlBar('inf', 1, vPastaName, 21017); //Tempo de Producao

        vPastaName = window.top.getPastaName(21012);
        if (vPastaName != '')
            addOptionToSelInControlBar('inf', 1, vPastaName, 21012); //Estoques

        vPastaName = window.top.getPastaName(21050); // PROJETO IMPORTACAO
        if (vPastaName != '')
            addOptionToSelInControlBar('inf', 1, vPastaName, 21050);
    }

    if (nTipoRegistroID == 306) {
        vPastaName = window.top.getPastaName(21013);
        if (vPastaName != '')
            addOptionToSelInControlBar('inf', 1, vPastaName, 21013); //Aliquotas

        vPastaName = window.top.getPastaName(21014);
        if (vPastaName != '')
            addOptionToSelInControlBar('inf', 1, vPastaName, 21014); //Mensagens


        vPastaName = window.top.getPastaName(21023);
        if (vPastaName != '')
            addOptionToSelInControlBar('inf', 1, vPastaName, 21023); // Retencoes - BJBN 25/03/2011
    }
    if ((nTipoRegistroID == 301) || (nTipoRegistroID == 302) ||
         (nTipoRegistroID == 303) || (nTipoRegistroID == 305) ||
         (nTipoRegistroID == 306) || (nTipoRegistroID == 307) ||
         (nTipoRegistroID == 308)) {
        vPastaName = window.top.getPastaName(21015);
        if (vPastaName != '')
            addOptionToSelInControlBar('inf', 1, vPastaName, 21015); //Rel entre Conceitos
    }
    if ((nTipoRegistroID == 302) || (nTipoRegistroID == 303) ||
         (nTipoRegistroID == 304)) {
        vPastaName = window.top.getPastaName(21016);
        if (vPastaName != '')
            addOptionToSelInControlBar('inf', 1, vPastaName, 21016); //Rel com Pessoas
    }

    if ((nTipoRegistroID == 309) || (nTipoRegistroID == 318)) {
        vPastaName = window.top.getPastaName(21021);
        if (vPastaName != '')
            addOptionToSelInControlBar('inf', 1, vPastaName, 21021); // Protocolos

        vPastaName = window.top.getPastaName(21022);
        if (vPastaName != '')
            addOptionToSelInControlBar('inf', 1, vPastaName, 21022); // Impostos

        vPastaName = window.top.getPastaName(21028);
        if (vPastaName != '')
            addOptionToSelInControlBar('inf', 1, vPastaName, 21028); // Contas - SGP13/02/2015

        vPastaName = window.top.getPastaName(21029);
        if (vPastaName != '')
            addOptionToSelInControlBar('inf', 1, vPastaName, 21029); // Beneficios Fiscais

        vPastaName = window.top.getPastaName(21081); // CEST
        if (vPastaName != '')
            addOptionToSelInControlBar('inf', 1, vPastaName, 21081);

    }

    if (nTipoRegistroID == 320) {
        vPastaName = window.top.getPastaName(21024);
        if (vPastaName != '')
            addOptionToSelInControlBar('inf', 1, vPastaName, 21024); // Servicos - BJBN 25/03/2011

        vPastaName = window.top.getPastaName(21025);
        if (vPastaName != '')
            addOptionToSelInControlBar('inf', 1, vPastaName, 21025); // Serv Retencoes - BJBN 25/03/2011

        vPastaName = window.top.getPastaName(21028);
        if (vPastaName != '')
            addOptionToSelInControlBar('inf', 1, vPastaName, 21028); // Contas - BJBN 26/07/2011
    }

    // Nao mexer - Inicio de automacao ============================== 	
    // Tenta selecionar novamente a pasta anterior selecionada no combo
    // Se nao mais existe a pasta anteriormente selecionada no combo
    // seleciona a pasta default

    stripFoldersByRight(currSelection);

    return true;
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Tratar consistencia dos dados inseridos pelo usuario aqui.
           
Parametros: 
btnClicked      - controlbar e botao clicado pelo usuario
folderID        - id da pasta selecionada
tipoCtlrsPasta  - 'GRID' a pasta e um grid/ 'CTRLS' a pasta nao e
um grid
currDSO         - referencia dso corrente
registroID      - id do registro corrente no sup

Retorno:
true se os dados estao ok, caso contrario false.
********************************************************************/
function criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID) {
    //@@ return true se tudo ok, caso contrario return false;

    var currLine = 0;
    var i = 0;
    var nPesoOld = 0;
    var bSkipPeso = false;

    // A pasta e grid    
    if (tipoCtlrsPasta == 'GRID') // se � grid
    {
        if (folderID == 21009) // Identificadores
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [2], ['ProdutoID', registroID]);
        else if (folderID == 21011) // Caracteristica
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [0, 1], ['ProdutoID', registroID]);
        else if (folderID == 21013) // Aliquotas
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [0, 2], ['ImpostoID', registroID], [4, 5]);
        else if (folderID == 21014) // Mensagens
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [2], ['ImpostoID', registroID]);
        else if (folderID == 21017) // Mensagens
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [0, 1], ['ProdutoID', registroID]);
        else if (folderID == 21079) // Configura��o
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [0], ['ProdutoID', registroID]);
        else if (folderID == 21018) // Pesos e Medidas
        {
            nPesoOld = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Peso'));
            nPesoOld = removeCharFromString(nPesoOld, ' ');
            nPesoOld = removeCharFromString(nPesoOld, '_');
            nPesoOld = replaceStr(nPesoOld, ',', '.');

            bSkipPeso = (removeCharFromString(removeCharFromString(fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Peso')), ','), '.') == '');

            for (i = (fg.Row - 1) ; i > 0; i--) {
                nPesoOld = fg.ValueMatrix(i, getColIndexByColKey(fg, 'Peso'));
            }

            nPesoOld = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Peso'));
            nPesoOld = removeCharFromString(nPesoOld, ' ');
            nPesoOld = removeCharFromString(nPesoOld, '_');
            nPesoOld = replaceStr(nPesoOld, ',', '.');

            for (i = (fg.Row + 1) ; i < fg.Rows; i++) {

                nPesoOld = fg.TextMatrix(i, getColIndexByColKey(fg, 'Peso'));
                nPesoOld = removeCharFromString(nPesoOld, ' ');
                nPesoOld = removeCharFromString(nPesoOld, '_');
                nPesoOld = replaceStr(nPesoOld, ',', '.');
            }

            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [0], ['ProdutoID', registroID], [14]);
        }
        else if ((folderID == 21015) || (folderID == 21016)) //Relacoes entre Conceitos/Relacoes com Pessoas
        {
            currCmb2Data = getCmbCurrDataInControlBar('inf', 2);
            aValue = currCmb2Data[1].split(',');
            if (aValue[1] == 'SUJEITO')
                currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [2], ['ObjetoID', registroID], [4, 5]);
            else if (aValue[1] == 'OBJETO')
                currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [2], ['SujeitoID', registroID], [4, 5]);
        }
        else if (folderID == 21020) // Argumentos de Venda
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [0, 2], ['ConceitoID', registroID], [3]);

        else if (folderID == 21021) // Protocolos
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [0], ['ConceitoID', registroID]);

        else if (folderID == 21022) // Impostos
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [0, 3], ['ConceitoID', registroID]);

        else if (folderID == 21023) // Retencoes
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [0, 3], ['ConceitoID', registroID], [11]);

        else if (folderID == 21024) // Servicos
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [1, 3, 6, 7], ['ConceitoID', registroID], [6, 7]);

        else if (folderID == 21025) // Serv Retencoes
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [1, 2, 3, 5], ['ConceitoID', registroID], [5]);

        else if (folderID == 21026) // Lojas e Canais
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [1], ['ConceitoID', registroID]);

        else if (folderID == 21028) // Contas
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [1], ['ConceitoID', registroID]);

        else if (folderID == 21029) // Beneficios Fiscais
        {
            var bTemDirAtivar = dso01GridLkp.recordset['TemDireitoAtivar'].value;
            var nEstadoBeneficioID = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'EstadoID'));
            var nEstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['EstadoID'].value");

            bTemDirAtivar = (bTemDirAtivar == 1 ? true : false);

            if ((!bTemDirAtivar) && (nEstadoBeneficioID == 2)) {
                if (window.top.overflyGen.Alert('Somente o setor fiscal pode fazer altera��es com o benef�cio ativo.') == 0)
                    return false;

                return false;
            }



            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [1, 2, 4], ['ConceitoID', registroID], []);
        }

        else if (folderID == 21081) // CEST
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [1], ['ConceitoID', registroID]);

        else if (folderID == 21050) // PROJETO IMPORTACAO           
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [], ['ConceitoID', registroID]);
        //currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [], ['ConceitoID', registroID], [getColIndexByColKey(fg, 'ConNVEID')]);

        if (currLine < 0)
            return false;
    }
    return true;
}

/********************************************************************
Constroi o corpo de um grid. Os combos sao construidos pelo frame work
           
Parametros: 
folderID        - id da pasta corrente
currDSO         - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function constructGridBody(folderID, currDSO) {
    var dTFormat = '';

    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';

    var aContextoID = getCmbCurrDataInControlBar('sup', 1);
    var nContextoID = aContextoID[1];
    var nTipoConceitoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['TipoConceitoID'].value");

    fg.FrozenCols = 0;

    if (folderID == 20010) // LOG
    {
        headerGrid(fg, ['SubForm',
                       'Data           Hora     ',
                       'Colaborador',
                       'Evento',
                       'Est',
                       'Motivo',
                       'LOGID'], [6]);

        fillGridMask(fg, currDSO, ['^SubFormID^dso01GridLkp^RecursoID^RecursoFantasia',
                                 'DataFuso',
                                 '^UsuarioID^dso02GridLkp^PessoaID^Fantasia',
                                 '^EventoID^dso03GridLkp^ItemID^ItemAbreviado',
                                 '^EstadoID^dso04GridLkp^RecursoID^RecursoAbreviado',
                                 'Motivo',
                                 'LOGID'],
                                 ['', '99/99/9999', '', '', '', '', ''],
                                 ['', dTFormat + ' hh:mm:ss', '', '', '', '', '']);

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
    else if (folderID == 21009) // Identificadores
    {
        headerGrid(fg, ['Var',
                       'Quant',
                       'Identificador',
                       'GTIN',
					   'Tam',
					   'Cor',
					   'EAN13',
					   'OK',
					   'Processado',
                       'ConIdentificadorID'], [6, 7, 9]);

        glb_aCelHint = [[0, 0, 'N�mero utilizado para relacionar os identificadores da mesma varia��o do produto'],
						[0, 1, 'Quantidade de itens deste identificador na embalagem'],
						[0, 3, 'Este identificador � um GTIN (Global Trade Item Number ou N�mero Global de Item Comercial)?'],
						[0, 4, 'Quantidade de d�gitos'],
						[0, 5, 'Correspondente'],
						[0, 6, 'Este identificador � padr�o EAN13?'],
						//[0, 7, 'Este identificador ainda est� em uso?'],
						[0, 8, 'Nulo - Novos registros, 0 - Registros antigos sem GTIN em PM, 1 - Atualizados, 2 - Inseridos (casos sem Identificadores), 3 - Inseridos (casos com outros Identificadores']];

        fillGridMask(fg, currDSO, ['Variacao',
                                 'Quantidade',
                                 'Identificador',
                                 'GTIN',
								 'Tamanho*',
								 'Correspondente*',
								 'EAN13*',
								 'OK',
								 'Processado*',
                                 'ConIdentificadorID'],
                                 ['###', '9999', '', '', '99', '999', '', '', '99', '']);

        alignColsInGrid(fg, [0, 1, 4, 5, 8]);

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;

    }
    else if (folderID == 21079) // Configura��o
    {
        headerGrid(fg, ['Fam�lia',
                       'AMP',
					   'M�ltiplo',
                       'Quant Inc',
                       'Quant M�x',
                       'COnConfiguracaoID'], [5]);

        glb_aCelHint = [[0, 1, 'Aceita mais produtos?'],
						[0, 3, 'Quantidade inclusa'],
						[0, 4, 'Quantidade m�xima']];

        fillGridMask(fg, currDSO, ['FamiliaID*',
                                 'AceitaMaisProdutos',
								 'Multiplo',
                                 'QuantidadeInclusa',
                                 'QuantidadeMaxima',
                                 'ConConfiguracaoID'],
                                 ['', '', '999', '999', '999', '']);

        alignColsInGrid(fg, [3, 4, 5]);
    }
    else if (folderID == 21018) // Pesos e Medidas
    {
        headerGrid(fg, ['Quant',
					   'Caixa',
					   'Peso',
                       'P-OK',
                       'Largura',
                       'Altura',
                       'Profund',
                       'M-OK',
                       'GTIN',
                       'Cubagem',
                       'PCR',
                       'PCA',
                       'Observa��o',
                       'Colaborador',
                       'ColaboradorID',
                       'ConPesoMedidaID'], [8, 14, 15]);

        glb_aCelHint = [[0, 0, '0: dados do produto sem a embalagem, 1: dados da embalagem, >1: dados da embalagem externa'],
						[0, 1, 'Refere-se �s informa��es do produto na caixa externa'],
						[0, 2, 'Peso (kg)'],
						[0, 3, 'Peso OK?'],
						[0, 4, 'Largura (mm)'],
						[0, 5, 'Altura (mm)'],
						[0, 6, 'Profundidade (mm)'],
						[0, 7, 'Medidas OK?'],
						[0, 8, 'C�digo GTIN da pe�a (linha com quant 0) ou das caixas (linhas com quant > 1)'],
						[0, 8, 'Quant = 0: GTIN-8/12/13, Quant > 0: GTIN-14'],
						[0, 9, 'Cubagem (m3)'],
						[0, 10, 'Peso Cubado Rodovi�rio (kg)'],
						[0, 11, 'Peso Cubado A�reo (kg)']];

        fillGridMask(fg, currDSO, ['Quantidade',
								 'Caixa',
								 'Peso',
								 'PesoOK',
								 'Largura',
								 'Altura',
								 'Profundidade',
								 'MedidasOK',
								 'CodigoGTIN*',
                                 '^ConPesoMedidaID^dso01GridLkp^ConPesoMedidaID^Cubagem*',
                                 '^ConPesoMedidaID^dso01GridLkp^ConPesoMedidaID^PesoCubadoRodoviario*',
                                 '^ConPesoMedidaID^dso01GridLkp^ConPesoMedidaID^PesoCubadoAereo*',
								 'Observacao',
                                 '^ColaboradorID^dso02GridLkp^PessoaID^Fantasia*',
								 'ColaboradorID',
								 'ConPesoMedidaID'],
                                 ['9999', '', '999.9999', '', '99999', '99999', '99999', '', '', '', '', '', '', '', '', ''],
                                 ['####', '', '##0.0000', '', '#####', '#####', '#####', '', '', '', '', '', '', '', '', '']);

        alignColsInGrid(fg, [0, 2, 4, 5, 6, 9, 10, 11]);
    }
    else if (folderID == 21011) // Caracteristicas
    {
        headerGrid(fg, ['Ordem',
                       'OK',
                       'Caracter�stica',
                       'Valor',
                       'FT',
                       'Rec',
                       'Desc',
                       'DC',
                       'Obrig',
                       'Img',
                       'PM',
                       'Web',
					   'RelacaoID',
                       'CaracteristicaID',
					   'PesquisaWeb',
                       'ConCaracteristicaID'], [12, 13, 14, 15]);

        fillGridMask(fg, currDSO, ['^CaracteristicaID^dso03GridLkp^CaracteristicaID^Ordem*',
                                 'Checado',
                                 '^CaracteristicaID^dso01GridLkp^ConceitoID^Conceito*',
                                 'Valor',
                                 'FichaTecnica*',
                                 'Recebimento*',
                                 'Descricao*',
                                 'DescricaoComplementar*',
                                 'Obrigatorio*',
                                 'Imagens*',
                                 'PesosMedidas*',
                                 'Web*',
								 '^CaracteristicaID^dso02GridLkp^ConceitoID^RelacaoID*',
                                 'CaracteristicaID*',
                                 'PesquisaWeb',
                                 'ConCaracteristicaID'],
                                 ['99', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '']);

        glb_aCelHint = [[0, 4, 'Esta Caracter�stica ser� usada na Ficha T�cnica do produto?'],
                        [0, 5, 'Esta Caracter�stica ser� usada no Recebimento do produto?'],
                        [0, 6, 'Esta Caracter�stica ser� usada para compor a Descri��o do produto?'],
						[0, 7, 'Esta Caracter�stica ser� usada para compor a Descri��o Complementar do produto?'],
						[0, 8, 'Esta Caracter�stica � obrigatoria para essa fam�lia?'],
                        [0, 9, 'Esta Caracter�stica ser� usada para replicar imagens para os produtos da mesma s�rie desta fam�lia?'],
                        [0, 10, 'Esta Caracter�stica ser� usada para replicar pesos e medidas para os produtos da mesma s�rie desta fam�lia?'],
                        [0, 11, 'Esta Caracter�stica ser� usada na pesquisa da Web?']];


        /*for (i = 1; i < fg.Rows; i++) {
        if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'PesquisaWeb')) != 0)
        fg.Cell(6, i, getColIndexByColKey(fg, 'Valor'), i, getColIndexByColKey(fg, 'Valor')) = 0X8CE6F0;
        }*/
    }
        // Estoques
    else if (folderID == 21012) {
        headerGrid(fg, ['Empresa', 'Dispon�vel',
                       'EPO', 'EPE', 'EPD', 'EPC',
                       'ETO', 'ETE', 'ETD', 'ETC',
                       'RC', 'RCC', 'RV', 'RVC'], []);

        fillGridMask(fg, currDSO, ['Fantasia', 'Disponivel',
                                 'EstoquePO', 'EstoquePE', 'EstoquePD', 'EstoquePC',
                                 'EstoqueTO', 'EstoqueTE', 'EstoqueTD', 'EstoqueTC',
                                 'EstoqueRC', 'EstoqueRCC', 'EstoqueRV', 'EstoqueRVC'],
                                 ['', '', '', '', '', '', '', '', '', '', '', '', '', ''],
                                 ['', '#####', '#####', '#####', '#####', '#####', '#####', '#####', '#####', '#####', '#####', '#####', '#####', '#####']);

        gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[1, '#####', 'S'], [2, '#####', 'S'], [3, '#####', 'S'], [4, '#####', 'S'],
															  [5, '#####', 'S'], [6, '#####', 'S'], [7, '#####', 'S'], [8, '#####', 'S'],
															  [9, '#####', 'S'], [10, '#####', 'S'], [11, '#####', 'S'], [12, '#####', 'S'], [13, '#####', 'S']]);

        alignColsInGrid(fg, [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]);
        fg.FrozenCols = 1;
    }

    else if (folderID == 21013) // Aliquotas
    {
        if (sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['RegraAliquotaID'].value") == 333) {
            headerGrid(fg, ['Localidade Origem',
                           'Localidade Destino',
                           'Al�quota',
                           'Origem',
                           'LocalidadeOrigemID',
                           'LocalidadeDestinoID',
                           'ConAliquotaID'], [4, 5, 6]);

            glb_aCelHint = [[0, 3, 'Origem do Produto']];

            fillGridMask(fg, currDSO, ['^LocalidadeOrigemID^dso01GridLkp^LocalidadeID^Localidade*',
                                     '^LocalidadeDestinoID^dso02GridLkp^LocalidadeID^Localidade*',
                                     'Aliquota',
                                     'Produto_OrigemID',
                                     'LocalidadeOrigemID',
                                     'LocalidadeDestinoID',
                                     'ConAliquotaID'],
                                     ['', '', '999.99', '', '', '', ''],
                                     ['', '', '###.00', '', '', '', '']);
        }
        else {
            headerGrid(fg, ['Localidade Origem',
                           'Localidade Destino',
                           'Al�quota',
                           'Origem',
                           'LocalidadeOrigemID',
                           'LocalidadeDestinoID',
                           'ConAliquotaID'], [1, 4, 5, 6]);

            glb_aCelHint = [[0, 3, 'Origem do Produto']];

            fillGridMask(fg, currDSO, ['^LocalidadeOrigemID^dso01GridLkp^LocalidadeID^Localidade*',
                                     '^LocalidadeDestinoID^dso02GridLkp^LocalidadeID^Localidade*',
                                     'Aliquota',
                                     'Produto_OrigemID',
                                     'LocalidadeOrigemID',
                                     'LocalidadeDestinoID',
                                     'ConAliquotaID'],
                                     ['', '', '999.99', '', '', '', '']);
        }
    }

    else if (folderID == 21014) // Mensagens
    {
        headerGrid(fg, ['ID',
                       'Est',
                       'Mensagem',
                       'MensagemID'], [3]);

        fillGridMask(fg, currDSO, ['MensagemID*',
                                 '^EstadoID^dsoStateMachineLkp^RecursoID^Estado*',
                                 'Mensagem',
                                 'MensagemID'],
                                 ['', '', '', '']);
    }
    else if (folderID == 21015) // Rel entre Conceitos
    {
        currCmb2Data = getCmbCurrDataInControlBar('inf', 2);
        aValue = currCmb2Data[1].split(',');

        if (aValue[1] == 'SUJEITO') {
            headerGrid(fg, ['ID',
                           'Est',
                            aValue[2],
                           '_calc_HoldKey_1',
                           'TipoRelacaoID',
                           'SujeitoID',
                           'RelacaoID'], [3, 4, 5, 6]);

            fillGridMask(fg, currDSO, ['RelacaoID*',
                                     '^EstadoID^dsoStateMachineLkp^RecursoID^Estado*',
                                     '^SujeitoID^dso01GridLkp^ConceitoID^Conceito*',
                                     '_calc_HoldKey_1',
                                     'TipoRelacaoID',
                                     'SujeitoID',
                                     'RelacaoID'], ['', '', '', '9', '', '']);
        }
        else if (aValue[1] == 'OBJETO') {
            headerGrid(fg, ['ID',
                           'Est',
                            aValue[2],
                           '_calc_HoldKey_1',
                           'TipoRelacaoID',
                           'ObjetoID',
                           'RelacaoID'], [3, 4, 5, 6]);

            fillGridMask(fg, currDSO, ['RelacaoID*',
                                     '^EstadoID^dsoStateMachineLkp^RecursoID^Estado*',
                                     '^ObjetoID^dso01GridLkp^ConceitoID^Conceito*',
                                     '_calc_HoldKey_1',
                                     'TipoRelacaoID',
                                     'ObjetoID',
                                     'RelacaoID'], ['', '', '', '9', '', '']);
        }
    }
    else if (folderID == 21016) // Rel com Pessoas
    {
        currCmb2Data = getCmbCurrDataInControlBar('inf', 2);
        aValue = currCmb2Data[1].split(',');

        headerGrid(fg, ['ID',
                       'Est',
                        aValue[2],
                       '_calc_HoldKey_1',
                       'TipoRelacaoID',
                       'SujeitoID',
                       'RelacaoID'], [3, 4, 5, 6]);

        fillGridMask(fg, currDSO, ['RelacaoID*',
                                 '^EstadoID^dsoStateMachineLkp^RecursoID^Estado*',
                                 '^SujeitoID^dso01GridLkp^PessoaID^Fantasia*',
                                 '_calc_HoldKey_1',
                                 'TipoRelacaoID',
                                 'SujeitoID',
                                 'RelacaoID'], ['', '', '', '9', '', '']);
    }
    else if (folderID == 21017) // Tempo de Producao
    {
        headerGrid(fg, ['Estado',
                       'Tempo',
                       'ConTempoID'], [2]);

        fillGridMask(fg, currDSO, ['EstadoID',
								 'Tempo',
								 'ConTempoID'], ['', '999.99', ''],
											  ['', '##0.00', '']);

        gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[1, '#####', 'S']]);
        alignColsInGrid(fg, [1]);
    }
    else if (folderID == 21020) // Argumento de Vendas
    {
        var i;

        headerGrid(fg, ['Idioma',
                       'Argumentos de venda',
                       'OK',
                       'Argumentos de venda',
                       'ConArgumentoID'], [3, 4]);

        fillGridMask(fg, currDSO, ['IdiomaID',
								 '_calc_ArgumentoAbreviado*',
								 'OK',
								 'ArgumentosVenda',
								 'ConArgumentoID'], ['', '', '', '', ''],
											  ['', '', '', '', '']);
        for (i = 1; i < fg.Rows; i++) {
            currDSO.recordset.MoveFirst();
            var currID = fg.TextMatrix(i, fg.Cols - 1);

            currDSO.recordset.find('ConArgumentoID', currID);

            if (!currDSO.recordset.EOF) {
                if (currDSO.recordset['ArgumentoAbreviado'].value != null)
                    fg.TextMatrix(i, getColIndexByColKey(fg, '_calc_ArgumentoAbreviado*')) = currDSO.recordset['ArgumentoAbreviado'].value;
                else
                    fg.TextMatrix(i, getColIndexByColKey(fg, '_calc_ArgumentoAbreviado*')) = '';

            }
            else {
                fg.TextMatrix(i, getColIndexByColKey(fg, '_calc_ArgumentoAbreviado*')) = '';
            }
        }
    }
    else if (folderID == 21021) // Protocolo
    {
        headerGrid(fg, ['Imposto',
                       'Setor',
                       'UF Origem',
                       'UF Destino',
                       'IVAST',
                       'Base',
                       'Al�quota',
                       'Legisla��o',
                       'Item',
                       'Data',
                       'Vig Inic�o',
                       'Vig Fim',
                       'Observacao',
                       'Observacoes',
                       'ConProtocoloID'], [13, 14]);

        fillGridMask(fg, currDSO, ['ImpostoID',
                       'Setor',
                       'UFOrigemID',
                       'UFDestinoID',
                       'IVAST',
                       'BaseCalculo',
                       'Aliquota',
                       'Legislacao',
                       'Item',
                       'dtLegislacao',
                       'dtVigenciaInicio',
                       'dtVigenciaFim',
                       'Observacao',
                       'Observacoes',
                       'ConProtocoloID'], ['', '', '', '', '999.99', '999.99', '999.99', '', '', '99/99/9999', '99/99/9999', '99/99/9999', '', '', ''],
										['', '', '', '', '##0.00', '##0.00', '##0.00', '', '', '99/99/9999', '99/99/9999', '99/99/9999', '', '', '']);

        alignColsInGrid(fg, [4, 5]);
    }
    else if (folderID == 21022) // Impostos
    {
        headerGrid(fg, ['Imposto',
                       'UF',
                       'NT',
                       'Al�quota',
                       'Base',
                       'Importa��o',
                       'A�reo',
                       'Mar�timo',
                       'Observa��o',
                       'ConImpostoID'], [9]);

        glb_aCelHint = [[0, 2, 'Imposto n�o tributado?']];

        fillGridMask(fg, currDSO,
                      ['ImpostoID',
                       'UFID',
                       'NaoTributado',
                       'Aliquota',
                       'BaseCalculo',
                       'Importacao',
                       'Aereo',
                       'Maritimo',
                       'Observacao',
                       'ConImpostoID'], ['', '', '', '999.99', '999.99', '', ''],
										['', '', '', '##0.00', '##0.00', '', '']);

        alignColsInGrid(fg, [3, 4]);
    }
    else if (folderID == 21023) // Retencoes
    {
        headerGrid(fg, ['Localidade',
                           'Aliquota',
                           'PeriodoApuracao',
                           'DataUtilizada',
                           'TipoMinimo',
                           'ValorMinimo',
                           'Recolhimento Desconsiderar',
                           'Recolhimento Prazo',
                           'Beneficiario',
                           'ArrecadacaoDocumento',
                           'ArrecadacaoCodigo',
                           'LocalidadeID',
                           'ConRetencaoID'], [11, 12]);

        //glb_aCelHint = [[0, 2, 'Imposto n�o tributado?']];

        fillGridMask(fg, currDSO,
                          ['^ConRetencaoID^dso01GridLkp^ConRetencaoID^Localidade*',
                           'Aliquota',
                           'PeriodoApuracao',
                           'DataUtilizada',
                           'TipoMinimo',
                           'ValorMinimo',
                           'RecolhimentoDesconsiderar',
                           'RecolhimentoPrazo',
                           'Beneficiario',
                           'ArrecadacaoDocumento',
                           'ArrecadacaoCodigo',
                           'LocalidadeID',
                           'ConRetencaoID'],
                           ['', '999.99', '', '', '', '999999999.99', '', '', '', '', '', '', ''],
						   ['', '##0.00', '', '', '', '###,###,##0.00', '', '', '', '', '', '', '']);

        alignColsInGrid(fg, [1, 5, 7, 10]);
    }

    else if (folderID == 21024) // Servico
    {
        headerGrid(fg, ['Localidade',
                           'Est',
                           'Codigo',
                           'CodigoTomado',
                           'AliquotaISS',
                           'Descri��o Local',
                           'DescricaoLocal', //1
                           'LocalidadeID', //1
                           'UsuarioID', //1
                           'PEDI',
                           'ConServicoID'], [6, 7, 8, 10]); //1

        //glb_aCelHint = [[0, 2, 'Imposto n�o tributado?']];
        glb_aCelHint = [[0, 9, 'Padr�o EDI contabilidade.']];

        fillGridMask(fg, currDSO,
                          ['^ConServicoID^dso01GridLkp^ConServicoID^Localidade*',
                           '^EstadoID^dsoStateMachineLkp^RecursoID^Estado*',
                           'Codigo',
                           'CodigoTomador',
                           'AliquotaISS',
                           'Descricao*',
                           'DescricaoLocal',
                           'LocalidadeID',
                           'UsuarioID',
                            'PadraoEDI',
                           'ConServicoID'],
                           ['', '', '', '', '999.99', '', '', '', '', '', ''],
						   ['', '', '', '', '##0.00', '', '', '', '', '', '']);

        alignColsInGrid(fg, [2, 3, 4]);

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }

    else if (folderID == 21025) // Serv Retencoes
    {
        headerGrid(fg, ['Localidade',
                           'Imposto',
                           'Al�quota',
                           'Codigo Arrecada��o',
                           'Observa��o',
                           'LocalidadeID',
                           'ConSerRetencaoID'], [5, 6]);

        //glb_aCelHint = [[0, 2, 'Imposto n�o tributado?']];

        fillGridMask(fg, currDSO,
                          ['^ConSerRetencaoID^dso01GridLkp^ConSerRetencaoID^Localidade*',
                           'ImpostoID',
                           'Aliquota',
                           'ArrecadacaoCodigo',
                           'Observacao',
                           'LocalidadeID',
                           'ConSerRetencaoID'],
                           ['', '', '999.99', '', '', '', ''],
						   ['', '', '##0.00', '', '', '', '']);

        alignColsInGrid(fg, [2, 3]);

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
    else if (folderID == 21026) // Lojas e Canais
    {
        headerGrid(fg, ['Tipo',
                        'Loja',
                        'ConConceitoID'], [2]);

        fillGridMask(fg, currDSO,
                          ['^ConConceitoID^dso01GridLkp^ConConceitoID^Tipo*',
                           'LojaCanalID',
                           'ConConceitoID'],
                           ['', '', ''],
						   ['', '', '']);

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
    else if (folderID == 21028) // Contas
    {
        // NCM
        if (nTipoConceitoID == 309) {
            headerGrid(fg, ['ContaID',
                            'Conta',
                            'ConContaID'], [2]);

            fillGridMask(fg, currDSO,
                              ['ContaID*',
                              'ContaID',
                               'ConContaID'],
                               ['', '', ''],
						       ['', '', '']);
        }
        else {
            headerGrid(fg, ['ContaID',
                            'Conta',
                            'CV',
                            'ConContaID'], [3]);

            glb_aCelHint = [[0, 2, 'Comiss�o de Vendas. Aparece na web?']];

            fillGridMask(fg, currDSO,
                              ['ContaID*',
                              'ContaID',
                               'ComissaoVendas',
                               'ConContaID'],
                               ['', '', '', ''],
						       ['', '', '', '']);
        }
    }
    else if (folderID == 21029) //Beneficios Fiscais
    {
        headerGrid(fg, [
                        'Est',
                        'Benef�cio',
                        (nContextoID == 2112 ? 'N�mero' : 'Legisla��o'),
                        'Cadastro',
                        'Vig In�cio',
                        'Vig Fim',
                        'Observa��o',
                        'EstadoID',
                        'ConBeneficioID'], [7, 8]);


        fillGridMask(fg, currDSO,
                    [
                     '^EstadoID^dsoStateMachineLkp^RecursoID^Estado*',
                     'TipoBeneficioID',
                     'LegislacaoAplicavel',
                     'dtCadastro*',
                     'dtVigenciaInicio',
                     'dtVigenciaFim',
                     'Observacao',
                     'EstadoID',
                     'ConBeneficioID'],
                    ['', '', '', '99/99/9999', '99/99/9999', '99/99/9999', '', '', ''],
                    ['', '', '', dTFormat, dTFormat, dTFormat, '', '', '']);
    }
    else if (folderID == 21081) // CEST
    {
        headerGrid(fg, ['Cest',
                        'Descricao',
                        'ConCestID'], [2]);

        fillGridMask(fg, currDSO,
                            ['Cest',
                            'Descricao',
                            'ConCestID'],
                            ['', '', ''],
						    ['', '', '']);
    }
    else if (folderID == 21050) // PROJETO IMPORTACAO
    {
        headerGrid(fg, ['ConceitoID',
                        'Atributo',
                        'Descricao',
                        'Ordem',
                        'Especificacao',
                        'ConNVEID'], [0,5]);

        fillGridMask(fg, currDSO,
                            ['ConceitoID',
                            'Atributo',
                            'Descricao',
                            'Ordem',
                            'Especificacao',
                            'ConNVEID'],
                            ['', '', '', '', '', ''],
						    ['', '', '', '', '', '']);
    }
}
