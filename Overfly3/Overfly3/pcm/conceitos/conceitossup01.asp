<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<!-- //@@ ID e name do arquivo -->
<html id="conceitossup01Html" name="conceitossup01Html">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/clientside/form.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_interfaceex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_detailsup.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_common.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_formscontrolsbar.js" & Chr(34) & "></script>" & vbCrLf                
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/pcm/conceitos/conceitossup01.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/pcm/conceitos/especificsup.js" & Chr(34) & "></script>" & vbCrLf
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

</head>

<!-- //@@ -->
<body id="conceitossup01Body" name="conceitossup01Body" LANGUAGE="javascript" onload="return window_onload()">
    <!-- Controle para simular tab na gravacao -->
    <input type="text" id="txtNulo" name="txtNulo" class="fldGeneral">
    
    <!-- //@@ Os divs sao definidos de acordo com o form -->
    <!-- //@@ Todos os controles select cujo onchange e invocado convergem para -->
    <!-- a funcao optChangedInCmb(cmb), que esta no sup01.js" -->
    
    <!-- Div principal superior - Conceito -->
	<div id="divSup01_01" name="divSup01_01" class="divExtern">
        <!-- Os dois controles RegistroID e EstadoID abaixo sao comuns a todos os forms -->
        <p class="lblGeneral" id="lblRegistroID">ID</p>
        <input type="text" id="txtRegistroID" name="txtRegistroID" class="fldGeneral">
        <p id="lblEstadoID" name="lblEstadoID" class="lblGeneral">Est</p>
        <input type="text" id="txtEstadoID" name="txtEstadoID" DATASRC="#dsoStateMachine" DATAFLD="Estado" name="txtEstadoID" class="fldGeneral">        
	    <p id="lblConceito" name="lblConceito" class="lblGeneral">Conceito</p>
	    <input type="text" id="txtConceito" name="txtConceito" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Conceito">
	    <p id="lblTipoRegistroID" name="lblTipoRegistroID" class="lblGeneral">Tipo</p>
	    <select id="selTipoRegistroID" name="selTipoRegistroID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="TipoConceitoID"></select>
	</div>

	<!-- Primeiro div secundario superior - Conceito Abstrato -->    
	<div id="divSup02_01" name="divSup02_01" class="divExtern">
		<p id="lblHrConceitoAbstrato" name="lblHrConceitoAbstrato" class="lblGeneral">Conceito Abstrato</p>
		<hr id="hrConceitoAbstrato" name="hrConceitoAbstrato" class="lblGeneral">
	    <p id="lblNivel" name="lblNivel" class="lblGeneral">N�vel</p>
	    <select id="selNivel" name="selNivel" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Nivel"></select>
	</div>

    <!-- Segundo div secundario superior - Conceito Concreto -->
	<div id="divSup02_02" name="divSup02_02" class="divExtern">
		<p id="lblHrConceitoConcreto" name="lblHrConceitoConcreto" class="lblGeneral">Conceito Concreto</p>
		<hr id="hrConceitoConcreto" name="hrConceitoConcreto" class="lblGeneral">
	    <p id="lblConceitoAbreviado_02" name="lblConceitoAbreviado_02" class="lblGeneral">Abrev</p>
	    <input type="text" id="txtConceitoAbreviado_02" name="txtConceitoAbreviado_02" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="ConceitoAbreviado">
	    <p id="lblOrdemConcreto" name="lblOrdemConcreto" class="lblGeneral">Ordem</p>
	    <input type="text" id="txtOrdemConcreto" name="txtOrdemConcreto" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Ordem">	    
	    <p id="lblEhConfiguravel" name="lblEhConfiguravel" class="lblGeneral">Config</p>
	    <input type="checkbox" id="chkEhConfiguravel" name="chkEhConfiguravel" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="EhConfiguravel" Title="� configur�vel?">	    
	    <p id="lblEhServico" name="lblEhServico" class="lblGeneral">Servi�o</p>
	    <input type="checkbox" id="chkEhServico" name="chkEhServico" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="EhServico" Title="� servi�o?">
	    <p id="lblEhMicro" name="lblEhMicro" class="lblGeneral">Micro</p>
	    <input type="checkbox" id="chkEhMicro" name="chkEhMicro" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="EhMicro" Title="� micro?">
	    <p id="lblEhGabinete" name="lblEhGabinete" class="lblGeneral">Gabinete</p>
	    <input type="checkbox" id="chkEhGabinete" name="chkEhGabinete" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="EhGabinete" Title="� gabinete?">
	    <p id="lblFamiliaPrincipal" name="lblFamiliaPrincipal" class="lblGeneral">FP</p>
	    <input type="checkbox" id="chkFamiliaPrincipal" name="chkFamiliaPrincipal" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="FamiliaPrincipal" Title="Fam�lia principal?">
	    <p id="lblAceitaMaisProdutos" name="lblAceitaMaisProdutos" class="lblGeneral">AMP</p>
	    <input type="checkbox" id="chkAceitaMaisProdutos" name="chkAceitaMaisProdutos" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="AceitaMaisProdutos" Title="Aceita mais produtos?">
	    <p id="lblBloqueiaMicroSemEste" name="lblBloqueiaMicroSemEste" class="lblGeneral">BMSE</p>
	    <input type="checkbox" id="chkBloqueiaMicroSemEste" name="chkBloqueiaMicroSemEste" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="BloqueiaMicroSemEste" Title="Bloqueia micro sem este produto?">
	    <p id="lblHomologavel" name="lblHomologavel" class="lblGeneral">Hom</p>
	    <input type="checkbox" id="chkHomologavel" name="chkHomologavel" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Homologavel" Title="Conceito homolog�vel?">
	    <p id="lblReplicar" name="lblReplicar" class="lblGeneral">Replicar</p>
	    <input type="checkbox" id="chkReplicar" name="chkReplicar" class="fldGeneral" onclick="return chkReplicar_onclick()" DATASRC="#dsoSup01" DATAFLD="Replicar" Title="Replicar dados de produtos desta familia?">	    
        <p id="lblPeso" name="lblPeso" class="lblGeneral">PL</p>
	    <input type="checkbox" id="chkPeso" name="chkPeso" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="PesoLiquido" Title="Peso Liquido � relevante?">	    
        <p id="lblDescricaoAuto" name="lblDescricaoAuto" class="lblGeneral">Auto</p>
	    <input type="checkbox" id="chkDescricaoAuto" name="chkDescricaoAuto" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="PermiteAlterarDescricaoAutomatica" Title="Permite alterar descri��o autom�tica do produto?">	    
	    <p id="lblTamanho" name="lblTamanho" class="lblGeneral" title="Tamanho do produto (1-pequeno, 2-m�dio, 3-grande)">Tam</p>
	    <select id="selTamanho" name="selTamanho" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Tamanho"></select>
	    
        <p id="lblImagensRequeridas" name="lblImagensRequeridas" class="lblGeneral" title="N�mero de imagens requeridas">Imagens Requeridas</p>
	    <select id="selImagensRequeridas" name="selImagensRequeridas" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="ImagensRequeridas">
            <OPTION VALUE=0>0</OPTION>
            <OPTION VALUE=3>3</OPTION>
            <OPTION VALUE=4>4</OPTION>
            <OPTION VALUE=5>5</OPTION>
            <OPTION VALUE=6>6</OPTION>
            <OPTION VALUE=7>7</OPTION>

	    </select>
	    
        <p id="lblValorEstoqueSeguro" name="lblValorEstoqueSeguro" class="lblGeneral">Estoque seguro</p>
	    <input type="text" id="txtValorEstoqueSeguro" name="txtValorEstoqueSeguro" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="ValorEstoqueSeguro" title="Valor m�nimo que requer estoque seguro">
	    <p id="lblSinonimos" name="lblSinonimos" class="lblGeneral">Sin�nimos</p>
	    <input type="text" id="txtSinonimos" name="txtSinonimos" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Sinonimos" title="">
	    <p id="lblObservacao" name="lblObservacao" class="lblGeneral">Observa��o</p>
	    <input type="text" id="txtObservacao" name="txtObservacao" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Observacao">
	</div>

    <!-- Terceiro div secundario superior - Produto -->
	<div id="divSup02_03" name="divSup02_03" class="divExtern">
	    <p id="lblClassificacaoID" name="lblClassificacaoID" class="lblGeneral">Classifica��o</p>
	    <select id="selClassificacaoID" name="selClassificacaoID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="ClassificacaoID"></select>
	    <p id="lblProdutoID" name="lblProdutoID" class="lblGeneral">Produto</p>
	    <select id="selProdutoID" name="selProdutoID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="ProdutoID"></select>
	    <p id="lblFabricanteID" name="lblFabricanteID" class="lblGeneral">Fabricante</p>
	    <select id="selFabricanteID" name="selFabricanteID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="FabricanteID"></select>
        <input type="image" id="btnFindFabricante" name="btnFindFabricante" class="fldGeneral" title="Preencher combo" WIDTH="24" HEIGHT="23">
	    <p id="lblMarcaID" name="lblMarcaID" class="lblGeneral">Marca</p>
	    <select id="selMarcaID" name="selMarcaID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="MarcaID"></select>
        <input type="image" id="btnFindMarca" name="btnFindMarca" class="fldGeneral" title="Preencher combo" WIDTH="24" HEIGHT="23">
	    <p id="lblLinhaProdutoID" name="lblLinhaProdutoID" class="lblGeneral">Linha de Produto</p>
	    <select id="selLinhaProdutoID" name="selLinhaProdutoID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="LinhaProdutoID"></select>
        <input type="image" id="btnFindLinhaProduto" name="btnFindLinhaProduto" class="fldGeneral" title="Preencher combo" WIDTH="24" HEIGHT="23">
	    <p id="lblModelo" name="lblModelo" class="lblGeneral">Modelo</p>
	    <input type="text" id="txtModelo" name="txtModelo" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Modelo">
	    <p id="lblSerie" name="lblSerie" class="lblGeneral">S�rie</p>
	    <input type="text" id="txtSerie" name="txtSerie" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Serie">
	    <p id="lblDescricao" name="lblDescricao" class="lblGeneral">Descri��o</p>
	    <input type="text" id="txtDescricao" name="txtDescricao" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Descricao" Title="Esta Caracter�stica ser� usada para compor a Descri��o do produto??">
	    <p id="lblDescricaoComplementar" name="lblDescricaoComplementar" class="lblGeneral">Descri��o Complementar</p>
	    <input type="text" id="txtDescricaoComplementar" name="txtDescricaoComplementar" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="DescricaoComplementar" Title="Esta Caracter�stica ser� usada para compor a Descri��o Complementar do produto?">
	    <p id="lblPartNumber" name="lblPartNumber" class="lblGeneral">Part Number</p>
	    <input type="text" id="txtPartNumber" name="txtPartNumber" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="PartNumber">
	    <p id="lblUnidadeID" name="lblUnidadeID" class="lblGeneral">Unidade</p>
	    <select id="selUnidadeID" name="selUnidadeID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="UnidadeID"></select>
		<p id="lblTamanho2" name="lblTamanho2" class="lblGeneral" title="Tamanho do produto (1-pequeno, 2-m�dio, 3-grande)">Tam</p>
		<select id="selTamanho2" name="selTamanho2" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Tamanho"></select>
	    <p id="lblInternacional" name="lblInternacional" class="lblGeneral">Int</p>
	    <input type="checkbox" id="chkInternacional" name="chkInternacional" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Internacional" Title="Produto internacional?">
	    <p id="lblAuto" name="lblAuto" class="lblGeneral">Auto</p>
	    <input type="checkbox" id="chkAuto" name="chkAuto" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="DescricaoAutomatica"  onclick="return chkAuto_onclick()" Title="Compor Descri��o e Descri��o complementar automaticamente?">
	    <p id="lblControlaEstoque" name="lblControlaEstoque" class="lblGeneral">CE</p>
	    <input type="checkbox" id="chkControlaEstoque" name="chkControlaEstoque" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="ControlaEstoque" Title="Controla estoque?">
	    <p id="lblProdutoSeparavel" name="lblProdutoSeparavel" class="lblGeneral">PS</p>
	    <input type="checkbox" id="chkProdutoSeparavel" name="chkProdutoSeparavel" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="ProdutoSeparavel" Title="Produto separ�vel? (� separado pela embalagem)">
	    <p id="lblNumerosSerieAlternativos" name="lblNumerosSerieAlternativos" class="lblGeneral">NSA</p>
	    <input type="text" id="txtNumerosSerieAlternativos" name="txtNumerosSerieAlternativos" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="NumerosSerieAlternativos" Title="Quantidade de N�meros de S�rie Alternativos">
	    <p id="lblControle" name="lblControle" class="lblGeneral">Contr</p>
	    <input type="checkbox" id="chkControle" name="chkControle" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Controle" Title="Controle: 1-Produto deve ser gatilhado item a item. 0-Sistema gera a quantidade de n�meros de s�rie contidos na caixa do produto.">
	    <p id="lblNumeroSerieEspecifico" name="lblNumeroSerieEspecifico" class="lblGeneral">NSE</p>
	    <input type="checkbox" id="chkNumeroSerieEspecifico" name="chkNumeroSerieEspecifico" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="NumeroSerieEspecifico" Title="Tem N�mero de S�rie Espec�fico?">
	    <p id="lblEtiquetaPropria" name="lblEtiquetaPropria" class="lblGeneral">EP</p>
	    <input type="checkbox" id="chkEtiquetaPropria" name="chkEtiquetaPropria" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="EtiquetaPropria" Title="Etiqueta pr�pria?">
	    <p id="lblImagem_Produto" name="lblImagem_Produto" class="lblGeneral">Imagem</p>
	    <input type="checkbox" id="chkImagem_Produto" name="chkImagem_Produto" class="fldGeneral" Title="Tem imagem dispon�vel?">
	    <p id="lblIdentificadoresOK" name="lblIdentificadoresOK" class="lblGeneral">Id</p>
	    <input type="checkbox" id="chkIdentificadoresOK" name="chkIdentificadoresOK" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="IdentificadoresOK" Title="Identificadores OK?">
	    <p id="lblHomologacaoOK" name="lblHomologacaoOK" class="lblGeneral">Hom</p>
	    <input type="checkbox" id="chkHomologacaoOK" name="chkHomologacaoOK" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="HomologacaoOK" Title="Homologa��o OK?">
	    <p id="lblTemCodigoGTIN" name="lblTemCodigoGTIN" class="lblGeneral">GTIN</p>
	    <input type="checkbox" id="chkTemCodigoGTIN" name="chkTemCodigoGTIN" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="TemCodigoGTIN" Title="Produto possui c�digo GTIN?">
	    <p id="lblCad" name="lblCad" class="lblGeneral">Conf</p>
	    <input type="text" id="txtCad" name="txtCad" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Conformidade_Conf" title="">
	    <p id="lblCodigoBNDES" name="lblCodigoBNDES" class="lblGeneral">BNDES</p>
	    <input type="text" id="txtCodigoBNDES" name="txtCodigoBNDES" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="CodigoBNDES">
        <p id="lblDescricaoFiscal" name="lblDescricaoFiscal" class="lblGeneral">Descri��o Fiscal</p>
	    <input type="text" id="txtDescricaoFiscal" name="txtDescricaoFiscal" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="DescricaoFiscal">
	</div>
	
	<!-- Quarto div secundario superior - Marca -->
	<div id="divSup02_04" name="divSup02_04" class="divExtern">
		<p id="lblHrMarca" name="lblHrMarca" class="lblGeneral">Marca</p>
		<hr id="hrMarca" name="hrMarca" class="lblGeneral">
	    <p id="lblConceitoAbreviado_04" name="lblConceitoAbreviado_04" class="lblGeneral">Abrev</p>
	    <input type="text" id="txtConceitoAbreviado_04" name="txtConceitoAbreviado_04" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="ConceitoAbreviado">
        <p id="lblDescricaoModelo" name="lblDescricaoModelo" class="lblGeneral">Modelo</p>
	    <input type="checkbox" id="chkDescricaoModelo" name="chkDescricaoModelo" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="DescricaoModelo" Title="Inclui o modelo na descri��o do produto para esta marca?">
        <p id="lblDescricaoPartnumber" name="lblDescricaoPartnumber" class="lblGeneral">Part Number</p>
	    <input type="checkbox" id="chkDescricaoPartnumber" name="chkDescricaoPartnumber" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="DescricaoPartnumber" Title="Inclui o part number na descri��o do produto para esta marca?">
        <p id="lblDescricaoInicio" name="lblDescricaoInicio" class="lblGeneral">Descri��o</p>
	    <input type="checkbox" id="chkDescricaoInicio" name="chkDescricaoInicio" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="DescricaoInicio" Title="Insere o modelo e/ou part number no inicio da descri��o do produto para esta marca?">
	    <p id="lblPublica" name="lblPublica" class="lblGeneral">Publica</p>
	    <input type="checkbox" id="chkPublica" name="chkPublica" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Publica" Title="Publica Marca na internet?">
	    <p id="lbldestacainternet" name="lbldestacainternet" class="lblGeneral">Destaca Internet</p>
	    <input type="checkbox" id="chkdestacainternet" name="chkdestacainternet" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="DestacaInternet" Title="Destaca marca na internet? (barra esquerda da p�gina)">
	    <p id="lblImagem_Marca" name="lblImagem_Marca" class="lblGeneral">Imagem</p>
	    <input type="checkbox" id="chkImagem_Marca" name="chkImagem_Marca" class="fldGeneral" Title="Tem imagem dispon�vel?">
	    <p id="lblSinonimos2" name="lblSinonimos2" class="lblGeneral">Sin�nimos</p>
	    <input type="text" id="txtSinonimos2" name="txtSinonimos2" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Sinonimos">
	</div>
	
	<!-- Quinto div secundario superior - Imposto -->
	<div id="divSup02_05" name="divSup02_05" class="divExtern">
		<p id="lblHrImposto" name="lblHrImposto" class="lblGeneral">Imposto</p>
		<hr id="hrImposto" name="hrImposto" class="lblGeneral">
	    <p id="lblOrdem" name="lblOrdem" class="lblGeneral">Ordem</p>
	    <input type="text" id="txtOrdem" name="txtOrdem" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Ordem">
	    <p id="lblImposto" name="lblImposto" class="lblGeneral">Imposto</p>
	    <input type="text" id="txtImposto" name="txtImposto" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Imposto">
	    <p id="lblPaisID" name="lblPaisID" class="lblGeneral">Pa�s</p>
	    <select id="selPaisID" name="selPaisID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="PaisID"></select>
	    <p id="lblNivelGovernoID" name="lblNivelGovernoID" class="lblGeneral">N�vel de Governo</p>
	    <select id="selNivelGovernoID" name="selNivelGovernoID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="NivelGovernoID"></select>
	    <p id="lblIncidePreco" name="lblIncidePreco" class="lblGeneral">Pre�o</p>
	    <input type="checkbox" id="chkIncidePreco" name="chkIncidePreco" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="IncidePreco" Title="Imposto incide no pre�o?">
	    <p id="lblInclusoPreco" name="lblInclusoPreco" class="lblGeneral">Incluso</p>
	    <input type="checkbox" id="chkInclusoPreco" name="chkInclusoPreco" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="InclusoPreco" Title="Imposto j� incluso no pre�o?">
	    <p id="lblEsquemaCredito" name="lblEsquemaCredito" class="lblGeneral" LANGUAGE="javascript">Cr�dito</p>
	    <input type="checkbox" id="chkEsquemaCredito" name="chkEsquemaCredito" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="EsquemaCredito" Title="Imposto tem esquema de cr�dito?">
	    <p id="lblAlteraAliquota" name="lblAlteraAliquota" class="lblGeneral">Altera</p>
	    <input type="checkbox" id="chkAlteraAliquota" name="chkAlteraAliquota" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="AlteraAliquota" Title="Imposto permite alterar aliquota?">
	    <p id="lblDestacaNota" name="lblDestacaNota" class="lblGeneral">Nota</p>
	    <input type="checkbox" id="chkDestacaNota" name="chkDestacaNota" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="DestacaNota" Title="Imposto destacado na nota?">
	    <p id="lblAIC" name="lblAIC" class="lblGeneral">AIC</p>
	    <input type="checkbox" id="chkAIC" name="chkAIC" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="AlteraImpostosComplementares" Title="Permite altera��o nos impostos complementares no pedido?">
	    <p id="lblRegraAliquotaID" name="lblRegraAliquotaID" class="lblGeneral">Regra</p>
	    <select id="selRegraAliquotaID" name="selRegraAliquotaID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="RegraAliquotaID"></select>
	</div>
	
	<!-- Sexto div secundario superior - Grupo de Imposto -->
	<div id="divSup02_06" name="divSup02_06" class="divExtern">
		<p id="lblHrGrupodeImposto" name="lblHrGrupodeImposto" class="lblGeneral">Grupo de Imposto</p>
		<hr id="hrGrupodeImposto" name="hrGrupodeImposto" class="lblGeneral">
	    <p id="lblImpostoOrdem" name="lblImpostoOrdem" class="lblGeneral">Ordem</p>
	    <input type="text" id="txtImpostoOrdem" name="txtImpostoOrdem" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Ordem">
	    <p id="lblImpostoPaisID" name="lblImpostoPaisID" class="lblGeneral">Pa�s</p>
	    <select id="selImpostoPaisID" name="selImpostoPaisID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="PaisID"></select>
	    <p id="lblGrupoAlternativoID" name="lblGrupoAlternativoID" class="lblGeneral">Alternativo</p>
	    <select id="selGrupoAlternativoID" name="selGrupoAlternativoID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="GrupoAlternativoID"></select>
	</div>

	<!-- S�timo div secundario superior - Moeda -->
	<div id="divSup02_07" name="divSup02_07" class="divExtern">
		<p id="lblHrMoeda" name="lblHrMoeda" class="lblGeneral">Moeda</p>
		<hr id="hrMoeda" name="hrMoeda" class="lblGeneral">
	    <p id="lblMoedaPaisID" name="lblMoedaPaisID" class="lblGeneral">Pa�s</p>
	    <select id="selMoedaPaisID" name="selMoedaPaisID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="PaisID"></select>
	    <p id="lblCodigo" name="lblCodigo" class="lblGeneral">C�digo</p>
	    <input type="text" id="txtCodigo" name="txtCodigo" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="ConceitoAbreviado" title="C�digo Internacional da moeda">
	    <p id="lblSimbolo" name="lblSimbolo" class="lblGeneral">S�mbolo</p>
	    <input type="text" id="txtSimbolo" name="txtSimbolo" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="SimboloMoeda">
	    <p id="lblCodigoConceito" name="lblCodigoConceito" class="lblGeneral">C�d BC</p>
	    <input type="text" id="txtCodigoConceito" name="txtCodigoConceito" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="CodigoConceito" title="C�digo Banco Central">
	    
	</div>
    
	<!-- Oitavo div secundario superior - Classificacao Fiscal -->
	<div id="divSup02_08" name="divSup02_08" class="divExtern">
		<p id="lblHrClassificacaoFiscal" name="lblHrClassificacaoFiscal" class="lblGeneral">Classifica��o Fiscal</p>
		<hr id="hrClassificacaoFiscal" name="hrClassificacaoFiscal" class="lblGeneral">
	    <p id="lblPaisID2" name="lblPaisID2" class="lblGeneral">Pa�s</p>
	    <select id="selPaisID2" name="selPaisID2" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="PaisID"></select>
	    <p id="lblClassificacaoFiscal" name="lblClassificacaoFiscal" class="lblGeneral">Classifica��o Fiscal</p>
	    <input type="text" id="txtClassificacaoFiscal" name="txtClassificacaoFiscal" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="ClassificacaoFiscal" title="">
	    <p id="lblMPBem" name="lblMPBem" class="lblGeneral">MP Bem</p>
	    <input type="checkbox" id="chkMPBem" name="chkMPBem" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="MPBem" Title="Classifica��o faz parte da MP do Bem?">
		<p id="lblObservacaoImposto" name="lblObservacaoImposto" class="lblGeneral">Observa��o</p>
	    <input type="text" id="txtObservacaoImposto" name="txtObservacaoImposto" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Observacao">
	</div>
	
	<!-- Nono div secundario superior - NBS -->
	<div id="divSup02_09" name="divSup02_09" class="divExtern">
		<p id="lblHrNBS" name="lblHrNBS" class="lblGeneral">Nomenclatura Brasileira de Servi�o (NBS)</p>
		<hr id="hrNBS" name="hrNBS" class="lblGeneral">
	    <p id="lblPaisID3" name="lblPaisID3" class="lblGeneral">Pa�s</p>
	    <select id="selPaisID3" name="selPaisID3" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="PaisID"></select>
	    <p id="lblNBS" name="lblNBS" class="lblGeneral">NBS</p>
	    <input type="text" id="txtNBS" name="txtNBS" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="NBS" title="">
		<p id="lblObservacaoNBS" name="lblObservacaoNBS" class="lblGeneral">Observa��o</p>
	    <input type="text" id="txtObservacaoNBS" name="txtObservacaoNBS" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Observacao">
	</div>
	
	<!-- decimo div secundario superior - Servi�o Federal -->
	<div id="divSup02_10" name="divSup02_10" class="divExtern">
		<p id="lblSvf" name="lblSvf" class="lblGeneral">Servi�o Federal</p>
		<hr id="hrSvf" name="hrSvf" class="lblGeneral">
	    <p id="lblObservacaoConceito" name="lblObservacaoConceito" class="lblGeneral">Observa��o</p>
	    <input type="text" id="txtObservacaoConceito" name="txtObservacaoConceito" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Observacao">
	</div>

</body>

</html>
