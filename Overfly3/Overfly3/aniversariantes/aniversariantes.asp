
<%@ LANGUAGE=VBSCRIPT %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Response.Write "<script ID=" & Chr(34) & "serverCfgVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
    Response.Write vbCrLf
    
    Response.Write "var __APP_NAME__ = " & Chr(39) & Application("appName") & Chr(39) & ";"
    Response.Write vbCrLf
    Response.Write "var __PAGES_DOMINIUM__ = " & Chr(39) & objSvrCfg.PagesDominium(Application("appName")) & Chr(39) & ";"
    Response.Write vbCrLf
    Response.Write "var __PAGES_URLROOT__ = " & Chr(39) & objSvrCfg.PagesURLRoot(Application("appName")) & Chr(39) & ";"
    Response.Write vbCrLf
    Response.Write "var __DATABASE_DOMINIUM__ = " & Chr(39) & objSvrCfg.DatabaseDominium(Application("appName")) & Chr(39) & ";"
    Response.Write vbCrLf
    Response.Write "var __DATABASE_ASPURLROOT__ = " & Chr(39) & objSvrCfg.DatabaseASPPagesURLRoot(Application("appName")) & Chr(39) & ";"
    Response.Write vbCrLf
    Response.Write "var __DATABASE_WSURLROOT__ = " & Chr(39) & objSvrCfg.PagesDominium(Application("appName")) & "/WSOverflyRDS" & Chr(39) & ";"
    Response.Write vbcrlf

    Response.Write "</script>"
    Response.Write vbCrLf    
    
    Set objSvrCfg = Nothing
%>

<html id="aniversariantesHtml" name="aniversariantesHtml">

<head>

<title>Aniversariantes</title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/aniversariantes/aniversariantes.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_browsers.js" & Chr(34) & "></script>" & vbCrLf    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    'Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    'Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    'Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    'Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf                
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_facilitiesnonforms.js" & Chr(34) & "></script>" & vbCrLf    
    
    'Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf                
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/aniversariantes/aniversariantes.js" & Chr(34) & "></script>" & vbCrLf
%>

<%
Response.Write "<script ID=" & Chr(34) & "formVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbCrLf
Response.Write "</script>"
Response.Write vbCrLf
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--
    
//-->
</script>

<script ID="vbFunctions" LANGUAGE="vbscript">
<!--

Function DateIsInInterval( dateBase, dateToCheck, intInDays )
    DateIsInInterval = False
    
    Dim actualIntInDays
    
    actualIntInDays = daysBetween(dateBase, dateToCheck)
    
    If (intInDays >= 0) Then
        If ( actualIntInDays >= 0 ) Then    
            If (intInDays >= actualIntInDays) Then
                DateIsInInterval = True
            End If
        End If
    Else
        If ( actualIntInDays <= 0 ) Then
            If ( intInDays <= actualIntInDays ) Then
                DateIsInInterval = True
            End If
        End If    
    End If    
    
End Function

Function DateOfToday()
    DateOfToday = Date
End Function

Function chkTypeData( sType, vValue )
    If sType = "D" Then ' Date Format
       chkTypeData = IsDate(vValue)
    ElseIf sType = "N" Or sType = "M" Then ' Number Format
       chkTypeData = IsNumeric(vValue)
    Else
       chkTypeData = TRUE
    End If
End Function

Function formatCurrValue(nValue)
    formatCurrValue = FormatNumber(nValue,2)
End Function

Function daysBetween(date1, date2)
    daysBetween = DateDiff("d",date1,date2)
End Function

Function daysSUM(dDate, nDias)
    daysSUM = DateAdd("d",nDias, Ddate)
End Function

Function DateToStr(dDate, sDateFormat)
    Dim sDay,sMonth,sYear
    sDay = Day(dDate)
    sMonth = Month(dDate)
    sYear = Year(dDate)
    If (sDateFormat = "DD/MM/YYYY") Then
        DateToStr = sDay & "/" & sMonth & "/" & sYear
    Else
        DateToStr = sMonth & "/" & sDay & "/" & sYear
    End If    
End Function

Function dataExtenso( dData, sIdioma)
    Dim sDataExtenso
    Dim aMonths
    Dim nDay, nMonth, nYear

    If not IsDate(dData) Then
        dataExtenso = Null
        Exit Function
    End If
    
    sIdioma = UCase(sIdioma)
    nDay = Day(dData)
    nMonth = Month(dData) - 1
    nYear = Year(dData)
    
    If sIdioma = "I" Then
        aMonths = Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December")
        sDataExtenso = aMonths(nMonth) & " " & CStr(nDay) & ", " & CStr(nYear)
    Else
        aMonths = Array("Janeiro", "Fevereiro", "Mar�o", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro")
        sDataExtenso = CStr(nDay) & " de " & aMonths(nMonth) & " de " & CStr(nYear)
    End If    
        
    dataExtenso = sDataExtenso
End Function
//-->
</script>

<!-- //@@ Eventos de grid -->
<!-- fg -->
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=EnterCell>
<!--
 js_fg_EnterCell(fg);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterRowColChange>
<!--
	js_fg_AfterRowColChange(fg, arguments[0], arguments[1], arguments[2], arguments[3]);
    js_aniversariantes_AfterRowColChange(fg, arguments[0], arguments[1], arguments[2], arguments[3]);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeRowColChange>
<!--
 js_fg_BeforeRowColChange(fg, arguments[0], arguments[1], arguments[2], arguments[3]);
//-->
</SCRIPT>
<script LANGUAGE="javascript" FOR=fg EVENT=BeforeSort>
<!--
fg_aniversariantes_BeforeSort(fg, arguments[0]);
//-->
</script>
<script LANGUAGE="javascript" FOR=fg EVENT=AfterSort>
<!--
fg_aniversariantes_AfterSort(fg, arguments[0]);
//-->
</script>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=DblClick>
<!--
var glb_PassedOneInDblClick;
glb_PassedOneInDblClick = false;

if (glb_PassedOneInDblClick == true)
{
    glb_PassedOneInDblClick = false;
    return;
}
    
glb_PassedOneInDblClick = true;

js_fg_aniversariantes_DblClick(fg, fg.Row, fg.Col);
//-->
</SCRIPT>

</head>

<body id="aniversariantesBody" name="aniversariantesBody" LANGUAGE="javascript" onload="return window_onload()" onbeforeunload="return window_onbeforeunload()" onunload="return window_onunload()">

    <!-- Objeto OverflyGen -->
    <object CLASSID="clsid:CBD8996E-E238-4A48-AB72-7A7250F700C1" ID="overflyGen" HEIGHT="0" WIDTH="0"></object>

    <div id="divFG" name="divFG" class="divExtern">
        <object CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fg" name="fg" VIEWASTEXT>
        </object>
    </div>
    
</body>

</html>
