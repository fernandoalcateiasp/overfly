
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<%
    'Aniversariantes do dia
    
	Dim rsData
	Dim strSQL
		
    strSQL = "SELECT DATEPART(dd, dtNascimento) AS Dia, Fantasia AS Aniversariante, " & _
                	"(SELECT TOP 1 URL " & _ 
                		"FROM Pessoas_URLs URLs WITH (NOLOCK) " & _ 
                		"WHERE URLs.PessoaID=Pessoas.PessoaID AND URLs.TipoURLID=124 " & _
                		"ORDER BY Ordem) AS EMail, dbo.fn_Pessoa_Observacao(Pessoas.PessoaID) AS Empresa " & _
                "FROM Pessoas WITH (NOLOCK) " & _
                "WHERE (Pessoas.EstadoID = 2 AND Pessoas.TipoPessoaID=51 AND (Pessoas.ClassificacaoID=57) AND " & _ 
                    "dbo.fn_Aniversario(Pessoas.dtNascimento, getdate()) = 1) " & _
                "ORDER BY DATEPART(mm, dtNascimento), DATEPART(dd, dtNascimento), Fantasia "

    Set rsData = Server.CreateObject("ADODB.Recordset")
    rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    ' send the new data back to the client
    rsData.Save Response, adPersistXML
      
    rsData.Close
    
    Set rsData = Nothing
%>
