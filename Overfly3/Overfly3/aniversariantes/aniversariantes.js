/********************************************************************
aniversariantes.js

Library javascript para o aniversariantes.asp
********************************************************************/
// VARIAVEIS GLOBAIS ************************************************
var dsoGen01 = new CDatatransport("dsoGen01");
// Variavel para exibir ou nao msg no beforeunload
var beforeUnloadMsg = true;
// Controla estado de travamento do browser filho
var __bInterfaceLocked = false;

var glb_aniversariantesTimer = null;

var glb_LASTLINESELID = '';

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
RESUMO DAS FUNCOES

window_onload()
window_onbeforeunload()
window_onunload()
sendJsCarrierFromForm(idElement, param1, param2, especChildBrowserName)
wndJSProc(idElement, msg, param1, param2)

setupPage()
adjustToobar()
adjustDivChamadas()
adjustDivCallbacks()
adjustDivCommonControls()

btnChamadasCBClick(ctrl)
modeChamadas()
modeCallbacks()
interfaceByGrid()
commonBtnsState()

btnDisponivelClick(ctrl)

btnPessoaClick(ctrl)
btnListaPrecosClick(ctrl)
listaPrecos_DSC()

rdsPadraoOnKeyDown(ctrl)
rdsPadraoOnMouseDown(ctrl)
rdsPadraoOnClick(ctrl)

btnDiscarOnClick(ctrl)
btnLigarOnClick(ctrl)

putHeaderLineInGrid(grid)

getCallbackData()
getCallbackData_DSC()
go_getCallbackData_DSC()

logInOnCentral()

js_fg_aniversariantes_DblClick( grid, Row, Col)
js_aniversariantes_AfterRowColChange
fg_aniversariantes_BeforeSort(grid, col)
fg_aniversariantes_AfterSort(grid, col)

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
	// Ze em 17/03/08
	dealWithObjects_Load();
	dealWithGrid_Load();
	// Fim de Ze em 17/03/08

	// Translada interface pelo dicionario do sistema
	translateInterface(window, null);
	
	window.document.title = translateTerm(window.document.title, null);

    var elem;
    
    // variavel do js_gridEx.js
    glb_HasGridButIsNotFormPage = true;
    
    // ajusta parametros fixos dos dsos da pagina
    dsoFixedParams();
    
    // coloca classe nos grids
    elem = window.document.getElementById('fg');
    
    if ( elem != null )
        elem.className = 'fldGeneral';
    
    elem = window.document.getElementById('fg');
    
    if ( elem != null )
        elem.className = 'fldGeneral';
    
    elem = document.getElementById('aniversariantesBody');
    with (elem)
    {
        style.width = '100%';
    }    
    
    setupPage();
    
    // ajusta o body do html
    elem = document.getElementById('aniversariantesBody');
    with (elem)
    {
        style.width = '100%';
        style.backgroundColor = 'white';
        scroll = 'no';
        style.visibility = 'visible';
    }
    
    // Forca scroll para o topo da pagina
    elem.scrollIntoView(true);
    
    // forca preenchimento do array de controle de travamento dos
    // elementos da interface
    lockControls(true);
    lockControls(false);

    getData();
}    

/********************************************************************
Confirma se deve ou nao fechar o browser
********************************************************************/
function window_onbeforeunload()
{
    if ( beforeUnloadMsg )
    {
        //event.returnValue = SYS_NAME;
    }
}

/********************************************************************
Diversos cleanup do sistema
********************************************************************/
function window_onunload()
{
	dealWithObjects_Unload();
	
    // o if abaixo deve estar aqui obrigatoriamente
    try
    {
        if ( window.opener && !window.opener.closed )
        {
           removeChildBrowserFromControl(window.name);
           
           // no caso de interrupcao do carregamento do browser filho
           // destrava a interface do browser mae
           // implantacao em 07/08/2001 - remover se comportamento
           // estranho do sistema
           sendJSMessage('aniversariantesHtml', JS_CHILDBROWSERLOADED, null, null);
           
           // fechou a janela de aniversariantes
           sendJSMessage(getHtmlId(), JS_NOMFORMCLOSE, 2, null);
        }   
    }
    catch(e)   
    {
        ;
    }
}

/********************************************************************
Funcao de comunicacao do carrier:
Chamada do arquivo js_sysbase.js.
Veio da funcao sendJSCarrier definida no arquivo js_sysbase.js e chamada
em qualquer arquivo de form.

Localizacao:
aniversariantes.asp

Chama:
Funcao sendJsCarrierFromChildBrowser(window.top.name,
                                     idElement, param1, param2)

do arquivo overfly.asp do browser mae.

Parametros:
idElement             - Obrigatorio. Id do html que manda o carrier.
param1                - De uso do programador. Qualquer objeto ou null.
param2                - De uso do programador. Qualquer objeto ou null.
especChildBrowserName - De uso do programador. Nome de um browser filho
                        para receber o carrier. Se null o carrier sera
                        propagado para todos os browsers.

Retorno     - null se nao tem browser filho.
********************************************************************/
function sendJsCarrierFromForm(idElement, param1, param2, especChildBrowserName)
{
    var wndTop = window.opener;

	if (wndTop == null)
		return null;

    // Chamada do carrier. Chama funcao do browser mae no overfly.asp
    wndTop.sendJsCarrierFromChildBrowser(window.top.name, idElement,
                                         param1, param2,
                                         especChildBrowserName);
}

/********************************************************************
Loop de mensagens
********************************************************************/
function wndJSProc(idElement, msg, param1, param2)
{
    switch (msg)
    {
        // Msg do carrier
        case JS_CARRIERCAMING :
        {
            return null;
        }    
        break;
            
        case JS_CARRIERRET :
        {
            return null;
        }
        break;
        
        case JS_MAINBROWSERCLOSING :
        {
            overflyGen.ForceDlgModalClose();
            beforeUnloadMsg = false;
            window.close();
        }
        break;
        
        // retorna ou altera e retorna o status da flag de interface
        // interface travada do browser filho
        // param1 == false -> so retorna
        case JS_CHILDBROWSERLOCKED:
        {
            if ( param1 == false )
                return __bInterfaceLocked;
            else if ( param1 == true )    
            {
                __bInterfaceLocked = ! __bInterfaceLocked;
                return __bInterfaceLocked;
            }    
        }
        break;
                     
        default:
            return null;
    }
}

/********************************************************************
Ajusta interface da pagina
********************************************************************/
function setupPage()
{
    var elem;
    
    // O div do grid de chamadas
    elem = divFG;
    with ( elem.style )
    {
        left = 0;
        top = 0;
        width = aniversariantesBody.offsetWidth - 4;
        backgroundColor = 'transparent';
        visibility = 'hidden';
        height = Math.abs(aniversariantesBody.offsetHeight - parseInt(elem.currentStyle.top, 10)) - 2;
    }
    elem.scroll = 'no';
    
    // o grid dos aniversariantes
    elem = fg;
    with ( elem.style )
    {
        left = ELEM_GAP;
        top = ELEM_GAP;
        width = parseInt(divFG.currentStyle.width) - (2 * ELEM_GAP);
        height = parseInt(divFG.currentStyle.height) - (2 * ELEM_GAP);
        visibility = 'hidden';
    }
    
    putHeaderLineInGrid(elem);
}

/********************************************************************
Configura o header do grid
********************************************************************/
function putHeaderLineInGrid(grid)
{
    startGridInterface(grid, 1, 1);
    
    grid.Cols = 1;
    grid.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;
 
    // zera a listagem
    grid.Rows = 1;
    
    // preenche o cabecalho do grid
    headerGrid(grid,['DDI', 
                   'DDD', 
                   'Telefone', 
                   'ID', 
                   'Fantasia', 
                   'Classificação', 
                   'Cidade', 
                   'UF',
                   'Call Status', 
                   'Tempo', 
                   'Contato',
                   'LigacaoID'],[0, 11]);
    
    grid.Redraw = 0;

    alignColsInGrid(grid,[]);
    
    grid.ExplorerBar = 5;
    grid.AutoSizeMode = 0;
    grid.AutoSize(0,fg.Cols-1);
    grid.Redraw = 2;    
}

/********************************************************************
Requisita dados ao servidor
********************************************************************/
function getData()
{
    lockControls(true);
    
    dsoGen01.URL = SYS_ASPURLROOT + '/aniversariantes/serversideEx/aniversariantesdata.aspx';
    dsoGen01.ondatasetcomplete = getData_DSC;
    dsoGen01.refresh();
}

/********************************************************************
Retorno do servidor da requisicao de dados de callback
********************************************************************/
function getData_DSC()
{
    glb_aniversariantesTimer = window.setInterval('go_getData_DSC()', 10, 'JavaScript');
}
    
/********************************************************************
Execucao por timer
Retorno do servidor da requisicao de dados de callback
********************************************************************/
function go_getData_DSC()
{    
    if ( glb_aniversariantesTimer != null )
    {
        window.clearInterval(glb_aniversariantesTimer);
        glb_aniversariantesTimer = null;
    }
    var dso = dsoGen01;
    
    var dTFormat = '';
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';

    // zera a listagem
    fg.Rows = 1;
    
    fg.Editable = false;
    fg.Redraw = 0;

    headerGrid(fg,['Dia',
                   'Aniversariante', 
                   'EMail',
                   'Empresa'],[2]);

    fillGridMask(fg, dso, ['Dia*', 
                           'Aniversariante*', 
                           'EMail',
                           'Empresa*'],
                          ['99', '', '', ''],
                          ['', '', '', '']);

    alignColsInGrid(fg,[0]);

    fg.Editable = false;
    fg.ExplorerBar = 5;
    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);
    fg.Redraw = 2;    
        
    // destrava interface
    lockControls(false);
    
    // coloca a primeira linha do grid como corrente
    if ( fg.Rows > 1 )
        fg.Row = 1;

    // coloca o grid visivel (pela primeira vez)
    if ( divFG.style.visibility != 'inherit' )
        divFG.style.visibility = 'inherit';    
    
    if ( fg.style.visibility != 'inherit' )
        fg.style.visibility = 'inherit';    
}

// EVENTOS DE GRID PARTICULARES DESTA PAGINA ========================

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_aniversariantes_AfterRowColChange (fg, OldRow, OldCol, NewRow, NewCol)
{
    var firstLine = 0;
    
    if ( glb_totalCols__ != false )
        firstLine = 1;
        
    if (NewRow > firstLine)
    {
        fg.Row = NewRow;
    }
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_aniversariantes_DblClick( grid, Row, Col)
{
    // alert(Row + ' ' + Col);
    
    if (fg.TextMatrix(Row, Col) != '')
        window.open('mailto:' + fg.TextMatrix(Row, 2) + '?subject=FELIZ ANIVERSÁRIO');
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function fg_aniversariantes_BeforeSort(grid, col)
{
    if (grid.Row > 0)
        glb_LASTLINESELID = grid.TextMatrix(grid.Row, 0);
    else
        glb_LASTLINESELID = '';
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function fg_aniversariantes_AfterSort(grid, col)
{
    var i;
    
    if ( (glb_LASTLINESELID != '') && (grid.Rows > 1) )
    {
        for (i=1; i<grid.Rows; i++)
        {
            if (grid.TextMatrix(i, 0) == glb_LASTLINESELID)
            {
                grid.TopRow = i;
                grid.Row = i;        
                break;
            }    
        }
    }
}
// FINAL DE EVENTOS DE GRID PARTICULARES DESTA PAGINA ===============