/********************************************************************
overfly.js
Library javascript de funcoes basicas do overfly.asp (default.asp)
********************************************************************/

// CONSTANTES *******************************************************
var __intID = null;
var __btnClicked;
var __formName;

// ID do usuario logado
var glb_CURRUSERID = 0;

// Username do usuario logado
var glb_USERNAME = '';

// Username do usuario logado
var glb_USERFANTASIA = '';

// E-mail do usuario logado
var glb_CURRUSEREMAIL = '';

// Impressora de codigo de barras do usuario logado
var glb_PRINTERBARCODE = null;

// Quantidade de notificacoes
var glb_NOTIFICATIONQTY = 0;
// Existe Notifica��o Critica
var glb_NOTIFICATIONCRITICAL = 0;

// Controla mensagem que confirma fechar a pagina
var beforeUnloadMsg = false;

// Controla se a janela de telefonia esta carregada
var glb_TelIsLoaded = false;

var glb_CALLBACKTOTAL = 0;

// var dsoNotificacoes = new CDatatransport('dsoNotificacoes');
var dsoNotificacoes = null;
var glb_TimerNotificacao = null;
var glb_QuantNotificacoesNaoLidas = 0;
var glb_QuantNotificacoesLidas = 0;
var glb_QuantNotificacoes = 0;
var glb_UserID = 0; //getCurrUserID();

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    /*
    var locator = new ActiveXObject("WbemScripting.SWbemLocator");
    var service = locator.ConnectServer(".");

    // Get the info
    var properties = service.ExecQuery("SELECT * FROM Win32_NetworkAdapterConfiguration");
    var e = new Enumerator(properties);
    var sNet = '';
    // Output info
    for (; !e.atEnd() ; e.moveNext()) {
        var p = e.item();
        sNet += p.Caption + ' : ' + p.MACAddress + '\r\n';
    }
    alert(sNet);
    */

    var i, elem, coll;
    var dimArray = new Array(0, 0, 0, 0);
    
	// Reordena os dicionarios
	for (i = 0; i <__arrDics.length; i++)
	{
		_asort(__arrDics[i][1], 0);
	}
	
    // O nome desta janela
    window.name = 'OVERFLYMAIN';
    
    // O titulo desta janela
    window.document.title = SYS_NAME;
    
    // todos os iframes
    coll = document.all.tags('IFRAME');
    
    // posi��o e dimens�es dos frames
    for (i=0;i<coll.length; i++)
    {
        coll.item(i).style.visibility = 'hidden';
        coll.item(i).scrolling = 'no';
        coll.item(i).tabIndex = -1;
        moveFrameInHtmlTop(coll.item(i).id, dimArray);
        // originally no backgroundColor
        coll.item(i).style.backgroundColor = 'white';
    }
    
    // ajusta o body do html
    with (overflyBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }    

    // carrega html de partida do sistema: checkusersystem.htm
    var theFrame = 'frameGen01';
    var theStartPage = SYS_PAGESURLROOT + '/introduction/checkusersystem.asp';

    loadPage(null, new Array(window.top.name.toString(), theFrame, theStartPage), null);
}     

/********************************************************************
Confirma se deve ou nao fechar o browser
********************************************************************/
function window_onbeforeunload()
{
    var eventText = '';
	var strPars = new String();
	var retVal = false;

    if ( beforeUnloadMsg )
    {
		// Modal do motivo para deslogar
		strPars = '?nUserID=' + escape(sendJSMessage(getHtmlId(), JS_WIDEMSG, '__CURRUSERID', null));
		strPars += '&nCallbacks=' + escape(glb_CALLBACKTOTAL);
		    
		retVal = window.showModalDialog( (SYS_PAGESURLROOT + '/login/userlogout.asp' + strPars), '', 'dialogWidth:266px;dialogHeight:130px;status:no;help:no' );    
		            
		if ( !retVal)
		{
		    eventText = SYS_NAME;
		    event.returnValue = eventText;
		}

/*********    
        if ( glb_CALLBACKTOTAL == 1 )
            eventText = '\n\n\n' + SYS_NAME + '\n' + 'Existe ' + glb_CALLBACKTOTAL.toString() + ' liga��o em callback!!!' + '\n\n\n';            
        else if ( glb_CALLBACKTOTAL > 1 )
            eventText = '\n\n\n' + SYS_NAME + '\n' + 'Existem ' + glb_CALLBACKTOTAL.toString() + ' liga��es em callback!!!' + '\n\n\n';
        else
            eventText = SYS_NAME;
            
        event.returnValue = eventText;    
*********/        
    }
}

/********************************************************************
Fecha todos os browsers que porventura estejam abertos.
********************************************************************/
function window_onunload()
{
    sendJSMessage('overflyHtml', JS_MAINBROWSERCLOSING, null, null);
    
    if (beforeUnloadMsg) 
    {
        // desloga usuario
        beforeUnloadMsg = false;
        window.location.replace(SYS_PAGESURLROOT + '/login/serverside/unlog01.aspx');
    }
}

/********************************************************************
Funcao de comunicacao do carrier:
Chamada do arquivo js_sysbase.js.
Veio da funcao sendJsCarrierFromForm definida
no arquivo childmain.asp, que veio da funcao sendJSCarrier definida
no arquivo js_sysbase.js e chamada em qualquer arquivo de form.

Localizacao:
overfly.js

Chama:
A funcao sendJSCarrierFromMotherToChild_Browser do arquivo js_sysbase.js
que propaga para todos os browsers filhos, menos o browser que chamou.

Parametros:
childBrowserName      - nome do browser filho que chamou.
idElement             - Obrigatorio. Id do html que manda o carrier.
param1                - De uso do programador. Qualquer objeto ou null.
param2                - De uso do programador. Qualquer objeto ou null.
especChildBrowserName - De uso do programador. Nome de um browser filho
                        para receber o carrier. Se null o carrier sera
                        propagado para todos os browsers.

Retorno
nao interessa
********************************************************************/
function sendJsCarrierFromChildBrowser(childBrowserName, idElement, param1, param2, especChildBrowserName)
{
    sendJSCarrierFromMotherToChild_Browser(childBrowserName, idElement,
                                           param1, param2,
                                           especChildBrowserName);
}

/********************************************************************
Faz uma pesquisa binaria em um array.

Parametros:
aArray     Array a usado como fonte de pesquisa
strToSeek  String a ser pesquisada
nArrayDimension  - (opcional), dimensao a ser usada na pesquisa caso
                   o array seja multidimensional

Retorno:   Indice do elemento encontrado no array
           -1 caso o valor nao foi encontrado
********************************************************************/
function _aseek(aArray, strToSeek, nArrayDimension)
{
	var nInicio = 0;
	var nFinal = aArray.length - 1;
	var nMetade = 0;
	var vMetadeValue;
	var retVal = -1;
	
	if (nArrayDimension != null)
	{
	    if (aArray[0].lenght > nArrayDimension)
	        return retVal;
	}
	
	if (trimStr(strToSeek) == '')
	    return retVal;

	while ( nInicio <= nFinal)
	{
		nMetade = Math.ceil((nInicio + nFinal)/2);

        // Ternario em funcao do Array ser unidimensional ou multidimensional
		vMetadeValue = (nArrayDimension == null ? aArray[nMetade] : aArray[nMetade][nArrayDimension]);

		if (strToSeek < vMetadeValue)
			nFinal = nMetade - 1;
		else
			if (strToSeek > vMetadeValue)
				nInicio = nMetade + 1;
			else
				nInicio = nFinal + 1;
	}

	if (strToSeek == (nArrayDimension == null ? aArray[nMetade] : aArray[nMetade][nArrayDimension]))
		retVal = nMetade;

    return retVal;
}

/********************************************************************
Ordena um array de elementos string, em ordem crescente

Parametros:
arrayToSort      - Referencia do array a ser ordenado
nArrayDimension  - (opcional), dimensao a ser usada na ordenacao caso
                   o array seja multidimensional

Retorno:      null
********************************************************************/
function _asort(arrayToSort, nArrayDimension)
{
    var i, j, k, vTemp;
    var nFim = arrayToSort.length;
    
	if (nArrayDimension != null)
	{
	    if (arrayToSort[0].length > nArrayDimension)
	        return null;
	}
	
	nArrayDimension = (nArrayDimension == null ? 0 : nArrayDimension);
    
    for (j=nFim; j>=1; j--)
    {
        for (i=0; i<=j-2; i++)
        {
            if (arrayToSort[i][nArrayDimension] > arrayToSort[i+1][nArrayDimension])
            {
                // Array unidimensional
                if (nArrayDimension == null)
                {
                    vTemp = arrayToSort[i];
                    arrayToSort[i] = arrayToSort[i+1];
                    arrayToSort[i+1] = vTemp;
                }
                // Array multidimensional
                else
                {
                    for (k=0; k<arrayToSort[i].length; k++)
                    {
                        vTemp = arrayToSort[i][k];
                        arrayToSort[i][k] = arrayToSort[i+1][k];
                        arrayToSort[i+1][k] = vTemp;
                    }
                }
            }
        }
    }
    
    return null;
}

/********************************************************************
Loop de mensagens
********************************************************************/
function wndJSProc(idElement, msg, param1, param2)
{
    switch (msg)
    {
        case JS_PAGELOAD :
        {
            if ( param1[0].toUpperCase() == window.top.name.toUpperCase())
            {
                loadPage(idElement, param1, param2);
                return 0;
            }
        }
        break;
        
        case JS_USERLOAD :
        {
            beforeUnloadMsg = true;
            startUserInterface(idElement, param1, param2);
            return 0;
        }
        break;
        
        case JS_FORMOPEN :
        {
            // param1[0] e verificado para saber se o form
            // foi requisitado a partir do browser mae
            // param1[4] e verificado para saber se abre browser filho
            
            if ( param1[0].toUpperCase() == window.top.name.toUpperCase() && param1[4] == true)
            {
                startChildBrowserForForm(idElement, param1, param2);
                return 0;    
            }
        }
        break;
    
        // abre browsers filhos para forms nao convencionais
        case JS_NOMFORMOPEN:
        {
            // param1 == 1 - identifica que e telefonia
            // param1 == 2 - identifica que e aniversariantes
            // param1 == 4/5 - identifica que e notificacoes
            // 4/5 apenas indica os dados a trazer 4 carregamento inicial/ 5 recarregamento mudando os dados

            if (param1 == 1) {
                if (!glb_TelIsLoaded) {
                    openChildBrowser(null, param1);
                    glb_TelIsLoaded = true;
                }
                else {
                    // a janela de telefonia esta aberta, coloca o foco nela
                    sendJSMessage(getHtmlId(), JS_WIDEMSG, JS_FOCUSFORM, null);
                }
            }
            else if (param1 == 2) {
                openChildBrowser(null, param1);
            }
            else if ((param1 == 4) || (param1 == 5)) {
                openChildBrowser(null, param1);
            }
            
            return 0;
        }
        break;
        
        // usuario fechou browsers filhos para forms nao convencionais
        case JS_NOMFORMCLOSE :
        {
            // param1 == 1 - identifica que e telefonia
            // param1 == 2 - identifica que e aniversariantes ou notificacoes 
        
            if ( (param1 == 1) && (param2 == null) )
                glb_TelIsLoaded = false;
            else if ( (param1 == 1) && (param2 != null) )   
                removeChildBrowserFromControl(param2);
           
            return 0;
        }
        break;
        
        case JS_NEWPASSWORD :
        {
            // janela de novo password necessita ser ativada
            if ( (param1 == 'SHOW_WIN') && (param2 == null) )
            {
                __intID = window.setInterval('newPassord()', 50, 'JavaScript');            
                return 0;
            }
        }
        break;
                
        case JS_WIDEMSG :
        {
            if (param1 == '__CURRUSERID')
                return glb_CURRUSERID;
            else if (param1 == '__CURRUSEREMAIL')
                return glb_CURRUSEREMAIL;
            else if (param1 == '__CBPENDENTES')
                glb_CALLBACKTOTAL = param2;    
            else if (param1 == '__PRINTERBARCODE')
                return glb_PRINTERBARCODE;
            else if (param1 == 'LABELBARCODE_GAP')
            {
                if ( param2 == null )
                    return LABELBARCODE_GAP;
                else
                {
                    LABELBARCODE_GAP = param2;
                    return true;
                }    
            }
        }
            
        case JS_MSGRESERVED :
        {
            if (param1 == 'PAGE_DELEGACOES')
            {
				// carrega, refresca grid e mostra pagina de direitos ou
				// se ja carregada, refresca grid e mostra pagina de direitos 
				if ( param2 == 'LOAD' )
				{
					__intID = window.setInterval('loadAndOrRefreshAndShowDelDirs()', 50, 'JavaScript');
					return true;
				}
				// pagina de direitos carregou
				else if ( param2 == 'LOADED' )
				{
					return true;
				}
				// mostrar pagina de direitos e esconder de e-mails
				else if ( param2 == 'SHOW' )
				{
					// todos os iframes
					var coll = document.all.tags('IFRAME');
					var i;
					    
					// posi��o e dimens�es dos frames
					for (i=(coll.length - 1);i>0; i--)
					{
						if ( coll.item(i).id == 'frameGen01')
							coll.item(i).style.visibility = 'hidden';
						if ( coll.item(i).id == 'frameGen03')	
						{
							coll.item(i).style.visibility = 'visible';
						}	
					}
										
					return true;
				}
				// mostrar pagina de e-mails e esconder de direitos
				else if ( param2 == 'HIDE' )
				{
					// todos os iframes
					var coll = document.all.tags('IFRAME');
					var i;
					    
					// posi��o e dimens�es dos frames
					for (i=0;i<coll.length; i++)
					{
						if ( coll.item(i).id == 'frameGen01')
						{
							coll.item(i).style.visibility = 'visible';
							getPageFrameInHtmlTop('frameGen01').lockControls(false);
						}
						if ( coll.item(i).id == 'frameGen03')	
							coll.item(i).style.visibility = 'hidden';
					}
										
					return true;
				}
            }
			
			/*if ( param1 == 'PAGE_VALE' )
            {
				// carrega, e mostra pagina de vale ou
				// se ja carregada, mostra pagina de vale
				if ( param2 == 'LOAD' )
				{
					__intID = window.setInterval('loadAndOrRefreshAndShowVale()', 50, 'JavaScript');
					return true;
				}
				// pagina de vale carregou
				else if ( param2 == 'LOADED' )
				{
					return true;
				}
				// mostrar pagina de vale e esconder de e-mails
				else if ( param2 == 'SHOW' )
				{
					// todos os iframes
					var coll = document.all.tags('IFRAME');
					var i;
					    
					// posi��o e dimens�es dos frames
					for (i=(coll.length - 1);i>0; i--)
					{
						if ( coll.item(i).id == 'frameGen01')
							coll.item(i).style.visibility = 'hidden';
						if ( coll.item(i).id == 'frameGen04')	
						{
							coll.item(i).style.visibility = 'visible';
						}	
					}
										
					return true;
				}
				// mostrar pagina de e-mails e esconder de vale
				else if ( param2 == 'HIDE' )
				{
					// todos os iframes
					var coll = document.all.tags('IFRAME');
					var i;
					    
					// posi��o e dimens�es dos frames
					for (i=0;i<coll.length; i++)
					{
						if ( coll.item(i).id == 'frameGen01')
						{
							coll.item(i).style.visibility = 'visible';
							getPageFrameInHtmlTop('frameGen01').lockControls(false);
						}
						if ( coll.item(i).id == 'frameGen04')	
							coll.item(i).style.visibility = 'hidden';
					}
										
					return true;
				}
			}*/
            
            // carrega p�gina de feriados
			if (param1 == 'PAGE_FERIADOS') {
			    // carrega, e mostra pagina de vale ou
			    // se ja carregada, mostra pagina de vale
			    if (param2 == 'LOAD') {
			        __intID = window.setInterval('loadAndOrRefreshAndShowFeriados()', 50, 'JavaScript');
			        return true;
			    }
			    // pagina de vale carregou
			    else if (param2 == 'LOADED') {
			        return true;
			    }
			    // mostrar pagina de vale e esconder de e-mails
			    else if (param2 == 'SHOW') {
			        // todos os iframes
			        var coll = document.all.tags('IFRAME');
			        var i;

			        // posi��o e dimens�es dos frames
			        for (i = (coll.length - 1); i > 0; i--) {
			            if (coll.item(i).id == 'frameGen01')
			                coll.item(i).style.visibility = 'hidden';
			            if (coll.item(i).id == 'frameGen04') {
			                coll.item(i).style.visibility = 'visible';
			            }
			        }

			        return true;
			    }
			    // mostrar pagina de e-mails e esconder de vale
			    else if (param2 == 'HIDE') {
			        // todos os iframes
			        var coll = document.all.tags('IFRAME');
			        var i;

			        // posi��o e dimens�es dos frames
			        for (i = 0; i < coll.length; i++) {
			            if (coll.item(i).id == 'frameGen01') {
			                coll.item(i).style.visibility = 'visible';
			                getPageFrameInHtmlTop('frameGen01').lockControls(false);
			            }
			            if (coll.item(i).id == 'frameGen04')
			                coll.item(i).style.visibility = 'hidden';
			        }

			        return true;
			    }
			}

			// carrega p�gina de notifica��o
			if (param1 == 'PAGE_NOTIFICACOES') {
			    // carrega, e mostra pagina de vale ou
			    // se ja carregada, mostra pagina de vale
			    if (param2 == 'LOAD') {
			        __intID = window.setInterval('loadAndOrRefreshAndShowNotificacao()', 50, 'JavaScript');
			        return true;
			    }
			    // pagina de vale carregou
			    else if (param2 == 'LOADED') {
			        return true;
			    }
			    // mostrar pagina de vale e esconder de e-mails
			    else if (param2 == 'SHOW') {
			        // todos os iframes
			        var coll = document.all.tags('IFRAME');
			        var i;

			        // posi��o e dimens�es dos frames
			        for (i = (coll.length - 1); i > 0; i--) {
			            if (coll.item(i).id == 'frameGen01')
			                coll.item(i).style.visibility = 'hidden';
			            if (coll.item(i).id == 'frameGen04') {
			                coll.item(i).style.visibility = 'visible';
			            }
			        }

			        return true;
			    }
			    // mostrar pagina de e-mails e esconder de vale
			    else if (param2 == 'HIDE') {
			        // todos os iframes
			        var coll = document.all.tags('IFRAME');
			        var i;

			        // posi��o e dimens�es dos frames
			        for (i = 0; i < coll.length; i++) {
			            if (coll.item(i).id == 'frameGen01') {
			                coll.item(i).style.visibility = 'visible';
			                getPageFrameInHtmlTop('frameGen01').lockControls(false);
			            }
			            if (coll.item(i).id == 'frameGen04')
			                coll.item(i).style.visibility = 'hidden';
			        }

			        return true;
			    }
			}

            // carrega p�gina de notifica��o
			if (param1 == 'PAGE_VENDAS') {
			    // carrega, e mostra pagina de vale ou
			    // se ja carregada, mostra pagina de vale
			    if (param2 == 'LOAD') {
			        __intID = window.setInterval('loadAndOrRefreshAndShowVendas()', 50, 'JavaScript');
			        return true;
			    }
			        // pagina de vale carregou
			    else if (param2 == 'LOADED') {
			        return true;
			    }
			        // mostrar pagina de vale e esconder de e-mails
			    else if (param2 == 'SHOW') {
			        // todos os iframes
			        var coll = document.all.tags('IFRAME');
			        var i;

			        // posi��o e dimens�es dos frames
			        for (i = (coll.length - 1) ; i > 0; i--) {
			            if (coll.item(i).id == 'frameGen01')
			                coll.item(i).style.visibility = 'hidden';
			            if (coll.item(i).id == 'frameGen04') {
			                coll.item(i).style.visibility = 'visible';
			            }
			        }

			        return true;
			    }
			        // mostrar pagina de e-mails e esconder de vale
			    else if (param2 == 'HIDE') {
			        // todos os iframes
			        var coll = document.all.tags('IFRAME');
			        var i;

			        // posi��o e dimens�es dos frames
			        for (i = 0; i < coll.length; i++) {
			            if (coll.item(i).id == 'frameGen01') {
			                coll.item(i).style.visibility = 'visible';
			                getPageFrameInHtmlTop('frameGen01').lockControls(false);
			            }
			            if (coll.item(i).id == 'frameGen04')
			                coll.item(i).style.visibility = 'hidden';
			        }

			        return true;
			    }
			}

			// Lingua corrente s ser usada para translado com o dicionario 
			// de termos do sistema
			else if ( param1 == 'GETCURRLANGDIC')
			{
				if (_glb_currDicLang == null)
					return 0;
				else
					return _glb_currDicLang;
			}
			else if ( param1 == 'SETCURRLANGDIC')
			{
				if ( param2 == null )
					return false;
					
				_glb_currDicLang = param2;
				return true;
			}	
			else if ( param1 == 'GETTERMINDIC')
			{
				if ( (param2 == null) || (param2 == 0) )
					return '';
				
				var _i, nPos, termTrans, sRet;	
				var dicToUse = null;
				
				sRet = '';
				
				// dicionario a usar
				for (_i = 0; _i <__arrDics.length; _i++)
				{
					if ( __arrDics[_i][0] == param2[2])
					{
						dicToUse = __arrDics[_i][1];
						break;
					}
				}
				
				// obtendo o termo transladado
				nPos = _aseek(dicToUse, param2[0], 0);
				
				if ( nPos >=0 )
					sRet = dicToUse[nPos][param2[1]];
				
				return sRet;
			}	
			
        }
        break;    
            
        default:
            return null;
    }
}

/********************************************************************
Chama a pagina que carrega, refresca o grid e mostra a pagina de
delegar direitos ou,
se a pagina ja esta carregada
refresca o grid e mostra a pagina de
delegar direitos

Parametros:
nenhum

Retorno:
nenhum
********************************************************************/
function loadAndOrRefreshAndShowDelDirs()
{
	if ( __intID != null )
    {
        window.clearInterval(__intID);
        __intID = null;
    }
    
    var theFrame = 'frameGen03';
    var theStartPage = SYS_PAGESURLROOT + '/home/delegacoes/delegacoes.asp';
    
    // Carrega a pagina se nao esta carregada
    if ( getPageFrameInHtmlTop('frameGen03') == '' )
		loadPage(null, new Array(window.top.name.toString(), theFrame, theStartPage), null);
	// Mostra a pagina se esta carregada e repreenche o grid
	else
	{
		// todos os iframes
		var coll = document.all.tags('IFRAME');
		var i;
							    
		// posi��o e dimens�es dos frames
		for (i=(coll.length - 1);i>0; i--)
		{
			if ( coll.item(i).id == 'frameGen01')
				coll.item(i).style.visibility = 'hidden';
			if ( coll.item(i).id == 'frameGen03')	
			{
				coll.item(i).style.visibility = 'visible';
				getPageFrameInHtmlTop('frameGen03').lockControls(false);
				getPageFrameInHtmlTop('frameGen03').loadDataAndFillGrid();
				getPageFrameInHtmlTop('frameGen03').setDelegDirsBtnState('btnIncluir', false);
				getPageFrameInHtmlTop('frameGen03').setDelegDirsBtnState('chkAtivos', false);
			}
		}
	}	
}

/********************************************************************
Chama a pagina que carrega, e mostra a pagina de vale ou, se a pagina 
ja esta carregada, mostra a pagina de vale

Parametros:
nenhum

Retorno:
nenhum
********************************************************************/
/*function loadAndOrRefreshAndShowVale()
{
	if ( __intID != null )
    {
        window.clearInterval(__intID);
        __intID = null;
    }
    
    var aEmpresaData = sendJSMessage(getHtmlId(), JS_WIDEMSG, '__EMPRESADATA', null);
    var strPars = '?nEmpresaID=' + escape(aEmpresaData[0]) +
		'&nUsuarioID=' + escape(glb_CURRUSERID);

    var theFrame = 'frameGen04';
    var theStartPage = SYS_PAGESURLROOT + '/modrecursoshumanos/submodfopag/antecipacaosalarial/antecipacaosalarial.asp' + strPars;
    
    loadPage(null, new Array(window.top.name.toString(), theFrame, theStartPage), null);
    
    return null;
    
    // Carrega a pagina se nao esta carregada
    if ( getPageFrameInHtmlTop('frameGen04') == '' )
		loadPage(null, new Array(window.top.name.toString(), theFrame, theStartPage), null);
	// Mostra a pagina se esta carregada
	else
	{
		// todos os iframes
		var coll = document.all.tags('IFRAME');
		var i;
							    
		// posi��o e dimens�es dos frames
		for (i=(coll.length - 1);i>0; i--)
		{
			if ( coll.item(i).id == 'frameGen01')
				coll.item(i).style.visibility = 'hidden';
			if ( coll.item(i).id == 'frameGen04')	
			{
				coll.item(i).style.visibility = 'visible';
				getPageFrameInHtmlTop('frameGen04').lockControls(false);
				getPageFrameInHtmlTop('frameGen04').setFocus();
				//getPageFrameInHtmlTop('frameGen04').setDelegDirsBtnState('btnIncluir', false);
				//getPageFrameInHtmlTop('frameGen04').setDelegDirsBtnState('chkAtivos', false);
			}
		}
	}	
}*/

/********************************************************************
Chama a pagina que carrega, e mostra a pagina feriados ou, se a pagina 
ja esta carregada, mostra a pagina de feriados

Parametros:
nenhum

Retorno:
nenhum
********************************************************************/
function loadAndOrRefreshAndShowFeriados() {
    if (__intID != null) {
        window.clearInterval(__intID);
        __intID = null;
    }

    var theFrame = 'frameGen04';
    var theStartPage = SYS_PAGESURLROOT + '/home/feriados/feriados.asp';

    loadPage(null, new Array(window.top.name.toString(), theFrame, theStartPage), null);

    return null;

    // Carrega a pagina se nao esta carregada
    if (getPageFrameInHtmlTop('frameGen04') == '')
        loadPage(null, new Array(window.top.name.toString(), theFrame, theStartPage), null);
    // Mostra a pagina se esta carregada
    else {
        // todos os iframes
        var coll = document.all.tags('IFRAME');
        var i;

        // posi��o e dimens�es dos frames
        for (i = (coll.length - 1); i > 0; i--) {
            if (coll.item(i).id == 'frameGen01')
                coll.item(i).style.visibility = 'hidden';
            if (coll.item(i).id == 'frameGen04') {
                coll.item(i).style.visibility = 'visible';
                getPageFrameInHtmlTop('frameGen04').lockControls(false);
                getPageFrameInHtmlTop('frameGen04').setFocus();
                //getPageFrameInHtmlTop('frameGen04').setDelegDirsBtnState('btnIncluir', false);
                //getPageFrameInHtmlTop('frameGen04').setDelegDirsBtnState('chkAtivos', false);
            }
        }
    }
}

/********************************************************************
Chama a pagina que carrega, e mostra a pagina notifica��o ou, se a pagina 
ja esta carregada, mostra a pagina de notifica��o

Parametros:
nenhum

Retorno:
nenhum
********************************************************************/
function loadAndOrRefreshAndShowNotificacao() {
    if (__intID != null) {
        window.clearInterval(__intID);
        __intID = null;
    }

    var aEmpresaData = sendJSMessage(getHtmlId(), JS_WIDEMSG, '__EMPRESADATA', null);
    var strPars = '?nEmpresaID=' + escape(aEmpresaData[0]) +
		'&nUsuarioID=' + escape(glb_CURRUSERID);

    var theFrame = 'frameGen04';
    var theStartPage = SYS_PAGESURLROOT + '/home/notificacoes/notificacoes.asp' + strPars;

    loadPage(null, new Array(window.top.name.toString(), theFrame, theStartPage), null);

    return null;

    // Carrega a pagina se nao esta carregada
    if (getPageFrameInHtmlTop('frameGen04') == '')
        loadPage(null, new Array(window.top.name.toString(), theFrame, theStartPage), null);
    // Mostra a pagina se esta carregada
    else {
        // todos os iframes
        var coll = document.all.tags('IFRAME');
        var i;

        // posi��o e dimens�es dos frames
        for (i = (coll.length - 1); i > 0; i--) {
            if (coll.item(i).id == 'frameGen01')
                coll.item(i).style.visibility = 'hidden';
            if (coll.item(i).id == 'frameGen04') {
                coll.item(i).style.visibility = 'visible';
                getPageFrameInHtmlTop('frameGen04').lockControls(false);
                getPageFrameInHtmlTop('frameGen04').setFocus();
                //getPageFrameInHtmlTop('frameGen04').setDelegDirsBtnState('btnIncluir', false);
                //getPageFrameInHtmlTop('frameGen04').setDelegDirsBtnState('chkAtivos', false);
            }
        }
    }
}

/********************************************************************
Chama a pagina que pede nova senha ao usuario para trocar a senha atual

Parametros:
nenhum

Retorno:
nenhum
********************************************************************/
function newPassord()
{
    if ( __intID != null )
    {
        window.clearInterval(__intID);
        __intID = null;
    }
    
    // carrega a pagina
    var theFrame = 'frameGen02';
    var theStartPage = SYS_PAGESURLROOT + '/login/newpassword.asp';
    
    loadPage(null, new Array(window.top.name.toString(), theFrame, theStartPage), null);
}

/********************************************************************
Esta funcao � executada se o usuario que esta logando foi validado
no servidor.
Chama a p�gina que monta os direitos do usuario e carrega o seu
main menu.

Parametros:
idElement           id do elemento que chamou a funcao
param1              livre
param2              livre

Retorno:
nenhum
********************************************************************/
function startUserInterface(idElement, param1, param2)
{
    var theFrame = 'frameMainMenu';
    var theStartPage = SYS_PAGESURLROOT + '/login/serverside/startuser.asp';

    loadPage(null, new Array(window.top.name.toString(), theFrame, theStartPage), null);
}

/********************************************************************
Esta funcao abre um browser filho para carregar um form nele

Parametros:
idElement           id do elemento que chamou a funcao
param1              array: nome do window do browser mae,
                           nome do form,
                           URL do arquivo que inicia o carregamento do form,
                           frame a usar e
                           true (browser novo)/false (browser corrente) 
                           esta ultima informacao e irrelevante neste caso
                           e veio porque esta no param1
param2              livre

Retorno:
Nenhum

Mecanica:
    Desabilita toda a interface do browser mae.
    Starta carregamento de browser filho.
    Ao carregar totalmente o browser filho este inicia o carregamento
    do form.
    Ao terminar o carregamento todo do form
        Avisa browser mae que carregou.
        O browser mae habilita toda a sua interface.
********************************************************************/
function startChildBrowserForForm(idElement, param1, param2)
{
    // prepara o formName, arqStart e o frameStart para informar o browser filho
    var strPars = new Array();
    strPars = '?';
    strPars += 'formName=';
    strPars += escape(param1[1]);
    strPars += '&';
    strPars += 'arqStart=';
    strPars += escape(param1[2]);
    strPars += '&';
    strPars += 'frameStart=';
    strPars += escape(param1[3]);
        
    // inicia a abertura de um browser filho
    openChildBrowser(strPars);
}

/********************************************************************
Recebe evento de botao clicado na barra de botoes de acesso rapido

Parametros:
btnClicked  - referencia ao botao clicado
formName    - nome do form associado ao botao

Retorno:
nenhum
********************************************************************/
function emulEvent(btnClicked, formName)
{
    __btnClicked = btnClicked;
    __formName = formName;
    __intID = window.setInterval('fastButtonClicked()', 50, 'JavaScript');    
}

/********************************************************************
Funcao complementar a funcao emulEvent(btnClicked, formName)

Parametros:
nenhum

Retorno:
nenhum
********************************************************************/
function fastButtonClicked()
{
    if ( __intID != null )
    {
        window.clearInterval(__intID);
        __intID = null;
    }    
    sendJSMessage('overflyHtml', JS_FASTBUTTONCLICKED, __btnClicked, __formName);
}

/********************************************************************
Fun��es que retornam a quantidade de notifica��es para o usuario
********************************************************************/

function barLoaded()
{
    //Inicia o botao e liga o timer
    refreshNotificacoes();
    startTimerNotificacoes();
}

function startTimerNotificacoes()
{
    // glb_TimerNotificacao = window.setInterval('refreshNotificacoes()', (1000 * 15), 'JavaScript');
    // Alterado de 15 segundos para 1 minuto por solitacao de Eduardo Rodrigues - MF 16/03/2018
    // Alterado de 1 minuto para 15 segundos por solitacao de Eduardo Rodrigues - MF 12/04/2018
    glb_TimerNotificacao = window.setInterval('refreshNotificacoes()', (1000 * 15), 'JavaScript');
}

function stopTimerNotificacoes()
{
    if (glb_TimerNotificacao != null)
    {
        window.clearInterval(glb_TimerNotificacao);
        glb_TimerNotificacao = null;
    }
}

function sendCarrierNotificacoes() {
}

function refreshNotificacoes()
{
    stopTimerNotificacoes();
    var userID = sendJSMessage(getHtmlId(), JS_WIDEMSG, '__CURRUSERID', null);

    var sSQL = 'SELECT dbo.fn_Pessoa_Notificacoes(' + userID + ', 1, NULL) AS Notificacoes, dbo.fn_Pessoa_Notificacoes(' + userID + ', 1, 1) AS NotificacoesCriticas ';
        
    if (dsoNotificacoes == null)
        dsoNotificacoes = new CDatatransport('dsoNotificacoes');

    setConnection(dsoNotificacoes);
    dsoNotificacoes.SQL = sSQL;
    dsoNotificacoes.ondatasetcomplete = refreshNotificacoes_DSC;
    dsoNotificacoes.Refresh();
}

function refreshNotificacoes_DSC()
{
    glb_NOTIFICATIONQTY = dsoNotificacoes.recordset['Notificacoes'].value;
    glb_NOTIFICATIONCRITICAL = dsoNotificacoes.recordset['NotificacoesCriticas'].value;

    if (glb_NOTIFICATIONCRITICAL > 0)
    {
        sendJSMessage(getHtmlId(), JS_NOTIFICATIONCALL, null, null);  
    }

    sendJSMessage('overflyHtml', JS_NOTIFICATIONQTY, glb_NOTIFICATIONQTY, null);
    startTimerNotificacoes();
}

/********************************************************************
Chama a pagina que carrega, e mostra a pagina vendas ou, se a pagina 
ja esta carregada, mostra a pagina de vendas

Parametros:
nenhum

Retorno:
nenhum
********************************************************************/
function loadAndOrRefreshAndShowVendas()
{
    if (__intID != null) {
        window.clearInterval(__intID);
        __intID = null;
    }

    var aEmpresaData = sendJSMessage(getHtmlId(), JS_WIDEMSG, '__EMPRESADATA', null);
    var strPars = '?nEmpresaID=' + escape(aEmpresaData[0]) +
		'&nUsuarioID=' + escape(glb_CURRUSERID);

    var theFrame = 'frameGen04';
    var theStartPage = SYS_PAGESURLROOT + '/home/vendas/vendas.asp' + strPars;

    loadPage(null, new Array(window.top.name.toString(), theFrame, theStartPage), null);

    return null;

    // Carrega a pagina se nao esta carregada
    if (getPageFrameInHtmlTop('frameGen04') == '')
        loadPage(null, new Array(window.top.name.toString(), theFrame, theStartPage), null);
        // Mostra a pagina se esta carregada
    else {
        // todos os iframes
        var coll = document.all.tags('IFRAME');
        var i;

        // posi��o e dimens�es dos frames
        for (i = (coll.length - 1) ; i > 0; i--) {
            if (coll.item(i).id == 'frameGen01')
                coll.item(i).style.visibility = 'hidden';
            if (coll.item(i).id == 'frameGen04') {
                coll.item(i).style.visibility = 'visible';
                getPageFrameInHtmlTop('frameGen04').lockControls(false);
                getPageFrameInHtmlTop('frameGen04').setFocus();
                //getPageFrameInHtmlTop('frameGen04').setDelegDirsBtnState('btnIncluir', false);
                //getPageFrameInHtmlTop('frameGen04').setDelegDirsBtnState('chkAtivos', false);
            }
        }
    }
}