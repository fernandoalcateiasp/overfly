/********************************************************************
commonqualidade.js

Library javascript para funcoes comuns aos forms da qualidade
********************************************************************/

/********************************************************************
Abre a janela de texto de acordo com o registro selecionado.

Parametros:
nenhum

Retorno:
nenhum
********************************************************************/
function showTexto()
{
	var nTipoDocumentoID;
	var nCurrDocumentoID;
	var nRegistroID;
	var nRegistroID = 0;

	if (fg.Row < 1)
		return null;
	
	nCurrDocumentoID = getCellValueByColKey(fg, 'DocumentoID', fg.Row);
	nRegistroID = getCellValueByColKey(fg, 'RegistroID', fg.Row);

	if (nCurrDocumentoID == null)	
		nCurrDocumentoID = getCellValueByColKey(fg, 'DocumentoID*', fg.Row);

	if (nRegistroID == null)	
		nRegistroID = getCellValueByColKey(fg, 'RegistroID*', fg.Row);

	// RAD
	if (nCurrDocumentoID == 560001)
		nTipoDocumentoID = 5;
	// RET	
	else if (nCurrDocumentoID == 620004)
		nTipoDocumentoID = 7;
	// RRC	
	else if (nCurrDocumentoID == 723001)
		nTipoDocumentoID = 4;
	// PSC
	else if (nCurrDocumentoID == 821001)
		nTipoDocumentoID = 6;
	// RAI
	else if (nCurrDocumentoID == 822001)
		nTipoDocumentoID = 2;
	// RACP
	else if (nCurrDocumentoID == 852001)
		nTipoDocumentoID = 1;
	else
		return null;	

	window.top.openModalHTML(nRegistroID, 'Texto', 'S', nTipoDocumentoID);
}
