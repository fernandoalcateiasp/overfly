/********************************************************************
superior.js

Library javascript para o superior.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_BtnFromFramWork = null;

var glb_bAlteracao = false;

// Dados do registro corrente .SQL 
var dsoSup01 = new CDatatransport("dsoSup01");
var dsoCmbDynamic01 = new CDatatransport("dsoCmbDynamic01");
// Descricao do estado atual do registro .SQL 
var dsoStateMachine = new CDatatransport("dsoStateMachine");
// Dados dos combos estaticos .URL 
var dsoEstaticCmbs = new CDatatransport("dsoEstaticCmbs");
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

FUNCOES DA AUTOMACAO:
	window_onload()
	setupPage()
    putSpecialAttributesInControls()
    formFinishLoad()
    prgServerSup(btnClicked)
    finalOfSupCascade(btnClicked)
    prgInterfaceSup(btnClicked)
    optChangedInCmb(cmb)
    btnLupaClicked(btnClicked)
    btnBarClicked(controlBar, btnClicked)
    btnBarNotEspecClicked(controlBar, btnClicked)
    modalInformForm(idElement, param1, param2)
    supInitEditMode()

FUNCOES DO CARRIER:
    carrierArrived(idElement, idBrowser, param1, param2)
    carrierReturning(idElement, idBrowser, param1, param2)

FUNCOES DA MAQUINA DE ESTADO:
    stateMachOpened( currEstadoID )
    stateMachBtnOK( currEstadoID, newEstadoID )
    stateMachClosed( oldEstadoID, currEstadoID, bSavedID )
    
FUNCOES ESPECIFICAS DO FORM:    
********************************************************************/

/********************************************************************

// IMPLEMENTACAO DAS FUNCOES

//@@ todas as funcoes abaixo sao particulares de cada form
// conforme o form algumas funcoes devem ser removidas
// e outras serao acrescentadas
********************************************************************/

// FUNCOES DA AUTOMACAO *********************************************

/********************************************************************
Evento disparado ao final do carregamento da pagina.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function window_onload()
{
    //@@ Iniciado carregamento de form, primeira funcao controladora
    initInterfaceLoad(window.top.formName, 0X7);
    
    //@@ array bidimensional de combos ids e campo indice na tabela
    // identificador do combo. O array deve ser null se nao tem combos estaticos

    glb_aStaticCombos = ([['selIndicadorID', '1'],
					      ['selItemNormaID', '2'],
						  ['selFrequencia', '3']]);

    windowOnLoad_1stPart();

    //@@ Path dos arquivos inf e pesqlist
    glb_aFilePath = new Array(SYS_PAGESURLROOT + '/modqualidade/submodiso/riq/inferior.asp',
                              SYS_PAGESURLROOT + '/modqualidade/submodiso/riq/pesquisa.asp');

    //@@ Linka os divs com os correspondentes subforms
    // criando o atributo sfsGrupo nos divs.
    // Os infs nao tem div principal e todos os divs sao secundarios.
    // Parametros:
    // 1o par: id do div principal ou null se nao tem div principal
    // 2opar: array de divs ids dos divs secundarios
    // ou null se nao tem divs secundarios
    // 3o par: array de xxx_ID que identifica o grupo contido no div
    // ou null se o form nao tem div secundarios.
    // Esta funcao tageia o div principal e preenche array
    // dos divs secundarios do arquivo guardando na variavel
    // global do arquivo: glb_aDIVS
    linkDivsAndSubForms('divSup01_01', null, null);
    
    windowOnLoad_2ndPart();
    
    //@@ Guarda o nome do campo na tabela que contem o id do registro corrente no sup
    glb_sFldIDName = 'RIQID';
    
    //@@ Guarda o id do controle (combo) que contem os tipos de registros
    // se nao e aplicavel no form, fazer glb_sCtlTipoRegistroID = '';
    glb_sCtlTipoRegistroID = '';

    //@@ Guarda o nome do campo na tabela que contem o tipo do registro corrente no sup
    // se nao e aplicavel no form, fazer glb_sFldTipoRegistroName = ''
    glb_sFldTipoRegistroName = '';
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pela funcao windowOnLoad_2ndPart() do js_detailsup.js

Usar para ajustes graficos da interface.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function setupPage()
{
    //@@ Ajusta os divs e seus elementos
    adjustDivs('sup',[[1,'divSup01_01']]);

    //@@ *** Div principal superior divSup01_01 ***/
    adjustElementsInForm([['lblRegistroID','txtRegistroID',10,1],
						  ['lblEstadoID','txtEstadoID',2,1],
						  ['lblIndicadorID','selIndicadorID',40,1],
						  ['lblAbreviacao','txtAbreviacao',6,1],
						  ['lblItemNormaID','selItemNormaID',7,1],
						  ['lblObjetivo','txtObjetivo',60,2],
						  ['lblUnidade','txtUnidade',10,2],
						  ['lblFormula','txtFormula',90,3],
						  ['lblFrequencia','selFrequencia',12,4],
						  ['lbldtInicioCalculo','txtdtInicioCalculo',10,4],
						  ['lbldtMeta','txtdtMeta',10,4],
						  ['lblMeta','txtMeta',6,4],
						  ['lblResultadoMinimo','txtResultadoMinimo',6,4],
						  ['lblResultadoMaximo','txtResultadoMaximo',6,4]], null, null, true);
}

/********************************************************************
Funcao disparada pelo frame work.
Chamado pela funcao windowOnLoad_2ndPart() do js_superior.js

Coloca atributos em campos para serem manobrados graficamente
(esconde/mostra e habilita desabilita) pelo framework.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function putSpecialAttributesInControls()
{
    //@@
    
    // Tabela de atributos especiais:
    // 1. fnShowHide - seta funcao do programador que o framework executa
    //                 ao mostrar/esconder o controle.
    // 2. prog_SHHD  - (true) retira do frame work a operacao de mostrar/esconder
    //                 habiitar/desabilitar o controle e transfere para
    //                 o programador. False ou null devolve para o framework.
    
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pelo loop de mensagens wndJSProc(idElement, msg, param1, param2)
do js_detailsup.js

Form carregou e vai mostrar a interface.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function formFinishLoad()
{
    // seta botao de impressao
    // especBtnIsPrintBtn('sup', 1);

    // mostra dois botoes especificos desabilitados
    showBtnsEspecControlBar('sup', true, [1,1,1,1]);
    tipsBtnsEspecControlBar('sup', ['Documentos', 'Relat�rios', 'Procedimento', 'Texto']);
}

/********************************************************************
Funcao disparada pelo frame work.
Funcao disparada pelo frame work.
Chamada pelo loop de mensagens wndJSProc(idElement, msg, param1, param2)
do js_detailsup.js

Esta funcao faz a chamada de todas as operacoes particulares de servidor
do e preenchimentos de combos do sup, quando o usuario clica
um botao da barra do sup.
           
Parametros: 
ultimo botao clicado na barra de botoes superior

Retorno:
nenhum
********************************************************************/
function prgServerSup(btnClicked)
{
    // chama funcao do programador que inicia o carregamento
    // de dados dos combos dinamicos (selSujeitoID e selObjetoID)
    glb_BtnFromFramWork = btnClicked;
    
    if ( (btnClicked == 'SUPDET') || (btnClicked == 'SUPREFR') )
    {
        setCalcFields(false);
        adjustLabelsCombos(true);
    }    
	
	finalOfSupCascade(glb_BtnFromFramWork);
}

function setCalcFields(bErase)
{
	if (bErase)
	{
		txtAbreviacao.value = '';
	}
	else
	{
		txtAbreviacao.value = dsoSup01.recordset['Abreviacao'].value;
	}
}

function adjustLabelsCombos(bPaging)
{
	var nNormaID = 0;
	
    if (bPaging)
    {
        setLabelOfControl(lblIndicadorID, dsoSup01.recordset['IndicadorID'].value);
        setLabelOfControl(lblItemNormaID, dsoSup01.recordset['ItemNormaID'].value);
        nNormaID = dsoSup01.recordset['ItemNormaID'].value;
    }
    else
    {
        setLabelOfControl(lblIndicadorID, selIndicadorID.value);
        setLabelOfControl(lblItemNormaID, selItemNormaID.value);
        nNormaID = selItemNormaID.value;
    }
    
	lblItemNormaID.title = '';
	selItemNormaID.title = '';

    if ( !isNaN(parseInt(nNormaID, 10)) )
    {
		if (dsoEstaticCmbs.recordset.fields.count > 0)
		{
			if ( !(dsoEstaticCmbs.recordset.BOF && dsoEstaticCmbs.recordset.EOF) )
			{
				dsoEstaticCmbs.recordset.moveFirst();
				dsoEstaticCmbs.recordset.find('fldID', nNormaID);
				
				if ( !(dsoEstaticCmbs.recordset.BOF && dsoEstaticCmbs.recordset.EOF) )
				{
					lblItemNormaID.title = dsoEstaticCmbs.recordset['Hint'].value;
					selItemNormaID.title = dsoEstaticCmbs.recordset['Hint'].value;
				}
			}
		}
    }
    
}

/********************************************************************
Funcao disparada ao final da funcao prgServerInf(btnClicked) que e
disparada pelo frame work,

Esta funcao tem que ser chamada na ultima (ou ultimas) funcoes de
retorno de dados do servidor, feitas pelo programador.
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function finalOfSupCascade(btnClicked)
{
    //@@
    
    // Nao mexer - Inicio de automacao ==============================
    // Invoca a funcao lockAndOrSvrInf_SYS() do detailinf.js
    // para as operacoes de bancos de dados da automacao.
	sendJSMessage(getHtmlId(), JS_INFSYS, 'ENDPROGSERVERSUP', btnClicked);
	// Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pelo loop de mensagens wndJSProc(idElement, msg, param1, param2)
do js_detailsup.js

Esta funcao faz a chamada de todas as operacoes particulares
do form, necessarias apos um changeDiv. NAO PODE CONTER NENHUMA
OPERACAO DE BANCO DE DADOS.
           
Parametros: 
ultimo botao clicado na barra de botoes superior

Retorno:
nenhum
********************************************************************/
function prgInterfaceSup(btnClicked)
{
    //@@
    // Chama funcoes particulares do form que forem necessarias
    // depois de trocar o div

    // mostra dois botoes especificos desabilitados
    showBtnsEspecControlBar('sup', true, [1,1,0,0]);
    tipsBtnsEspecControlBar('sup', ['Texto', 'Procedimento', '', '']);
    
    if ( btnClicked == 'SUPINCL' )
    {
        // funcao que controla campos read-only
        controlReadOnlyFields();
	}
	
	adjustLabelsCombos(true);

    // Nao mexer - Inicio de automacao ==============================
    sendJSMessage(getHtmlId(), JS_INFSYS, 'ENDCHANGEDIVSUP', btnClicked);
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pela funcao selChanged() do js_interfaceex.js

Esta funcao recebe todos os onchange de combos do form e executa as
funcoes apropriadas definidas pelo programador e aqui invocadas. 
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function optChangedInCmb(cmb)
{
    var cmbID = cmb.id;
    
    //@@ Os if e os else if abaixo sao particulares de cada form
    // Troca a interface do sup em funcao de item selecionado
    // no combo de tipo.
    // Se for necessario ao programador, repetir o if abaixo
    // definido, antes deste comentario.
    // Nao mexer - Inicio de automacao ==============================
    if ( cmbID == 'selTipoRegistroID' )
        adjustSupInterface();
    else if ( cmbID == 'selIndicadorID' )
		setCalcFields(true);
	
	adjustLabelsCombos(false);		

    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pelo evento onClickBtnLupa() do js_detailsup.js

Propaga clique e botao de lupa que nao seja proprietario e
alternativo.
           
Parametros: 
btnClicked      - referencia ao botao de lupa clicado

Retorno:
nenhum
********************************************************************/
function btnLupaClicked(btnClicked)
{

}

/********************************************************************
Funcao disparada pelo frame work.
Chamada em diversos pontos do js_detailinf.js

Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked)
{
	var nAnchorID = '2541';
	
	if (btnClicked == 4)
	{
		window.top.openModalHTML(dsoSup01.recordset['RIQID'].value, 'Texto', 'S', 5);
	}
	else if (btnClicked == 2)
	{
		window.top.openModalControleDocumento('S', '', 422,null, nAnchorID, 'T');
	}
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada em diversos pontos do js_detailinf.js

Usuario clicou um botao nao especifico do control bar.

Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - Um dos seguintes botoes:
                - Avancar, retroagir, incluir, alterar, estado, 
                  excluir, OK, cancelar refresh, anterior e proximo. 
                  (Testar a string que vem).

Retornos:
null            - a automacao prossegue. Qualquer outro retorno a
                  automacao nao prossegue.
********************************************************************/
function btnBarNotEspecClicked(controlBar, btnClicked)
{
    var aEmpresaData = getCurrEmpresaData();

    if (btnClicked == 'SUPOK')
    {
        // Documentos
        if ((controlBar == 'SUP') && (btnClicked == 1)) {
            __openModalDocumentos(controlBar, dsoSup01.recordset.Fields[0].value);
        }
		// Novo Registro
		else if(dsoSup01.recordset[glb_sFldIDName] == null || dsoSup01.recordset[glb_sFldIDName].value == null)
		{
            // Grava o campo empresaID
            dsoSup01.recordset['EmpresaID'].value = aEmpresaData[0];
		}
	}

    // Para prosseguir a automacao retornar null
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pelo loop de mensagens wndJSProc(idElement, msg, param1, param2)
do js_detailsup.js

Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2)
{
    if (idElement.toUpperCase() == 'MODALCONTROLEDOCUMENTOHTML')
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALOPENHTML')
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    // Modal de documentos
    else if (idElement.toUpperCase() == 'MODALDOCUMENTOSHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pela funcao editRegister() do js_superior.js

Informa que o sup esta em modo de edicao e a interface ja esta
destravada.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function supInitEditMode()
{
    // funcao que controla campos read-only
    controlReadOnlyFields();

    // chamada abaixo necessaria para prosseguir a automacao
    // mover para final de operacoes de servidor se for o caso
    supInitEditMode_Continue();
}

// FINAL DE FUNCOES DA AUTOMACAO ************************************

// FUNCOES DO CARRIER ***********************************************

/********************************************************************
DISPARAR UM CARRIER:
Usar a seguinte funcao:

sendJSCarrier(getHtmlId(),param1, param2);

Onde:
getHtmlId() - fixo, remete o id do html que disparou o carrier.
param1      - qualquer coisa. De uso do programador.
param2      - qualquer coisa. De uso do programador.

********************************************************************/

/********************************************************************
Funcao disparada pelo frame work.
CARRIER CHEGANDO NO ARQUIVO.
Executa quando um carrier e disparado por outro form.

Parametros: 
idElement   - id do html que disparou o carrier
idBrowser   - id do browser que disparou o carrier
param1      - parametro um trazido pelo carrier
param2      - parametro dois trazido pelo carrier

Retorno:
Se intercepta sempre retornar o seguinte array:
array[0] = getHtmlId() ou null
array[1] = qualquer coisa ou null
array[2] = qualquer coisa ou null
Se nao intercepta retornar null
********************************************************************/
function carrierArrived(idElement, idBrowser, param1, param2)
{
    if (param1 == 'SHOWRIQ')
    {
        // Da automacao, deve constar de todos os ifs do carrierArrived
        if ( __currFormState() != 0 )
            return null;
        
        // param2 - traz array:
            // o id da empresa do form que mandou
            // o id do registro a abrir
        // No sendJSMessage abaixo:
        // param2[1] = null significa que nao deve paginar
        // param2[2] = null significa que nao ha array de ids

        var empresa = getCurrEmpresaData();
        
        if ( param2[0] != empresa[0])
            return null;

        // Exclusivo para exibir detalhe em form
        showDetailByCarrier(param2[1]);
            
        window.top.focus();
                
        return new Array(getHtmlId(), null, null);          
    }
    // Programador alterar conforme necessario
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
CARRIER RETORNANDO AO ARQUIVO QUE O DISPAROU.
Executa quando um carrier e disparado por este form e apos executar
nos demais forms abertos, retorna a este form.

Parametros: 
idElement   - string id do html que interceptou o carrier
idBrowser   - string id do browser que contem o form que interceptou
              o carrier
param1      - parametro um trazido pelo carrier
param2      - parametro dois trazido pelo carrier

Retorno:
Sempre retorna null
********************************************************************/
function carrierReturning(idElement, idBrowser, param1, param2)
{
    // Nao mexer - Inicio de automacao ==============================
    return null;
    // Final de Nao mexer - Inicio de automacao =====================
}

// FINAL DE FUNCOES DO CARRIER **************************************

// FUNCOES DA MAQUINA DE ESTADO *************************************

/********************************************************************
Funcao disparada pelo frame work.
Dispara logo apos abrir a maquina de estado. 
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
nenhum
********************************************************************/
function stateMachOpened( currEstadoID )
{

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou OK na maquina de estado
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
false - automacao prossegue
true  - automacao para        
       
Nota:
Se o programador retornar diferente de null nesta funcao,
o sistema permanecera travado, ao final do seu codigo o programador
dever� chamar a seguinte funcao da automacao:
stateMachSupExec(action)
onde action = 'OK' - o novo estado sera gravado
              'CANC' - a gravacao sera cancelada
para prosseguir a automacao
********************************************************************/
function stateMachBtnOK( currEstadoID, newEstadoID )
{
	//@@ prossegue a automacao
	return false;    
}

/********************************************************************
Funcao disparada pelo frame work.
Dispara se o usuario trocou o estado.
Dispara logo apos fechar a maquina de estado. 
           
Parametros: 
oldEstadoID     - previo EstadoID do registro
currEstadoID    - novo EstadoID do registro
bSavedID        - o novo estadoID foi gravado no banco (true/false)

Retorno:
nenhum
********************************************************************/
function stateMachClosed( oldEstadoID, currEstadoID, bSavedID )
{

}

/********************************************************************
Funcao criada pelo programador.
Controla os campos Read Only

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function controlReadOnlyFields()
{
    txtAbreviacao.readOnly = true;

	return null;
}

// FINAL DE FUNCOES DA MAQUINA DE ESTADO ****************************

// FUNCOES ESPECIFICAS DO FORM **************************************

// FINAL DE FUNCOES ESPECIFICAS DO FORM *****************************