/********************************************************************
superior.js

Library javascript para o superior.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_BtnFromFramWork = null;

var glb_CounterCmbsDynamics = 0;

var glb_bAlteracao = false;

// Dados do registro corrente .SQL 
var dsoSup01 = new CDatatransport("dsoSup01");
var dsoCmbDynamic01 = new CDatatransport("dsoCmbDynamic01");
var dsoCmbDynamic02 = new CDatatransport("dsoCmbDynamic02");
// Descricao do estado atual do registro .SQL 
var dsoStateMachine = new CDatatransport("dsoStateMachine");
// Dados dos combos estaticos .URL 
var dsoEstaticCmbs = new CDatatransport("dsoEstaticCmbs");
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

FUNCOES DA AUTOMACAO:
	window_onload()
	setupPage()
    putSpecialAttributesInControls()
    formFinishLoad()
    prgServerSup(btnClicked)
    finalOfSupCascade(btnClicked)
    prgInterfaceSup(btnClicked)
    optChangedInCmb(cmb)
    btnLupaClicked(btnClicked)
    btnBarClicked(controlBar, btnClicked)
    btnBarNotEspecClicked(controlBar, btnClicked)
    modalInformForm(idElement, param1, param2)
    supInitEditMode()

FUNCOES DO CARRIER:
    carrierArrived(idElement, idBrowser, param1, param2)
    carrierReturning(idElement, idBrowser, param1, param2)

FUNCOES DA MAQUINA DE ESTADO:
    stateMachOpened( currEstadoID )
    stateMachBtnOK( currEstadoID, newEstadoID )
    stateMachClosed( oldEstadoID, currEstadoID, bSavedID )
    
FUNCOES ESPECIFICAS DO FORM:    
********************************************************************/

/********************************************************************

// IMPLEMENTACAO DAS FUNCOES

//@@ todas as funcoes abaixo sao particulares de cada form
// conforme o form algumas funcoes devem ser removidas
// e outras serao acrescentadas
********************************************************************/

// FUNCOES DA AUTOMACAO *********************************************

/********************************************************************
Evento disparado ao final do carregamento da pagina.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function window_onload()
{
    //@@ Iniciado carregamento de form, primeira funcao controladora
    initInterfaceLoad(window.top.formName, 0X7);
    
    //@@ array bidimensional de combos ids e campo indice na tabela
    // identificador do combo. O array deve ser null se nao tem combos estaticos

    glb_aStaticCombos = ([['selTipoRealimentacaoID', '1']]);

    windowOnLoad_1stPart();

    //@@ Path dos arquivos inf e pesqlist
    glb_aFilePath = new Array(SYS_PAGESURLROOT + '/modqualidade/submodiso/rrc/inferior.asp',
                              SYS_PAGESURLROOT + '/modqualidade/submodiso/rrc/pesquisa.asp');

    //@@ Linka os divs com os correspondentes subforms
    // criando o atributo sfsGrupo nos divs.
    // Os infs nao tem div principal e todos os divs sao secundarios.
    // Parametros:
    // 1o par: id do div principal ou null se nao tem div principal
    // 2opar: array de divs ids dos divs secundarios
    // ou null se nao tem divs secundarios
    // 3o par: array de xxx_ID que identifica o grupo contido no div
    // ou null se o form nao tem div secundarios.
    // Esta funcao tageia o div principal e preenche array
    // dos divs secundarios do arquivo guardando na variavel
    // global do arquivo: glb_aDIVS
    linkDivsAndSubForms('divSup01_01', null, null);
    
    windowOnLoad_2ndPart();
    
    //@@ Guarda o nome do campo na tabela que contem o id do registro corrente no sup
    glb_sFldIDName = 'RRCID';
    
    //@@ Guarda o id do controle (combo) que contem os tipos de registros
    // se nao e aplicavel no form, fazer glb_sCtlTipoRegistroID = '';
    glb_sCtlTipoRegistroID = '';

    //@@ Guarda o nome do campo na tabela que contem o tipo do registro corrente no sup
    // se nao e aplicavel no form, fazer glb_sFldTipoRegistroName = ''
    glb_sFldTipoRegistroName = '';
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pela funcao windowOnLoad_2ndPart() do js_detailsup.js

Usar para ajustes graficos da interface.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function setupPage()
{
    //@@ Ajusta os divs e seus elementos
    adjustDivs('sup',[[1,'divSup01_01']]);

    //@@ *** Div principal superior divSup01_01 ***/
    adjustElementsInForm([['lblRegistroID','txtRegistroID',10,1],
						  ['lblEstadoID','txtEstadoID',2,1],
						  ['lblRealimentacao','txtRealimentacao',60,1],
						  ['lblTipoRealimentacaoID','selTipoRealimentacaoID',20,1],
						  ['lbldtEmissao','txtdtEmissao',10,2],
						  ['lbldtVencimento','txtdtVencimento',10,2],
						  ['lblCritico','chkCritico',3,2],
						  ['lblObservacao','txtObservacao',30,2,-10],
						  ['lblClienteID','selClienteID',20,3],
						  ['btnFindCliente','btn',24,3],
						  ['lblContatoID','selContatoID',20,3],
						  ['btnFindContato','btn',24,3],
						  ['lblContato','txtContato',20,3],
						  ['lblDDI','txtDDI',5,4],
						  ['lblDDD','txtDDD',5,4],
						  ['lblTelefone','txtTelefone',8,4],
						  ['lblEMail','txtEMail',40,4]], null, null, true);
						  
	txtTelefone.ondblclick = txtTelefone_ondblclick;
	txtEMail.ondblclick = txtEMail_ondblclick;
}

/********************************************************************
Funcao disparada pelo frame work.
Chamado pela funcao windowOnLoad_2ndPart() do js_superior.js

Coloca atributos em campos para serem manobrados graficamente
(esconde/mostra e habilita desabilita) pelo framework.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function putSpecialAttributesInControls()
{
    //@@
    
    // Tabela de atributos especiais:
    // 1. fnShowHide - seta funcao do programador que o framework executa
    //                 ao mostrar/esconder o controle.
    // 2. prog_SHHD  - (true) retira do frame work a operacao de mostrar/esconder
    //                 habiitar/desabilitar o controle e transfere para
    //                 o programador. False ou null devolve para o framework.
    
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pelo loop de mensagens wndJSProc(idElement, msg, param1, param2)
do js_detailsup.js

Form carregou e vai mostrar a interface.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function formFinishLoad()
{
    // seta botao de impressao
    // especBtnIsPrintBtn('sup', 1);

    // mostra dois botoes especificos desabilitados
    showBtnsEspecControlBar('sup', true, [1,1,1,1,1]);
    tipsBtnsEspecControlBar('sup', ['Documentos', 'Relatórios', 'Procedimento', 'Texto', 'E-mail cliente']);
}

/********************************************************************
Funcao disparada pelo frame work.
Funcao disparada pelo frame work.
Chamada pelo loop de mensagens wndJSProc(idElement, msg, param1, param2)
do js_detailsup.js

Esta funcao faz a chamada de todas as operacoes particulares de servidor
do e preenchimentos de combos do sup, quando o usuario clica
um botao da barra do sup.
           
Parametros: 
ultimo botao clicado na barra de botoes superior

Retorno:
nenhum
********************************************************************/
function prgServerSup(btnClicked)
{
    // chama funcao do programador que inicia o carregamento
    // de dados dos combos dinamicos (selSujeitoID e selObjetoID)
    glb_BtnFromFramWork = btnClicked;

    if ( (btnClicked == 'SUPDET') ||
         (btnClicked == 'SUPREFR') ||
         (btnClicked == 'SUPANT') ||
         (btnClicked == 'SUPSEG') )
    {
        startDynamicCmbs();
    }    
    else    
		finalOfSupCascade(glb_BtnFromFramWork);
}

/********************************************************************
Funcao disparada ao final da funcao prgServerInf(btnClicked) que e
disparada pelo frame work,

Esta funcao tem que ser chamada na ultima (ou ultimas) funcoes de
retorno de dados do servidor, feitas pelo programador.
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function finalOfSupCascade(btnClicked)
{
    //@@
    
    // Nao mexer - Inicio de automacao ==============================
    // Invoca a funcao lockAndOrSvrInf_SYS() do detailinf.js
    // para as operacoes de bancos de dados da automacao.
	sendJSMessage(getHtmlId(), JS_INFSYS, 'ENDPROGSERVERSUP', btnClicked);
	// Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pelo loop de mensagens wndJSProc(idElement, msg, param1, param2)
do js_detailsup.js

Esta funcao faz a chamada de todas as operacoes particulares
do form, necessarias apos um changeDiv. NAO PODE CONTER NENHUMA
OPERACAO DE BANCO DE DADOS.
           
Parametros: 
ultimo botao clicado na barra de botoes superior

Retorno:
nenhum
********************************************************************/
function prgInterfaceSup(btnClicked)
{
    //@@
    // Chama funcoes particulares do form que forem necessarias
    // depois de trocar o div
    
    clearAndLockCmbsDynamics(btnClicked);

    // mostra dois botoes especificos desabilitados
    showBtnsEspecControlBar('sup', true, [1,1,1,1,1]);
    tipsBtnsEspecControlBar('sup', ['Documentos', 'Relatórios', 'Procedimento', 'Texto', 'E-mail cliente']);
    
    if ( btnClicked == 'SUPINCL' )
    {
        // funcao que controla campos read-only
        controlReadOnlyFields();
	}    

    // Nao mexer - Inicio de automacao ==============================
    sendJSMessage(getHtmlId(), JS_INFSYS, 'ENDCHANGEDIVSUP', btnClicked);
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao do programador
Disparada pela funcao prgServerSup()
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function startDynamicCmbs()
{
    // controla o retorno do servidor dos dados de combos dinamicos
    glb_CounterCmbsDynamics = 2;

    // parametrizacao do dso dsoCmbDynamic01 (designado para selSujeitoID)
    setConnection(dsoCmbDynamic01);
    
    dsoCmbDynamic01.SQL = 'SELECT 0 AS fldID,SPACE(0) AS fldName ' +
						  'UNION ALL ' +
						  'SELECT PessoaID AS fldID,Fantasia AS fldName ' +
						  'FROM Pessoas ' +
                          'WHERE PessoaID = ' + dsoSup01.recordset['ClienteID'].value;
    dsoCmbDynamic01.ondatasetcomplete = dsoCmbDynamic_DSC;
    dsoCmbDynamic01.Refresh();

    // parametrizacao do dso dsoCmbDynamic02 (designado para selObjetoID)
    setConnection(dsoCmbDynamic02);
    
    dsoCmbDynamic02.SQL = 'SELECT 0 AS fldID,SPACE(0) AS fldName ' +
						  'UNION ALL ' +
						  'SELECT PessoaID AS fldID,Fantasia AS fldName ' +
						  'FROM Pessoas ' +
                          'WHERE PessoaID = ' + dsoSup01.recordset['ContatoID'].value;
    dsoCmbDynamic02.ondatasetcomplete = dsoCmbDynamic_DSC;
    dsoCmbDynamic02.Refresh();
}

/********************************************************************
Funcao disparada pelo programador.
Funcao do ondatasetcomplete disparada pela funcao prgServerSup()
           
Parametros:     
Nenhum  

Retorno:
nenhum
********************************************************************/
function dsoCmbDynamic_DSC() {
    var i;
    var optionStr, optionValue;
    var aCmbsDynamics = [selClienteID,selContatoID];
    var aDSOsDunamics = [dsoCmbDynamic01,dsoCmbDynamic02];

    clearComboEx(['selClienteID','selContatoID',]);
    
    glb_CounterCmbsDynamics --;
    
    if (glb_CounterCmbsDynamics == 0)
    {
        for (i=0; i<=1; i++)
        {
            while (! aDSOsDunamics[i].recordset.EOF )
            {
                optionStr = aDSOsDunamics[i].recordset['fldName'].value;
	        	optionValue = aDSOsDunamics[i].recordset['fldID'].value;
                var oOption = document.createElement("OPTION");
                oOption.text = optionStr;
                oOption.value = optionValue;
                aCmbsDynamics[i].add(oOption);
                aDSOsDunamics[i].recordset.MoveNext();
            }
        }

        adjustLabelsCombos(true);
        
        // volta para a automacao
	    finalOfSupCascade(glb_BtnFromFramWork);
	}
	return null;
}


/********************************************************************
Funcao disparada pelo frame work.
Chamada pela funcao selChanged() do js_interfaceex.js

Esta funcao recebe todos os onchange de combos do form e executa as
funcoes apropriadas definidas pelo programador e aqui invocadas. 
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function optChangedInCmb(cmb)
{
    var cmbID = cmb.id;
    
    //@@ Os if e os else if abaixo sao particulares de cada form
    // Troca a interface do sup em funcao de item selecionado
    // no combo de tipo.
    // Se for necessario ao programador, repetir o if abaixo
    // definido, antes deste comentario.
    // Nao mexer - Inicio de automacao ==============================
    if ( cmbID == 'selTipoRegistroID' )
        adjustSupInterface();

	adjustLabelsCombos(false);        

    // Final de Nao mexer - Inicio de automacao =====================
}

function adjustLabelsCombos(bPaging)
{
    if (bPaging)
    {
        setLabelOfControl(lblClienteID, dsoSup01.recordset['ClienteID'].value);
        setLabelOfControl(lblContatoID, dsoSup01.recordset['ContatoID'].value);
    }
    else
    {
        setLabelOfControl(lblClienteID, selClienteID.value);
        setLabelOfControl(lblContatoID, selContatoID.value);
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pelo evento onClickBtnLupa() do js_detailsup.js

Propaga clique e botao de lupa que nao seja proprietario e
alternativo.
           
Parametros: 
btnClicked      - referencia ao botao de lupa clicado

Retorno:
nenhum
********************************************************************/
function btnLupaClicked(btnClicked)
{
    //@@
    var cmbID = '';
    var nTipoRelacaoID = 21;
    var nRegExcluidoID = '';
    
    if ( btnClicked.id == 'btnFindCliente' )
    {
        nRegExcluidoID = '';
        showModalRelacoes(window.top.formID, 'S', 'selClienteID' , 'SUJ', getLabelNumStriped(lblClienteID.innerText) , nTipoRelacaoID, nRegExcluidoID);
        return null;
    }
    else if ( btnClicked.id == 'btnFindContato' )
    {
		if (selClienteID.selectedIndex < 0)
		{
			if ( window.top.overflyGen.Alert ('Selecione o cliente antes.') == 0 )
				return null;
				
			return null;				
		}
		
        nRegExcluidoID = selClienteID.value;
        showModalRelacoes(window.top.formID, 'S', 'selContatoID' , 'SUJ', getLabelNumStriped(lblContatoID.innerText) , nTipoRelacaoID, nRegExcluidoID);
        return null;
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada em diversos pontos do js_detailinf.js

Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked)
{
	var nEstadoID = dsoSup01.recordset['EstadoID'].value;
	var nAnchorID = '';

	// Documentos
	if ((controlBar == 'SUP') && (btnClicked == 1)) {
	    __openModalDocumentos(controlBar, dsoSup01.recordset.Fields[0].value);
	}
	else if (btnClicked == 4)
		window.top.openModalHTML(dsoSup01.recordset['RRCID'].value, 'Texto', 'S', 4);
	else if (btnClicked == 3)
	{
		if (nEstadoID == 1)
			nAnchorID = '21';
		else if (nEstadoID == 41)
			nAnchorID = '212';

		window.top.openModalControleDocumento('S', '', 723, null, nAnchorID, 'T');
	}
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada em diversos pontos do js_detailinf.js

Usuario clicou um botao nao especifico do control bar.

Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - Um dos seguintes botoes:
                - Avancar, retroagir, incluir, alterar, estado, 
                  excluir, OK, cancelar refresh, anterior e proximo. 
                  (Testar a string que vem).

Retornos:
null            - a automacao prossegue. Qualquer outro retorno a
                  automacao nao prossegue.
********************************************************************/
function btnBarNotEspecClicked(controlBar, btnClicked)
{
    var aEmpresaData = getCurrEmpresaData();
    var sTelefone = '';
    var sDDD = '';
    var i;

    if (btnClicked == 'SUPOK')
    {
		if(dsoSup01.recordset[glb_sFldIDName] == null || dsoSup01.recordset[glb_sFldIDName].value == null)
		{
            // Grava o campo empresaID
            dsoSup01.recordset['EmpresaID'].value = aEmpresaData[0];
		}
		
		sTelefone = trimStr(stripNonNumericChars(txtTelefone.value));
		sDDD = trimStr(stripNonNumericChars(txtDDD.value));
				
		for (i=0; i<sDDD.length; i++)
		{
			if (sDDD.substr(i,1) != '0')
				break;
		}
			
		if (i<sDDD.length)
		{
			sDDD = sDDD.substr(i, sDDD.length - i);
		}
		else
		{
			sDDD = '';
		}

		txtTelefone.value = sTelefone;
		txtDDD.value = sDDD;
	}
			
    // Para prosseguir a automacao retornar null
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pelo loop de mensagens wndJSProc(idElement, msg, param1, param2)
do js_detailsup.js

Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2)
{
    if (idElement.toUpperCase() == 'MODALCONTROLEDOCUMENTOHTML')
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALOPENHTML')
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    else if ( idElement.toUpperCase() == 'MODALPRINTHTML' )    
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
           
            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }    
    }
    // Modal de relacoes
    if ( idElement.toUpperCase() == 'MODALRELACOESHTML' )
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            
            fillFieldsByRelationModal(param2);
            
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
           
            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }
    // Modal de documentos
    else if (idElement.toUpperCase() == 'MODALDOCUMENTOSHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }      
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pela funcao editRegister() do js_superior.js

Informa que o sup esta em modo de edicao e a interface ja esta
destravada.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function supInitEditMode()
{
    // funcao que controla campos read-only
    controlReadOnlyFields();

    // chamada abaixo necessaria para prosseguir a automacao
    // mover para final de operacoes de servidor se for o caso
    supInitEditMode_Continue();
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal de impressao

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalPrint()
{
    var htmlPath;
    var strPars = new String();
    var empresaData = getCurrEmpresaData();
    var empresaID = empresaData[0];
    var empresaFantasia = empresaData[3];
    var contexto = getCmbCurrDataInControlBar('sup', 1);
    var userID = getCurrUserID();
    var nDocumentoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,"dsoSup01.recordset['DocumentoID'].value");
    
    // mandar os parametros para o servidor
    // o primeiro parametro e obrigatorio
    
    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + escape('S');

    // parametros opcionais
    strPars += '&nEmpresaID=' + escape(empresaID);
    strPars += '&sEmpresaFantasia=' + escape(empresaFantasia);       
    strPars += '&nContextoID=' + escape(contexto[1]);       
    strPars += '&nUserID=' + escape(userID);       
    strPars += '&nDocumentoID=' + escape(nDocumentoID);       

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_ASPURLROOT + '/modqualidade/submodiso/rrc/modalpages/modalprint.asp'+strPars;

    showModalWin(htmlPath, new Array(346,234));
}

function clearAndLockCmbsDynamics(btnClicked)
{
    if (btnClicked == 'SUPINCL')
    {
        clearComboEx(['selClienteID','selContatoID']);
        selClienteID.disabled = true;
        selContatoID.disabled = true;
	}
}

/********************************************************************
Funcao criada pelo programador.
Preenche os campos do subform corrente, com os dados vindos da janela
modal de relacoes

Parametro:
aData   -   array de dados
            aData[0] - ID do combo
            aData[1] - ID selecionado no grid da modal
            aData[2] - texto correspondente ao ID selecionado no grid da modal
            
            os demais elementos sao opcionais

Retorno:
nenhum
********************************************************************/
function fillFieldsByRelationModal(aData)
{
    var oldDataSrc;
    var oldDataFld;
    var oOption;
    var elem;
    var addSexOrServ = null;
    
    elem = window.document.getElementById(aData[0]);
    
    if ( elem == null )
        return;
    
    clearComboEx([elem.id]);
        
    oldDataSrc = elem.dataSrc;
    oldDataFld = elem.dataFld;
    elem.dataSrc = '';
    elem.dataFld = '';

    // Preencher o combo selSujeitoID
    if ( aData[0] == 'selClienteID' )
    {
        dsoSup01.recordset['ClienteID'].value = aData[1];
        clearComboEx(['selContatoID']);
        selContatoID.disabled = true;
    }    
    // Preencher o combo selObjetoID
    else if ( aData[0] == 'selContatoID' )
    {
        dsoSup01.recordset['ContatoID'].value = aData[1];
        selContatoID.disabled = false;
	}        

    // Insere um item em branco
    oOption = document.createElement("OPTION");
    oOption.text = '';
    oOption.value = 0;
    elem.add(oOption);

    oOption = document.createElement("OPTION");
    oOption.text = aData[2];
    oOption.value = aData[1];
    elem.add(oOption);
    elem.selectedIndex = 1;

    elem.dataSrc = oldDataSrc;
    elem.dataFld = oldDataFld;
    
    if ((aData[3] != null) && (aData[3] != ''))
		txtEMail.value = aData[3];
    
    if ((aData[4] != null) && (aData[4] != '') &&
		(aData[5] != null) && (aData[5] != '') &&
		(aData[6] != null) && (aData[6] != ''))
    {
		// DDI
		txtDDI.value = aData[4];
		// DDD
		txtDDD.value = aData[5];
		// Numero do Telefone
		txtTelefone.value = aData[6];
	}
}

// FINAL DE FUNCOES DA AUTOMACAO ************************************

// FUNCOES DO CARRIER ***********************************************

/********************************************************************
DISPARAR UM CARRIER:
Usar a seguinte funcao:

sendJSCarrier(getHtmlId(),param1, param2);

Onde:
getHtmlId() - fixo, remete o id do html que disparou o carrier.
param1      - qualquer coisa. De uso do programador.
param2      - qualquer coisa. De uso do programador.

********************************************************************/

/********************************************************************
Funcao disparada pelo frame work.
CARRIER CHEGANDO NO ARQUIVO.
Executa quando um carrier e disparado por outro form.

Parametros: 
idElement   - id do html que disparou o carrier
idBrowser   - id do browser que disparou o carrier
param1      - parametro um trazido pelo carrier
param2      - parametro dois trazido pelo carrier

Retorno:
Se intercepta sempre retornar o seguinte array:
array[0] = getHtmlId() ou null
array[1] = qualquer coisa ou null
array[2] = qualquer coisa ou null
Se nao intercepta retornar null
********************************************************************/
function carrierArrived(idElement, idBrowser, param1, param2)
{
    if (param1 == 'SHOWRRC')
    {
        // Da automacao, deve constar de todos os ifs do carrierArrived
        if ( __currFormState() != 0 )
            return null;
        
        // param2 - traz array:
            // o id da empresa do form que mandou
            // o id do registro a abrir
        // No sendJSMessage abaixo:
        // param2[1] = null significa que nao deve paginar
        // param2[2] = null significa que nao ha array de ids

        var empresa = getCurrEmpresaData();
        
        if ( param2[0] != empresa[0])
            return null;

        // Exclusivo para exibir detalhe em form
        showDetailByCarrier(param2[1]);
            
        window.top.focus();
                
        return new Array(getHtmlId(), null, null);          
    }
    // Programador alterar conforme necessario
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
CARRIER RETORNANDO AO ARQUIVO QUE O DISPAROU.
Executa quando um carrier e disparado por este form e apos executar
nos demais forms abertos, retorna a este form.

Parametros: 
idElement   - string id do html que interceptou o carrier
idBrowser   - string id do browser que contem o form que interceptou
              o carrier
param1      - parametro um trazido pelo carrier
param2      - parametro dois trazido pelo carrier

Retorno:
Sempre retorna null
********************************************************************/
function carrierReturning(idElement, idBrowser, param1, param2)
{
    // Nao mexer - Inicio de automacao ==============================
    return null;
    // Final de Nao mexer - Inicio de automacao =====================
}

// FINAL DE FUNCOES DO CARRIER **************************************

// FUNCOES DA MAQUINA DE ESTADO *************************************

/********************************************************************
Funcao disparada pelo frame work.
Dispara logo apos abrir a maquina de estado. 
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
nenhum
********************************************************************/
function stateMachOpened( currEstadoID )
{

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou OK na maquina de estado
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
false - automacao prossegue
true  - automacao para        
       
Nota:
Se o programador retornar diferente de null nesta funcao,
o sistema permanecera travado, ao final do seu codigo o programador
deverá chamar a seguinte funcao da automacao:
stateMachSupExec(action)
onde action = 'OK' - o novo estado sera gravado
              'CANC' - a gravacao sera cancelada
para prosseguir a automacao
********************************************************************/
function stateMachBtnOK( currEstadoID, newEstadoID )
{
	//@@ prossegue a automacao
	return false;    
}

/********************************************************************
Funcao disparada pelo frame work.
Dispara se o usuario trocou o estado.
Dispara logo apos fechar a maquina de estado. 
           
Parametros: 
oldEstadoID     - previo EstadoID do registro
currEstadoID    - novo EstadoID do registro
bSavedID        - o novo estadoID foi gravado no banco (true/false)

Retorno:
nenhum
********************************************************************/
function stateMachClosed( oldEstadoID, currEstadoID, bSavedID )
{

}

/********************************************************************
Funcao criada pelo programador.
Controla os campos Read Only

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function controlReadOnlyFields()
{
	return null;
}

// FINAL DE FUNCOES DA MAQUINA DE ESTADO ****************************

// FUNCOES ESPECIFICAS DO FORM **************************************

function txtTelefone_ondblclick()
{
	if ( (txtDDD.value == '') || (txtTelefone.value == '') || 
		 (selClienteID.selectedIndex < 0) || (txtTelefone.readOnly == false))
		return true;

	lockInterface(true);

	dialByModalPage(txtDDD.value, txtTelefone.value, selClienteID.value);

	lockInterface(false);
}

function txtEMail_ondblclick()
{
	if ( (txtEMail.value == '') || (txtEMail.readOnly == false))
		return true;

	lockInterface(true);

	window.location = 'mailto:' + txtEMail.value;

	lockInterface(false);
}

/********************************************************************
Recebe input de janela modal para discar pelo carrier.
Definir e implementar no pesqlist, no sup e no inf do form.
********************************************************************/
function dialByModalPage(sDDD, sNumero, nPessoaID)
{
	sDDD = sDDD.toString();
	sNumero = sNumero.toString();
	nPessoaID = parseInt(nPessoaID, 10);
		
    sendJSCarrier(getHtmlId(), 'TELEFONIA_DIAL', new Array(sDDD, sNumero, nPessoaID));
}

// FINAL DE FUNCOES ESPECIFICAS DO FORM *****************************