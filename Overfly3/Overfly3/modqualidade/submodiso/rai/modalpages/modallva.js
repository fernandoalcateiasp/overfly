/********************************************************************
modallva.js

Library javascript para o modallva.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_aEmpresaData = null;
var glb_nUserID = 0;
// Controla se algum dado do grid foi alterado
var glb_dataGridWasChanged = false;
var glb_nDSOs = 0;
var glb_LVATimerInt = null;

//  Dados do grid .RDS  
var dsoGrid = new CDatatransport("dsoGrid");
//  Dados do grid .RDS  
var dsoCmbLVA = new CDatatransport("dsoCmbLVA");
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
cmb_onchange()
btn_onclick(ctl)
refreshParamsAndDataAndShowModalWin(fromServer)
getCurrEmpresaData()
setupBtnsFromGridState()
fillGridData()
fillGridData_DSC()
saveDataInGrid()
saveDataInGrid_DSC()

EVENTOS DE GRID

DEFINIDAS NO ARQUIVO modallva.ASP
Sub fg_BeforeRowColChange(OldRow, OldCol, NewRow, NewCol, Cancel)
    Cancel = js_fg_BeforeRowColChange (fg, OldRow, OldCol, NewRow, NewCol, Cancel)
End Sub

Sub fg_AfterRowColChange(OldRow, OldCol, NewRow, NewCol)
    fg_AfterRowColChange()
End Sub

Sub fg_EnterCell()
    js_fg_EnterCell fg
End Sub
FINAL DE DEFINIDAS NO ARQUIVO modallva.ASP

js_fg_modallvaBeforeMouseDown(fg, Button, Shift, X, Y, Cancel)
js_fg_modallvaDblClick( grid, Row, Col)
js_modallvaKeyPress(KeyAscii)
js_modallva_AfterRowColChange
js_modallva_ValidateEdit()
js_modallva_AfterEdit(Row, Col)
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();
    
    // ajusta o body do html
    with (modallvaBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // configuracao inicial do html
    setupPage();   

    // mostra a modal
    refreshParamsAndDataAndShowModalWin(true);
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    secText('LVA', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var eLeft = 2;
    var eTop = 1;
    var rQuote = 0;
    var btnWidth = 78;
    
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    eLeft = 2;
    eTop = 2;
    rQuote = 0;
    
    // desabilita o botao OK
    btnOK.disabled = true;

    // troca label botao Cancelar   
    btnCanc.value = 'Cancelar';
    
    // ajusta o divFG
    with (divFG.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;    
        height = parseInt(btnOK.currentStyle.top, 10) - parseInt(top, 10) - ELEM_GAP;
    }
    
    with (fg.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }
    
    startGridInterface(fg);
    fg.Cols = 0;
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;    
    fg.Redraw = 2;
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;
    
    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnOK')
    {
        saveDataInGrid();
    }
    // codigo privado desta janela
    else
    {
        var _retMsg;
        var sMsg = '';
            
        if ( sMsg != '' )
        {
            _retMsg = window.top.overflyGen.Confirm(sMsg);
	        	    
	        if ( (_retMsg == 0) || (_retMsg == 2) )
	            return null;
        }    
        
        //restoreInterfaceFromModal();
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, false ); 
    }    
}

/********************************************************************
Funcao chamada pela automacao
Esta funcao so executa se a modal havia sido previamente executada,
ou seja, apos a primeira carga da modal.
Atualizar parametros e dados aqui e apos, mostrar a modal.

Parametros:
fromServer  - true, a modal esta vindo do servidor
            - null, a modal ja esta carregada

********************************************************************/
function refreshParamsAndDataAndShowModalWin(fromServer)
{
    // @@ atualizar parametros e dados aqui
    glb_aEmpresaData = getCurrEmpresaData();
    glb_nUserID = glb_USERID;
        
    // @@ atualizar interface aqui
    setupBtnsFromGridState();
    
    // mostra a janela modal
    fillGridData();
}

/********************************************************************
Trava/destrava todos os botoes
********************************************************************/
function setupBtnsFromGridState()
{
    var bHasRowsInGrid = fg.Rows > 1;
    
    btnOK.disabled = !(bHasRowsInGrid && glb_dataGridWasChanged);
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridData()
{
	glb_nDSOs = 2;
    if (glb_LVATimerInt != null)
    {
        window.clearInterval(glb_LVATimerInt);
        glb_LVATimerInt = null;
    }

    var nRAIID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,"dsoSup01.recordset['RAIID'].value");

    lockControlsInModalWin(true);
    
    // zera o grid
    fg.Rows = 1;
    
    glb_dataGridWasChanged = false;
    
    // parametrizacao do dso dsoGrid
    setConnection(dsoGrid);

    dsoGrid.SQL = 'SELECT a.* FROM RAI_LVA a WITH(NOLOCK) WHERE ' +
        'a.RAIID = ' + nRAIID + ' ' +
        'ORDER BY (SELECT c.Ordem FROM Documentos_Questionarios b WITH(NOLOCK), TiposAuxiliares_Itens c WITH(NOLOCK) ' +
				'WHERE b.DocQuestionarioID=a.DocQuestionarioID AND b.ItemNormaID=c.ItemID), ' +
			'(SELECT d.Ordem FROM Documentos_Questionarios d WITH(NOLOCK) WHERE d.DocQuestionarioID=a.DocQuestionarioID)';

    dsoGrid.ondatasetcomplete = fillGridData_DSC;
    dsoGrid.Refresh();
    
    setConnection(dsoCmbLVA);
    dsoCmbLVA.SQL = 'SELECT ItemID, ItemAbreviado ' +
			'FROM TiposAuxiliares_Itens WITH(NOLOCK) ' +
			'WHERE (EstadoID=2 AND TipoID=604) ' +
			'ORDER BY Ordem';
	dsoCmbLVA.ondatasetcomplete = fillGridData_DSC;
	dsoCmbLVA.Refresh();
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData_DSC() {
    glb_nDSOs--;
	
	if (glb_nDSOs > 0)
		return null;
	
    showExtFrame(window, true);
    // restoreInterfaceFromModal(true);

    var dTFormat = '';
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';
        
    fg.Redraw = 0;
    fg.Editable = false;
    startGridInterface(fg);
    fg.FrozenCols = 0;
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;    
    
    headerGrid(fg,['ID',
				   'Observa��o',
                   'Evid�ncia',
                   'Constata��o',
                   'Class',
                   'A��o',
                   'Eficaz',
                   'RAILVAID'], [7]);

    fillGridMask(fg,dsoGrid,['DocQuestionarioID*',
							 'Observacao',
							 'Evidencia',
							 'Constatacao',
							 'ClassificacaoID',
							 'Acao',
							 'Eficaz',
							 'RAILVAID'],
							 ['', '', '', '', '', '', '', ''],
                             ['', '', '', '', '', '', '', '']);
                             
	insertcomboData(fg,4,dsoCmbLVA,'ItemAbreviado','ItemID');

    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);
    
    fg.FrozenCols = 1;
    
    fg.Redraw = 2;
    
    // Nao retirar o if abaixo, ela garante que o grid
    // so deixa editar a coluna 4
    if ( fg.Rows > 1 )
        fg.Col = 1;
    
    if (fg.Rows > 1)
        fg.Editable = true;

    lockControlsInModalWin(false);

    // se nenhuma linha foi selecionada, seleciona a primeira
    // se a mesma existe
    if ( (fg.Row < 1) && (fg.Rows > 1) )
        fg.Row = 1;
        
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;    
    
    fg.Redraw = 2;

    // se tem linhas no grid, coloca foco no grid
    if (fg.Rows > 1)
    {
        window.focus();
        fg.focus();
    }                
    else
    {
        ;
    }            

    // ajusta estado dos botoes
    setupBtnsFromGridState();
}

/********************************************************************
Salva as linhas alteradas no grid
********************************************************************/
function saveDataInGrid()
{
    glb_dataGridWasChanged = false;
    
    lockControlsInModalWin(true);
    
    try
    {
        dsoGrid.SubmitChanges();
    }
    catch(e)
    {
        // Numero de erro qdo o registro foi alterado ou removido por outro usuario
        if (e.number == -2147217887)
        {
            if ( window.top.overflyGen.Alert ('Este registro acaba de ser alterado ou removido.') == 0 )
                return null;
        }
        
        fg.Rows = 1;
        lockControlsInModalWin(false);
        
        // setupBtnsFromGridState();
        // sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, true ); 
        return null;
    }    

    dsoGrid.ondatasetcomplete = saveDataInGrid_DSC;
    dsoGrid.Refresh();
}

/********************************************************************
Retorno do servidor, apos salvar as linhas alteradas no grid
********************************************************************/
function saveDataInGrid_DSC() {
    lockControlsInModalWin(false);
    
    sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, true );
}

// EVENTOS DE GRID **************************************************

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modallvaBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
{
	;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modallvaDblClick(grid, Row, Col)
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modallvaKeyPress(KeyAscii)
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modallva_ValidateEdit()
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modallva_BeforeEdit(grid, row, col)
{
    var fldDet = get_FieldDetails(dsoGrid, grid.ColKey(col));
    
    // limita qtd de caracteres digitados em uma celula do grid
    if ( fldDet == null )   // erro, permite digitar cerca 32 k caracteres
        grid.EditMaxLength = 0;
    else if ( (fldDet[0] == 200) || (fldDet[0] == 129) )  // varchar ou char
        grid.EditMaxLength = fldDet[1];
    else    
        grid.EditMaxLength = 0;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modallva_AfterEdit(Row, Col)
{
    var nType;
    
    if (fg.Editable)
    {
        // Altera o campo do dso com o novo valor
        dsoGrid.recordset.MoveFirst();
        dsoGrid.recordset.find('DocQuestionarioID', getCellValueByColKey(fg, 'DocQuestionarioID*', Row));

        if ( !(dsoGrid.recordset.EOF) )
        {
            nType = dsoGrid.recordset[fg.ColKey(Col)].type;
            
			// Se decimal , numerico , int, bigint ou boolean
			if ( (nType == 131) || (nType == 14) || (nType == 11) || (nType == 3) || (nType == 20) )
                dsoGrid.recordset[fg.ColKey(Col)].value = fg.ValueMatrix(Row, Col);
            else    
                dsoGrid.recordset[fg.ColKey(Col)].value = fg.TextMatrix(Row, Col);
                
            glb_dataGridWasChanged = true;
            setupBtnsFromGridState();
        }    
    }    
}
// FINAL DE EVENTOS DE GRID *****************************************
