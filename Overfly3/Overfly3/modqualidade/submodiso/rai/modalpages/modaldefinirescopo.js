/********************************************************************
modaldefinirescopo.js

Library javascript para o modaldefinirescopo.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_timerInterval = null;
var glb_PassedOneInDblClick = false;

//  Dsos genericos para banco de dados 
var dsoGen01 = new CDatatransport("dsoGen01");
var dsoGen02 = new CDatatransport("dsoGen02");
var dsoIncluiItens = new CDatatransport("dsoIncluiItens");
// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modaldefinirescopo_AfterRowColChange (fg, OldRow, OldCol, NewRow, NewCol)
{
    var firstLine = 0;
    
    if ( glb_totalCols__ != false )
        firstLine = 1;

    if (NewRow > firstLine)
    {
        fg.Row = NewRow;
    }
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modaldefinirescopo_AfterEdit(Row, Col)
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modaldefinirescopo_DblClick( grid, Row, Col)
{
	var i = 0;
	var bChecked = 1;
	
    if (glb_PassedOneInDblClick == true)
    {    
        glb_PassedOneInDblClick = false;
        return true;
    }

	if (Col != getColIndexByColKey(grid, 'OK'))
		return null;
		
	if (grid.Row <= 0)
		return null;
	
	if ( grid.ValueMatrix(1, getColIndexByColKey(grid, 'OK')) != 0 )
		bChecked = 0;
		
	glb_PassedOneInDblClick = true;

	for (i=1; i<grid.Rows; i++)
	{
		grid.TextMatrix(i, getColIndexByColKey(grid, 'OK')) = bChecked;
	}
}

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();   
    
    // ajusta o body do html
    with (modaldefinirescopoBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
       
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);

    window.focus();

    startPesq();
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    window.focus();
    if (ctl.id == btnOK.id )
        btnOK.focus();
    else if (ctl.id == btnCanc.id )
        btnCanc.focus();
    
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id )
    {
        // 1. O usuario clicou o botao OK
        btnOK_Clicked();
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_I', null );
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // texto da secao01
    secText('Definir Escopo', 1);

    // carrega dados na interface e configura travamento de seus elementos
    loadDataAndTreatInterface();
}

/********************************************************************
Carrega dados nos elementos da interface e trava/destrava controles
********************************************************************/
function loadDataAndTreatInterface()
{
    // ajusta elementos da janela
    var elem;
    var frameRect, modWidth, modHeight;
    var topFree, widthFree, heightFree; 
    var frameBorder = 6;
    var x_gap, y_gap;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    // inicio da altura livre
    topFree = parseInt(divMod01.currentStyle.top, 10) + parseInt(divMod01.currentStyle.height, 10);
    
    // altura livre da janela modal (descontando barra azul 
    // posicao verical dos botoes OK/Cancel nao e considerada nesta janela)
    heightFree = modHeight - topFree - frameBorder;    
    
    // largura livre
    widthFree = modWidth - frameBorder;

    // ajusta o divFG
    elem = window.document.getElementById('divFG');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = ELEM_GAP * 3;
        width = modWidth - frameBorder - (2 * ELEM_GAP);    
        height = parseInt(btnOK.currentStyle.top, 10) -
                 parseInt(top, 10) - frameBorder - ELEM_GAP;
    }
    
    elem = document.getElementById('fg');
    with (elem.style)
    {
        visibility = 'visible';
        left = 0;
        top = 0;
        width = parseInt(document.getElementById('divFG').currentStyle.width);
        height = parseInt(document.getElementById('divFG').currentStyle.height);
    }
    
    startGridInterface(fg);
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;
    fg.Editable = false;
    
    btnOK.disabled = true;
    
    btnCanc.style.left = parseInt(btnOK.currentStyle.left, 10) +
                         parseInt(btnOK.currentStyle.width, 10) + ELEM_GAP;
    
}

/********************************************************************
Inverte o check de um checkbox associado a um label
Funcao copiada da lib js_interfacex.js, nao linkada neste form
********************************************************************/
function invertChkBox()
{
    var ctl = this.nextSibling;
    
    if ( ctl.disabled == true )
        return;

    ctl.checked = !ctl.checked;

    window.focus();
    if ( ctl.disabled == false )
        ctl.focus();

    return true;
}

/********************************************************************
Usuario clicou o botao OK
********************************************************************/
function btnOK_Clicked()
{
	incluiItem();
}

function incluiItem()
{
	var strPars = '';
	var bOK = 0;

	strPars = '?nRAIID=' + glb_nRAIID;
	
	for (i=1; i<fg.Rows; i++)
	{
		if ( fg.ValueMatrix(i, getColIndexByColKey(fg, 'OK')) != 0 )
			bOK = 1;
		else
			bOK = 0;
		
		strPars += '&sNorma' + '=' + escape(getCellValueByColKey(fg, 'Norma*', i));
		strPars += '&nOK' + '=' + escape(bOK);
	}

	strPars += '&nDataLen=' + (i-1).toString();
	
    // pagina asp no servidor saveitemspedido.asp
    dsoGen02.URL = SYS_ASPURLROOT + '/modqualidade/submodiso/rai/serverside/savedefinirescopo.aspx' + strPars;
    dsoGen02.ondatasetcomplete = saveItens_DSC;
    dsoGen02.refresh();
}

function saveItens_DSC() {
    var i;
    
    if ( !(dsoGen02.recordset.BOF && dsoGen02.recordset.EOF) )
    {
            if ( window.top.overflyGen.Alert (dsoGen02.recordset['fldErrorText'].value) == 0 )
                return null;
                
            lockControlsInModalWin(false);       
            window.focus();
            fg.focus();    
            return null;
    }
    
    lockControlsInModalWin(false);
    glb_timerInterval = window.setInterval ( 'startPesq()', 10 , 'JavaScript');
}


/********************************************************************
Mostra janela apos carregar dados
********************************************************************/
function showModalAfterDataLoad()
{
    // ajusta o body do html
    with (modaldefinirescopoBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
       
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
    window.focus();
}

function startPesq()
{
    if ( glb_timerInterval != null )
    {
        window.clearInterval(glb_timerInterval);
        glb_timerInterval = null;
    }

	btnOK.disabled = true;
    lockControlsInModalWin(true);
    
    setConnection(dsoGen01);

	dsoGen01.SQL = 'SELECT c.ItemID AS NormaID, c.ItemAbreviado AS Norma, c.ItemMasculino AS Descricao, ' +
		'CONVERT(BIT, (SELECT COUNT(*) FROM RAI_LVA d WITH(NOLOCK), Documentos_Questionarios e WITH(NOLOCK), TiposAuxiliares_Itens f WITH(NOLOCK) ' +
		   'WHERE (d.RAIID = ' + glb_nRAIID + ' AND d.DocQuestionarioID = e.DocQuestionarioID AND ' +
		    'e.ItemNormaID = f.ItemID AND f.ItemFeminino = c.ItemAbreviado))) AS OK ' +
		'FROM Documentos_Questionarios a WITH(NOLOCK), TiposAuxiliares_Itens b WITH(NOLOCK), TiposAuxiliares_Itens c WITH(NOLOCK) ' +
		'WHERE (a.DocumentoID = 822001 AND a.ItemNormaID = b.ItemID AND ' +
		'b.EstadoID = 2 AND b.Aplicar = 1 AND b.ItemFeminino = c.ItemAbreviado AND ' +
		'c.TipoID = 602) ' +
		'GROUP BY c.ItemID, c.ItemAbreviado, c.ItemMasculino, c.Ordem ' +
		'ORDER BY c.ItemID';
	
    dsoGen01.ondatasetcomplete = startPesq_DSC;
    dsoGen01.Refresh();
}

function startPesq_DSC() {
    fg.Redraw = 0;
    startGridInterface(fg);
    fg.FrozenCols = 0;

    var dTFormat = '';
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';

    headerGrid(fg,['Norma',
				   'Descri��o',
				   'OK',
				   'NormaID'], [3]);

    fillGridMask(fg,dsoGen01,['Norma*',
							  'Descricao*',
							  'OK',
							  'NormaID'],
                              ['','','',''],
                              ['','','','']);

    // Merge de Colunas
    fg.MergeCells = 4;
    fg.MergeCol(-1) = true;

    // alignColsInGrid(fg,[1]);
    alignColsInGrid(fg,[]);

    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);
    
    fg.Redraw = 2;
    
    
    lockControlsInModalWin(false);
    
    fg.Editable = true;
    
    btnOK.disabled = fg.Rows <= 1;
    
	fg.focus();
}
