/********************************************************************
modalincluidocs.js

Library javascript para o modalincluidocs.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_aEmpresaData = null;
var glb_nUserID = 0;

var glb_nDSOs = 0;

var glb_ModIncDocsTimerInt = null;

//  Dados do grid .RDS  
var dsoGrid = new CDatatransport("dsoGrid");
//  Gravacao de dados do grid .RDS  
var dsoGrava = new CDatatransport("dsoGrava");

// Variaveis de eventos de grid

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
btn_onclick(ctl)
refreshParamsAndDataAndShowModalWin(fromServer)
setupBtnsFromGridState()
fillGridData()
fillGridData_DSC()
saveDataInGrid()
saveDataInGrid_DSC()

js_fg_modalincluidocsBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
js_fg_modalincluidocsDblClick(grid, Row, Col)
js_modalincluidocsKeyPress(KeyAscii)
js_modalincluidocs_ValidateEdit()
js_modalincluidocs_BeforeEdit(grid, row, col)
js_modalincluidocs_AfterEdit(Row, Col)
js_fg_AfterRowColmodalincluidocs (grid, OldRow, OldCol, NewRow, NewCol)
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();   

    // ajusta o body do html
    with (modalincluidocsBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
    
    showExtFrame(window, true);
   
    // mostra a modal
    refreshParamsAndDataAndShowModalWin(true);
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
	var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    
	secText('Documentos Verificados', 1);
	
    // Desabilita o botao OK
    btnOK.disabled = true;
    
    // Reposiciona botao OK
    with (btnOK)
    {         
		style.top = divMod01.offsetTop + divMod01.offsetHeight + ELEM_GAP;
		style.left = divMod01.offsetLeft + divMod01.offsetWidth - btnOK.offsetWidth - ELEM_GAP;
		value = 'Gravar';
    }
	
    // Troca label botao Cancelar   
    btnCanc.value = 'Cancelar';
    // Esconde botao cancel
    with (btnCanc)
    {
		style.visibility = 'hidden';
    }
       
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }

    // ajusta o divFG
    with (divFG.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = btnOK.offsetTop + btnOK.offsetHeight + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;    
        height = modHeight - divFG.offsetTop - (3 * ELEM_GAP / 2);
    }
    
    with (fg.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }
    
    startGridInterface(fg);
    fg.Cols = 0;
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;    
    fg.Redraw = 2;
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;
    
    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnOK')
    {
        saveDataInGrid();
    }
    else
    {
        //restoreInterfaceFromModal();
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, false ); 
    }    
}

/********************************************************************
Funcao chamada pela automacao
Esta funcao so executa se a modal havia sido previamente executada,
ou seja, apos a primeira carga da modal.
Atualizar parametros e dados aqui e apos, mostrar a modal.

Parametros:
fromServer  - true, a modal esta vindo do servidor
            - null, a modal ja esta carregada

********************************************************************/
function refreshParamsAndDataAndShowModalWin(fromServer)
{
    // @@ atualizar parametros e dados aqui
    glb_aEmpresaData = getCurrEmpresaData();
    glb_nUserID = glb_USERID;
    
    lockControlsInModalWin(true);
        
    // @@ atualizar interface aqui
    setupBtnsFromGridState();
    
    // preenche o grid da janela modal
    fillGridData();
}

/********************************************************************
Trava/destrava todos os botoes
********************************************************************/
function setupBtnsFromGridState()
{
    
    btnOK.disabled = true;
    
    if ( fg.Rows <= 1 )
		return true;
    
    var i;
    var col = getColIndexByColKey(fg, 'OK');
    
    for ( i=1; i<fg.Rows; i++ )
    {
		if ( fg.ValueMatrix(i, col) != 0 )
		{
			btnOK.disabled = false;		
			break;
		}
    }
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridData()
{
	if (glb_ModIncDocsTimerInt != null)
    {
        window.clearInterval(glb_ModIncDocsTimerInt);
        glb_ModIncDocsTimerInt = null;
    }
    
	var aGrid = null;
	var i = 0;
	var strSQL = '';
	var nSujeitoID = 0;

	lockControlsInModalWin(true);
	
	glb_nDSOs = 1;
    
    // zera o grid
    fg.Rows = 1;
    
    // parametrizacao do dso dsoGrid
    setConnection(dsoGrid);

	strSQL = 'SELECT a.DocumentoID, b.RecursoAbreviado, a.Documento, CONVERT(BIT, 0) AS OK, ' +
				'c.ItemAbreviado AS Norma, a.Versao, a.dtEmissao, d.Fantasia ' +
             'FROM Documentos a WITH(NOLOCK), Recursos b WITH(NOLOCK), TiposAuxiliares_Itens c WITH(NOLOCK), Pessoas d WITH(NOLOCK) ' +
		     'WHERE (dbo.fn_Documento_Estado(a.DocumentoID, 2) = 2 AND ' +
				'a.EstadoID = b.RecursoID AND a.ItemNormaID = c.ItemID AND a.ProprietarioID = d.PessoaID AND ' +
                '(SELECT COUNT(*) FROM RAI_Documentos WITH(NOLOCK) WHERE(RAIID = ' + glb_nRAIID + ' AND ' +
					'DocumentoID = a.DocumentoID)) = 0) ' +
			 'ORDER BY a.DocumentoID';

	dsoGrid.SQL = strSQL;
			
    dsoGrid.ondatasetcomplete = fillGridData_DSC;
    dsoGrid.Refresh();
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData_DSC() {
    glb_nDSOs--;
	
	if (glb_nDSOs > 0)
		return null;

    var dTFormat = '';
    var i;
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';
    
    fg.Redraw = 0;
    fg.Editable = false;
    startGridInterface(fg);
    
    fg.FontSize = '8';
    fg.FrozenCols = 0;
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;    

    headerGrid(fg,['ID',
                   'Est',
                   'Documento',
				   'OK',
				   'Norma',
				   'Rev',
				   'Emiss�o',
				   'Aprova��o'], []);
	
    fillGridMask(fg,dsoGrid,['DocumentoID*',
                             'RecursoAbreviado*',
                             'Documento*',
							 'OK',
							 'Norma*',
							 'Versao*',
							 'dtEmissao*',
							 'Fantasia*'],
							 ['', '', '', '', '', '', '99/99/9999', ''],
                             ['', '', '', '', '', '', 'dtFormat', '']);

    alignColsInGrid(fg,[]);                           

    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);
    
    fg.FrozenCols = 4;
        
    fg.Redraw = 2;
    
    if ( fg.Rows > 1 )
		fg.Row = 1;

    // Nao retirar o if abaixo, ela garante que o grid
    // so deixa editar a partir da primeira coluna readonly
    if ( fg.Rows > 1 )
        fg.Col = getColIndexByColKey(fg, 'OK');
        
	fg.ExplorerBar = 5;
	
	lockControlsInModalWin(false);

    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;    

    fg.Redraw = 2;

    // se tem linhas no grid, coloca foco no grid
    if (fg.Rows > 1)
    {
        window.focus();
        fg.Editable = true;
        fg.focus();
    }                
    else
    {
        ;
    }            

    // Merge de Colunas
    fg.MergeCells = 4;
    fg.MergeCol(-1) = true;
    
    // ajusta estado dos botoes
    setupBtnsFromGridState();
}

/********************************************************************
Salva as linhas alteradas no grid
********************************************************************/
function saveDataInGrid()
{
	var strPars = new String();
	var i=0;
	var nCol = getColIndexByColKey(fg, 'OK');
	var nDataLen = 0;
	
    lockControlsInModalWin(true);
    
    strPars = '?nRAIID=' + escape(glb_nRAIID);
    
    for (i=1; i<fg.Rows; i++)
    {
		if ( fg.ValueMatrix(i, nCol) != 0 )
		{
			strPars += '&nDocID=' + escape(getCellValueByColKey(fg, 'DocumentoID*', i));
			nDataLen++;
		}	
	}
	
	strPars += '&nDataLen=' + escape(nDataLen);

    if (nDataLen > 0)
    {
		dsoGrava.URL = SYS_ASPURLROOT + '/modqualidade/submodiso/rai/serverside/gravamodalincluidocs.aspx' + strPars;
		dsoGrava.ondatasetcomplete = saveDataInGrid_DSC;
		dsoGrava.refresh();
    }
    else
    {
		lockControlsInModalWin(false);
		
		if ( window.top.overflyGen.Alert('Selecione pelo menos um documento') == 0 )
                return null;		
        
        window.focus();
        fg.focus();        
	}	
}

/********************************************************************
Retorno do servidor, apos salvar as linhas alteradas no grid
********************************************************************/
function saveDataInGrid_DSC() {
    // Fecha a janela
	sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_' + glb_sCaller, false );
}

// EVENTOS DE GRID **************************************************

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalincluidocsBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
{
	;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalincluidocsDblClick(grid, Row, Col)
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalincluidocsKeyPress(KeyAscii)
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalincluidocs_ValidateEdit()
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalincluidocs_BeforeEdit(grid, row, col)
{
    var fldDet = get_FieldDetails(dsoGrid, grid.ColKey(col));

    // limita qtd de caracteres digitados em uma celula do grid
    if ( fldDet == null )   // erro, permite digitar cerca 32 k caracteres
        grid.EditMaxLength = 0;
    else if ( (fldDet[0] == 200) || (fldDet[0] == 129) )  // varchar ou char
        grid.EditMaxLength = fldDet[1];
    else    
        grid.EditMaxLength = 0;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalincluidocs_AfterEdit(Row, Col)
{
	if (Col == getColIndexByColKey(fg, 'OK'))
		setupBtnsFromGridState();
}

// FINAL DE EVENTOS DE GRID *****************************************
