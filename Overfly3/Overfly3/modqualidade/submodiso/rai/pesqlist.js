/********************************************************************
pesqlist.js

Library javascript para o pesquisa.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
//  Dados da listagem da pesquisa 
var dsoListData01 = new CDatatransport("dsoListData01");
//  Dados dos combos de contexto e filtros 
var dsoCmbsContFilt = new CDatatransport("dsoCmbsContFilt");
//  Dados dos proprietarios para o pesqlist .URL 
var dsoPropsPL = new CDatatransport("dsoPropsPL");
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

FUNCOES DA AUTOMACAO:
	window_onload()
    pesqlistIsVisibleAndUnlocked()
    btnBarClicked(controlBar, btnClicked)
    modalInformForm(idElement, param1, param2)
    specialClauseOfResearch()
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

// FUNCOES DA AUTOMACAO *********************************************

/********************************************************************
Evento disparado ao final do carregamento da pagina.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function window_onload()
{
    windowOnLoad_1stPart();
    
    //@@ Ordem e titulos das colunas do grid de pesquisa
    glb_COLPESQORDER = new Array('ID', 'Est', 'Data', 'Vencimento', 'GC', 'OK', 'Observação', 'Proprietário');

    var dTFormat = '';

    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';
    
    glb_aCOLPESQFORMAT = new Array('', '', dTFormat, dTFormat,'###.00','','','');
    
    windowOnLoad_2ndPart();
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada em diversos pontos do js_pesqlist.js

O pesqlist esta visivel e ativo
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function pesqlistIsVisibleAndUnlocked()
{
	var nCurrRAIID = 0;
	
	especBtnIsPrintBtn('sup', 1);
	showBtnsEspecControlBar('sup', true, [1,1,1,1]);
	//tipsBtnsEspecControlBar('sup', ['Relatórios', 'Procedimento', 'Texto', '']);
	tipsBtnsEspecControlBar('sup', ['Documentos', 'Relatórios', 'Procedimento', 'Texto']);

	if (fg.Row < 1)
		setupEspecBtnsControlBar('sup', 'DDHD');
	else
		setupEspecBtnsControlBar('sup', 'HDHH');
	
	// Reajustar alinhamento das colunas do grid aqui, se necessario
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada em diversos pontos do js_pesqlist.js

Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked) {
    // Usuario clicou botao documentos
    if (btnClicked == 1) {
        if (fg.Rows > 1) {
            __openModalDocumentos(controlBar, fg.TextMatrix(fg.Row, 0));
        }
        else {
            window.top.overflyGen.Alert('Selecione um registro.');
        }
    }
	else if (btnClicked == 3)
	{
		window.top.openModalControleDocumento('PL', '', 822, null, null, 'T');
	}
	else if (btnClicked == 4)
	{
		nCurrRAIID = getCellValueByColKey(fg, 'RAIID', fg.Row);
		window.top.openModalHTML(nCurrRAIID, 'Texto', 'S', 2);
	}
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada em dois pontos do js_pesqlist.js

Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2)
{
    if (idElement.toUpperCase() == 'MODALCONTROLEDOCUMENTOHTML')
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    // Modal de documentos
    else if (idElement.toUpperCase() == 'MODALDOCUMENTOSHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pela funcao goExecPesqShowList(formID) do js_pesqlist.js

Pede string complementar de pesquisa para forms que tenham este caso.

Parametro:
nenhum

Retorno:
a string ou null se nao tem
********************************************************************/
function specialClauseOfResearch()
{
    //@@ da automacao -> retorno padrao
    return null;
}

// FINAL DE FUNCOES DA AUTOMACAO ************************************