/********************************************************************
inferioresp.js

Library javascript para o inferior.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_nFormID = window.top.formID;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

FUNCOES DA AUTOMACAO:	
    setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
	setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
	fillCmbPastasInCtlBarInf(nTipoRegistroID)

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

// FUNCOES DA AUTOMACAO *********************************************

/********************************************************************
Funcao disparada pelo frame work.
Chamada pela funcao filtroComplete_DSC(emptyFilter) do js_detailinf.js

Esta funcao seta o sql do dso para buscar o registro a ser mostrado no detalhe,
vai ao banco de dados no servidor e no retorno chama a funcao de
ondatasetcomplete de acordo com o parametro recebido.
           
Parametros: 
dso                        -> dso a ser aberto
folderID                   -> id da pasta corrente no control bar inferior
idToFind                   -> id do registro corrente no sup
sFiltro                    -> string do filtro corrente no control bar inferior
nTipoRegistroID            -> tipo do registro corrente
Retorno:
nulo
********************************************************************/
function setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
{
    var nEmpresaData = getCurrEmpresaData();

    setConnection(dso);
    
    //@@
    if ( (folderID == 20008) || (folderID == 20009) ) // Prop/Altern ou Observacoes
    {
        dso.SQL = 'SELECT a.RAIID,a.ProprietarioID,a.AlternativoID,a.Observacoes FROM RAI a WITH(NOLOCK) WHERE '+
        sFiltro + 'a.RAIID = ' + idToFind;
        return 'dso01JoinSup_DSC';
    }
    // Texto
    else if (folderID == 26151)
    {
        dso.SQL = 'SELECT a.RAIID,a.ProprietarioID,a.AlternativoID,a.Texto FROM RAI a WITH(NOLOCK) WHERE ' +
        sFiltro + 'a.RAIID = ' + idToFind;
        return 'dso01JoinSup_DSC';
    }
    // Participantes
    else if (folderID == 26152)
    {
        dso.SQL = 'SELECT a.*, CONVERT(BIT, (CASE a.PessoaID WHEN (SELECT b.ProprietarioID FROM RAI b WITH(NOLOCK) WHERE RAIID=a.RAIID) ' +
			'THEN 1 ELSE 0 END)) AS AuditorLider ' +
		'FROM RAI_Participantes a WITH(NOLOCK) WHERE a.RAIID = ' + idToFind + ' ' +
		'ORDER BY a.Auditor DESC, (SELECT Fantasia FROM Pessoas WITH(NOLOCK) WHERE PessoaID=a.PessoaID)';
        return 'dso01Grid_DSC';
    }
    // Plano de Auditoria
    else if (folderID == 26153)
    {
        dso.SQL = 'SELECT a.*, LEFT(CONVERT(VARCHAR(8000),Observacoes),30) + (CASE CONVERT(VARCHAR(8000),Observacoes) WHEN SPACE(0) THEN ' +
        'SPACE(0) ELSE ' + '\'' + '...' + '\'' + 'END) AS ObsAbreviado FROM RAI_PlanosAuditoria a WITH(NOLOCK) WHERE ' +
        sFiltro + 'a.RAIID = ' + idToFind+ ' ' +
        'ORDER BY a.dtInicio, (SELECT Fantasia FROM Pessoas WITH(NOLOCK) WHERE PessoaID=a.AuditorID) ';
        return 'dso01Grid_DSC';
    }
    // LVA
    else if (folderID == 26154)
    {
        dso.SQL = 'SELECT a.*, LEFT(CONVERT(VARCHAR(8000),Observacoes),30) + (CASE CONVERT(VARCHAR(8000),Observacoes) WHEN SPACE(0) THEN ' +
        'SPACE(0) ELSE ' + '\'' + '...' + '\'' + 'END) AS ObsAbreviado FROM RAI_LVA a WITH(NOLOCK) WHERE ' +
        sFiltro + 'a.RAIID = ' + idToFind+ ' ' +
        'ORDER BY (SELECT c.Ordem FROM Documentos_Questionarios b WITH(NOLOCK), TiposAuxiliares_Itens c WITH(NOLOCK) ' +
				'WHERE b.DocQuestionarioID=a.DocQuestionarioID AND b.ItemNormaID=c.ItemID), ' +
			'(SELECT d.Ordem FROM Documentos_Questionarios d WITH(NOLOCK) WHERE d.DocQuestionarioID=a.DocQuestionarioID)';
        return 'dso01Grid_DSC';
    }
    // Documentos Verificados
    else if (folderID == 26155)
    {
        dso.SQL = 'SELECT a.* FROM RAI_Documentos a WITH(NOLOCK) WHERE ' +
        sFiltro + 'a.RAIID = ' + idToFind+ ' ' +
        'ORDER BY a.DocumentoID ';
        return 'dso01Grid_DSC';
    }
    // Documentos Relacionados
    else if (folderID == 26156)
    {
        dso.SQL = 'EXEC sp_DocumentosRelacionados 822001, ' + idToFind;
        return 'dso01Grid_DSC';
    }
    // LOG
    else if (folderID == 20010)
    {
        dso.SQL = 'SELECT *, dbo.fn_Data_Fuso(a.Data, NULL, ' + nEmpresaData[2] + ') AS DataFuso ' +
                  'FROM LOGs a WITH(NOLOCK) ' + 
                  'WHERE ' + sFiltro + ' a.RegistroID = '+ idToFind + ' ' +
                  'AND a.FormID=' + glb_nFormID + ' ' +
                  'ORDER BY a.SubFormID ASC, a.Data DESC';
        return 'dso01Grid_DSC';
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pela funcao dso01Grid_DSC() do js_detailinf.js

Monta string de select para combo de grid

Parametros:
dso                 - referencia ao dso corrente
pastaID             - pasta corrente
nRegistroID         - id do registro corrente
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
{
    setConnection(dso);
    
    // LOG
    if (pastaID == 20010)
    {
        if ( dso.id == 'dso01GridLkp' )
        {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoFantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.SubFormID = b.RecursoID)';
        }
        else if ( dso.id == 'dso02GridLkp' )
        {
            dso.SQL = 'SELECT b.PessoaID, b.Fantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Pessoas b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.UsuarioID = b.PessoaID)';
        }
        else if ( dso.id == 'dso03GridLkp' )
        {
            dso.SQL = 'SELECT b.ItemID, b.ItemAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), TiposAuxiliares_Itens b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EventoID = b.ItemID)';
        }
        else if ( dso.id == 'dso04GridLkp' )
        {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EstadoID = b.RecursoID)';
        }
    }
    // Participantes
    else if (pastaID == 26152)
    {
        var nEmpresaData = getCurrEmpresaData();
        var nEmpresaID = nEmpresaData[0]; 

        if ( dso.id == 'dsoCmb01Grid_01' )
        {
            dso.SQL = 'SELECT DISTINCT c.PessoaID, c.Fantasia ' +
				'FROM RelacoesPesRec_Perfis a WITH(NOLOCK), RelacoesPesRec b WITH(NOLOCK), Pessoas c WITH(NOLOCK) ' +
				'WHERE (a.EmpresaID = ' + nEmpresaID + ' AND a.OK = 1 AND a.RelacaoID = b.RelacaoID AND ' +
					'b.EstadoID = 2 AND b.SujeitoID = c.PessoaID AND c.EstadoID = 2) ' +
				'ORDER BY c.Fantasia';
        }
        else if ( dso.id == 'dsoCmb02Grid_01' )
        {
            dso.SQL = 'SELECT a.RecursoID, a.RecursoFantasia ' +
				'FROM Recursos a WITH(NOLOCK) ' +
				'WHERE (a.EstadoID = 2 AND a.TipoRecursoID=6) ' +
				'ORDER BY a.RecursoFantasia';
        }
        else if ( dso.id == 'dso01GridLkp')
        {
            dso.SQL = 'SELECT a.RAIPessoaID, CONVERT(BIT, (CASE a.PessoaID WHEN (SELECT b.ProprietarioID FROM RAI b WITH(NOLOCK) WHERE RAIID=a.RAIID) ' +
				'THEN 1 ELSE 0 END)) AS AuditorLider ' +
				'FROM RAI_Participantes a WITH(NOLOCK) WHERE a.RAIID = ' + nRegistroID + ' ' +
				'ORDER BY Auditor DESC, (SELECT Fantasia FROM Pessoas WITH(NOLOCK) WHERE PessoaID=a.PessoaID)';
        }
    }
    // Plano de Auditoria
    else if (pastaID == 26153)
    {
        if ( dso.id == 'dso01GridLkp' )
        {
            dso.SQL = 'SELECT DISTINCT a.RAIPlanoID, ' +
					'CONVERT(VARCHAR(5), dbo.fn_Data_Formata(a.dtInicio, a.dtFim, 1)) AS Tempo ' +
				'FROM RAI_PlanosAuditoria a WITH(NOLOCK) ' +
				'WHERE (a.RAIID = ' + nRegistroID + ') ';
        }
        else if ( dso.id == 'dso02GridLkp' )
        {
            dso.SQL = 'SELECT dbo.fn_Pad(CONVERT(VARCHAR(2), ' +
						'FLOOR(dbo.fn_DivideZero(SUM(DATEDIFF(mi, a.dtInicio, a.dtFim)), 60))), 2, CHAR(48), CHAR(76)) + CHAR(58) + ' + 
						'dbo.fn_Pad(CONVERT(VARCHAR(2), SUM(DATEDIFF(mi, a.dtInicio, a.dtFim)) % 60), 2, CHAR(48), CHAR(76)) AS TempoTotal ' +
				'FROM RAI_PlanosAuditoria a WITH(NOLOCK) ' +
				'WHERE (a.RAIID = ' + nRegistroID + ') ';
        }
        else if ( dso.id == 'dsoCmb01Grid_01' )
        {
            dso.SQL = 'SELECT DISTINCT b.PessoaID, b.Fantasia ' +
				'FROM RAI_Participantes a WITH(NOLOCK), Pessoas b WITH(NOLOCK) ' +
				'WHERE (a.RAIID =  ' + nRegistroID + ' AND ((a.PessoaID = (SELECT b.ProprietarioID FROM RAI b WITH(NOLOCK) ' +
					'WHERE b.RAIID=a.RAIID) ) OR a.Auditor = 1) AND ' +
					'a.PessoaID = b.PessoaID) ' +
				'ORDER BY b.Fantasia';
        }
    }
    // LVA
    else if (pastaID == 26154)
    {
		if ( dso.id == 'dsoCmb01Grid_01' )
		{
			dso.SQL = 'SELECT ItemID, ItemAbreviado ' +
						'FROM TiposAuxiliares_Itens WITH(NOLOCK) ' +
						'WHERE (EstadoID=2 AND TipoID=604) ' +
						'ORDER BY Ordem';
		}
        else if (dso.id == 'dso01GridLkp')
        {
            dso.SQL = 'SELECT a.DocQuestionarioID, b.Letra AS Letra, c.ItemAbreviado AS Norma ' +
				'FROM RAI_LVA a WITH(NOLOCK), Documentos_Questionarios b WITH(NOLOCK), TiposAuxiliares_Itens c WITH(NOLOCK) ' +
				'WHERE (a.RAIID = ' + nRegistroID + ' AND a.DocQuestionarioID = b.DocQuestionarioID AND ' +
					'b.ItemNormaID = c.ItemID)';
        }
    }
    // Documentos Verificados
    else if (pastaID == 26155)
    {
		if ( dso.id == 'dso01GridLkp' )
		{
			dso.SQL = 'SELECT RecursoID, RecursoAbreviado ' +
						'FROM Recursos WITH(NOLOCK) ' +
						'WHERE (TipoRecursoID = 7)';
		}
        else if (dso.id == 'dso02GridLkp')
        {
            dso.SQL = 'SELECT b.DocumentoID, c.ItemAbreviado AS Norma ' +
				'FROM RAI_Documentos a WITH(NOLOCK), Documentos b WITH(NOLOCK), TiposAuxiliares_Itens c WITH(NOLOCK) ' +
				'WHERE (a.RAIID = ' + nRegistroID + ' AND a.DocumentoID = b.DocumentoID AND ' +
					'b.ItemNormaID = c.ItemID)';
        }
        else if (dso.id == 'dso03GridLkp')
        {
            dso.SQL = 'SELECT b.PessoaID, b.Fantasia ' +
				'FROM RAI_Documentos a WITH(NOLOCK), Pessoas b WITH(NOLOCK) ' +
				'WHERE (a.RAIID = ' + nRegistroID + ' AND a.ProprietarioID = b.PessoaID)';
        }
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada em diversos pontos do js_detailinf.js

Esta funcao preenche o combo de pastas do controlbar inferior

Parametros:
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function fillCmbPastasInCtlBarInf(nTipoRegistroID)
{
    var i=0;
    var currSelection = getCmbCurrDataInControlBar('inf', 1);
    var vPastaName;

	cleanupSelInControlBar('inf',1);

	vPastaName = window.top.getPastaName(20008);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20008); //Observa��es

    vPastaName = window.top.getPastaName(20009);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20009); //Proprietario

    vPastaName = window.top.getPastaName(20010);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20010); //LOG

    vPastaName = window.top.getPastaName(26151);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 26151); //Texto

    vPastaName = window.top.getPastaName(26152);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 26152); //Participantes

    vPastaName = window.top.getPastaName(26153);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 26153); //Plano de Auditoria

    vPastaName = window.top.getPastaName(26154);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 26154); //LVA

    vPastaName = window.top.getPastaName(26155);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 26155); //Documentos Verificados

    vPastaName = window.top.getPastaName(26156);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 26156); //Documentos Relacionados

    // Nao mexer - Inicio de automacao ============================== 	
	// Tenta selecionar novamente a pasta anterior selecionada no combo
	// Se nao mais existe a pasta anteriormente selecionada no combo
	// seleciona a pasta default
	
	stripFoldersByRight(currSelection);
        
    return true;
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada em diversos pontos do js_detailinf.js

Tratar consistencia dos dados inseridos pelo usuario aqui.
           
Parametros: 
btnClicked      - controlbar e botao clicado pelo usuario
folderID        - id da pasta selecionada
tipoCtlrsPasta  - 'GRID' a pasta e um grid/ 'CTRLS' a pasta nao e
                  um grid
currDSO         - referencia dso corrente
registroID      - id do registro corrente no sup

Retorno:
true se os dados estao ok, caso contrario false.
********************************************************************/
function criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)
{
    //@@ return true se tudo ok, caso contrario return false;
 
    var currLine = 0;

    // A pasta e grid    
    if (tipoCtlrsPasta == 'GRID') // se � grid
    {
        if (folderID == 26152) // Participantes
            currLine = criticLineGridAndFillDsoEx(fg, currDSO,keepCurrLineInGrid(),[0], ['RAIID', registroID]);

        if (folderID == 26153) // Plano de Auditoria
            currLine = criticLineGridAndFillDsoEx(fg, currDSO,keepCurrLineInGrid(),[0,1,2,3], ['RAIID', registroID], [6]);

        if (folderID == 26154) // LVA
            currLine = criticLineGridAndFillDsoEx(fg, currDSO,keepCurrLineInGrid(),[2], ['RAIID', registroID], [10]);

        if (folderID == 26155) // Documentos Verificados
            currLine = criticLineGridAndFillDsoEx(fg, currDSO,keepCurrLineInGrid(),[0], ['RAIID', registroID]);

        if (currLine < 0)
            return false;
    }   
    
    return true;
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pela funcao buildGridFromFolder(folderID) do js_detailinf.js

Constroi o corpo de um grid. Os combos sao construidos pelo frame work
           
Parametros: 
folderID        - id da pasta corrente
currDSO         - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function constructGridBody(folderID, currDSO)
{
    var dTFormat = '';
    var i = 0;
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';
    
    fg.FrozenCols = 0;
    
    // LOG
    if (folderID == 20010)
    {
        headerGrid(fg,['SubForm',
                       'Data           Hora     ',
                       'Colaborador',
                       'Evento',
                       'Est',
                       'Motivo',
                       'LOGID'], [6]);
                       
        fillGridMask(fg,currDSO,['^SubFormID^dso01GridLkp^RecursoID^RecursoFantasia',
                                 'DataFuso',
                                 '^UsuarioID^dso02GridLkp^PessoaID^Fantasia',
                                 '^EventoID^dso03GridLkp^ItemID^ItemAbreviado',
                                 '^EstadoID^dso04GridLkp^RecursoID^RecursoAbreviado',
                                 'Motivo',
                                 'LOGID'],
                                 ['','99/99/9999','','','','',''],
                                 ['',dTFormat + ' hh:mm:ss','','','','','']);

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
    // Participantes
    else if (folderID == 26152)
    {
        headerGrid(fg,['Colaborador',
					   'Cargo',
					   'L�der',
					   'Auditor',
					   'RA',
					   'Auditado',
					   'RE',
                       'RAIPessoaID'], [7]);

        glb_aCelHint = [[0,2,'Auditor L�der'],
                        [0,3,'Auditor'],
                        [0,4,'Participou da reuni�o de abertura?'],
                        [0,5,'Foi auditado?'],
                        [0,6,'Participou da reuni�o de encerramento?']];

        fillGridMask(fg,currDSO,['PessoaID',
								 'PerfilID',
								 '^RAIPessoaID^dso01GridLkp^RAIPessoaID^AuditorLider*',
								 'Auditor',
								 'ReuniaoAbertura',
								 'Auditado',
								 'ReuniaoEncerramento',
								 'RAIPessoaID'],
								 ['','','','','','','',''],
                                 ['','','','','','','','']);

		fg.FrozenCols = 1;
    }
    // Plano de Auditoria
    else if (folderID == 26153)
    {
        headerGrid(fg,['In�cio         Hora     ',
                       'Fim            Hora     ',
                       'Tempo',
                       'Assunto',
                       'Auditor',
                       'Observa��es',
                       'Observa��es',
                       'RAIPlanoID'], [6,7]);

        fillGridMask(fg,currDSO,['dtInicio',
								 'dtFim',
								 '^RAIPlanoID^dso01GridLkp^RAIPlanoID^Tempo*',
								 'Assunto',
								 'AuditorID',
								 '_calc_ObsAbreviado_30*',
								 'Observacoes',
								 'RAIPlanoID'],
								 ['99/99/9999 99:99','99/99/9999 99:99','','','','','',''],
                                 [dTFormat + ' hh:mm',dTFormat + ' hh:mm','','','','','','']);
                                 
		for (i=1; i<fg.Rows; i++)
		{
			currDSO.recordset.MoveFirst();
			var currID = fg.TextMatrix(i,fg.Cols-1);
        
			currDSO.recordset.find('RAIPlanoID', currID);
			
			if (!currDSO.recordset.EOF)
			{
				if (currDSO.recordset['ObsAbreviado'].value != null)
					fg.TextMatrix(i, getColIndexByColKey(fg, '_calc_ObsAbreviado_30*')) = currDSO.recordset['ObsAbreviado'].value;
				else
					fg.TextMatrix(i, getColIndexByColKey(fg, '_calc_ObsAbreviado_30*')) = '';
			}
			else
				fg.TextMatrix(i, getColIndexByColKey(fg, '_calc_ObsAbreviado_30*')) = '';
		}
                                 
		fg.FrozenCols = 1;
		alignColsInGrid(fg,[]);

        // Linha de Totais
        gridHasTotalLine(fg, 'Total', 0xC0C0C0, null, true, [[2,'####','S']]);

        // Atualiza o total de horas
        if (fg.Rows>2)
        {
            if (!((dso02GridLkp.recordset.BOF)&&(dso02GridLkp.recordset.EOF)))
            {
                dso02GridLkp.recordset.MoveFirst();
                fg.TextMatrix(1, 2) = dso02GridLkp.recordset['TempoTotal'].value;
            }       
        }
    }
    // LVA
    else if (folderID == 26154)
    {
        headerGrid(fg,['Norma',
                       'L',
                       'ID',
                       'Observa��o',
                       'Evid�ncia',
                       'Constata��o',
                       'Class',
                       'A��o',
                       'Eficaz',
                       'Observa��es',
                       'Observa��es',
                       'RAILVAID'], [10, 11]);

        fillGridMask(fg,currDSO,['^DocQuestionarioID^dso01GridLkp^DocQuestionarioID^Norma*',
								 '^DocQuestionarioID^dso01GridLkp^DocQuestionarioID^Letra*',
								 'DocQuestionarioID*',
								 'Observacao',
								 'Evidencia',
								 'Constatacao',
								 'ClassificacaoID',
								 'Acao',
								 'Eficaz',
								 '_calc_ObsAbreviado_30*',
								 'Observacoes',
								 'RAILVAID'],
								 ['', '', '', '', '', '', '', '', '', '', '', ''],
                                 ['', '', '', '', '', '', '', '', '', '', '', '']);

		for (i=1; i<fg.Rows; i++)
		{
			currDSO.recordset.MoveFirst();
			var currID = fg.TextMatrix(i,fg.Cols-1);
        
			currDSO.recordset.find('RAILVAID', currID);
			
			if (!currDSO.recordset.EOF)
			{
				if (currDSO.recordset['ObsAbreviado'].value != null)
					fg.TextMatrix(i, getColIndexByColKey(fg, '_calc_ObsAbreviado_30*')) = currDSO.recordset['ObsAbreviado'].value;
				else
					fg.TextMatrix(i, getColIndexByColKey(fg, '_calc_ObsAbreviado_30*')) = '';
			}
			else
				fg.TextMatrix(i, getColIndexByColKey(fg, '_calc_ObsAbreviado_30*')) = '';
		}

		alignColsInGrid(fg,[2]);
		fg.FrozenCols = 1;

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;

    }
    // Documentos Verificados
    else if (folderID == 26155)
    {
        headerGrid(fg,['ID',
					   'Est',
					   'Documento',
                       'Norma',
                       'Rev',
                       'Emiss�o',
                       'Aprova��o',
                       'RAIDocumentoID'], [7]);

        fillGridMask(fg,currDSO,['DocumentoID',
								 '^EstadoID^dso01GridLkp^RecursoID^RecursoAbreviado*',
								 'Documento',
								 '^DocumentoID^dso02GridLkp^DocumentoID^Norma*',
								 'Versao',
								 'dtEmissao',
								 '^ProprietarioID^dso03GridLkp^PessoaID^Fantasia*',
								 'RAIDocumentoID'],
								 ['','','','','','','',''],
                                 ['','','','','',dTFormat,'','']);
		
		alignColsInGrid(fg,[0, 4]);
		fg.FrozenCols = 1;

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
    // Documentos Relacionados
    else if (folderID == 26156)
    {
        headerGrid(fg,['Tipo',
					   'DocumentoID',
					   'Documento',
                       'ID',
                       'Est',
                       'Data',
                       'Vencimento',
                       'Conclus�o',
                       'TipoRelacionamentoID',
                       'DocRelacaoID'], [1, 8, 9]);

        fillGridMask(fg,currDSO,['TipoRelacionamento',
								 'DocumentoID',
								 'DocumentoAbreviado',
								 'RegistroID',
								 'Estado',
								 'dtEmissao',
								 'dtVencimento',
								 'dtConclusao',
								 'TipoRelacionamentoID',
								 'DocRelacaoID'],
								 ['','','','','','99/99/9999','99/99/9999','99/99/9999','',''],
                                 ['','','','','',dTFormat,dTFormat,dTFormat,'','']);
		
		alignColsInGrid(fg,[1, 3]);
		fg.FrozenCols = 1;

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
}

// FINAL DE FUNCOES DA AUTOMACAO ************************************
