using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using Overfly3.systemEx.serverside;
using java.lang;

namespace Overfly3.modqualidade.submodiso.rai.serverside
{
	public partial class gravamodalincluidocs : System.Web.UI.OverflyPage
	{
		protected override void PageLoad(object sender, EventArgs e)
		{
			DataInterfaceObj.ExecuteSQLCommand(SQL);
			
			WriteResultXML(DataInterfaceObj.getRemoteData(
				"select 0 as fldErrorNumber, 'Erro ao gravar.' as fldErrorText"
			));
		}
		
		protected Integer RAIID = Constants.INT_ZERO;
		protected Integer nRAIID
		{
			get { return RAIID; }
			set { if(value != null) RAIID = value; }
		}

		protected Integer[] docID = Constants.INT_EMPTY_SET;
		protected Integer[] nDocID
		{
			get { return docID; }
			set { if(value != null) docID = value; }
		}
		
		protected string SQL
		{
			get
			{
				string sql = "";
				
				for(int i = 0; i < docID.Length; i++)
				{
					sql += " INSERT INTO RAI_Documentos (RAIID, DocumentoID, EstadoID, Documento, Versao, dtEmissao, ProprietarioID) " +
						"SELECT " + RAIID + ", a.DocumentoID, dbo.fn_Documento_Estado(a.DocumentoID, 2), "+
                            "a.Documento, a.Versao, a.dtEmissao, a.ProprietarioID " +
                        "FROM Documentos a WITH(NOLOCK) " +
						"WHERE (a.DocumentoID = " + docID[i] + " AND " +
                            "(SELECT COUNT(*) FROM RAI_Documentos WITH(NOLOCK) WHERE (RAIID = " + RAIID + " AND " +
								"DocumentoID = " + docID[i] + ")) = 0) ";
				}
				
				return sql;
			}
		}
	}
}
