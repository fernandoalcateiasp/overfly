using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using Overfly3.systemEx.serverside;
using java.lang;

namespace Overfly3.modqualidade.submodiso.rai.serverside
{
	public partial class savedefinirescopo : System.Web.UI.OverflyPage
	{
		protected override void PageLoad(object sender, EventArgs e)
		{
			RAIEscopo();
			
			WriteResultXML(DataInterfaceObj.getRemoteData(
				" select top 0 0 as fldErrorNumber, NULL as fldErrorText "
			));
		}

		protected Integer RAIID = Constants.INT_ZERO;
		protected Integer nRAIID
		{
			get { return RAIID; }
			set { if (value != null) RAIID = value; }
		}

		protected string[] norma = Constants.STR_EMPTY_SET;
		protected string[] sNorma
		{
			get { return norma; }
			set { if (value != null) norma = value; }
		}

		protected Integer[] ok = Constants.INT_EMPTY_SET;
		protected Integer[] nOK
		{
			get { return ok; }
			set { if (value != null) ok = value; }
		}
		
		protected void RAIEscopo()
		{
			ProcedureParameters[] param = new ProcedureParameters[4];
			
			param[0] = new ProcedureParameters("@RAIID", SqlDbType.Int, RAIID.intValue());
			param[1] = new ProcedureParameters("@Norma", SqlDbType.VarChar, DBNull.Value);
			param[1].Length = 10;
			param[2] = new ProcedureParameters("@OK", SqlDbType.Int, DBNull.Value);
			param[3] = new ProcedureParameters("@Resultado", SqlDbType.VarChar, DBNull.Value, ParameterDirection.InputOutput);
			param[3].Length = 8000;

			for(int i = 0; i < ok.Length; i++)
			{
				param[1].Data = norma[i];
				param[2].Data = ok[i].intValue();
			
				DataInterfaceObj.execNonQueryProcedure("sp_RAI_Escopo", param);
			}
		}
	}
}
