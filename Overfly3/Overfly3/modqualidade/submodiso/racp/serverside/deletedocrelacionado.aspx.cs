using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;
using Overfly3.systemEx.serverside;

namespace Overfly3.modqualidade.submodiso.racp.serverside
{
	public partial class deletedocrelacionado : System.Web.UI.OverflyPage
	{
		protected override void PageLoad(object sender, EventArgs e)
		{
			
			int fldresp = DataInterfaceObj.ExecuteSQLCommand(
					"DELETE FROM documentosRelacionados WHERE DocRelacaoID =" + nDocRelacaoID);
					
			WriteResultXML(DataInterfaceObj.getRemoteData(
				"select " + fldresp + " as fldresp"
			));

		}
		
		protected Integer docRelacaoID = Constants.INT_ZERO;
		protected Integer nDocRelacaoID
		{
			get { return docRelacaoID; }
			set { if(value != null) docRelacaoID = value; }
		}
	}
}
