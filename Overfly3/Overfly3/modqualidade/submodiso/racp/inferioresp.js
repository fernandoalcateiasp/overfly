/********************************************************************
inferioresp.js

Library javascript para o inferior.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_nFormID = window.top.formID;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

FUNCOES DA AUTOMACAO:	
    setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
	setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
	fillCmbPastasInCtlBarInf(nTipoRegistroID)

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

// FUNCOES DA AUTOMACAO *********************************************

/********************************************************************
Funcao disparada pelo frame work.
Chamada pela funcao filtroComplete_DSC(emptyFilter) do js_detailinf.js

Esta funcao seta o sql do dso para buscar o registro a ser mostrado no detalhe,
vai ao banco de dados no servidor e no retorno chama a funcao de
ondatasetcomplete de acordo com o parametro recebido.
           
Parametros: 
dso                        -> dso a ser aberto
folderID                   -> id da pasta corrente no control bar inferior
idToFind                   -> id do registro corrente no sup
sFiltro                    -> string do filtro corrente no control bar inferior
nTipoRegistroID            -> tipo do registro corrente
Retorno:
nulo
********************************************************************/
function setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
{
    var nEmpresaData = getCurrEmpresaData();

    setConnection(dso);
    
    //@@
    if ( (folderID == 20008) || (folderID == 20009) ) // Prop/Altern ou Observacoes
    {
        dso.SQL = 'SELECT a.ProprietarioID,a.AlternativoID,a.Observacoes FROM RACP a WITH(NOLOCK) WHERE '+
        sFiltro + 'a.RACPID = ' + idToFind;
        return 'dso01JoinSup_DSC';
    }
    // Texto
    else if (folderID == 26181)
    {
        dso.SQL = 'SELECT a.ProprietarioID,a.AlternativoID,a.Texto FROM RACP a WITH(NOLOCK)  WHERE ' +
        sFiltro + 'a.RACPID = ' + idToFind;
        return 'dso01JoinSup_DSC';
    }
    // Grupo Trabalho
    else if (folderID == 26182)
    {
        dso.SQL = 'SELECT a.* FROM RACP_Grupos a WITH(NOLOCK) WHERE ' +
        sFiltro + 'a.RACPID = ' + idToFind+ ' ' +
        'ORDER BY (SELECT Fantasia FROM Pessoas WITH(NOLOCK) WHERE PessoaID=a.PessoaID)';
        return 'dso01Grid_DSC';
    }
    // Documentos Relacionados
    else if (folderID == 26183)
    {
        dso.SQL = 'EXEC sp_DocumentosRelacionados 852001, ' + idToFind;
        return 'dso01Grid_DSC';
    }
    // Plano Acao
    else if (folderID == 26184)
    {
        dso.SQL = 'SELECT a.*, LEFT(CONVERT(VARCHAR(8000),Observacoes),30) + (CASE CONVERT(VARCHAR(8000),Observacoes) WHEN SPACE(0) THEN ' +
        'SPACE(0) ELSE ' + '\'' + '...' + '\'' + 'END) AS ObsAbreviado FROM RACP_PlanosAcao a WITH(NOLOCK) WHERE ' +
			sFiltro + 'a.RACPID = ' + idToFind+ ' ' +
			'ORDER BY a.dtVencimento, (SELECT Fantasia FROM Pessoas WITH(NOLOCK) WHERE PessoaID=a.PessoaID)';
        return 'dso01Grid_DSC';
    }
    // LOG
    else if (folderID == 20010)
    {
        dso.SQL = 'SELECT *, dbo.fn_Data_Fuso(a.Data, NULL, ' + nEmpresaData[2] + ') AS DataFuso ' +
                  'FROM LOGs a WITH(NOLOCK) ' + 
                  'WHERE ' + sFiltro + ' a.RegistroID = '+ idToFind + ' ' +
                  'AND a.FormID=' + glb_nFormID + ' ' +
                  'ORDER BY a.SubFormID ASC, a.Data DESC';
        return 'dso01Grid_DSC';
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pela funcao dso01Grid_DSC() do js_detailinf.js

Monta string de select para combo de grid

Parametros:
dso                 - referencia ao dso corrente
pastaID             - pasta corrente
nRegistroID         - id do registro corrente
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
{
    setConnection(dso);
    
    // LOG
    if (pastaID == 20010)
    {
        if ( dso.id == 'dso01GridLkp' )
        {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoFantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.SubFormID = b.RecursoID)';
        }
        else if ( dso.id == 'dso02GridLkp' )
        {
            dso.SQL = 'SELECT b.PessoaID, b.Fantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Pessoas b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.UsuarioID = b.PessoaID)';
        }
        else if ( dso.id == 'dso03GridLkp' )
        {
            dso.SQL = 'SELECT b.ItemID, b.ItemAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), TiposAuxiliares_Itens b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EventoID = b.ItemID)';
        }
        else if ( dso.id == 'dso04GridLkp' )
        {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EstadoID = b.RecursoID)';
        }
    }
    // Grupos Trabalho
    else if (pastaID == 26182)
    {
        var nEmpresaData = getCurrEmpresaData();
        var nEmpresaID = nEmpresaData[0]; 

        if ( dso.id == 'dsoCmb01Grid_01' )
        {
            dso.SQL = 'SELECT DISTINCT c.PessoaID, c.Fantasia ' +
				'FROM RelacoesPesRec_Perfis a WITH(NOLOCK), RelacoesPesRec b WITH(NOLOCK), Pessoas c WITH(NOLOCK) ' +
				'WHERE (a.EmpresaID = ' + nEmpresaID + ' AND a.OK = 1 AND a.RelacaoID = b.RelacaoID AND ' +
					'b.EstadoID = 2 AND b.SujeitoID = c.PessoaID AND c.EstadoID = 2) ' +
				'ORDER BY c.Fantasia';
        }
    }
    // Plano Acao
    else if (pastaID == 26184)
    {
        var nEmpresaData = getCurrEmpresaData();
        var nEmpresaID = nEmpresaData[0]; 

        if ( dso.id == 'dsoCmb01Grid_01' )
        {
            dso.SQL = 'SELECT DISTINCT c.PessoaID, c.Fantasia ' +
				'FROM RelacoesPesRec_Perfis a WITH(NOLOCK), RelacoesPesRec b WITH(NOLOCK), Pessoas c WITH(NOLOCK) ' +
				'WHERE (a.EmpresaID = ' + nEmpresaID + ' AND a.OK = 1 AND a.RelacaoID = b.RelacaoID AND ' +
					'b.EstadoID = 2 AND b.SujeitoID = c.PessoaID AND c.EstadoID = 2) ' +
				'ORDER BY c.Fantasia';
        }
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada em diversos pontos do js_detailinf.js

Esta funcao preenche o combo de pastas do controlbar inferior

Parametros:
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function fillCmbPastasInCtlBarInf(nTipoRegistroID)
{
    var i=0;
    var currSelection = getCmbCurrDataInControlBar('inf', 1);
    var vPastaName;

	cleanupSelInControlBar('inf',1);

	vPastaName = window.top.getPastaName(20008);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20008); //Observa��es

    vPastaName = window.top.getPastaName(20009);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20009); //Proprietario

    vPastaName = window.top.getPastaName(20010);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20010); //LOG

    vPastaName = window.top.getPastaName(26181);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 26181); //Texto

    vPastaName = window.top.getPastaName(26182);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 26182); //Grupo Trabalho

    vPastaName = window.top.getPastaName(26183);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 26183); //Documentos Relacionados

    vPastaName = window.top.getPastaName(26184);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 26184); //Plano Acao

    // Nao mexer - Inicio de automacao ============================== 	
	// Tenta selecionar novamente a pasta anterior selecionada no combo
	// Se nao mais existe a pasta anteriormente selecionada no combo
	// seleciona a pasta default
	
	stripFoldersByRight(currSelection);
        
    return true;
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada em diversos pontos do js_detailinf.js

Tratar consistencia dos dados inseridos pelo usuario aqui.
           
Parametros: 
btnClicked      - controlbar e botao clicado pelo usuario
folderID        - id da pasta selecionada
tipoCtlrsPasta  - 'GRID' a pasta e um grid/ 'CTRLS' a pasta nao e
                  um grid
currDSO         - referencia dso corrente
registroID      - id do registro corrente no sup

Retorno:
true se os dados estao ok, caso contrario false.
********************************************************************/
function criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)
{
    //@@ return true se tudo ok, caso contrario return false;
 
    var currLine = 0;

    // A pasta e grid    
    if (tipoCtlrsPasta == 'GRID') // se � grid
    {
        if (folderID == 26182) // Grupo Trabalho
            currLine = criticLineGridAndFillDsoEx(fg, currDSO,keepCurrLineInGrid(),[0], ['RACPID', registroID]);

        if (folderID == 26184) // Plano Acao
            currLine = criticLineGridAndFillDsoEx(fg, currDSO,keepCurrLineInGrid(),[1, 2, 3, 4, 5], ['RACPID', registroID], [9]);

        if (currLine < 0)
            return false;
    }   
    
    return true;
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pela funcao buildGridFromFolder(folderID) do js_detailinf.js

Constroi o corpo de um grid. Os combos sao construidos pelo frame work
           
Parametros: 
folderID        - id da pasta corrente
currDSO         - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function constructGridBody(folderID, currDSO)
{
    var dTFormat = '';
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';
    
    fg.FrozenCols = 0;
    
    // LOG
    if (folderID == 20010)
    {
        headerGrid(fg,['SubForm',
                       'Data           Hora     ',
                       'Colaborador',
                       'Evento',
                       'Est',
                       'Motivo',
                       'LOGID'], [6]);
                       
        fillGridMask(fg,currDSO,['^SubFormID^dso01GridLkp^RecursoID^RecursoFantasia',
                                 'DataFuso',
                                 '^UsuarioID^dso02GridLkp^PessoaID^Fantasia',
                                 '^EventoID^dso03GridLkp^ItemID^ItemAbreviado',
                                 '^EstadoID^dso04GridLkp^RecursoID^RecursoAbreviado',
                                 'Motivo',
                                 'LOGID'],
                                 ['','99/99/9999','','','','',''],
                                 ['',dTFormat + ' hh:mm:ss','','','','','']);

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
    // Grupos Trabalho
    else if (folderID == 26182)
    {
        headerGrid(fg,['Colaborador',
                       'RACPPessoaID'], [1]);

        fillGridMask(fg,currDSO,['PessoaID',
								 'RACPPessoaID'],
								 ['',''],
                                 ['','']);

		fg.FrozenCols = 1;
    }
    // Documentos Relacionados
    else if (folderID == 26183)
    {
        headerGrid(fg,['Tipo',
					   'DocumentoID',
					   'Documento',
                       'ID',
                       'Est',
                       'Data',
                       'Vencimento',
                       'Conclus�o',
                       'TipoRelacionamentoID',
                       'DocRelacaoID'], [1, 8, 9]);

        fillGridMask(fg,currDSO,['TipoRelacionamento',
								 'DocumentoID',
								 'DocumentoAbreviado',
								 'RegistroID',
								 'Estado',
								 'dtEmissao',
								 'dtVencimento',
								 'dtConclusao',
								 'TipoRelacionamentoID',
								 'DocRelacaoID'],
								 ['','','','','','99/99/9999','99/99/9999','99/99/9999','',''],
                                 ['','','','','',dTFormat,dTFormat,dTFormat,'','']);
		
		alignColsInGrid(fg,[1, 3]);
		fg.FrozenCols = 1;

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
    // Planos de Acao
    else if (folderID == 26184)
    {
        headerGrid(fg,['ID',
					   'A��o',
					   'Vencimento',
					   'Respons�vel',
					   'Abrang�ncia',
					   'Resultado Esperado',
					   'Conclus�o',
					   'Eficaz',
					   'Observa��es',
					   'Observa��es',
                       'RACPPlanoID'], [9, 10]);

        fillGridMask(fg,currDSO,['RACPPlanoID*',
								 'Acao',
								 'dtVencimento',
								 'PessoaID',
								 'Abrangencia',
								 'ResultadoEsperado',
								 'dtConclusao',
								 'Eficaz',
								 '_calc_ObsAbreviado_30*',
								 'Observacoes',
								 'RACPPlanoID'],
								 ['','','99/99/9999', '', '', '', '99/99/9999', '', '', '',''],
								 ['','',dTFormat, '', '', '', dTFormat, '', '', '','']);

		for (i=1; i<fg.Rows; i++)
		{
			currDSO.recordset.MoveFirst();
			var currID = fg.TextMatrix(i,fg.Cols-1);
        
			currDSO.recordset.find('RACPPlanoID', currID);
			
			if (!currDSO.recordset.EOF)
			{
				if (currDSO.recordset['ObsAbreviado'].value != null)
					fg.TextMatrix(i, getColIndexByColKey(fg, '_calc_ObsAbreviado_30*')) = currDSO.recordset['ObsAbreviado'].value;
				else
					fg.TextMatrix(i, getColIndexByColKey(fg, '_calc_ObsAbreviado_30*')) = '';
			}
			else
				fg.TextMatrix(i, getColIndexByColKey(fg, '_calc_ObsAbreviado_30*')) = '';
		}
		
		alignColsInGrid(fg,[0]);
		fg.FrozenCols = 1;
    }
}

// FINAL DE FUNCOES DA AUTOMACAO ************************************
