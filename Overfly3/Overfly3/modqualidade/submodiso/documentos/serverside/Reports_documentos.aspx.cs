﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using System.Data;
using System.Configuration;
using OVERFLYSVRCFGLib;
using System.Data.SqlClient;
using System.IO;
using System.Diagnostics;
using WSData;

namespace Overfly3.PrintJet
{
    public partial class Reports_documentos : System.Web.UI.Page
    {
        ClsReport Relatorio = new ClsReport();

        public string[,] arrSqlQuery;
        public string[,] arrIndexKey;
        private int nEmpresaID = Convert.ToInt32(HttpContext.Current.Request.Params["glb_nEmpresaID"]);
        private string sEmpresaFantasia = Convert.ToString(HttpContext.Current.Request.Params["glb_sEmpresaFantasia"]);
        private string nEmpresaLogadaIdioma = Convert.ToString(HttpContext.Current.Request.Params["nEmpresaLogadaIdioma"]);
        protected dataInterface DataInterfaceObj = new dataInterface(System.Configuration.ConfigurationManager.AppSettings["application"]);
        private int formato = Convert.ToInt32(HttpContext.Current.Request.Params["Formato"]);
        int Datateste = 0;
        bool nulo = false;

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                string sRelatorioID = Request.Params["RelatorioID"].ToString();

                switch (sRelatorioID) {
                    case "40173":
                        relatorioLVA();
                        break;
                }
            }
        }

        public void relatorioLVA()
        {
            bool bNaoConformidade = Convert.ToBoolean(HttpContext.Current.Request.Params["bNaoConformidade"]);
            string nRAIID = Convert.ToString(HttpContext.Current.Request.Params["nRAIID"]);


            string sFiltro = "";

            if (bNaoConformidade)
                sFiltro = " AND RAI_LVA.ClassificacaoID IN (817, 818, 819) ";

            string Title = "";

            if (nRAIID == "null")
                Title = "LVA - Lista de Verificação de Auditoria #822001";
            else
                Title = "LVA - Lista de Verificação de Auditoria RAI " + nRAIID;


            string strSQL1 = "";
            string strSQL2 = "";


            if (nRAIID == "null")
            {
                strSQL1 = " SELECT DISTINCT " +
                                " Norma.ItemID AS NormaID_, NULL AS[ID], Norma.ItemAbreviado AS 'Item', SPACE(0) AS Ver, Norma.ItemMasculino AS 'Questão', Norma.Ordem AS Ordem_ " +
                            " FROM " +
                                " Documentos_Questionarios Questionario WITH(NOLOCK) " +
                                    " INNER JOIN TiposAuxiliares_Itens Norma WITH(NOLOCK) ON(Norma.ItemID = Questionario.ItemNormaID) " +
                            " WHERE " +
                                " (Questionario.DocumentoID = 822001 AND Norma.EstadoID = 2) " +
                            " ORDER BY Ordem_ ";

                strSQL2 = " SELECT " +
                                " Norma.ItemID AS NormaID_, Questionario.DocQuestionarioID AS[ID], Questionario.Letra AS Item, " +
                                    " (SELECT b.ItemAbreviado + ISNULL(SPACE(1) + a.Letra, SPACE(0)) " +
                                    " FROM Documentos_Questionarios a WITH(NOLOCK)INNER JOIN TiposAuxiliares_Itens b WITH(NOLOCK) ON (b.ItemID = a.ItemNormaID) " +
                                    " WHERE (Questionario.ReferenciaID = a.DocQuestionarioID)) AS Ver, " +
                                " SUBSTRING(Questionario.Pergunta, 1, 50) +CHAR(13) + CHAR(10) + SUBSTRING(Questionario.Pergunta, 51, 50) AS [Questao_], " +
                                " Questionario.Pergunta AS [Questão], NULL AS[Observação], NULL AS[Evidência], NULL AS[Constatação], NULL AS Class " +
                            " FROM " +
                                " Documentos_Questionarios Questionario WITH(NOLOCK) " +
                                    " INNER JOIN TiposAuxiliares_Itens Norma WITH(NOLOCK) ON (Norma.ItemID = Questionario.ItemNormaID) " +
                            " WHERE (Questionario.DocumentoID = 822001 AND Norma.EstadoID = 2) " +
                            " ORDER BY Norma.Ordem, Questionario.Ordem ";
            }
            else
            {
                strSQL1 = " SELECT DISTINCT " +
                                " Norma.ItemID AS NormaID_, NULL AS [ID], Norma.ItemAbreviado AS 'Item', SPACE(0) AS Ver, Norma.ItemMasculino AS 'Questão', Norma.Ordem AS Ordem_ " +
                            " FROM " +
                                " RAI_LVA WITH(NOLOCK) " +
                                    " INNER JOIN Documentos_Questionarios Questionario WITH(NOLOCK) ON(Questionario.DocQuestionarioID = RAI_LVA.DocQuestionarioID) " +
                                    " INNER JOIN TiposAuxiliares_Itens Norma WITH(NOLOCK) ON(Norma.ItemID = Questionario.ItemNormaID) " +
                            " WHERE (RAI_LVA.RAIID = " + nRAIID + " AND Questionario.DocumentoID = 822001 AND Norma.EstadoID = 2) " +
                                sFiltro +
                            " ORDER BY Ordem_ ";

                strSQL2 = " SELECT " +
                                " Norma.ItemID AS NormaID_, Questionario.DocQuestionarioID AS [ID], Questionario.Letra AS Item, " +
                                    " (SELECT b.ItemAbreviado + ISNULL(SPACE(1) + a.Letra, SPACE(0)) " +
                                    " FROM Documentos_Questionarios a WITH(NOLOCK) INNER JOIN TiposAuxiliares_Itens b WITH(NOLOCK) ON (b.ItemID = a.ItemNormaID) " +
                                    " WHERE(Questionario.ReferenciaID = a.DocQuestionarioID)) AS Ver, " +
                                " SUBSTRING(Questionario.Pergunta, 1, 50) +CHAR(13) + CHAR(10) + SUBSTRING(Questionario.Pergunta, 51, 50) AS [Questao_], " +
                                " Questionario.Pergunta AS [Questão], " +
                                " SUBSTRING(RAI_LVA.Observacao, 1, 30) +CHAR(13) + CHAR(10) + SUBSTRING(RAI_LVA.Observacao, 31, 60) AS [Observação_], " +
                                " RAI_LVA.Observacao AS [Observação], " +
                                " SUBSTRING(RAI_LVA.Evidencia, 1, 30) +CHAR(13) + CHAR(10) + SUBSTRING(RAI_LVA.Evidencia, 31, 60) AS [Evidência_], " +
                                " RAI_LVA.Evidencia AS [Evidência], " +
                                " SUBSTRING(RAI_LVA.Constatacao, 1, 30) +CHAR(13) + CHAR(10) + SUBSTRING(RAI_LVA.Constatacao, 31, 60) AS [Constatação_], " +
                                " RAI_LVA.Constatacao AS [Constatação], " +
                                " Class.ItemAbreviado AS Class " +
                            " FROM " +
                                " RAI_LVA WITH(NOLOCK) " +
                                    " LEFT OUTER JOIN TiposAuxiliares_Itens Class WITH(NOLOCK) ON(RAI_LVA.ClassificacaoID = Class.ItemID) " +
                                    " INNER JOIN Documentos_Questionarios Questionario WITH(NOLOCK) ON(RAI_LVA.DocQuestionarioID = Questionario.DocQuestionarioID) " +
                                    " INNER JOIN TiposAuxiliares_Itens Norma WITH(NOLOCK) ON(Questionario.ItemNormaID = Norma.ItemID) " +
                            " WHERE (RAI_LVA.RAIID = " + nRAIID + " AND Questionario.DocumentoID = 822001 AND Norma.EstadoID = 2) " +
                                sFiltro +
                            " ORDER BY Norma.Ordem, Questionario.Ordem ";
            }


            arrSqlQuery = new string[,] { { "strSQL1", strSQL1 },
                                          { "strSQL2", strSQL2 } };

            string[,] arrIndexKey = new string[,] { { "strSQL2", "NormaID_", "NormaID_" } };


            if (formato == 1)
                Relatorio.CriarRelatório(Title, arrSqlQuery, "BRA", "Landscape", "1.2", "Default", "Default", true);
            else if(formato == 2)
                Relatorio.CriarRelatório(Title, arrSqlQuery, "BRA", "Landscape");


            DataSet dsFin = new DataSet();
            dsFin = (DataSet)HttpContext.Current.Session["Query"];

            //Verifica se a Query está vazia e retorna mensagem de erro
            Datateste = dsFin.Tables.Count;

            int linhas = dsFin.Tables["strSQL1"].Rows.Count;

            if (linhas == 0)
            {
                nulo = true;
            }

            if (nulo)
            {
                HttpContext.Current.Response.Write("<script type=\"text/javascript\"> alert('Não há dados a serem impressos.');window.parent.pb_StopProgressBar(true);window.parent.lockControlsInModalWin(false);</script>");
                HttpContext.Current.Response.End();
                return;
            }
            else
            {
                if (formato == 1) //SE PDF
                {
                    double headerX = 0.2;
                    double headerY = 0.2;
                    double tabelaTitulo = 0.8;

                    double tabelaX = 0.2;
                    double tabelaY = 0;

                    double bodyX = 0.2;
                    double bodyY = 0.0;

                    //Header
                    Relatorio.CriarObjLabel("Fixo", "", sEmpresaFantasia, (headerX).ToString(), headerY.ToString(), "12", "black", "B", "Header", "5", "");
                    Relatorio.CriarObjLabel("Fixo", "", Title, (headerX + 8).ToString(), headerY.ToString(), "12", "#0113a0", "B", "Header", "15", "");
                    Relatorio.CriarObjLabelData("Now", "", "", (headerX + 24).ToString(), headerY.ToString(), "10", "black", "B", "Header", "3.72187", "Horas");
                    Relatorio.CriarNumeroPagina((headerX + 27).ToString(), (headerY).ToString(), "10", "Black", "B", false);

                    //Cabeçalho da tabela
                    Relatorio.CriarObjLabel("Fixo", "", "ID", (headerX).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "1.5", "");
                    Relatorio.CriarObjLabel("Fixo", "", "Item", (headerX + 1.5).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "1.5", "");
                    Relatorio.CriarObjLabel("Fixo", "", "Ver", (headerX + 3).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "1.5", "");
                    Relatorio.CriarObjLabel("Fixo", "", "Questão", (headerX + 4.5).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "8", "");
                    Relatorio.CriarObjLabel("Fixo", "", "Observação", (headerX + 12.5).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "5", "");
                    Relatorio.CriarObjLabel("Fixo", "", "Evidência", (headerX + 17.5).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "5", "");
                    Relatorio.CriarObjLabel("Fixo", "", "Constatação", (headerX + 22.5).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "5", "");
                    Relatorio.CriarObjLabel("Fixo", "", "Class", (headerX + 27.5).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "1.5", "");


                    //Tabela
                    Relatorio.CriarObjTabela(tabelaX.ToString(), tabelaY.ToString(), arrIndexKey, false, true);

                    Relatorio.CriarObjColunaNaTabela(" ", "ID", false, "1.5", "Default","1","0");
                    Relatorio.CriarObjCelulaInColuna("Query", "ID", "DetailGroup","Null","Null",0,false,"0","",true,"6");

                    Relatorio.CriarObjColunaNaTabela(" ", "Item", false, "1.5", "Default", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Item", "DetailGroup", "Null", "Null", 0, false, "0", "", true, "6");

                    Relatorio.CriarObjColunaNaTabela(" ", "Ver", false, "1.5", "Default", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Ver", "DetailGroup", "Null", "Null", 0, false, "0", "", true, "6");

                    Relatorio.CriarObjColunaNaTabela(" ", "Questão", false, "8", "Default", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Questão", "DetailGroup", "Null", "Null", 0, false, "0", "", true, "6");

                    Relatorio.CriarObjColunaNaTabela(" ", "Observação", false, "5", "Default", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Observação", "DetailGroup", "Null", "Null", 0, false, "0", "", true, "6");

                    Relatorio.CriarObjColunaNaTabela(" ", "Evidência", false, "5", "Default", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Evidência", "DetailGroup", "Null", "Null", 0, false, "0", "", true, "6");

                    Relatorio.CriarObjColunaNaTabela(" ", "Constatação", false, "5", "Default", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Constatação", "DetailGroup", "Null", "Null", 0, false, "0", "", true, "6");

                    Relatorio.CriarObjColunaNaTabela(" ", "Class", false, "1.5", "Default", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Class", "DetailGroup", "Null", "Null", 0, false, "0", "", true, "6");

                    Relatorio.TabelaEnd();
                }
                else if (formato == 2) // Se Excel
                {
                    double headerX = 0;
                    double headerY = 0;

                    double tabelaX = 0;
                    double tabelaY = 0.3;

                    //Header
                    Relatorio.CriarObjLabel("Fixo", "", sEmpresaFantasia, (headerX).ToString(), headerY.ToString(), "8", "black", "B", "Body", "3.5", "");
                    Relatorio.CriarObjLabel("Fixo", "", Title, (headerX + 6.5).ToString(), headerY.ToString(), "8", "#0113a0", "B", "Body", "8", "");
                    Relatorio.CriarObjLabelData("Now", "", "", (headerX + 19.5).ToString(), headerY.ToString(), "8", "black", "B", "Body", "5", "Horas");

                    Relatorio.CriarObjTabela(tabelaX.ToString(), tabelaY.ToString(), arrIndexKey, false, true);

                    Relatorio.CriarObjColunaNaTabela("ID", "ID", false, "3.5", "Default");
                    Relatorio.CriarObjCelulaInColuna("Query", "ID", "DetailGroup", "Null", "Null", 0, false, "0", "", true, "6");

                    Relatorio.CriarObjColunaNaTabela("Item", "Item", false, "1.5", "Default");
                    Relatorio.CriarObjCelulaInColuna("Query", "Item", "DetailGroup", "Null", "Null", 0, false, "0", "", true, "6");

                    Relatorio.CriarObjColunaNaTabela("Ver", "Ver", false, "1.5", "Default");
                    Relatorio.CriarObjCelulaInColuna("Query", "Ver", "DetailGroup", "Null", "Null", 0, false, "0", "", true, "6");

                    Relatorio.CriarObjColunaNaTabela("Questão", "Questão", false, "8", "Default");
                    Relatorio.CriarObjCelulaInColuna("Query", "Questão", "DetailGroup", "Null", "Null", 0, false, "0", "", true, "6");

                    Relatorio.CriarObjColunaNaTabela("Observação", "Observação", false, "5", "Default");
                    Relatorio.CriarObjCelulaInColuna("Query", "Observação", "DetailGroup", "Null", "Null", 0, false, "0", "", true, "6");

                    Relatorio.CriarObjColunaNaTabela("Evidência", "Evidência", false, "5", "Default");
                    Relatorio.CriarObjCelulaInColuna("Query", "Evidência", "DetailGroup", "Null", "Null", 0, false, "0", "", true, "6");

                    Relatorio.CriarObjColunaNaTabela("Constatação", "Constatação", false, "5", "Default");
                    Relatorio.CriarObjCelulaInColuna("Query", "Constatação", "DetailGroup", "Null", "Null", 0, false, "0", "", true, "6");

                    Relatorio.CriarObjColunaNaTabela("Class", "Class", false, "1.5", "Default");
                    Relatorio.CriarObjCelulaInColuna("Query", "Class", "DetailGroup", "Null", "Null", 0, false, "0", "", true, "6");

                    Relatorio.TabelaEnd();
                }
            }
            Relatorio.CriarPDF_Excel(Title, formato);
        }
    }
}
