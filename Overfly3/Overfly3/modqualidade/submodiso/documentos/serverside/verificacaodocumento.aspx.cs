using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Overfly3.systemEx.serverside;
using java.lang;

namespace Overfly3.modqualidade.submodiso.documentos.serverside
{
	public partial class verificacaodocumento : System.Web.UI.OverflyPage
	{
		protected override void PageLoad(object sender, EventArgs e)
		{
			WriteResultXML(DataInterfaceObj.getRemoteData(
				"SELECT dbo.fn_Documento_Direitos( " + documentoID + ", " +
					usuarioID + ", GETDATE(), " +
					tipoResultado + ") AS Verificacao"
			));
		}

		protected Integer documentoID = Constants.INT_ZERO;
		protected Integer nDocumentoID
		{
			get { return documentoID; }
			set { if (value != null) documentoID = value; }
		}

		protected Integer usuarioID = Constants.INT_ZERO;
		protected Integer nUsuarioID
		{
			get { return usuarioID; }
			set { if (value != null) usuarioID = value; }
		}

		protected Integer tipoResultado = Constants.INT_ZERO;
		protected Integer nTipoResultado
		{
			get { return tipoResultado; }
			set { if (value != null) tipoResultado = value; }
		}
	}
}
