using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using Overfly3.systemEx.serverside;
using java.lang;

namespace Overfly3.modqualidade.submodiso.documentos.serverside
{
	public partial class cancelestudo : System.Web.UI.OverflyPage
	{
		protected override void PageLoad(object sender, EventArgs e)
		{
			WriteResultXML(DocumentosCancelarEstudo());
		}
		
		protected Integer documentoID = Constants.INT_ZERO;
		protected Integer nDocumentoID
		{
			get { return documentoID; }
			set { if (value != null) documentoID = value; }
		}

		protected Integer usuarioID = Constants.INT_ZERO;
		protected Integer nUsuarioID
		{
			get { return usuarioID; }
			set { if (value != null) usuarioID = value; }
		}
		
		protected DataSet DocumentosCancelarEstudo()
		{
			ProcedureParameters[] param = new ProcedureParameters[3];
			
			param[0] = new ProcedureParameters("@DocumentoID", SqlDbType.Int, documentoID.intValue());
			param[1] = new ProcedureParameters("@UsuarioID", SqlDbType.Int, usuarioID.intValue());
			param[2] = new ProcedureParameters("@Resultado", SqlDbType.VarChar, System.DBNull.Value);
			
			DataInterfaceObj.execNonQueryProcedure("sp_Documentos_CancelarEstudo", param);

			return param[2].Data != DBNull.Value ?
				DataInterfaceObj.getRemoteData("select '" + (string) param[2].Data + "' as Resultado") :
				DataInterfaceObj.getRemoteData("select '' as Resultado");
		}
	}
}
