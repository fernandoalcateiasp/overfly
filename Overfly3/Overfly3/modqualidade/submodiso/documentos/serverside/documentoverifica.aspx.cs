using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;
using Overfly3.systemEx.serverside;

namespace Overfly3.modqualidade.submodiso.documentos.serverside
{
	public partial class documentoverifica : System.Web.UI.OverflyPage
	{
		protected Integer documentoID = Constants.INT_ZERO;
		protected Integer nDocumentoID
		{
			get { return documentoID; }
			set { if (value != null) documentoID = value; }
		}

        private Integer estadoDeID = Constants.INT_ZERO;
        protected Integer nEstadoDeID
        {
            get { return estadoDeID; }
            set { estadoDeID = value; }
        }

        private Integer estadoParaID = Constants.INT_ZERO;
        protected Integer nEstadoParaID
        {
            get { return estadoParaID; }
            set { estadoParaID = value; }
        }

        private Integer usuarioID = Constants.INT_ZERO;
        protected Integer nUsuarioID
        {
            get { return usuarioID; }
            set { usuarioID = value; }
        }

        private int resultado = 0;
        protected int Resultado
        {
            get { return resultado; }
            set { resultado = value; }
        }

        private string mensagem = Constants.STR_EMPTY;
        protected string Mensagem
        {
            get { return mensagem; }
            set { mensagem = value; }
        }

        protected void DocumentoVerifica()
        {
            ProcedureParameters[] param = new ProcedureParameters[] {
				new ProcedureParameters("@DocumentoID",SqlDbType.Int,nDocumentoID.intValue()),
				new ProcedureParameters("@EstadoDeID",SqlDbType.Int,nEstadoDeID.intValue()),
				new ProcedureParameters("@EstadoParaID",SqlDbType.Int,nEstadoParaID.intValue()),
				new ProcedureParameters("@UsuarioID",SqlDbType.Int,nUsuarioID.intValue()),
				new ProcedureParameters("@Resultado",SqlDbType.Int,DBNull.Value, ParameterDirection.InputOutput),
				new ProcedureParameters("@Mensagem",SqlDbType.VarChar,DBNull.Value, ParameterDirection.InputOutput,8000)
			};

            DataInterfaceObj.execNonQueryProcedure("sp_Documento_Verifica", param);

            Resultado = (param[4].Data != DBNull.Value) ? (int)param[4].Data : 0;
            Mensagem = (param[5].Data != DBNull.Value) ? (string)param[5].Data : "";
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            DocumentoVerifica();

            WriteResultXML(DataInterfaceObj.getRemoteData(
                "select " + Resultado + " as Resultado, '" + Mensagem + "' as Mensagem"
            ));
        }
	}
}
