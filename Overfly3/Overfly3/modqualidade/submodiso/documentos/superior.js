/********************************************************************
superior.js

Library javascript para o superior.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_TimerDocumentoVar = null;

// Dados do registro corrente .SQL 
var dsoSup01 = new CDatatransport("dsoSup01");
// Descricao do estado atual do registro .SQL 
var dsoStateMachine = new CDatatransport("dsoStateMachine");
// Dados dos combos estaticos .URL 
var dsoEstaticCmbs = new CDatatransport("dsoEstaticCmbs");
// Busca data corrente no servidor .URL 
var dsoCurrData = new CDatatransport("dsoCurrData");
// //@@ Os dsos sao definidos de acordo com o form 
var dsoVerificacao = new CDatatransport("dsoVerificacao");
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

FUNCOES DA AUTOMACAO:
    putSpecialAttributesInControls()
    prgServerSup(btnClicked)
    prgInterfaceSup(btnClicked)
    optChangedInCmb(cmb)
    btnLupaClicked(btnClicked)
    finalOfSupCascade(btnClicked)
    btnBarClicked(controlBar, btnClicked)
    btnBarNotEspecClicked(controlBar, btnClicked)
    modalInformForm(idElement, param1, param2)
    supInitEditMode()
    formFinishLoad()

FUNCOES DA MAQUINA DE ESTADO:
    stateMachOpened( currEstadoID )
    stateMachBtnOK( currEstadoID, newEstadoID )
    stateMachClosed( oldEstadoID, currEstadoID, bSavedID )
    
FUNCOES ESPECIFICAS DO FORM:    

FUNCOES DO CARRIER:
    carrierArrived(idElement, idBrowser, param1, param2)
    carrierReturning(idElement, idBrowser, param1, param2)

********************************************************************/

/********************************************************************

// IMPLEMENTACAO DAS FUNCOES

//@@ todas as funcoes abaixo sao particulares de cada form
// conforme o form algumas funcoes devem ser removidas
// e outras serao acrescentadas
********************************************************************/

function window_onload()
{
    //@@ Iniciado carregamento de form, primeira funcao controladora
    initInterfaceLoad(window.top.formName, 0X7);
    
    //@@ array bidimensional de combos ids e campo indice na tabela
    // identificador do combo. O array deve ser null se nao tem combos estaticos

    glb_aStaticCombos = ([['selTipoDocumentoID', '1'],
                          ['selItemNormaID', '2'],
                          ['selFormContextoID', '3']]);

    windowOnLoad_1stPart();
        
    //@@ Path dos arquivos inf e pesqlist
    glb_aFilePath = new Array(SYS_PAGESURLROOT + '/modqualidade/submodiso/documentos/inferior.asp',
                              SYS_PAGESURLROOT + '/modqualidade/submodiso/documentos/pesquisa.asp');
        
    //@@ Linka os divs com os correspondentes subforms
    // criando o atributo sfsGrupo nos divs.
    // Os infs nao tem div principal e todos os divs sao secundarios.
    // Parametros:
    // 1o par: id do div principal ou null se nao tem div principal
    // 2opar: array de divs ids dos divs secundarios
    // ou null se nao tem divs secundarios
    // 3o par: array de xxx_ID que identifica o grupo contido no div
    // ou null se o form nao tem div secundarios.
    // Esta funcao tageia o div principal e preenche array
    // dos divs secundarios do arquivo guardando na variavel
    // global do arquivo: glb_aDIVS
    linkDivsAndSubForms('divSup01_01', ['divSup02_01'], [805]);
    
    windowOnLoad_2ndPart();
    
    //@@ Guarda o nome do campo na tabela que contem o id do registro corrente no sup
    glb_sFldIDName = 'DocumentoID';
    
    //@@ Guarda o id do controle (combo) que contem os tipos de registros
    // se nao e aplicavel no form, fazer glb_sCtlTipoRegistroID = '';
    glb_sCtlTipoRegistroID = 'selTipoDocumentoID';

    //@@ Guarda o nome do campo na tabela que contem o tipo do registro corrente no sup
    // se nao e aplicavel no form, fazer glb_sFldTipoRegistroName = ''
    glb_sFldTipoRegistroName = 'TipoDocumentoID';
}

function setupPage()
{
    //@@ Ajusta os divs e seus elementos
    adjustDivs('sup',[[1,'divSup01_01']]);
    
    //@@ Ajusta os divs e seus elementos
    adjustDivs('sup',[[1,'divSup01_01'],
                      [2,'divSup02_01']]);

    //@@ *** Div principal superior divSup01_01 ***/
    adjustElementsInForm([['lblRegistroID','txtRegistroID',10,1],
                          ['lblEstadoID','txtEstadoID',2,1],
                          ['lblDocumento','txtDocumento',50,1],
                          ['lblDocumentoAbreviado','txtDocumentoAbreviado',7,1],
                          ['lblTipoDocumentoID','selTipoDocumentoID',8,1],
                          ['lblItemNormaID','selItemNormaID',8,1],
                          ['lblVersao', 'txtVersao', 5, 2],
                          ['lbldtEmissao','txtdtEmissao',10,2],
                          ['lblObservacao','txtObservacao',30,2],
                          ['lblTreinamento','chkTreinamento',3,2],
                          ['lblTemQuestionario','chkTemQuestionario',3,2],
                          ['lblImagem','chkImagem',3,2],
                          ['lblAprovacao','txtAprovacao',20,2]], null, null, true);
                          
	//@@ *** Div secundario superior divSup02_01 ***/
    adjustElementsInForm([['lblFormContextoID','selFormContextoID',40,1],
                          ['lblDias','txtDias',4,1]], null, null, true);                          
	
}

// CARRIER **********************************************************

/********************************************************************
DISPARAR UM CARRIER:
Usar a seguinte funcao:

sendJSCarrier(getHtmlId(),param1, param2);

Onde:
getHtmlId() - fixo, remete o id do html que disparou o carrier.
param1      - qualquer coisa. De uso do programador.
param2      - qualquer coisa. De uso do programador.

********************************************************************/

/********************************************************************
Funcao disparada pelo frame work.
CARRIER CHEGANDO NO ARQUIVO.
Executa quando um carrier e disparado por outro form.

Parametros: 
idElement   - id do html que disparou o carrier
idBrowser   - id do browser que disparou o carrier
param1      - parametro um trazido pelo carrier
param2      - parametro dois trazido pelo carrier

Retorno:
Se intercepta sempre retornar o seguinte array:
array[0] = getHtmlId() ou null
array[1] = qualquer coisa ou null
array[2] = qualquer coisa ou null
Se nao intercepta retornar null
********************************************************************/
function carrierArrived(idElement, idBrowser, param1, param2)
{
    if (param1 == 'SHOWDOCUMENTO')
    {
        // Da automacao, deve constar de todos os ifs do carrierArrived
        if ( __currFormState() != 0 )
            return null;
            
        // param2 - traz array:
            // o id da empresa do form que mandou
            // o id do registro a abrir
        // No sendJSMessage abaixo:
        // param2[1] = null significa que nao deve paginar
        // param2[2] = null significa que nao ha array de ids

        var empresa = getCurrEmpresaData();
        
        if ( param2[0] != empresa[0])
            return null;

        window.top.focus();

        // Exclusivo para exibir detalhe em form
        showDetailByCarrier(param2[1]);
        
        return new Array(getHtmlId(), null, null);          
    }
    // Programador alterar conforme necessario
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
CARRIER RETORNANDO AO ARQUIVO QUE O DISPAROU.
Executa quando um carrier e disparado por este form e apos executar
nos demais forms abertos, retorna a este form.

Parametros: 
idElement   - string id do html que interceptou o carrier
idBrowser   - string id do browser que contem o form que interceptou
              o carrier
param1      - parametro um trazido pelo carrier
param2      - parametro dois trazido pelo carrier

Retorno:
Sempre retorna null
********************************************************************/
function carrierReturning(idElement, idBrowser, param1, param2)
{

    // Nao mexer - Inicio de automacao ==============================
    return null;
    // Final de Nao mexer - Inicio de automacao =====================
}

// FINAL DE CARRIER *************************************************

/********************************************************************
Funcao disparada pelo frame work.
Coloca atributos em campos para serem manobrados graficamente
(esconde/mostra e habilita desabilita) pelo framework.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function putSpecialAttributesInControls()
{
    //@@
    
    // Tabela de atributos especiais:
    // 1. fnShowHide - seta funcao do programador que o framework executa
    //                 ao mostrar/esconder o controle.
    // 2. prog_SHHD  - (true) retira do frame work a operacao de mostrar/esconder
    //                 habiitar/desabilitar o controle e transfere para
    //                 o programador. False ou null devolve para o framework.

    chkImagem.disabled = true;
    chkTreinamento.disabled = true;
    
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares de servidor
do e preenchimentos de combos do sup, quando o usuario clica
um botao da barra do sup.
           
Parametros: 
ultimo botao clicado na barra de botoes superior

Retorno:
nenhum
********************************************************************/
function prgServerSup(btnClicked)
{
    //@@
    if ( (btnClicked == 'SUPDET') || (btnClicked == 'SUPREFR'))
    {
		adjustLabelsCombos(true);
    
		chkImagem.checked = dsoSup01.recordset['TemImagem'].value;
	}
    
	// Mover esta funcao para os finais de retornos de operacoes no
	// banco de dados
	finalOfSupCascade(btnClicked);
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao tem que ser chamada na ultima (ou ultimas) funcoes de
retorno de dados do servidor, feitas pelo programador.
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function finalOfSupCascade(btnClicked)
{
    //@@
    

    // Nao mexer - Inicio de automacao ==============================
    // Invoca a funcao lockAndOrSvrInf_SYS() do detailinf.js
    // para as operacoes de bancos de dados da automacao.
	sendJSMessage(getHtmlId(), JS_INFSYS, 'ENDPROGSERVERSUP', btnClicked);
	// Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares
do form, necessarias apos um changeDiv. NAO PODE CONTER NENHUMA
OPERACAO DE BANCO DE DADOS.
           
Parametros: 
ultimo botao clicado na barra de botoes superior

Retorno:
nenhum
********************************************************************/
function prgInterfaceSup(btnClicked)
{
    //@@
    // Chama funcoes particulares do form que forem necessarias
    // depois de trocar o div

    if ( btnClicked == 'SUPINCL' )
    {
        // funcao que controla campos read-only
        controlReadOnlyFields();
    }
    
    adjustLabelsCombos(true);

    // Nao mexer - Inicio de automacao ==============================
    sendJSMessage(getHtmlId(), JS_INFSYS, 'ENDCHANGEDIVSUP', btnClicked);
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao recebe todos os onchange de combos do form e executa as
funcoes apropriadas definidas pelo programador e aqui invocadas. 
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function optChangedInCmb(cmb)
{
    var cmbID = cmb.id;
    
    //@@ Os if e os else if abaixo sao particulares de cada form
    // Troca a interface do sup em funcao de item selecionado
    // no combo de tipo.
    // Se for necessario ao programador, repetir o if abaixo
    // definido, antes deste comentario.
    // Nao mexer - Inicio de automacao ==============================
    // If abaixo alterado especificamente para este forma
    // o normal seria este campo se chamar selTipoRegistroID no
    // arquivo .asp
    //if ( cmbID == 'selTipoRegistroID' )
    if ( cmbID == 'selTipoDocumentoID' )
        adjustSupInterface();
    // Final de Nao mexer - Inicio de automacao =====================
    
    adjustLabelsCombos(false);
}

/********************************************************************
Funcao disparada pelo frame work.
Dispara logo apos abrir a maquina de estado. 
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
nenhum
********************************************************************/
function stateMachOpened( currEstadoID )
{

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou OK na maquina de estado
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
false - automacao prossegue
true  - automacao para        
       
Nota:
Se o programador retornar diferente de null nesta funcao,
o sistema permanecera travado, ao final do seu codigo o programador
dever� chamar a seguinte funcao da automacao:
stateMachSupExec(action)
onde action = 'OK' - o novo estado sera gravado
              'CANC' - a gravacao sera cancelada
para prosseguir a automacao
********************************************************************/
function stateMachBtnOK( currEstadoID, newEstadoID )
{
    // Ativo 
    if (newEstadoID == 2)
    {
        verifyDocumentoInServer(2);
        return true;
    }
    
    // adequacao para aprovacao
    if (currEstadoID == 92 && newEstadoID == 93) 
    {
        documentoVerifica(currEstadoID, newEstadoID);
    }
    
    //@@ prossegue a automacao
    return false;    
}

/********************************************************************
Funcao disparada pelo frame work.
Dispara se o usuario trocou o estado.
Dispara logo apos fechar a maquina de estado. 
           
Parametros: 
oldEstadoID     - previo EstadoID do registro
currEstadoID    - novo EstadoID do registro
bSavedID        - o novo estadoID foi gravado no banco (true/false)

Retorno:
nenhum
********************************************************************/
function stateMachClosed( oldEstadoID, currEstadoID, bSavedID )
{
	if (bSavedID)
        glb_TimerDocumentoVar = window.setInterval('refreshSup()', 10, 'JavaScript');
}

/********************************************************************
Funcao disparada pelo frame work.
Propaga clique e botao de lupa que nao seja proprietario e
alternativo.
           
Parametros: 
btnClicked      - referencia ao botao de lupa clicado

Retorno:
nenhum
********************************************************************/
function btnLupaClicked(btnClicked)
{

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked)
{
    // Documentos
    if ((controlBar == 'SUP') && (btnClicked == 1)) {
        __openModalDocumentos(controlBar, dsoSup01.recordset.Fields[0].value);
    }
    else if (btnClicked == 2)
        openModalPrint();
    else if (btnClicked == 3)
        window.top.openModalControleDocumento('S', '', 423, null, null, 'T', 'btn3');
    else if (btnClicked == 4) 
    {
        nEstado = txtEstadoID.value;
        
        if (nEstado == 'E')
            window.top.openModalControleDocumento('S', '', txtRegistroID.value, -1, null, 'T', 'btn4');
        else
            window.top.openModalControleDocumento('S', '', txtRegistroID.value, null, null, 'T', 'btn4');
    }
    //else if (btnClicked == 5)
    //    loadModalImagem();
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal de impressao

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalPrint()
{
    var htmlPath;
    var strPars = new String();
    var empresaData = getCurrEmpresaData();
    var empresaID = empresaData[0];
    var empresaFantasia = empresaData[3];
    var contexto = getCmbCurrDataInControlBar('sup', 1);
    var userID = getCurrUserID();
    var nDocumentoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,"dsoSup01.recordset['DocumentoID'].value");
    
    // mandar os parametros para o servidor
    // o primeiro parametro e obrigatorio
    
    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + escape('S');

    // parametros opcionais
    strPars += '&nEmpresaID=' + escape(empresaID);
    strPars += '&sEmpresaFantasia=' + escape(empresaFantasia);       
    strPars += '&nContextoID=' + escape(contexto[1]);       
    strPars += '&nUserID=' + escape(userID);       
    strPars += '&nDocumentoID=' + escape(nDocumentoID);       

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_ASPURLROOT + '/modqualidade/submodiso/documentos/modalpages/modalprint.asp'+strPars;

    showModalWin(htmlPath, new Array(346,234));
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou um botao nao especifico do control bar.

Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - Um dos seguintes botoes:
                - Avancar, retroagir, incluir, alterar, estado, 
                  excluir, OK, cancelar refresh, anterior e proximo. 
                  (Testar a string que vem).

Retornos:
null            - a automacao prossegue. Qualquer outro retorno a
                  automacao nao prossegue.
********************************************************************/
function btnBarNotEspecClicked(controlBar, btnClicked) 
{
   // Para prosseguir a automacao retornar null
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2)
{
    if (idElement.toUpperCase() == 'MODALCONTROLEDOCUMENTOHTML')
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    else if ( idElement.toUpperCase() == 'MODALPRINTHTML' )    
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
           
            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }    
    }
    // Imagens
    if ( (idElement.toUpperCase() == 'MODALGERIMAGENSHTML')||
          (idElement.toUpperCase() == 'MODALSHOWIMAGENSHTML'))
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    // Modal de documentos
    else if (idElement.toUpperCase() == 'MODALDOCUMENTOSHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Informa que o sup esta em modo de edicao e a interface ja esta
destravada.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function supInitEditMode()
{
    // funcao que controla campos read-only
    controlReadOnlyFields();

    // chamada abaixo necessaria para prosseguir a automacao
    // mover para final de operacoes de servidor se for o caso
    supInitEditMode_Continue();
}

/********************************************************************
Funcao disparada pelo frame work.
Form carregou e vai mostrar a interface.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function formFinishLoad()
{
    // seta botao de impressao
    //especBtnIsPrintBtn('sup', 1);

    // mostra dois botoes especificos desabilitados
    showBtnsEspecControlBar('sup', true, [1,1,1,1]);
    tipsBtnsEspecControlBar('sup', ['Documentos', 'Relat�rios', 'Procedimento', 'Texto']);

}

/********************************************************************
Funcao criada pelo programador.
Controla os campos Read Only

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function controlReadOnlyFields()
{
    txtVersao.readOnly = true;
    txtdtEmissao.readOnly = true;
    txtAprovacao.readOnly = true;
}

/********************************************************************
Funcao criada pelo programador.
Da refresh no sup

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function refreshSup()
{
    if ( glb_TimerDocumentoVar != null )
    {
        window.clearInterval(glb_TimerDocumentoVar);
        glb_TimerDocumentoVar = null;
    }
    
    __btn_REFR('sup');
}

function adjustLabelsCombos(bPaging)
{
	var nNormaID = 0;
	var nTipoDocumentoID = 0;
	
    if (bPaging)
    {
        setLabelOfControl(lblTipoDocumentoID, dsoSup01.recordset['TipoDocumentoID'].value);
        setLabelOfControl(lblItemNormaID, dsoSup01.recordset['ItemNormaID'].value);
        setLabelOfControl(lblFormContextoID, dsoSup01.recordset['ContextoID'].value);
        
        nNormaID = dsoSup01.recordset['ItemNormaID'].value;
        nTipoDocumentoID = dsoSup01.recordset['TipoDocumentoID'].value;
	}        
    else
    {
        setLabelOfControl(lblTipoDocumentoID, selTipoDocumentoID.value);
        setLabelOfControl(lblItemNormaID, selItemNormaID.value);
        setLabelOfControl(lblFormContextoID, selFormContextoID.value);
        
        nNormaID = selItemNormaID.value;
        nTipoDocumentoID = selTipoDocumentoID.value;
    }
    
	// Diferente de Registro (805)
	if (nTipoDocumentoID != 805)
	{
		selFormContextoID.selectedIndex = -1;
			
		// Atribui valor 0 para forcar o banco a gravar NULL neste campo
		if (! bPaging)
			dsoSup01.recordset['ContextoID'].value = 0;
	}
	else
	{
		if (selFormContextoID.selectedIndex == -1)
			selFormContextoID.selectedIndex = 0;
	}
    
	lblItemNormaID.title = '';
	selItemNormaID.title = '';

    if ( !isNaN(parseInt(nNormaID, 10)) )
    {
		if (dsoEstaticCmbs.recordset.fields.count > 0)
		{
			if ( !(dsoEstaticCmbs.recordset.BOF && dsoEstaticCmbs.recordset.EOF) )
			{
				dsoEstaticCmbs.recordset.moveFirst();
				dsoEstaticCmbs.recordset.find('fldID', nNormaID);
				
				if ( !(dsoEstaticCmbs.recordset.BOF && dsoEstaticCmbs.recordset.EOF) )
				{
					lblItemNormaID.title = dsoEstaticCmbs.recordset['Hint'].value;
					selItemNormaID.title = dsoEstaticCmbs.recordset['Hint'].value;
				}
			}
		}
    }
}

/********************************************************************
Verificacoes do Documento
nTipoResultado: 1-Estudo
				2-Aprova��o
********************************************************************/
function verifyDocumentoInServer(nTipoResultado)
{
    var nDocumentoID = dsoSup01.recordset['DocumentoID'].value;
	var nUsuarioID = getCurrUserID();
    
    var strPars = new String();

    strPars = '?nDocumentoID='+escape(nDocumentoID);
    strPars += '&nUsuarioID='+escape(nUsuarioID);
    strPars += '&nTipoResultado='+escape(nTipoResultado);

    dsoVerificacao.URL = SYS_ASPURLROOT + '/modqualidade/submodiso/documentos/serverside/verificacaodocumento.aspx' + strPars;

	if (nTipoResultado == 1)
		dsoVerificacao.ondatasetcomplete = verifyDocumentoInServer_DSC;
	else
		dsoVerificacao.ondatasetcomplete = verifyDocumentoInServer2_DSC;

    dsoVerificacao.refresh();
}

/********************************************************************
Retorno do servidor - Verificacoes do Documento
********************************************************************/
function verifyDocumentoInServer_DSC() {
    lockInterface(false);

	sendJSMessage('INF_HTML', JS_DATAINFORM, EXECEVAL,'editCurrFolder()');

    if (dsoVerificacao.recordset['Verificacao'].value == null)
    {
        return null;
    }    
    else
    {
        if ( window.top.overflyGen.Alert(dsoVerificacao.recordset['Verificacao'].value) == 0 )
            return null;

		sendJSMessage('INF_HTML', JS_DATAINFORM, EXECEVAL,'__btn_CANC(' + '\'' + 'inf' + '\'' + ')');
    }
}

/********************************************************************
Retorno do servidor - Verificacoes do Documento
********************************************************************/
function verifyDocumentoInServer2_DSC() 
{
    var _retConf;
    var versao = txtVersao.value;
    var bTreinamento = false;

    if (dsoVerificacao.recordset['Verificacao'].value == null)
    {
        if (versao == "")
            bTreinamento = true;
        else 
        {
            _retConf = window.top.overflyGen.Confirm('Documento ter� Treinamento?');

            if (_retConf == 1)
                bTreinamento = true;
            else
                bTreinamento = false;
        }

        chkTreinamento.checked = bTreinamento;
        dsoSup01.recordset['Treinamento'].value = bTreinamento;

        stateMachSupExec('OK');
    }    
    else
    {
        if ( window.top.overflyGen.Alert(dsoVerificacao.recordset['Verificacao'].value) == 0 )
            return null;
        stateMachSupExec('CANC');
    }
}

function loadModalImagem()
{
	var strPars = new String();
	
	var nDocumentoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,"dsoSup01.recordset['DocumentoID'].value");
 
	var nTipoDocumentoID = dsoSup01.recordset['TipoDocumentoID'].value;

	var sCaller = 'S';
	
	var nSubFormID = window.top.subFormID;

	var sVersao = txtVersao.value;
 
    var nA1 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                                ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'A1' + '\'' + ')') );

    var nA2 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                                ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'A2' + '\'' + ')') );

	strPars = '?nRegistroID=' + escape(nDocumentoID);
	strPars += '&nTipoRegistroID=' + escape(nTipoDocumentoID);
	strPars += '&nSubFormID=' + escape(nSubFormID);	
	strPars += '&sCaller=' + escape(sCaller);
	strPars += '&sVersao=' + escape(sVersao);
	
    //DireitoEspecifico
    //Documentos->Documentos->SUP->Modal Imagens
    //26000 SFS Grupo Documento-> A1&&A2 -> Abre modal de gerenciamento de imagem, caso contrario apenas mostra a imagem.
    if ( (nA1 == 1) && (nA2 == 1) )
    {
		htmlPath = SYS_ASPURLROOT + '/modalgen/modalgerimagens.asp'+strPars;
    	
		showModalWin(htmlPath, new Array(724, 488));
		//showModalWin(htmlPath, new Array(524, 488));
    }
    else
    {
		htmlPath = SYS_ASPURLROOT + '/modalgen/modalshowimagens.asp'+strPars;
    
		showModalWin(htmlPath, new Array(0,0));
    }
}


function documentoVerifica(currEstadoID, newEstadoID) 
{
    var nDocumentoID = dsoSup01.recordset['DocumentoID'].value;
    var strPars = new String();

    strPars = '?nDocumentoID=' + escape(nDocumentoID);
    strPars += '&nEstadoDeID=' + escape(currEstadoID);
    strPars += '&nEstadoParaID=' + escape(newEstadoID);
    strPars += '&nUsuarioID=' + escape(getCurrUserID());

    dsoVerificacao.URL = SYS_ASPURLROOT + '/modqualidade/submodiso/documentos/serverside/documentoverifica.aspx' + strPars;
    dsoVerificacao.ondatasetcomplete = documentoVerifica_DSC;
    dsoVerificacao.refresh();
}

function documentoVerifica_DSC() 
{
    var sMensagem = (dsoVerificacao.recordset['Mensagem'].value == null ? '' : dsoVerificacao.recordset['Mensagem'].value);
    var nResultado = (dsoVerificacao.recordset['Resultado'].value == null ? 0 : dsoVerificacao.recordset['Resultado'].value);

    if (nResultado == 1) {
        var _retConf = window.top.overflyGen.Confirm(sMensagem);

        if (_retConf == 0)
            return null;
        else if (_retConf == 1)
            stateMachSupExec('OK');
        else
            stateMachSupExec('CANC');
    }
    else if (nResultado == 2) {
        if (sMensagem != '') {
            if (window.top.overflyGen.Alert(sMensagem) == 0)
                return null;
        }

        stateMachSupExec('OK');
    }
    else {
        if (sMensagem != '') {
            if (window.top.overflyGen.Alert(sMensagem) == 0)
                return null;
        }

        stateMachSupExec('CANC');
    }
}