function relatorioLVA(nRAIID, bNaoConformidade)
{
    var aEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');
    var sLinguaLogada = getDicCurrLang();

    formato = selFormatoSolicitacao.value;

    var strParameters = "RelatorioID=" + selReports.value + "&glb_nEmpresaID=" + aEmpresaData[0] + "&glb_sEmpresaFantasia=" + glb_sEmpresaFantasia + "&Formato=" + formato +
                        "&nEmpresaLogadaIdioma=" + aEmpresaData[8] + "&bNaoConformidade=" + bNaoConformidade + "&nRAIID=" + nRAIID;

    pb_StartProgressBar(glb_aReportsTimeExecution[aseek(glb_aReportsTimeExecution, selReports.value, 0)][1]);
    lockControlsInModalWin(true);

    var frameReport = document.getElementById("frmReport");
    frameReport.contentWindow.document.onreadystatechange = reports_onreadystatechange;
    frameReport.contentWindow.location = SYS_PAGESURLROOT + "/modqualidade/submodiso/documentos/serverside/Reports_documentos.aspx?" + strParameters;

    pb_StartProgressBar(glb_aReportsTimeExecution[aseek(glb_aReportsTimeExecution, selReports.value, 0)][1]);
}