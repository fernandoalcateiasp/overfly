/********************************************************************
modalalternativas.js

Library javascript para o modalalternativas.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var fg_ExecEvents = true;
var glb_LastBtnClicked = null;
var glb_bEnableGravar = false;

// Dados do grid .RDS
var dsoGrid = new CDatatransport("dsoGrid");
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
btn_onclick(ctl)
btn_onclick_not_OK_Canc(ctl)
fillGridData()
fillGridData_DSC()
setBtnsState()
addLineAlternativaInGrid()
saveDataGridInServer()
exclLineFromGrid()

EVENTOS DE GRID

DEFINIDAS NO ARQUIVO modalalternativas.ASP
Sub fg_BeforeRowColChange(OldRow, OldCol, NewRow, NewCol, Cancel)
    Cancel = js_fg_BeforeRowColChange (fg, OldRow, OldCol, NewRow, NewCol, Cancel)
End Sub

Sub fg_AfterRowColChange(OldRow, OldCol, NewRow, NewCol)
    fg_AfterRowColChange()
End Sub

Sub fg_EnterCell()
    js_fg_EnterCell fg
End Sub
FINAL DE DEFINIDAS NO ARQUIVO modalalternativas.ASP

js_fg_modalalternativasBeforeMouseDown(fg, Button, Shift, X, Y, Cancel)
js_fg_modalalternativasDblClick( grid, Row, Col)
js_modalalternativasKeyPress(KeyAscii)
js_modalalternativas_AfterRowColChange
js_modalalternativas_ValidateEdit()
js_modalalternativas_AfterEdit(Row, Col)
js_modalalternativas_CellChanged fg, Row, Col
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();
    
    // ajusta o body do html
    with (modalalternativasBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // configuracao inicial do html
    setupPage();
    
    // Mostra a janela
    showExtFrame(window, true);
    
    fillGridData();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
	// texto da secao01
    secText('Alternativas', 1);

    // ajusta elementos da janela
    var elem;
    var i;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var eLeft = 2;
    var eTop = 1;
    var rQuote = 0;
    var btnXGap = 20;
    
    var previousBtn;       
    
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    eLeft = 2;
    eTop = 2;
    rQuote = 0;
    
    // desabilita e esconde o botao OK pois nao tem utilizacao nesta janela
    btnOK.disabled = true;
    btnOK.style.visibility = 'hidden';

    // ajusta o divFG
    with (divFG.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;    
        height = parseInt(btnOK.currentStyle.top, 10) - parseInt(top, 10) - ELEM_GAP;
    }
    
    with (fg.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }
    
    startGridInterface(fg);
    fg.Cols = 0;
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;    
    fg.Redraw = 2;
    
    // Ajusta e posiciona os botoes
    // btnIncluir btnExcluir btnGravar btnRefresh btnCanc
	
	btnIncluir.style.top = parseInt(btnCanc.currentStyle.top, 10);
	btnExcluir.style.top = parseInt(btnCanc.currentStyle.top, 10);
	btnGravar.style.top = parseInt(btnCanc.currentStyle.top, 10);
	btnRefresh.style.top = parseInt(btnCanc.currentStyle.top, 10);
	
	btnIncluir.style.width = parseInt(btnCanc.currentStyle.width, 10);
	btnExcluir.style.width = parseInt(btnCanc.currentStyle.width, 10);
	btnGravar.style.width = parseInt(btnCanc.currentStyle.width, 10);
	btnRefresh.style.width = parseInt(btnCanc.currentStyle.width, 10);
	
	btnIncluir.style.height = parseInt(btnCanc.currentStyle.height, 10);
	btnExcluir.style.height = parseInt(btnCanc.currentStyle.height, 10);
	btnGravar.style.height = parseInt(btnCanc.currentStyle.height, 10);
	btnRefresh.style.height = parseInt(btnCanc.currentStyle.height, 10);
	
	// Posicionamento horizontal dos botoes
	
	elem = btnGravar;
	with (elem.style)
	{
		left = parseInt(divFG.currentStyle.left, 10) + 
		       (parseInt(divFG.currentStyle.width, 10) / 2) -
		       (parseInt(elem.currentStyle.width, 10) / 2);
		
		previousBtn = elem;       
	}
	
	elem = btnExcluir;
	with (elem.style)
	{
		left = parseInt(previousBtn.currentStyle.left, 10) - 
		       parseInt(elem.currentStyle.width, 10) - btnXGap;
		       
		previousBtn = elem;
	}
	
	elem = btnIncluir;
	with (elem.style)
	{
		left = parseInt(previousBtn.currentStyle.left, 10) - 
		       parseInt(elem.currentStyle.width, 10) - btnXGap;
		       
		previousBtn = elem;
	}

	previousBtn = btnGravar;       	
	
	elem = btnRefresh;
	with (elem.style)
	{
		left = parseInt(previousBtn.currentStyle.left, 10) +
		       parseInt(previousBtn.currentStyle.width, 10) + btnXGap;
		       
		previousBtn = elem;
	}
	
	elem = btnCanc;
	with (elem.style)
	{
		left = parseInt(previousBtn.currentStyle.left, 10) +
		       parseInt(previousBtn.currentStyle.width, 10) + btnXGap;
		       
		previousBtn = elem;
	}
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
	glb_LastBtnClicked = ctl;
	
    // Exclusivo para esta janela
    if ( (ctl == btnIncluir) || (ctl == btnExcluir) ||
         (ctl == btnGravar) || (ctl == btnRefresh) )
    {
		btn_onclick_not_OK_Canc(ctl);
		return true;    
    }     
    
    window.focus();
    if (ctl.id == btnOK.id )
        btnOK.focus();
    else if (ctl.id == btnCanc.id )
        btnCanc.focus();
    
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id )
    {
        // 1. O usuario clicou o botao OK
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_I', null );    
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_I', null );    
}

/********************************************************************
Clique botoes Incluir, Excluir, Gravar e Refresh
********************************************************************/
function btn_onclick_not_OK_Canc(ctl)
{
    if ( ctl == btnIncluir )
	{
		glb_LastBtnClicked = ctl;
		addLineAlternativaInGrid();
	}
    else if ( ctl == btnExcluir )
	{
		glb_LastBtnClicked = ctl;

		exclLineFromGrid();
	}	
    else if ( ctl == btnGravar )
	{
		// esta funcao trava o html contido na janela modal
		lockControlsInModalWin(true);
		
		glb_LastBtnClicked = ctl;
		saveDataGridInServer();
	}	
    else if ( ctl == btnRefresh )
	{
		// esta funcao trava o html contido na janela modal
		lockControlsInModalWin(true);
		
		glb_LastBtnClicked = null;
		fillGridData();
	}	
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridData()
{
    var strSQL = '';
    
    lockControlsInModalWin(true);
    
    // zera o grid
    fg.Rows = 1;
    
    // parametrizacao do dso dsoGrid
    setConnection(dsoGrid);

    strSQL = 'SELECT * FROM Documentos_Questionarios_Alternativas WITH(NOLOCK) ' +
		'WHERE DocQuestionarioID = ' + glb_nDocQuestionarioID;
    
    if (strSQL != null)
    {
        dsoGrid.SQL = strSQL;
        dsoGrid.ondatasetcomplete = fillGridData_DSC;
        dsoGrid.Refresh();
    }
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData_DSC() {
    var dTFormat = '';
    
    glb_bEnableGravar = false;
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';
    
    fg_ExecEvents = false;
        
    fg.Redraw = 0;
    fg.Editable = false;
    startGridInterface(fg);
    fg.FrozenCols = 0;
    
    fg.Editable = false;
    fg.Redraw = 0;
    fg.Rows = 1;

    fg.ExplorerBar = 0;

    headerGrid(fg,['Ordem',
                   'Alternativa', 
                   'Peso',
                   'TemTexto',
                   'DocQuestionarioID',
                   'DocAlternativaID'],[4, 5]);

    // fg.FrozenCols = 3;

    fg.ColKey(0)  = 'Ordem';
    fg.ColKey(1)  = 'Alternativa';
    fg.ColKey(2)  = 'Peso';
    fg.ColKey(3)  = 'TemTexto';
    fg.ColKey(4)  = 'DocQuestionarioID';
    fg.ColKey(5)  = 'DocAlternativaID';

    // Format boolean (checkbox)
    fg.ColDataType(getColIndexByColKey(fg, 'TemTexto')) = 11;

    if (!(dsoGrid.recordset.BOF || dsoGrid.recordset.EOF))
    {
		dsoGrid.recordset.MoveFirst();

		while ( !dsoGrid.recordset.EOF )
		{
		    fg.Rows = fg.Rows + 1;
		    fg.TextMatrix(fg.Rows-1, getColIndexByColKey(fg, 'Ordem'))  = (dsoGrid.recordset['Ordem'].value == null ? '' : dsoGrid.recordset('Ordem').value);
		    fg.TextMatrix(fg.Rows-1, getColIndexByColKey(fg, 'Alternativa'))  = (dsoGrid.recordset['Alternativa'].value == null ? '' : dsoGrid.recordset('Alternativa').value);
		    fg.TextMatrix(fg.Rows-1, getColIndexByColKey(fg, 'Peso'))  = (dsoGrid.recordset['Peso'].value == null ? '' : dsoGrid.recordset('Peso').value);
		    fg.TextMatrix(fg.Rows-1, getColIndexByColKey(fg, 'TemTexto'))  = (dsoGrid.recordset['TemTexto'].value == null ? '' : dsoGrid.recordset('TemTexto').value);
		    fg.TextMatrix(fg.Rows-1, getColIndexByColKey(fg, 'DocQuestionarioID'))  = (dsoGrid.recordset['DocQuestionarioID'].value == null ? '' : dsoGrid.recordset('DocQuestionarioID').value);
		    fg.TextMatrix(fg.Rows-1, getColIndexByColKey(fg, 'DocAlternativaID'))  = (dsoGrid.recordset['DocAlternativaID'].value == null ? '' : dsoGrid.recordset('DocAlternativaID').value);

		    dsoGrid.recordset.MoveNext();
		}
    }

    paintReadOnlyCols(fg);
        
    putMasksInGrid(fg, ['999',
                        '',
                        '999.99',
                        '',
                        '',
                        ''],
                        ['###',
                        '',
                        '###.##',
                        '',
                        '',
                        '']);
        
    fg.Col = 1;

    //fg.ExplorerBar = 5;

    // se nenhuma linha foi selecionada, seleciona a primeira
    // se a mesma existe
    if ( fg.Rows > 1 )
        fg.Row = 1;
    
    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);
    fg.Redraw = 2;
    
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;    
    
    if (fg.Rows > 1)
        fg.Editable = true;
	
    lockControlsInModalWin(false);
    
    fg_ExecEvents = true;

    // se tem linhas no grid, coloca foco no grid
    if (fg.Rows > 1)
    {
        window.focus();
        fg.focus();
    }
    else
    {
        ;
    }            
	
	glb_LastBtnClicked = null;
	
	setBtnsState();
}

/********************************************************************
Trata o estado dos botoes
Incluir - So inclue uma linha no grid, nao inclue registro novo no dso.
          Esta sempre habilitado.
          Desabilita quando e clicado e habilita novamente depois
          de um gravar pou refresh.
Excluir - Exclue linha do grid e do dso.
		  Esta sempre habilitado se tem uma linha do grid corrente.
          Desabilita se nao tem linha no grid.
Gravar  - Inclue registros novos no dso.
          Habilita se incluiu uma linha ou alterou qualquer celula
          de qualquer linha do grid.
Refresh - Esta sempre habilitado.
Fechar  - Esta sempre habilitado.

Nota: Os campos alterados no grid sao imediatamente alterados no dso,
      no evento afterEdit do grid.

********************************************************************/
function setBtnsState()
{
	btnGravar.disabled = !glb_bEnableGravar;
	
	// Janela acabou de carregar ou foi dado um refresh no grid
	if (glb_LastBtnClicked == null)
	{
		btnIncluir.disabled = false;
		btnExcluir.disabled = !(fg.Row > 0);
		btnRefresh.disabled = false;
		btnCanc.disabled = false;
	}	
	// Foi incluida uma linha no grid	
	else if (glb_LastBtnClicked == btnIncluir)
	{
		btnIncluir.disabled = true;
		btnExcluir.disabled = !(fg.Row > 0);
		btnRefresh.disabled = false;
		btnCanc.disabled = false;
	}		
	// Foi excluida uma linha no grid	
	else if (glb_LastBtnClicked == btnExcluir)
	{
		btnIncluir.disabled = false;
		btnExcluir.disabled = !(fg.Row > 0);
		btnRefresh.disabled = false;
		btnCanc.disabled = false;
	}		
}

/********************************************************************
ASdiciona uma linha no grid
********************************************************************/
function addLineAlternativaInGrid()
{
	fg_ExecEvents = false;

	addNewLine(fg);
		
	fg.SelectionMode = 1;
		
	fg.Redraw = 2;
		
	glb_LASTLINESELID = fg.Rows - 1;
		
	fg.AutoSizeMode = 0;
	fg.AutoSize(0,fg.Cols-1);
				
	fg_ExecEvents = true;
		
	window.focus();
	fg.focus();
	
	setBtnsState();        
}

/********************************************************************
Salva os registros do grid no servidor
********************************************************************/
function saveDataGridInServer()
{
	var i;
	var nDocAlternativaID;

	for (i=1; i<fg.Rows; i++)
	{
		nDocAlternativaID = getCellValueByColKey(fg, 'DocAlternativaID', i);

		// Se eh um registro novo
		if ( (nDocAlternativaID == null) || (nDocAlternativaID == 0) )
		{
			dsoGrid.recordset.AddNew();

			dsoGrid.recordset['Ordem'].value = fg.ValueMatrix(i, getColIndexByColKey(fg, 'Ordem'));
			dsoGrid.recordset['Alternativa'].value = fg.TextMatrix(i, getColIndexByColKey(fg, 'Alternativa'));
			dsoGrid.recordset['Peso'].value = fg.ValueMatrix(i, getColIndexByColKey(fg, 'Peso'));
			dsoGrid.recordset['TemTexto'].value = parseInt(fg.ValueMatrix(i, getColIndexByColKey(fg, 'TemTexto')));
			dsoGrid.recordset['DocQuestionarioID'].value = glb_nDocQuestionarioID;
		}
		// O registro ja existe
		else
		{
			dsoGrid.recordset.MoveFirst();
			dsoGrid.recordset.Find('DocAlternativaID', nDocAlternativaID);

			if ( !(dsoGrid.recordset.BOF || dsoGrid.recordset.EOF) )
			{
				dsoGrid.recordset['Ordem'].value = fg.ValueMatrix(i, getColIndexByColKey(fg, 'Ordem'));
				dsoGrid.recordset['Alternativa'].value = fg.TextMatrix(i, getColIndexByColKey(fg, 'Alternativa'));
				dsoGrid.recordset['Peso'].value = fg.ValueMatrix(i, getColIndexByColKey(fg, 'Peso'));
				dsoGrid.recordset['TemTexto'].value = (fg.ValueMatrix(i, getColIndexByColKey(fg, 'TemTexto')) == 0 ? 0 : 1);
				dsoGrid.recordset['DocQuestionarioID'].value = glb_nDocQuestionarioID;
			}
		}
	}

    try
    {
        dsoGrid.SubmitChanges();   
    }
    catch(e)
    {
        // Numero de erro qdo o registro foi alterado ou removido por outro usuario
        // ou operacao de gravacao abortada no banco
        if (e.number == -2147217887)
            if ( window.top.overflyGen.Alert('Este registro acaba de ser alterado ou removido.') == 0 )
                return null;
    }    
    
    dsoGrid.ondatasetcomplete = fillGridData_DSC;
    dsoGrid.Refresh();
}

/********************************************************************
Remove a linha selecionada do grid
********************************************************************/
function exclLineFromGrid()
{
	var nDocAlternativaID = getCellValueByColKey(fg, 'DocAlternativaID', fg.Row);
	var lineToDelete = fg.Row;
	var last_fg_ExecEvents = null;
	
	// Se for um registro novo, sai da rotina
	if ( (nDocAlternativaID == null) || (nDocAlternativaID == '') )
	{
		last_fg_ExecEvents = fg_ExecEvents;
		fg_ExecEvents = false;
		
		fg.RemoveItem(fg.Row);

		setLineAfterDelete(fg, lineToDelete);
		
		fg_ExecEvents = last_fg_ExecEvents;
		
		setBtnsState();
		
		return null;
	}	

	dsoGrid.recordset.MoveFirst();
    dsoGrid.recordset.Find('DocAlternativaID', nDocAlternativaID);

    if ( !(dsoGrid.recordset.BOF || dsoGrid.recordset.EOF) )
    {
		dsoGrid.recordset.Delete();
		glb_bEnableGravar = true;
	}

	last_fg_ExecEvents = fg_ExecEvents;
	fg_ExecEvents = false;
		
	fg.RemoveItem(fg.Row);
	
	setLineAfterDelete(fg, lineToDelete);
	
	fg_ExecEvents = last_fg_ExecEvents;

    setBtnsState();
}

// EVENTOS DE GRID **************************************************

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalalternativasBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
{
    return true;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalalternativasDblClick(grid, Row, Col)
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalalternativasKeyPress(KeyAscii)
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalalternativas_ValidateEdit()
{
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalalternativas_AfterEdit(Row, Col)
{
	
}

function js_modalalternativas_CellChanged(grid, Row, Col)
{
	if ( fg_ExecEvents == false )
		return true;

	glb_bEnableGravar = true;
	setBtnsState();
}

// FINAL DE EVENTOS DE GRID *****************************************
