/********************************************************************
documentosinf01.js

Library javascript para o recursosinf01.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
// Dados do registro corrente .SQL (no minimo: regID, observacoes, propID e alternativoID) 
var dso01JoinSup = new CDatatransport("dso01JoinSup");
// Dados de combos estaticos do form .URL 
var dsoEstaticCmbs = new CDatatransport("dsoEstaticCmbs");
// Dados de combos dinamicos do form .URL 
var dsoDynamicCmbs = new CDatatransport("dsoDynamicCmbs");
// Descricao dos estados atuais dos registros do grid corrente .SQL         
var dsoStateMachineLkp = new CDatatransport("dsoStateMachineLkp");        
// Dados do combo de filtros da barra inferior .SQL         
var dsoFiltroInf = new CDatatransport("dsoFiltroInf");
// Dados dos combos de proprietario e alternativo         
var dsoCmbsPropAlt = new CDatatransport("dsoCmbsPropAlt");
// Dados de um grid .SQL 
var dso01Grid = new CDatatransport("dso01Grid");
var dsoCmb01Grid_01 = new CDatatransport("dsoCmb01Grid_01");
var dsoCmb01Grid_02 = new CDatatransport("dsoCmb01Grid_02");
var dsoCmb01Grid_03 = new CDatatransport("dsoCmb01Grid_03");
var dso01GridLkp = new CDatatransport("dso01GridLkp");
var dso02GridLkp = new CDatatransport("dso02GridLkp");
var dso03GridLkp = new CDatatransport("dso03GridLkp");
var dso04GridLkp = new CDatatransport("dso04GridLkp");
// Dados do retorno de cancelamento de estudo .SQL         
var dsoCancelEstudo = new CDatatransport("dsoCancelEstudo");
var dsoDataUltimaAlteracao = new CDatatransport("dsoDataUltimaAlteracao");
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:
    adjustElementsInDivsNonSTD()
    putSpecialAttributesInControls()
    optChangedInCmb(cmb)
    prgServerInf(btnClicked)
    prgInterfaceInf(btnClicked, pastaID, dso)
    finalOfInfCascade(btnClicked)
    treatsControlsBars(btnClicked, folderID, tipoCtlrsPasta, currDSO)
    btnLupaClicked(btnClicked)
    addedLineInGrid(folderID, grid, nLineInGrid, dso)
    btnBarClicked(controlBar, btnClicked)
    btnBarNotEspecClicked(controlBar, btnClicked)
    modalInformForm(idElement, param1, param2)
    btnAltPressedInGrid( folderID )
    btnAltPressedInNotGrid( folderID )
    fillCmbFiltsInfAutomatic(folderID)
    selPastaChanged(optText, optValue, optIndex, nQtyGridsInFolder)
    frameIsAboutToApplyRights(folderID)

FUNCOES DA MAQUINA DE ESTADO:
    stateMachOpened( currEstadoID )
    stateMachClosed( oldEstadoID, currEstadoID )

EVENTOS DO GRID FG
    fg_AfterRowColChange_Prg()
    fg_DblClick_Prg()
    fg_ChangeEdit_Prg()
    fg_ValidateEdit_Prg()
    fg_BeforeRowColChange_Prg()
    fg_EnterCell_Prg()
    fg_MouseUp_Prg()
    fg_MouseDown_Prg()
    fg_BeforeEdit_Prg()

FUNCOES PARTICULARES DESTA PAGINA:
	openModalDocQuestionario(nDocQuestionarioID)

********************************************************************/

/********************************************************************

// IMPLEMENTACAO DAS FUNCOES

//@@ todas as funcoes abaixo sao particulares de cada form
// conforme o form algumas funcoes devem ser removidas
// e outras serao acrescentadas
********************************************************************/

function window_onload()
{
    // Ze em 17/03/08
    dealWithGrid_Load();
    
    // Funcoes complementares da automacao
    windowOnLoad_1stPart();

    //@@ Combos estaticos. Array bidimensional de combos ids e campo indice
    // na tabela, identificador do combo.
    // O array deve ser null se nao tem combos estaticos
    glb_aStaticCombos = null;
    
    //@@ Combos estaticos em grids.
    // Array bidimensional da pasta id e array bidimensional
    // do indice das colunas
    // (a contar de zero), id do dso dos dados do combo,
    // nome do campo da tabela visivel no combo e
    // nome do campo do grid que linka o grid com o combo.
    // O array deve ser null se nenhum grid tem combos estaticos
    // Notacao para combo multicoluna:
    // [ [20012, [[1, 'dsoCmb01Grid_01', '*ItemAbreviado|ItemMasculino','ItemID']]] ] 

    glb_aGridStaticCombos = [ [20010, [[0, 'dso01GridLkp'   , ''             , ''],
                                       [0, 'dso02GridLkp'   , ''             , ''],
                                       [0, 'dso03GridLkp'   , ''             , ''],
                                       [0, 'dso04GridLkp'   , ''             , '']]],
                              [26001, [[0, 'dsoCmb01Grid_01', 'RecursoFantasia', 'RecursoID']]],
                              [26005, [[0, 'dsoCmb01Grid_01', '*DocumentoID|Documento', 'DocumentoID']]],
                              [26006, [[1, 'dsoCmb01Grid_01', '*ItemAbreviado|Norma', 'NormaID'],
									   [2, 'dsoCmb01Grid_02', 'Letra', 'Letra'],
									   [4, 'dsoCmb01Grid_03', 'Referencia', 'DocQuestionarioID']]],
							  [26008, [[0, 'dsoCmb01Grid_01', '*ItemAbreviado|ItemMasculino', 'ItemID'],
									   [2, 'dsoCmb01Grid_02', 'CodigoLiteral', 'CodigoLiteral']]] ];

    //@@ Linka os divs com os correspondentes subforms
    // criando o atributo sfsGrupo nos divs.
    // Os infs nao tem div principal e todos os divs sao secundarios.
    // Parametros:
    // 1o par: id do div principal ou null se nao tem div principal
    // 2opar: array de divs ids dos divs secundarios
    // ou null se nao tem divs secundarios
    // 3o par: array de xxx_ID que identifica o grupo contido no div
    // ou null se o form nao tem div secundarios.
    // Esta funcao tageia o div principal e preenche array
    // dos divs secundarios do arquivo guardando na variavel
    // global do arquivo: glb_aDIVS
        
    linkDivsAndSubForms(null, ['divInf01_01',
                               'divInf01_02',
                               'divInf01_03',
                               'divInf01_04',
                               'divInf01_05'],
                               [20008, 20009, [20010, 26001, 26002, 26005, 26006, 26007, 26008], 26004, 26011] );

    // Funcoes complementares da automacao
    windowOnLoad_2ndPart();
    
}

function setupPage()
{
    //@@ Ajustar os divs
    // Nada a codificar
    
    adjustDivs('inf',[[1,'divInf01_01'],
                      [1,'divInf01_02'],
                      [1,'divInf01_03'],
                      [1,'divInf01_04'],
                      [1,'divInf01_05']]);

    // txtEstudo
    elem = window.document.getElementById('txtEstudo');
    //elem.disabled = false;
    with (elem.style)
    {
        left = 10;
        top = 10;
        width = MAX_FRAMEWIDTH - (ELEM_GAP * 2);
        height = (MAX_FRAMEHEIGHT / 2) - (ELEM_GAP * 7);
    }

    // txtTexto
    elem = window.document.getElementById('txtTexto');
    //elem.disabled = false;
    with (elem.style)
    {
        left = 10;
        top = 10;
        width = MAX_FRAMEWIDTH - (ELEM_GAP * 2);
        height = (MAX_FRAMEHEIGHT / 2) - (ELEM_GAP * 7);
    }

}

// EVENTOS DO GRID FG ***********************************************
// Nota - sao disparados pelo frame work apos suas operacoes

function fg_AfterRowColChange_Prg()
{

}

function fg_DblClick_Prg()
{
	if ( fg.Row < 1 )
		return true;
	
	var nDocumentoID = getCellValueByColKey(fg, 'DocumentoID', fg.Row);
	
	if ( (keepCurrFolder() == 26002) && (fg.Rows > 1) &&
		 (nDocumentoID != null) && (nDocumentoID != '') )
	{
		sendJSMessage(window.top.pesqListID, JS_DATAINFORM, 'SHOWDETAIL', 
		      [getCellValueByColKey(fg, 'DocumentoID', fg.Row), null, null]);
	}		      
}

function fg_ChangeEdit_Prg()
{

}
function fg_ValidateEdit_Prg()
{

}
function fg_BeforeRowColChange_Prg()
{

}

function fg_EnterCell_Prg()
{

}

function fg_MouseUp_Prg()
{

}

function fg_MouseDown_Prg()
{

}

function fg_BeforeEdit_Prg()
{

}

// FINAL DE EVENTOS DO GRID FG **************************************

/********************************************************************
Funcao disparada pelo frame work.
Ajusta graficamente elementos de divs que nao estao no padrao do
framework.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function adjustElementsInDivsNonSTD()
{
    //@@
    // Este form nao tem este caso    
}

/********************************************************************
Funcao disparada pelo frame work.
Coloca atributos em campos para serem manobrados graficamente
(esconde/mostra e habilita desabilita) pelo framework.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function putSpecialAttributesInControls()
{
    //@@
    glb_pastaDefault = 26001;
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao recebe todos os onchange de combos do inf e executa as
funcoes apropriadas definidas pelo programador e aqui invocadas. 
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function optChangedInCmb(cmb)
{
    var cmbID = cmb.id;
    
    //@@ Os if e os else if abaixo sao particulares de cada form
    // as operacoes foram executadas manualmente pelo usuario

     
    // Troca a interface do sup em funcao de item selecionado
    // no combo de tipo.
    // Se for necessario ao programador, repetir o if abaixo
    // definido, antes deste comentario.
    // Nao mexer - Inicio de automacao ==============================
    if ( cmbID == 'selProprietario' || cmbID == 'selAlternativo')
        setPropAlt();
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares de servidor
e preenchimentos de combos do inf. Esta funcao e chamada quando
o usuario clica um botao da barra do sup ou inf.
           
Parametros: 
btnClicked    - ultimo botao clicado na barra (superior ou inferior)

Retorno:
nenhum
********************************************************************/
function prgServerInf(btnClicked)
{
    //@@
    // Chama funcoes particulares do form que forem necessarias
    // antes de trocar o div

	// Mover esta funcao para os finais retornos de operacoes no
	// banco de dados
	finalOfInfCascade(btnClicked);
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares
do inf, necessarias apos um changeDiv.
           
Parametros: 
btnClicked  - ultimo botao clicado na barra de botoes inferior
pastaID     - id da pasta corrente
dso         - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function prgInterfaceInf(btnClicked, pastaID, dso)
{

    //@@
    // Chama funcoes particulares do form que forem necessarias
    // depois de trocar o div

    especBtnIsPrintBtn('inf', 0);
    
    showBtnsEspecControlBar('inf', false, [1,1,1,1]);
    
    // Referencias
    if ( pastaID == 26002 )
    {
        showBtnsEspecControlBar('inf', true, [1,1,0,0]);
        tipsBtnsEspecControlBar('inf', ['Detalhar Documento','Texto','','']);
    }
	// Questionario
	else if ( pastaID == 26006 )
    {
        showBtnsEspecControlBar('inf', true, [1,0,0,0]);
        tipsBtnsEspecControlBar('inf', ['Alternativas','','','']);
    }

    // Ao final das operacoes particulares da interface do inf
    // manda mensagem para a biblioteca do inf tratar os controls
    // bars superior e inferior
    sendJSMessage(getHtmlId(), JS_INFSYS, 'FINISHCHANGEDIV', btnClicked);
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao tem que ser chamada por todas as funcoes de retorno de
dados do servidor.
Se o botao clicado e da barra superior volta mensagem para o superior
efetuar troca de interface.
Se o botao clicado e da barra inferior volta mensagem para o inferior
efetuar troca de interface.           
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function finalOfInfCascade(btnClicked)
{
    //@@

    // Nao mexer - Inicio de automacao ==============================
    if ( (btnClicked.toUpperCase()).indexOf('SUP') >= 0 )
        // usario clicou botao no control bar sup
	    sendJSMessage(getHtmlId(), JS_SUPSYS, 'CHANGEDIV', btnClicked);
	else
	    // usario clicou botao no control bar inf
	    sendJSMessage(getHtmlId(), JS_INFSYS, 'CHANGEDIV', btnClicked);
	// Final de Nao mexer - Inicio de automacao =====================
	    
}

/********************************************************************
Funcao disparada pelo frame work.
Tratar os controls bar sup e inf aqui, apos o final de uma operacao
de interface e dados do form.
           
Parametros: 
btnClicked      - ultimo control bar (sup/inf) e botao clicado
folderID        - id da pasta corrente
tipoCtlrsPasta  - 'GRID' a pasta e um grid/ 'CTRLS' a pasta nao e
                  um grid
dsoID           - id do dso corrente

Retorno:
nenhum
********************************************************************/
function treatsControlsBars(btnClicked, folderID, tipoCtlrsPasta, dsoID)
{
    //@@
	var nCurrEstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,"dsoSup01.recordset['EstadoID'].value");
	var nCurrVersao = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['Versao'].value");
	var strBtns;
    var aStrBtns;
	
	txtEstudo.onclick = null;
	txtEstudo.onfocus = null;

	txtTexto.onclick = null;
	txtTexto.onfocus = null;
	
	if ( (nCurrEstadoID != null) && (nCurrEstadoID != 1) && (nCurrEstadoID != 91) && (nCurrEstadoID != 92) )
	{

		// Trava o botao de alteracao superior
	    strBtns = currentBtnsCtrlBarString('sup');
        aStrBtns = strBtns.split('');

        aStrBtns[3] = 'D';
        strBtns = aStrBtns.toString();
        re = /,/g;
        strBtns = strBtns.replace(re, '');

        adjustControlBarEx(window,'sup', strBtns);
	}

    if ( (btnClicked == 'SUPOK') || (btnClicked == 'SUPREFR') || 
         (btnClicked == 'SUPDET') || (btnClicked == 'INFREFR') || 
         (btnClicked == 'INFOK') || (btnClicked == 'INFCANC') )
    {
		// Se documento desativo
		if ( nCurrEstadoID == 4 )
			setupEspecBtnsControlBar('sup', 'HHDDH');
		else
			setupEspecBtnsControlBar('sup', 'HHHHH');
    }    
    else    
        setupEspecBtnsControlBar('sup', 'HDDDD');

	if ( (folderID == 26002) && (fg.Editable == false) )
		setupEspecBtnsControlBar('inf', 'HHDD');
	else if ( (folderID == 26006) && (fg.Editable == false) )
	{
		if ( fg.Rows > 1 )
		{
			if ( fg.Row >0 )
				setupEspecBtnsControlBar('inf', 'HDDD');
		}
	}
    else
        setupEspecBtnsControlBar('inf', 'DDDD');
}

/********************************************************************
Funcao disparada pelo frame work.
Propaga clique e botao de lupa que nao seja proprietario e
alternativo.
           
Parametros: 
btnClicked      - referencia ao botao de lupa clicado

Retorno:
nenhum
********************************************************************/
function btnLupaClicked(btnClicked)
{

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario inseriu uma linha nova no grid.
           
Parametros: 
folderID        - id da pasta corrente
grid            - referencia ao grid
nLineInGrid     - numero da linha inserida no grid
dso             - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function addedLineInGrid(folderID, grid, nLineInGrid, dso)
{
	if ( folderID == 26006 ) 
	{
		setupEspecBtnsControlBar('inf', 'DDDD');
	}
}

/********************************************************************
Funcao disparada pelo frame work.
Dispara logo apos abrir a maquina de estado. 
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
nenhum
********************************************************************/
function stateMachOpened( currEstadoID )
{
}

/********************************************************************
Funcao disparada pelo frame work.
Dispara se o usuario trocou o estado.
Dispara logo apos fechar a maquina de estado. 
           
Parametros: 
oldEstadoID     - previo EstadoID do registro
currEstadoID    - novo EstadoID do registro

Retorno:
nenhum
********************************************************************/
function stateMachClosed( oldEstadoID, currEstadoID )
{
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked)
{
	if ( (keepCurrFolder() == 26002) && (btnClicked == 1) && (fg.Rows > 1) )
	{
		fg_DblClick_Prg();
	}	
	// Referencias
	else if ( (keepCurrFolder() == 26002) && (btnClicked == 2) )
	{
		if (fg.Rows > 1)
		{
			var nDocumentoID = getCellValueByColKey(fg, 'DocumentoID', fg.Row);
			
			if ((nDocumentoID != null) && (nDocumentoID != ''))
				window.top.openModalControleDocumento('I', '', nDocumentoID, null, null, 'T');
		}
	}
	// Questionarios
	else if ( (keepCurrFolder() == 26006))
	{
		if ((btnClicked == 1) && (fg.Rows > 1))
		{
			var nDocQuestionarioID = getCellValueByColKey(fg, 'DocQuestionarioID', fg.Row);
			openModalDocQuestionario(nDocQuestionarioID);
		}
	}
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou o botao OK do control bar inferior.
QUER GRAVAR BULLET E TAB EM CAMPO TEXTAREA

Parametros: 
controlBar      - 'SUP' ou 'INF'

Retornos:
null            - nao grava, qualquer valor difetente de null grava
********************************************************************/
function saveBulletAndTabInTextArea(controlBar, btnClicked, folderID) 
{
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou um botao nao especifico do control bar.

Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - Um dos seguintes botoes:
                - Avancar, retroagir, incluir, alterar, estado, 
                  excluir, OK, cancelar refresh, anterior e proximo. 
                  (Testar a string que vem).

Retornos:
null            - a automacao prossegue. Qualquer outro retorno a
                  automacao nao prossegue.
********************************************************************/
function btnBarNotEspecClicked(controlBar, btnClicked)
{
    // Para prosseguir a automacao retornar null
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2)
{
    if (idElement.toUpperCase() == 'MODALCONTROLEDOCUMENTOHTML')
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALALTERNATIVASHTML')
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou o botao de alteracao em uma pasta de grid. 
           
Parametros: 
folderID    - id da pasta corrente

Retorno:
nenhum
********************************************************************/
function btnAltPressedInGrid( folderID )
{
	if ( folderID == 26006 ) 
	{
		setupEspecBtnsControlBar('inf', 'DDDD');
	}
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou o botao de alteracao em uma pasta que nao e grid. 
           
Parametros: 
folderID    - id da pasta corrente

Retorno:
nenhum
********************************************************************/
function btnAltPressedInNotGrid( folderID )
{
    ;
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario selecionou uma pasta no control bar inferior.
Nesta funcao o programador preenche o combo de filtros inferiores com 
dados
particulares do form ou deixa o frame work preencher com o combo de
filtros inferiores com os dados dos filtros inferiores.
           
Parametros: 
folderID    - id da pasta corrente

Retorno:
true    - o frame work opera automaticamente
false   - o programador preenche o combo manualmente
********************************************************************/
function fillCmbFiltsInfAutomatic(folderID)
{
    //@@ Retornar true se nao ha preenchimento manual.
    // Retornar false se ha.
    return true;
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario selecionou uma pasta no control bar inferior.
Esta funcao executa antes de qualquer operacao do frame work           

Parametros: 
optText             - texto do option do combo
optValue            - value do option do combo
optIndex            - indice do option do combo
nQtyGridsInFolder   - 0 nao tem grid, 1 ou 2

Retorno:
qualquer coisa diferente de null para a automacao
********************************************************************/
function selPastaChanged(optText, optValue, optIndex, nQtyGridsInFolder)
{
    //@@ retornar null para prosseguir a automacao
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
O frame work esta prestes a setar os botoes da barra infeior, em funcao
dos direitos do usuario logado

Parametros: 
folderID    - pasta corrente

Retorno:
indiferente para a automacao
********************************************************************/
function frameIsAboutToApplyRights(folderID)
{
    // Pasta de LOG Read Only
    if ( (folderID == 20010) || (folderID == 26002) )
    {
        // trava as barras do inf para a pasta acima definida
        glb_BtnsIncAltEstExcl[0] = true;
        glb_BtnsIncAltEstExcl[1] = true;
        glb_BtnsIncAltEstExcl[2] = true;
        glb_BtnsIncAltEstExcl[3] = true;
    }
    else
    {
        // destrava as barras do inf para a pasta acima definida
        glb_BtnsIncAltEstExcl[0] = null;
        glb_BtnsIncAltEstExcl[1] = null;
        glb_BtnsIncAltEstExcl[2] = null;
        glb_BtnsIncAltEstExcl[3] = null;
    }
    return null;
}
/********************************************************************
Funcao criada pelo programador.
Verifica a �ltima data de Alteracao do registro

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function verificaDataUltimaAlteracao()
{
	lockInterface(true);

	var nDocumentoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,"dsoSup01.recordset['DocumentoID'].value");

    setConnection(dsoDataUltimaAlteracao);
    
    dsoDataUltimaAlteracao.SQL = 'SELECT dtUltimaAlteracao ' +
									'FROM Documentos WITH(NOLOCK) ' +
									'WHERE (DocumentoID = ' + nDocumentoID + ')';

    dsoDataUltimaAlteracao.ondatasetcomplete = verificaDataUltimaAlteracao_DSC;

    dsoDataUltimaAlteracao.refresh();
}
/********************************************************************
Funcao criada pelo programador.
Retorno de verificaDataUltimaAlteracao

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function verificaDataUltimaAlteracao_DSC() {
    var dtLocal;	
	var dtServidor;

	lockInterface(false);

	if (dsoDataUltimaAlteracao.recordset.BOF && dsoDataUltimaAlteracao.recordset.EOF)
	{
		if ( window.top.overflyGen.Alert('Este documento foi removido.') == 0 )
		    return null;
	}
	else
	{
		dtLocal = new Date(dso01JoinSup.recordset['dtUltimaAlteracao'].value).toString();
		dtServidor = new Date(dsoDataUltimaAlteracao.recordset['dtUltimaAlteracao'].value).toString();
		
		if (dtLocal != dtServidor)	
		{
			if ( window.top.overflyGen.Alert('Este documento foi alterado por outro usu�rio.') == 0 )
			    return null;
		}
		else		
			saveRegister_1stPart();
	}
}

function openModalDocQuestionario(nDocQuestionarioID)
{
    var strPars = new String();
    strPars = '?nDocQuestionarioID=' + escape(nDocQuestionarioID);

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_ASPURLROOT + '/modqualidade/submodiso/documentos/modalpages/modalalternativas.asp'+strPars;
    
    showModalWin(htmlPath, new Array(650,420));
}