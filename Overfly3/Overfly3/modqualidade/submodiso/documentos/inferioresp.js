/********************************************************************
especificinf.js
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_nFormID = window.top.formID;
var glb_nColPergunta = 0;
var glb_nColPeso = 0;
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

    setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
    setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
    fillCmbPastasInCtlBarInf(nTipoRegistroID)    
    criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)
    constructGridBody(folderID, currDSO)    

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Esta funcao seta o sql do dso para buscar o registro a ser mostrado no detalhe,
vai ao banco de dados no servidor e chama a funcao de ondatasetcomplete de acordo 
com o parametro recebido.
           
Parametros: 
dso                        -> dso a ser aberto
folderID                   -> id da pasta corrente no control bar inferior
idToFind                   -> id do registro corrente no sup
sFiltro                    -> string do filtro corrente no control bar inferior
nTipoRegistroID            -> tipo do registro corrente
Retorno:
nulo
********************************************************************/
function setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
{
    var nEmpresaData = getCurrEmpresaData();

    setConnection(dso);
    
    //@@
    if ( (folderID == 20008) || (folderID == 20009) ) // Prop/Altern ou Observacoes
    {
        dso.SQL = 'SELECT a.DocumentoID, a.ProprietarioID,a.AlternativoID,a.Observacoes,a.dtUltimaAlteracao FROM Documentos a WITH(NOLOCK) WHERE ' +
        sFiltro + 'a.DocumentoID = ' + idToFind;
        return 'dso01JoinSup_DSC';
    }
    // LOG
    else if (folderID == 20010)
    {
        dso.SQL = 'SELECT *, dbo.fn_Data_Fuso(a.Data, NULL, ' + nEmpresaData[2] + ') AS DataFuso ' +
                  'FROM LOGs a WITH(NOLOCK) ' + 
                  'WHERE ' + sFiltro + ' a.RegistroID = '+ idToFind + ' ' +
                  'AND a.FormID=' + glb_nFormID + ' ' +
                  'ORDER BY a.SubFormID ASC, a.Data DESC';
        return 'dso01Grid_DSC';
    }
    // Responsabilidades
    else if (folderID == 26001)
    {
        dso.SQL = 'SELECT * ' +
                  'FROM Documentos_Perfis a WITH(NOLOCK) ' + 
                  'WHERE ' + sFiltro + ' a.DocumentoID = '+ idToFind + ' ' +
                  'ORDER BY a.Aprovacao DESC, a.Estudo DESC, a.PerfilID';
        return 'dso01Grid_DSC';
    }
    // Referencias
    else if (folderID == 26002)
    {
        dso.SQL = 'EXEC sp_Documentos_Dados ' + idToFind + ', NULL, 2 ';
        return 'dso01Grid_DSC';
    }
    // Mapeamento de Imagem
    else if (folderID == 26005)
    {
        dso.SQL = 'SELECT a.* ' +
                  'FROM Documentos_Mapeamentos a WITH(NOLOCK) ' + 
                  'WHERE ' + sFiltro + ' a.DocumentoID = '+ idToFind + ' ' +
                  'ORDER BY a.DocumentoAncoraID, a.Ancora';
        return 'dso01Grid_DSC';
    }
    // Questionario
    else if (folderID == 26006)
    {
        dso.SQL = 'SELECT a.* ' +
                  'FROM Documentos_Questionarios a WITH(NOLOCK) ' + 
                  'WHERE ' + sFiltro + ' a.DocumentoID = '+ idToFind + ' ' +
                  'ORDER BY a.Ordem';
        return 'dso01Grid_DSC';
    }
    // Codigos Amostra
    else if (folderID == 26007)
    {
        dso.SQL = 'SELECT a.* ' +
                  'FROM Documentos_CodigosAmostra a WITH(NOLOCK) ' + 
                  'WHERE ' + sFiltro + ' a.DocumentoID = '+ idToFind + ' ' +
                  'ORDER BY a.NivelInspecao, a.QuantidadeLote, a.CodigoLiteral';
        return 'dso01Grid_DSC';
    }
    // Plano Amostragem
    else if (folderID == 26008)
    {
        dso.SQL = 'SELECT a.* ' +
                  'FROM Documentos_PlanoAmostragem a WITH(NOLOCK) ' + 
                  'WHERE ' + sFiltro + ' a.DocumentoID = '+ idToFind + ' ' +
                  'ORDER BY a.RegimeInspecaoID, a.NivelQualidadeAceitavel, a.CodigoLiteral';
        return 'dso01Grid_DSC';
    }
}

/********************************************************************
Monta string de select para combo dentro de grid

Parametros:
dso                 - referencia ao dso corrente
pastaID             - pasta corrente
nRegistroID         - id do registro corrente
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
{
    setConnection(dso);
    
    var nDocumentoID = 0;
    
    // LOG
    if (pastaID == 20010)
    {
        if ( dso.id == 'dso01GridLkp' )
        {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoFantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.SubFormID = b.RecursoID)';
        }
        else if ( dso.id == 'dso02GridLkp' )
        {
            dso.SQL = 'SELECT b.PessoaID, b.Fantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Pessoas b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.UsuarioID = b.PessoaID)';
        }
        else if ( dso.id == 'dso03GridLkp' )
        {
            dso.SQL = 'SELECT b.ItemID, b.ItemAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), TiposAuxiliares_Itens b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EventoID = b.ItemID)';
        }
        else if ( dso.id == 'dso04GridLkp' )
        {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EstadoID = b.RecursoID)';
        }
    }
    // Responsabilidades
    else if (pastaID == 26001)
    {
        if ( dso.id == 'dsoCmb01Grid_01' )
        {
            dso.SQL = 'SELECT RecursoID, RecursoFantasia ' +
                      'FROM Recursos WITH(NOLOCK) ' +
                      'WHERE (EstadoID=2 AND TipoRecursoID = 6) ' +
			          'ORDER BY RecursoFantasia';
        }
    }
    // Mapeamento de Imagens
    else if (pastaID == 26005)
    {
        if ( dso.id == 'dsoCmb01Grid_01' )
        {
            dso.SQL = 'SELECT NULL AS DocumentoID, SPACE(0) AS Documento ' +
					  'UNION ALL ' +
					  'SELECT DocumentoID, Documento ' +
                      'FROM Documentos WITH(NOLOCK) ' +
                      'WHERE (EstadoID NOT IN (1, 4)) ' +
			          'ORDER BY DocumentoID ';
        }
    }
    // Questionario
    else if (pastaID == 26006)
    {
        if ( dso.id == 'dsoCmb01Grid_01' )
        {
            dso.SQL = 'SELECT ItemID AS NormaID, ItemMasculino AS Norma, ItemAbreviado ' +
                      'FROM TiposAuxiliares_Itens WITH(NOLOCK) ' +
                      'WHERE (EstadoID = 2 AND TipoID=602) ' +
			          'ORDER BY Ordem';
        }
        else if ( dso.id == 'dsoCmb01Grid_02' )
        {
            dso.SQL = 'SELECT CHAR(97) AS Letra ' +
							'UNION ALL SELECT CHAR(98) AS Letra ' +
							'UNION ALL SELECT CHAR(99) AS Letra ' +
							'UNION ALL SELECT CHAR(100) AS Letra ' +
							'UNION ALL SELECT CHAR(101) AS Letra ' +
							'UNION ALL SELECT CHAR(102) AS Letra ' +
							'UNION ALL SELECT CHAR(103) AS Letra ';
        }
        else if ( dso.id == 'dsoCmb01Grid_03' )
        {
			nDocumentoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL ,"dsoSup01.recordset['DocumentoID'].value");

            dso.SQL = 'SELECT 0 AS DocQuestionarioID, SPACE(1) AS Referencia ' +
					  'UNION ALL ' +
					  'SELECT a.DocQuestionarioID, CONVERT(VARCHAR(10), a.DocQuestionarioID) + ' +
					  'ISNULL(SPACE(1) + b.ItemAbreviado, SPACE(0)) + ISNULL(SPACE(1) + a.Letra,SPACE(0)) AS Referencia ' +
					  'FROM Documentos_Questionarios a WITH(NOLOCK) ' +
					        'LEFT OUTER JOIN TiposAuxiliares_Itens b WITH(NOLOCK) ON (a.ItemNormaID = b.ItemID) ' +
					  'WHERE (a.DocumentoID=' + nDocumentoID + ')';
        }
    }
    // Plano Amostragem
    else if (pastaID == 26008)
    {
        if ( dso.id == 'dsoCmb01Grid_01' )
        {
            dso.SQL = 'SELECT ItemID , ItemAbreviado, ItemMasculino ' +
                      'FROM TiposAuxiliares_Itens WITH(NOLOCK) ' +
                      'WHERE (EstadoID = 2 AND TipoID=414) ' +
			          'ORDER BY Ordem';
        }
        else if ( dso.id == 'dsoCmb01Grid_02' )
        {
            dso.SQL = 'SELECT DISTINCT CodigoLiteral ' +
					  'FROM Documentos_CodigosAmostra WITH(NOLOCK) ' +
					  'WHERE DocumentoID=743' +
					  'ORDER BY CodigoLiteral';
        }
	}
}

/********************************************************************
Esta funcao preenche o combo de pastas do controlbar inferior

Parametros:
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function fillCmbPastasInCtlBarInf(nTipoRegistroID)
{
    var i=0;
    var currSelection = getCmbCurrDataInControlBar('inf', 1);
    var vPastaName;
    var nDocumentoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL ,"dsoSup01.recordset['DocumentoID'].value");
    var nEstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL ,"dsoSup01.recordset['EstadoID'].value");
    var bTemQuestionario = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL ,"dsoSup01.recordset['TemQuestionario'].value");

	cleanupSelInControlBar('inf',1);

	vPastaName = window.top.getPastaName(20008);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20008); //Observa��es

    vPastaName = window.top.getPastaName(20009);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20009); //Proprietario

    vPastaName = window.top.getPastaName(20010);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20010); //LOG

    vPastaName = window.top.getPastaName(26001);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 26001); //Responsabilidades

    vPastaName = window.top.getPastaName(26005);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 26005); //Mapeamento da Imagem

    vPastaName = window.top.getPastaName(26002);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 26002); //Referencias

	if (bTemQuestionario)
	{
		vPastaName = window.top.getPastaName(26006);
		if (vPastaName != '')
			addOptionToSelInControlBar('inf', 1, vPastaName, 26006); //Questionarios
	}	

	if (nDocumentoID == 743)
	{
		vPastaName = window.top.getPastaName(26007);
		if (vPastaName != '')
			addOptionToSelInControlBar('inf', 1, vPastaName, 26007); //CodigosAmostra

		vPastaName = window.top.getPastaName(26008);
		if (vPastaName != '')
			addOptionToSelInControlBar('inf', 1, vPastaName, 26008); //Plano Amostragem
	}	

    // Nao mexer - Inicio de automacao ============================== 	
	// Tenta selecionar novamente a pasta anterior selecionada no combo
	// Se nao mais existe a pasta anteriormente selecionada no combo
	// seleciona a pasta default
	
	stripFoldersByRight(currSelection);
        
    return true;
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Tratar consistencia dos dados inseridos pelo usuario aqui.
           
Parametros: 
btnClicked      - controlbar e botao clicado pelo usuario
folderID        - id da pasta selecionada
tipoCtlrsPasta  - 'GRID' a pasta e um grid/ 'CTRLS' a pasta nao e
                  um grid
currDSO         - referencia dso corrente
registroID      - id do registro corrente no sup

Retorno:
true se os dados estao ok, caso contrario false.
********************************************************************/
function criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)
{
    //@@ return true se tudo ok, caso contrario return false;
 
    var currLine = 0;

    // A pasta e grid    
    if (tipoCtlrsPasta == 'GRID') // se � grid
    {
        if (folderID == 26001) // Responsabilidades
            currLine = criticLineGridAndFillDsoEx(fg, currDSO,keepCurrLineInGrid(),[0], ['DocumentoID', registroID]);
		else if (folderID == 26005) // Mapeamento de Imagens
            currLine = criticLineGridAndFillDsoEx(fg, currDSO,keepCurrLineInGrid(),[2, 3, 4, 5], ['DocumentoID', registroID]);
		else if (folderID == 26006) // Questionarios
            currLine = criticLineGridAndFillDsoEx(fg, currDSO,keepCurrLineInGrid(),[glb_nColPergunta, glb_nColPeso], ['DocumentoID', registroID]);
		else if (folderID == 26007) // Codigos Amostra
            currLine = criticLineGridAndFillDsoEx(fg, currDSO,keepCurrLineInGrid(),[0, 1, 2], ['DocumentoID', registroID]);
		else if (folderID == 26008) // Plano Amostragem
            currLine = criticLineGridAndFillDsoEx(fg, currDSO,keepCurrLineInGrid(),[0, 1, 2, 3, 4, 5], ['DocumentoID', registroID]);

        if (currLine < 0)
            return false;
    }

    return true;
}

/********************************************************************
Constroi o corpo de um grid. Os combos sao construidos pelo frame work
           
Parametros: 
folderID        - id da pasta corrente
currDSO         - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function constructGridBody(folderID, currDSO)
{
    var dTFormat = '';
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';
    
    fg.FrozenCols = 0;
    
    // LOG
    if (folderID == 20010)
    {
        headerGrid(fg,['SubForm',
                       'Data           Hora     ',
                       'Colaborador',
                       'Evento',
                       'Est',
                       'Motivo',
                       'LOGID'], [6]);
                       
        fillGridMask(fg,currDSO,['^SubFormID^dso01GridLkp^RecursoID^RecursoFantasia',
                                 'DataFuso',
                                 '^UsuarioID^dso02GridLkp^PessoaID^Fantasia',
                                 '^EventoID^dso03GridLkp^ItemID^ItemAbreviado',
                                 '^EstadoID^dso04GridLkp^RecursoID^RecursoAbreviado',
                                 'Motivo',
                                 'LOGID'],
                                 ['','99/99/9999','','','','',''],
                                 ['',dTFormat + ' hh:mm:ss','','','','','']);

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
    // Responsabilidades
    else if (folderID == 26001)
    {
        headerGrid(fg,['Perfil',
                       'Aprova��o',
                       'Estudo',
                       'Execu��o',
                       'DocPerfilID'], [4]);

        // array de celulas com hint
        glb_aCelHint = [[0,1,'Respons�vel pela aprova��o do documento?'],
						[0,2,'Participa do grupo de estudo?'],
						[0,3,'Respons�vel pela execu��o/cumprimento do documento?']];

        fillGridMask(fg,currDSO,['PerfilID',
								 'Aprovacao',
								 'Estudo',
								 'Execucao',
								 'DocPerfilID'],
								 ['','','','',''],
                                 ['','','','','']);

        alignColsInGrid(fg,[]);                           
		fg.FrozenCols = 1;
    }
    // Referencias
    else if (folderID == 26002)
    {
        headerGrid(fg,['Tipo de Refer�ncia',
                       'Tipo de Documento',
                       'ID',
                       'Abrev',
                       'Documento',
                       'Item'], []);

        fillGridMask(fg,currDSO,['TipoReferencia',
								 'TipoDocumento',
								 'DocumentoID',
								 'DocumentoAbreviado',
								 'Documento',
								 'Norma'],
								 ['','','','','',''],
                                 ['','','','','','']);

        alignColsInGrid(fg,[2]);                           
		fg.FrozenCols = 1;

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
    // Mapeamento de Imagens
    else if (folderID == 26005)
    {
        headerGrid(fg,['Documento',
					   '�ncora',
                       'X1',
                       'Y1',
                       'X2',
                       'Y2',
                       'Hint',
                       'DocMapeamentoID'], [7]);

        fillGridMask(fg,currDSO,['DocumentoAncoraID',
 								 'Ancora',
								 'X1',
								 'Y1',
								 'X2',
								 'Y2',
								 'Hint',
								 'DocMapeamentoID'],
								 ['9999999999','','9999','9999','9999','9999','','']);

        alignColsInGrid(fg,[0, 1, 2, 3, 4, 5]);                           
		fg.FrozenCols = 1;
    }
    // Questionarios
    else if (folderID == 26006)
    {
		var nDocumentoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,"dsoSup01.recordset['DocumentoID'].value");

		var aHeader = new Array();
		var aFields = new Array();
		var aMaskShow = new Array();
		var aMaskEdit = new Array();
		var aHoldCols = new Array();
		var nCounter = 0;
		var nCounterHolds = 0;

		aHeader[nCounter] = 'ID';
		aFields[nCounter] = 'DocQuestionarioID*';
		aMaskShow[nCounter] = '9999999999';
		aMaskEdit[nCounter++] = '##########';

		if (nDocumentoID != 822001)
		{
			aHoldCols[nCounterHolds++] = nCounter;
		}

		aHeader[nCounter] = 'Norma';
		aFields[nCounter] = 'ItemNormaID';
		aMaskShow[nCounter] = '';
		aMaskEdit[nCounter++] = '';

		if (nDocumentoID != 822001)
		{
			aHoldCols[nCounterHolds++] = nCounter;
		}

		aHeader[nCounter] = 'Letra';
		aFields[nCounter] = 'Letra';
		aMaskShow[nCounter] = '';
		aMaskEdit[nCounter++] = '';

		aHeader[nCounter] = 'Ordem';
		aFields[nCounter] = 'Ordem';
		aMaskShow[nCounter] = '999';
		aMaskEdit[nCounter++] = '###';

		aHeader[nCounter] = 'Refer�ncia';
		aFields[nCounter] = 'ReferenciaID';
		aMaskShow[nCounter] = '';
		aMaskEdit[nCounter++] = '';

		glb_nColPergunta = nCounter;
		aHeader[nCounter] = 'Pergunta';
		aFields[nCounter] = 'Pergunta';
		aMaskShow[nCounter] = '';
		aMaskEdit[nCounter++] = '';

		glb_nColPeso = nCounter;
		aHeader[nCounter] = 'Peso';
		aFields[nCounter] = 'Peso';
		aMaskShow[nCounter] = '999.99';
		aMaskEdit[nCounter++] = '###.##';

		aHeader[nCounter] = 'Coment�rios';
		aFields[nCounter] = 'Comentarios';
		aMaskShow[nCounter] = '';
		aMaskEdit[nCounter++] = '';

		aHoldCols[nCounterHolds++] = nCounter;
		aHeader[nCounter] = 'DocQuestionarioID';
		aFields[nCounter] = 'DocQuestionarioID';
		aMaskShow[nCounter] = '';
		aMaskEdit[nCounter++] = '';

        headerGrid(fg, aHeader, aHoldCols);
        fillGridMask(fg,currDSO, aFields, aMaskShow, aMaskEdit);

		alignColsInGrid(fg,[0, 3, 6]);

		fg.FrozenCols = 1;
    }
    // Codigos Amostra
    else if (folderID == 26007)
    {
        headerGrid(fg,['N�vel',
					   'Lote',
                       'CL',
                       'DocCodAmostraID'], [3]);

        glb_aCelHint = [[0,2,'C�digo Literal']];

        fillGridMask(fg,currDSO,['NivelInspecao',
								 'QuantidadeLote',
								 'CodigoLiteral',
								 'DocCodAmostraID'],
								 ['9','999999','',''],
								 ['#','######','','']);

        alignColsInGrid(fg,[0, 1]);                           
		fg.FrozenCols = 1;
    }
    // Plano Amostragem
    else if (folderID == 26008)
    {
        headerGrid(fg,['RI',
					   'NQA',
                       'CL',
                       'Amostra',
                       'Aceita��o',
                       'Rejei��o',
                       'DocPlanAmostragemID'], [6]);

        glb_aCelHint = [[0,0,'Regime de Inspe��o'],
						[0,1,'N�vel de Qualidade Aceit�vel'],
						[0,3,'C�digo Literal']];

        fillGridMask(fg,currDSO,['RegimeInspecaoID',
								 'NivelQualidadeAceitavel',
							  	 'CodigoLiteral',
								 'QuantidadeAmostra',
								 'QuantidadeAceitacao',
								 'QuantidadeRejeicao',
								 'DocPlanAmostragemID'],
								 ['','999.99','','999999','999999','999999',''],
								 ['','###.##','','######','######','######','']);

        alignColsInGrid(fg,[1, 3, 4, 5]);
		fg.FrozenCols = 1;
    }
    
}
