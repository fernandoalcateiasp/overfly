<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<!-- //@@ ID e name do arquivo -->
<html id="qafsup01Html" name="qafsup01Html">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/clientside/form.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_interfaceex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_detailsup.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_common.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_formscontrolsbar.js" & Chr(34) & "></script>" & vbCrLf                
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modqualidade/commonqualidade/commonqualidade.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modqualidade/submodiso/qaf/superior.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modqualidade/submodiso/qaf/superioresp.js" & Chr(34) & "></script>" & vbCrLf
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

</head>

<!-- //@@ -->
<body id="qafsup01Body" name="qafsup01Body" LANGUAGE="javascript" onload="return window_onload()">
    <!-- Controle para simular tab na gravacao -->
    <input type="text" id="txtNulo" name="txtNulo" class="fldGeneral">
    
    <!-- //@@ Os divs sao definidos de acordo com o form -->
    <!-- //@@ Todos os controles select cujo onchange e invocado convergem para -->
    <!-- a funcao optChangedInCmb(cmb), que esta no sup01.js" -->
    
    <!-- Div principal superior -->
    <div id="divSup01_01" name="divSup01_01" class="divExtern">
        <!-- Os dois controles RegistroID e EstadoID abaixo sao comuns a todos os forms -->
        <p id="lblRegistroID" name="lblRegistroID" class="lblGeneral">ID</p>
        <input type="text" id="txtRegistroID" name="txtRegistroID" class="fldGeneral">
        <p id="lblEstadoID" name="lblEstadoID" class="lblGeneral">Est</p>
        <input type="text" id="txtEstadoID" name="txtEstadoID" DATASRC="#dsoStateMachine" DATAFLD="Estado" class="fldGeneral">
        <p id="lbldtEmissao" name="lbldtEmissao" class="lblGeneral">Data</p>
        <input type="text" id="txtdtEmissao" name="txtdtEmissao" DATASRC="#dsoSup01" DATAFLD="V_dtEmissao" class="fldGeneral"></input>
        <p id="lbldtVencimento" name="lbldtVencimento" class="lblGeneral">Vencimento</p>
        <input type="text" id="txtdtVencimento" name="txtdtVencimento" DATASRC="#dsoSup01" DATAFLD="V_dtVencimento" class="fldGeneral"></input>
        <p id="lblObservacao" name="lblObservacao" class="lblGeneral">Observa��o</p>
        <input type="text" id="txtObservacao" name="txtObservacao" DATASRC="#dsoSup01" DATAFLD="Observacao" class="fldGeneral"></input>
        <p id="lblFornecedorID" name="lblFornecedorID" class="lblGeneral">Fornecedor</p>
		<select id="selFornecedorID" name="selFornecedorID" DATASRC="#dsoSup01" DATAFLD="FornecedorID" class="fldGeneral"></select>
		<input type="image" id="btnFindCliente" name="btnFindCliente" class="fldGeneral" title="Preencher combo" WIDTH="24" HEIGHT="23">		
        <p id="lblContatoID" name="lblContatoID" class="lblGeneral">Contato</p>
		<select id="selContatoID" name="selContatoID" DATASRC="#dsoSup01" DATAFLD="ContatoID" class="fldGeneral"></select>
		<input type="image" id="btnFindContato" name="btnFindContato" class="fldGeneral" title="Preencher combo" WIDTH="24" HEIGHT="23">
        <p id="lblContato" name="lblContato" class="lblGeneral">Contato</p>
        <input type="text" id="txtContato" name="txtContato" DATASRC="#dsoSup01" DATAFLD="Contato" class="fldGeneral"></input>
        <p id="lblDDI" name="lblDDI" class="lblGeneral">DDI</p>
        <input type="text" id="txtDDI" name="txtDDI" DATASRC="#dsoSup01" DATAFLD="DDI" class="fldGeneral"></input>
        <p id="lblDDD" name="lblDDD" class="lblGeneral">DDD</p>
        <input type="text" id="txtDDD" name="txtDDD" DATASRC="#dsoSup01" DATAFLD="DDD" class="fldGeneral"></input>
        <p id="lblTelefone" name="lblTelefone" class="lblGeneral">Telefone</p>
        <input type="text" id="txtTelefone" name="txtTelefone" DATASRC="#dsoSup01" DATAFLD="Telefone" class="fldGeneral"></input>
        <p id="lblEMail" name="lblEMail" class="lblGeneral">Email</p>
        <input type="text" id="txtEMail" name="txtEMail" DATASRC="#dsoSup01" DATAFLD="EMail" class="fldGeneral"></input>
        <p id="lblPontuacao" name="lblPontuacao" class="lblGeneral">Pontua��o</p>
        <input type="text" id="txtPontuacao" name="txtPontuacao" DATASRC="#dsoSup01" DATAFLD="Pontuacao" class="fldGeneral"></input>
        <p  id="lblTemISO" name="lblTemISO" class="lblGeneral">ISO</p>
        <input type="checkbox" id="chkTemISO" name="chkTemISO" DATASRC="#dsoSup01" DATAFLD="TemISO" class="fldGeneral" title="Tem ISO?"></input>
		<p id="lblNormaISO" name="lblNormaISO" class="lblGeneral">Norma ISO</p>
		<input type="text" id="txtNormaISO" name="txtNormaISO" DATASRC="#dsoSup01" DATAFLD="NormaISO" class="fldGeneral"></input>
		<p id="lblNumeroCertificado" name="lblNumeroCertificado" class="lblGeneral">N�mero Certificado</p>
		<input type="text" id="txtNumeroCertificado" name="txtNumeroCertificado" DATASRC="#dsoSup01" DATAFLD="NumeroCertificado" class="fldGeneral"></input>
		<p id="lblOrganismoCertificador" name="lblOrganismoCertificador" class="lblGeneral">Organismo Certificador</p>
		<input type="text" id="txtOrganismoCertificador" name="txtOrganismoCertificador" DATASRC="#dsoSup01" DATAFLD="OrganismoCertificador" class="fldGeneral"></input>
		<p id="lbldtCertificacao" name="lbldtCertificacao" class="lblGeneral">Certifica��o</p>
		<input type="text" id="txtdtCertificacao" name="txtdtCertificacao" DATASRC="#dsoSup01" DATAFLD="V_dtCertificacao" class="fldGeneral"></input>
		<p id="lbldtValidade" name="lbldtValidade" class="lblGeneral">Validade</p>
		<input type="text" id="txtdtValidade" name="txtdtValidade" DATASRC="#dsoSup01" DATAFLD="V_dtValidade" class="fldGeneral"></input>
		<p id="lblEscopo" name="lblEscopo" class="lblGeneral">Escopo</p>
		<input type="text" id="txtEscopo" name="txtEscopo" DATASRC="#dsoSup01" DATAFLD="Escopo" class="fldGeneral"></input>
		<p id="lblExclusoes" name="lblExclusoes" class="lblGeneral">Exclus�es</p>
		<input type="text" id="txtExclusoes" name="txtExclusoes" DATASRC="#dsoSup01" DATAFLD="Exclusoes" class="fldGeneral"></input>
    </div>
    
</body>

</html>
