using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;
using Overfly3.systemEx.serverside;

namespace Overfly3.serversidegenEx.emails
{
	public partial class execradprocedures : System.Web.UI.OverflyPage
	{
		private string procedure;
		private string alert;

		protected override void PageLoad(object sender, EventArgs e)
		{
			Procedure();
			
			WriteResultXML(DS);
		}

		protected DataSet DS
		{
			get
			{
				return DataInterfaceObj.getRemoteData(
						"select '" + alert + "' as fldResponse"
					);
			}
		}

		protected void Procedure()
		{
			ProcedureParameters[] param = null;
			
			if(tipo.intValue() == 1) 
			{
				param = new ProcedureParameters[1];
				
				param[0] = new ProcedureParameters("@RADID", SqlDbType.Int, radID.intValue());
			}
			else if (tipo.intValue() == 2)
			{
				param = new ProcedureParameters[2];
				
				param[0] = new ProcedureParameters("@RADID", SqlDbType.Int, radID.intValue());
				param[1] = new ProcedureParameters("@TipoResultadoID", SqlDbType.Int, 1);
			}
			
			DataInterfaceObj.execNonQueryProcedure(procedure, param);
		}
		
		protected Integer tipo = Constants.INT_ZERO;
		protected Integer nTipo
		{
			get { return tipo; }
			set
			{
				if (value != null) tipo = value;
				
				if(tipo.intValue() == 1)
				{
					procedure = "sp_RAD_Documentos";
					alert = "Documentos sincronizados.";
				}
				else if(tipo.intValue() == 2)
				{
					procedure = "sp_RAD_Indicadores";
					alert = "Indicadores sincronizados.";
				}
			}
		}

		protected Integer radID = Constants.INT_ZERO;
		protected Integer nRADID
		{
			get { return radID; }
			set { if (value != null) radID = value; }
		}
	}
}
