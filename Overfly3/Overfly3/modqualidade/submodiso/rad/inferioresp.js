/********************************************************************
inferioresp.js

Library javascript para o inferior.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_nFormID = window.top.formID;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

FUNCOES DA AUTOMACAO:	
    setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
	setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
	fillCmbPastasInCtlBarInf(nTipoRegistroID)

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

// FUNCOES DA AUTOMACAO *********************************************

/********************************************************************
Funcao disparada pelo frame work.
Chamada pela funcao filtroComplete_DSC(emptyFilter) do js_detailinf.js

Esta funcao seta o sql do dso para buscar o registro a ser mostrado no detalhe,
vai ao banco de dados no servidor e no retorno chama a funcao de
ondatasetcomplete de acordo com o parametro recebido.
           
Parametros: 
dso                        -> dso a ser aberto
folderID                   -> id da pasta corrente no control bar inferior
idToFind                   -> id do registro corrente no sup
sFiltro                    -> string do filtro corrente no control bar inferior
nTipoRegistroID            -> tipo do registro corrente
Retorno:
nulo
********************************************************************/
function setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
{
    var nEmpresaData = getCurrEmpresaData();

    setConnection(dso);
    
    //@@
    if ( (folderID == 20008) || (folderID == 20009) ) // Prop/Altern ou Observacoes
    {
        dso.SQL = 'SELECT a.RADID,a.ProprietarioID,a.AlternativoID,a.Observacoes FROM RAD a WITH(NOLOCK) WHERE '+
        sFiltro + 'a.RADID = ' + idToFind;
        return 'dso01JoinSup_DSC';
    }
    // Texto
    else if (folderID == 26061)
    {
        dso.SQL = 'SELECT a.RADID,a.ProprietarioID,a.AlternativoID,a.Texto FROM RAD a WITH(NOLOCK) WHERE ' +
        sFiltro + 'a.RADID = ' + idToFind;
        return 'dso01JoinSup_DSC';
    }
    // LOG
    else if (folderID == 20010)
    {
        dso.SQL = 'SELECT *, dbo.fn_Data_Fuso(a.Data, NULL, ' + nEmpresaData[2] + ') AS DataFuso ' +
                  'FROM LOGs a WITH(NOLOCK) ' + 
                  'WHERE ' + sFiltro + ' a.RegistroID = '+ idToFind + ' ' +
                  'AND a.FormID=' + glb_nFormID + ' ' +
                  'ORDER BY a.SubFormID ASC, a.Data DESC';
        return 'dso01Grid_DSC';
    }
    // Participantes
    else if (folderID == 26062)
    {
        dso.SQL = 'SELECT * ' +
                  'FROM RAD_Participantes a WITH(NOLOCK) ' + 
                  'WHERE ' + sFiltro + ' a.RADID = '+ idToFind + ' ' +
                  'ORDER BY (SELECT b.Fantasia FROM Pessoas b WITH(NOLOCK) WHERE b.PessoaID=a.PessoaID)';
        return 'dso01Grid_DSC';
    }
    // Documentos Analisados
    else if (folderID == 26063)
    {
        dso.SQL = 'SELECT * ' +
                  'FROM RAD_Documentos a WITH(NOLOCK) ' + 
                  'WHERE ' + sFiltro + ' a.RADID = '+ idToFind + ' ' +
                  'ORDER BY a.DocumentoID, a.RegistroID';
        return 'dso01Grid_DSC';
    }
    // Indicadores da Qualidade
    else if (folderID == 26064)
    {
        dso.SQL = 'SELECT * ' +
                  'FROM RAD_Indicadores a WITH(NOLOCK) ' + 
                  'WHERE ' + sFiltro + ' a.RADID = '+ idToFind + ' ' +
                  'ORDER BY (SELECT c.Ordem FROM RIQ b WITH(NOLOCK), TiposAuxiliares_Itens c WITH(NOLOCK) WHERE a.RIQID = b.RIQID AND b.IndicadorID = c.ItemID)';
        return 'dso01Grid_DSC';
    }
    // Documentos Relacionados
    else if (folderID == 26065)
    {
        dso.SQL = 'EXEC sp_DocumentosRelacionados 560001, ' + idToFind;
        return 'dso01Grid_DSC';
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pela funcao dso01Grid_DSC() do js_detailinf.js

Monta string de select para combo de grid

Parametros:
dso                 - referencia ao dso corrente
pastaID             - pasta corrente
nRegistroID         - id do registro corrente
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
{
	var aEmpresaData = getCurrEmpresaData();
    setConnection(dso);
    
    // LOG
    if (pastaID == 20010)
    {
        if ( dso.id == 'dso01GridLkp' )
        {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoFantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.SubFormID = b.RecursoID)';
        }
        else if ( dso.id == 'dso02GridLkp' )
        {
            dso.SQL = 'SELECT b.PessoaID, b.Fantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Pessoas b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.UsuarioID = b.PessoaID)';
        }
        else if ( dso.id == 'dso03GridLkp' )
        {
            dso.SQL = 'SELECT b.ItemID, b.ItemAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), TiposAuxiliares_Itens b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EventoID = b.ItemID)';
        }
        else if ( dso.id == 'dso04GridLkp' )
        {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EstadoID = b.RecursoID)';
        }
    }
    else if (pastaID == 26062)
    {
		if ( dso.id == 'dsoCmb01Grid_01' )
		{
		    dso.SQL = 'SELECT DISTINCT c.PessoaID, c.Fantasia ' +
				'FROM RelacoesPesRec_Perfis a WITH(NOLOCK), RelacoesPesRec b WITH(NOLOCK), Pessoas c WITH(NOLOCK) ' +
				'WHERE (a.EmpresaID = ' + aEmpresaData[0] + ' AND a.OK = 1 AND a.RelacaoID = b.RelacaoID AND ' +
					'b.EstadoID = 2 AND b.SujeitoID = c.PessoaID AND c.EstadoID = 2) ' +
				'ORDER BY c.Fantasia';
		}
        else if ( dso.id == 'dsoCmb02Grid_01' )
        {
            dso.SQL = 'SELECT a.RecursoID, a.RecursoFantasia ' +
				'FROM Recursos a WITH(NOLOCK) ' +
				'WHERE (a.EstadoID = 2 AND a.TipoRecursoID=6) ' +
				'ORDER BY a.RecursoFantasia';
        }
    }
    else if (pastaID == 26063)
    {
        if (dso.id == 'dso01GridLkp')
        {
            dso.SQL = 'SELECT RADDocumentoID, (CASE DocumentoID WHEN 560001 THEN (SELECT b.RecursoAbreviado FROM RAD a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
				'WHERE (a.RADID = RegistroID AND a.EstadoID = b.RecursoID)) ' +
					'WHEN 620004 THEN (SELECT b.RecursoAbreviado FROM RET a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
						'WHERE (a.RETID = RegistroID AND a.EstadoID = b.RecursoID)) ' +
					'WHEN 723001 THEN (SELECT b.RecursoAbreviado FROM RRC a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
						'WHERE (a.RRCID = RegistroID AND a.EstadoID = b.RecursoID)) ' +
					'WHEN 821001 THEN (SELECT b.RecursoAbreviado FROM PSC a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
						'WHERE (a.PSCID = RegistroID AND a.EstadoID = b.RecursoID)) ' +
					'WHEN 822001 THEN (SELECT b.RecursoAbreviado FROM RAI a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
						'WHERE (a.RAIID = RegistroID AND a.EstadoID = b.RecursoID)) ' +
					'WHEN 852001 THEN (SELECT b.RecursoAbreviado FROM RACP a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
						'WHERE (a.RACPID = RegistroID AND a.EstadoID = b.RecursoID)) END) AS Est ' +
				'FROM RAD_Documentos WITH(NOLOCK) ' +
				'WHERE (RADID = ' + nRegistroID + ')';
        }
        else if (dso.id == 'dso02GridLkp')
        {
            dso.SQL = 'SELECT a.DocumentoID, a.DocumentoAbreviado ' +
				'FROM Documentos a WITH(NOLOCK), RAD_Documentos b WITH(NOLOCK) ' +
				'WHERE b.RADID = ' + nRegistroID + ' AND a.DocumentoID=b.DocumentoID';
        }
    }
    else if (pastaID == 26064)
    {
        if (dso.id == 'dso01GridLkp')
        {
            dso.SQL = 'SELECT DISTINCT b.RIQID, b.Unidade, c.ItemAbreviado AS Indicador ' +
				'FROM RAD_Indicadores a WITH(NOLOCK), RIQ b WITH(NOLOCK), TiposAuxiliares_Itens c WITH(NOLOCK) ' +
				'WHERE (a.RADID = ' + nRegistroID + ' AND a.RIQID=b.RIQID AND b.IndicadorID = c.ItemID)';
        }
        else if (dso.id == 'dso02GridLkp')
        {
            dso.SQL = 'SELECT DISTINCT a.RADIndicadorID, dbo.fn_RADIndicador_OK(a.RADIndicadorID) AS OK ' +
				'FROM RAD_Indicadores a WITH(NOLOCK) ' +
				'WHERE (a.RADID = ' + nRegistroID + ')';
        }
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada em diversos pontos do js_detailinf.js

Esta funcao preenche o combo de pastas do controlbar inferior

Parametros:
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function fillCmbPastasInCtlBarInf(nTipoRegistroID)
{
    var i=0;
    var currSelection = getCmbCurrDataInControlBar('inf', 1);
    var vPastaName;

	cleanupSelInControlBar('inf',1);

	vPastaName = window.top.getPastaName(20008);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20008); //Observa��es

    vPastaName = window.top.getPastaName(20009);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20009); //Proprietario

    vPastaName = window.top.getPastaName(20010);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20010); //LOG

    vPastaName = window.top.getPastaName(26061);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 26061); //Texto

    vPastaName = window.top.getPastaName(26062);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 26062); //Participantes

    vPastaName = window.top.getPastaName(26063);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 26063); //Documentos Analisados

    vPastaName = window.top.getPastaName(26064);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 26064); //Indicadores da Qualidade

    vPastaName = window.top.getPastaName(26065);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 26065); //Documentos Relacionados

    // Nao mexer - Inicio de automacao ============================== 	
	// Tenta selecionar novamente a pasta anterior selecionada no combo
	// Se nao mais existe a pasta anteriormente selecionada no combo
	// seleciona a pasta default
	
	stripFoldersByRight(currSelection);
        
    return true;
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada em diversos pontos do js_detailinf.js

Tratar consistencia dos dados inseridos pelo usuario aqui.
           
Parametros: 
btnClicked      - controlbar e botao clicado pelo usuario
folderID        - id da pasta selecionada
tipoCtlrsPasta  - 'GRID' a pasta e um grid/ 'CTRLS' a pasta nao e
                  um grid
currDSO         - referencia dso corrente
registroID      - id do registro corrente no sup

Retorno:
true se os dados estao ok, caso contrario false.
********************************************************************/
function criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)
{
    //@@ return true se tudo ok, caso contrario return false;
 
    var currLine = 0;

    // A pasta e grid    
    if (tipoCtlrsPasta == 'GRID') // se � grid
    {
        if (folderID == 26062) // Participantes
			currLine = criticLineGridAndFillDsoEx(fg, currDSO,keepCurrLineInGrid(),[0], ['RADID', registroID]);

        if (folderID == 26063) // Documentos Analisados
			currLine = criticLineGridAndFillDsoEx(fg, currDSO,keepCurrLineInGrid(),[], ['RADID', registroID]);

        if (folderID == 26064) // Indicadores da Qualidade
			currLine = criticLineGridAndFillDsoEx(fg, currDSO,keepCurrLineInGrid(),[], ['RADID', registroID]);

        if (currLine < 0)
            return false;
    }   
    
    return true;
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pela funcao buildGridFromFolder(folderID) do js_detailinf.js

Constroi o corpo de um grid. Os combos sao construidos pelo frame work
           
Parametros: 
folderID        - id da pasta corrente
currDSO         - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function constructGridBody(folderID, currDSO)
{
    var dTFormat = '';
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';
    
    fg.FrozenCols = 0;
    
    // LOG
    if (folderID == 20010)
    {
        headerGrid(fg,['SubForm',
                       'Data           Hora     ',
                       'Colaborador',
                       'Evento',
                       'Est',
                       'Motivo',
                       'LOGID'], [6]);
                       
        fillGridMask(fg,currDSO,['^SubFormID^dso01GridLkp^RecursoID^RecursoFantasia',
                                 'DataFuso',
                                 '^UsuarioID^dso02GridLkp^PessoaID^Fantasia',
                                 '^EventoID^dso03GridLkp^ItemID^ItemAbreviado',
                                 '^EstadoID^dso04GridLkp^RecursoID^RecursoAbreviado',
                                 'Motivo',
                                 'LOGID'],
                                 ['','99/99/9999','','','','',''],
                                 ['',dTFormat + ' hh:mm:ss','','','','','']);

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
    // Participantes
    else if (folderID == 26062)
    {
        headerGrid(fg,['Participante',
					   'Cargo',
                       'Participou',
					   'Observa��o',
                       'RADPessoaID'], [4]);

        fillGridMask(fg,currDSO,['PessoaID',
								 'PerfilID',
								 'Participou',
								 'Observacao',
								 'RADPessoaID'],
								 ['','','','',''],
                                 ['','','','','']);
		
		alignColsInGrid(fg,[]);
		fg.FrozenCols = 1;

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
    // Documentos Analisados
    else if (folderID == 26063)
    {
        headerGrid(fg,['Documento',
					   'ID',
					   'Est',
                       'OK',
                       'Coment�rios',
                       'DocumentoID',
                       'RADDocumentoID'], [5, 6]);

        fillGridMask(fg,currDSO,['^DocumentoID^dso02GridLkp^DocumentoID^DocumentoAbreviado*',
								 'RegistroID*',
								 '^RADDocumentoID^dso01GridLkp^RADDocumentoID^Est*',
								 'OK',
								 'Comentarios',
								 'DocumentoID',
								 'RADDocumentoID'],
								 ['','','','','','',''],
                                 ['','','','','','','']);
		
		alignColsInGrid(fg,[]);
		fg.FrozenCols = 1;

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
    // Indicadores da Qualidade
    else if (folderID == 26064)
    {
        headerGrid(fg,['RIQ',
					   'Indicador',
					   'Un',
					   'Inc',
                       'Data Meta',
                       'Meta',
                       'Resultado',
                       'Pior',
                       'Melhor',
                       'M�dia',
                       'DP',
                       'OK',
                       'Nova Data Meta',
                       'Nova Meta',
                       'M�nimo',
                       'M�ximo',
                       'Coment�rios',
                       'RADIndicadorID'], [0, 17]);

        fillGridMask(fg,currDSO,['RIQID*',
								 '^RIQID^dso01GridLkp^RIQID^Indicador*',
								 '^RIQID^dso01GridLkp^RIQID^Unidade*',
								 'Incidencia*',
								 'dtMeta*',
								 'Meta*',
								 'Resultado*',
								 'Pior*',
								 'Melhor*',
								 'Media*',
								 'DesvioPadrao*',
								 '^RADIndicadorID^dso02GridLkp^RADIndicadorID^OK*',
								 'dtNovaMeta',
								 'NovaMeta',
								 'NovoMinimo',
								 'NovoMaximo',
								 'Comentarios',
								 'RADIndicadorID'],
								 ['','','','99','99/99/9999','999.99','999.99','999.99','999.99','999.99','999.99','','99/99/9999','999.99','999.99',
								 '999.99','',''],
                                 ['','','','##',dTFormat,'##0.00','##0.00','##0.00','##0.00','##0.00','##0.00','',dTFormat,'##0.00','##0.00',
								 '##0.00','','']);

		alignColsInGrid(fg,[0,3,5,6,7,8,9,10,13,14,15]);
		fg.FrozenCols = 3;

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
    // Documentos Relacionados
    else if (folderID == 26065)
    {
        headerGrid(fg,['Tipo',
					   'DocumentoID',
					   'Documento',
                       'ID',
                       'Est',
                       'Data',
                       'Vencimento',
                       'Conclus�o',
                       'TipoRelacionamentoID',
                       'DocRelacaoID'], [1, 8, 9]);

        fillGridMask(fg,currDSO,['TipoRelacionamento',
								 'DocumentoID',
								 'DocumentoAbreviado',
								 'RegistroID',
								 'Estado',
								 'dtEmissao',
								 'dtVencimento',
								 'dtConclusao',
								 'TipoRelacionamentoID',
								 'DocRelacaoID'],
								 ['','','','','','99/99/9999','99/99/9999','99/99/9999','',''],
                                 ['','','','','',dTFormat,dTFormat,dTFormat,'','']);
		
		alignColsInGrid(fg,[1, 3]);
		fg.FrozenCols = 1;

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
    
}

// FINAL DE FUNCOES DA AUTOMACAO ************************************
