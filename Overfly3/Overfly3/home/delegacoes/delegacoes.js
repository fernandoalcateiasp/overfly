/********************************************************************
delegacoes.js

Library javascript para o delegacoes.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

// Ultima linha selecionada
var glb_LASTLINESELID = '';
var fg_ExecEvents = true;

var glb_dsoQty = null;

var glb_timerVarDelegDirs = null;

var glb_Local_DATE_FORMAT = "DD/MM/YYYY";
var glb_Local_DATE_SQL_PARAM = 103;


var dsoGen01 = new CDatatransport("dsoGen01");
var dsoDataToSave = new CDatatransport("dsoDataToSave");
var dsoCmbCol = new CDatatransport("dsoCmbCol");
var dsoDireitoNotificacao = new CDatatransport("dsoDireitoNotificacao");
var dsoGrid = new CDatatransport("dsoGrid");
// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Loop de mensagens
********************************************************************/
function wndJSProc(idElement, msg, param1, param2)
{
    switch (msg)
    {
       
        default:
            return null;
    }
}

/*
Ze em 17/03/08
*/
function window_OnUnload()
{
	dealWithObjects_Unload();
}

/********************************************************************
Window onload
********************************************************************/
function window_onload()
{
	// Garante inicializacao do lockControls
	lockControls(true);
	lockControls(false);
	
	// ajusta parametros fixos dos dsos da pagina
    dsoFixedParams();
	
    var elem;
    
    // variavel do js_gridEx.js
    if (window.document.getElementById('fg') != null)
        glb_HasGridButIsNotFormPage = true;

	// configuracao inicial do html
    var sPag = setupPage();   
    
    // se retorna false o usuario esta navegando pelo browser
    if ( sPag == false )
        return null;

    // FG - grid
    // coloca classe no grid
    elem = window.document.getElementById('fg');
    
    if ( elem != null )
        elem.className = 'fldGeneral';
        
    // Ze em 17/03/08
	document.body.onunload = window_OnUnload;
	dealWithObjects_Load();
	dealWithGrid_Load();
	// Fim de Ze em 17/03/08    
    
    // ajusta o body do html
	elem = document.getElementById('delegacoesBody');

    with (elem)
    {
        style.border = 'none'; 
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

	resetDateFormat();

    // a janela carregou
	sendJSMessage(getHtmlId(), JS_MSGRESERVED, 'PAGE_DELEGACOES', 'LOADED');
    
    // mostrar aqui a pagina de direitos
	sendJSMessage(getHtmlId(), JS_MSGRESERVED, 'PAGE_DELEGACOES', 'SHOW');
    
	fillCmbColaboradores();

    // Verifica direitos de acesso �s notifica��es
	verificaDireitoNotificacao();

    // Verifica direitos de acesso �s vendas
	verificaDireitoVendas();
}

/*****************************************************************************************
Funcao criada para ser usada no delegacoes.js, pois no carregamento da pagina
o overflygen ainda nao tinha sido criado, entao a constante DATE_FORMAT ficou com
o valor default que eh o formato brasileiro. Mostrar p/ o Ze
*****************************************************************************************/
function resetDateFormat()
{
	DATE_FORMAT = overflyGen.ShortDateFormat();
	
	if ( DATE_FORMAT == "DD/MM/YYYY" )
	{
		glb_Local_DATE_FORMAT = "DD/MM/YYYY";
	    glb_Local_DATE_SQL_PARAM = 103;
	}    
	else if ( DATE_FORMAT == "MM/DD/YYYY" )
	{
		glb_Local_DATE_FORMAT = "MM/DD/YYYY";
	    glb_Local_DATE_SQL_PARAM = 101;
	}    
}


/********************************************************************
Window befor unload
********************************************************************/
function window_onbeforeunload()
{
	if (glb_timerVarDelegDirs != null)
    {
        window.clearInterval(glb_timerVarDelegDirs);
        glb_timerVarDelegDirs = null;
    }
}

/********************************************************************
Configuracao inicial da pagina
********************************************************************/
function setupPage()
{
	var elem;
	var nNextLeft, nNextTop;
    
    // o frame da pagina
    var frameID = getExtFrameID(window);
    
    var rectFrame = new Array();
    if (frameID)
        rectFrame = getRectFrameInHtmlTop(frameID);
        
    // Nao mostra se o frame esta com retangulo zerado
    // significa que o usuario esta navegando pelo browser
    if ( rectFrame[0] == 0 )    
        return false;

    // ajusta o divBotoes
    elem = window.document.getElementById('divBotoes');
    with (elem.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = 0;
        top = ELEM_GAP;
        width = rectFrame[2] - ELEM_GAP;
        height = 30;
    }

    // ajusta o botao Voltar
    elem = window.document.getElementById('btnVoltar');
    with (elem.style) {
        height = 24;
        width = 59;
        left = 0;
        top = (ELEM_GAP - 7);
    }

    // ajusta o botao Colaboradores
    elem = window.document.getElementById('btnColaboradores');
    with (elem.style) {
        backgroundColor = "#C7C1B7";
        height = parseInt(btnVoltar.currentStyle.height, 10);
        width = 114;
        left = parseInt(btnVoltar.currentStyle.left, 10) + parseInt(btnVoltar.currentStyle.width, 10) + ELEM_GAP;
        top = parseInt(btnVoltar.currentStyle.top, 10);
    }

    // ajusta o botao Delegacoes
    elem = window.document.getElementById('btnDelegacoes');
    with (elem.style) {
        backgroundColor = "#B4D2E3";
        height = parseInt(btnColaboradores.currentStyle.height, 10);
        width = parseInt(btnColaboradores.currentStyle.width, 10);
        left = parseInt(btnColaboradores.currentStyle.left, 10) + parseInt(btnColaboradores.currentStyle.width, 10) - 3;
        top = parseInt(btnColaboradores.currentStyle.top, 10);
    }

    // ajusta o botao Feriados
    elem = window.document.getElementById('btnFeriados');
    with (elem.style) {
        backgroundColor = "#C7C1B7";
        height = parseInt(btnDelegacoes.currentStyle.height, 10);
        width = parseInt(btnDelegacoes.currentStyle.width, 10);
        left = parseInt(btnDelegacoes.currentStyle.left, 10) + parseInt(btnDelegacoes.currentStyle.width, 10) - 3;
        top = parseInt(btnDelegacoes.currentStyle.top, 10);
    }

    // ajusta o botao Vendas
    elem = window.document.getElementById('btnVendas');
    with (elem.style) {
        backgroundColor = "#C7C1B7";
        height = parseInt(btnFeriados.currentStyle.height, 10);
        width = parseInt(btnFeriados.currentStyle.width, 10);
        left = parseInt(btnFeriados.currentStyle.left, 10) + parseInt(btnFeriados.currentStyle.width, 10) - 3;
        top = parseInt(btnFeriados.currentStyle.top, 10);
    }

    // ajusta o botao Notifica��es
    elem = window.document.getElementById('btnNotificacoes');
    with (elem.style) {
        visibility = 'hidden';
        backgroundColor = "#C7C1B7";
        height = parseInt(btnVendas.currentStyle.height, 10);
        width = parseInt(btnVendas.currentStyle.width, 10);
        left = parseInt(btnVendas.currentStyle.left, 10) + parseInt(btnVendas.currentStyle.width, 10);
        top = parseInt(btnVendas.currentStyle.top, 10);
    }

    // ajusta o divControls
    elem = window.document.getElementById('divControls');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = 0;
        top = parseInt(divBotoes.currentStyle.top, 10) + parseInt(divBotoes.currentStyle.height, 10) + ELEM_GAP;
        width = rectFrame[2] - 2 * ELEM_GAP;
        height = 40;
    }

    // ajusta o lblAtivos
    elem = window.document.getElementById('lblAtivos');
    with (elem.style) 
    {
        top = 0;
    }

    // ajusta o botao Incluir
    elem = window.document.getElementById('btnIncluir');
    with (elem.style) 
    {
        height = 24;
        width = 80;
        left = 0;
        top = parseInt(lblAtivos.currentStyle.top, 10) + parseInt(lblAtivos.currentStyle.height, 10);
    }

    // ajusta o botao Gravar
    elem = window.document.getElementById('btnGravar');
    with (elem.style) 
    {
        height = 24;
        width = 80;
        left = parseInt(btnIncluir.currentStyle.left, 10) + parseInt(btnIncluir.currentStyle.width, 10) + ELEM_GAP;
        top = parseInt(btnIncluir.currentStyle.top, 10);
    }

    // ajusta o botao Refresh
    elem = window.document.getElementById('btnRefresh');
    with (elem.style) {
        height = 24;
        width = 80;
        left = parseInt(btnGravar.currentStyle.left, 10) + parseInt(btnGravar.currentStyle.width, 10) + ELEM_GAP;
        top = parseInt(btnGravar.currentStyle.top, 10);
    }

    elem = window.document.getElementById('lblAtivos');
    with (elem.style) 
    {
        left = parseInt(btnRefresh.currentStyle.left, 10) + parseInt(btnRefresh.currentStyle.width, 10) + ELEM_GAP;
    }

    // ajusta o checkbox chkAtivos
    elem = window.document.getElementById('chkAtivos');
    with (elem.style) {
        width = 25;
        left = parseInt(btnRefresh.currentStyle.left, 10) + parseInt(btnRefresh.currentStyle.width, 10) + ELEM_GAP;
        top = parseInt(btnRefresh.currentStyle.top, 10);
    }
    elem.checked = true;

    // ajusta o divColaboradores
    elem = window.document.getElementById('divColaboradores');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = parseInt(chkAtivos.currentStyle.left, 10) + parseInt(chkAtivos.currentStyle.width, 10) + ELEM_GAP;
        width = parseInt(divControls.currentStyle.width, 10) - (parseInt(chkAtivos.currentStyle.left, 10) + parseInt(chkAtivos.currentStyle.width, 10) + ELEM_GAP);
        visibility = 'hidden';
    }

    // ajusta lblColaboradores
    elem = window.document.getElementById('lblAtivos');
    with (elem.style) 
    {
        top = parseInt(lblAtivos.currentStyle.top, 10);
        left = parseInt(btnRefresh.currentStyle.left, 10) + parseInt(btnRefresh.currentStyle.width, 10) + ELEM_GAP;
    }


    // ajusta o combo de colaboradores
    elem = window.document.getElementById('selColaboradores');
    elem.disabled = true;
    with (elem.style)
    {
		fontSize = '10 pt';
        width = 151;
        left = 0;
        top = parseInt(lblColaboradores.currentStyle.top, 10) + parseInt(lblColaboradores.currentStyle.height, 10);
    }
    
    // ajusta o botao btnInsertColaborador
    elem = window.document.getElementById('btnInsertColaborador');
    elem.disabled = true;
    with (elem.style)
    {
        height = 24;
        width = 80;
        left = parseInt(selColaboradores.currentStyle.left, 10) + parseInt(selColaboradores.currentStyle.width, 10) + ELEM_GAP;
        top = parseInt(selColaboradores.currentStyle.top, 10);
    }
    
    // ajusta o botao btnCancelColaborador
    elem = window.document.getElementById('btnCancelColaborador');
    elem.disabled = true;
    with (elem.style)
    {
        height = 24;
        width = 80;
        left = parseInt(btnInsertColaborador.currentStyle.left, 10) + parseInt(btnInsertColaborador.currentStyle.width, 10) + ELEM_GAP;
        top = parseInt(btnInsertColaborador.currentStyle.top, 10);
    }
    
    // ajusta o divFG
    elem = window.document.getElementById('divFG');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = 0;
        top = (parseInt(divControls.currentStyle.top, 10) + parseInt(divControls.currentStyle.height, 10) + ELEM_GAP);
        width = rectFrame[2] - ELEM_GAP;
        height = 435;
    }
    
    elem = document.getElementById('fg');
    with (elem.style)
    {
        left = 0;
        top = 0;
        width = parseInt(document.getElementById('divFG').currentStyle.width);
        height = parseInt(document.getElementById('divFG').currentStyle.height);
    }
    
    startGridInterface(fg, 1, 1);
    
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;

    
}

/********************************************************************
Obtem dados no banco e carrega o combo de colaboradores
********************************************************************/
function fillCmbColaboradores()
{
	var nUserID = sendJSMessage(getHtmlId(), JS_WIDEMSG, '__CURRUSERID', null);		
	
	// parametrizacao do dso dsoCmbCol
    setConnection(dsoCmbCol);
				   
	dsoCmbCol.SQL = 'SELECT e.PessoaID, e.Fantasia ' +
					'FROM RelacoesPesRec a WITH(NOLOCK), RelacoesPesRec_Perfis b WITH(NOLOCK), RelacoesPesRec_Perfis c WITH(NOLOCK), RelacoesPesRec d WITH(NOLOCK), Pessoas e WITH(NOLOCK) ' +
					'WHERE (a.SujeitoID = ' + nUserID + ' AND a.TipoRelacaoID = 11 AND a.EstadoID = 2 AND a.ObjetoID = 999 AND a.RelacaoID = b.RelacaoID AND ' +
		         	'b.EmpresaID = c.EmpresaID AND c.RelacaoID = d.RelacaoID AND d.EstadoID = 2 AND d.ObjetoID = 999 AND d.TipoRelacaoID = 11 AND ' +
				    'd.SujeitoID = e.PessoaID AND e.EstadoID = 2) ' +
					'GROUP BY e.PessoaID, e.Fantasia ' +
					'ORDER BY e.Fantasia';

    dsoCmbCol.ondatasetcomplete = fillCmbColaboradores_DSC;
    dsoCmbCol.Refresh();
}

/********************************************************************
Preenche o combo de colaboradores
********************************************************************/
function fillCmbColaboradores_DSC()
{
	var optionStr;
	var optionValue;
	var oOption;

	var nUserID = sendJSMessage(getHtmlId(), JS_WIDEMSG, '__CURRUSERID', null);

	while ( !dsoCmbCol.recordset.EOF )
    {
		// Nao carrega o usuario logado no combo
	    if ( dsoCmbCol.recordset['PessoaID'].value == nUserID )
		{
			dsoCmbCol.recordset.MoveNext();
			continue;
		}
		
		optionStr = dsoCmbCol.recordset['Fantasia'].value;
		optionValue = dsoCmbCol.recordset['PessoaID'].value;
        oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        selColaboradores.add(oOption);
        
        dsoCmbCol.recordset.MoveNext();
    }
	
	if ( selColaboradores.options.length > 0 )
		selColaboradores.selectedIndex = 0;
	
	loadDataAndFillGrid();
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado

    // O usuario clicou o botao Voltar
    if (ctl.id == btnVoltar.id) {
        lockControls(true);

        // zera a listagem
        fg.Rows = 1;

        // volta para logo
        sendJSMessage(getHtmlId(), JS_MSGRESERVED, 'PAGE_DELEGACOES', 'HIDE');
        sendJSMessage(getHtmlId(), JS_MSGRESERVED, 'PAGE_COLABORADORES', 'CANC');

        lockControls(false);
    }

    // O usuario clicou o botao Colaboradores
    else if (ctl.id == btnColaboradores.id) {
        lockControls(true);
        sendJSMessage(getHtmlId(), JS_MSGRESERVED, 'PAGE_DELEGACOES', 'HIDE');
    }

    // O usuario clicou o botao Feriados
    else if (ctl.id == btnFeriados.id) {
        lockControls(true);
        sendJSMessage(getHtmlId(), JS_MSGRESERVED, 'PAGE_DELEGACOES', 'HIDE');
        sendJSMessage(getHtmlId(), JS_MSGRESERVED, 'PAGE_FERIADOS', 'LOAD');
    }

    // O usuario clicou o botao Notifica��es
    else if (ctl.id == btnNotificacoes.id) {
        lockControls(true);
        sendJSMessage(getHtmlId(), JS_MSGRESERVED, 'PAGE_DELEGACOES', 'HIDE');
        sendJSMessage(getHtmlId(), JS_MSGRESERVED, 'PAGE_NOTIFICACOES', 'LOAD');
    }

        // O usuario clicou o botao Vendas
    else if (ctl.id == btnVendas.id) {
        lockControls(true);
        sendJSMessage(getHtmlId(), JS_MSGRESERVED, 'PAGE_DELEGACOES', 'HIDE');
        sendJSMessage(getHtmlId(), JS_MSGRESERVED, 'PAGE_VENDAS', 'LOAD');
    }
    
    // O usuario clicou o botao Incluir
    if (ctl.id == btnIncluir.id )
    {
		lockControls(true);
		
		if ( selColaboradores.options.length > 0 )
			selColaboradores.disabled = false;
        
        btnInsertColaborador.disabled = false;
        btnCancelColaborador.disabled = false;
        
        divColaboradores.style.visibility = 'inherit';
        
        if ( !selColaboradores.disabled )
        {
			window.focus();
			selColaboradores.focus();
		}	
    }
    // O usuario clicou o botao Gravar
    else if (ctl.id == btnGravar.id )
    {
		btnIncluir.disabled = false;
		chkAtivos.disabled = false;
        
        saveDataInGrid();
    }
    // O usuario clicou o botao Refresh
    else if (ctl.id == btnRefresh.id )
    {
		btnIncluir.disabled = false;
		chkAtivos.disabled = false;
		
		btnRefresh.value = 'Refresh';
		
        loadDataAndFillGrid();
    }

    // O usuario clicou o botao btnInsertColaborador
    else if (ctl.id == btnInsertColaborador.id )
    {
		lockControls(false);
		
		selColaboradores.disabled = true;

        btnIncluir.disabled = true;
        
        chkAtivos.disabled = true;
        
        btnRefresh.value = 'Cancelar';
        
        divColaboradores.style.visibility = 'hidden';
		
		fg_ExecEvents = false;
		addNewLine(fg);
		
		fg.SelectionMode = 1;
		
		fg.Redraw = 2;
		
		glb_LASTLINESELID = fg.Rows - 1;
		
		var nUserID = sendJSMessage(getHtmlId(), JS_WIDEMSG, '__CURRUSERID', null);		
		var sColaboradorFantasia = selColaboradores.options.item(selColaboradores.selectedIndex).innerText;
		var nColaboradorID = selColaboradores.options.item(selColaboradores.selectedIndex).value;
		var thisTime = DateAndTimeNow();
		
		setCellValueByColKey(fg, 'dtData*', fg.Row, thisTime);
		setCellValueByColKey(fg, 'UsuarioPara*', fg.Row, sColaboradorFantasia);
		setCellValueByColKey(fg, 'dtInicio', fg.Row, thisTime);
		setCellValueByColKey(fg, 'dtFim', fg.Row, '');
		setCellValueByColKey(fg, 'UsuarioDeID', fg.Row, nUserID);
		setCellValueByColKey(fg, 'UsuarioParaID', fg.Row, nColaboradorID);
		setCellValueByColKey(fg, 'DelegacaoID', fg.Row, '');
		
		fg.AutoSizeMode = 0;
		fg.AutoSize(0,fg.Cols-1);
		
		fg.ColWidth(3) = fg.ColWidth(2);
				
		setCellValueByColKey(fg, 'dtData*', fg.Row, '');		
		setCellValueByColKey(fg, 'dtInicio', fg.Row, '');
				
		fg_ExecEvents = true;
		
		window.focus();
		fg.focus();        
    }
    // O usuario clicou o botao btnCancelColaborador
    else if (ctl.id == btnCancelColaborador.id )
    {
		lockControls(false);
		
		window.focus();
		fg.focus();        
				
		selColaboradores.disabled = true;
		btnInsertColaborador.disabled = true;
        btnCancelColaborador.disabled = true;
        
        divColaboradores.style.visibility = 'hidden';
    }
}

/********************************************************************
Carrega dados para preencher grid
********************************************************************/
function loadDataAndFillGrid()
{
	if (glb_timerVarDelegDirs != null)
    {
        window.clearInterval(glb_timerVarDelegDirs);
        glb_timerVarDelegDirs = null;
    }
    
	lockControls(true);
	
	// Previne nao vir Cancelar no caption do botao, quando
	// vem da interface de e-mails
	btnRefresh.value = 'Refresh';
	
	fg.Rows = 1;
	
    var nUserID = sendJSMessage(getHtmlId(), JS_WIDEMSG, '__CURRUSERID', null);

    var nAtivos;
    if (chkAtivos.checked) 
		nAtivos = '1';
	else		
		nAtivos = '0';
    
    glb_dsoQty = 0;
    
    // parametrizacao do dso dsoGrid
    setConnection(dsoGrid);
    
    dsoGrid.SQL = 'SELECT Delegacao.DelegacaoID, Delegacao.dtData, ' +
						'Delegacao.UsuarioDeID, Delegacao.UsuarioParaID, ' +
						'Pessoas.Fantasia AS UsuarioPara, ' +
                        'Delegacao.dtInicio, Delegacao.dtFim ' +
			       'FROM DelegacaoDireitos Delegacao WITH(NOLOCK), Pessoas WITH(NOLOCK) ' +
				   'WHERE (Delegacao.UsuarioDeID = ' + nUserID + ' AND ' +
						'Delegacao.UsuarioParaID = Pessoas.PessoaID AND ' +
						'((' + nAtivos + ' = 0) OR (Delegacao.dtFim > GETDATE()))) ' +
				   'ORDER BY Delegacao.dtData DESC';

    dsoGrid.ondatasetcomplete = fillGridData_DSC;
    
    // parametrizacao do dso dsoDataToSave
    setConnection(dsoDataToSave);
    
    dsoDataToSave.SQL = 'SELECT Delegacao.DelegacaoID, Delegacao.dtData, ' +
							'Delegacao.UsuarioDeID, Delegacao.UsuarioParaID, ' +
							'Delegacao.dtInicio, Delegacao.dtFim ' +
						'FROM DelegacaoDireitos Delegacao WITH(NOLOCK) ' +
						'WHERE (Delegacao.UsuarioDeID = ' + nUserID + ' AND ' +
							'((' + nAtivos + ' = 0) OR (Delegacao.dtFim > GETDATE()))) ' +
						'ORDER BY Delegacao.dtData DESC';

    dsoDataToSave.ondatasetcomplete = fillGridData_DSC;
    
    dsoGrid.Refresh();
    dsoDataToSave.Refresh();
}

/********************************************************************
Preenche grid
********************************************************************/
function fillGridData_DSC()
{
	glb_dsoQty++;
	
	if ( glb_dsoQty != 2 )
		return null;
	
	fg.Editable = false;
    fg.Redraw = 0;
    
    var i, dTFormat;
    
    if ( glb_Local_DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( glb_Local_DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';
    
    // preenche grid
    /******* ATENCAO !!! **************
    Se mexer nas colunas das funcoes abaixo, atualizar tambem as 
    criticas de colunas ou movimento para coluna das funcoes:
	js_fg_delegDirsCellChanged
	fg_BeforeSort
	fg_AfterSort
	**********************************/

    startGridInterface(fg, 1, 1);
        
    headerGrid(fg,['Data', 'Colaborador', 'Data In�cio', 'Data Fim',
                   'UsuarioDeID', 'UsuarioParaID' ,'DelegacaoID'],[4, 5, 6]);
    
    fg.FontSize = '10';
                           
    fillGridMask(fg, dsoGrid,['dtData*',
                              'UsuarioPara*', 
                              'dtInicio', 
                              'dtFim',
                              'UsuarioDeID',
                              'UsuarioParaID',
                              'DelegacaoID'],
                              ['99/99/9999 99:99:99', '', '99/99/9999 99:99:99', '99/99/9999 99:99:99', '', '', ''],
                              [dTFormat + ' hh:mm:ss', '', dTFormat + ' hh:mm:ss', dTFormat + ' hh:mm:ss', '', '', '']);
    
    alignColsInGrid(fg,[]);
    
    fg.FrozenCols = 2;
    
    fg.Editable = true;
    //fg.ExplorerBar = 5;
    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);

    fg.Redraw = 2;    
        
    // destrava interface
    lockControls(false);
    
    fg.Redraw = 2;    
    
    if (glb_LASTLINESELID != '')
    {
		for ( i=1; i< fg.Rows; i++ )
		{	
			if ( getCellValueByColKey(fg, 'DelegacaoID', i) == glb_LASTLINESELID )
			{
				fg.Row = i;
				break;
			}
		}
    }
        
    // Ultima linha selecionada
    glb_LASTLINESELID = '';
    
    // Eventos do grid
    fg_ExecEvents = true;
    
    // foca o grid se tem linhas
    if ( fg.Rows > 1 )
    {
        window.focus();
        fg.focus();
    }
    
    // desabilita o botao gravar
    btnGravar.disabled = true;    
}

/********************************************************************
Salva dados do grid no banco
********************************************************************/
function saveDataInGrid()
{
	lockControls(true);
	
	var dso = dsoDataToSave;
	var i, j, nDelegacaoID;
	var bError = false;
	var cellData;
	var cellData2;
	var tempVar;
	var numeroDias = 0;
	var numeroMaximoDias = 7;

	// Verifica validade de todos os campos data 
	// 1. Nao verifica o campo dtData
	// 2. Se um campo data esta vazio, aceita
	for ( i=1; i< fg.Rows; i++ )
	{
		cellData = getCellValueByColKey(fg, 'dtInicio', i);
		cellData = treatCellWithDateMask(cellData);
		setCellValueByColKey(fg, 'dtInicio', i, cellData);
	
		if ( chkDataEx(cellData) == false )
		{
			if ( overflyGen.Alert ('Data In�cio n�o � v�lida!') == 0 )
                    return null;
            lockControls(false);
            
            window.focus();
            fg.focus();
            fg.Row = i;
            
            return true;
		}
		
		cellData2 = getCellValueByColKey(fg, 'dtFim', i);
		cellData2 = treatCellWithDateMask(cellData2);
		setCellValueByColKey(fg, 'dtFim', i, cellData2);
		if ( chkDataEx(cellData2) == false )
		{
			if ( overflyGen.Alert ('Data Fim n�o � v�lida!') == 0 )
                    return null;
            
            lockControls(false);
            
            window.focus();
            fg.focus();
            fg.Row = i;
            
            return true;
		}
		
		if ((cellData != '') && (cellData2 != ''))
			numeroDias = daysBetween(getCellValueByColKey(fg, 'dtInicio', i), getCellValueByColKey(fg, 'dtFim', i));
		
		if (numeroDias > numeroMaximoDias)
		{
			if ( overflyGen.Alert ('Per�odo m�ximo permitido � de ' + numeroMaximoDias + ' dias!') == 0 )
                    return null;
            
            lockControls(false);
            
            window.focus();
            fg.focus();
            fg.Row = i;
            
            return true;
		}
	}

	// Loop no grid procurando no dsoGrid 
	// usando o campo DelegacaoID como chave de pesquisa.
	// Se o registro existe no dsoGrid, altera o registro.
	// se nao existe, insere o registro.
	
	if ( !(dso.recordset.BOF && dso.recordset.EOF) )
		dso.recordset.MoveFirst();
	
	for ( i=1; i< fg.Rows; i++ )
	{
		nDelegacaoID = parseInt(getCellValueByColKey(fg, 'DelegacaoID', i), 10);
		
		// O registro ja existia e esta sendo alterado	
		if ( !isNaN(nDelegacaoID) )
		{
			dso.recordset.MoveFirst();
			
			for ( j=0; j<dso.recordset.RecordCount; j++ )
			{
				if ( !isNaN(nDelegacaoID) )
				{
				    if ( nDelegacaoID == dso.recordset.Fields['DelegacaoID'].value )
					{
						tempVar = getCellValueByColKey(fg, 'dtInicio', i);
						if ( tempVar == '' )
							tempVar = null;
						if ( dso.recordset.Fields['dtInicio'].value = tempVar )		
						    dso.recordset.Fields['dtInicio'].value = tempVar;

						tempVar = getCellValueByColKey(fg, 'dtFim', i);
						if ( tempVar == '' )
							tempVar = null;
						if ( dso.recordset.Fields['dtFim'].value != tempVar )	
						    dso.recordset.Fields['dtFim'].value = tempVar;
					}
					
					if ( !dso.recordset.EOF )
						dso.recordset.MoveNext();
				}
				
			}
		}	
		// O registro e novo e esta sendo incluido
		else
		{
			dso.recordset.AddNew();
					
			tempVar = getCellValueByColKey(fg, 'dtInicio', i);
			if ( tempVar == '' )
				tempVar = null;
			dso.recordset.Fields['dtInicio'].value = tempVar;

			tempVar = getCellValueByColKey(fg, 'dtFim', i);
			if ( tempVar == '' )
				tempVar = null;
			dso.recordset.Fields['dtFim'].value = tempVar;
					
			dso.recordset.Fields['UsuarioDeID'].value = getCellValueByColKey(fg, 'UsuarioDeID', i, true);
			dso.recordset.Fields['UsuarioParaID'].value = getCellValueByColKey(fg, 'UsuarioParaID', i, true);
		}	
	}
	
	try
	{
	    dso.SubmitChanges();
	}
	catch(e)
	{
	    if (e.number == -2147217887)
				bError = true;
	}

	// Numero de erro qdo o registro foi alterado ou removido por outro usuario
	if (bError)
	{
	    if ( overflyGen.Alert ('Registros com datas inv�lidas n�o foram gravados.') == 0 )
	        return null;
	}        
	
	if ( fg.Rows > 1)
		glb_LASTLINESELID = getCellValueByColKey(fg, 'DelegacaoID', fg.Row);
	else	
		glb_LASTLINESELID = '';
	
    dso.ondatasetcomplete = saveDataInGrid_DSC;
    dso.refresh();
}

/********************************************************************
Retorno de salvar dados do grid no banco
********************************************************************/
function saveDataInGrid_DSC()
{
	lockControls(false);
	
	glb_timerVarDelegDirs = window.setInterval('loadDataAndFillGrid()', 50, 'JavaScript');
}

/********************************************************************
Trata mascara de celula de data retornando '' se os digitos da
data estao em branco
********************************************************************/
function treatCellWithDateMask(cellData)
{
    var sString;
    var aStringPrev;
    var rExp;
    
    if (cellData == null || cellData == '')
        return '';
    
    sString = cellData;
    rExp = /\W/g;
    aStringPrev = sString.split(rExp);
    
	if ( aStringPrev.length == 0 )
		return '';
		
	// So tem dia/mes/ano, remove simbolos para hora, min e segundo
	if ( aStringPrev.length == 3 )
	{
		cellData = aStringPrev[0] + '/' +
		           aStringPrev[1] + '/' +
		           aStringPrev[2];
    }
    return cellData;
}

/********************************************************************
Seta estado enable/disable de um botao
********************************************************************/
function setDelegDirsBtnState(btnID, state)
{
	if ( (btnID == null) || (btnID == '') )
		return null;
		
	if ( (state == null) || !((state == true) || (state == false)) )
		return null;
	
	var elem = window.document.getElementById(btnID);
	
	if (elem != null)	
		elem.disabled = state;
		
}

// EVENTOS DE GRID DESTA PAGINA =====================================

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_delegDirsAfterRowColChange (fg, OldRow, OldCol, NewRow, NewCol)
{
    var firstLine = 0;
    
    if ( glb_totalCols__ != false )
        firstLine = 1;
        
    if (NewRow > firstLine)
    {
        fg.Row = NewRow;
    }
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_delegDirsBeforeMouseDown (grid, Button, Shift, X, Y, Cancel)
{
    // trap (grid esta vazio)
    if (grid.Rows <= 1)    
        return true;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_delegDirsCellChanged(grid, Row, Col)
{
    if ( grid.Editable == false )
        return true;
        
    // So colunas editaveis    
    if ( !((Col == 2) || (Col == 3)) )
        return true;
        
    // habilita o botao gravar
    btnGravar.disabled = false;
    
    // desabilita o botao incluir
    btnIncluir.disabled = true;                
    
    window.focus();
    try
    {
		grid.focus();
	}
	catch(e)
	{
		;
	}	
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function fg_BeforeSort(Col)
{
	// So colunas nao editaveis
    if ( (Col == 0) || (Col == 1) )
        fg_ExecEvents = false;
    
    if (fg.Row > 0)
        glb_LASTLINESELID = getCellValueByColKey(fg, 'DelegacaoID', fg.Row);
    else
        glb_LASTLINESELID = '';
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function fg_AfterSort(Col)
{
	// So colunas nao editaveis
    if ( (Col == 0) || (Col == 1) )
    {
        fg.Col = 2;
        fg_ExecEvents = true;
    }    

    var i;
    
    if ( (glb_LASTLINESELID != '') && (fg.Rows > 1) )
    {
        for (i=1; i<fg.Rows; i++)
        {
            if (getCellValueByColKey(fg, 'DelegacaoID', i) == glb_LASTLINESELID)
            {
                fg.TopRow = i;
                fg.Row = i;        
                break;
            }    
        }
    }
}

// FINAL DE EVENTOS DE GRID DESTA PAGINA ============================

/********************************************************************
Obtem direitos da pessoa no banco
********************************************************************/
function verificaDireitoNotificacao() {
    // parametrizacao do dso dsoCmbCol
    setConnection(dsoDireitoNotificacao);

    var empresaData = getCurrEmpresaData();

    var nEmpresaID = escape(empresaData[0]).toString();
    var nPessoaID = (window.top.glb_CURRUSERID).toString();
    var nOverflyID = '999';

    var strPars = new String();

    strPars = '?nEmpresaID=' + escape(nEmpresaID);
    strPars += '&nPessoaID=' + escape(nPessoaID);
    strPars += '&nOverflyID=' + escape(nOverflyID);

    dsoDireitoNotificacao.SQL = 'SELECT dbo.fn_Direitos(' + nEmpresaID + ', ' + nOverflyID + ', NULL, NULL, NULL, NULL, NULL, 3, ' + nPessoaID + ', 1, 1) AS Direito';
    //dsoDireitoNotificacao.URL = SYS_ASPURLROOT + '/serversidegen/direitonotificacao.asp' + strPars;
    dsoDireitoNotificacao.ondatasetcomplete = verificaDireitoNotificacao_DSC;
    dsoDireitoNotificacao.Refresh();
}

function verificaDireitoVendas() {
    var elem = window.document.getElementById('btnVendas');
    var nPessoaID = (window.top.glb_CURRUSERID).toString();
    var aPessoas = new Array(1336, 1679, 1002, 44065, 1142, 1004, 1069);

    for (var i = 0; i < aPessoas.length; i++) {
        if (aPessoas[i] == nPessoaID) {
            elem.style.visibility = 'visible';

            break;
        }
        else {
            elem.style.visibility = 'hidden';
        }
    }
}

/********************************************************************
Retorno da fun��o verificaDireitoNotificacao
********************************************************************/
function verificaDireitoNotificacao_DSC() {
    var elem = window.document.getElementById('btnNotificacoes');

    if (dsoDireitoNotificacao.recordset['Direito'].value)
        elem.style.visibility = 'visible';
    else
        elem.style.visibility = 'hidden';
}
