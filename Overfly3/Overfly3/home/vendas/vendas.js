/********************************************************************
vendas.js

Library javascript para o vendas.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

var glb_Local_DATE_FORMAT = "DD/MM/YYYY";
var glb_Local_DATE_SQL_PARAM = 103;
var dsoGrid = new CDatatransport("dsoGrid");
var dsoCmbCol = new CDatatransport("dsoCmbCol");
var dsoDireitoNotificacao = new CDatatransport("dsoDireitoNotificacao");
// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Loop de mensagens
********************************************************************/
function wndJSProc(idElement, msg, param1, param2)
{
    switch (msg)
    {
        default:
            return null;
    }
}

function window_OnUnload()
{
	dealWithObjects_Unload();
}

/********************************************************************
Window onload
********************************************************************/
function window_onload()
{
	// Garante inicializacao do lockControls
	lockControls(true);
	lockControls(false);
	
	// ajusta parametros fixos dos dsos da pagina
    dsoFixedParams();
	
    var elem;
    
    // variavel do js_gridEx.js
    if (window.document.getElementById('fg') != null)
        glb_HasGridButIsNotFormPage = true;
    
	// configuracao inicial do html
    var sPag = setupPage();   
    
    // se retorna false o usuario esta navegando pelo browser
    if ( sPag == false )
        return null;
    
    // FG - grid
    // coloca classe no grid
    elem = window.document.getElementById('fg');
    
    if ( elem != null )
        elem.className = 'fldGeneral';

    // ajusta o body do html
    elem = document.getElementById('vendasBody');

    with (elem)
    {
        style.border = 'none'; 
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

	resetDateFormat();

    // a janela carregou
	sendJSMessage(getHtmlId(), JS_MSGRESERVED, 'PAGE_VENDAS', 'LOADED');
    
    // mostrar aqui a pagina de direitos
	sendJSMessage(getHtmlId(), JS_MSGRESERVED, 'PAGE_VENDAS', 'SHOW');
}

/********************************************************************
Configuracao inicial da pagina
********************************************************************/
function setupPage()
{
    var elem;
    var nNextLeft, nNextTop;

    // o frame da pagina
    var frameID = getExtFrameID(window);

    var rectFrame = new Array();
    if (frameID)
        rectFrame = getRectFrameInHtmlTop(frameID);

    // Nao mostra se o frame esta com retangulo zerado
    // significa que o usuario esta navegando pelo browser
    if (rectFrame[0] == 0)
        return false;

    // ajusta o divBotoes
    elem = window.document.getElementById('divBotoes');
    with (elem.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = 0;
        top = ELEM_GAP;
        width = rectFrame[2] - ELEM_GAP;
        height = 30;
    }

    // ajusta o botao Voltar
    elem = window.document.getElementById('btnVoltar');
    with (elem.style) {
        height = 24;
        width = 59;
        left = 0;
        top = (ELEM_GAP - 7);
    }

    // ajusta o botao Colaboradores
    elem = window.document.getElementById('btnColaboradores');
    with (elem.style)
    {
        backgroundColor = "#C7C1B7";
        height = parseInt(btnVoltar.currentStyle.height, 10);
        width = 114;
        left = parseInt(btnVoltar.currentStyle.left, 10) + parseInt(btnVoltar.currentStyle.width, 10) + ELEM_GAP;
        top = parseInt(btnVoltar.currentStyle.top, 10);
    }

    // ajusta o botao Delegacoes
    elem = window.document.getElementById('btnDelegacoes');
    with (elem.style) {
        backgroundColor = "#C7C1B7";
        height = parseInt(btnColaboradores.currentStyle.height, 10);
        width = parseInt(btnColaboradores.currentStyle.width, 10);
        left = parseInt(btnColaboradores.currentStyle.left, 10) + parseInt(btnColaboradores.currentStyle.width, 10) - 3;
        top = parseInt(btnColaboradores.currentStyle.top, 10);
    }

    // ajusta o botao Feriados
    elem = window.document.getElementById('btnFeriados');
    with (elem.style) {
        backgroundColor = "#C7C1B7";
        height = parseInt(btnDelegacoes.currentStyle.height, 10);
        width = parseInt(btnDelegacoes.currentStyle.width, 10);
        left = parseInt(btnDelegacoes.currentStyle.left, 10) + parseInt(btnDelegacoes.currentStyle.width, 10) - 3;
        top = parseInt(btnDelegacoes.currentStyle.top, 10);
    }

    // ajusta o botao Vendas
    elem = window.document.getElementById('btnVendas');
    with (elem.style) {
        backgroundColor = "#B4D2E3";
        height = parseInt(btnFeriados.currentStyle.height, 10);
        width = parseInt(btnFeriados.currentStyle.width, 10);
        left = parseInt(btnFeriados.currentStyle.left, 10) + parseInt(btnFeriados.currentStyle.width, 10) - 3;
        top = parseInt(btnFeriados.currentStyle.top, 10);
    }

    // Verifica direitos de acesso �s vendas
    verificaDireitoVendas();

    // ajusta o botao Notifica��es
    elem = window.document.getElementById('btnNotificacoes');
    with (elem.style) {
        visibility = 'hidden';
        backgroundColor = "#C7C1B7";
        height = parseInt(btnVendas.currentStyle.height, 10);
        width = parseInt(btnVendas.currentStyle.width, 10);
        left = parseInt(btnVendas.currentStyle.left, 10) + parseInt(btnVendas.currentStyle.width, 10);
        top = parseInt(btnVendas.currentStyle.top, 10);
    }

    // Verifica direitos de acesso �s notifica��es
    verificaDireitoNotificacao();

    // ajusta o divControls
    elem = window.document.getElementById('divControls');
    with (elem.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = 0;
        top = parseInt(divBotoes.currentStyle.top, 10) + parseInt(divBotoes.currentStyle.height, 10) + ELEM_GAP;
        width = rectFrame[2] - 2 * ELEM_GAP;
        height = 40;
    }
                          
/*
    // ajusta o combo Ano
    elem = window.document.getElementById('selAno');
    elem.disabled = false;
    with (elem.style) {
        fontSize = '10 pt';
        top = ELEM_GAP;
        width = 100;
        left = 393;
        nNextLeft = parseInt(elem.currentStyle.left, 10) +
                    parseInt(elem.currentStyle.width, 10) +
                    ELEM_GAP;
        nNextTop = parseInt(elem.currentStyle.top, 10);
    }
*/
    
    // ajusta o divFG
    elem = window.document.getElementById('divFG');
    with (elem.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = 0;
        top = (parseInt(divControls.currentStyle.top, 10) + parseInt(divControls.currentStyle.height, 10) - 30);
        width = rectFrame[2] - ELEM_GAP;
        height = 475;
    }

    elem = document.getElementById('fg');
    with (elem.style) {
        left = 0;
        top = 0;
        width = parseInt(document.getElementById('divFG').currentStyle.width);
        height = parseInt(document.getElementById('divFG').currentStyle.height);
    }
    
    startGridInterface(fg, 1, 1);
    
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;

    loadDataAndFillGrid();
}

/*****************************************************************************************
Funcao criada para ser usada no vendas.js, pois no carregamento da pagina
o overflygen ainda nao tinha sido criado, entao a constante DATE_FORMAT ficou com
o valor default que eh o formato brasileiro. Mostrar p/ o Ze
*****************************************************************************************/
function resetDateFormat()
{
	DATE_FORMAT = overflyGen.ShortDateFormat();
	
	if ( DATE_FORMAT == "DD/MM/YYYY" )
	{
		glb_Local_DATE_FORMAT = "DD/MM/YYYY";
	    glb_Local_DATE_SQL_PARAM = 103;
	}    
	else if ( DATE_FORMAT == "MM/DD/YYYY" )
	{
		glb_Local_DATE_FORMAT = "MM/DD/YYYY";
	    glb_Local_DATE_SQL_PARAM = 101;
	}    
}

/********************************************************************
Carrega dados para preencher grid
********************************************************************/
function loadDataAndFillGrid() {
    lockControls(true);

    var empresaData = getCurrEmpresaData();
    var nEmpresaID = empresaData[0];

    setConnection(dsoGrid);

    dsoGrid.SQL = 'SELECT dbo.fn_Localidade_Dado(dbo.fn_Pessoa_Localidade(a.EmpresaID, 1, NULL, NULL), 1) AS Pais, GETDATE() AS Data, e.Fantasia AS Equipe, COUNT(DISTINCT a.PedidoID) AS Pedidos,  ' +
                        'SUM(a.ValorTotalPedido * (CASE WHEN (a.TipoPedidoID = 601) THEN -1 ELSE 1 END)) AS Faturamento, ' +
                        'CONVERT(NUMERIC(11, 2), SUM((a.ValorTotalContrAjustada * a.TaxaMoeda) * (CASE WHEN (a.TipoPedidoID = 601) THEN -1 ELSE 1 END))) AS Contribuicao, ' +
                        'CONVERT(NUMERIC(11, 2), (dbo.fn_DivideZero(SUM(dbo.fn_Pedido_Totais(a.PedidoID, 26) * (CASE WHEN (a.TipoPedidoID = 601) THEN -1 ELSE 1 END)), ' +
                            'SUM(dbo.fn_Pedido_Totais(a.PedidoID, 23)))) * 100.00) AS MargemContribuicao, ' +
                        'CONVERT(NUMERIC(11, 2), SUM(dbo.fn_Pedido_Totais(a.PedidoID, 23) * a.TaxaMoeda)) AS FaturamentoBase ' +
                    'FROM Pedidos a WITH(NOLOCK) ' +
                        'LEFT OUTER JOIN NotasFiscais b WITH(NOLOCK) ON (b.NotaFiscalID = a.NotaFiscalID) ' +
                        'INNER JOIN Pessoas c WITH(NOLOCK) ON (c.PessoaID = a.EmpresaID) ' +
                        'INNER JOIN Operacoes d WITH(NOLOCK) ON (d.OperacaoID = a.TransacaoID) ' +
                        'INNER JOIN Pessoas e WITH(NOLOCK) ON (e.PessoaID = dbo.fn_Pessoa_GrupoVendasProprietario(a.ProprietarioID)) ' +
                    'WHERE ((dbo.fn_Pessoa_Localidade(a.EmpresaID, 1, NULL, NULL) = dbo.fn_Pessoa_Localidade(' + nEmpresaID + ', 1, NULL, NULL)) ' +
                        'AND ((d.Resultado = 1) AND (d.OperacaoID <> 155)) AND (a.EstadoID >= 25) AND (a.EstadoID <> 33) AND (a.dtPedido >= (GETDATE() - 90)) ' +
                        'AND (((a.TipoPedidoID = 601) AND (b.dtNotaFiscal = dbo.fn_Data_Zero(GETDATE()))) ' +
                            'OR ((a.TipoPedidoID = 602) AND ((SELECT COUNT(1) ' +
                                                                'FROM LOGs aa WITH(NOLOCK) ' +
                                                                'WHERE ((aa.SubFormID = 24000) AND (aa.RegistroID = a.PedidoID) ' +
                                                                        'AND (aa.EstadoID = 25) AND (aa.EventoID = 30) ' +
                                                                        'AND (dbo.fn_Data_Zero(aa.Data) = dbo.fn_Data_Zero(GETDATE())))) > 0)))) ' +
	                'GROUP BY dbo.fn_Localidade_Dado(dbo.fn_Pessoa_Localidade(a.EmpresaID, 1, NULL, NULL), 1), e.Fantasia ' +
	                'ORDER BY Equipe';

    dsoGrid.ondatasetcomplete = fillGridData_DSC;
    dsoGrid.Refresh();
}

/********************************************************************
Preenche grid
********************************************************************/
function fillGridData_DSC() {
    
    fg.Editable = false;
    fg.Redraw = 0;
    var nVermelhoNegativo = 0X0000FF;
    var dTFormat = '';

    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy hh:mm';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy hh:mm';

    var i;

    headerGrid(fg, ['Pa�s',
                    'Data',
                    'Equipe',
                    'Pedidos',
                    'Faturamento',
                    'Contribui��o',
                    'MC',
                    'FaturamentoBase'], [7]);

    // fg.FontSize = '10';

    fillGridMask(fg, dsoGrid, ['Pais',
                              'Data',
                              'Equipe',
                              'Pedidos',
                              'Faturamento',
                              'Contribuicao',
                              'MargemContribuicao',
                              'FaturamentoBase'],
                              ['', '99/99/9999 99:99:9', '', '', '999999999.99', '999999999.99', '999999999.99', '999999999.99'],
                              ['', dTFormat, '', '', '###,###,##0.00', '###,###,##0.00', '###,###,##0.00', '###,###,##0.00']);

    alignColsInGrid(fg, [3, 4, 5, 6, 7]);

    // Merge de Colunas
    fg.MergeCells = 4;
    fg.mergecol(0) = true;
    fg.mergecol(1) = true;

    gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[getColIndexByColKey(fg, 'Pedidos*'), '######', 'S'],
														  [getColIndexByColKey(fg, 'Faturamento'), '###,###,###,###.00', 'S'],
                                                          [getColIndexByColKey(fg, 'Contribuicao'), '###,###,###,###.00', 'S'],
														  [getColIndexByColKey(fg, 'MargemContribuicao'), '###,###,###,###.00', 'M'],
                                                          [getColIndexByColKey(fg, 'FaturamentoBase'), '###,###,###,###.00', 'S']]);

    if (fg.Rows > 2)
        fg.TextMatrix(1, getColIndexByColKey(fg, 'MargemContribuicao')) = ((fg.ValueMatrix(1, getColIndexByColKey(fg, 'Contribuicao')) / fg.ValueMatrix(1, getColIndexByColKey(fg, 'FaturamentoBase'))) * 100.00);
    

    for (i = 2; i < fg.Rows; i++)
    {
        if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'Faturamento')) < 0)
        {
            // Pinta campos negativos com vermelho
            fg.Select(i, getColIndexByColKey(fg, 'Faturamento'), i, getColIndexByColKey(fg, 'Faturamento'));
            fg.FillStyle = 1;
            fg.CellForeColor = nVermelhoNegativo;
        }

        if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'Contribuicao')) < 0) {
            // Pinta campos negativos com vermelho
            fg.Select(i, getColIndexByColKey(fg, 'Contribuicao'), i, getColIndexByColKey(fg, 'Contribuicao'));
            fg.FillStyle = 1;
            fg.CellForeColor = nVermelhoNegativo;
        }

        if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'MargemContribuicao')) < 0) {
            // Pinta campos negativos com vermelho
            fg.Select(i, getColIndexByColKey(fg, 'MargemContribuicao'), i, getColIndexByColKey(fg, 'MargemContribuicao'));
            fg.FillStyle = 1;
            fg.CellForeColor = nVermelhoNegativo;
        }
    }
    
    // Define tamanho das coluna do grid
    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1);
    
    // Permite ordena��o pelo header do grid
    fg.ExplorerBar = 5;
    
    fg.Redraw = 2;

    // destrava interface
    lockControls(false);

    fg.Redraw = 2;

    // foca o grid se tem linhas
    if (fg.Rows > 1)
    {
        window.focus();
        fg.focus();
        fg.col = 0;
        fg.row = 2;
    }
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado
    // O usuario clicou o botao Voltar
    if (ctl.id == btnVoltar.id) {
        lockControls(true);

        // zera a listagem
        fg.Rows = 1;

        // volta para logo
        sendJSMessage(getHtmlId(), JS_MSGRESERVED, 'PAGE_VENDAS', 'HIDE');
        sendJSMessage(getHtmlId(), JS_MSGRESERVED, 'PAGE_COLABORADORES', 'CANC');

        lockControls(false);
    }
        
    // O usuario clicou o botao Colaboradores
    else if (ctl.id == btnColaboradores.id)
    {
        lockControls(true);
        sendJSMessage(getHtmlId(), JS_MSGRESERVED, 'PAGE_VENDAS', 'HIDE');
    }

    // O usuario clicou o botao Delega��es
    else if (ctl.id == btnDelegacoes.id) 
    {
        lockControls(true);
        sendJSMessage(getHtmlId(), JS_MSGRESERVED, 'PAGE_VENDAS', 'HIDE');
        sendJSMessage(getHtmlId(), JS_MSGRESERVED, 'PAGE_DELEGACOES', 'LOAD');
    }

    // O usuario clicou o botao Notifica��es
    else if (ctl.id == btnNotificacoes.id) 
    {
        lockControls(true);
        sendJSMessage(getHtmlId(), JS_MSGRESERVED, 'PAGE_VENDAS', 'HIDE');
        sendJSMessage(getHtmlId(), JS_MSGRESERVED, 'PAGE_NOTIFICACOES', 'LOAD');
    }

    // O usuario clicou o botao Feriados
    else if (ctl.id == btnFeriados.id) {
        lockControls(true);
        sendJSMessage(getHtmlId(), JS_MSGRESERVED, 'PAGE_VENDAS', 'HIDE');
        sendJSMessage(getHtmlId(), JS_MSGRESERVED, 'PAGE_FERIADOS', 'LOAD');
    }

    // O usuario clicou o botao Vendas
    else if (ctl.id == btnVendas.id)
    {
        loadDataAndFillGrid();
    }
}

/********************************************************************
Obtem direitos da pessoa no banco
********************************************************************/
function verificaDireitoNotificacao() {
    // parametrizacao do dso dsoCmbCol
    setConnection(dsoDireitoNotificacao);

    var empresaData = getCurrEmpresaData();

    var nEmpresaID = escape(empresaData[0]).toString();
    var nPessoaID = (window.top.glb_CURRUSERID).toString();
    var nOverflyID = '999';

    var strPars = new String();

    strPars = '?nEmpresaID=' + escape(nEmpresaID);
    strPars += '&nPessoaID=' + escape(nPessoaID);
    strPars += '&nOverflyID=' + escape(nOverflyID);

    dsoDireitoNotificacao.SQL = 'SELECT dbo.fn_Direitos(' + nEmpresaID + ', ' + nOverflyID + ', NULL, NULL, NULL, NULL, NULL, 3, ' + nPessoaID + ', 1, 1) AS Direito';
    dsoDireitoNotificacao.ondatasetcomplete = verificaDireitoNotificacao_DSC;
    dsoDireitoNotificacao.Refresh();
}

function verificaDireitoVendas() {
    var elem = window.document.getElementById('btnVendas');
    var nPessoaID = (window.top.glb_CURRUSERID).toString();
    var aPessoas = new Array(1336, 1679, 1002, 44065, 1142, 1004, 1069);

    for (var i = 0; i < aPessoas.length; i++) {
        if (aPessoas[i] == nPessoaID) {
            elem.style.visibility = 'visible';

            break;
        }
        else {
            elem.style.visibility = 'hidden';
        }
    }
}

/********************************************************************
Retorno da fun��o verificaDireitoNotificacao
********************************************************************/
function verificaDireitoNotificacao_DSC() {
    var elem = window.document.getElementById('btnNotificacoes');

    if (dsoDireitoNotificacao.recordset['Direito'].value)
        elem.style.visibility = 'visible';
    else
        elem.style.visibility = 'hidden';
}