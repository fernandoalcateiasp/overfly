
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="colaboradoresHtml" name="colaboradoresHtml">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/home/colaboradores/colaboradores.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
       
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
 
    'Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_botoes.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf                
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_facilitiesnonforms.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf                
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/home/colaboradores/colaboradores.js" & Chr(34) & "></script>" & vbCrLf
%>

<%
    'Aniversariantes do dia
	Dim rsData
	Dim strSQL
	Dim i
	
    strSQL = "SELECT COUNT (*) AS QtyAniversariantes " & _
                "FROM Pessoas WITH(NOLOCK) " & _
                "WHERE (Pessoas.EstadoID = 2 AND Pessoas.TipoPessoaID=51 AND Pessoas.ClassificacaoID=57 AND " & _ 
                    "dbo.fn_Aniversario(Pessoas.dtNascimento, GETDATE()) = 1) "

    Set rsData = Server.CreateObject("ADODB.Recordset")
    rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

    Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
    Response.Write vbcrlf

    Response.Write "var glb_nQtyAniversariantes = 0;"
    Response.Write vbcrlf
    
    If ( CLng(rsData.Fields("QtyAniversariantes").Value) <> 0 ) Then
        Response.Write "glb_nQtyAniversariantes = " & CStr(rsData.Fields("QtyAniversariantes").Value) & ";"
        Response.Write vbcrlf
    End If

    Response.Write "</script>"
    Response.Write vbcrlf

    rsData.Close
    
    Set rsData = Nothing
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

<!-- //@@ Eventos de grid -->
<!-- fg -->
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=EnterCell>
<!--
    if (fg_ExecEvents == true)
    {
        js_fg_EnterCell(fg);
    }
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterRowColChange>
<!--
    if (fg_ExecEvents == true)
    {
        js_fg_AfterRowColChange(fg, arguments[0], arguments[1], arguments[2], arguments[3]);
        js_fg_eMailAfterRowColChange(fg, arguments[0], arguments[1], arguments[2], arguments[3]);
    }
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeRowColChange>
<!--
	if (fg_ExecEvents == true)
	{
		js_fg_BeforeRowColChange(fg, arguments[0], arguments[1], arguments[2], arguments[3], false, true);
    }
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=CellChanged>
<!--
    js_fg_eMailCellChanged(fg, arguments[0], arguments[1]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=DblClick>
<!--
    js_fg_eMailDblClick(fg, fg.Row, fg.Col);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeMouseDown>
<!--
    js_fg_eMailBeforeMouseDown(fg, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
//-->
</SCRIPT>

<script LANGUAGE="javascript" FOR=fg EVENT=BeforeSort>
<!--
	fg_BeforeSort(arguments[0]);
//-->
</script>

<script LANGUAGE="javascript" FOR=fg EVENT=AfterSort>
<!--
	fg_AfterSort(arguments[0]);
//-->
</script>

</head>

<body id="colaboradoresBody" name="colaboradoresBody" LANGUAGE="javascript" onload="return window_onload()">

    <!-- Objeto OverflyGen -->
    <object CLASSID="clsid:CBD8996E-E238-4A48-AB72-7A7250F700C1" ID="overflyGen" HEIGHT="0" WIDTH="0"></object>
    <div id="divBotoes" name="divBotoes" class="divGeneral">
        <input type="button" id="btnVoltar" name="btnVoltar" value="Voltar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns"></input>
        <input type="button" id="btnColaboradores" name="btnColaboradores" value="Colaboradores" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns"></input>
        <input type="button" id="btnDelegacoes" name="btnDelegacoes" value="Delegações" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns"></input>
        <input type="button" id="btnFeriados" name="btnFeriados" value="Feriados" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns" title=""></input>
        <input type="button" id="btnvendas" name="btnVendas" value="Vendas" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns" title=""></input>
        <input type="button" id="btnNotificacoes" name="btnNotificacoes" value="Notificações" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns"></input>
    </div>
    
    <div id="divControls" name="divControls" class="divGeneral">
        <p id="lblEmpresaLogada" name="lblEmpresaLogada" class="lblGeneral">Emp</p>
        <input type="checkbox" id="chkEmpresaLogada" name="chkEmpresaLogada" class="fldGeneral" title="Somente colaboradores da empresa logada?"></input>
        
        <p id="lblPessoa" name="lblPessoa" class="lblGeneral">Colaborador</p>
        <input type="text" id="txtPessoa" name="txtPessoa" class="fldGeneral"></input>
        
        <input type="button" id="btnListar" name="btnListar" value="Listar" LANGUAGE="javascript" onclick="return btnListar_onclick(this)" class="btns"></input>
        <input type="button" id="btnEmail" name="btnEmail" value="E-mail" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns"></input>
    </div>
    
    <div id="divFG" name="divFG" class="divGeneral">
        <object CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fg" name="fg" VIEWASTEXT>
        </object>
    </div>
    
</body>

</html>
