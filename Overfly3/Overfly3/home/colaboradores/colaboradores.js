/********************************************************************
colaboradores.js

Library javascript para o colaboradores.asp
********************************************************************/
var dsoGen01 = new CDatatransport("dsoGen01");
var dsoDireitoNotificacao = new CDatatransport("dsoDireitoNotificacao");
// VARIAVEIS GLOBAIS ************************************************
var __ELEMSTATE = new Array();  //array: controlId, state
var __ELEMCOUNT = 0;

// Ultima linha selecionada
var glb_LASTLINESELID = '';
var fg_ExecEvents = true;

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// imagens das lupas
var glb_LUPA_IMAGES = new Array();
glb_LUPA_IMAGES[0] = new Image();
glb_LUPA_IMAGES[1] = new Image();

// carrega as imagens de lupa
glb_LUPA_IMAGES[0].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_list.gif';
glb_LUPA_IMAGES[1].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_list_dis.gif';
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

var __aEMAILSADDRESS_To = new Array();
var __aEMAILSADDRESS_Cc = new Array();
var __aEMAILSADDRESS_Bcc = new Array();

// Variaveis de eventos de grid
var glb_PassedOneInDblClick = false;
var glb_LocalClickedIsReadOnly = false;

var glb_timerVarEmails = null;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Zera todos os elementos do array de enderecos de e-mails
********************************************************************/
function reinitEmailsAddress() {
    var i;

    for (i = 0; i < __aEMAILSADDRESS_To.length; i++) {
        __aEMAILSADDRESS_To[i] = null;
    }

    for (i = 0; i < __aEMAILSADDRESS_Cc.length; i++) {
        __aEMAILSADDRESS_Cc[i] = null;
    }

    for (i = 0; i < __aEMAILSADDRESS_Bcc.length; i++) {
        __aEMAILSADDRESS_Bcc[i] = null;
    }
}

/********************************************************************
Remove um endereco do array de enderecos de e-mails

Parametros:
address     - o endereco de e-mail
type        - o tipo: 3 = 'To', 4 = 'Cc', 5 = 'Bcc'

Retorno:
irrelevante
********************************************************************/
function removeAddressInEmailsAddress(address, type) {
    if (type == null)
        return null;

    if (type == 3)
        type = 'TO';
    else if (type == 4)
        type = 'CC';
    else if (type == 5)
        type = 'BCC';
    else
        return null;

    var i;

    if (type.toUpperCase() == 'TO') {
        for (i = 0; i < __aEMAILSADDRESS_To.length; i++) {
            if (__aEMAILSADDRESS_To[i] == address) {
                __aEMAILSADDRESS_To[i] = null;
                break;
            }
        }
    }
    else if (type.toUpperCase() == 'CC') {
        for (i = 0; i < __aEMAILSADDRESS_Cc.length; i++) {
            if (__aEMAILSADDRESS_Cc[i] == address) {
                __aEMAILSADDRESS_Cc[i] = null;
                break;
            }
        }
    }
    else if (type.toUpperCase() == 'BCC') {
        for (i = 0; i < __aEMAILSADDRESS_Bcc.length; i++) {
            if (__aEMAILSADDRESS_Bcc[i] == address) {
                __aEMAILSADDRESS_Bcc[i] = null;
                break;
            }
        }
    }
}

/********************************************************************
Adiciona um endereco ao array de enderecos de e-mails
Parametros:
address     - o endereco de e-mail
type        - o tipo: 3 = 'To', 4 = 'Cc', 5 = 'Bcc'

Retorno:
irrelevante
********************************************************************/
function addAddressInEmailsAddress(address, type) {
    if (type == null)
        return null;

    if (type == 3)
        type = 'TO';
    else if (type == 4)
        type = 'CC';
    else if (type == 5)
        type = 'BCC';
    else
        return null;

    var added = false;
    var i;
    var addExists = false;

    // Previne inserir o mesmo endereco se ja consta do array
    if (type.toUpperCase() == 'TO') {
        for (i = 0; i < __aEMAILSADDRESS_To.length; i++) {
            if (__aEMAILSADDRESS_To[i] == address) {
                addExists = true;
                break;
            }
        }
    }
    else if (type.toUpperCase() == 'CC') {
        for (i = 0; i < __aEMAILSADDRESS_Cc.length; i++) {
            if (__aEMAILSADDRESS_Cc[i] == address) {
                addExists = true;
                break;
            }
        }
    }
    else if (type.toUpperCase() == 'BCC') {
        for (i = 0; i < __aEMAILSADDRESS_Bcc.length; i++) {
            if (__aEMAILSADDRESS_Bcc[i] == address) {
                addExists = true;
                break;
            }
        }
    }

    if (addExists)
        return;

    // Insere o endereco
    if (type.toUpperCase() == 'TO') {
        for (i = 0; i < __aEMAILSADDRESS_To.length; i++) {
            if (__aEMAILSADDRESS_To[i] == null) {
                __aEMAILSADDRESS_To[i] = address;
                added = true;
                break;
            }
        }

        if (!added) {
            __aEMAILSADDRESS_To[__aEMAILSADDRESS_To.length] = address;
        }
    }
    else if (type.toUpperCase() == 'CC') {
        for (i = 0; i < __aEMAILSADDRESS_Cc.length; i++) {
            if (__aEMAILSADDRESS_Cc[i] == null) {
                __aEMAILSADDRESS_Cc[i] = address;
                added = true;
                break;
            }
        }

        if (!added) {
            __aEMAILSADDRESS_Cc[__aEMAILSADDRESS_Cc.length] = address;
        }
    }
    else if (type.toUpperCase() == 'BCC') {
        for (i = 0; i < __aEMAILSADDRESS_Bcc.length; i++) {
            if (__aEMAILSADDRESS_Bcc[i] == null) {
                __aEMAILSADDRESS_Bcc[i] = address;
                added = true;
                break;
            }
        }

        if (!added) {
            __aEMAILSADDRESS_Bcc[__aEMAILSADDRESS_Bcc.length] = address;
        }
    }
}

/********************************************************************
Estado habilitado ou desabilitado do botao OK
********************************************************************/
function btnEmail_Status() {
    return true;
    /**********************************
    btnOK.disabled = true;
    
    // verifica os arrays de To e Cc e trava ou destrava o botao OK
    var btnOKDisabled = true;
    var i;
    
    for (i=0; i<__aEMAILSADDRESS_To.length; i++)
    {
    if ( __aEMAILSADDRESS_To[i] != null )
    {
    btnOKDisabled = false;
    break;
    }
    }
    
    if ( btnOKDisabled )
    {
    for (i=0; i<__aEMAILSADDRESS_Cc.length; i++)
    {
    if ( __aEMAILSADDRESS_Cc[i] != null )
    {
    btnOKDisabled = false;
    break;
    }
    }
    }
    
    btnOK.disabled = btnOKDisabled;
    **********************************/
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_eMailAfterRowColChange(fg, OldRow, OldCol, NewRow, NewCol) {
    var firstLine = 0;

    if (glb_totalCols__ != false)
        firstLine = 1;

    if (NewRow > firstLine) {
        fg.Row = NewRow;
    }
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_eMailBeforeMouseDown(grid, Button, Shift, X, Y, Cancel) {
    // trap (grid esta vazio)
    if (grid.Rows <= 1)
        return true;

    if ((X > (grid.ColWidth(0) + grid.ColWidth(1) + grid.ColWidth(2) + grid.ColWidth(3))) &&
		(X < (grid.ColWidth(0) + grid.ColWidth(1) + grid.ColWidth(2) + grid.ColWidth(3) + grid.ColWidth(4) + grid.ColWidth(5) + grid.ColWidth(6))) &&
        (Y < grid.RowHeight(0)))
        glb_LocalClickedIsReadOnly = false;
    else
        glb_LocalClickedIsReadOnly = true;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_eMailDblClick(grid, Row, Col) {
    if (glb_LocalClickedIsReadOnly == true)
        return true;

    if (glb_PassedOneInDblClick == true) {
        glb_PassedOneInDblClick = false;
        return true;
    }

    // trap (grid esta vazio)
    if (grid.Rows <= 1)
        return true;

    if (!((Col == 4) || (Col == 5) || (Col == 6)))
        return true;

    var i;
    var bFill = true;

    grid.Editable = false;
    grid.Redraw = 0;
    lockControls(true);

    glb_PassedOneInDblClick = true;

    // limpa coluna se tem um check box checado
    for (i = 1; i < grid.Rows; i++) {
        if (grid.ValueMatrix(i, Col) != 0) {
            bFill = false;
            break;
        }
    }

    for (i = 1; i < grid.Rows; i++) {
        if (bFill) {
            grid.TextMatrix(i, Col) = 1;
            addAddressInEmailsAddress(grid.TextMatrix(i, 6), Col);
        }
        else {
            grid.TextMatrix(i, Col) = 0;
            removeAddressInEmailsAddress(grid.TextMatrix(i, 6), Col);
        }
    }

    lockControls(false);
    grid.Editable = true;
    grid.Redraw = 2;
    window.focus();
    grid.focus();
    btnEmail_Status();
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_eMailCellChanged(grid, Row, Col) {
    // Libera OK se tem um To ou um Cc
    // A pessoa pode ter To, Cc e Bcc

    if (grid.Editable == false)
        return true;

    // So colunas editaveis
    if (!((Col == 3) || (Col == 4) || (Col == 5)))
        return true;

    var i;
    //var btnOKDisabled;

    lockControls(true);

    // adiciona ou remove o elemento
    if (grid.ValueMatrix(Row, Col) != 0) {
        addAddressInEmailsAddress(grid.TextMatrix(Row, 6), Col);
    }
    else {
        removeAddressInEmailsAddress(grid.TextMatrix(Row, 6), Col);
    }

    lockControls(false);
    /**************************    
    // verifica a coluna de To e de Cc do grid e trava ou destrava o botao OK
    btnOKDisabled = true;
    for (i=1; i<grid.Rows; i++)
    {
    if ( (grid.ValueMatrix(i, 2) != 0) || (grid.ValueMatrix(i, 3) != 0) )
    {
    btnOKDisabled = false;
    break;
    }
    }
    
    btnOK.disabled = btnOKDisabled;
    **************************/
    /**************************
    // verifica os arrays de To e Cc e trava ou destrava o botao OK
    btnOKDisabled = true;
    for (i=0; i<__aEMAILSADDRESS_To.length; i++)
    {
    if ( __aEMAILSADDRESS_To[i] != null )
    {
    btnOKDisabled = false;
    break;
    }
    }
    
    if ( btnOKDisabled )
    {
    for (i=0; i<__aEMAILSADDRESS_Cc.length; i++)
    {
    if ( __aEMAILSADDRESS_Cc[i] != null )
    {
    btnOKDisabled = false;
    break;
    }
    }
    }
    
    btnOK.disabled = btnOKDisabled;
    **************************/
    window.focus();
    grid.focus();
    btnEmail_Status();
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function fg_BeforeSort(Col) {
    // So colunas nao editaveis
    if ((Col == 0) || (Col == 1) || (Col == 2) || (Col == 3) || (Col == 7))
        fg_ExecEvents = false;

    if (fg.Row > 0)
        glb_LASTLINESELID = fg.TextMatrix(fg.Row, 0);
    else
        glb_LASTLINESELID = '';
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function fg_AfterSort(Col) {
    // So colunas nao editaveis
    if ((Col == 0) || (Col == 1) || (Col == 2) || (Col == 3) || (Col == 6)) {
        fg.Col = 4;
        fg_ExecEvents = true;
    }

    var i;

    if ((glb_LASTLINESELID != '') && (fg.Rows > 1)) {
        for (i = 1; i < fg.Rows; i++) {
            if (fg.TextMatrix(i, 0) == glb_LASTLINESELID) {
                fg.TopRow = i;
                fg.Row = i;
                break;
            }
        }
    }
}

/********************************************************************
Loop de mensagens
********************************************************************/
function wndJSProc(idElement, msg, param1, param2) {
    switch (msg) {
        case JS_MSGRESERVED:
            {
                if ((param1 == 'BTN_FAST_EMAIL') && (param2 == 'SHOWED')) {
                    initialFocus();
                    return 0;
                }

                if ((param1 == 'EMAIL') && (param2 == 'ZERO')) {
                    // zera a listagem
                    fg.Rows = 1;

                    // trava botao OK
                    //btnOK.disabled = true;

                    // reinicia variavel de emails
                    reinitEmailsAddress();

                    return 0;
                }
            }

        default:
            return null;
    }
}


/*
Ze em 17/03/08
*/
function window_OnUnload() {
    dealWithObjects_Unload();
}

/********************************************************************
Configura o html
********************************************************************/
function window_onload() {
    // Garante inicializacao do lockControls
    lockControls(true);
    lockControls(false);

    var elem;

    // variavel do js_gridEx.js
    if (window.document.getElementById('fg') != null)
        glb_HasGridButIsNotFormPage = true;

    // FG - grid
    // coloca classe no grid
    elem = window.document.getElementById('fg');

    if (elem != null)
        elem.className = 'fldGeneral';

    // Ze em 17/03/08
    document.body.onunload = window_OnUnload;
    dealWithObjects_Load();
    dealWithGrid_Load();
    // Fim de Ze em 17/03/08    

    // configuracao inicial do html
    var sPag = setupPage();

    // se retorna false o usuario esta navegando pelo browser
    if (sPag == false) {
        /*********************     
        // fecha o status bar
        var frameRef = getFrameInHtmlTop('frameStatusBar');
        showFrameInHtmlTop(frameRef, false);
        moveFrameInHtmlTop(frameRef, new Array(0,0,0,0));
        //top.frames(frameRef.name).document.location.replace('');
        ********************/
        return null;
    }

    // ajusta o body do html
    elem = document.getElementById('colaboradoresBody');

    with (elem) {
        style.border = 'none';
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // a janela carregou
    sendJSMessage(getHtmlId(), JS_MSGRESERVED, 'PAGE_COLABORADORES', 'LOADED');

    // timer de 100 millisecs e inicia o carregamento da janela
    // de aniversariantes
    if (glb_nQtyAniversariantes != 0)
        glb_timerVarEmails = window.setInterval('loadAniversariantes()', 100, 'JavaScript');
    else
        glb_timerVarEmails = window.setInterval('loadTelefonia()', 100, 'JavaScript');

    overflyGen.PlayEagleSound();
}

/********************************************************************
Inicia o carregamento dos aniversariantes
********************************************************************/
function loadAniversariantes() {
    if (glb_timerVarEmails != null) {
        window.clearInterval(glb_timerVarEmails);
        glb_timerVarEmails = null;
    }

    sendJSMessage(getHtmlId(), JS_NOMFORMOPEN, 2, null);

    // timer de 100 millisecs e inicia o carregamento da janela
    // de telefonia
    glb_timerVarEmails = window.setInterval('loadTelefonia()', 100, 'JavaScript');
}

/********************************************************************
Inicia o carregamento da telefonia
********************************************************************/
function loadTelefonia() {
    if (!TEL_ACTIVETED)
        return null;

    if (glb_timerVarEmails != null) {
        window.clearInterval(glb_timerVarEmails);
        glb_timerVarEmails = null;
    }

    var bObjInst = false;
    var bObjSafe = false;

    try {
        // VERIFICA INSTALACAO DA TELEFONIA
        // se OK executa sendJSMessage para carregar a janela de telefonia
        // (neste sendJSMessage serao verificados os direitos de telefonia)

        bObjInst = overflyGen.ObjectIsInstalled(VOICE_OCX_CLSID);

        if (bObjInst)
            bObjSafe = overflyGen.CheckObjectSafeForBrowser(VOICE_OCX_CLSID);

        if ((bObjInst == true) && (bObjSafe == true)) {
            sendJSMessage(getHtmlId(), JS_NOMFORMOPEN, 1, null);
            btnTel.disabled = false;
            btnTel.style.visibility = 'inherit';
        }
    }
    catch (e) {
        ;
    }
}

/********************************************************************
Clique botoes Telefonia, OK, Cancela, Delegar Direitos
********************************************************************/
function btn_onclick(ctl) {
    // ctl.id retorna o id do botao clicado

    // O usuario clicou o botao Voltar
    if (ctl.id == btnVoltar.id) 
    {
        lockControls(true);

        // zera a listagem
        fg.Rows = 1;

        // volta para logo
        sendJSMessage(getHtmlId(), JS_MSGRESERVED, 'PAGE_COLABORADORES', 'CANC');

        // reinicia variavel de enderecos
        reinitEmailsAddress();

        chkEmpresaLogada.checked = true;

        lockControls(false);
    }

    // O usuario clicou o botao Delega��es
    else if (ctl.id == btnDelegacoes.id) 
    {
        lockControls(true);
        loadAndOrShowDireitos();
    }

    // O usuario clicou o botao Feriados
    else if (ctl.id == btnFeriados.id) 
    {
        lockControls(true);
        loadAndOrShowFeriados();
    }

    // O usuario clicou o botao Notifica��es
    else if (ctl.id == btnNotificacoes.id) 
    {
        lockControls(true);
        loadAndOrNotificacao();
    }

    // O usuario clicou o botao Vendas
    else if (ctl.id == btnVendas.id)
    {
        lockControls(true);
        sendJSMessage(getHtmlId(), JS_MSGRESERVED, 'PAGE_VENDAS', 'LOAD');
    }

    // O usuario clicou o botao Email
    else if (ctl.id == btnEmail.id) {
        lockControls(true);

        //abre novo e-mail
        useOutLook(__aEMAILSADDRESS_To, __aEMAILSADDRESS_Cc, __aEMAILSADDRESS_Bcc, null, null);

        // zera a listagem
        fg.Rows = 1;

        // volta para logo
        sendJSMessage(getHtmlId(), JS_MSGRESERVED, 'PAGE_COLABORADORES', 'CANC');

        // reinicia variavel de enderecos
        reinitEmailsAddress();

        chkEmpresaLogada.checked = true;

        lockControls(false);

        // trava botao OK
        //btnOK.disabled = true;
    }
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() {
    var elem;

    // o frame da pagina
    var frameID = getExtFrameID(window);

    var rectFrame = new Array();
    if (frameID)
        rectFrame = getRectFrameInHtmlTop(frameID);

    // Nao mostra se o frame esta com retangulo zerado
    // significa que o usuario esta navegando pelo browser
    if (rectFrame[0] == 0)
        return false;

    // ajusta o divBotoes
    elem = window.document.getElementById('divBotoes');
    with (elem.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = 0;
        top = ELEM_GAP;
        width = rectFrame[2] - ELEM_GAP;
        height = 30;
    }

    // ajusta o botao Voltar
    elem = window.document.getElementById('btnVoltar');
    with (elem.style) 
    {
        height = 24;
        width = 59;
        left = 0;
        top = (ELEM_GAP - 7);
    }

    // ajusta o botao Colaboradores
    elem = window.document.getElementById('btnColaboradores');
    with (elem.style) 
    {
        backgroundColor = "#B4D2E3";
        height = parseInt(btnVoltar.currentStyle.height, 10);
        width = 114;
        left = parseInt(btnVoltar.currentStyle.left, 10) + parseInt(btnVoltar.currentStyle.width, 10) + ELEM_GAP;
        top = parseInt(btnVoltar.currentStyle.top, 10);
    }

    // ajusta o botao Delegacoes
    elem = window.document.getElementById('btnDelegacoes');
    with (elem.style) 
    {
        backgroundColor = "#C7C1B7";
        height = parseInt(btnColaboradores.currentStyle.height, 10);
        width = parseInt(btnColaboradores.currentStyle.width, 10);
        left = parseInt(btnColaboradores.currentStyle.left, 10) + parseInt(btnColaboradores.currentStyle.width, 10) - 3;
        top = parseInt(btnColaboradores.currentStyle.top, 10);
    }

    // ajusta o botao Feriados
    elem = window.document.getElementById('btnFeriados');
    with (elem.style) 
    {
        backgroundColor = "#C7C1B7";
        height = parseInt(btnDelegacoes.currentStyle.height, 10);
        width = parseInt(btnDelegacoes.currentStyle.width, 10);
        left = parseInt(btnDelegacoes.currentStyle.left, 10) + parseInt(btnDelegacoes.currentStyle.width, 10) - 3;
        top = parseInt(btnDelegacoes.currentStyle.top, 10);
    }

    // ajusta o botao Vendas
    elem = window.document.getElementById('btnVendas');
    with (elem.style) {
        backgroundColor = "#C7C1B7";
        height = parseInt(btnFeriados.currentStyle.height, 10);
        width = parseInt(btnFeriados.currentStyle.width, 10);
        left = parseInt(btnFeriados.currentStyle.left, 10) + parseInt(btnFeriados.currentStyle.width, 10) - 3;
        top = parseInt(btnFeriados.currentStyle.top, 10);
    }

    // Verifica direitos de acesso �s vendas
    verificaDireitoVendas();

    // ajusta o botao Notifica��es
    elem = window.document.getElementById('btnNotificacoes');
    with (elem.style) 
    {
        visibility = 'hidden';
        backgroundColor = "#C7C1B7";
        height = parseInt(btnVendas.currentStyle.height, 10);
        width = parseInt(btnVendas.currentStyle.width, 10);
        left = parseInt(btnVendas.currentStyle.left, 10) + parseInt(btnVendas.currentStyle.width, 10);
        top = parseInt(btnVendas.currentStyle.top, 10);
    }

    // Verifica direitos de acesso �s notifica��es
    verificaDireitoNotificacao();

    // ajusta o divControls
    elem = window.document.getElementById('divControls');
    with (elem.style) 
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = 0;
        top = parseInt(divBotoes.currentStyle.top, 10) + parseInt(divBotoes.currentStyle.height, 10) + ELEM_GAP;
        width = rectFrame[2] - 2 * ELEM_GAP;
        height = 40;
    }

    // elementos contidos em divControls
    adjustElementsInForm([['lblEmpresaLogada', 'chkEmpresaLogada', 3, 1, -10],
                          ['lblPessoa', 'txtPessoa', 58, 1, -4],
                          ['btnListar', 'btnListar', 60, 1, 10],
                          ['btnEmail', 'btnEmail', 60, 1, 10]], null, null, true);

    // ajusta o divFG
    elem = window.document.getElementById('divFG');
    with (elem.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = 0;
        top = (parseInt(divControls.currentStyle.top, 10) + parseInt(divControls.currentStyle.height, 10) + ELEM_GAP);
        width = rectFrame[2] - ELEM_GAP;
        height = 435;
    }

    elem = document.getElementById('fg');
    with (elem.style) {
        left = 0;
        top = 0;
        width = parseInt(document.getElementById('divFG').currentStyle.width);
        height = parseInt(document.getElementById('divFG').currentStyle.height);
    }

    startGridInterface(fg, 1, 1);

    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;

/*

    // ajusta o divList e btnListar
    elem = window.document.getElementById('divList');
    with (elem.style) {
        visibility = 'inherit';
        border = 'none';
        //backgroundColor = 'silver';
        backgroundColor = 'transparent';
        width = 24;
        height = 23;
        left = parseInt(txtPessoa.currentStyle.left, 10) +
               parseInt(txtPessoa.currentStyle.width, 10) + 7;
        top = parseInt(txtPessoa.currentStyle.top, 10);
    }

    elem = window.document.getElementById('btnListar');
    with (elem.style) {
        visibility = 'inherit';
        left = 0;
        top = 0;
        height = 24;
        width = 80;
    }
*/

    loadDataAndTreatInterface();
}

/********************************************************************
Coloca foco no campo apropriado quando mostra a interface
********************************************************************/
function initialFocus() {
    window.focus();

    if (txtPessoa.disabled == false)
        txtPessoa.focus();
    else if (btnListar.disabled == false)
        btnListar.focus();
}

/********************************************************************
Carrega dados nos elementos da interface e trava/destrava controles
********************************************************************/
function loadDataAndTreatInterface() {
    txtPessoa.onfocus = selFieldContent;
    txtPessoa.onkeypress = txtPessoa_keyPress;

    lblEmpresaLogada.onclick = invertChkBox;
    chkEmpresaLogada.checked = true;
}

/********************************************************************
Configuracao do keyPress do txtPessoa
********************************************************************/
function txtPessoa_keyPress() {
    if (event.keyCode != 13)
        return true;

    btnListar_onclick(btnListar);

}


/********************************************************************
Usuario trocou combo de tipo
********************************************************************/
/*
function selTipo_onchange()
{
with (fg)
{
Redraw = 0;
Rows = 1;
Redraw = 2;
}    
}
*/

/********************************************************************
Inverte o check de um checkbox associado a um label
Funcao copiada da lib js_interfacex.js, nao linkada neste form
********************************************************************/
function invertChkBox() {
    var ctl = this.nextSibling;

    if (ctl.disabled == true)
        return;

    ctl.checked = !ctl.checked;

    return true;
}

/********************************************************************
Usuario clicou o botao listar
********************************************************************/
function btnListar_onclick(ctl) {
    // trima o campo pessoa
    txtPessoa.value = trimStr(txtPessoa.value);

    // trava interface
    lockControls(true);

    var empresaData = getCurrEmpresaData();

    // obtem dados basicos do form do lado do servidor
    var strPars = new String();
    strPars = '?';
    strPars += 'nUserID=';
    strPars += escape((window.top.glb_CURRUSERID).toString());
    strPars += '&nEmpresaID=';
    strPars += escape(empresaData[0]);
    strPars += '&sNomePessoa=';
    strPars += escape(txtPessoa.value);

    strPars += '&nSoEmpLog=';
    if (chkEmpresaLogada.checked)
        strPars += escape(1);
    else
        strPars += escape(0);

    dsoGen01.URL = SYS_ASPURLROOT + '/serversidegenEx/emailsdata.aspx' + strPars;
    dsoGen01.ondatasetcomplete = dsoGen01_emails_DSC;

    // ajusta parametros fixos dos dsos da pagina
    dsoFixedParams();

    dsoGen01.refresh();
}

/********************************************************************
Retorno de Usuario clicou o botao listar
********************************************************************/
function dsoGen01_emails_DSC() {
    fg.Editable = false;
    fg.Redraw = 0;

    fg.Rows = 1;

    // preenche grid
    headerGrid(fg, ['Nome', 'Telefone', 'Celular', 'To', 'Cc', 'Bcc', 'Email'], []);

    fillGridMask(fg, dsoGen01, ['Fantasia*', 'Telefone*', 'Celular*', 'To', 'Cc', 'Bcc', 'Email*'], ['', '', '', '', '', '']);

    alignColsInGrid(fg, []);

    fg.FrozenCols = 0;
    fg.FrozenCols = 1;

    // checa e-mails ja existentes na lista
    checkEmailsAlreadyInArray();

    // coluna checkBox
    fg.ColDataType(3) = 11;
    fg.ColDataType(4) = 11;
    fg.ColDataType(5) = 11;
    fg.MergeCells = 4;
    fg.MergeCol(0) = true;
    fg.MergeCol(1) = true;
    fg.MergeCol(2) = true;
    fg.MergeCol(6) = true;
    fg.Editable = true;
    fg.ExplorerBar = 5;
    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1);
    fg.Redraw = 2;

    // Ultima linha selecionada
    glb_LASTLINESELID = '';

    // Eventos do grid
    fg_ExecEvents = true;

    // destrava interface
    lockControls(false);


    // foca o grid se tem linhas
    if (fg.Rows > 1) {
        window.focus();
        fg.focus();
    }
}

/********************************************************************
Checa checkbox do grid se emails que ja nas lista de To Cc e Bcc
********************************************************************/
function checkEmailsAlreadyInArray() {
    var i, j;

    if (fg.Rows <= 1)
        return null;

    for (j = 1; j < fg.Rows; j++) {
        for (i = 0; i < __aEMAILSADDRESS_To.length; i++) {
            if (__aEMAILSADDRESS_To[i] == fg.TextMatrix(j, 6))
                fg.TextMatrix(j, 3) = true;
        }

        for (i = 0; i < __aEMAILSADDRESS_Cc.length; i++) {
            if (__aEMAILSADDRESS_Cc[i] == fg.TextMatrix(j, 6))
                fg.TextMatrix(j, 4) = true;
        }
        for (i = 0; i < __aEMAILSADDRESS_Bcc.length; i++) {
            if (__aEMAILSADDRESS_Bcc[i] == fg.TextMatrix(j, 6))
                fg.TextMatrix(j, 5) = true;
        }
    }
}

/********************************************************************
Refresca e exibe o grid de delegar direitos se ja esta carregado,
caso contrario, carrega a pagina, refresca o grid e exibe
********************************************************************/
function loadAndOrShowDireitos() {
    sendJSMessage(getHtmlId(), JS_MSGRESERVED, 'PAGE_DELEGACOES', 'LOAD');
}

/********************************************************************
Refresca e exibe o grid de FERIADOS se ja esta carregado,
caso contrario, carrega a pagina, refresca o grid e exibe
********************************************************************/
function loadAndOrShowFeriados() {
    sendJSMessage(getHtmlId(), JS_MSGRESERVED, 'PAGE_FERIADOS', 'LOAD');
}

/********************************************************************
Refresca e exibe modal de notifica��es se ja esta carregado,
caso contrario, carrega a pagina, refresca o grid e exibe
********************************************************************/
function loadAndOrNotificacao() {
    sendJSMessage(getHtmlId(), JS_MSGRESERVED, 'PAGE_NOTIFICACOES', 'LOAD');
}

/********************************************************************
Obtem direitos da pessoa no banco
********************************************************************/
function verificaDireitoNotificacao() {
    // parametrizacao do dso dsoCmbCol
    setConnection(dsoDireitoNotificacao);

    var empresaData = getCurrEmpresaData();

    var nEmpresaID = escape(empresaData[0]).toString();
    var nPessoaID = (window.top.glb_CURRUSERID).toString();
    var nOverflyID = '999';
    
    var strPars = new String();

    strPars = '?nEmpresaID=' + escape(nEmpresaID);
    strPars += '&nPessoaID=' + escape(nPessoaID);
    strPars += '&nOverflyID=' + escape(nOverflyID);

    dsoDireitoNotificacao.SQL = 'SELECT dbo.fn_Direitos(' + nEmpresaID + ', ' + nOverflyID + ', NULL, NULL, NULL, NULL, NULL, 3, ' + nPessoaID + ', 1, 1) AS Direito';
    dsoDireitoNotificacao.ondatasetcomplete = verificaDireitoNotificacao_DSC;
    dsoDireitoNotificacao.Refresh();
}

function verificaDireitoVendas() {
    var elem = window.document.getElementById('btnVendas');
    var nPessoaID = (window.top.glb_CURRUSERID).toString();
    var aPessoas = new Array(1336, 1679, 1002, 44065, 1142, 1004, 1069);

    for (var i = 0; i < aPessoas.length; i++) {
        if (aPessoas[i] == nPessoaID) {
            elem.style.visibility = 'visible';

            break;
        }
        else {
            elem.style.visibility = 'hidden';
        }
    }
}

/********************************************************************
Retorno da fun��o verificaDireitoNotificacao
********************************************************************/
function verificaDireitoNotificacao_DSC() {
    var elem = window.document.getElementById('btnNotificacoes');

    if (dsoDireitoNotificacao.recordset['Direito'].value)
        elem.style.visibility = 'visible';
    else
        elem.style.visibility = 'hidden';
}
