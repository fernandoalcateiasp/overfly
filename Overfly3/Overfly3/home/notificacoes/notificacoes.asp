
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="notificacoesHtml" name="notificacoesHtml">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/home/notificacoes/notificacoes.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
        
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf                
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_facilitiesnonforms.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf                
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/home/notificacoes/notificacoes.js" & Chr(34) & "></script>" & vbCrLf
%>

<%
    <!--Resgata UsuarioID e EmpresaID do usu�rio logado-->
    Dim nEmpresaID, nUsuarioID
    Dim i

    i = 0
    nEmpresaID = 0
    nUsuarioID = 0

    For i = 1 To Request.QueryString("nEmpresaID").Count
        nEmpresaID = Request.QueryString("nEmpresaID")(i)
    Next

    For i = 1 To Request.QueryString("nUsuarioID").Count
        nUsuarioID = Request.QueryString("nUsuarioID")(i)
    Next
%>

<%
'Script de variaveis globais

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

Response.Write "var glb_nEmpresaID = " & CStr(nEmpresaID) & ";"
Response.Write vbcrlf

Response.Write "var glb_nUsuarioID = " & CStr(nUsuarioID) & ";"
Response.Write vbcrlf

Response.Write "</script>"
Response.Write vbcrlf
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

<!-- //@@ Eventos de grid -->


</head>

<body id="notificacoesBody" name="notificacoesBody" LANGUAGE="javascript" onload="return window_onload()">

    <!-- Objeto OverflyGen -->
    <object CLASSID="clsid:CBD8996E-E238-4A48-AB72-7A7250F700C1" ID="overflyGen" HEIGHT="0" WIDTH="0" VIEWASTEXT></object>

    <div id="divBotoes" name="divBotoes" class="divGeneral">
        <input type="button" id="btnVoltar" name="btnVoltar" value="Voltar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns"></input>
        <input type="button" id="btnColaboradores" name="btnColaboradores" value="Colaboradores" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns"></input>
        <input type="button" id="btnDelegacoes" name="btnDelegacoes" value="Delega��es" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns"></input>
        <input type="button" id="btnFeriados" name="btnFeriados" value="Feriados" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns" title=""></input>
        <input type="button" id="btnvendas" name="btnVendas" value="Vendas" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns" title=""></input>
        <input type="button" id="btnNotificacoes" name="btnNotificacoes" value="Notifica��es" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns"></input>
    </div>

	<div id="divControls" name="divControls" class="divGeneral">
	    <p  id="lblNotificacoesAnteriores" name="lblNotificacoesAnteriores" class="lblGeneral">Notifica��es Anteriores</p>
        <input type="text" id="txtNotificacoesAnteriores" name="txtNotificacoesAnteriores" class="fldGeneral"></input>
        <select id="selNotificacoesAnteriores" name="selNotificacoesAnteriores" class="fldGeneral"></select>

	    <p  id="lblInicio" name="lblInicio" class="lblGeneral">In�cio</p>
        <input type="text" id="txtInicio" name="txtInicio" class="fldGeneral"></input>

	    <p  id="lblDuracao" name="lblDuracao" class="lblGeneral">Dura��o</p>
	    <select id="selDuracao" name="selDuracao" class="fldGeneral"></select>
	    
	    <p  id="lblEmpresa" name="lblEmpresa" class="lblGeneral">Empresa</p>
	    <select id="selEmpresa" name="selEmpresa" class="fldGeneral"></select>

        <p  id="lblOverfly" name="lblOverfly" class="lblGeneral">Overfly</p>
        <input type="checkbox" id="chkOverfly" name="chkOverfly" class="fldGeneral" title="Notifica��o como Overfly ou Comunicados ?"></input>

        <p  id="lblCritica" name="lblCritica" class="lblGeneral">Cr�tica</p>
        <input type="checkbox" id="chkCritica" name="chkCritica" class="fldGeneral" title="A notifica��o � cr�tica ?"></input>
	    
	    <p  id="lblTituloNotificacao" name="lblTituloNotificacao" class="lblGeneral">T�tulo da Notifica��o</p>
        <input type="text" id="txtTituloNotificacao" name="txtTituloNotificacao" class="fldGeneral"></input>

	    <p  id="lblNotificacao" name="lblNotificacao" class="lblGeneral">Notifica��o</p>
        <textarea id="txtNotificacao" name="txtNotificacao" class="fldGeneral" onkeypress="if (this.value.length > 200) { return false; }" ></textarea>

	    <p  id="lblEmail" name="lblEmail" class="lblGeneral">E-mail</p>
        <input type="text" id="txtEmail" name="txtEmail" class="fldGeneral"></input>

	    <p  id="lblAssunto" name="lblAssunto" class="lblGeneral">Assunto</p>
        <input type="text" id="txtAssunto" name="txtAssunto" class="fldGeneral"></input>
        
        <input type="button" id="btnIncluir" name="btnIncluir" value="Incluir" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns"></input>
    </div>
</body>

</html>