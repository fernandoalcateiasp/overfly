<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<%
On Error Resume Next
Response.ContentType = "text/xml"

Dim i, rsNew, rsSPCommand
Dim sDataInicio, sDataFim, nEmpresaID, bOverfly, nFormID, nSubFormID, nRegistroID, sTituloNotificacao
Dim sNotificacao, sEmail, sAssunto, bCritica, nRelevancia, nUsuarioID, sPessoas, nNotificacaoID, sErro

sDataInicio = ""
sDataFim = ""
nEmpresaID = 0
bOverfly = false
nFormID = 0
nSubFormID = 0
nRegistroID = 0
sTituloNotificacao = ""
sNotificacao = ""
sEmail = ""
sAssunto = ""
bCritica = false
nRelevancia = 1
nUsuarioID = 0
sPessoas = ""

For i = 1 To Request.QueryString("sDataInicio").Count    
    sDataInicio = Request.QueryString("sDataInicio")(i)
Next

For i = 1 To Request.QueryString("sDataFim").Count    
    sDataFim = Request.QueryString("sDataFim")(i)
Next

For i = 1 To Request.QueryString("nEmpresaID").Count    
    nEmpresaID = Request.QueryString("nEmpresaID")(i)
Next

For i = 1 To Request.QueryString("bOverfly").Count    
    bOverfly = Request.QueryString("bOverfly")(i)
Next

For i = 1 To Request.QueryString("nFormID").Count    
    nFormID = Request.QueryString("nFormID")(i)
Next

For i = 1 To Request.QueryString("nSubFormID").Count    
    nSubFormID = Request.QueryString("nSubFormID")(i)
Next

For i = 1 To Request.QueryString("nRegistroID").Count    
    nRegistroID = Request.QueryString("nRegistroID")(i)
Next

For i = 1 To Request.QueryString("sTituloNotificacao").Count    
    sTituloNotificacao = Request.QueryString("sTituloNotificacao")(i)
Next

For i = 1 To Request.QueryString("sNotificacao").Count    
    sNotificacao = Request.QueryString("sNotificacao")(i)
Next

For i = 1 To Request.QueryString("sEmail").Count    
    sEmail = Request.QueryString("sEmail")(i)
Next

For i = 1 To Request.QueryString("sAssunto").Count    
    sAssunto = Request.QueryString("sAssunto")(i)
Next

For i = 1 To Request.QueryString("bCritica").Count    
    bCritica = Request.QueryString("bCritica")(i)
Next

For i = 1 To Request.QueryString("nRelevancia").Count    
    nRelevancia = Request.QueryString("nRelevancia")(i)
Next

For i = 1 To Request.QueryString("nUsuarioID").Count    
    nUsuarioID = Request.QueryString("nUsuarioID")(i)
Next

For i = 1 To Request.QueryString("sPessoas").Count    
    sPessoas = Request.QueryString("sPessoas")(i)
Next

Set rsSPCommand = Server.CreateObject("ADODB.Command")
  
With rsSPCommand
	.CommandTimeout = 60 * 10
    .ActiveConnection = strConn
    .CommandText = "sp_Notificacao_Gerador"
    .CommandType = adCmdStoredProc

    .Parameters.Append( .CreateParameter("@dtInicio", adDate, adParamInput) )
    
    If ( sDataInicio = "null" ) Then
	    .Parameters("@dtInicio").Value = Null
	Else
	    .Parameters("@dtInicio").Value = CDate(sDataInicio)
    End If

    .Parameters.Append( .CreateParameter("@dtFim", adDate, adParamInput) )
 
    If ( sDataFim = "null" ) Then
	    .Parameters("@dtFim").Value = Null
	Else
	    .Parameters("@dtFim").Value = CDate(sDataFim)
    End If

    .Parameters.Append( .CreateParameter("@EmpresaID", adInteger, adParamInput) )
    
    If ( nEmpresaID = "null" ) Then
        .Parameters("@EmpresaID").Value = Null
	Else
	    .Parameters("@EmpresaID").Value = CLng(nEmpresaID)
    End If

    .Parameters.Append( .CreateParameter("@FormID", adInteger, adParamInput) )

    If ( nFormID = "null" ) Then
	    .Parameters("@FormID").Value = Null
	Else
	    .Parameters("@FormID").Value = CLng(nFormID)
    End If

    .Parameters.Append( .CreateParameter("@SubFormID", adInteger, adParamInput) )

    If ( nSubFormID = "null" ) Then
	    .Parameters("@SubFormID").Value = Null
	Else
	    .Parameters("@SubFormID").Value = CLng(nSubFormID)
    End If
    
    .Parameters.Append( .CreateParameter("@RegistroID", adInteger, adParamInput) )

    If ( nRegistroID = "null" ) Then
	    .Parameters("@RegistroID").Value = Null
	Else
	    .Parameters("@RegistroID").Value = CLng(nRegistroID)
    End If

    .Parameters.Append( .CreateParameter("@WEB", adBoolean, adParamInput) )
    .Parameters("@WEB").Value = CBool(0)

    .Parameters.Append( .CreateParameter("@Pagina", adVarchar, adParamInput, 100) )
    .Parameters("@Pagina").Value = Null

    .Parameters.Append( .CreateParameter("@RegistroDescricao", adVarchar, adParamInput, 60) )
    .Parameters("@RegistroDescricao").Value = sTituloNotificacao
    
    .Parameters.Append( .CreateParameter("@Notificacao", adVarchar, adParamInput, 200) )
    .Parameters("@Notificacao").Value = sNotificacao
    
    .Parameters.Append( .CreateParameter("@Email", adVarchar, adParamInput, 80) )

    If ( sEmail = "null" ) Then
	    .Parameters("@Email").Value = Null
	Else
	    .Parameters("@Email").Value = sEmail
    End If

    .Parameters.Append( .CreateParameter("@Assunto", adVarchar, adParamInput, 100) )

    If ( sAssunto = "null" ) Then
	    .Parameters("@Assunto").Value = Null
	Else
	    .Parameters("@Assunto").Value = sAssunto
    End If

    .Parameters.Append( .CreateParameter("@Critica", adBoolean, adParamInput) )
    .Parameters("@Critica").Value = CBool(bCritica)

    .Parameters.Append( .CreateParameter("@Relevancia", adBoolean, adParamInput) )
    .Parameters("@Relevancia").Value = CBool(nRelevancia)

    .Parameters.Append( .CreateParameter("@UsuarioID", adInteger, adParamInput) )
    
    If ( nUsuarioID = "null" ) Then
	    .Parameters("@UsuarioID").Value = Null
	Else
	    .Parameters("@UsuarioID").Value = CLng(nUsuarioID)
    End If
    
    .Parameters.Append( .CreateParameter("@Pessoas", adVarchar, adParamInput, 8000) )
    .Parameters("@Pessoas").Value = sPessoas

    .Parameters.Append( .CreateParameter("@NotificacaoID", adInteger, adParamOutput) )
    .Parameters.Append( .CreateParameter("@Erro", adVarchar, adParamOutput, 8000) )
    
    .Execute
    
End With

If (Not IsNull(rsSPCommand.Parameters("@NotificacaoID").Value) ) Then
	nNotificacaoID = rsSPCommand.Parameters("@NotificacaoID").Value
End If

If (Not IsNull(rsSPCommand.Parameters("@Erro").Value) ) Then
	sErro = rsSPCommand.Parameters("@Erro").Value
End If

'Devolucao da resposta
Set rsNew = Server.CreateObject("ADODB.Recordset")

rsNew.CursorLocation = adUseServer
rsNew.Fields.Append "nNotificacaoID", adInteger, adFldMayBeNull OR adFldUpdatable
rsNew.Fields.Append "sErro", adVarchar, 8000, adFldMayBeNull OR adFldUpdatable
rsNew.Open , , adOpenKeyset, adLockOptimistic, adCmdText

If (Not IsNull(sErro)) Then

    rsNew.AddNew
	rsNew.Fields("sErro").Value = sErro
	rsNew.Update

Else

    rsNew.AddNew
    rsNew.Fields("nNotificacaoID").Value =  nNotificacaoID
    rsNew.Update

End If

rsNew.Save Response, adPersistXML
rsNew.Close

Set rsNew = Nothing
Set rsSPCommand = Nothing

%>
