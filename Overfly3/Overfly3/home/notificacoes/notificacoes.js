/********************************************************************
notificacoes.js

Library javascript para o notificacoes.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

var glb_Local_DATE_FORMAT = "DD/MM/YYYY";
var glb_Local_DATE_SQL_PARAM = 103;
var dsoCmbNotificacoesAnteriores = new CDatatransport("dsoCmbNotificacoesAnteriores");
var dsoCmbDuracao = new CDatatransport("dsoCmbDuracao");
var dsoCmbEmpresa = new CDatatransport("dsoCmbEmpresa");
var dsoNotificacao = new CDatatransport("dsoNotificacao");
var dsoNotificacoesAnteriores = new CDatatransport("dsoNotificacoesAnteriores");
var dsoDireitoNotificacao = new CDatatransport("dsoDireitoNotificacao");


// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Loop de mensagens
********************************************************************/
function wndJSProc(idElement, msg, param1, param2) {
    switch (msg) {
        default:
            return null;
    }
}

function window_OnUnload() {
    dealWithObjects_Unload();
}

/********************************************************************
Window onload
********************************************************************/
function window_onload() {
    // Garante inicializacao do lockControls
    lockControls(true);
    lockControls(false);

    // ajusta parametros fixos dos dsos da pagina
    dsoFixedParams();

    var elem;

    // variavel do js_gridEx.js
    //if (window.document.getElementById('fg') != null)
    //    glb_HasGridButIsNotFormPage = true;

    // configuracao inicial do html
    var sPag = setupPage();

    // se retorna false o usuario esta navegando pelo browser
    if (sPag == false)
        return null;

    // FG - grid
    // coloca classe no grid
    //elem = window.document.getElementById('fg');

    //if (elem != null)
    //    elem.className = 'fldGeneral';

    // ajusta o body do html
    elem = document.getElementById('notificacoesBody');

    with (elem) {
        style.border = 'none';
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    resetDateFormat();

    // a janela carregou
    sendJSMessage(getHtmlId(), JS_MSGRESERVED, 'PAGE_NOTIFICACOES', 'LOADED');

    // mostrar aqui a pagina de direitos
    sendJSMessage(getHtmlId(), JS_MSGRESERVED, 'PAGE_NOTIFICACOES', 'SHOW');
}

/********************************************************************
Configuracao inicial da pagina
********************************************************************/
function setupPage() {
    var elem;
    var nNextLeft, nNextTop;

    // o frame da pagina
    var frameID = getExtFrameID(window);

    var rectFrame = new Array();
    if (frameID)
        rectFrame = getRectFrameInHtmlTop(frameID);

    // Nao mostra se o frame esta com retangulo zerado
    // significa que o usuario esta navegando pelo browser
    if (rectFrame[0] == 0)
        return false;

    // ajusta o divBotoes
    elem = window.document.getElementById('divBotoes');
    with (elem.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = 0;
        top = ELEM_GAP;
        width = rectFrame[2] - ELEM_GAP;
        height = 30;
    }

    // ajusta o botao Voltar
    elem = window.document.getElementById('btnVoltar');
    with (elem.style) {
        height = 24;
        width = 59;
        left = 0;
        top = (ELEM_GAP - 7);
    }

    // ajusta o botao Colaboradores
    elem = window.document.getElementById('btnColaboradores');
    with (elem.style) {
        backgroundColor = "#C7C1B7";
        height = parseInt(btnVoltar.currentStyle.height, 10);
        width = 114;
        left = parseInt(btnVoltar.currentStyle.left, 10) + parseInt(btnVoltar.currentStyle.width, 10) + ELEM_GAP;
        top = parseInt(btnVoltar.currentStyle.top, 10);
    }

    // ajusta o botao Delegacoes
    elem = window.document.getElementById('btnDelegacoes');
    with (elem.style) {
        backgroundColor = "#C7C1B7";
        height = parseInt(btnColaboradores.currentStyle.height, 10);
        width = parseInt(btnColaboradores.currentStyle.width, 10);
        left = parseInt(btnColaboradores.currentStyle.left, 10) + parseInt(btnColaboradores.currentStyle.width, 10) - 3;
        top = parseInt(btnColaboradores.currentStyle.top, 10);
    }

    // ajusta o botao Feriados
    elem = window.document.getElementById('btnFeriados');
    with (elem.style) 
    {
        backgroundColor = "#C7C1B7";
        height = parseInt(btnDelegacoes.currentStyle.height, 10);
        width = parseInt(btnDelegacoes.currentStyle.width, 10);
        left = parseInt(btnDelegacoes.currentStyle.left, 10) + parseInt(btnDelegacoes.currentStyle.width, 10) - 3;
        top = parseInt(btnDelegacoes.currentStyle.top, 10);
    }

    // ajusta o botao Vendas
    elem = window.document.getElementById('btnVendas');
    with (elem.style) {
        backgroundColor = "#C7C1B7";
        height = parseInt(btnFeriados.currentStyle.height, 10);
        width = parseInt(btnFeriados.currentStyle.width, 10);
        left = parseInt(btnFeriados.currentStyle.left, 10) + parseInt(btnFeriados.currentStyle.width, 10) - 3;
        top = parseInt(btnFeriados.currentStyle.top, 10);
    }

    // Verifica direitos de acesso �s vendas
    verificaDireitoVendas();

    // ajusta o botao Notifica��es
    elem = window.document.getElementById('btnNotificacoes');
    with (elem.style) 
    {
        visibility = 'hidden';
        backgroundColor = "#B4D2E3";
        height = parseInt(btnVendas.currentStyle.height, 10);
        width = parseInt(btnVendas.currentStyle.width, 10);
        left = parseInt(btnVendas.currentStyle.left, 10) + parseInt(btnVendas.currentStyle.width, 10) - 3;
        top = parseInt(btnVendas.currentStyle.top, 10);
    }

    // Verifica direitos de acesso �s notifica��es
    verificaDireitoNotificacao();

    // ajusta o divControls
    elem = window.document.getElementById('divControls');
    with (elem.style) 
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = 0;
        top = parseInt(divBotoes.currentStyle.top, 10) + parseInt(divBotoes.currentStyle.height, 10) + ELEM_GAP;
        width = 645;
        height = 490;
    }

    elem = window.document.getElementById('lblNotificacoesAnteriores');
    with (elem.style) {
        left = 0;
        top = 5;
    }

    elem = window.document.getElementById('txtNotificacoesAnteriores');
    elem.maxLength = 10;
    with (elem.style) {
        width = 80;
        left = parseInt(lblNotificacoesAnteriores.currentStyle.left, 10);
        top = parseInt(lblNotificacoesAnteriores.currentStyle.top, 10) + parseInt(lblNotificacoesAnteriores.currentStyle.height, 10);
    }

    txtNotificacoesAnteriores.onkeypress = txtNotificacoesAnteriores_onKeyPress;

    elem = window.document.getElementById('selNotificacoesAnteriores');
    elem.onchange = selNotificacoesAnteriores_onchange;
    with (elem.style) {
        width = 440;
        left = parseInt(txtNotificacoesAnteriores.currentStyle.left, 10) + parseInt(txtNotificacoesAnteriores.currentStyle.width, 10);
        top = parseInt(txtNotificacoesAnteriores.currentStyle.top, 10);
    }

    elem = window.document.getElementById('lblInicio');
    with (elem.style) 
    {
        left = parseInt(lblNotificacoesAnteriores.currentStyle.left, 10);
        top = parseInt(txtNotificacoesAnteriores.currentStyle.top, 10) + parseInt(txtNotificacoesAnteriores.currentStyle.height, 10) + ELEM_GAP;
    }

    elem = window.document.getElementById('txtInicio');
    elem.onkeypress = verifyDateTimeNotLinked;
    elem.maxLength = 16;
    with (elem.style) {
        width = 130;
        left = parseInt(lblInicio.currentStyle.left, 10);
        top = parseInt(lblInicio.currentStyle.top, 10) + parseInt(lblInicio.currentStyle.height, 10);
    }

    elem = window.document.getElementById('lblDuracao');
    with (elem.style) {
        left = parseInt(txtInicio.currentStyle.left, 10) + parseInt(txtInicio.currentStyle.width, 10) + ELEM_GAP;
        top = parseInt(lblInicio.currentStyle.top, 10);
    }

    elem = window.document.getElementById('selDuracao');
    with (elem.style) {
        width = 110;
        left = parseInt(txtInicio.currentStyle.left, 10) + parseInt(txtInicio.currentStyle.width, 10) + ELEM_GAP;
        top = parseInt(lblInicio.currentStyle.top, 10) + parseInt(lblInicio.currentStyle.height, 10);
    }

    elem = window.document.getElementById('lblEmpresa');
    with (elem.style) {
        left = parseInt(selDuracao.currentStyle.left, 10) + parseInt(selDuracao.currentStyle.width, 10) + ELEM_GAP;
        top = parseInt(lblDuracao.currentStyle.top, 10);
    }

    elem = window.document.getElementById('selEmpresa');
    with (elem.style) 
    {
        width = 170;
        left = parseInt(selDuracao.currentStyle.left, 10) + parseInt(selDuracao.currentStyle.width, 10) + ELEM_GAP;
        top = parseInt(lblInicio.currentStyle.top, 10) + parseInt(lblInicio.currentStyle.height, 10);
    }

    elem = window.document.getElementById('lblOverfly');
    with (elem.style) 
    {
        width = 40;
        left = parseInt(selEmpresa.currentStyle.left, 10) + parseInt(selEmpresa.currentStyle.width, 10) + ELEM_GAP;
        top = parseInt(lblEmpresa.currentStyle.top, 10);
    }

    elem = window.document.getElementById('chkOverfly');
    with (elem.style) 
    {
        width = 25;
        left = parseInt(selEmpresa.currentStyle.left, 10) + parseInt(selEmpresa.currentStyle.width, 10) + ELEM_GAP;
        top = parseInt(lblOverfly.currentStyle.top, 10) + parseInt(lblOverfly.currentStyle.height, 10);
    }

    elem = window.document.getElementById('lblCritica');
    with (elem.style) {
        width = 40;
        left = parseInt(lblOverfly.currentStyle.left, 10) + parseInt(lblOverfly.currentStyle.width, 10) + ELEM_GAP;
        top = parseInt(lblOverfly.currentStyle.top, 10);
    }

    elem = window.document.getElementById('chkCritica');
    with (elem.style) {
        width = 25;
        left = parseInt(lblOverfly.currentStyle.left, 10) + parseInt(lblOverfly.currentStyle.width, 10) + ELEM_GAP;
        top = parseInt(chkOverfly.currentStyle.top, 10);
    }

    elem = window.document.getElementById('lblTituloNotificacao');
    with (elem.style) 
    {
        top = parseInt(selEmpresa.currentStyle.top, 10) + parseInt(selEmpresa.currentStyle.height, 10) + ELEM_GAP;
    }

    elem = window.document.getElementById('txtTituloNotificacao');
    elem.maxLength = 60;
    with (elem.style) 
    {
        width = parseInt(txtNotificacoesAnteriores.currentStyle.width, 10) + parseInt(selNotificacoesAnteriores.currentStyle.width, 10);
        top = parseInt(lblTituloNotificacao.currentStyle.top, 10) + parseInt(lblTituloNotificacao.currentStyle.height, 10);
    }

    elem = window.document.getElementById('lblNotificacao');
    with (elem.style) {
        top = parseInt(txtTituloNotificacao.currentStyle.top, 10) + parseInt(txtTituloNotificacao.currentStyle.height, 10) + ELEM_GAP;
    }

    elem = window.document.getElementById('txtNotificacao');
    with (elem.style) {
        width = parseInt(txtTituloNotificacao.currentStyle.width, 10);
        height = 55;
        top = parseInt(lblNotificacao.currentStyle.top, 10) + parseInt(lblNotificacao.currentStyle.height, 10);
    }

    elem = window.document.getElementById('lblEmail');
    with (elem.style) {
        top = parseInt(txtNotificacao.currentStyle.top, 10) + parseInt(txtNotificacao.currentStyle.height, 10) + ELEM_GAP;
    }

    elem = window.document.getElementById('txtEmail');
    elem.maxLength = 80;
    with (elem.style) {
        width = parseInt(txtNotificacao.currentStyle.width, 10);
        top = parseInt(lblEmail.currentStyle.top, 10) + parseInt(lblEmail.currentStyle.height, 10);
    }

    elem = window.document.getElementById('lblAssunto');
    with (elem.style) {
        top = parseInt(txtEmail.currentStyle.top, 10) + parseInt(txtEmail.currentStyle.height, 10) + ELEM_GAP;
    }

    elem = window.document.getElementById('txtAssunto');
    elem.maxLength = 100;
    with (elem.style) {
        width = parseInt(txtEmail.currentStyle.width, 10);
        top = parseInt(lblAssunto.currentStyle.top, 10) + parseInt(lblAssunto.currentStyle.height, 10);
    }

    elem = window.document.getElementById('btnIncluir');
    with (elem.style) {
        height = 24;
        width = 60;
        left = parseInt(txtAssunto.currentStyle.left, 10) + parseInt(txtAssunto.currentStyle.width, 10) - parseInt(btnIncluir.currentStyle.width, 10);
        top = parseInt(txtAssunto.currentStyle.top, 10) + parseInt(txtAssunto.currentStyle.height, 10) + ELEM_GAP;
    }

    setarVariaveis();
}

/*****************************************************************************************
Funcao criada para ser usada no notificacoes.js, pois no carregamento da pagina
o overflygen ainda nao tinha sido criado, entao a constante DATE_FORMAT ficou com
o valor default que eh o formato brasileiro. Mostrar p/ o Ze
*****************************************************************************************/
function resetDateFormat() {
    DATE_FORMAT = overflyGen.ShortDateFormat();

    if (DATE_FORMAT == "DD/MM/YYYY") {
        glb_Local_DATE_FORMAT = "DD/MM/YYYY";
        glb_Local_DATE_SQL_PARAM = 103;
    }
    else if (DATE_FORMAT == "MM/DD/YYYY") {
        glb_Local_DATE_FORMAT = "MM/DD/YYYY";
        glb_Local_DATE_SQL_PARAM = 101;
    }
}

/********************************************************************
Obtem dados no banco e carrega o combo de Notifica��es Anteriores
********************************************************************/
function fillCmbNotificacoesAnteriores() {
    // parametrizacao do dso dsoCmbNotificacoesAnteriores
    setConnection(dsoCmbNotificacoesAnteriores);

    dsoCmbNotificacoesAnteriores.SQL = 'SELECT \'\' AS NotificacaoAnterior ' +
                                       'UNION ALL ' +
                                       'SELECT TOP 200 a.RegistroDescricao AS NotificacaoAnterior ' +
	                                       'FROM Notificacoes a WITH(NOLOCK) ' +
	                                       'WHERE (((a.RegistroDescricao LIKE (\'%\' + \'' + (txtNotificacoesAnteriores.value) + '\' + \'%\')) ' +
	                                                                                'OR (\'' + (txtNotificacoesAnteriores.value) + '\' = \'\')) ' +
	                                            'AND (a.UsuarioID <> 999) AND ((DATEDIFF(DD, a.dtNotificacao, (CONVERT(DATETIME, \'' + (txtInicio.value) + '\', 103)))) < 365)) ' +
	                                       'GROUP BY a.RegistroDescricao ' +
	                                       'ORDER BY NotificacaoAnterior ';

    dsoCmbNotificacoesAnteriores.ondatasetcomplete = fillCmbNotificacoesAnteriores_DSC;
    dsoCmbNotificacoesAnteriores.Refresh();
}

/********************************************************************
Preenche o combo de Notifica��es Anteriores
********************************************************************/
function fillCmbNotificacoesAnteriores_DSC() {
    var optionStr;
    var optionValue;
    var oOption;
    var i = 0;

    clearComboEx(['selNotificacoesAnteriores']);

    while (!dsoCmbNotificacoesAnteriores.recordset.EOF) {
        optionStr = dsoCmbNotificacoesAnteriores.recordset['NotificacaoAnterior'].value;
        optionValue = i;
        oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        //oOption.selected = (i == 0);
        selNotificacoesAnteriores.add(oOption);

        i += 1;
        dsoCmbNotificacoesAnteriores.recordset.MoveNext();
    }
}


/********************************************************************
Controla evendo onchange do selNotificacoesAnteriores
********************************************************************/
function selNotificacoesAnteriores_onchange() {
    var sNotificacaoAnterior = selNotificacoesAnteriores[selNotificacoesAnteriores.selectedIndex].text;

    if (sNotificacaoAnterior == '')
        setarVariaveis();
    else {
        setarVariaveis(sNotificacaoAnterior);
    }
}

/********************************************************************
Obtem dados no banco e carrega o combo de Dura��o
********************************************************************/
function fillCmbDuracao(nDuracao) 
{
    if (nDuracao == null) {
        // parametrizacao do dso dsoCmbDuracao
        setConnection(dsoCmbDuracao);

        dsoCmbDuracao.SQL = 'SELECT 0 AS id, \'Final do dia\' AS fld UNION ALL ' +
                            'SELECT 1 AS id, \'1 dia\' AS fld UNION ALL ' +
                            'SELECT 2 AS id, \'2 dias\' AS fld UNION ALL ' +
                            'SELECT 3 AS id, \'3 dias\' AS fld UNION ALL ' +
                            'SELECT 5 AS id, \'5 dias\' AS fld UNION ALL ' +
                            'SELECT 7 AS id, \'7 dias\' AS fld UNION ALL ' +
                            'SELECT 15 AS id, \'15 dias\' AS fld UNION ALL ' +
                            'SELECT 30 AS id, \'30 dias\' AS fld ';

        dsoCmbDuracao.ondatasetcomplete = fillCmbDuracao_DSC;
        dsoCmbDuracao.Refresh();
    }
    else {
        //selDuracao.selectedIndex = nDuracao;
        selDuracao.value = nDuracao;    
    }
}

/********************************************************************
Preenche o combo de Dura��o
********************************************************************/
function fillCmbDuracao_DSC() {
    var optionStr;
    var optionValue;
    var oOption;

    clearComboEx(['selDuracao']);

    while (!dsoCmbDuracao.recordset.EOF) {
        optionStr = dsoCmbDuracao.recordset['fld'].value;
        optionValue = dsoCmbDuracao.recordset['id'].value;
        oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        oOption.selected = (dsoCmbDuracao.recordset['id'].value == 0);
        selDuracao.add(oOption);

        dsoCmbDuracao.recordset.MoveNext();
    }
}

/********************************************************************
Obtem dados no banco e carrega o combo de Empresas
********************************************************************/
function fillCmbEmpresa(nEmpresaID) 
{
    if (nEmpresaID == null) {
        // parametrizacao do dso dsoCmbEmpresa
        setConnection(dsoCmbEmpresa);

        dsoCmbEmpresa.SQL = 'SELECT 0 AS EmpresaID, \'\' AS EmpresaFantasia, \'\' AS Ordem, 0 AS Def ' +
                        'UNION ALL ' +
                        'SELECT DISTINCT c.PessoaID AS EmpresaID, c.Fantasia AS EmpresaFantasia, c.Fantasia AS Ordem, ' +
				                        'CONVERT(BIT, CASE WHEN ((b.EmpresaID = a.EmpresaDefaultID) AND (a.SujeitoID = ' + glb_nUsuarioID + ')) THEN 1 ELSE 0 END) AS Def ' +
	                        'FROM RelacoesPesRec a WITH(NOLOCK) ' +
                               'INNER JOIN RelacoesPesRec_Perfis b WITH(NOLOCK) ON (b.RelacaoID = a.RelacaoID) ' +
	                           'INNER JOIN Pessoas c WITH(NOLOCK) ON (c.PessoaID = b.EmpresaID) ' +
	                        'WHERE a.SujeitoID IN (	SELECT ' + glb_nUsuarioID + ' ' +
							                        'UNION ALL ' +
							                        'SELECT aaa.UsuarioDeID ' +
							                        'FROM DelegacaoDireitos aaa WITH(NOLOCK) ' +
							                        'WHERE (aaa.UsuarioParaID = ' + glb_nUsuarioID + ' AND GETDATE() BETWEEN aaa.dtInicio AND aaa.dtFim)) ' +
                               'AND a.ObjetoID = 999 ' +
                               'AND a.EstadoID = 2 ' +
                               'AND a.TipoRelacaoID = 11 ' +
	                        'ORDER BY Ordem, Def DESC ';

        dsoCmbEmpresa.ondatasetcomplete = fillCmbEmpresa_DSC;
        dsoCmbEmpresa.Refresh();
    }
    else {
        selEmpresa.value = nEmpresaID;
    }
}

/********************************************************************
Preenche o combo de Empresas
********************************************************************/
function fillCmbEmpresa_DSC() {
    var optionStr;
    var optionValue;
    var oOption;

    clearComboEx(['selEmpresa']);
    dsoCmbEmpresa.recordset.MoveFirst();

    while (!dsoCmbEmpresa.recordset.EOF) {
        
        optionStr = dsoCmbEmpresa.recordset['EmpresaFantasia'].value;
        optionValue = dsoCmbEmpresa.recordset['EmpresaID'].value;
        oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        oOption.selected = (dsoCmbEmpresa.recordset['Def'].value);
        selEmpresa.add(oOption);

        dsoCmbEmpresa.recordset.MoveNext();
    }
}
/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl) {

    // ctl.id retorna o id do botao clicado
    // O usuario clicou o botao Voltar
    if (ctl.id == btnVoltar.id) 
    {
        fecharNotificacoes();
    }

    // O usuario clicou o botao
    else if (ctl.id == btnColaboradores.id) {
        lockControls(true);

        // volta para e-mails
        sendJSMessage(getHtmlId(), JS_MSGRESERVED, 'PAGE_NOTIFICACOES', 'HIDE');
    }

    // O usuario clicou o botao Delega��es
    else if (ctl.id == btnDelegacoes.id) {
        lockControls(true);
        sendJSMessage(getHtmlId(), JS_MSGRESERVED, 'PAGE_NOTIFICACOES', 'HIDE');
        sendJSMessage(getHtmlId(), JS_MSGRESERVED, 'PAGE_DELEGACOES', 'LOAD');
    }

    // O usuario clicou o botao Feriados
    else if (ctl.id == btnFeriados.id) {
        lockControls(true);
        sendJSMessage(getHtmlId(), JS_MSGRESERVED, 'PAGE_NOTIFICACOES', 'HIDE');
        sendJSMessage(getHtmlId(), JS_MSGRESERVED, 'PAGE_FERIADOS', 'LOAD');
    }

        // O usuario clicou o botao Vendas
    else if (ctl.id == btnVendas.id) {
        lockControls(true);
        sendJSMessage(getHtmlId(), JS_MSGRESERVED, 'PAGE_NOTIFICACOES', 'HIDE');
        sendJSMessage(getHtmlId(), JS_MSGRESERVED, 'PAGE_VENDAS', 'LOAD');
    }

    // ctl.id retorna o id do botao clicado
    if (ctl.id == btnIncluir.id) 
    {
        geraNotificacao();
    }
}

/********************************************************************
Fun��o que obt�m data atual para preencher txtInicio
********************************************************************/
function getDateTime(date) {
    if (date == null)
        date = new Date();

    var strDateTime = AddZero(date.getDate()) + '/' + AddZero(date.getMonth() + 1) + '/' + date.getFullYear() + ' ' +
                        AddZero(date.getHours()) + ':' + AddZero(date.getMinutes());

    return strDateTime;
}

/********************************************************************
Fun��o complementar da getDateTime()
********************************************************************/
function AddZero(num) {
    return (num >= 0 && num < 10) ? "0" + num : num + "";
}

/********************************************************************
Fun��o complementar da getDateTime()
********************************************************************/
function calculaDtFim(duracao) {
    var dtInicio;
    dtInicio = normalizeDate_DateTime(txtInicio.value, true);

    var dtFim;
    dtFim = new Date(dtInicio);

    dtFim.setDate(dtFim.getDate() + parseInt(duracao));

    return getDateTime(dtFim);
}

/********************************************************************
Fun��o complementar da geraNotificacao()
********************************************************************/
function geraNotificacao() 
{
    lockControls(true);

    var strPars = new String();
    var dtInicio, dtFim, nEmpresaID, bOverfly, nFormID, nSubFormID, nRegistroID,
        sTituloNotificacao, sNotificacao, sEmail, sAssunto, bCritica, nRelevancia, nUsuarioID, sPessoas;
    var sValidacao = "";

    dtInicio = txtInicio.value;
    nEmpresaID = selEmpresa.value;
    bOverfly = (chkOverfly.checked);
    nFormID = null;
    nSubFormID = null;
    nRegistroID = null;
    sTituloNotificacao = txtTituloNotificacao.value;
    sNotificacao = txtNotificacao.value;
    sEmail = (txtEmail.value == "" ? null : txtEmail.value);
    sAssunto = (txtAssunto.value == "" ? null : txtAssunto.value);
    bCritica = (chkCritica.checked);
    nRelevancia = 1;
    nUsuarioID = glb_nUsuarioID;
    sPessoas = "|NULL|";

    if (chkOverfly.checked) 
    {
        nFormID = 999;
    }
    
    // Verifica se data � v�lida
    if (chkDataEx(dtInicio)) 
    {
        // Normaliza data inicio
        dtInicio = normalizeDate_DateTime(txtInicio.value, true);
        
        // Calcula dtFim em rela��o � dura��o escolhida
        dtFim = calculaDtFim(selDuracao.value);
        dtFim = normalizeDate_DateTime(dtFim, true);
    }
    else 
    {
        sValidacao += 'Data In�cio inv�lida\n';
    }

    if (txtTituloNotificacao.value == "")
        sValidacao += 'T�tulo da Notifica��o obrigat�rio\n';

    if (txtNotificacao.value == "")
        sValidacao += 'Notifica��o obrigat�ria\n';

    if (sValidacao != "") 
    {
        if (overflyGen.Alert(sValidacao) == 0)
            return null;

        lockControls(false);
        return null;
    }

    if (nEmpresaID == "")
        nEmpresaID = null;
    
    strPars  = '?sDataInicio=' + escape(dtInicio);
    strPars += '&sDataFim=' + escape(dtFim);
    strPars += '&nEmpresaID=' + escape(nEmpresaID);
    strPars += '&nFormID=' + escape(nFormID);
    strPars += '&nSubFormID=' + escape(nSubFormID);
    strPars += '&nRegistroID=' + escape(nRegistroID);
    strPars += '&bOverfly=' + escape((bOverfly) ? 1 : 0);
    strPars += '&sTituloNotificacao=' + escape(sTituloNotificacao);
    strPars += '&sNotificacao=' + escape(sNotificacao);
    strPars += '&sEmail=' + escape(sEmail);
    strPars += '&sAssunto=' + escape(sAssunto);
    strPars += '&bCritica=' + escape((bCritica) ? 1 : 0);
    strPars += '&nRelevancia=' + escape(nRelevancia);
    strPars += '&nUsuarioID=' + escape(nUsuarioID);
    strPars += '&sPessoas=' + escape(sPessoas);

    dsoNotificacao.URL = SYS_ASPURLROOT + '/home/notificacoes/serverside/gravanotificacao.asp' + strPars;
    dsoNotificacao.ondatasetcomplete = geraNotificacao_DSC;
    dsoNotificacao.refresh();
}

/********************************************************************
Retorno do servidor, apos executar sp_Notificacao_Gerador
********************************************************************/
function geraNotificacao_DSC() 
{
    lockControls(false);

    setarVariaveis();
    fecharNotificacoes();
}

/********************************************************************
Seta componentes do asp
********************************************************************/
function setarVariaveis(sNotificacaoAnterior)
{
    if (sNotificacaoAnterior == null) {
        txtInicio.value = getDateTime();
        fillCmbNotificacoesAnteriores();
        fillCmbDuracao();
        fillCmbEmpresa();
        chkOverfly.checked = false;
        txtTituloNotificacao.value = "";
        txtNotificacao.value = "";
        txtEmail.value = "";
        txtAssunto.value = "";
        
        txtNotificacoesAnteriores.focus();
    }
    else {
        preencheCampos(sNotificacaoAnterior);
        selNotificacoesAnteriores.focus();
    }
}

/********************************************************************
Busca informa��es da notifica��o anterior para preencher campos
********************************************************************/
function preencheCampos(sNotificacaoAnterior) 
{
    lockControls(true);
    
    setConnection(dsoNotificacoesAnteriores);

    dsoNotificacoesAnteriores.SQL = 'SELECT TOP 1 (DATEDIFF(dd, a.dtInicio, a.dtFim)) AS Duracao, a.EmpresaID AS EmpresaID, ' +
		                                    '(CASE WHEN a.FormID = 999 THEN 1 ELSE 0 END) Overfly, a.Critica AS Critica, ' +
		                                    'a.RegistroDescricao AS TituloNotificacao, a.Notificacao AS Notificacao, ' +
		                                    'a.Email AS Email, a.Assunto AS Assunto ' +
	                                    'FROM Notificacoes a WITH(NOLOCK) ' +
	                                    'WHERE ((RegistroDescricao LIKE \'' + sNotificacaoAnterior + '\') AND (UsuarioID <> 999) ' +
		                                    'AND ((DATEDIFF(DD, a.dtNotificacao, CONVERT(DATETIME, \'' + txtInicio.value + '\', 103))) < 365)) ' +
	                                    'ORDER BY a.NotificacaoID DESC ';

    dsoNotificacoesAnteriores.ondatasetcomplete = preencheCampos_DSC;
    dsoNotificacoesAnteriores.Refresh();
}

/********************************************************************
Preenche o combo de Empresas
********************************************************************/
function preencheCampos_DSC() {
    var nDuracao, nEmpresaID, bOverfly, bCritica, sTituloNotificacao, sNotificacao, sEmail, sAssunto;

    if (!dsoNotificacoesAnteriores.recordset.EOF) 
    {
        dsoNotificacoesAnteriores.recordset.MoveFirst();
        
        //txtInicio.value = getDateTime();
        //fillCmbNotificacoesAnteriores();

        nDuracao = dsoNotificacoesAnteriores.recordset['Duracao'].value;
        nEmpresaID = dsoNotificacoesAnteriores.recordset['EmpresaID'].value;
        bOverfly = dsoNotificacoesAnteriores.recordset['Overfly'].value;
        bCritica = dsoNotificacoesAnteriores.recordset['Critica'].value;
        sTituloNotificacao = dsoNotificacoesAnteriores.recordset['TituloNotificacao'].value;
        sNotificacao = dsoNotificacoesAnteriores.recordset['Notificacao'].value;
        sEmail = dsoNotificacoesAnteriores.recordset['Email'].value;
        sAssunto = dsoNotificacoesAnteriores.recordset['Assunto'].value;
    }

    fillCmbDuracao(nDuracao);
    fillCmbEmpresa(((nEmpresaID == null) ? "NULL" : nEmpresaID));
    chkOverfly.checked = (bOverfly == "1");
    chkCritica.checked = (bCritica == "1");
    txtTituloNotificacao.value = sTituloNotificacao;
    txtNotificacao.value = sNotificacao;
    txtEmail.value = ((sEmail == null) ? '' : sEmail);
    txtAssunto.value = ((sAssunto == null) ? '' : sAssunto);

    lockControls(false);
}

/********************************************************************
Finaliza janela Notifica��es
********************************************************************/
function fecharNotificacoes()
{
    lockControls(true);

    // volta para logo
    sendJSMessage(getHtmlId(), JS_MSGRESERVED, 'PAGE_NOTIFICACOES', 'HIDE');
    sendJSMessage(getHtmlId(), JS_MSGRESERVED, 'PAGE_COLABORADORES', 'CANC');

    lockControls(false);
}

/********************************************************************
Filtra combo de Notifica��es
********************************************************************/
function txtNotificacoesAnteriores_onKeyPress()
{
    // Se pressionar tecla Enter
    if (event.keyCode == 13) 
    {
        var i = 0;
        var sBusca = txtNotificacoesAnteriores.value.toUpperCase();
        
        fillCmbNotificacoesAnteriores();

        while (i < selNotificacoesAnteriores.length) 
        {
            if (!(selNotificacoesAnteriores.options[i].text.toUpperCase().indexOf(sBusca) < 0)) 
            {
                selNotificacoesAnteriores.options[i].selected = true;
                selNotificacoesAnteriores_onchange();
                break;
            }

            i += 1;
        }
    }
}

/********************************************************************
Obtem direitos da pessoa no banco
********************************************************************/
function verificaDireitoNotificacao() {
    // parametrizacao do dso dsoCmbCol
    setConnection(dsoDireitoNotificacao);

    var empresaData = getCurrEmpresaData();

    var nEmpresaID = escape(empresaData[0]).toString();
    var nPessoaID = (window.top.glb_CURRUSERID).toString();
    var nOverflyID = '999';

    var strPars = new String();

    strPars = '?nEmpresaID=' + escape(nEmpresaID);
    strPars += '&nPessoaID=' + escape(nPessoaID);
    strPars += '&nOverflyID=' + escape(nOverflyID);

    dsoDireitoNotificacao.SQL = 'SELECT dbo.fn_Direitos(' + nEmpresaID + ', ' + nOverflyID + ', NULL, NULL, NULL, NULL, NULL, 3, ' + nPessoaID + ', 1, 1) AS Direito';
    dsoDireitoNotificacao.ondatasetcomplete = verificaDireitoNotificacao_DSC;
    dsoDireitoNotificacao.Refresh();
}

function verificaDireitoVendas() {
    var elem = window.document.getElementById('btnVendas');
    var nPessoaID = (window.top.glb_CURRUSERID).toString();
    var aPessoas = new Array(1336, 1679, 1002, 44065, 1142, 1004, 1069);

    for (var i = 0; i < aPessoas.length; i++) {
        if (aPessoas[i] == nPessoaID) {
            elem.style.visibility = 'visible';

            break;
        }
        else {
            elem.style.visibility = 'hidden';
        }
    }
}

/********************************************************************
Retorno da fun��o verificaDireitoNotificacao
********************************************************************/
function verificaDireitoNotificacao_DSC() {
    var elem = window.document.getElementById('btnNotificacoes');

    if (dsoDireitoNotificacao.recordset['Direito'].value)
        elem.style.visibility = 'visible';
    else
        elem.style.visibility = 'hidden';
}
