/********************************************************************
feriados.js

Library javascript para o feriados.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

var glb_Local_DATE_FORMAT = "DD/MM/YYYY";
var glb_Local_DATE_SQL_PARAM = 103;
var dsoGrid = new CDatatransport("dsoGrid");
var dsoCmbCol = new CDatatransport("dsoCmbCol");
var dsoDireitoNotificacao = new CDatatransport("dsoDireitoNotificacao");
// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Loop de mensagens
********************************************************************/
function wndJSProc(idElement, msg, param1, param2)
{
    switch (msg)
    {
        default:
            return null;
    }
}

function window_OnUnload()
{
	dealWithObjects_Unload();
}

/********************************************************************
Window onload
********************************************************************/
function window_onload()
{
	// Garante inicializacao do lockControls
	lockControls(true);
	lockControls(false);
	
	// ajusta parametros fixos dos dsos da pagina
    dsoFixedParams();
	
    var elem;
    
    // variavel do js_gridEx.js
    if (window.document.getElementById('fg') != null)
        glb_HasGridButIsNotFormPage = true;
    
	// configuracao inicial do html
    var sPag = setupPage();   
    
    // se retorna false o usuario esta navegando pelo browser
    if ( sPag == false )
        return null;
    
    // FG - grid
    // coloca classe no grid
    elem = window.document.getElementById('fg');
    
    if ( elem != null )
        elem.className = 'fldGeneral';

    // ajusta o body do html
    elem = document.getElementById('feriadosBody');

    with (elem)
    {
        style.border = 'none'; 
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

	resetDateFormat();

    // a janela carregou
    sendJSMessage( getHtmlId(), JS_MSGRESERVED, 'PAGE_FERIADOS', 'LOADED');
    
    // mostrar aqui a pagina de direitos
    sendJSMessage( getHtmlId(), JS_MSGRESERVED, 'PAGE_FERIADOS', 'SHOW');
    
	fillCmbAno();
}

/********************************************************************
Configuracao inicial da pagina
********************************************************************/
function setupPage()
{
    var elem;
    var nNextLeft, nNextTop;

    // o frame da pagina
    var frameID = getExtFrameID(window);

    var rectFrame = new Array();
    if (frameID)
        rectFrame = getRectFrameInHtmlTop(frameID);

    // Nao mostra se o frame esta com retangulo zerado
    // significa que o usuario esta navegando pelo browser
    if (rectFrame[0] == 0)
        return false;

    // ajusta o divBotoes
    elem = window.document.getElementById('divBotoes');
    with (elem.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = 0;
        top = ELEM_GAP;
        width = rectFrame[2] - ELEM_GAP;
        height = 30;
    }

    // ajusta o botao Voltar
    elem = window.document.getElementById('btnVoltar');
    with (elem.style) {
        height = 24;
        width = 59;
        left = 0;
        top = (ELEM_GAP - 7);
    }

    // ajusta o botao Colaboradores
    elem = window.document.getElementById('btnColaboradores');
    with (elem.style) {
        backgroundColor = "#C7C1B7";
        height = parseInt(btnVoltar.currentStyle.height, 10);
        width = 114;
        left = parseInt(btnVoltar.currentStyle.left, 10) + parseInt(btnVoltar.currentStyle.width, 10) + ELEM_GAP;
        top = parseInt(btnVoltar.currentStyle.top, 10);
    }

    // ajusta o botao Delegacoes
    elem = window.document.getElementById('btnDelegacoes');
    with (elem.style) {
        backgroundColor = "#C7C1B7";
        height = parseInt(btnColaboradores.currentStyle.height, 10);
        width = parseInt(btnColaboradores.currentStyle.width, 10);
        left = parseInt(btnColaboradores.currentStyle.left, 10) + parseInt(btnColaboradores.currentStyle.width, 10) - 3;
        top = parseInt(btnColaboradores.currentStyle.top, 10);
    }

    // ajusta o botao Feriados
    elem = window.document.getElementById('btnFeriados');
    with (elem.style) {
        backgroundColor = "#B4D2E3";
        height = parseInt(btnDelegacoes.currentStyle.height, 10);
        width = parseInt(btnDelegacoes.currentStyle.width, 10);
        left = parseInt(btnDelegacoes.currentStyle.left, 10) + parseInt(btnDelegacoes.currentStyle.width, 10) - 3;
        top = parseInt(btnDelegacoes.currentStyle.top, 10);
    }

    // ajusta o botao Vendas
    elem = window.document.getElementById('btnVendas');
    with (elem.style) {
        backgroundColor = "#C7C1B7";
        height = parseInt(btnFeriados.currentStyle.height, 10);
        width = parseInt(btnFeriados.currentStyle.width, 10);
        left = parseInt(btnFeriados.currentStyle.left, 10) + parseInt(btnFeriados.currentStyle.width, 10) - 3;
        top = parseInt(btnFeriados.currentStyle.top, 10);
    }

    // Verifica direitos de acesso �s vendas
    verificaDireitoVendas();

    // ajusta o botao Notifica��es
    elem = window.document.getElementById('btnNotificacoes');
    with (elem.style) {
        visibility = 'hidden';
        backgroundColor = "#C7C1B7";
        height = parseInt(btnVendas.currentStyle.height, 10);
        width = parseInt(btnVendas.currentStyle.width, 10);
        left = parseInt(btnVendas.currentStyle.left, 10) + parseInt(btnVendas.currentStyle.width, 10);
        top = parseInt(btnVendas.currentStyle.top, 10);
    }

    // Verifica direitos de acesso �s notifica��es
    verificaDireitoNotificacao();

    // ajusta o divControls
    elem = window.document.getElementById('divControls');
    with (elem.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = 0;
        top = parseInt(divBotoes.currentStyle.top, 10) + parseInt(divBotoes.currentStyle.height, 10) + ELEM_GAP;
        width = rectFrame[2] - 2 * ELEM_GAP;
        height = 40;
    }

    // elementos contidos em divControls
    adjustElementsInForm([['lblAno', 'selAno', 8, 1, -10]], null, null, true);
                          
/*
    // ajusta o combo Ano
    elem = window.document.getElementById('selAno');
    elem.disabled = false;
    with (elem.style) {
        fontSize = '10 pt';
        top = ELEM_GAP;
        width = 100;
        left = 393;
        nNextLeft = parseInt(elem.currentStyle.left, 10) +
                    parseInt(elem.currentStyle.width, 10) +
                    ELEM_GAP;
        nNextTop = parseInt(elem.currentStyle.top, 10);
    }
*/
    
    // ajusta o divFG
    elem = window.document.getElementById('divFG');
    with (elem.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = 0;
        top = (parseInt(divControls.currentStyle.top, 10) + parseInt(divControls.currentStyle.height, 10) + ELEM_GAP);
        width = rectFrame[2] - ELEM_GAP;
        height = 435;
    }

    elem = document.getElementById('fg');
    with (elem.style) {
        left = 0;
        top = 0;
        width = parseInt(document.getElementById('divFG').currentStyle.width);
        height = parseInt(document.getElementById('divFG').currentStyle.height);
    }
    
    startGridInterface(fg, 1, 1);
    
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;

    selAno.onchange = selAnoChange;
}

/*****************************************************************************************
Funcao criada para ser usada no feriados.js, pois no carregamento da pagina
o overflygen ainda nao tinha sido criado, entao a constante DATE_FORMAT ficou com
o valor default que eh o formato brasileiro. Mostrar p/ o Ze
*****************************************************************************************/
function resetDateFormat()
{
	DATE_FORMAT = overflyGen.ShortDateFormat();
	
	if ( DATE_FORMAT == "DD/MM/YYYY" )
	{
		glb_Local_DATE_FORMAT = "DD/MM/YYYY";
	    glb_Local_DATE_SQL_PARAM = 103;
	}    
	else if ( DATE_FORMAT == "MM/DD/YYYY" )
	{
		glb_Local_DATE_FORMAT = "MM/DD/YYYY";
	    glb_Local_DATE_SQL_PARAM = 101;
	}    
}

/********************************************************************
Obtem dados no banco e carrega o combo de colaboradores
********************************************************************/
function fillCmbAno()
{
	// parametrizacao do dso dsoCmbCol
    setConnection(dsoCmbCol);
				   
	dsoCmbCol.SQL = 'SELECT a.Ano AS id, a.Ano ' +
                        'FROM Feriados_Datas a WITH(NOLOCK) ' +
                        'WHERE (a.Ano IS NOT NULL) ' +
                        'GROUP BY Ano ' +
                        'ORDER BY a.Ano DESC';

    dsoCmbCol.ondatasetcomplete = fillCmbAno_DSC;
    dsoCmbCol.Refresh();
}

/********************************************************************
Preenche o combo de colaboradores
********************************************************************/
function fillCmbAno_DSC()
{
	var optionStr;
	var optionValue;
	var oOption;

    var data = new Date();
    var ano = data.getFullYear();

	while ( !dsoCmbCol.recordset.EOF )
    {
        optionStr = dsoCmbCol.recordset['Ano'].value;
		optionValue = dsoCmbCol.recordset['id'].value;
        oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        oOption.selected = ((optionValue == ano) ? (true) : (false));
        selAno.add(oOption);
        
        dsoCmbCol.recordset.MoveNext();
    }
	
//	if ( selAno.options.length > 0 )
//		selAno.selectedIndex = 0;

    loadDataAndFillGrid(ano);
}

/********************************************************************
Carrega dados para preencher grid
********************************************************************/
function loadDataAndFillGrid(ano) {
    lockControls(true);

    //fg.Rows = 1;

    setConnection(dsoGrid);

    dsoGrid.SQL = 'SELECT a.FeriadoID, CONVERT(VARCHAR(4), ISNULL(b.Ano, ' + ano + ')) AS Ano, ' +
            '(dbo.fn_Data_Extenso(dbo.fn_Data_Converte(b.Dia, b.Mes, (ISNULL(b.Ano, ' + ano + ')), NULL, NULL, NULL), 2)) AS Mes, ' +
            '(dbo.fn_Pad(b.Dia, 2, \'0\', \'L\') + SPACE(1) + \'(\'+ ' +
                'LEFT(dbo.fn_Data_Extenso(dbo.fn_Data_Converte(b.Dia, b.Mes, ISNULL(b.Ano, ' + ano + '), NULL, NULL, NULL), 4), 3) + \')\') AS Dia, ' +
            '(a.Feriado + ISNULL((SPACE(1) + \'(\' + b.Observacao + \')\'), SPACE(0))) Feriado, ' +
            '(ISNULL((d.Localidade + SPACE(1)), SPACE(0)) + \'(\' + c.ItemFeminino + \')\') Localidade ' +
        'FROM Feriados a WITH(NOLOCK) ' +
            'INNER JOIN Feriados_Datas b WITH(NOLOCK) ON (b.FeriadoID = a.FeriadoID) ' +
            'INNER JOIN TiposAuxiliares_Itens c WITH(NOLOCK) ON (c.ItemID = a.TipoFeriadoID) ' +
            'LEFT OUTER JOIN Localidades d WITH(NOLOCK) ON (d.LocalidadeID = b.LocalidadeID) ' +
        'WHERE ((ISNULL(b.Ano, ' + ano + ') = ' + ano + ')) ' +
        'ORDER BY Ano, b.Mes, Dia, Feriado, Localidade';

    dsoGrid.ondatasetcomplete = fillGridData_DSC;
    dsoGrid.Refresh();
}

/********************************************************************
Preenche grid
********************************************************************/
function fillGridData_DSC() {
    
    fg.Editable = false;
    fg.Redraw = 0;

    var i;

    headerGrid(fg, ['Ano',
                    'M�s',
                    'Dia',
                    'Feriado',
                    'Localidade',
                    'FeriadoID'], [5]);

    fg.FontSize = '10';

    fillGridMask(fg, dsoGrid, ['Ano*',
                              'Mes*',
                              'Dia*',
                              'Feriado*',
                              'Localidade*',
                              'FeriadoID'],
                              ['', '', '', '', '', ''],
                              ['', '', '', '', '', '']);

    alignColsInGrid(fg, []);

    // Merge de Colunas
    fg.MergeCells = 4;
    fg.mergecol(0) = true;
    fg.mergecol(1) = true;
    fg.mergecol(2) = true;
    fg.mergecol(3) = true;

    // Define tamanho das coluna do grid
    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1);
    
    // Permite ordena��o pelo header do grid
    fg.ExplorerBar = 5;

    // Pinta Ter�a e Quinta de amarelo
    nColDia = getColIndexByColKey(fg, 'Dia*');
    nAmarelo = 0X8CE6F0;

    for (i = 1; i < fg.Rows; i++) 
    {
        var dia = fg.TextMatrix(i, nColDia);

        if ((dia.indexOf('Ter') > 0) || (dia.indexOf('Qui') > 0))
            fg.Cell(6, i, nColDia, i, nColDia) = nAmarelo;
    }
    
    fg.Redraw = 2;

    // destrava interface
    lockControls(false);

    fg.Redraw = 2;

// Eventos do grid
//fg_ExecEvents = true;

    // foca o grid se tem linhas
    if (fg.Rows > 1) {
        window.focus();
        fg.focus();
    }
}

/********************************************************************
Alterado valor no combo de Anos
********************************************************************/
function selAnoChange()
{
    var ano = selAno.value;
    loadDataAndFillGrid(ano);
}


/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado
    // O usuario clicou o botao Voltar
    if (ctl.id == btnVoltar.id) {
        lockControls(true);

        // zera a listagem
        fg.Rows = 1;

        // volta para logo
        sendJSMessage(getHtmlId(), JS_MSGRESERVED, 'PAGE_FERIADOS', 'HIDE');
        sendJSMessage(getHtmlId(), JS_MSGRESERVED, 'PAGE_COLABORADORES', 'CANC');

        lockControls(false);
    }
        
    // O usuario clicou o botao Colaboradores
    else if (ctl.id == btnColaboradores.id)
    {
        lockControls(true);
        sendJSMessage(getHtmlId(), JS_MSGRESERVED, 'PAGE_FERIADOS', 'HIDE');
    }

    // O usuario clicou o botao Delega��es
    else if (ctl.id == btnDelegacoes.id) 
    {
        lockControls(true);
        sendJSMessage(getHtmlId(), JS_MSGRESERVED, 'PAGE_FERIADOS', 'HIDE');
        sendJSMessage(getHtmlId(), JS_MSGRESERVED, 'PAGE_DELEGACOES', 'LOAD');
    }

    // O usuario clicou o botao Notifica��es
    else if (ctl.id == btnNotificacoes.id) 
    {
        lockControls(true);
        sendJSMessage(getHtmlId(), JS_MSGRESERVED, 'PAGE_FERIADOS', 'HIDE');
        sendJSMessage(getHtmlId(), JS_MSGRESERVED, 'PAGE_NOTIFICACOES', 'LOAD');
    }

    // O usuario clicou o botao Vendas
    else if (ctl.id == btnVendas.id) {
        lockControls(true);
        sendJSMessage(getHtmlId(), JS_MSGRESERVED, 'PAGE_FERIADOS', 'HIDE');
        sendJSMessage(getHtmlId(), JS_MSGRESERVED, 'PAGE_VENDAS', 'LOAD');
    }
}

/********************************************************************
Obtem direitos da pessoa no banco
********************************************************************/
function verificaDireitoNotificacao() {
    // parametrizacao do dso dsoCmbCol
    setConnection(dsoDireitoNotificacao);

    var empresaData = getCurrEmpresaData();

    var nEmpresaID = escape(empresaData[0]).toString();
    var nPessoaID = (window.top.glb_CURRUSERID).toString();
    var nOverflyID = '999';

    var strPars = new String();

    strPars = '?nEmpresaID=' + escape(nEmpresaID);
    strPars += '&nPessoaID=' + escape(nPessoaID);
    strPars += '&nOverflyID=' + escape(nOverflyID);

    dsoDireitoNotificacao.SQL = 'SELECT dbo.fn_Direitos(' + nEmpresaID + ', ' + nOverflyID + ', NULL, NULL, NULL, NULL, NULL, 3, ' + nPessoaID + ', 1, 1) AS Direito';
    dsoDireitoNotificacao.ondatasetcomplete = verificaDireitoNotificacao_DSC;
    dsoDireitoNotificacao.Refresh();
}

function verificaDireitoVendas() {
    var elem = window.document.getElementById('btnVendas');
    var nPessoaID = (window.top.glb_CURRUSERID).toString();
    var aPessoas = new Array(1336, 1679, 1002, 44065, 1142, 1004, 1069);

    for (var i = 0; i < aPessoas.length; i++) {
        if (aPessoas[i] == nPessoaID) {
            elem.style.visibility = 'visible';

            break;
        }
        else {
            elem.style.visibility = 'hidden';
        }
    }
}

/********************************************************************
Retorno da fun��o verificaDireitoNotificacao
********************************************************************/
function verificaDireitoNotificacao_DSC() {
    var elem = window.document.getElementById('btnNotificacoes');

    if (dsoDireitoNotificacao.recordset['Direito'].value)
        elem.style.visibility = 'visible';
    else
        elem.style.visibility = 'hidden';
}