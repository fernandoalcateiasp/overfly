<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<HTML id="fastbuttonsmainHtml" name="fastbuttonsmainHtml">

<HEAD>

<TITLE></TITLE>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/fastbuttons/fastbuttons.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/fastbuttons/fastbuttons.js" & Chr(34) & "></script>" & vbCrLf                
%>

<SCRIPT ID=wndJSProc LANGUAGE=javascript>
<!--

//-->
</SCRIPT>

</HEAD>

<BODY id="fastbuttonsmainBody" name="fastbuttonsmainBody" LANGUAGE=javascript onload="return window_onload()">
   
    <div id="divFastButtons" name="divFastButtons" class="divGeneral">
        <hr class="lnGeneral" id="hrBottonLineBlack" name="hrBottonLineBlack"></hr>
        <hr class="lnGeneral" id="hrBottonLineWhite" name="hrBottonLineWhite"></hr>
        <input type="button" id="btnNotificacoes" name="btnNotificacoes" value="" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="botao" title="Notificações">
    </div>
   
    

</BODY>

</HTML>
