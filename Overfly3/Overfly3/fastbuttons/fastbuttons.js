/********************************************************************
fastbuttons.js

Library javascript para o fastbuttons.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
// estado de travamento dos componentes da barra
var __ELEMSTATE = new Array();  //array: controlId, state
var __ELEMCOUNT = 0;

// numero maximo de botoes =
// num botoes de acesso rapido + botao todo da direita (Fale Conosco)
var __NUMMAXBTNS = 17;

// largura maxima de um botao
var __BTNWIDTH = 70;

// Controla estado de travamento do fast buttons bar
var __fBarIsLocked = false;

/******************************************************
Controlador do status da janela de notifica��es
    - 0 Descarregada (Carrega janela)
    - 1 Carregando (N�o faz nada)
    - 2 Carregada (Foco na Janela)
******************************************************/
var glb_notification_status = 0;

var glb_TimerReloadNotificacoes = null;
var glb_TimerReloadCheckLidas = null;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
LISTA DAS FUNCOES

window_onload()
wndJSProc(idElement, msg, param1, param2)
window_onload_1stPart()
createFastButtons()
allFastBtns_click()
initAllFastBtns()
lockFastButtonsControls(cmdLock)
adjustElement(elem, cmdLock)
adjustActiveXControl(elem, cmdLock)
setupButton(btnNumber, arrayInterface, formName)

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload() {
    var coll, i;

    window_onload_1stPart();

    if (window.document.getElementById('fastbuttonsmainBody') != null) {
        // Translada interface pelo dicionario do sistema
        translateInterface(window, null);

        // ajusta o body do html
        with (fastbuttonsmainBody) {
            style.backgroundColor = 'silver';
            scroll = 'no';
            style.visibility = 'visible';
        }
        // este arquivo carregou
        sendJSMessage('fastbuttonsmainHtml', JS_PAGELOADED, new Array(window.top.name.toString(), 'USERMAINBROWSER'), 0X4);
    }
}

/********************************************************************
Loop de mensagens
********************************************************************/
function wndJSProc(idElement, msg, param1, param2) {
    switch (msg)
    {
        // Msg do carrier, atualiza numeros de notifica��es no bot�o.
        case JS_NOTIFICATIONQTY:
        {
            confBotaoNotificacao(param1);
            return null;
        }
        break;

    case JS_NOTIFICATIONCALL:
        {
            /*
            param2 diferente de null, significa que a janela foi fechada por ela mesma
            via c�digo.
            param2 == 1 a janela vai abrir novamente ap�s um pequeno intervalo de tempo
            */
            if (param2 != null) 
            {
                if (param2 == 1) 
                {
                    startTimerReloadNotificacoes();
                }
                else if (param2 == 2) 
                {
                    startTimerReloadCheckLidas();
                }
                return 1;
            }

            /* param1 � sempre nulo quando o botao de chamada da janela for clicado,
            param1 = 2 quando a janela de notifica��es terminou de carregar.
            param1 = 0 quando a janela fechada */
            if ((param1 == null) && (getNotificationStatus() == 0)) {
                loadNotificacoes();
                return 1;
            }
            else if ((param1 == null) && (getNotificationStatus() != 0)) {
                loadNotificacoes();
                return 1;
            }
            else {
                setNotificationStatus(param1);
            }
        }
        break;
        
        default:
            return null;
    }
}

/********************************************************************
Complementa a funcao window_onload

Parametros:
Nenhum

Retorno:
Nenhum
********************************************************************/
function window_onload_1stPart() {
    // Cria os botoes rapidos
    createFastButtons();

    // linha inferior black de relevo do html hrBottonLineBlack
    hrBottonLineBlack.disabled = true;
    with (hrBottonLineBlack.style) {
        color = 'gray';
        left = 0;
        top = 25;
        height = 1;
        width = MAX_FRAMEWIDTH;
    }

    // linha inferior white de relevo do html hrBottonLineWhite
    hrBottonLineWhite.disabled = true;
    with (hrBottonLineWhite.style) {
        color = 'white';
        left = 0;
        top = 27;
        height = 1;
        width = MAX_FRAMEWIDTH;
    }

    with (btnNotificacoes.style) {
        top = 3;
        height = 20;
        width = 25;
        
    }

    lockFastButtonsControls(true);
    lockFastButtonsControls(false);

    window.top.barLoaded();
}

/********************************************************************
Cria e configura os botoes de acesso rapido e suas funcoes
********************************************************************/
function createFastButtons() {
    var elem;

    // ajusta o div divFastButtons
    with (divFastButtons.style) {
        backgroundColor = 'transparent';
        border = 'none';
        width = MAX_FRAMEWIDTH - 2;
        left = 2;
        top = 0;
    }

    // cria ate __NUMMAXBTNS + 1 botoes
    var i, btnGap;

    for (i = 0; i < __NUMMAXBTNS; i++) {
        btnGap = -6.5/*((i == 0) ? 0 : 1)*/;

        elem = document.createElement('INPUT');
        elem.type = 'button';
        with (elem.style) {
            position = 'absolute';
            fontFamily = 'Tahoma';
            fontSize = '8pt';
            height = 24;
            //width = __BTNWIDTH;
            width = __BTNWIDTH - 7;
            top = 1;
            left = 1 + (btnGap * i) + (i * parseInt(width, 10)) - 2;
            elem.value = '';
            visibility = 'visible';
        }
        elem.onclick = allFastBtns_click;
        elem.id = i.toString();
        elem.name = i.toString();
        elem.disabled = false;

        // acrescenta o botao
        divFastButtons.appendChild(elem);
    }

    btnNotificacoes.style.left = 968;
}

/********************************************************************
Invoca a funcao associada ao botao clicado
********************************************************************/
function allFastBtns_click() {
    var btnClicked = window.document.activeElement.id;

    /******************************   
    // se for o botao e-mail
    if ( (btnClicked == (__NUMMAXBTNS - 1)) && ((window.document.getElementById(btnClicked)).getAttribute('formName', 1) == null) )
    {
    if (window.document.getElementById('fastbuttonsmainBody') != null)
    {
    sendJSMessage('fastbuttonsmainHtml', JS_MSGRESERVED, 'BTN_FAST_EMAIL', 'MAIN');
    }
    }    
    else
    ******************************/
    {
        // dispara evento window top (browser mae ou filho conforme o caso)
        window.top.emulEvent(parseInt(btnClicked, 10), (window.document.getElementById(btnClicked)).getAttribute('formName', 1));
    }
}

/********************************************************************
Inicializa todos os botoes da barra de botoes de atalho

Parametros:
Nenhum

Retorno:
o numero de botoes inicializados

Se a funcao e chamada em um arquivo contido no browser mae, atua sobre
botao da barra do browser mae, se chamada em um browser filho, atua
sobre botao da barra do browser filho.
********************************************************************/
function initAllFastBtns() {
    var i, j, coll;

    j = 0;
    coll = window.document.getElementsByTagName('INPUT');

    for (i = 0; i < coll.length; i++) {
        if (((coll.item(i)).type).toUpperCase() == 'BUTTON') {
            setupButton((i + 1), new Array(false, false, null, null), null);
            j++;
        }
    }

    return j;
}

/********************************************************************
Esta funcao habilita/desabilita todos os controles do html/asp corrente.

Parametros:
cmdLock         - true (desabilita) / false (habilita)

Retornos:
Nenhum
********************************************************************/
function lockFastButtonsControls(cmdLock) {
    __ELEMCOUNT = 0;
    var i;
    var elem;

    for (i = 0; i < window.document.all.length; i++) {
        elem = window.document.all.item(i);

        // controles ActiveX
        // o grid
        if (elem.tagName.toUpperCase() == 'OBJECT') {
            if (elem.classid.toUpperCase() == 'CLSID:D76D712E-4A96-11D3-BD95-D296DC2DD072') {
                // grid
                adjustActiveXControl(elem, cmdLock);
            }
        }

        // demais elementos        
        if (elem.tagName.toUpperCase() == 'INPUT')
            adjustElement(elem, cmdLock);
        else if (elem.tagName.toUpperCase() == 'SELECT')
            adjustElement(elem, cmdLock);
        else if (elem.tagName.toUpperCase() == 'TEXTAREA')
            adjustElement(elem, cmdLock);


    }

    __fBarIsLocked = cmdLock;

}

// FUNCOES INTERNAS DA LIBRARY, NAO PODEM SER USADAS DIRETAMENTE ****

/********************************************************************
Esta funcao trava ou destrava um elemento de html/asp da interface
do browser filho corrente.

Parametros:
elem            - elemento corrente
cdmLock         - true (trava) / false (destrava)

Retorno:
nenhum
********************************************************************/
function adjustElement(elem, cmdLock) {
    // cmdLock == true => desabilitar elemento

    if (cmdLock == true) {
        // salva status corrente
        __ELEMSTATE[__ELEMCOUNT] = new Array(elem.id, elem.disabled);
        // desabilita o elemento
        elem.disabled = true;
        // incrementa contador de elementos
        __ELEMCOUNT++;
    }
    else    // restaura o status do elemento
    {
        // restaura status do elemento
        window.document.getElementById((__ELEMSTATE[__ELEMCOUNT])[0]).disabled = (__ELEMSTATE[__ELEMCOUNT])[1];
        // incrementa contador de elementos
        __ELEMCOUNT++;
    }
}

/********************************************************************
Esta funcao trava ou destrava um elemento ACTIVEX CONTROL
de html/asp da interface do browser filho corrente.

Parametros:
elem            - elemento ACTIVEX CONTROL corrente
cdmLock         - true (trava) / false (destrava)

Retorno:
nenhum
********************************************************************/
function adjustActiveXControl(elem, cmdLock) {
    // cmdLock == true => desabilitar elemento

    if (cmdLock == true) {
        // salva status corrente
        __ELEMSTATE[__ELEMCOUNT] = new Array(elem.id, elem.Enabled);
        // desabilita o elemento
        elem.Enabled = false;
        // incrementa contador de elementos
        __ELEMCOUNT++;
    }
    else    // restaura o status do elemento
    {
        // restaura status do elemento
        window.document.getElementById((__ELEMSTATE[__ELEMCOUNT])[0]).Enabled = (__ELEMSTATE[__ELEMCOUNT])[1];
        // incrementa contador de elementos
        __ELEMCOUNT++;
    }
}

/********************************************************************
Esta funcao configura um botao da barra.

Parametros:
Parametros:
btnNumber           - botao a atuar: de 1 a 9
arrayInterface      - array: cmdShow (true/false),
cmdLocked (true/false),
caption,
title
formName            - nome do form a carregar

Retorno:
true se configurou, caso contrario false
********************************************************************/
function setupButton(btnNumber, arrayInterface, formName) {
    // os arrays tem as dimensoes apropriadas?
    if (arrayInterface.length != 4)
        return false;

    // os botoes aqui vao de 0 a (__NUMMAXBTNS - 1)

    btnNumber--;

    var elem = window.document.getElementById(btnNumber);

    // o botao nao existe
    if (elem == null)
        return false;

    // tudo OK, configura    

    // mostra botao (true/false)
    if (arrayInterface[0] == true)
        elem.style.visibility = 'visible';
    else if (arrayInterface[0] == false)
        elem.style.visibility = 'hidden';

    // habilita/desabilita o botao (true/false)
    if (arrayInterface[1] == true)
        elem.disabled = false;
    else if (arrayInterface[1] == false)
        elem.disabled = true;

    // caption do botao    
    if (arrayInterface[2] != null)
        elem.value = arrayInterface[2].toString();

    // title (hint) do botao    
    if (arrayInterface[3] != null)
        elem.title = arrayInterface[3].toString();

    // nome do form    
    if (formName != null) {
        elem.setAttribute('formName', formName.toString().toUpperCase(), 1);
    }

    return true;
}

/********************************************************************
Clique botao de Notificacoes
********************************************************************/
function btn_onclick(ctl) {
    window.focus();
    if (ctl.id == btnNotificacoes.id)
        btnNotificacoes.focus();

    if (ctl.id == btnNotificacoes.id) {
        // 1. O usuario clicou o botao OK
        btnNotificacoes_Clicked();
    }
}

function btnNotificacoes_Clicked()
{
    stopTimerReloadNotificacoes();

    // 1. O usuario clicou o botao Notificacao
    sendJSMessage('fastbuttonsmainHtml', JS_NOTIFICATIONCALL, null, null);
}

/********************************************************************
Inicia o carregamento das notificacoes
********************************************************************/
function loadNotificacoes(param1) 
{
    stopTimerReloadCheckLidas();
    /******************************************************
    Controlador do status da janela de notifica��es
    - 0 Descarregada (Carrega janela)
    - 1 Carregando (N�o faz nada)
    - 2 Carregada (Foco na Janela)
    - 3 Descarregando (N�o faz nada)
    ******************************************************/
       
    if (getNotificationStatus() == 0) 
    {
        setNotificationStatus(1);

        // No If/Else If abaixo, a janela � carregada do servidor de p�ginas
        if (param1 == null) {
            sendJSMessage(getHtmlId(), JS_NOMFORMOPEN, 4, null);
        }
        else if (param1 == 1) 
        {
            sendJSMessage(getHtmlId(), JS_NOMFORMOPEN, 5, null);
        }
        return null;
    }

    if ((getNotificationStatus() == 1) || (getNotificationStatus() == 3))
    {
        return null;
    }
    
    //No If abaixo, a janela j� esta carregada e � dado foco na mesma
    if (getNotificationStatus() == 2) {
        sendJSMessage(getHtmlId(), JS_WIDEMSG, JS_FOCUSFORM, null);
        return null;
    }
}

function setNotificationStatus(nStatus) 
{
    glb_notification_status = nStatus;
}

function getNotificationStatus() 
{
    return glb_notification_status;
}

/*******************************************************************
Fim carregamento das notificacoes
*******************************************************************/

// FINAL DE FUNCOES INTERNAS DA LIBRARY *****************************

function confBotaoNotificacao(quantidadeNotificacoes) {

    with (btnNotificacoes.style) {
        left = 970;
        top = 3;
        height = 20;
        width = 25;
        if (quantidadeNotificacoes > 0) {
            color = 'white';
            background = 'red';
            btnNotificacoes.value = quantidadeNotificacoes;
        }
        else {
            background = 'transparent';
            color = 'white';
            background = 'gray';
            btnNotificacoes.value = 0;
        }
    }

}

function startTimerReloadNotificacoes()
{
    glb_TimerReloadNotificacoes = window.setInterval('btnNotificacoes_Clicked()', (500), 'JavaScript');
}

function startTimerReloadCheckLidas() 
{
    glb_TimerReloadCheckLidas = window.setInterval('loadNotificacoes(1)', (500), 'JavaScript');
}


function stopTimerReloadNotificacoes()
{
    if (glb_TimerReloadNotificacoes != null)
    {
        window.clearInterval(glb_TimerReloadNotificacoes);
        glb_TimerReloadNotificacoes = null;
    }
}

function stopTimerReloadCheckLidas() 
{
    if (glb_TimerReloadCheckLidas != null) {
        window.clearInterval(glb_TimerReloadCheckLidas);
        glb_TimerReloadCheckLidas = null;
    }
}
