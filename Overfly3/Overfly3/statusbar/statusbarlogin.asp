<%@ LANGUAGE=VBSCRIPT %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="statusbarloginHtml" name="statusbarloginHtml">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/statusbar/statusbar.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf                
      
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/statusbar/statusbar.js" & Chr(34) & "></script>" & vbCrLf
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--
/********************************************************************
Loop de mensagens
********************************************************************/
function wndJSProc(idElement, msg, param1, param2)
{
    switch (msg)
    {
        case JS_STBARWRITE :
        {
            if ( param1[0].toUpperCase() == 'LOGIN' )
                writeInCell(param1);
        }
        break;

        default:
            return null;
    }
}

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    // Cores do status bar
    statusBar_Colors(true);
    
    // parametros de dimensoes dos elementos
    var divCount = 0;
    var divWidth = (MAX_FRAMEWIDTH / 5) ;
    var divGap = 1;
       
    // ajusta o div PrgNome
    with (divPrgNome.style)
    {
        height = 24;
        width = divWidth;
        left = 0;
    }
    
    // ajusta o paragrafo paraPrgNome
    with (paraPrgNome.style)
    {
        fontSize = '10pt'; 
        fontWeight = 'bold';
        left = 4;
        top = 2;
        width = 145;
    }
    
    // ajusta o div divEmpresa
    divCount++;
    with (divEmpresa.style)
    {
        width = divWidth - 4;
        left = (divCount * divWidth) + divGap;
    }

    // ajusta o paragrafo paraEmpresa
    with (paraEmpresa.style)
    {
        fontSize = '10pt'; 
        fontWeight = 'normal';
        left = 4;
        top = 2;
        width = divEmpresa.offsetWidth - 4;
    }
    
    // ajusta o div divUsuario
    divCount++;
    with (divUsuario.style)
    {
        width = divWidth + 3;    
        left = divEmpresa.offsetLeft + divEmpresa.offsetWidth + divGap;
    }
    
    // ajusta o paragrafo paraUsuario
    with (paraUsuario.style)
    {
        fontSize = '10pt'; 
        fontWeight = 'normal';
        left = 4;
        top = 2;
        width = divUsuario.offsetWidth - 2;
    }
        
    // ajusta o div divForm
    divCount++;
    with (divForm.style)
    {
        width = divWidth;
        left = (divCount * divWidth) + divGap;
    }
    
    // ajusta o paragrafo paraForm
    with (paraForm.style)
    {
        fontSize = '10pt'; 
        fontWeight = 'normal';
        left = 4;
        top = 2;
        width = 145;
    }
    
    // ajusta o div divModo
    divCount++
    with (divModo.style)
    {
        width = divWidth;
        left = (divCount * divWidth) + divGap + 1;
    }
    
    // ajusta o paragrafo paraModo
    with (paraModo.style)
    {
        fontSize = '10pt'; 
        fontWeight = 'normal';
        left = 4;
        top = 2;
        width = 150;
    }

    // ajusta o traco inferior hr_Inf
    with (hr_Inf.style)
    {
        backgroundColor = 'darkgray';
        left = 0;
        top = parseInt(divPrgNome.currentStyle.top, 10) + parseInt(divPrgNome.currentStyle.height, 10) - 2;
        width = parseInt(divModo.currentStyle.left, 10) + parseInt(divModo.currentStyle.width, 10);
        height = 2;
    }
    
    // ajusta o traco a direita hr_Right
    with (hr_Right.style)
    {
        backgroundColor = 'darkgray';
        left = parseInt(divModo.currentStyle.left, 10) + parseInt(divModo.currentStyle.width, 10) - 2;
        top = 0;
        width = 2;
        height = parseInt(divModo.currentStyle.height, 10);
    }
    
    setDivTitlesPos();
    
    // ajusta o body do html
    with (statusbarloginBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }    
    
    // ajusta dimensoes e mostra o status bar no arquivo userlogin.asp
    // (ou sua biblioteca js) que esta controlando o carregamento
    // da interface da qual o status bar faz parte.
    
    // este arquivo asp carregou
    sendJSMessage('statusbarloginHtml', JS_PAGELOADED, new Array(window.top.name.toString(), 'USERLOGIN'), 0X2);
}    

var __glbDivTitlesPos = [0, 0, 0, 0, 0, 0]; 

/********************************************************************
Muda posicao dos divs do status bar
********************************************************************/
function setDivTitlesPos()
{
	__glbDivTitlesPos[1] = divPrgNome.offsetLeft;
	__glbDivTitlesPos[2] = divEmpresa.offsetLeft;
	__glbDivTitlesPos[3] = divUsuario.offsetLeft;
	__glbDivTitlesPos[4] = divForm.offsetLeft;
	__glbDivTitlesPos[5] = divModo.offsetLeft;

	divPrgNome.style.left = __glbDivTitlesPos[3];
	divPrgNome.style.width = divPrgNome.offsetWidth + 7;
	
	divEmpresa.style.left = __glbDivTitlesPos[2];
		
	divUsuario.style.left = __glbDivTitlesPos[4];
	divUsuario.style.width = divUsuario.offsetWidth - 5;

	divForm.style.left = __glbDivTitlesPos[1];
	divForm.style.width = divForm.offsetWidth - 2;
	
	divModo.style.left = __glbDivTitlesPos[5];
}

//-->
</script>

</head>

<body id="statusbarloginBody" name="statusbarloginBody" LANGUAGE="javascript" onload="return window_onload()">
    <div id="divPrgNome" name="divPrgName" class="divGeneral">
        <p id="paraPrgNome" name="paraPrgNome" class="paraNormal"></p>
    </div>    
    
    <div id="divUsuario" name="divUsuario" class="divGeneral">
        <p id="paraUsuario" name="paraUsuario" class="paraNormal"></p>
    </div>
    <div id="divForm" name="divForm" class="divGeneral">
        <p id="paraForm" name="paraForm" class="paraNormal"></p>
    </div>
        </div>
    <div id="divModo" name="divModo" class="divGeneral">
        <p id="paraModo" name="paraModo" class="paraLogin"></p>
    </div>
    <div id="divEmpresa" name="divEmpresa" class="divGeneral">
        <p id="paraEmpresa" name="paraEmpresa" class="paraNormal"></p>
    </div>
    <img id="hr_Inf" name="hr_Inf" class="lblGeneral"></img>
    <img id="hr_Right" name="hr_Right" class="lblGeneral"></img>

</body>

</html>
