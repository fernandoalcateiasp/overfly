var __dataEmpresasSelected = new Array();

/********************************************************************
statusbar.js

Library javascript para o statusbar.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

// IMPLEMENTACAO DAS FUNCOES

********************************************************************/

/********************************************************************
O backgroundColor dos divs do StatusBar e o color dos p
array __dataEmpresas[x]
    x[0]    empresaID
    x[3]    default
    x[8]    color
    x[9]    backgroundColor

Parametros:
logIn           null ou true se for o statusbar do loginIn
empresaID       null ou o ID da empresa selecionada

Retorno:
nenhum
********************************************************************/
function statusBar_Colors(logIn, empresaID)
{
    var i, j, k, coll;

    if ( logIn != null )    
    {
        coll = window.document.getElementsByTagName('DIV');
    
        for (j=0; j<coll.length; j++)
            coll.item(j).style.backgroundColor = '#FFFFFF';
                        
        coll = window.document.getElementsByTagName('P');
    
        for (j = 0; j < coll.length; j++)
        {
            if (coll.item(j).id == "paraModo")
                coll.item(j).style.color = 'red';
            else
                coll.item(j).style.color = '#000000';
        }
    }
    else
    {
        // a empresa default
        if ( empresaID == null )
        {
            for (i=0; i< __dataEmpresas.length; i++ )
            {
                if ( __dataEmpresas[i][3] )
                {
					if (OVER_TESTE)
					{
						selEmpresa.style.color = '#' + __dataEmpresas[i][9];
						selEmpresa.style.background = '#' + __dataEmpresas[i][8];
						try
						{
							selForm.style.color = '#' + __dataEmpresas[i][9];
							selForm.style.background = '#' + __dataEmpresas[i][8];
						}
						catch(e)
						{
							;
						}
					}
					else
					{
						selEmpresa.style.color = '#' + __dataEmpresas[i][8];
						selEmpresa.style.background = '#' + __dataEmpresas[i][9];
						try
						{
							selForm.style.color = '#' + __dataEmpresas[i][8];
							selForm.style.background = '#' + __dataEmpresas[i][9];
						}
						catch(e)
						{
							;
						}
					}
                
                    coll = window.document.getElementsByTagName('DIV');
    
                    for (j=0; j<coll.length; j++)
                    {
                        if ( OVER_TESTE )
                            coll.item(j).style.backgroundColor = '#' + __dataEmpresas[i][8];
                        else
                            coll.item(j).style.backgroundColor = '#' + __dataEmpresas[i][9];
                    }    
                        
                    coll = window.document.getElementsByTagName('P');
    
                    for (j=0; j<coll.length; j++)
                    {
                        if ( OVER_TESTE )
                            coll.item(j).style.color = '#' + __dataEmpresas[i][9];    
                        else    
                            coll.item(j).style.color = '#' + __dataEmpresas[i][8];    
                    }        
                }
            }
        }
        else
        {
            for (i=0; i< __dataEmpresas.length; i++ )
            {
                if ( __dataEmpresas[i][0] == empresaID )
                {
                
					if (OVER_TESTE)
					{
						selEmpresa.style.color = '#' + __dataEmpresas[i][9];
						selEmpresa.style.background = '#' + __dataEmpresas[i][8];
						try
						{
							selForm.style.color = '#' + __dataEmpresas[i][9];
							selForm.style.background = '#' + __dataEmpresas[i][8];
						}
						catch(e)
						{
							;
						}
					}
					else
					{
						selEmpresa.style.color = '#' + __dataEmpresas[i][8];
						selEmpresa.style.background = '#' + __dataEmpresas[i][9];
						try
						{
							selForm.style.color = '#' + __dataEmpresas[i][8];
							selForm.style.background = '#' + __dataEmpresas[i][9];
						}
						catch(e)
						{
							;
						}
					}
                
                    coll = window.document.getElementsByTagName('DIV');
    
                    for (j=0; j<coll.length; j++)
                    {
                        if ( OVER_TESTE )
                            coll.item(j).style.backgroundColor = '#' + __dataEmpresas[i][8];
                        else
                            coll.item(j).style.backgroundColor = '#' + __dataEmpresas[i][9];
                    }    
                        
                    coll = window.document.getElementsByTagName('P');
    
                    for (j=0; j<coll.length; j++)
                    {
                        if ( OVER_TESTE )
                            coll.item(j).style.color = '#' + __dataEmpresas[i][9];    
                        else    
                            coll.item(j).style.color = '#' + __dataEmpresas[i][8];    
                    }        
                }
            }
        }
    }    
}

/********************************************************************
Esta funcao escreve em um paragraph do statusbar

Parametros:
param1              array (cellName, string, ellipsis)

Retorno:
nenhum
********************************************************************/
function writeInCell(param1)
{
    theCell = param1[1].toUpperCase();
    
    strCell = param1[2];
    
    if ( param1[3] != null && param1[3] != false )
        strCell += '...';
    
    var pID = null;
    
    if ( theCell == 'CELLPRGNAME' )
        pID = 'paraPrgNome';
    else if ( theCell == 'CELLEMPRESA' )        
        pID = 'paraEmpresa';    
    else if ( theCell == 'CELLUSERNAME' )        
        pID = 'paraUsuario';
    else if ( theCell == 'CELLFORM' )        
        pID = 'paraForm';
    else if ( theCell == 'CELLMODE' )        
        pID = 'paraModo'; 
    
    if (pID != null)
    {
        var elem = window.document.getElementById(pID);

        if ( elem )
            elem.innerText = strCell;    
    }    
}

/********************************************************************
Esta funcao recupera o dado de uma celula do status bar

Parametros:
thisWin             o window do statusBar a ler
param1              array (statusBarToGet, cellName)

Retorno:
O conteudo da celula. Se for combo retorna um array com o conteudo
e o value
********************************************************************/
function getDataInCell(thisWin, param1)
{
    var retVal;
    var i;
    
    theCell = param1[1].toUpperCase();
       
    var pID = null;
    
    if ( theCell == 'CELLPRGNAME' )
        pID = 'paraPrgNome';
    else if ( theCell == 'CELLEMPRESA' )        
        pID = 'paraEmpresa';    
    else if ( theCell == 'CELLUSERNAME' )        
        pID = 'paraUsuario';
    else if ( theCell == 'CELLFORM' )        
        pID = 'paraForm';
    else if ( theCell == 'CELLMODE' )        
        pID = 'paraModo'; 
    
    // tem combo de empresa, acerta o pID
    if ( pID == 'paraEmpresa' )
    {
        var collSel = thisWin.document.getElementsByTagName('SELECT');
    
        for (i=0; i<collSel.length; i++)    
        {
            if ( collSel.item(i).id.toUpperCase() == 'SELEMPRESA' )
                pID = 'selEmpresa';
        }        
    }
        
    // recupera os dados
    if (pID != null)
    {
        var elem = thisWin.document.getElementById(pID);

        if (elem == null)
            return null;
        
        // campo e <p>
        if ( elem.tagName.toUpperCase() == 'P' )
        {
            retVal = elem.innerText;
        }
        // campo e <select>
        if ( elem.tagName.toUpperCase() == 'SELECT' )
        {
            retVal = new Array();
            retVal[0] = thisWin.document.getElementById("selEmpresa").selectedIndex;    
            retVal[1] = thisWin.document.getElementById("selEmpresa").value;
            retVal[2] = thisWin.document.getElementById("selEmpresa").options.item(thisWin.document.getElementById("selEmpresa").selectedIndex).text;
        }
    }

    return retVal;    
}

/********************************************************************
Grava no array os dados da empresa corrente

Parametros: nenhum

Retorno: nenhum

array   
- array[0] - empresaID
- array[1] - empresaPaisID
- array[2] - empresaCidadeID
- array[3] - nome de fantasia da empresa
- array[4] - empresaUFID
- array[5] - TipoEmpresaID
- array[6] - Nome - nome completo da empresa
- array[7] - ID do idioma do sistema.
- array[8] - ID do idioma da empresa.
- array[9] - MoedaID.
- array[10] - Simbolo Moeda.
- array[11] - TemProducao.
- array[12] - URL.
null    - nao tem empresa selecionada        
********************************************************************/
function empresaData_Change() 
{
    var i;
    var optValue = selEmpresa.value;
    var optText = selEmpresa.options.item(selEmpresa.selectedIndex).text;

    if (optValue == null)
        return null;

    for (i = 0; i < __dataEmpresas.length; i++) 
    {
        if (__dataEmpresas[i][0] == parseInt(optValue, 10)) 
        {
            __dataEmpresasSelected = (new Array(__dataEmpresas[i][0],
                                               __dataEmpresas[i][1],
                                               __dataEmpresas[i][2],
                                               optText,
                                               __dataEmpresas[i][5],
                                               __dataEmpresas[i][6],
                                               __dataEmpresas[i][7],
                                               __dataEmpresas[i][10],
                                               __dataEmpresas[i][11],
                                               __dataEmpresas[i][12],
                                               __dataEmpresas[i][13],
                                               __dataEmpresas[i][14],
                                               __dataEmpresas[i][15]));
        }
    }
}

/********************************************************************
Retorna dados da empresa corrente
Esta funcao retorna um array com os dados da empresa corrente

Parametros:
nenhum

Retorno:
array   - array[0] - empresaID
        - array[1] - empresaPaisID
        - array[2] - empresaCidadeID
        - array[3] - nome de fantasia da empresa
        - array[4] - empresaUFID
        - array[5] - TipoEmpresaID
        - array[6] - Nome - nome completo da empresa
        - array[7] - ID do idioma do sistema.
        - array[8] - ID do idioma da empresa.        
        - array[9] - MoedaID.
        - array[10] - Simbolo Moeda.
        - array[11] - TemProducao.
        - array[12] - URL.
null    - nao tem empresa selecionada        
********************************************************************/
function empresaData()
{
    if (__dataEmpresasSelected == null)
        empresaData_Change();

    return __dataEmpresasSelected;    
}

/********************************************************************
Retorna o estado abreviado dado um estadoID (recursoID)

Parametros:
estadoID    - o ID do estado

Retorno:
o estado abreviado ou '' se nao encontra
********************************************************************/
function estadoAbreviado(estadoID)
{
    var retVal = new Array();
    var i;
    
    for ( i=0; i< __dataEstados.length; i++)
    {
        if (__dataEstados[i][0] == estadoID)
        {
            retVal[0]= __dataEstados[i][1];
            retVal[1]= __dataEstados[i][2];
            break;
        }
    }
    
    return retVal;
}