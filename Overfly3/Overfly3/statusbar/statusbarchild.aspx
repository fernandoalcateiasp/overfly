﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="statusbarchild.aspx.cs" Inherits="Overfly3.statusbar.statusbarchild" %>

<%         
    string pagesURLRoot;
    string strConn;
    string ApplicationName;
    Object oOverflySvrCfg = null;

    OVERFLYSVRCFGLib.OverflyMTS objSvrCfg = new OVERFLYSVRCFGLib.OverflyMTS();

    WSData.dataInterface oDataInterface = new WSData.dataInterface(
           System.Configuration.ConfigurationManager.AppSettings["application"]
       );
    pagesURLRoot = objSvrCfg.PagesURLRoot(
           System.Configuration.ConfigurationManager.AppSettings["application"]);

    ApplicationName = oDataInterface.ApplicationName;

%>
<html id="statusbarchildHtml" name="statusbarchildHtml">

<head>

    <title></title>

    <%
        //Links de estilo, bibliotecas da automacao e especificas
        Response.Write("<LINK REL=" + (char)34 + "stylesheet" + (char)34 + " HREF=" + (char)34 + pagesURLRoot + "/statusbar/statusbar.css" + (char)34 + "type=" + (char)34 + "text/css" + (char)34 + ">"); Response.Write("\r\n");

        Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/js_sysbase.js" + (char)34 + "></script>"); Response.Write("\r\n");
        Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/js_constants.js" + (char)34 + "></script>"); Response.Write("\r\n");
        Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/js_htmlbase.js" + (char)34 + "></script>"); Response.Write("\r\n");
        Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/js_strings.js" + (char)34 + "></script>"); Response.Write("\r\n");
        Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/js_interface.js" + (char)34 + "></script>"); Response.Write("\r\n");
        Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/js_statusbar.js" + (char)34 + "></script>"); Response.Write("\r\n");
        Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/js_fastbuttonsbar.js" + (char)34 + "></script>"); Response.Write("\r\n");
        Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/js_controlsbar.js" + (char)34 + "></script>"); Response.Write("\r\n");
        Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/js_modalwin.js" + (char)34 + "></script>"); Response.Write("\r\n");
        Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/js_rdsfacil.js" + (char)34 + "></script>"); Response.Write("\r\n");

        Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/statusbar/statusbar.js" + (char)34 + "></script>"); Response.Write("\r\n");
    %>

    <%
        //Os forms que o usuario tem direito


        ComboFormData = DataInterfaceObj.getRemoteData("SELECT DISTINCT b.EmpresaID, DF.RecursoFantasia AS FormTitle,DF.FormName " +
                        "FROM RelacoesPesRec a WITH(NOLOCK) " +
                                "INNER JOIN RelacoesPesRec_Perfis b WITH(NOLOCK) ON a.RelacaoID=b.RelacaoID " +
                                "INNER JOIN dbo.vw_Direitos_Overfly DO WITH(NOEXPAND) ON b.PerfilID = DO.PerfilID " +
                                "INNER JOIN dbo.vw_Direitos_Modulo DM WITH(NOEXPAND) ON b.PerfilID = DM.PerfilID AND DO.RecursoID = DM.RecursoMaeID " +
                                "INNER JOIN dbo.vw_Direitos_SubModulo DSM WITH(NOEXPAND) ON b.PerfilID = DSM.PerfilID AND DM.RecursoID = DSM.RecursoMaeID " +
                                "INNER JOIN dbo.vw_Direitos_Form DF WITH(NOEXPAND) ON b.PerfilID = DF.PerfilID AND DSM.RecursoID = DF.RecursoMaeID " +
                        "WHERE a.SujeitoID IN ((SELECT  " + Session["userID"].ToString() + " " +
                                                        "UNION ALL " +
                                                    "SELECT UsuarioDeID " +
                                                        "FROM DelegacaoDireitos WITH(NOLOCK) " +
                                                    "WHERE (UsuarioParaID = " + Session["userID"].ToString() + " AND GETDATE() BETWEEN dtInicio AND dtFim))) " +
                                "AND a.ObjetoID=999 " +
                                "AND a.TipoRelacaoID=11 " +
                                "AND (DO.Consultar1=1 OR DO.Consultar2=1) " +
                                "AND (DM.Consultar1=1 OR DM.Consultar2=1) " +
                                "AND (DSM.Consultar1=1 OR DSM.Consultar2=1) " +
                                "AND (DF.Consultar1=1 OR DF.Consultar2=1) " +
                        "ORDER BY b.EmpresaID, FormTitle");

        Response.Write("<script ID=" + (char)34 + "ComboFormsData" + (char)34 + " LANGUAGE=" + (char)34 + "javascript" + (char)34 + ">");
        Response.Write("\r\n");

        Response.Write("var __cmbFormsData = new Array();");
        Response.Write("\r\n");

        //Guarda dados em array para consulta do lado cliente
        //EmpresaID, FormTitle, FormName
        //CUIDADO - campos usados no loop abaixo, definidos pelos seus indices
        //se mudar a ordem dos campos do select acima, revisar o loop


        for (int i = 0; ComboFormData != null && i < ComboFormData.Tables[1].Rows.Count; i++)
        {
            Response.Write("__cmbFormsData[" + i + "] = ");
            Response.Write("new Array(");
            //EmpresaID
            if (ComboFormData.Tables[1].Rows[i][0] == null)
            {
                Response.Write("0" + ",");
            }
            else
            {
                Response.Write(ComboFormData.Tables[1].Rows[i][0].ToString() + ", ");
            }

            //FormTitle
            if (ComboFormData.Tables[1].Rows[i][1] == null)
            {
                Response.Write(" " + ",");
            }
            else
            {
                Response.Write("'" + ComboFormData.Tables[1].Rows[i][1].ToString() + "'" + ", ");
            }

            //FormName
            if (ComboFormData.Tables[1].Rows[i][2] == null)
            {
                Response.Write(" ");
            }
            else
            {
                Response.Write("'" + ComboFormData.Tables[1].Rows[i][2].ToString() + "'");
            }

            Response.Write(");");
            Response.Write("\r\n");
        }
        Response.Write("</script>");
        Response.Write("\r\n");


        Empresas = DataInterfaceObj.getRemoteData("SELECT DISTINCT c.PessoaID AS EmpresaID, c.Nome AS EmpresaNome, c.Fantasia AS EmpresaFantasia, c.Fantasia AS Ordem, " +
                     "CONVERT(BIT, CASE b.EmpresaID WHEN a.EmpresaDefaultID THEN 1 ELSE 0 END) AS Def, d.PaisID AS EmpresaPaisID, " +
                     "d.CidadeID AS EmpresaCidadeID, d.UFID AS EmpresaUFID, aa.TipoEmpresaID AS TipoEmpresaID, aa.CorFrente AS CorFrente, " +
                     "aa.CorFundo AS CorFundo, (ISNULL((SELECT TOP 1 IdiomaID FROM Recursos  WITH(NOLOCK) WHERE (RecursoID=999)),246)) AS IdiomaSistemaID, " +
                     "(ISNULL((SELECT TOP 1 IdiomaID FROM Localidades_Idiomas  WITH(NOLOCK) WHERE (LocalidadeID=d.PaisID AND Oficial=1) ORDER BY Ordem),246)) AS IdiomaEmpresaID, " +
                     "l.ConceitoID AS MoedaID, l.SimboloMoeda, aa.TemProducao, dbo.fn_Pessoa_URL(c.PessoaID, 125, NULL) AS URL " +
                "FROM RelacoesPesRec a WITH(NOLOCK) " +
                        "INNER JOIN RelacoesPesRec_Perfis b WITH(NOLOCK) ON a.RelacaoID = b.RelacaoID " +
                        "INNER JOIN RelacoesPesRec aa WITH(NOLOCK) ON aa.SujeitoID=b.EmpresaID " +
                        "INNER JOIN Pessoas c WITH(NOLOCK) ON b.EmpresaID = c.PessoaID " +
                        "INNER JOIN Pessoas_Enderecos d WITH(NOLOCK) ON c.PessoaID = d.PessoaID " +
                        "INNER JOIN Recursos j WITH(NOLOCK) ON b.PerfilID = j.RecursoID " +
                        "INNER JOIN dbo.vw_Direitos_Form DF with(noexpand)  ON b.PerfilID = DF.PerfilID " +
                        "INNER JOIN dbo.vw_Direitos_SubModulo DSM with(noexpand)  ON b.PerfilID = DSM.PerfilID AND DF.RecursoMaeID = DSM.RecursoID " +
                        "INNER JOIN dbo.vw_Direitos_Modulo DM with(noexpand) ON b.PerfilID = DM.PerfilID AND DF.RecursoMaeID = DSM.RecursoID " +
                        "INNER JOIN dbo.vw_Direitos_Overfly DO with(noexpand) ON b.PerfilID = DO.PerfilID AND DF.RecursoMaeID = DSM.RecursoID " +
                        "INNER JOIN RelacoesPesRec_Moedas k WITH(NOLOCK) ON aa.RelacaoID = k.RelacaoID " +
                        "INNER JOIN Conceitos l WITH(NOLOCK) ON k.MoedaID = l.ConceitoID " +
                "WHERE a.SujeitoID IN ((SELECT " + Session["userID"].ToString() + " " +
                                            "UNION ALL " +
                                        "SELECT UsuarioDeID " +
                                            "FROM DelegacaoDireitos WITH(NOLOCK) " +
                                            "WHERE (UsuarioParaID = " + Session["userID"].ToString() + " AND GETDATE() BETWEEN dtInicio AND dtFim))) " +
                         "AND a.ObjetoID = 999 " +
                         "AND a.EstadoID=2 " +
                         "AND a.TipoRelacaoID = 11 " +
                         "AND d.Ordem=1 " +
                         "AND aa.ObjetoID = 999 " +
                         "AND aa.EstadoID=2 " +
                         "AND aa.TipoRelacaoID = 12 " +
                         "AND DF.FormName = " + (char)39 + Session["formName"].ToString() + (char)39 + " " +
                         "AND (DF.Consultar1=1 OR DF.Consultar2=1) " +
                         "AND (DM.Consultar1=1 OR DM.Consultar2=1) " +
                         "AND (DSM.Consultar1=1 OR DSM.Consultar2=1) " +
                         "AND (DO.Consultar1=1 OR DO.Consultar2=1) " +
                         "AND j.EstadoID = 2 " +
                         "AND k.Faturamento = 1 " +
                "ORDER BY Ordem");


        Response.Write("<script ID=" + (char)34 + "variousVars" + (char)34 + " LANGUAGE=" + (char)34 + "javascript" + (char)34 + ">");
        Response.Write("\r\n");
        Response.Write("\r\n");
        Response.Write("var __dataEmpresas = new Array();");
        Response.Write("\r\n");



        for (int i = 0; Empresas != null && i < Empresas.Tables[1].Rows.Count; i++)
        {
            Response.Write("__dataEmpresas[" + i + "] = ");
            Response.Write("new Array(");

            //EmpresaID
            if (Empresas.Tables[1].Rows[i]["EmpresaID"] == null)
            {
                Response.Write("0" + ",");
            }
            else
            {
                Response.Write(Empresas.Tables[1].Rows[i]["EmpresaID"].ToString() + ", ");
            }

            //PaisEmpresaID
            if (Empresas.Tables[1].Rows[i]["EmpresaPaisID"] == null)
            {
                Response.Write("0" + ",");
            }
            else
            {
                Response.Write(Empresas.Tables[1].Rows[i]["EmpresaPaisID"].ToString() + ", ");
            }
            //EmpresaCidade
            if (Empresas.Tables[1].Rows[i]["EmpresaCidadeID"] == null)
            {
                Response.Write("0" + ",");
            }
            else
            {
                Response.Write(Empresas.Tables[1].Rows[i]["EmpresaCidadeID"].ToString() + ", ");
            }
            //Empresa Default
            if (Empresas.Tables[1].Rows[i]["Def"] == null)
            {
                Response.Write("false" + ",");
            }
            else
            {
                Response.Write("'" + Empresas.Tables[1].Rows[i]["Def"].ToString().ToLower() + "'" + ", ");
            }
            //Empresa Fantasia
            if (Empresas.Tables[1].Rows[i]["EmpresaFantasia"] == null)
            {
                Response.Write(" " + ",");
            }
            else
            {
                Response.Write("'" + Empresas.Tables[1].Rows[i]["EmpresaFantasia"].ToString() + "'" + " , ");
            }
            //Empresa UF ID
            if (Empresas.Tables[1].Rows[i]["EmpresaUFID"] == null)
            {
                Response.Write("0" + ",");
            }
            else
            {
                Response.Write("'" + Empresas.Tables[1].Rows[i]["EmpresaUFID"].ToString() + "'" + " , ");
            }
            //TipoEmpresaID
            if (Empresas.Tables[1].Rows[i]["TipoEmpresaID"] == null)
            {
                Response.Write("0" + ",");
            }
            else
            {
                Response.Write("'" + Empresas.Tables[1].Rows[i]["TipoEmpresaID"].ToString() + "'" + " , ");
            }
            //Empresa Nome
            if (Empresas.Tables[1].Rows[i]["EmpresaNome"] == null)
            {
                Response.Write("''" + ",");
            }
            else
            {
                Response.Write("'" + Empresas.Tables[1].Rows[i]["EmpresaNome"].ToString() + "'" + " , ");
            }
            //Cor Frente
            if (Empresas.Tables[1].Rows[i]["CorFrente"] == null)
            {
                Response.Write("000000" + ",");
            }
            else
            {
                Response.Write("'" + Empresas.Tables[1].Rows[i]["CorFrente"].ToString() + "'" + " , ");
            }
            // CorFundo
            if (Empresas.Tables[1].Rows[i]["CorFundo"] == null)
            {
                Response.Write("FFFFFF" + ",");
            }
            else
            {
                Response.Write("'" + Empresas.Tables[1].Rows[i]["CorFundo"].ToString() + "'" + " , ");
            }
            //IdiomaSistema
            if (Empresas.Tables[1].Rows[i]["IdiomaSistemaID"] == null)
            {
                Response.Write("0" + ",");
            }
            else
            {
                Response.Write("'" + Empresas.Tables[1].Rows[i]["IdiomaSistemaID"].ToString() + "'" + " , ");
            }
            //IdiomaEmpresa
            if (Empresas.Tables[1].Rows[i]["IdiomaEmpresaID"] == null)
            {
                Response.Write("0" + ",");
            }
            else
            {
                Response.Write("'" + Empresas.Tables[1].Rows[i]["IdiomaEmpresaID"].ToString() + "'" + " , ");
            }
            //Moeda
            if (Empresas.Tables[1].Rows[i]["MoedaID"] == null)
            {
                Response.Write("0" + ",");
            }
            else
            {
                Response.Write("'" + Empresas.Tables[1].Rows[i]["MoedaID"].ToString() + "'" + " , ");
            }
            //SimboloMoeda
            if (Empresas.Tables[1].Rows[i]["SimboloMoeda"] == null)
            {
                Response.Write(" " + ",");
            }
            else
            {
                Response.Write("'" + Empresas.Tables[1].Rows[i]["SimboloMoeda"].ToString() + "'" + " , ");
            }
            //Tem Producao
            if (Empresas.Tables[1].Rows[i]["TemProducao"] == null)
            {
                Response.Write("0" + ",");
            }
            else
            {
                Response.Write("'" + Empresas.Tables[1].Rows[i]["TemProducao"].ToString() + "'" + " , ");
            }
            // URL
            if (Empresas.Tables[1].Rows[i]["URL"] == null)
            {
                Response.Write(" " + ",");
            }
            else
            {
                Response.Write("'" + Empresas.Tables[1].Rows[i]["URL"].ToString() + "'");
            }

            Response.Write(");");
            Response.Write("\r\n");
        }
        Response.Write("</script>");
        Response.Write("\r\n");
        
        
    %>



    <script id="wndJSProc" language="javascript">
<!--

    // variavel de timer
    var __intIDEx;

    /********************************************************************
    Loop de mensagens
    ********************************************************************/
    function wndJSProc(idElement, msg, param1, param2) {
        switch (msg) {
            case JS_STBARWRITE:
                {
                    if (param1[0].toUpperCase() == 'CHILD')
                        writeInCell(param1);
                }
                break;

            case JS_STBARREAD:
                {
                    if (param1[0].toUpperCase() == 'CHILD')
                        return getDataInCell(window, param1);
                }
                break;

            case JS_STBARGEN:
                {
                    if (param1.toUpperCase() == 'CMBSTATUS') {
                        if (selEmpresa.options.length != 0)
                            selEmpresa.disabled = param2;
                        if (selForm.options.length != 0)
                            selForm.disabled = param2;
                        return param2;
                    }
                    if (param1.toUpperCase() == 'CMBSTATUS_FORMS') {
                        if (selForm.options.length != 0)
                            selForm.disabled = param2;
                        return param2;
                    }
                }
                break;

            case JS_COMBODATA:
                {
                    if (param1.toUpperCase() == 'VALUE')
                        return selEmpresa.options[selEmpresa.selectedIndex].value;
                }
                break;

            case JS_WIDEMSG:
                {
                    // Retorna a quem chamou desde que nao seja o proprio
                    if (idElement == 'statusbarchildHtml')
                        return null;

                    if (param1 == '__EMPRESADATA') {
                        return empresaData();
                    }
                }
                break;

            case JS_MSGRESERVED:
                {
                    if (param1 == 'SETCMBFORMS') {
                        setComboOption();
                        paraForm.style.visibility = 'hidden';
                        selForm.style.visibility = 'inherit';

                        return true;
                    }

                }
            default:
                return null;
        }
    }

    /********************************************************************
    Configura o html
    ********************************************************************/
    function window_onload() {
        var i;

        var nEmpresaToStartForm = sendJSMessage('statusbarchildHtml', JS_MSGRESERVED, 'GETEMPRESATOSTARTFORM', null);

        for (i = 0; i < selEmpresa.options.length; i++) {
            if (selEmpresa.options[i].value == nEmpresaToStartForm) {
                selEmpresa.selectedIndex = i;
                break;
            }
        }

        // Cores do status bar
        statusBar_Colors(null, nEmpresaToStartForm);

        /*    
        // seleciona no combo de empresas a atual empresa do browser mae
        var empresaData = null;
        
        empresaData = sendJSMessage('statusbarchildHtml', JS_WIDEMSG, '__EMPRESADATA', null);    
        
        if ( empresaData == null )
            return false;
        
        // Isto era necessario porque no css option color era white
        //for ( i=0; i<selEmpresa.options.length; i++ )
        //    selEmpresa.options[i].style.color = 'black';
        
        for ( i=0; i<selEmpresa.options.length; i++ )
        {
            if ( selEmpresa.options[i].value == empresaData[0] )
            {
                selEmpresa.selectedIndex = i;
                break;
            }
        }
                
        // Cores do status bar
        statusBar_Colors(null, empresaData[0]);
    */
        // parametros de dimensoes dos elementos
        var divCount = 0;
        var divWidth = (MAX_FRAMEWIDTH / 5);
        var divGap = 1;
        var i;

        // ajusta o div PrgNome
        with (divPrgNome.style) {
            height = 24;
            width = divWidth - 2;
            left = 0;
        }

        // ajusta o paragrafo paraPrgNome
        with (paraPrgNome.style) {
            fontSize = '10pt';
            fontWeight = 'bold';
            left = 4;
            top = 2;
            width = divPrgNome.offsetWidth - 4;
        }

        // ajusta o div divEmpresa
        divCount++;
        with (divEmpresa.style) {
            width = divWidth - 4;
            left = divPrgNome.offsetLeft + divPrgNome.offsetWidth + divGap;
        }

        // ajusta o sel selEmpresa
        with (selEmpresa.style) {
            //background = STBAR_BKGROUND;
            left = 0;
            fontSize = '10pt';
            fontWeight = 'normal';
            width = divEmpresa.offsetWidth - 1;
        }

        // ajusta o div divUsuario
        divCount++;
        with (divUsuario.style) {
            width = divWidth + 5;
            left = divEmpresa.offsetLeft + divEmpresa.offsetWidth + divGap;
        }

        // ajusta o paragrafo paraUsuario
        with (paraUsuario.style) {
            fontSize = '10pt';
            fontWeight = 'normal';
            left = 4;
            top = 2;
            width = divUsuario.offsetWidth - 2;
        }

        // ajusta o div divForm
        divCount++;
        with (divForm.style) {
            width = divWidth;
            left = (divCount * divWidth) + divGap;
        }

        // ajusta o paragrafo paraForm
        with (paraForm.style) {
            fontSize = '10pt';
            fontWeight = 'normal';
            left = 4;
            top = 2;
            width = 145;
            visibility = 'inherit';
        }

        // ajusta o sel selEmpresa
        with (selForm.style) {
            //background = STBAR_BKGROUND;
            left = 0;
            fontSize = '10pt';
            fontWeight = 'normal';
            width = divForm.offsetWidth - 1;
            visibility = 'hidden';
        }

        fillComboSelForm();

        // ajusta o div divModo
        divCount++
        with (divModo.style) {
            width = divWidth;
            left = (divCount * divWidth) + divGap + 1;
        }

        // ajusta o paragrafo paraModo
        with (paraModo.style) {
            fontSize = '10pt';
            fontWeight = 'normal';
            left = 4;
            top = 2;
            width = 150;
        }

        // ajusta o traco inferior hr_Inf
        with (hr_Inf.style) {
            backgroundColor = 'darkgray';
            left = 0;
            top = parseInt(divPrgNome.currentStyle.top, 10) + parseInt(divPrgNome.currentStyle.height, 10) - 2;
            width = parseInt(divModo.currentStyle.left, 10) + parseInt(divModo.currentStyle.width, 10);
            height = 2;
        }

        // ajusta o traco a direita hr_Right
        with (hr_Right.style) {
            backgroundColor = 'darkgray';
            left = parseInt(divModo.currentStyle.left, 10) + parseInt(divModo.currentStyle.width, 10) - 2;
            top = 0;
            width = 2;
            height = parseInt(divModo.currentStyle.height, 10);
        }

        setDivTitlesPos();

        empresaData_Change();

        selEmpresa.style.fontWeight = 'bold';

        // Translada interface pelo dicionario do sistema
        translateInterface(window, null);

        // ajusta o body do html
        with (statusbarchildBody) {
            style.backgroundColor = 'transparent';
            scroll = 'no';
            style.visibility = 'visible';
        }

        // ajusta dimensoes e mostra o status bar no arquivo mainmenu.asp
        // (ou sua biblioteca js) que esta controlando o carregamento
        // da interface da qual o status bar faz parte.

        // este arquivo asp carregou
        sendJSMessage('statusbarchildHtml', JS_PAGELOADED, new Array(window.top.name.toString(), 'USERCHILDBROWSER'), 0X1);
    }

    var __glbDivTitlesPos = [0, 0, 0, 0, 0, 0];

    /********************************************************************
    Muda posicao dos divs do status bar
    ********************************************************************/
    function setDivTitlesPos() {
        __glbDivTitlesPos[1] = divPrgNome.offsetLeft;
        __glbDivTitlesPos[2] = divEmpresa.offsetLeft;
        __glbDivTitlesPos[3] = divUsuario.offsetLeft;
        __glbDivTitlesPos[4] = divForm.offsetLeft;
        __glbDivTitlesPos[5] = divModo.offsetLeft;

        divPrgNome.style.left = __glbDivTitlesPos[3];
        divPrgNome.style.width = divPrgNome.offsetWidth + 7;

        divEmpresa.style.left = __glbDivTitlesPos[2];

        divUsuario.style.left = __glbDivTitlesPos[4];
        divUsuario.style.width = divUsuario.offsetWidth - 5;

        divForm.style.left = __glbDivTitlesPos[1];
        divForm.style.width = divForm.offsetWidth - 2;

        divModo.style.left = __glbDivTitlesPos[5];
    }


    /********************************************************************
    A empresa selecionada mudou
    ********************************************************************/
    function selEmpresa_onchange() {
        // via timer por questoes esteticas
        selEmpresaChanged();
    }

    /********************************************************************
    Informa pesqlist que a empresa selecionada mudou
    ********************************************************************/
    function selEmpresaChanged() {
        if (__intIDEx != null) {
            window.clearInterval(__intIDEx);
            __intIDEx = null;
        }

        // Cores do status bar
        statusBar_Colors(null, selEmpresa.options[selEmpresa.selectedIndex].value);

        empresaData_Change();

        sendJSMessage('AnyHtml', JS_STBARGEN, 'EMPRESACHANGED', null);
    }

    function setComboOption() {
        var i;

        for (i = 0; i < selForm.length; i++) {
            if (selForm.options[i].innerText == paraForm.innerText) {
                // Duplica o form atual no combo
                selForm.options[0].value = selForm.options[i].value;
                selForm.options[0].innerText = selForm.options[i].innerText;
                selForm.selectedIndex = 0;
                break;
            }
        }
    }

    function fillComboSelForm() {
        var i;
        clearComboSelForm(['selForm']);

        // Este option em branco ira conter o form atual, eh que neste ponto ainda nao
        // eh possivel saber qual eh
        var oOption = document.createElement("OPTION");
        oOption.text = '';
        oOption.value = '';
        selForm.add(oOption);

        for (i = 0; i < __cmbFormsData.length; i++) {
            if (__cmbFormsData[i][0] == selEmpresa.options(selEmpresa.selectedIndex).value) {
                optionStr = __cmbFormsData[i][1];
                optionValue = __cmbFormsData[i][2];
                var oOption = document.createElement("OPTION");
                oOption.text = optionStr;
                oOption.value = optionValue;
                selForm.add(oOption);
            }
        }
    }

    function clearComboSelForm(aCombos) {
        var comboID;
        var i, j, y, coll = document.all.tags('SELECT');

        for (y = 0; y < aCombos.length; y++) {
            for (i = 0; i < coll.length ; i++) {
                if (coll(i).id == aCombos[y]) {

                    for (j = coll(i).length - 1; j >= 0; j--) {
                        coll(i).remove(j);
                    }
                }
            }
        }
    }

    function selForm_onchange() {
        sendJSMessage('statusbarchildHtml', JS_MSGRESERVED, 'SETEMPRESATOSTARTFORM', selEmpresa.options(selEmpresa.selectedIndex).value);
        sendJSMessage('statusbarchildHtml', JS_MSGRESERVED, 'LOADFORMFROMFORM', selForm.options(selForm.selectedIndex).value);

        setComboOption();
    }

    //-->
    </script>

</head>

<body id="statusbarchildBody" name="statusbarchildBody" language="javascript" onload="return window_onload()">
    <div id="divPrgNome" name="divPrgName" class="divGeneral">
        <p id="paraPrgNome" name="paraPrgNome" class="paraNormal"></p>
    </div>

    <div id="divUsuario" name="divUsuario" class="divGeneral">
        <p id="paraUsuario" name="paraUsuario" class="paraNormal"></p>
    </div>
    <div id="divForm" name="divForm" class="divGeneral">
        <p id="paraForm" name="paraForm" class="paraNormal"></p>
        <select id="selForm" name="selForm"
            class="selGeneral"
            language="javascript" onchange="return selForm_onchange()">
        </select>
    </div>
    </div>
    <div id="divModo" name="divModo" class="divGeneral">
        <p id="paraModo" name="paraModo" class="paraNormal"></p>
    </div>

    <%
        //'Preenche o combo de empresas
        string cmbEmpresaDisabled;
        int iCount = 0;

        cmbEmpresaDisabled = "disabled";

        while (Empresas.Tables[1].Rows.Count > iCount)
        {
            cmbEmpresaDisabled = "";

            iCount += 1;
        }
    %>
    <div id="divEmpresa" name="divEmpresa" class="divGeneral">
        <select id="selEmpresa" name="selEmpresa"
            class="selGeneral" <% = cmbEmpresaDisabled%>
            language="javascript" onchange="return selEmpresa_onchange()">
            <%
                //Preenche os options do combo de empresas
                string optSel;
                int idNum, nLastEmpresaID;
                optSel = "selected";
                idNum = 1;
                nLastEmpresaID = -1;
                Response.Write("\r\n");

                for (int i = 0; Empresas != null && i < Empresas.Tables[1].Rows.Count; i++)
                {
                    if (Empresas.Tables[1].Rows[i]["Def"].ToString() == "True")
                    {
                        optSel = "selected";
                    }
                    else
                    {
                        optSel = "";
                    }

                    if ((nLastEmpresaID.ToString()) != (Empresas.Tables[1].Rows[i]["EmpresaID"].ToString()))
                    {

                        Response.Write("<option value =" + Empresas.Tables[1].Rows[i]["EmpresaID"].ToString() +
                                    " id=optEmpresa" + idNum +
                                    " " + optSel + ">" +
                                    Empresas.Tables[1].Rows[i]["EmpresaFantasia"].ToString() +
                                    "</option>" + (char)13 + (char)10);
                        Response.Write("\r\n");
                        idNum = idNum + 1;
                    }
                    nLastEmpresaID = Convert.ToInt32(Empresas.Tables[1].Rows[i]["EmpresaID"].ToString());
                }
            %>
        </select>
    </div>
    <img id="hr_Inf" name="hr_Inf" class="lblGeneral"></img>
    <img id="hr_Right" name="hr_Right" class="lblGeneral"></img>
</body>
</html>
