﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="statusbarmain.aspx.cs" Inherits="Overfly3.statusbar.statusbarmain" %>

<html id="statusbarmainHtml" name="statusbarmainHtml">


<head>
    <title></title>
    <% 
        
        string pagesURLRoot;
        OVERFLYSVRCFGLib.OverflyMTS objSvrCfg = new OVERFLYSVRCFGLib.OverflyMTS();

        pagesURLRoot = objSvrCfg.PagesURLRoot(
                System.Configuration.ConfigurationManager.AppSettings["application"]);

        Response.Write("\r\n");
        Response.Write("\r\n");

        Response.Write("<LINK REL=" + (char)34 + "stylesheet" + (char)34 + " HREF=" + (char)34 + pagesURLRoot + "/statusbar/statusbar.css" + (char)34 + "type=" + (char)34 + "text/css" + (char)34 + ">");

        Response.Write("\r\n");

        Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/js_sysbase.js" + (char)34 + "></script>"); Response.Write("\r\n");
        Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/js_constants.js" + (char)34 + "></script>"); Response.Write("\r\n");
        Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/js_htmlbase.js" + (char)34 + "></script>"); Response.Write("\r\n");
        Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/js_strings.js" + (char)34 + "></script>"); Response.Write("\r\n");
        Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/js_interface.js" + (char)34 + "></script>"); Response.Write("\r\n");
        Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/js_statusbar.js" + (char)34 + "></script>"); Response.Write("\r\n");
        Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/js_fastbuttonsbar.js" + (char)34 + "></script>"); Response.Write("\r\n");
        Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/js_controlsbar.js" + (char)34 + "></script>"); Response.Write("\r\n");
        Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/js_modalwin.js" + (char)34 + "></script>"); Response.Write("\r\n");
        Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/js_rdsfacil.js" + (char)34 + "></script>"); Response.Write("\r\n");

        Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/statusbar/statusbar.js" + (char)34 + "></script>"); Response.Write("\r\n");

        Response.Write("\r\n");
        Response.Write("<script ID=" + (char)34 + "variousVars" + (char)34 + " LANGUAGE=" + (char)34 + "javascript" + (char)34 + ">");
        Response.Write("\r\n");

        Response.Write("var __dataEstados = new Array();");
        Response.Write("\r\n");

        Estados = DataInterfaceObj.getRemoteData("SELECT RecursoID AS EstadoID, RecursoAbreviado AS EstadoAbv, RecursoFantasia AS EstadoFantasia " +
                                            "FROM Recursos WITH(NOLOCK) WHERE TipoRecursoID = 7");

        for (int i = 0; Estados != null && i < Estados.Tables[1].Rows.Count; i++)
        {
            Response.Write("__dataEstados[" + i + "] = ");
            Response.Write("new Array(");

            if (Estados.Tables[1].Rows[i]["EstadoID"] == null)
            {
                Response.Write("0" + ",");
            }
            else
            {
                Response.Write(Estados.Tables[1].Rows[i]["EstadoID"].ToString() + ", ");
            }

            if (Estados.Tables[1].Rows[i]["EstadoAbv"] == null)
            {
                Response.Write("''" + ",");
            }
            else
            {
                Response.Write("'" + Estados.Tables[1].Rows[i]["EstadoAbv"].ToString() + "'" + " , ");
            }

            if (Estados.Tables[1].Rows[i]["EstadoFantasia"] == null)
            {
                Response.Write("''" + ",");
            }
            else
            {
                Response.Write("'" + Estados.Tables[1].Rows[i]["EstadoFantasia"].ToString() + "'");
            }

            Response.Write(");");
            Response.Write("\r\n");
        }

        Empresas = DataInterfaceObj.getRemoteData("SELECT DISTINCT c.PessoaID AS EmpresaID, c.Nome AS EmpresaNome, c.Fantasia AS EmpresaFantasia, c.Fantasia AS Ordem, " +
             "CONVERT(BIT, CASE WHEN ((b.EmpresaID = a.EmpresaDefaultID) AND (a.SujeitoID = " + Session["userID"].ToString() + " )) THEN 1 ELSE 0 END) AS Def, d.PaisID AS EmpresaPaisID, d.CidadeID AS EmpresaCidadeID, " +
             "d.UFID AS EmpresaUFID, aa.TipoEmpresaID AS TipoEmpresaID, aa.CorFrente AS CorFrente, aa.CorFundo AS CorFundo, " +
             "(ISNULL((SELECT TOP 1 IdiomaID FROM Recursos WITH(NOLOCK) WHERE (RecursoID=999)),246)) AS IdiomaSistemaID, " +
             "(ISNULL((SELECT TOP 1 IdiomaID FROM Localidades_Idiomas WITH(NOLOCK) WHERE (LocalidadeID=d.PaisID AND Oficial=1) ORDER BY Ordem),246)) AS IdiomaEmpresaID, " +
             "h.ConceitoID AS MoedaID, h.SimboloMoeda, aa.TemProducao, dbo.fn_Pessoa_URL(c.PessoaID, 125, NULL) AS URL " +
             "FROM RelacoesPesRec a WITH(NOLOCK) " +
                    "INNER JOIN RelacoesPesRec_Perfis b WITH(NOLOCK) ON a.RelacaoID = b.RelacaoID " +
                    "INNER JOIN Pessoas c WITH(NOLOCK) ON b.EmpresaID = c.PessoaID " +
                    "INNER JOIN Pessoas_Enderecos d WITH(NOLOCK) ON c.PessoaID=d.PessoaID " +
                    "INNER JOIN RelacoesPesRec aa WITH(NOLOCK) ON aa.SujeitoID=b.EmpresaID " +
                    "INNER JOIN vw_Direitos_Overfly DO WITH(NOEXPAND) ON DO.PerfilID = b.PerfilID " +
                    "INNER JOIN Recursos f  WITH(NOLOCK) ON b.PerfilID = f.RecursoID " +
                    "INNER JOIN RelacoesPesRec_Moedas g WITH(NOLOCK) ON aa.RelacaoID = g.RelacaoID " +
                    "INNER JOIN Conceitos h WITH(NOLOCK) ON g.MoedaID = h.ConceitoID " +
             "WHERE a.SujeitoID IN ((SELECT " + Session["userID"].ToString() + " " +
                                            "UNION ALL SELECT UsuarioDeID " +
                                        "FROM DelegacaoDireitos  WITH(NOLOCK) " +
                                        "WHERE (UsuarioParaID =  " + Session["userID"].ToString() + "  AND GETDATE() BETWEEN dtInicio AND dtFim))) " +
                    "AND a.ObjetoID = 999 " +
                    "AND a.EstadoID=2 " +
                    "AND a.TipoRelacaoID = 11 " +
                    "AND d.Ordem=1 " +
                    "AND aa.ObjetoID = 999 " +
                    "AND aa.EstadoID=2 " +
                    "AND aa.TipoRelacaoID = 12 " +
                    "AND (DO.Consultar1=1 OR DO.Consultar2=1) " +
                    "AND f.EstadoID = 2 AND g.Faturamento = 1 " +
             "ORDER BY Ordem, Def DESC");

        Response.Write("var __dataEmpresas = new Array();");
        Response.Write("\r\n");

        for (int i = 0; Empresas != null && i < Empresas.Tables[1].Rows.Count; i++)
        {
            Response.Write("__dataEmpresas[" + i + "] = ");
            Response.Write("new Array(");

            //EmpresaID
            if (Empresas.Tables[1].Rows[i]["EmpresaID"] == null)
            {
                Response.Write("0" + ",");
            }
            else
            {
                Response.Write(Empresas.Tables[1].Rows[i]["EmpresaID"].ToString() + ", ");
            }
            //PaisEmpresaID
            if (Empresas.Tables[1].Rows[i]["EmpresaPaisID"] == null)
            {
                Response.Write("0" + ",");
            }
            else
            {
                Response.Write(Empresas.Tables[1].Rows[i]["EmpresaPaisID"].ToString() + ", ");
            }
            //EmpresaCidade
            if (Empresas.Tables[1].Rows[i]["EmpresaCidadeID"] == null)
            {
                Response.Write("0" + ",");
            }
            else
            {
                Response.Write(Empresas.Tables[1].Rows[i]["EmpresaCidadeID"].ToString() + ", ");
            }
            //Empresa Default
            if (Empresas.Tables[1].Rows[i]["Def"] == null)
            {
                Response.Write("false" + ",");
            }
            else
            {
                Response.Write("'" + Empresas.Tables[1].Rows[i]["Def"].ToString().ToLower() + "'" + ", ");
            }
            //Empresa Fantasia
            if (Empresas.Tables[1].Rows[i]["EmpresaFantasia"] == null)
            {
                Response.Write(" " + ",");
            }
            else
            {
                Response.Write("'" + Empresas.Tables[1].Rows[i]["EmpresaFantasia"].ToString() + "'" + " , ");
            }
            //Empresa UF ID
            if (Empresas.Tables[1].Rows[i]["EmpresaUFID"] == null)
            {
                Response.Write("0" + ",");
            }
            else
            {
                Response.Write("'" + Empresas.Tables[1].Rows[i]["EmpresaUFID"].ToString() + "'" + " , ");
            }
            //TipoEmpresaID
            if (Empresas.Tables[1].Rows[i]["TipoEmpresaID"] == null)
            {
                Response.Write("0" + ",");
            }
            else
            {
                Response.Write("'" + Empresas.Tables[1].Rows[i]["TipoEmpresaID"].ToString() + "'" + " , ");
            }
            //Empresa Nome
            if (Empresas.Tables[1].Rows[i]["EmpresaNome"] == null)
            {
                Response.Write("''" + ",");
            }
            else
            {
                Response.Write("'" + Empresas.Tables[1].Rows[i]["EmpresaNome"].ToString() + "'" + " , ");
            }
            //Cor Frente
            if (Empresas.Tables[1].Rows[i]["CorFrente"] == null)
            {
                Response.Write("000000" + ",");
            }
            else
            {
                Response.Write("'" + Empresas.Tables[1].Rows[i]["CorFrente"].ToString() + "'" + " , ");
            }
            // CorFundo
            if (Empresas.Tables[1].Rows[i]["CorFundo"] == null)
            {
                Response.Write("FFFFFF" + ",");
            }
            else
            {
                Response.Write("'" + Empresas.Tables[1].Rows[i]["CorFundo"].ToString() + "'" + " , ");
            }
            //IdiomaSistema
            if (Empresas.Tables[1].Rows[i]["IdiomaSistemaID"] == null)
            {
                Response.Write("0" + ",");
            }
            else
            {
                Response.Write("'" + Empresas.Tables[1].Rows[i]["IdiomaSistemaID"].ToString() + "'" + " , ");
            }
            //IdiomaEmpresa
            if (Empresas.Tables[1].Rows[i]["IdiomaEmpresaID"] == null)
            {
                Response.Write("0" + ",");
            }
            else
            {
                Response.Write("'" + Empresas.Tables[1].Rows[i]["IdiomaEmpresaID"].ToString() + "'" + " , ");
            }
            //Moeda
            if (Empresas.Tables[1].Rows[i]["MoedaID"] == null)
            {
                Response.Write("0" + ",");
            }
            else
            {
                Response.Write("'" + Empresas.Tables[1].Rows[i]["MoedaID"].ToString() + "'" + " , ");
            }
            //SimboloMoeda
            if (Empresas.Tables[1].Rows[i]["SimboloMoeda"] == null)
            {
                Response.Write(" " + ",");
            }
            else
            {
                Response.Write("'" + Empresas.Tables[1].Rows[i]["SimboloMoeda"].ToString() + "'" + " , ");
            }
            //Tem Producao
            if (Empresas.Tables[1].Rows[i]["TemProducao"] == null)
            {
                Response.Write("0" + ",");
            }
            else
            {
                Response.Write("'" + Empresas.Tables[1].Rows[i]["TemProducao"].ToString() + "'" + " , ");
            }
            // URL
            if (Empresas.Tables[1].Rows[i]["URL"] == null)
            {
                Response.Write(" " + ",");
            }
            else
            {
                Response.Write("'" + Empresas.Tables[1].Rows[i]["URL"].ToString() + "'");
            }

            Response.Write(");");
            Response.Write("\r\n");
        }
        Response.Write("</script>");
        Response.Write("\r\n");
    %>
    <script id="wndJSProc" language="javascript">

        /********************************************************************
        Informa pesqlist que a empresa selecionada mudou
        ********************************************************************/


        // variavel de timer
        var __intIDEx;

        /********************************************************************
        Loop de mensagens
        ********************************************************************/
        function wndJSProc(idElement, msg, param1, param2) {
            var nTemp = null;

            switch (msg) {
                case JS_WIDEMSG:
                    {
                        // Retorna a quem chamou
                        if (param1 == '__EMPRESADATA') {
                            return empresaData();
                        }
                        else if (param1 == '__ESTADODATA') {
                            return estadoAbreviado(param2);
                        }
                    }
                    break;

                case JS_STBARWRITE:
                    {
                        if (param1[0].toUpperCase() == 'MAIN')
                            writeInCell(param1);
                    }
                    break;

                case JS_STBARREAD:
                    {
                        if (param1[0].toUpperCase() == 'MAIN')
                            return getDataInCell(window, param1);
                    }
                    break;

                case JS_STBARGEN:
                    {
                        if (param1.toUpperCase() == 'CMBSTATUS') {
                            if (selEmpresa.options.length != 0)
                                selEmpresa.disabled = param2;
                            return param2;
                        }
                    }
                    break;

                case JS_MSGRESERVED:
                    {
                        // Empresa para abrir o form
                        if (param1 == 'GETEMPRESATOSTARTFORM') {
                            nTemp = glb_empresaToStartForm;
                            glb_empresaToStartForm = (selEmpresa.options(selEmpresa.selectedIndex)).value;
                            return nTemp;
                        }
                        else if (param1 == 'SETEMPRESATOSTARTFORM') {
                            glb_empresaToStartForm = param2;
                            return true;
                        }
                    }

                default:
                    return null;
            }
        }

        var glb_empresaToStartForm = null;

        /********************************************************************
        Configura o html
        ********************************************************************/
        function window_onload() {
            glb_empresaToStartForm = (selEmpresa.options(selEmpresa.selectedIndex)).value;

            // Cores do status bar
            statusBar_Colors(null, null);

            //
            selEmpresaChanged()

            // parametros de dimensoes dos elementos
            var divCount = 0;
            var divWidth = (MAX_FRAMEWIDTH / 5);
            var divGap = 1;

            // ajusta o div PrgNome
            with (divPrgNome.style) {
                height = 24;
                width = divWidth;
                left = 0;
            }

            // ajusta o paragrafo paraPrgNome
            with (paraPrgNome.style) {
                fontSize = '10pt';
                fontWeight = 'bold';
                left = 4;
                top = 2;
                width = 145;
            }

            // ajusta o div divEmpresa
            divCount++;
            with (divEmpresa.style) {
                width = divWidth - 4;
                left = (divCount * divWidth) + divGap;
            }

            // ajusta o sel selEmpresa
            with (selEmpresa.style) {
                //background = STBAR_BKGROUND;
                left = 0;
                fontSize = '10pt';
                fontWeight = 'normal';
                width = divEmpresa.offsetWidth - 1;
            }

            // ajusta o div divUsuario
            divCount++;
            with (divUsuario.style) {
                width = divWidth + 3;
                left = divEmpresa.offsetLeft + divEmpresa.offsetWidth + divGap;
            }

            // ajusta o paragrafo paraUsuario
            with (paraUsuario.style) {
                fontSize = '10pt';
                fontWeight = 'normal';
                left = 4;
                top = 2;
                width = divUsuario.offsetWidth - 2;
            }

            // ajusta o div divForm
            divCount++;
            with (divForm.style) {
                width = divWidth;
                left = (divCount * divWidth) + divGap;
            }

            // ajusta o paragrafo paraForm
            with (paraForm.style) {
                fontSize = '10pt';
                fontWeight = 'normal';
                left = 4;
                top = 2;
                width = 145;
            }

            // ajusta o div divModo
            divCount++
            with (divModo.style) {
                width = divWidth;
                left = (divCount * divWidth) + divGap + 1;
            }

            // ajusta o paragrafo paraModo
            with (paraModo.style) {
                fontSize = '10pt';
                fontWeight = 'normal';
                left = 4;
                top = 2;
                width = 150;
            }

            // ajusta o traco inferior hr_Inf
            with (hr_Inf.style) {
                backgroundColor = 'darkgray';
                left = 0;
                top = parseInt(divPrgNome.currentStyle.top, 10) + parseInt(divPrgNome.currentStyle.height, 10) - 2;
                width = parseInt(divModo.currentStyle.left, 10) + parseInt(divModo.currentStyle.width, 10);
                height = 2;
            }

            // ajusta o traco a direita hr_Right
            with (hr_Right.style) {
                backgroundColor = 'darkgray';
                left = parseInt(divModo.currentStyle.left, 10) + parseInt(divModo.currentStyle.width, 10) - 2;
                top = 0;
                width = 2;
                height = parseInt(divModo.currentStyle.height, 10);
            }

            setDivTitlesPos();

            selEmpresa.style.fontWeight = 'bold';

            // Translada interface pelo dicionario do sistema
            translateInterface(window, null);

            // ajusta o body do html
            with (statusbarmainBody) {
                style.backgroundColor = 'transparent';
                scroll = 'no';
                style.visibility = 'visible';
            }          

            // ajusta dimensoes e mostra o status bar no arquivo mainmenu.asp
            // (ou sua biblioteca js) que esta controlando o carregamento
            // da interface da qual o status bar faz parte.

            // este arquivo asp carregou
            sendJSMessage('statusbarmainHtml', JS_PAGELOADED, new Array(window.top.name.toString(), 'USERMAINBROWSER'), 0X2);
        }

        var __glbDivTitlesPos = [0, 0, 0, 0, 0, 0];

        /********************************************************************
        Muda posicao dos divs do status bar
        ********************************************************************/
        function setDivTitlesPos() {
            __glbDivTitlesPos[1] = divPrgNome.offsetLeft;
            __glbDivTitlesPos[2] = divEmpresa.offsetLeft;
            __glbDivTitlesPos[3] = divUsuario.offsetLeft;
            __glbDivTitlesPos[4] = divForm.offsetLeft;
            __glbDivTitlesPos[5] = divModo.offsetLeft;

            divPrgNome.style.left = __glbDivTitlesPos[3];
            divPrgNome.style.width = divPrgNome.offsetWidth + 3;

            divEmpresa.style.left = __glbDivTitlesPos[2];

            divUsuario.style.left = __glbDivTitlesPos[4];
            divUsuario.style.width = divUsuario.offsetWidth - 3;
            divForm.style.left = __glbDivTitlesPos[1];
            divModo.style.left = __glbDivTitlesPos[5];
        }

        /********************************************************************
        Informa o mainmenu do sistema que a empresa selecionada mudou
        ********************************************************************/
        function selEmpresa_onchange() {
            glb_empresaToStartForm = (selEmpresa.options(selEmpresa.selectedIndex)).value;
            selEmpresaChanged();
        }

        /********************************************************************
        Informa pesqlist que a empresa selecionada mudou
        ********************************************************************/
        function selEmpresaChanged() {
            if (__intIDEx != null) {
                window.clearInterval(__intIDEx);
                __intIDEx = null;
            }

            // Cores do status bar
            statusBar_Colors(null, selEmpresa.options[selEmpresa.selectedIndex].value);

            empresaData_Change();

            sendJSMessage('statusbarmainHtml', JS_COMBOSELCHANGE, selEmpresa.options[selEmpresa.selectedIndex].value, null);
        }
    </script>

</head>

<body id="statusbarmainBody" name="statusbarmainBody" language="javascript" onload="return window_onload()">
    <div id="divPrgNome" name="divPrgName" class="divGeneral">
        <p id="paraPrgNome" name="paraPrgNome" class="paraNormal"></p>
    </div>

    <div id="divUsuario" name="divUsuario" class="divGeneral">
        <p id="paraUsuario" name="paraUsuario" class="paraNormal"></p>
    </div>
    <div id="divForm" name="divForm" class="divGeneral">
        <p id="paraForm" name="paraForm" class="paraNormal"></p>
    </div>
    <div id="divModo" name="divModo" class="divGeneral">
        <p id="paraModo" name="paraModo" class="paraNormal"></p>
    </div>

    <%
        //'Preenche o combo de empresas
        string cmbEmpresaDisabled;
        int iCount = 0;

        cmbEmpresaDisabled = "disabled";

        while (Empresas.Tables[1].Rows.Count > iCount)
        {
            cmbEmpresaDisabled = "";

            iCount += 1;
        }
    %>
    <div id="divEmpresa" name="divEmpresa" class="divGeneral">
        <select id="selEmpresa" name="selEmpresa"
            class="selGeneral" <% = cmbEmpresaDisabled%>
            language="javascript" onchange="return selEmpresa_onchange()">
            <%
                //Preenche os options do combo de empresas
                string optSel;
                int idNum, nLastEmpresaID;
                optSel = "selected";
                idNum = 1;
                nLastEmpresaID = -1;
                Response.Write("\r\n");

                for (int i = 0; Empresas != null && i < Empresas.Tables[1].Rows.Count; i++)
                {
                    if (Empresas.Tables[1].Rows[i]["Def"].ToString() == "True")
                    {
                        optSel = "selected";
                    }
                    else
                    {
                        optSel = "";
                    }

                    if ((nLastEmpresaID.ToString()) != (Empresas.Tables[1].Rows[i]["EmpresaID"].ToString()))
                    {

                        Response.Write("<option value =" + Empresas.Tables[1].Rows[i]["EmpresaID"].ToString() +
                                    " id=optEmpresa" + idNum +
                                    " " + optSel + ">" +
                                    Empresas.Tables[1].Rows[i]["EmpresaFantasia"].ToString() +
                                    "</option>" + (char)13 + (char)10);
                        Response.Write("\r\n");
                        idNum = idNum + 1;
                    }
                    nLastEmpresaID = Convert.ToInt32(Empresas.Tables[1].Rows[i]["EmpresaID"].ToString());
                }

            %>
        </select>
    </div>
    <img id="hr_Inf" name="hr_Inf" class="lblGeneral"></img>
    <img id="hr_Right" name="hr_Right" class="lblGeneral"></img>

</body>
</html>
