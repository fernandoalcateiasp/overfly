using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace WSData
{
    public partial class teste2URL : System.Web.UI.Page
    {
        public dataInterface m_oDataInterface;

        protected void Page_Load(object sender, EventArgs e)
        {
			Response.Cache.SetNoStore();
			Response.Cache.SetCacheability(HttpCacheability.NoCache);

			m_oDataInterface = new dataInterface(
				System.Configuration.ConfigurationManager.AppSettings["application"]
			);
        }
    }
}
