using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace WSData
{
    /******************************************************
     * Classe dataInterface
     * Constructors
     * public dataInterface(string applicationName)
     * 
     * Destructor
     * 
     * Private variables
     * private string m_strApplicationName = "";
     * private string m_strConn = "";
     * private int m_timeout = (10 * 60);
     * private System.Data.SqlClient.SqlConnection m_connection = null;
     * private Object m_oOverflySvrCfg = null;
     * 
     * Public variables
     * public string ApplicationName
     * public int timeout (read only)
     * 
     * Private functions
     * private static Object comCreateObject(string sProgID)
     * private void normalizaDatasetXMLReturn(System.Data.DataTable dsStructure, System.Data.DataTable dsData)
     * 
     * Public functions
     * public bool connectDatabase()
     * public void disconnectDatabase()
     * public System.Data.SqlClient.SqlConnection getConnection()
     * public void getRemoteData(ref string strSQL, ref string Error, ref System.Data.DataSet oDataSet)
     * public System.Data.DataSet getRemoteData(string strSQL)
     * public void getRemoteDataEX(ref string strSQL, ref System.Data.DataSet oDataSet)
     * public void execNonQueryProcedure(string procName, ProcedureParameters[] parametros)
     * public System.Data.DataSet execQueryProcedure(string procName, ProcedureParameters[] parametros)
     * public Int32 ExecuteSQLCommand(string strSQL)
     * public string updateRecords(string strRecords, string culture)
     * public System.Xml.XmlDocument datasetToXML(System.Data.DataSet dataset)
     * 
     * Events
     * 
     *       
    *******************************************************/
    public class dataInterface
    {
        private string m_strApplicationName = "";
        private string m_strConn = "";
        private int m_timeout = (10 * 60);

        private System.Data.SqlClient.SqlConnection m_connection = null;

        private Object m_oOverflySvrCfg = null;

        public dataInterface(string applicationName)
        {
			m_strApplicationName = applicationName;

            m_oOverflySvrCfg = comCreateObject("OverflySvrCfg.OverflyMTS");
            m_strConn = ((OVERFLYSVRCFGLib.OverflyMTS)m_oOverflySvrCfg).DotNetDataBaseStrConn(m_strApplicationName);
        }

        private static Object comCreateObject(string sProgID)
        {
            // We get the type using just the ProgID
            Type oType = Type.GetTypeFromProgID(sProgID);

            if (oType != null)
            {
                return Activator.CreateInstance(oType);
            }

            return null;
        }

        public string ApplicationName
        {
            get { return m_strApplicationName; }
        }

        public bool connectDatabase()
        {
            bool bResult = true;
            string sError = "";

            try
            {
                m_connection = new System.Data.SqlClient.SqlConnection(m_strConn);
                m_connection.Open();
            }
            catch (System.Data.SqlClient.SqlException e)
            {
                disconnectDatabase();

                sError = e.Message;
                bResult = false;
            }
            return bResult;
        }

        public void disconnectDatabase()
        {
            if (m_connection != null)
            {
                m_connection.Close();
                m_connection.Dispose();
            }
        }

        public System.Data.SqlClient.SqlConnection getConnection()
        {
            return m_connection;
        }
        
        public void getRemoteData(ref string strSQL, ref string Error, ref System.Data.DataSet oDataSet)
        {
            try
            {
                getRemoteDataEX(ref strSQL, ref oDataSet);
            }
            catch (Exception e)
            {
                Error = e.Message;
            }

            return;
        }

        public System.Data.DataSet getRemoteData(string strSQL)
        {
            System.Data.DataSet oDataSet = null;

            getRemoteDataEX(ref strSQL, ref oDataSet);

            return oDataSet;
        }

        public void getRemoteDataEX(ref string strSQL, ref System.Data.DataSet oDataSet)
        {
            if (!this.connectDatabase())
                return;

            System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand(strSQL, m_connection);

            cmd.CommandTimeout = timeout;

            if (oDataSet == null)
                oDataSet = new System.Data.DataSet();

            // Obtem o schema da query
            System.Data.SqlClient.SqlDataReader reader = null;
            reader = cmd.ExecuteReader(CommandBehavior.KeyInfo);

            DataTable testeDT = ((DataTable)reader.GetSchemaTable());

            oDataSet.Tables.Add(testeDT.Copy());

            int tblCount = oDataSet.Tables.Count;
            tblCount--;

            oDataSet.Tables[tblCount].TableName = "TABLE_STRUCTURE";
            reader.Close();
            reader.Dispose();

            // Obtem os dados da query
            System.Data.SqlClient.SqlDataAdapter oDataAdapter = new System.Data.SqlClient.SqlDataAdapter();
            oDataAdapter.SelectCommand = cmd;
            oDataAdapter.Fill(oDataSet, "TABLE_DATA");

            normalizaDatasetXMLReturn(oDataSet.Tables[tblCount], oDataSet.Tables[(tblCount + 1)]);

            // Descarrega os objetos
            testeDT.Dispose();
            oDataAdapter.Dispose();

            cmd.Dispose();
            this.disconnectDatabase();

            return;
        }

        public void execNonQueryProcedure(string procName, ProcedureParameters[] parametros)
        {
            execNonQueryProcedure(procName, parametros, "");
        }

        public void execNonQueryProcedure(string procName, ProcedureParameters[] parametros, string TypeName)
        {
            if (!connectDatabase())
                return;

            // Create a new SqlDataAdapter object.
            System.Data.SqlClient.SqlDataAdapter oDataAdapter = new System.Data.SqlClient.SqlDataAdapter();

            // Garante que as informacoes pertinentes aos campos chegue do lado do cliente
            oDataAdapter.MissingSchemaAction = System.Data.MissingSchemaAction.AddWithKey;

            // Create a Command Object and pass a valid Connection object
            System.Data.SqlClient.SqlCommand cmd =
                new System.Data.SqlClient.SqlCommand(procName, m_connection);

            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandTimeout = timeout;

            System.Data.SqlClient.SqlParameter[] procParams =
                new System.Data.SqlClient.SqlParameter[parametros.Length];

            for (int p = 0; p < parametros.Length; p++)
            {
                // Quando h� um type do tipo table
                if (parametros[p].Type == SqlDbType.Structured)
                {
                    procParams[p] = cmd.Parameters.Add(
                        new System.Data.SqlClient.SqlParameter(parametros[p].Name, parametros[p].Type)
                        {
                            TypeName = TypeName,
                            Value = parametros[p].Data
                        }
                    );
                }
                else
                {
                    if (parametros[p].Length == -1)
                    {
                        procParams[p] = cmd.Parameters.Add(parametros[p].Name, parametros[p].Type);
                    }
                    else
                    {
                        procParams[p] = cmd.Parameters.Add(parametros[p].Name, parametros[p].Type, parametros[p].Length);
                    }

                    procParams[p].Direction = parametros[p].Direction;
                    procParams[p].Value = parametros[p].Data; 
                }
            }

            cmd.ExecuteNonQuery();

            for (int p = 0; p < parametros.Length; p++)
            {
                if (parametros[p].Direction == ParameterDirection.Output
                    || parametros[p].Direction == ParameterDirection.InputOutput
                    || parametros[p].Direction == ParameterDirection.ReturnValue)
                {
                    parametros[p].Data = procParams[p].Value;
                }
            }
           
            cmd.Dispose();
            disconnectDatabase();
        }

        public System.Data.DataSet execQueryProcedure(string procName, ProcedureParameters[] parametros)
        {
            if (!connectDatabase())
                return null;
			
            // Create a new SqlDataAdapter object.
            System.Data.SqlClient.SqlDataAdapter oDataAdapter = new System.Data.SqlClient.SqlDataAdapter();

            // Garante que as informacoes pertinentes aos campos chegue do lado do cliente
            oDataAdapter.MissingSchemaAction = System.Data.MissingSchemaAction.AddWithKey;

            // Create a Command Object and pass a valid Connection object
            System.Data.SqlClient.SqlCommand cmd =
                new System.Data.SqlClient.SqlCommand(procName, m_connection);

            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandTimeout = timeout;

            System.Data.SqlClient.SqlParameter[] procParams =
                new System.Data.SqlClient.SqlParameter[parametros.Length];

            string strReturn = "";
            for (int p = 0; p < parametros.Length; p++)
            {   
                procParams[p] = cmd.Parameters.Add(parametros[p].Name, parametros[p].Type);
                procParams[p].Direction = parametros[p].Direction;
                procParams[p].Value = parametros[p].Data;
				procParams[p].Size = parametros[p].Length;

              //  strReturn += procParams[p].ParameterName + "=" + procParams[p].SqlValue.ToString();
                
                //if(p < parametros.Length - 1) {
                //strReturn += ", ";retirado por Marco Fortunato - 10/10/2014    
				//}             
            }

            // Set the SelectCommand property to the Command object
            oDataAdapter.SelectCommand = cmd;

            // Create a new DataSet to hold the data from the SqlDataAdapter
            System.Data.DataSet oDataSet = new System.Data.DataSet();

			// Obtem o schema da query
			System.Data.SqlClient.SqlDataReader reader = cmd.ExecuteReader(CommandBehavior.KeyInfo);
			DataTable testeDT = ((DataTable)reader.GetSchemaTable());
			oDataSet.Tables.Add(testeDT.Copy());
			oDataSet.Tables[0].TableName = "TABLE_STRUCTURE";
			reader.Close();
			reader.Dispose();

			// Obtem os dados da query
			// Call the Fill method to execute the Command
            // and load the data into the DataSet.
			oDataSet.EnforceConstraints = false;
			oDataAdapter.Fill(oDataSet, "TABLE_DATA");

            normalizaDatasetXMLReturn(oDataSet.Tables[0], oDataSet.Tables[1]);

            for (int p = 0; p < parametros.Length; p++)
            {
                if (parametros[p].Direction == ParameterDirection.Output
                    || parametros[p].Direction == ParameterDirection.InputOutput
                    || parametros[p].Direction == ParameterDirection.ReturnValue)
                {
                    parametros[p].Data = procParams[p].Value;
                }
            }

            oDataAdapter.Dispose();
            cmd.Dispose();
            disconnectDatabase();

			DataColumnCollection tableCollumns = oDataSet.Tables[0].Columns;

			DataColumn collumn = tableCollumns[0];
			
            return oDataSet;
        }   

		public Int32 ExecuteSQLCommand(string strSQL)
        {
			// N�o executa se o comando recebido for nulo ou vazio.
			if(strSQL == null || strSQL.Trim().Length == 0) {
				return -1;
			}
			
            if (!this.connectDatabase())
                return -1;

            Int32 nReturn = -1;

            System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand(strSQL, m_connection);
            cmd.CommandTimeout = timeout;

            nReturn = cmd.ExecuteNonQuery();

            this.disconnectDatabase();

            // Retorna o numero de linhas afetadas
            return nReturn;
        }

        /**
         * Este metodo e responsavel por atualizar o banco de dados com o registro recebido.
         * A string recebida para a operacao esta codificada em blocos, separados por |,
         * segundo a seguinte formatacao:
         * 
         * Bloco 1
         *	Codigo da natureza da operacao que pode ser:
         *		1 = Update
         *		2 = Delete
         *		3 = Insert
         * 
         *	Qualquer codigo diferente destes representa um erro que e� informado no retorno
         * do web service.
         * 
         * Bloco 2
         *	Identifica a, ou as tabelas do banco de dados alvo da operacao e quais campos
         * fazem parte desta operacao.
         * 
         * Bloco 3
         *	Informacoes dos campos que participam da alteracao.
         */
		public string updateRecords(string strRecords, string culture)
        {
			// Se a conexao com o banco de dados nao estiver estabelecida,
			// retorna informando o fato.
			if (!this.connectDatabase())
			{
				return "Sem conexao com o banco de dados.";
			}

            string msgReturn = "";

			// Formato de strRecords:
			// "<ACAO>'\n'<TABELA>'\n'[<CAMPO>[char(14)]*]*[\n]*
			// Exemplo.
			//"1|Pessoas|0;PessoaID;int;7;557056/1;Fantasia;varchar;Allplus;12\n1;PessoaID;int;8;557056/1;Fantasia;varchar;Alcateia;64"
			/////////////////////////////////////////////////////////////
			char[] tabelaToken = { (char)23 };
			char[] registroToken = { (char)24 };

			// Define o indice no bloco de registros.
			int bloco = -4;
			// Faz o parser na string recebida separando os registros para processamento.
			string[] blocos = strRecords.Split(tabelaToken);

			System.Data.SqlClient.SqlCommand cmd = null;
			WSData.RecordData record;
			System.Text.StringBuilder sql =
				new System.Text.StringBuilder();

			try
			{
				// Marca o in�cio da transa��o.
				sql.Append("BEGIN TRANSACTION;\n");

				// Monta os comandos da transa��o
				while (blocos[(bloco += 4)].Length > 0)
				{
					// Cria um objeto WSData.RecordData para gerar o comando sql necessario
					// passando ...
					record = new WSData.RecordData(
						int.Parse(blocos[bloco]), // A acao a ser executada
						blocos[bloco + 1], // A tabela afetada
						blocos[bloco + 2].Split(registroToken), // O registro da tabela.
						blocos[bloco + 3].Split(registroToken), // O registro original da tabela.
						culture);

					// Gera o comando sql a ser executado.
					sql.Append(record.ToString());
					sql.Append("\n");
				}

				// Marca o fim da transa��o.
				sql.Append("COMMIT TRANSACTION;\n");

				// Executa a transa��o.
				if (sql.Length > 0)
				{
					cmd =
						new System.Data.SqlClient.SqlCommand(
							sql.ToString(), m_connection);
                    cmd.CommandTimeout = timeout;
					cmd.ExecuteNonQuery();
				}
			}
			catch (Exception e)
			{
				msgReturn = e.Message;
			}
			finally
			{
                if (cmd != null)
                {
                    cmd.Connection.Close();
                    cmd.Dispose();
                }
			}

            return msgReturn;
		}

        public System.Xml.XmlDocument datasetToXML(System.Data.DataSet dataset)
        {
            string strXML = null;
            System.Xml.XmlDocument xmlReturn = null;

            // Converte o dataset em XML
            System.IO.MemoryStream XMLStream = new System.IO.MemoryStream();
            dataset.WriteXml(XMLStream);

            System.IO.StreamReader streamReader = 
				new System.IO.StreamReader(XMLStream, System.Text.Encoding.UTF8);

            XMLStream.Position = 0;

            strXML = streamReader.ReadToEnd();
            
            xmlReturn = new System.Xml.XmlDocument();
            
            xmlReturn.LoadXml(strXML);

            return xmlReturn;
        }
        
        public int timeout
        {
            get { return m_timeout; }

            // comentado para que a variavel seja read only
            // set { m_timeout = value; }
        }

        private void normalizaDatasetXMLReturn(System.Data.DataTable dsStructure, System.Data.DataTable dsData)
        {
            if (dsData.Columns.Count != dsStructure.Rows.Count)
            {
                int strutcSize = dsStructure.Rows.Count;
                int dataSize = dsData.Columns.Count;

                while (strutcSize > dataSize)
                {
                    dsStructure.Rows[strutcSize - 1].Delete();
                    strutcSize--;
                }
            }
        }
    }
}