using System;
using System.Data;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.ComponentModel;


namespace WSData
{
    /// <summary>
    /// Summary description for Service1
    /// </summary>

    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    public class OverflyRDSWebService : System.Web.Services.WebService
    {
        private dataInterface m_oDataInterface;

        // This is a little sample of a WEB Service method
        [WebMethod]
        public int HelloWorld(string teste)
        {
            return 2;
        }

        [WebMethod]
        public System.Xml.XmlDocument WSgetXMLRemoteData(string strSQL, string applicationName)
        {
            System.Xml.XmlDocument result;
            System.Data.DataSet dataSet = null;

            m_oDataInterface = new dataInterface(applicationName);

            string Error = null;

            m_oDataInterface.getRemoteData(ref strSQL, ref Error, ref dataSet);

            if (Error != null)
            {

                Error = Error.Replace("'", "\"");

                //dataSet = m_oDataInterface.getRemoteData("select '" + Error + "' as OverflyRDSError");

                Error = "select '" + Error + "' as OverflyRDSError";
                m_oDataInterface.getRemoteDataEX(ref Error, ref dataSet);
            }

            try
            {
                result = m_oDataInterface.datasetToXML(dataSet);
            }
            catch (Exception e)
            {

                if (dataSet != null)
                    dataSet.Dispose();

                throw e;
            }

            if (dataSet != null)
                dataSet.Dispose();

            return result;
        }

        [WebMethod]
        public string WSupdateRecords(string strRecords, string culture, string applicationName)
        {
            m_oDataInterface = new dataInterface(applicationName);

            string retorno = m_oDataInterface.updateRecords(strRecords, culture);

            return retorno;
        }
    }
}
