using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Reflection;
using System.Xml;
using Overfly3.Attributes;
using WSData;

namespace System.Web.UI
{
	public abstract class OverflyPage : System.Web.UI.Page
	{
		/*
		 * Separador utilizado nos par�metros que s�o arrays, mas recebidos
		 * como string separados por v�rgula.
		 */
		public static char[] Separator = new char[] {','};

		/*
		 * Conjunto de parametros recebidos pela p�gina.
		 */
		protected System.Web.UI.Parameters parameters;
		
		/*
		 * Guarda a resposta XML gerada por uma pagina de acesso a dados.
		 */
		protected string responseXML;
		
		/* 
		 * Propriedade read-only de acesso ao XML de resposta de uma
		 * p�gina de dados.
		 * Para uma p�gina de acesso a dados, seu conte�do deve ser escrito 
		 * conforme o exemplo:
		 * 
		 *		<%
		 *			Response.ContentType = "text/xml";
		 *			Response.ContentEncoding = System.Text.Encoding.UTF8;
		 *			Response.Write(ResponseXML);
		 *		%>
		 * 
		 * Pode haver mudan�a na propriedade Response.ContentEncoding a depender do tipo
		 * de codifica��o. As demais linha devem permanecer inalteradas.
		 */
		public string ResponseXML
		{
			get { return responseXML; }
		}

		protected dataInterface DataInterfaceObj = new dataInterface(
			System.Configuration.ConfigurationManager.AppSettings["application"]
		);


		/*
		 * Construtor default da classe da p�gina. Ele l� autom�ticamente os 
		 * par�metros enviados � p�gina.
		 */
		public OverflyPage()
			: base()
		{
			parameters = new System.Web.UI.Parameters();
			
			responseXML = "";
		}
		
		/*
		 * L� os par�meros recebidos pela p�gina e atribui seu valor ao 
		 * atributo correspondente do objeto webParameters.
		 */
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!this.IsPostBack)
			{
				try
				{
					Response.Cache.SetNoStore();
					Response.Cache.SetCacheability(HttpCacheability.NoCache);

					// Pega os par�metros da p�gina.
					ReadParameters();
						
					// Chama o m�todo da classe derivada que far� o trabalho.
					PageLoad(sender, e);
				}
				catch (Exception exception)
				{
					Response.StatusCode = 500;
					Response.StatusDescription = "#ERROR#OverflyPage (" + this.GetType().Name + 
						"):" + exception.Message; 
				}
			}
		}

		protected abstract void PageLoad(object sender, EventArgs e);

		/*
		 */
		protected void ReadParameters()
		{
			Type pageType = this.GetType();
			PropertyInfo[] properties = pageType.GetProperties(
				BindingFlags.Public | 
				BindingFlags.NonPublic | 
				BindingFlags.Instance);

			// Pega os parametros recebidos.
			foreach (string key in Request.QueryString.AllKeys)
			{
				parameters.Add(key, Request.QueryString[key]);
			}

			// Carrega os valores nas propriedades da p�gina.
			foreach (PropertyInfo property in properties)
			{
				if (parameters[property.Name] == null)
				{
					continue;
				}
				
				if (property.PropertyType.IsArray)
				{
					if (property.PropertyType.Name.StartsWith("Int"))
						property.SetValue(
							this,
							parameters.AsIntArray(property.Name),
							null);

					else if (property.PropertyType.Name.Equals("String[]"))
						property.SetValue(
							this,
							parameters.AsArray(property.Name),
							null);

					else if (property.PropertyType.Name.Equals("Long[]"))
						property.SetValue(
							this,
							parameters.AsLongArray(property.Name),
							null);

					else if (property.PropertyType.Name.Equals("Float[]"))
						property.SetValue(
							this,
							parameters.AsFloatArray(property.Name),
							null);

					else if (property.PropertyType.Name.Equals("Double[]"))
						property.SetValue(
							this,
							parameters.AsDoubleArray(property.Name),
							null);

					else if (property.PropertyType.Name.Equals("DateTimeArray[]"))
						property.SetValue(
							this,
							parameters.AsDateTimeArray(property.Name),
							null);

					else if (property.PropertyType.Name.Equals("Boolean[]"))
						property.SetValue(
							this,
							parameters.AsBooleanArray(property.Name),
							null);

					else
						property.SetValue(
							this,
							parameters.AsObjectArray(property.Name),
							null);
				}
				else
				{
					if (property.PropertyType.Name.StartsWith("Int"))
						property.SetValue(
							this,
							parameters.AsInt(property.Name),
							null);

					else if (property.PropertyType.Name.Equals("String"))
						property.SetValue(
							this,
							parameters[property.Name],
							null);

					else if (property.PropertyType.Name.Equals("Long"))
						property.SetValue(
							this,
							parameters.AsLong(property.Name),
							null);

					else if (property.PropertyType.Name.Equals("Float"))
						property.SetValue(
							this,
							parameters.AsFloat(property.Name),
							null);

					else if (property.PropertyType.Name.Equals("Double"))
						property.SetValue(
							this,
							parameters.AsDouble(property.Name),
							null);

					else if (property.PropertyType.Name.Equals("DateTimeArray"))
						property.SetValue(
							this,
							parameters.AsDateTime(property.Name),
							null);

					else if (property.PropertyType.Name.Equals("Boolean"))
						property.SetValue(
							this,
							parameters.AsBoolean(property.Name),
							null);

					else
						property.SetValue(
							this,
							parameters.AsObject(property.Name),
							null);
				}
			}
		}

		/*
		 * Escreve a sa�da em XML com o DataSet recebido.
		 */
		protected void WriteResultXML(DataSet dataSet)
		{
			// Converte o dso para XML
			System.Xml.XmlDocument xmlDataSet =
				DataInterfaceObj.datasetToXML(dataSet);

			// Concatena o cabecalho e o conteudo do rsultado.
			responseXML = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>" +
				xmlDataSet.InnerXml;
			
			Response.ContentType = "text/xml";
			Response.ContentEncoding = System.Text.Encoding.UTF8;
			Response.Write(responseXML);
		}
		
		/*
		 * Roda a procedure solicitada gerarando um DataSet.
		 */
		protected DataSet CallQueryProcedure(string procedureName)
		{
			DataSet result = DataInterfaceObj.execQueryProcedure(
				procedureName,
				ReadProcedureParameters(procedureName));
			
			return result;
		}

		/*
		 * Roda a procedure solicitada sem gerar um DataSet.
		 */
		protected void CallNonQueryProcedure(string procedureName)
		{
			DataInterfaceObj.execNonQueryProcedure(
				procedureName,
				ReadProcedureParameters(procedureName));
		}

		/*
		 * Retorna o conjunto de par�metros da procedure solicitada.
		 * Para que este conjunto seja criado � necess�rio que o programador
		 * tenha definido os atributos ProcedureDefinition na classe da p�gina
		 * e os ProcedureParameterDefinition correspondentes aos dados dos 
		 * par�metros para as propriedades da p�gina.
		 */
		protected ProcedureParameters[] ReadProcedureParameters(string procedureName)
		{
			ProcedureParameters[] parameters;
			
			/**************************************************************** 
			 *  Verifica se a classe possui uma defini��o para a procedure. *
			 ****************************************************************/
			// Obt�m todos os atributos customizados definidos 
			// para a classe.
			object[] classAttrs = this.GetType().GetCustomAttributes(true);
			ProcedureDefinition pd = null;

			// Para cada atributo customizado da classe...
			foreach (object classAttr in classAttrs)
			{
				// ... se for um ProcedureDefinition e for a procedure 
				// solicitada, ...
				if((classAttr is ProcedureDefinition) &&
				  ((ProcedureDefinition)classAttr).Name.Equals(procedureName))
				{
					// ... pega a refer�ncia do objeto e ...
					pd = (ProcedureDefinition) classAttr;
					// ... sai, pois precisa apenas da primeira defini��o.
					// Na verdade, se tudo estiver correto, n�o deve haver outra.
					break;
				}
			}

			// Se n�o foi obtida uma refer�ncia do objeto, n�o se
			// definil a procedure para esta classe.
			if (pd == null)
			{
				throw new Exception("A procedure " +
					procedureName +
					" n�o foi definida para a classe " +
					this.GetType().Name + ".");
			}

			// Se h� defini��o para a procedure,
			// cria o espa�o necess�rio para seus par�metros.
			parameters = new ProcedureParameters[pd.ParametersLength];
			
			/***************************************************
			 * Busca os parametros definidos para a procedure. *
			 ***************************************************/
			// Obt�m todos os par�metros definidos na p�gina.
			PropertyInfo[] properties = this.GetType().GetProperties();

			// Para cada propriedade definida na p�gina...
			foreach (PropertyInfo property in properties)
			{
				ProcedureParameterDefinition ppd;
				ProcedureParameters parameter;
				
				// Obt�m todos os atributos customizados definidos 
				// para a propriedade.
				object[] attributes = property.GetCustomAttributes(true);

				// Para cada atributo customizado definido na propriedade...
				foreach (object procParamDef in attributes)
				{
					if(
						// .. se n�o for um ProcedureParameterDefinition, 
						!(procParamDef is ProcedureParameterDefinition) ||
						// e n�o for um par�metro definido para a procedure definida,
						!((ProcedureParameterDefinition)procParamDef).
							ProcedureName.Equals(procedureName)
					  )
					{
						// ... passa para o pr�ximo attributo.
						continue;
					}
					
					// Pega o refer�ncia do par�metro, ...
					ppd = (ProcedureParameterDefinition) procParamDef;
					// ... cria a refer�ncia do par�metro da procedure, ...
					parameter = new ProcedureParameters();

					// ... setar o objeto de par�metro, ...
					parameter.Name = ppd.Name;
					parameter.Type = ppd.Type;
					parameter.Data = property.GetValue(this, null) != null ?
						property.GetValue(this, null).ToString() :
						null;
					parameter.Direction = ppd.IsOutput ? 
						ParameterDirection.InputOutput :
						ParameterDirection.Input;
					parameter.Length = ppd.Length;

					// ... e coloca o par�metro na posi��o definida.
					parameters[ppd.Position] = parameter;
				}
			}

			return parameters;
		}
	}
}
