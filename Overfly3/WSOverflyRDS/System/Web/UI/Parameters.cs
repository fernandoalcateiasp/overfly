using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections;
using System.Reflection;
using java.lang;

namespace System.Web.UI
{
	public class Parameters : Hashtable
	{
		/*
		 * Separador utilizado nos par�metros que s�o arrays, mas recebidos
		 * como string separados por v�rgula.
		 */
		private static char[] Separator = new char[] { ',' };

		/*
		 * Retorna o par�metro informado como um array de string.
		 * 
		 * <throws>
		 * System.FormatException
		 * System.ArgumentException
		 */
		public string[] AsArray(string name)
		{
			string[] result;

			try
			{
				result = ((string)this[name]).Split(Parameters.Separator);
			}
			catch(Exception)
			{
				result = null;
			}
			
			return result;
		}

		/*
		 * Retorna o par�metro informado como um int.
		 * 
		 * <throws>
		 * System.ArgumentException
		 * System.ArgumentNullException
		 * System.OverflowException
		 * System.FormatException
		 */
		public Integer AsInt(string name)
		{
			Integer result;

			try
			{
				result = new Integer(int.Parse((string)this[name]));
			}
			catch (Exception)
			{
				result = null;
			}

			return result;
		}

		/*
		 * Retorna o par�metro informado como um array de int.
		 * 
		 * <throws>
		 * System.ArgumentException
		 * System.ArgumentNullException
		 * System.OverflowException
		 * System.FormatException
		 */
		public Integer[] AsIntArray(string name)
		{
			// Pega o conjunto de dados.
			string[] source = ((string)this[name]).Split(Parameters.Separator);
			// Cria o conjunto de resposta.
			Integer[] result = new Integer[source.Length];

			// Converte os valores.
			for(int i = 0; i < source.Length; i++)
			{
				try
				{
                    result[i] = new Integer(int.Parse((string)source[i]));
				}
				catch (Exception)
				{
					result[i] = null;
				}
			}
				
			return result;
		}

		/*
		 * Retorna o par�metro informado como um long.
		 * 
		 * <throws>
		 * System.ArgumentException
		 * System.ArgumentNullException
		 * System.OverflowException
		 * System.FormatException
		 */
		public Long AsLong(string name)
		{
			Long result;

			try
			{
				result = new Long(long.Parse((string)this[name]));
			}
			catch (Exception)
			{
				result = null;
			}

			return result;
		}

		/*
		 * Retorna o par�metro informado como um array de long.
		 * 
		 * <throws>
		 * System.ArgumentException
		 * System.ArgumentNullException
		 * System.OverflowException
		 * System.FormatException
		 */
		public Long[] AsLongArray(string name)
		{
			// Pega o conjunto de dados.
			string[] source = ((string)this[name]).Split(Parameters.Separator);
			// Cria o conjunto de resposta.
			Long[] result = new Long[source.Length];

			// Converte os valores.
			for (int i = 0; i < source.Length; i++)
			{
				try
				{
                    result[i] = new Long(long.Parse((string)source[i]));
				}
				catch (Exception)
				{
					result[i] = null;
				}
			}

			return result;
		}

		/*
		 * Retorna o par�metro informado como um float.
		 * 
		 * <throws>
		 * System.ArgumentException
		 * System.ArgumentNullException
		 * System.OverflowException
		 * System.FormatException
		 */
		public Float AsFloat(string name)
		{
			Float result;

			try
			{
				result = new Float(float.Parse((string)this[name]));
			}
			catch (Exception)
			{
				result = null;
			}

			return result;
		}

		/*
		 * Retorna o par�metro informado como um float.
		 * 
		 * <throws>
		 * System.ArgumentException
		 * System.ArgumentNullException
		 * System.OverflowException
		 * System.FormatException
		 */
		public Float[] AsFloatArray(string name)
		{
			// Pega o conjunto de dados.
			string[] source = ((string)this[name]).Split(Parameters.Separator);
			// Cria o conjunto de resposta.
			Float[] result = new Float[source.Length];

			// Converte os valores.
			for (int i = 0; i < source.Length; i++)
			{
				try
				{
                    result[i] = new Float(float.Parse((string)source[i]));
				}
				catch (Exception)
				{
					result[i] = null;
				}
			}

			return result;
		}

		/*
		 * Retorna o par�metro informado como um double.
		 * 
		 * <throws>
		 * System.ArgumentException
		 * System.ArgumentNullException
		 * System.OverflowException
		 * System.FormatException
		 */
		public java.lang.Double AsDouble(string name)
		{
			java.lang.Double result;

			try
			{
                result = new java.lang.Double(float.Parse((string)this[name])); 
			}
			catch (Exception)
			{
				result = null;
			}

			return result;
		}

		/*
		 * Retorna o par�metro informado como um double.
		 * 
		 * <throws>
		 * System.ArgumentException
		 * System.ArgumentNullException
		 * System.OverflowException
		 * System.FormatException
		 */
		public java.lang.Double[] AsDoubleArray(string name)
		{
			// Pega o conjunto de dados.
			string[] source = ((string)this[name]).Split(Parameters.Separator);
			// Cria o conjunto de resposta.
			java.lang.Double[] result = new java.lang.Double[source.Length];

			// Converte os valores.
			for (int i = 0; i < source.Length; i++)
			{
				try
				{
                    result[i] = new java.lang.Double(float.Parse((string)source[i]));
				}
				catch (Exception)
				{
					result[i] = null;
				}
			}

			return result;
		}

		/*
		 * Retorna o par�metro informado como um DateTime.
		 * 
		 * <throws>
		 * System.ArgumentException
		 * System.ArgumentNullException
		 * System.FormatException
		 */
		public DateTime AsDateTime(string name)
		{
			// Verifica se o par�metro existe.
			if (this[name] == null)
			{
				throw new System.ArgumentException(name + " n�o existe.");
			}

			return DateTime.Parse(
				(string)this[name], 
				System.Globalization.CultureInfo.InvariantCulture.DateTimeFormat);
		}

		/*
		 * Retorna o par�metro informado como um DateTime.
		 * 
		 * <throws>
		 * System.ArgumentException
		 * System.ArgumentNullException
		 * System.FormatException
		 */
		public DateTime[] AsDateTimeArray(string name)
		{
			// Verifica se o par�metro existe.
			if (this[name] == null)
			{
				throw new System.ArgumentException(name + " n�o existe.");
			}

			// Pega o conjunto de dados.
			string[] source = ((string)this[name]).Split(Parameters.Separator);
			// Cria o conjunto de resposta.
			DateTime[] result = new DateTime[source.Length];

			// Converte os valores.
			for (int i = 0;
				i < source.Length;
				result[i] = DateTime.Parse((string)source[i++],
							System.Globalization.CultureInfo.InvariantCulture.DateTimeFormat));

			return result;
		}

		/*
		 * Retorna o par�metro informado como um Boolean.
		 * 
		 * <throws>
		 * System.ArgumentNullException
		 * System.FormatException
		 */
		public java.lang.Boolean AsBoolean(string name)
		{
			// Verifica se o par�metro existe.
			if (this[name] == null)
			{
				throw new System.ArgumentException(name + " n�o existe.");
			}

			string valor = ((string)this[name]);

			bool result = valor.Equals("1") 
				|| valor.ToLower().Equals("true") 
				|| valor.ToLower().Equals("verdadeiro");
			
			return new java.lang.Boolean(result);
		}

		/*
		 * Retorna o par�metro informado como um Boolean.
		 * 
		 * <throws>
		 * System.ArgumentNullException
		 * System.FormatException
		 */
		public Boolean[] AsBooleanArray(string name)
		{
			// Verifica se o par�metro existe.
			if (this[name] == null)
			{
				throw new System.ArgumentException(name + " n�o existe.");
			}

			// Pega o conjunto de dados.
			string[] source = ((string)this[name]).Split(Parameters.Separator);
			// Cria o conjunto de resposta.
			Boolean[] result = new Boolean[source.Length];

			// Converte os valores.
			for (int i = 0;
				i < source.Length;
				result[i] = Boolean.Parse((string)source[i++])) ;

			return result;
		}

		/*
		 * Retorna o par�metro informado como um Object.
		 * 
		 * <throws>
		 * System.ArgumentNullException
		 * System.FormatException
		 */
		public Object AsObject(string name)
		{
			// Verifica se o par�metro existe.
			if (this[name] == null)
			{
				throw new System.ArgumentException(name + " n�o existe.");
			}

			return this[name];
		}

		/*
		 * Retorna o par�metro informado como um Boolean.
		 * 
		 * <throws>
		 * System.ArgumentNullException
		 * System.FormatException
		 */
		public Object[] AsObjectArray(string name)
		{
			// Verifica se o par�metro existe.
			if (this[name] == null)
			{
				throw new System.ArgumentException(name + " n�o existe.");
			}

			// Pega o conjunto de dados.
			string[] source = ((string)this[name]).Split(Parameters.Separator);
			// Cria o conjunto de resposta.
			Object[] result = new Object[source.Length];

			// Converte os valores.
			for (int i = 0;
				i < source.Length;
				result[i] = source[i++]) ;

			// Finito!
			return result;
		}
	}
}
