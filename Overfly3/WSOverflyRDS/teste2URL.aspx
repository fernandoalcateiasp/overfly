<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="teste2URL.aspx.cs" Inherits="WSData.teste2URL" %>
<%
    System.Data.DataSet testeDataSet =
		m_oDataInterface.getRemoteData("select top 0 * from _RdsTable");
	
    string strXML = "";

    // Converte o dso para XML
    System.Xml.XmlDocument xmlDataSet = m_oDataInterface.datasetToXML(testeDataSet);
    strXML = xmlDataSet.InnerXml;
    strXML = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>" + strXML;
    
    Response.ContentType = "text/xml";
    Response.ContentEncoding = System.Text.Encoding.UTF8;
    Response.Write(strXML);
%>
