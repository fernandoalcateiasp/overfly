using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Overfly3.Attributes
{
	[AttributeUsage(
		AttributeTargets.Property,
		AllowMultiple = true)]
	public class ProcedureParameterDefinition : System.Attribute
	{
		private string procedureName;
		private string name;
		private int position;
		private SqlDbType type;
		private int length;
		private bool isOutput;
		
		public ProcedureParameterDefinition()
		{
			isOutput = false;
		}

		public string ProcedureName
		{
			get { return procedureName; }
			set { procedureName = value; }
		}

		public string Name
		{
			get { return name; }
			set { name = value; }
		}

		public int Position
		{
			get { return position; }
			set { position = value; }
		}
		
		public SqlDbType Type
		{
			get { return type; }
			set { type = value; }
		}
		
		public int Length
		{
			get { return length; }
			set { length = value; }
		}
		
		public bool IsOutput
		{
			get { return isOutput; }
			set { isOutput = value; }
		}
	}
}
