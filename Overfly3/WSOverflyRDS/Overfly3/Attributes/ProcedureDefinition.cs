using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Overfly3.Attributes
{
	[AttributeUsage(
		AttributeTargets.Class,
		AllowMultiple = true)]
	public class ProcedureDefinition : System.Attribute
	{
		private string name;
		private int parametersLength;
		
		public ProcedureDefinition()
		{
		}

		public string Name
		{
			get { return name; }
			set { name = value; }
		}
		
		public int ParametersLength
		{
			get { return parametersLength; }
			set { parametersLength = value; }
		}
	}
}
