using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace WSData
{
	public class FieldData
	{
		private string _nome;
		private string _valor;
		private string _tipo;
		private bool _alterado;

		private long attribute;

		/**
		 * Cria um objeto que representa um campo do registro da tabela que sera
		 * atualizada.
		 */
		public FieldData(string record)
		{
			// O parametro record segue o formato:
			// <FLAG ALTERADO>;<CAMPO>;<TIPO>;<VALOR>;<ATTRIBUTOS>
			// Exemplo de uma alteracao de registro.
			// "0[char(13)]PessoaID[char(13)]int[char(13)]7[char(13)]557056"
			// "1[char(13)]Fantasia[char(13)]varchar[char(13)]Allplus[char(13)]12"
			char[] marks = new char[] { (char)25 };
			string[] values = record.Split(marks);

			// Se n�o houver tres valores no array, significa que 
			// o formato de record esta errada.
			if (values.Length != 5)
			{
				throw new FormatException(
					"O formato do registro recebido n�o est� correto: " +
					record
					);
			}

			// Recupera os valores dos attributos.
			alterado = values[0].Equals("1");
			nome = values[1];
			tipo = values[2];
			valor = !values[3].Equals("null") ? values[3] : null;
			attribute = long.Parse(values[4]);
		}

		public string nome
		{
			get { return _nome; }
			set { _nome = value; }
		}

		public string valor
		{
			get { return _valor; }
			set { _valor = value; }
		}

		public string tipo
		{
			get { return _tipo; }
			set { _tipo = value; }
		}

		public bool alterado
		{
			get { return _alterado; }
			set { _alterado = value; }
		}

		public bool IsKey
		{
			get { return (attribute & 32768) > 0; }
		}

		public bool AllowDBNull
		{
			get { return (attribute & 64) > 0; }
		}

		public bool IsReadOnly
		{
			get { return (attribute & 4) > 0 && (attribute & 8) > 0; }
		}

		public bool IsWriteable
		{
			get { return !IsReadOnly; }
		}

		public bool IsIdentity
		{
			get { return (attribute & 524288) > 0; }
		}

		public bool IsAutoIncrement
		{
			get { return (attribute & 1048576) > 0; }
		}
	}
}
