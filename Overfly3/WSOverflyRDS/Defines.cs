using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace WSData
{
    public class Defines
    {
        public static int dataTypesCode(string typeName)
        {
			typeName = typeName.ToLower();
			
            if (typeName.Equals("bigint")) return 20;
            else if (typeName.Equals("binary")) return 128;
            else if (typeName.Equals("bit")) return 11;
            else if (typeName.Equals("char")) return 129;
            else if (typeName.Equals("datetime")) return 135;
            else if (typeName.Equals("decimal")) return 131;
            else if (typeName.Equals("float")) return 5;
            else if (typeName.Equals("image")) return 205;
            else if (typeName.Equals("int")) return 3;
            else if (typeName.Equals("money")) return 6;
            else if (typeName.Equals("nchar")) return 130;
            else if (typeName.Equals("ntext")) return 203;
            else if (typeName.Equals("numeric")) return 131;
            else if (typeName.Equals("nvarchar")) return 202;
            else if (typeName.Equals("real")) return 4;
            else if (typeName.Equals("smalldatetime")) return 135;
            else if (typeName.Equals("smallint")) return 2;
            else if (typeName.Equals("smallmoney")) return 6;
            else if (typeName.Equals("sql_variant")) return 12;
            else if (typeName.Equals("text")) return 201;
            else if (typeName.Equals("timestamp")) return 128;
            else if (typeName.Equals("tinyint")) return 17;
            else if (typeName.Equals("uniqueidentifier")) return 72;
            else if (typeName.Equals("varbinary")) return 204;
            else if (typeName.Equals("varchar")) return 200;
            else return -1;
        }

        public static string dataTypesName(int typeName)
        {
            if (typeName == 20) return "bigint";
            else if (typeName == 128) return "binary";
            else if (typeName == 11) return "bit";
            else if (typeName == 129) return "char";
            else if (typeName == 135) return "datetime";
            else if (typeName == 131) return "decimal";
            else if (typeName == 5) return "float";
            else if (typeName == 205) return "image";
            else if (typeName == 3) return "int";
            else if (typeName == 6) return "money";
            else if (typeName == 130) return "nchar";
            else if (typeName == 203) return "ntext";
            else if (typeName == 131) return "numeric";
            else if (typeName == 202) return "nvarchar";
            else if (typeName == 4) return "real";
            else if (typeName == 135) return "smalldatetime";
            else if (typeName == 2) return "smallint";
            else if (typeName == 6) return "smallmoney";
            else if (typeName == 12) return "sql_variant";
            else if (typeName == 201) return "text";
            else if (typeName == 128) return "timestamp";
            else if (typeName == 17) return "tinyint";
            else if (typeName == 72) return "uniqueidentifier";
            else if (typeName == 204) return "varbinary";
            else if (typeName == 200) return "varchar";
            else return "";
        }
    }
}
