using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace WSData
{
	public class RecordData
	{
		private const int UPDATE_ACTION = 1;
		private const int INSERT_ACTION = 2;
		private const int DELETE_ACTION = 3;

		private int _action;
		private string _table;
		private System.Collections.ArrayList _fields;
		private string _culture;
		
		private RecordData _originalRecord;

		/**
		 * Constroi um RecordData com os registros passados por
		 * paranetros.
		 * Constroi-se tambem um RecordData interno para os dados originais 
		 * recuperados do bando de dados.
		 */
		public RecordData(int act, string entity, string[] records, string[] original, string culture)
		{
			init(act, entity, records, culture);

			// Constroi um RecordData interno para o controle de alteracoes e
			// exclusoes, colocando os dados originais na clausula where do 
			// comando delete ou update.
			if (act != INSERT_ACTION)
			{
				_originalRecord = new RecordData(act, entity, original, culture);
			}

		}

		/**
		 * Constroi um RecordData com os registros passados por
		 * paranetros.
		 */
		public RecordData(int act, string entity, string[] records, string culture)
		{
			init(act, entity, records, culture);
		}

		/**
		 * Funcoa auxiliar para a construcao do objeto.
		 * Esta funcao e' necessaria para que nao ocorra um laco infinito 
		 * quando se usa o construtor que recebe os registros originais, lidos do
		 * banco de dados.
		 */
		private void init(int act, string entity, string[] records, string culture)
		{
			_action = act;
			_table = entity;
			_fields = new System.Collections.ArrayList();
			_culture = culture;

			for (int r = 0; r < records.Length; r++)
			{
				// Evita a construcao do FieldData com string vazia.
				if (records[r].Length == 0)
				{
					continue;
				}

				FieldData field = new FieldData(records[r]);

				fields.Add(field);
			}
		}

		public int action
		{
			get { return _action; }
		}

		public string table
		{
			get { return _table; }
		}

		public System.Collections.ArrayList fields
		{
			get { return _fields; }
		}

		override
		public string ToString()
		{
			switch(action) {
				case UPDATE_ACTION:
					return this.makeUpdateCommand();
				case INSERT_ACTION:
					return this.makeInsertCommand();
				case DELETE_ACTION:
					return this.makeDeleteCommand();
				default:
					throw new Exception("A��o sobre o registro n�o � v�lida.");
			}
		}

		private string makeUpdateCommand()
		{
			System.Text.StringBuilder update = new System.Text.StringBuilder();
			System.Text.StringBuilder where = new System.Text.StringBuilder();
			System.Text.StringBuilder key = new System.Text.StringBuilder();

			// Monta o set do comando update.
			for (int i = 0; i < fields.Count; i++)
			{
				if (((FieldData)fields[i]).IsWriteable && ((FieldData)fields[i]).alterado)
				{
					// Se o campo � auto incrementado ou apenas para leitura,
					// ele n�o pode fazer parte da atualiza��o.
					// O mesmo ocorrendo se ele for chave.
					// Al�m disso se o campo n�o permitir nulo e o valor 
					// estiver nulo, ele tamb�m n�o far� parte da atualiza��o.
					if (((FieldData)fields[i]).IsAutoIncrement ||
						((FieldData)fields[i]).IsReadOnly ||
						((FieldData)fields[i]).IsKey ||
						(!((FieldData)fields[i]).AllowDBNull && ((FieldData)fields[i]).valor == null))
					{
						continue;
					}

					update.Append(((FieldData)fields[i]).nome);
					update.Append(" = ");
					update.Append(valueToSQL(
						((FieldData)fields[i]).tipo,
						((FieldData)fields[i]).valor));

					if (i < fields.Count - 1 &&
						!((FieldData)fields[i + 1]).IsKey &&
						!((FieldData)fields[i + 1]).IsAutoIncrement &&
						!((FieldData)fields[i + 1]).IsReadOnly &&
						 ((FieldData)fields[i + 1]).alterado)
					{
						update.Append(", ");
					}
				}
			}

			// Monta a condicao de existencia da chave primaria que testa a 
			// existencia do registro caso a atualizacao nao afete linhas.
			for (int i = 0; i < _originalRecord.fields.Count; i++)
			{
				if (((FieldData)_originalRecord.fields[i]).IsKey ||
					((FieldData)_originalRecord.fields[i]).IsAutoIncrement)
				{
					// Monta a chave do registro para consulta-lo no caso
					// da atualizacao nao ocorrer.
					key.Append(((FieldData)_originalRecord.fields[i]).nome);
					key.Append(" = ");
					key.Append(valueToSQL(
						((FieldData)_originalRecord.fields[i]).tipo,
						((FieldData)_originalRecord.fields[i]).valor));

					if (i < _originalRecord.fields.Count - 1 &&
						((FieldData)_originalRecord.fields[i + 1]).IsKey)
					{
						key.Append(" and ");
					}
				}
			}

			// Cria a string de resultado.
			System.Text.StringBuilder result = new System.Text.StringBuilder();
			
			if(update.Length != 0) {
				result.Append("update ");
				result.Append(this.table);
				result.Append(" set ");
				result.Append(update);
				result.Append(" where ");
				result.Append(makeWhere(_originalRecord.fields));
				result.Append("\n");

				result.Append("if @@rowcount = 0\n");
				result.Append("if ((select count(1) from ");
				result.Append(this.table);
				result.Append(" where ");
				result.Append(key);
				result.Append(" ) > 0) ");
				result.Append("raiserror('O registro foi alterado ou excluido anteriormente.', 16, 1);");
			}

			// Retorna o comando de atualizacao.
			return result.ToString();
		}

		private string makeInsertCommand()
		{
			// insert into <TABELA> (<CAMPO1>, ..., <CAMPOn>) values (<VALOR1>, ..., <VALORn>)

			System.Text.StringBuilder nomesDosCampos = new System.Text.StringBuilder(" (");
			System.Text.StringBuilder valores = new System.Text.StringBuilder(" (");

			for (int i = 0; i < fields.Count; i++)
			{
				// Se o campo � auto incrementado ou apenas para leitura,
				// ele n�o participa de inserts.
				// Al�m disso se o campo n�o permitir nulo e o valor 
				// estiver nulo, ele tamb�m n�o far� parte da atualiza��o.
				if (((FieldData)fields[i]).IsAutoIncrement ||
					((FieldData)fields[i]).IsReadOnly ||
					(!((FieldData)fields[i]).AllowDBNull && ((FieldData)fields[i]).valor == null)) 
				{
					continue;
				}

				if (nomesDosCampos.Length > 2)
				{
					nomesDosCampos.Append(", ");
					valores.Append(", ");
				}

				nomesDosCampos.Append(((FieldData)fields[i]).nome);
				valores.Append(valueToSQL(
					((FieldData)fields[i]).tipo, 
					((FieldData)fields[i]).valor));
			}

			nomesDosCampos.Append(")");
			valores.Append(")");

			System.Text.StringBuilder result = new System.Text.StringBuilder();
			result.Append("insert into ");
			result.Append(this.table);
			result.Append(nomesDosCampos);
			result.Append(" values ");
			result.Append(valores);

			return result.ToString();
		}

		private string makeDeleteCommand()
		{
			System.Text.StringBuilder where = new System.Text.StringBuilder();
			System.Text.StringBuilder key = new System.Text.StringBuilder();

			for (int i = 0; i < fields.Count; i++)
			{
				if (((FieldData)fields[i]).IsKey)
				{
					// Monta a chave do registro para consulta-lo no caso
					// da atualizacao nao ocorrer.
					key.Append(((FieldData)_originalRecord.fields[i]).nome);
					key.Append(" = ");
					key.Append(valueToSQL(
						((FieldData)_originalRecord.fields[i]).tipo,
						((FieldData)_originalRecord.fields[i]).valor));

					if (i < fields.Count - 1 && ((FieldData)fields[i + 1]).IsKey)
					{
						key.Append(" and ");
					}
				}
			}

			// Cria a string de resultado.
			System.Text.StringBuilder result = new System.Text.StringBuilder();

			result.Append("delete from ");
			result.Append(this.table);
			result.Append(" where ");
			result.Append(makeWhere(_originalRecord.fields));
			result.Append("\n");

			result.Append("if @@rowcount = 0\n");
			result.Append("if ((select count(1) from ");
			result.Append(this.table);
			result.Append(" where ");
			result.Append(key);
			result.Append(" ) > 0) ");
			result.Append("raiserror('O registro foi alterado ou excluido anteriormente.', 16, 1);");

			// Retorna o comando de atualizacao.
			return result.ToString();
		}

		/**
		 * Constroi a clausula where que identifica mudancas no banco de dados.
		 */
		private string makeWhere(System.Collections.ArrayList fields)
		{
			System.Text.StringBuilder where = new System.Text.StringBuilder();

			// Monta a clausula where com os compos que contem os dados originais
			// do banco de dados.
			for (int i = 0; i < fields.Count; i++)
			{
				// Monta a clausula where para identificar se o registro foi alterado.
				if ((((FieldData)fields[i]).valor) != null)
				{
					where.Append(((FieldData)fields[i]).nome);
					where.Append(" = ");
					where.Append(valueToSQL(
						((FieldData)fields[i]).tipo,
						((FieldData)fields[i]).valor));
				}
				else
				{
					where.Append(((FieldData)fields[i]).nome);
					where.Append(" is null ");
				}

				if (i < fields.Count - 1)
				{
					where.Append(" and ");
				}
			}

			return where.ToString();
		}

		private string valueToSQL(string type, string value)
		{
			if (value == null)
			{
				return "null";
			}
			else if (type == "varchar" || type == "nvarchar" || type == "text" || type == "char")
			{
				return "'" + value + "'";
			}
			else if (type == "datetime")
			{
				IFormatProvider culture =
					new System.Globalization.CultureInfo(_culture, true);
					
				DateTime dt = DateTime.Parse(
					value,
					culture,
					System.Globalization.DateTimeStyles.NoCurrentDateDefault);

				string milli = dt.Millisecond.ToString();
				
				while(milli.Length < 3)
				{
					milli = "0" + milli;
				}
				
				string result = "convert(datetime, '" +
					dt.Month + "/" +
					dt.Day + "/" +
					dt.Year + " " +
					dt.Hour + ":" +
					dt.Minute + ":" +
					dt.Second + "." +
					milli + "')";

				return result;
			}
			else if (type == "bit")
			{
				return (value.Equals("0") || value.ToLower().Equals("false") || value.ToLower().Equals("falso")) ? "0" : "1";
			}

			return value;
		}
	}
}
