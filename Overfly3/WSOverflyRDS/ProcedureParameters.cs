using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace WSData
{
    /**
     * Classe que representa um parametro passado para uma procedure.
     */
    public class ProcedureParameters
    {
        private string name;
        private System.Data.SqlDbType type;
        private Object data;
        private int length;
        private ParameterDirection direction;

		public ProcedureParameters() 
		{
		}
		
        /**
         * Constroi um parametro de entrada.
         */
        public ProcedureParameters(string pName, System.Data.SqlDbType pType, Object pData)
        {
            Name = pName;
            Type = pType;
            Data = pData;
            Length = -1;
            Direction = ParameterDirection.Input;
        }

		/**
		 * Constroi um parametro de entrada com tamanho.
		 */
		public ProcedureParameters(string pName, System.Data.SqlDbType pType, Object pData, int pLength)
		{
			Name = pName;
			Type = pType;
			Data = pData;
			Length = pLength;
			Direction = ParameterDirection.Input;
		}

		/**
		 * Constroi um parametro com o dire��o definida em ParameterDirection.
		 */
        public ProcedureParameters(string pName, System.Data.SqlDbType pType, Object pData, ParameterDirection pDirection)
        {
            Name = pName;
            Type = pType;
            Data = pData;
            Length = -1;
            Direction = pDirection;
        }

		/**
		 * Constroi um parametro com tamanho e dire��o definida em ParameterDirection.
		 */
		public ProcedureParameters(string pName, System.Data.SqlDbType pType, Object pData, ParameterDirection pDirection, int pLength)
		{
			Name = pName;
			Type = pType;
			Data = pData;
			Length = pLength;
			Direction = pDirection;
		}

		public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public System.Data.SqlDbType Type
        {
            get { return type; }
            set { type = value; }
        }

        public Object Data
        {
            get { return data; }
            set { data = value; }
        }

        public int Length
        {
            get { return length; }
            set { length = value; }
        }

        public ParameterDirection Direction
        {
            get { return direction; }
            set { direction = value; }
        }
    }
}
